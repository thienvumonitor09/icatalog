<%@ Reference Page="~/program/edit.aspx" %>
<%@ Page language="c#" Inherits="ICatalog.program.popups.addelective" CodeFile="addelective.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Add Option Elective</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
		<script type="text/javascript" src="../../_phatt3_src_files/trim.js"></script>
		<script type="text/javascript">
		<!--
			function validate(){ //shouldn't execute when cancel is clicked on add elective group screen - change this
				var txtElectGroupTitle = document.getElementById("txtElectGroupTitle");
				var txtOverwriteUnitsMinimum = document.getElementById("txtCrsMin"), txtOverwriteUnitsMaximum = document.getElementById("txtCrsMax");
				var hidUnitsMinimum = document.getElementById("hidUnitsMinimum"), hidUnitsMaximum = document.getElementById("hidUnitsMaximum");
				var chkOverwrite = document.getElementById("chkOverwrite");
				var isValid = false;
				
				if(txtElectGroupTitle != null){
					if(trim(txtElectGroupTitle.value) == ""){
						alert("Please enter an elective group title.");
						txtElectGroupTitle.select();
					}else{
						isValid = true;
					}
				}else{	
					if(document.getElementById("hidFocus").value == "cmdAddProgElect" && document.getElementById("cboElectGroup").value == ""){
						alert("Please add an elective group before continuing.");
					}else if(chkOverwrite == null){
						isValid = true;
					}else if(chkOverwrite.checked){
						if(txtOverwriteUnitsMinimum.value == ""){
							alert("Please enter the minimum credits.");
							txtOverwriteUnitsMinimum.select();
						}else if(isNaN(txtOverwriteUnitsMinimum.value)){
							alert("Please enter a number for the minimum credits.");
							txtOverwriteUnitsMinimum.select();
						}else if(eval(txtOverwriteUnitsMinimum.value) < eval(hidUnitsMinimum.value) || eval(txtOverwriteUnitsMinimum.value) > eval(hidUnitsMaximum.value)){
							alert("The minimum credits must be within the variable credit range.");
							txtOverwriteUnitsMinimum.select();
						}else if(txtOverwriteUnitsMaximum.value == ""){
							alert("Please enter the maximum credits.");
							txtOverwriteUnitsMaximum.select();
						}else if(isNaN(txtOverwriteUnitsMaximum.value)){
							alert("Please enter a number for the maximum credits.");
							txtOverwriteUnitsMaximum.select();
						}else if(eval(txtOverwriteUnitsMaximum.value) < eval(hidUnitsMinimum.value) || eval(txtOverwriteUnitsMaximum.value) > eval(hidUnitsMaximum.value)){
							alert("The maximum credits must be within the variable credit range.");
							txtOverwriteUnitsMaximum.select();
						}else if(eval(txtOverwriteUnitsMinimum.value) > eval(txtOverwriteUnitsMaximum.value)){
							alert("The minimum credits cannot be more than the maximum credits.");
							txtOverwriteUnitsMinimum.select();
						}else{
							isValid = true;
						}
					}else{
						isValid = true;
					}
				}
				
				if(isValid){
					getProgOptValues();
				}

				return isValid;
			}
			
			function getProgOptValues(){
				document.getElementById("hidOptionDescription").value = window.opener.CKEDITOR.instances['txtOptionDescription'].getData();
				document.getElementById("hidGainfulEmploymentID").value = window.opener.document.getElementById('txtGainfulEmploymentID').value;
				document.getElementById("hidOptionStateApproval").value = window.opener.document.getElementById('txtOptionStateApproval').value;
				document.getElementById("hidCIP").value = window.opener.document.getElementById('txtCIP').value;
				document.getElementById("hidEPC").value = window.opener.document.getElementById('txtEPC').value;
				document.getElementById("hidAcademicPlan").value = window.opener.document.getElementById('txtAcademicPlan').value;
				document.getElementById("hidTotalQuarters").value = window.opener.document.getElementById('txtTotalQuarters').value;
				document.getElementById("hidPrimaryOption").value = window.opener.document.getElementById('chkPrimary').checked;
				
				var strOptionLocationFields = window.opener.document.getElementById('hidLocationFields').value;
				var hidLocations = document.getElementById("hidLocations");
				hidLocations.value = "";
				if(strOptionLocationFields != null && strOptionLocationFields != ""){
					var aryOptionLocationFields = strOptionLocationFields.substring(1).split("|");
					for(var i = 0; i < aryOptionLocationFields.length; i++){

						if(window.opener.document.getElementById(aryOptionLocationFields[i]).checked){
							hidLocations.value += "|" + aryOptionLocationFields[i].replace("chkLocation_" + document.getElementById("hidOptionID").value + "_","");
						}
					}
				}

				var strOptionMCodeFields = window.opener.document.getElementById('hidMCodeFields').value;
				var hidMCodes = document.getElementById("hidMCodes");
				hidMCodes.value = "";
				if(strOptionMCodeFields != null && strOptionMCodeFields != ""){
					var aryOptionMCodeFields = strOptionMCodeFields.substring(1).split("|");
					for(var i = 0; i < aryOptionMCodeFields.length; i++){
						var aryCollegeMCodeFields = aryOptionMCodeFields[i].split(";");
						if(window.opener.document.getElementById(aryCollegeMCodeFields[0]).checked){
							hidMCodes.value += "|" + aryCollegeMCodeFields[0].replace("chkMCode_" + document.getElementById("hidOptionID").value + "_","") + ";" + window.opener.document.getElementById(aryCollegeMCodeFields[1]).value;
						}						
					}
				}
			}
		//-->
		</script>
	</head>
	<body style="background: #ffffff url('');">
		<form id="frmElective" runat="server">
			<input type="hidden" id="hidProgramDegreeID" runat="server" />
			<input type="hidden" id="hidProgramVersionID" runat="server" />
			<input type="hidden" id="hidCollegeID" runat="server" />
			<input type="hidden" id="hidLtr" runat="server" />
			<input type="hidden" id="hidProgramTitle" runat="server" />
			<input type="hidden" id="hidOfferedAt" runat="server" />
			<input type="hidden" id="hidOptionDescription" runat="server" />
			<input type="hidden" id="hidOptionStateApproval" runat="server" />
			<input type="hidden" id="hidCIP" runat="server" />
			<input type="hidden" id="hidEPC" runat="server" />
            <input type="hidden" id="hidAcademicPlan" runat="server" />
            <input type="hidden" id="hidTotalQuarters" runat="server" />
			<input type="hidden" id="hidPrimaryOption" runat="server" />
			<input type="hidden" id="hidDegreeID" runat="server" />
			<input type="hidden" id="hidOptionID" runat="server" />
			<input type="hidden" id="hidProgramDisplay" runat="server" />
			<input type="hidden" id="hidVarCr" runat="server" />
			<input type="hidden" id="hidUnitsMinimum" runat="server" />
			<input type="hidden" id="hidUnitsMaximum" runat="server" />
			<input type="hidden" id="hidFocus" runat="server" />
			<input type="hidden" id="hidProgramBeginSTRM" runat="server" />
			<input type="hidden" id="hidProgramEndSTRM" runat="server" />
			<input type="hidden" id="hidPublishedProgram" runat="server" />
			<input type="hidden" id="hidPublishedProgramVersionID" runat="server" />
			<input type="hidden" id="hidSelectedCollegeID" runat="server" />
			<input type="hidden" id="hidSTRM" runat="server" />
			<input type="hidden" id="hidProgramID" runat="server" />
			<input type="hidden" id="hidMCodes" runat="server" />
			<input type="hidden" id="hidLocations" runat="server" />
			<input type="hidden" id="hidGainfulEmploymentID" runat="server" />
			<input type="hidden" id="hidSearchSubject" runat="server" />
			<input type="hidden" id="hidSearchId" runat="server" />
			<input type="hidden" id="hidSearchSTRM" runat="server" />
			<input type="hidden" id="hidSearchText" runat="server" />
			<table class="centeredTable" style="width:375px;" cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding-left:4px;padding-right:0px;">
						<table style="width:100%;" cellpadding="0" cellspacing="0">
							<tr>
								<td style="background-color:#000000;text-align:left;">
									<asp:panel id="panAddElective" runat="server">
										<table style="width:100%;" cellpadding="1" cellspacing="1">
											<tr>
												<td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;"
													class="portletHeader">
													<table style="width:100%;" cellpadding="0" cellspacing="0">
														<tr>
															<td class="portletHeader" style="width:100%;">&nbsp;Add Option Elective</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="portletMain" style="width:100%">
													<table style="width:100%;" cellspacing="0" cellpadding="0">
														<tr>
															<td class="portletDark" style="width:100%;">
																<table cellspacing="1" cellpadding="0" style="width:100%;">
																	<tr>
																		<td class="portletLight" style="width:100%;vertical-align:text-top;">
																			<table cellpadding="0" cellspacing="0" style="width:100%;">
																				<tr>
																					<td style="width:100%;padding-bottom:15px;">
																						<table cellpadding="0" cellspacing="0" style="width:100%;">
																							<asp:panel runat="server" id="panError">
																								<tr>
																									<td class="portletLight" style="color:red;padding-left:10px;padding-top:15px;">
																										<asp:label runat="server" id="lblErrorMsg"></asp:label>
																									</td>
																								</tr>
																							</asp:panel>
																							<!-- ELECTIVE GROUP SELECTION -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px;">
																									<b>Elective Group:</b>&nbsp;&nbsp;
																								</td>
																							</tr>
																							<tr>
																								<td class="portletLight" style="padding-top:5px;padding-left:10px;padding-right:10px;">
																									<asp:dropdownlist id="cboElectGroup" runat="server" cssclass="small" autopostback="true" OnFocus="document.frmElective.hidFocus.value=this.id;"></asp:dropdownlist>&nbsp;
																									<asp:button id="cmdAddNew" runat="server" text="Add New" cssclass="small" OnFocus="document.frmElective.hidFocus.value=this.id" style="background:#ccccaa;border:1px solid #000000;width:60px;" onclick="cmdAddNew_Click"></asp:button>
																								</td>
																							</tr>
																							<!-- ELECTIVE COURSE DEPARTMENT SELECTION -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px">
																									<b>Subject Area:</b>&nbsp;&nbsp;
																								    <asp:dropdownlist id="cboCrsDept" runat="server" cssclass="small" autopostback="true" OnFocus="document.frmElective.hidFocus.value=this.id;"></asp:dropdownlist>
																								</td>
																							</tr>
																							<!-- ELECTIVE COURSE NUMBER SELECTION -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px">
																									<b>Catalog Nbr:</b>&nbsp;&nbsp;
																								    <asp:dropdownlist id="cboCatalogNbr" runat="server" cssclass="small" autopostback="true" OnFocus="document.frmElective.hidFocus.value=this.id;"></asp:dropdownlist>
																								</td>
																							</tr>
																							<!-- COURSE TERM -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px">
																									<b>Effective Date:</b>&nbsp;&nbsp;
																								    <asp:label id="lblEFFDT" runat="server" cssclass="small"></asp:label>
																								</td>
																							</tr>
																							<!-- LONG COURSE TITLE -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px;">
																									<b>Long Course Title:</b>&nbsp;
																								</td>
																							</tr>
																							<tr>
																								<td class="portletLight" colspan="2" style="padding-top:15px;padding-left:10px;padding-right:10px;">
																									<asp:label id="lblCourseLongTitle" runat="server" cssclass="small"></asp:label>
																								</td>
																							</tr>
																							<!-- COURSE CREDITS -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px;padding-right:10px;">
																									<b>Units:</b>&nbsp;&nbsp;<asp:label id="lblCrsCredits" runat="server"></asp:label>
																								    <asp:panel id="panOverwrite" runat="server" style="display:inline">
																									    &nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" id="chkOverwrite" autopostback="true"></asp:checkbox><b>Overwrite</b>
																								    </asp:panel>
                                                                                                </td>
																							</tr>
																							<asp:panel id="panOverwriteUnits" runat="server">
																								<tr>
																									<td class="portletLight" style="padding-top:5px;width:100%;padding-left:10px;padding-right:10px;">
																										<asp:textbox id="txtCrsMin" columns="1" maxlength="4" style="width:25px;" runat="server" cssclass="small"></asp:textbox>&nbsp;Min
																										&nbsp;&nbsp;&nbsp;
																										<asp:textbox id="txtCrsMax" columns="1" maxlength="4" style="width:25px;" runat="server" cssclass="small"></asp:textbox>&nbsp;Max
																									</td>
																								</tr>
																							</asp:panel>
																							<!-- ELECTIVE FOOTNOTE -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px;white-space:nowrap;">
																									<b>Select Footnote (optional):</b>&nbsp;&nbsp;
																								    <asp:dropdownlist id="cboFootnote" runat="server" cssclass="small" autopostback="true" OnFocus="document.frmElective.hidFocus.value=this.id;"></asp:dropdownlist>
																								</td>
																							</tr>
																							<!-- SELECTED ELECTIVE FOOTNOTE DISPLAY -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px;">
																									<b>Footnote:</b>
																								</td>
																							</tr>
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px;padding-right:10px;width:100%;">
																									<asp:label id="lblFootnote" runat="server" cssclass="small">None Selected</asp:label>
																								</td>
																							</tr>
																							<!-- BUTTONS -->
																							<tr>
																								<td class="portletLight" style="padding-top:20px;width:100%;text-align:center;">
																									<input type="button" id="cmdClose" value="Close Window" class="small" style="background:#ccccaa;border:1px solid #000000;width:85px;" onclick="self.close();" />
																									&nbsp;<asp:button runat="server" id="cmdAddProgElect" OnFocus="document.frmElective.hidFocus.value=this.id;" text="Add Option Elective" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:135px;" onclientclick="return validate();" onclick="cmdAddProgElect_Click"></asp:button>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</asp:panel>
									<asp:panel id="panAddElectGroup" runat="server">
										<table style="width:100%;" cellpadding="1" cellspacing="1">
											<tr>
												<td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;"
													class="portletHeader">
													<table style="width:100%;" cellpadding="0" cellspacing="0">
														<tr>
															<td class="portletHeader" style="width:100%;">&nbsp;Add Elective Group</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="portletMain" style="width:100%">
													<table style="width:100%;" cellspacing="0" cellpadding="0">
														<tr>
															<td class="portletDark" style="width:100%;">
																<table cellspacing="1" cellpadding="0" style="width:100%;">
																	<tr>
																		<td class="portletLight" style="width:100%;vertical-align:text-top;">
																			<table cellpadding="0" cellspacing="0" style="width:100%;">
																				<tr>
																					<td style="width:100%;padding-bottom:15px;">
																						<table cellpadding="0" cellspacing="0" style="width:100%;">
																							<asp:panel runat="server" id="panError2">
																								<tr>
																									<td class="portletLight" style="color:red;padding-left:10px;padding-top:15px;">
																										<asp:label runat="server" id="lblErrorMsg2"></asp:label>
																									</td>
																								</tr>
																							</asp:panel>
																							<!-- ELECTIVE GROUP Title -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px;">
																									<b>Elective Group Title:</b>&nbsp;&nbsp;
																								</td>
																							</tr>
																							<tr>
																								<td class="portletLight" style="padding-top:5px;padding-left:10px;padding-right:72px;">
																									<asp:textbox id="txtElectGroupTitle" runat="server" cssclass="small" style="width:260px;" maxlength="75"></asp:textbox>
																								</td>
																							</tr>
																							<!-- ELECTIVE FOOTNOTE -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px;text-align:left;width:100%;">
																									<b>Select Footnote (optional):</b>&nbsp;&nbsp;<asp:dropdownlist id="cboElectGroupFootnote" runat="server" cssclass="small" autopostback="true" OnFocus="document.frmElective.hidFocus.value=this.id;"></asp:dropdownlist>
																								</td>
																							</tr>
																							<!-- SELECTED ELECTIVE FOOTNOTE DISPLAY -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px;width:100%;text-align:left;">
																									<b>Footnote:</b>
																								</td>
																							</tr>
																							<tr>
																								<td class="portletLight" style="padding-top:15px;padding-left:10px;padding-right:10px;width:100%;text-align:left;">
																									<asp:label id="lblElectGroupFootnote" runat="server" cssclass="small">None Selected</asp:label>
																								</td>
																							</tr>
																							<!-- BUTTONS -->
																							<tr>
																								<td class="portletLight" style="padding-top:20px;width:100%;text-align:center;">
																									<input type="button" id="cmdCancel" value="Cancel" class="small" style="background:#ccccaa;border:1px solid #000000;width:85px;" onclick="self.location.href='addelective.aspx?pvid=' + document.frmElective.hidProgramVersionID.value + '&pdid=' + document.frmElective.hidProgramDegreeID.value + '&did=' + document.frmElective.hidDegreeID.value + '&todo=step2&ltr=' + document.frmElective.hidLtr.value + '&oa=' + document.frmElective.hidOfferedAt.value + '&pt=' + document.frmElective.hidProgramTitle.value + '&oid=' + document.frmElective.hidOptionID.value + '&pd=' + document.frmElective.hidProgramDisplay.value;" />
																									&nbsp;<asp:button runat="server" id="cmdAddElectGroup" text="Add Elective Group" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:135px;" onclientclick="return validate();" onclick="cmdAddElectGroup_Click"></asp:button>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</asp:panel>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
		<script type="text/javascript">
		<!--
			var hidFocus = document.getElementById("hidFocus");
				
			if(hidFocus.value != ""){
				document.getElementById(hidFocus.value).focus();
			}else{
				document.frmElective.cboElectGroup.focus();
			}
		//-->
		</script>
	</body>
</html>