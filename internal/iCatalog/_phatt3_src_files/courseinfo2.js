<!--
	function getHTTPObject(){
		var xmlhttp; 
		try {  
			xmlhttp = new ActiveXObject('Msxml2.XMLHTTP');   
		}catch (e){
			try {   
				xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');    
			}catch (e2){
				try {  
					xmlhttp = new XMLHttpRequest();     
				}catch (e3){  
					xmlhttp = false;   
				}
			}
		}
		return xmlhttp;
    }
            
    function getSubjectDescription(){
        document.getElementById("hidTodo").value = "subjectDescription";
        var subject = document.getElementById("cboCourseSubject").value;
        var http = getHTTPObject();
        http.open("get", "courseinfo.aspx?todo=subjectDescription&subject=" + escape(subject), true);
        http.onreadystatechange = function(){
			if (http.readyState == 4) {
				document.getElementById("divSubjectDescription").innerHTML = http.responseText;
            }
        };
        
        http.send(null);
    }
            
    function getDescriptionCourses(){
        document.getElementById("hidTodo").value = "DescriptionCourses";
        var subject = document.getElementById("cboDescriptionSubject").value;
        if(subject != ""){
			var http = getHTTPObject();
            http.open("get", "courseinfo.aspx?todo=DescriptionCourses&subject=" + escape(subject), true);
            http.onreadystatechange = function(){
				if (http.readyState == 4) {
					document.frmCourse.cboDescriptionCourse.length = 1;
					document.frmCourse.cboDescriptionCourse.options[0] = new Option("","");
					var courses = http.responseText.split("|");
					for(var i = 1; i < courses.length; i++){
						var courseInfo = courses[i].split(",");
						document.frmCourse.cboDescriptionCourse.options[i] = new Option(courseInfo[0],courseInfo[1]);
					}
				}
            };
            http.send(null);
        }else{
            document.frmCourse.cboDescriptionCourse.length = 1;
            document.frmCourse.cboDescriptionCourse.options[0] = new Option("Select a Long Description Subject","");
        }
    }
            
    function getDescriptionByCourseID(){
        document.getElementById("hidTodo").value = "getDescriptionByCourseID";
        var courseID = document.getElementById("cboDescriptionCourse").value;
        if(courseID != ""){
			var http = getHTTPObject();
            http.open("get", "courseinfo.aspx?todo=getDescriptionByCourseID&dcid=" + escape(courseID), true);
            http.onreadystatechange = function(){
				if (http.readyState == 4) {
					//document.getElementById("txtCourseLongDescription").value = http.responseText;
                    CKEDITOR.instances.txtCourseLongDescription.setData(http.responseText);
				}
            };
            http.send(null);
        }
    }
            
    function generateCourseID(){
        var courseSubject = document.getElementById("cboCourseSubject").value;
        var catalogNbr = trim(document.getElementById("txtCatalogNbr").value);
        var courseSuffix = document.getElementById("cboCourseSuffix").value;
        if(courseSubject != ""){
            var courseOffering = courseSubject;
            while(courseOffering.length < 5){
			    courseOffering += " "; 
			}
			if(catalogNbr != ""){
			    //rExp = / /gi;
				//courseOffering = courseOffering.replace(rExp,"&nbsp;") + catalogNbr; 
				courseOffering += catalogNbr; 
				if(courseSuffix != ""){
					courseOffering += courseSuffix;
				}
			}
			document.getElementById("divCourseOffering").innerHTML = courseOffering;
        }else{
            document.getElementById("divCourseOffering").innerHTML = "auto generated";
        }
    }
            
    function selectDescriptionSubject(){
        if(document.getElementById("cboDescriptionCourse").value == "" && document.getElementById("cboCourseSubject").value != ""){
            document.getElementById("cboDescriptionSubject").value = document.getElementById("cboCourseSubject").value;
            if(document.getElementById("cboDescriptionSubject").value != ""){
                getDescriptionCourses();
            }
        }
    }
            
    /* 
        executes onblur of the UnitsMinimum form element
        If the fixed credits radio button is selected,
        the minimum amount of credits is set to the same value as the maximum amount of credits. 
    */
    function generateFixed(){
        if(document.frmCourse.optVariableUnits[0].checked){
            document.getElementById("txtUnitsMaximum").value = document.getElementById("txtUnitsMinimum").value;
        }
    }
            
    /*  
        executes onfocus of the VariableUnits radio buttons
        If the fixed radio button(0) is selected, the min credit value is set to the max credit value,
        and the min credit text box is disabled.
        If the variable radio button(1) is selected, then the min credit text box is enabled.
    */
    function disable(num){
        if(num == 0){
            document.getElementById("txtUnitsMaximum").value = document.getElementById("txtUnitsMinimum").value;
            document.getElementById("txtUnitsMaximum").disabled = true;
        }else if(num == 1){
            document.getElementById("txtUnitsMaximum").disabled = false;
        }
    }
    
    function validate(){
        var isValid = false;//, chkSCC = document.getElementById("chkOfferedAt_0"), chkSFCC = document.getElementById("chkOfferedAt_1"), chkIEL = document.getElementById("chkOfferedAt_2");
        var unitsMinimum = trim(document.frmCourse.txtUnitsMinimum.value);
        var beginSTRM = document.frmCourse.cboCourseBeginSTRM.value, endSTRM = document.frmCourse.cboCourseEndSTRM.value;

        if(document.frmCourse.optVariableUnits.value == "N"){
            var unitsMaximum = unitsMinimum;
        }else{
            var unitsMaximum = trim(document.frmCourse.txtUnitsMaximum.value);
        }

        if(beginSTRM > endSTRM){
            alert("The begin effective year quarter must be before the end effective year quarter.");
        }else if(trim(document.frmCourse.cboCourseSubject.value) == ""){
            alert("Please select a Subject.");
        }else if(trim(document.frmCourse.txtCatalogNbr.value) == ""){
            alert("Please enter a Course Number.");
            document.frmCourse.txtCatalogNbr.select();
        }else if(trim(document.frmCourse.txtCourseShortTitle.value) == ""){
            alert("Please enter a Course Short Title.");
            document.frmCourse.txtCourseShortTitle.select();
        }else if(trim(document.frmCourse.txtCourseLongTitle.value) == ""){
            alert("Please enter a Course Long Title.");
            document.frmCourse.txtCourseLongTitle.select();
        }else if(unitsMinimum == "" || isNaN(unitsMinimum)){
            alert("Please enter the Min Credits.");
            document.frmCourse.txtUnitsMinimum.select();
        }else if(unitsMaximum == "" || isNaN(unitsMaximum)){
            alert("Please enter the Max Credits.");
            document.frmCourse.txtUnitsMaximum.select();
        }else if(parseFloat(unitsMinimum) > parseFloat(unitsMaximum)){
            alert("The Min Credit value cannot exceed the Max Credit value.");
            document.frmCourse.txtUnitsMinimum.select();
        }else if(unitsMaximum > 99.9){
            alert("The Max Credit value cannot exceed 99.9");
            document.frmCourse.txtUnitsMaximum.select();
//        }else if(chkSCC.checked == false && chkSFCC.checked == false && chkIEL.checked == false){
//            alert("Please check where the course is offered.");
        }else if(CKEDITOR.instances.txtCourseLongDescription.getData() == ""){
            document.getElementById("cboDescriptionCourse").value = "";
            alert("Please enter a Course Description.");
        }else{
			document.frmCourse.txtCourseShortTitle.value = document.frmCourse.txtCourseShortTitle.value.toUpperCase();
			document.frmCourse.txtCatalogNbr.value = trim(document.frmCourse.txtCatalogNbr.value);
            isValid = true;
        }
        return isValid;
    }
//-->
