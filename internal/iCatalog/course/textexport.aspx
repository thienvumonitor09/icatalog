<%@ Page language="c#" Inherits="ICatalog.course.textexport" CodeFile="textexport.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Export to Text</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <script type="text/javascript">
        <!--
            function addCollege(ID, collegeID) {
                //add/remove college to/from a comma delimited string
                var CollegeList = document.getElementById("hidCollegeList");
                if (document.getElementById(ID).checked) {
                    if (CollegeList.value.length == 0) {
                        CollegeList.value += collegeID;
                    } else {
                        CollegeList.value += "," + collegeID;
                    }
                } else {
                    if (CollegeList.value.indexOf("," + collegeID, 0) > 0) {
                        CollegeList.value = CollegeList.value.replace("," + collegeID, "");
                    } else {
                        CollegeList.value = CollegeList.value.replace(collegeID, "");
                    }
                }

                if (CollegeList.value.indexOf(",") == 0) {
                    CollegeList.value = CollegeList.value.substr(1, CollegeList.value.length);
                }
            }

            function validate() {
                if (document.getElementById("hidCollegeList").value == "") {
                    alert("Please check where the courses are offered.");
                    return false;
                } else {
                    return true;
                }
            }
        //-->
        </script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmCourse" runat="server">
                    <input type="hidden" id="hidCollegeList" runat="server" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Export to Text</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                        <!-- TERM -->
                                                                        <tr>
                                                                            <td class="portletSecondary" style="width:42%;text-align:right;">
                                                                                <b>Term:&nbsp;</b>
                                                                            </td>
                                                                            <td class="portletLight" style="width:58%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                <asp:dropdownlist runat="server" id="cboTerm" cssclass="small"></asp:dropdownlist>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- OFFERED AT -->
                                                                        <tr>
                                                                            <td class="portletSecondary" style="width:42%;text-align:right;vertical-align:top;padding-top:9px;">
                                                                                <b>Offered at:&nbsp;</b>
                                                                            </td>
                                                                            <td class="portletLight" style="width:58%;vertical-align:top;padding-left:0px;padding-bottom:4px;">
                                                                                <asp:Panel ID="panCollege" runat="server"></asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- BUTTON -->
                                                                        <tr>
														                    <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                    <asp:button id="cmdSubmit" runat="server" text="Export to Text" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclientclick="return validate();" onclick="cmdSubmit_Click"></asp:button>
														                    </td>
													                    </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <div class="clearer"></div>
        </asp:panel>
    </body>
</html>



