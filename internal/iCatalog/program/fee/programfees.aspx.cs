using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.fee
{
	/// <summary>
	/// Summary description for programfees.
	/// </summary>
	public partial class programfees : System.Web.UI.Page
	{
		programData csProgram = new programData();

		#region GLOBAL CONTROLS
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidProgram;
		#endregion

		#region ADD PANEL CONTROLS
		#endregion

		#region DISPLAY PANEL CONTROLS
		#endregion

		#region CONFIRM PANEL CONTROLS
		#endregion

		protected void Page_Load(object sender, System.EventArgs e) {
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panAdd.Visible = false;
			panConfirm.Visible = false;
			panError.Visible = false;
			panDisplay.Visible = false;

			if(!IsPostBack){
				DataSet dsTerms = csProgram.GetTermsForPrograms();
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
					cboTerm.Items.Add(new ListItem(dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString(), dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString()));
				}
				cboTerm.SelectedValue = Request.QueryString["strm"];

				DataSet dsCollege = csProgram.GetCollegeList();
				for(Int32 intDSRow = 0; intDSRow < dsCollege.Tables[0].Rows.Count; intDSRow++){
					cboOfferedAt.Items.Add(new ListItem(dsCollege.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString(), dsCollege.Tables[0].Rows[intDSRow]["CollegeID"].ToString()));
				}
				cboOfferedAt.SelectedValue = Request.QueryString["colid"];
			}

			if(hidTodo.Value == "delete"){
				
				csProgram.DeleteProgramFees(Convert.ToInt32(Request.Form["hidProgramVersionID"]));
				txtBookMinimum.Value = "";
				txtBookMaximum.Value = "";
				txtSuppliesMinimum.Value = "";
				txtSuppliesMaximum.Value = "";
				txtMiscMinimum.Value = "";
				txtMiscMaximum.Value = "";
				txtNote.Text = "";
				panAdd.Visible = true;
			
			}else if(hidTodo.Value == "edit"){

				DataSet dsProgramTitles = csProgram.GetProgramTitles();
				Int32 intRowCount = dsProgramTitles.Tables[0].Rows.Count;

				if(intRowCount > 0){	
					txtBookMinimum.Value = "";
					txtBookMaximum.Value = "";
					txtSuppliesMinimum.Value = "";
					txtSuppliesMaximum.Value = "";
					txtMiscMinimum.Value = "";
					txtMiscMaximum.Value = "";
					txtNote.Text = "";

					lblOfferedAt.Text = "";

					Int32 programVersionID = Convert.ToInt32(Request.Form["hidProgramVersionID"]);

					//get college(s) offering program for display
					DataSet dsProgramDescription = csProgram.GetProgramDescription(programVersionID);
					Int32 intProgramCount = dsProgramDescription.Tables[0].Rows.Count;

					if(intProgramCount > 0){
						lblProgram.Text = dsProgramDescription.Tables[0].Rows[0]["ProgramTitle"].ToString();
						lblBeginTerm.Text = dsProgramDescription.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString();
						lblEndTerm.Text = dsProgramDescription.Tables[0].Rows[0]["EndTerm_DESCR"].ToString();

						for(Int32 intDSRow = 0; intDSRow < intProgramCount; intDSRow++){
							lblOfferedAt.Text += ", " + dsProgramDescription.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
						}

						lblOfferedAt.Text = lblOfferedAt.Text.Substring(2);
					}else{
						lblOfferedAt.Text = "Not currently offered";
					}

					//get the selected program fees
					DataSet dsProgramFees = csProgram.GetProgramFees(programVersionID);
					Int32 intProgramFeeCount = dsProgramFees.Tables[0].Rows.Count;

					if(intProgramFeeCount > 0){
						String strBookMinimum = dsProgramFees.Tables[0].Rows[0]["BookMinimum"].ToString();
						String strBookMaximum = dsProgramFees.Tables[0].Rows[0]["BookMaximum"].ToString();
						String strSuppliesMinimum = dsProgramFees.Tables[0].Rows[0]["SuppliesMinimum"].ToString();
						String strSuppliesMaximum = dsProgramFees.Tables[0].Rows[0]["SuppliesMaximum"].ToString();
						String strMiscMinimum = dsProgramFees.Tables[0].Rows[0]["MiscMinimum"].ToString();
						String strMiscMaximum = dsProgramFees.Tables[0].Rows[0]["MiscMaximum"].ToString();
								
						if(strBookMinimum != ""){
							txtBookMinimum.Value = Convert.ToDouble(strBookMinimum).ToString();
						}
						if(strBookMaximum != ""){
							txtBookMaximum.Value = Convert.ToDouble(strBookMaximum).ToString();
						}
						if(strSuppliesMinimum != ""){
							txtSuppliesMinimum.Value = Convert.ToDouble(strSuppliesMinimum).ToString();
						}
						if(strSuppliesMaximum != ""){
							txtSuppliesMaximum.Value = Convert.ToDouble(strSuppliesMaximum).ToString();
						}
						if(strMiscMinimum != ""){
							txtMiscMinimum.Value = Convert.ToDouble(strMiscMinimum).ToString();
						}
						if(strMiscMaximum != ""){
							txtMiscMaximum.Value = Convert.ToDouble(strMiscMaximum).ToString();
						}

						txtNote.Text = dsProgramFees.Tables[0].Rows[0]["Note"].ToString();
					}
				}

				panAdd.Visible = true;	

			}else{
				GenerateProgramList();
				panDisplay.Visible = true;
			}
		}

		public void GenerateProgramList(){ //generate and show the program list alphabetically
			TableRow tr = new TableRow();
			TableCell td = new TableCell();

			//create the alphabet selection list
			for(Int16 intAlphaCtr = 65; intAlphaCtr <= 90; intAlphaCtr++){
				td = new TableCell();
				td.Attributes["style"] = "width:23px;text-align:center;";
				td.Attributes["onclick"] = "document.frmProgramFee.hidTodo.value='search';document.frmProgramFee.hidLtr.value='" + Convert.ToChar(intAlphaCtr) + "';document.frmProgramFee.submit();return false;";
				td.CssClass = "portletMain";
				td.Controls.Add(new LiteralControl("<a class=\"ltr\" href=\"#\" onclick=\"document.frmProgramFee.hidTodo.value='search';document.frmProgramFee.hidLtr.value='" + Convert.ToChar(intAlphaCtr) + "';document.frmProgramFee.submit();return false;\">" + Convert.ToChar(intAlphaCtr) + "</a>"));
				tr.Cells.Add(td);
			}

			tblAlpha.Rows.Add(tr);

			//select letter 'A' as the default
			String strLtr = hidLtr.Value;
			if(strLtr == ""){
				strLtr = "A";
			}

			//get a dataset of programs starting with the selected letter
			DataSet dsPrograms = csProgram.GetCareerPlanningGuidePrograms(Convert.ToChar(strLtr), cboTerm.SelectedValue, cboOfferedAt.SelectedValue);
			Int32 intRowCtr = dsPrograms.Tables[0].Rows.Count;
			
			if(intRowCtr > 0){ //if program titles starting with the selected letter exist

				//display the program header information
				tr = new TableRow();

                td = new TableCell();
                td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:155px;";
                td.CssClass = "portletSecondary";
                td.Controls.Add(new LiteralControl("Award Type"));

                tr.Cells.Add(td);

				td = new TableCell();
                td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:286px;";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("Program Title"));

				tr.Cells.Add(td);

				td = new TableCell();
                td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:72px;";
				td.CssClass = "portletSecondary";
				td.Attributes["nowrap"] = "nowrap";
				td.Controls.Add(new LiteralControl("Offered by"));

				tr.Cells.Add(td);

				td = new TableCell();
                td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:85px;white-space:nowrap";
				td.CssClass = "portletSecondary";
				td.Attributes["nowrap"] = "nowrap";
				td.Controls.Add(new LiteralControl("Begin Term"));

				tr.Cells.Add(td);

				td = new TableCell();
                td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:85px;white-space:nowrap";
				td.CssClass = "portletSecondary";
				td.Attributes["nowrap"] = "nowrap";
				td.Controls.Add(new LiteralControl("End Term"));

				tr.Cells.Add(td);

				td = new TableCell();
                td.Attributes["style"] = "width:34px;";
				td.Attributes["nowrap"] = "nowrap";
				td.CssClass = "portletSecondary";

				tr.Cells.Add(td);
				tblDisplay.Rows.Add(tr);

                String oldAreaOfStudyTitle = "";

				//loop through and display the programs
				for(Int32 intDSRow = 0; intDSRow < intRowCtr; intDSRow++){

					String programTitle = dsPrograms.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString();
					String strProgramVersionID = dsPrograms.Tables[0].Rows[intDSRow]["ProgramVersionID"].ToString();
                    String degreeShortTitle = dsPrograms.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
                    String areaOfStudyTitle = dsPrograms.Tables[0].Rows[intDSRow]["Title"].ToString();

                    if (areaOfStudyTitle != oldAreaOfStudyTitle) {
                        tr = new TableRow();
                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "font-weight:bold;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;width:100%;";
                        td.ColumnSpan = 6;
                        td.Controls.Add(new LiteralControl(areaOfStudyTitle));
                        tr.Cells.Add(td);
                        tblDisplay.Rows.Add(tr);
                    }

					tr = new TableRow();

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:155px;";
                    td.Controls.Add(new LiteralControl(degreeShortTitle));

                    tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "width:100%;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;width:286px;";
					td.Controls.Add(new LiteralControl(programTitle));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:72px;";
					td.Controls.Add(new LiteralControl(dsPrograms.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString()));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:85px;";
					td.Controls.Add(new LiteralControl(dsPrograms.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString()));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:85px;";
					td.Controls.Add(new LiteralControl(dsPrograms.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString()));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:center;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:34px;";
					td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.frmProgramFee.hidTodo.value='edit';document.frmProgramFee.hidProgramVersionID.value='" + strProgramVersionID + "';document.frmProgramFee.submit();\">Edit</a>"));
					tr.Cells.Add(td);
					tblDisplay.Rows.Add(tr);

                    oldAreaOfStudyTitle = areaOfStudyTitle;
				}

			}else{ //else if program titles starting with the selected letter do not exist

				//display "selected letter returned 0 results"
				tr = new TableRow();
				td = new TableCell();
				td.CssClass = "portletLight";
				td.Attributes["style"] = "padding-top:4px;padding-bottom:4px;";
				td.Controls.Add(new LiteralControl("&nbsp;" + strLtr + " returned 0 results."));
				tr.Cells.Add(td);
				tblDisplay.Rows.Add(tr);

			}
		}

		protected void cmdSave_Click(object sender, System.EventArgs e){
			Int32 intError = 0;
			String strError = "", strBookMinimum = Request.Form["txtBookMinimum"], strBookMaximum = Request.Form["txtBookMaximum"], strSuppliesMinimum = Request.Form["txtSuppliesMinimum"], strSuppliesMaximum = Request.Form["txtSuppliesMaximum"], strMiscMinimum = Request.Form["txtMiscMinimum"], strMiscMaximum = Request.Form["txtMiscMaximum"], note = Request.Form["txtNote"];
			Double bookMinimum = 0, bookMaximum = 0, suppliesMinimum = 0, suppliesMaximum = 0, miscMinimum = 0, miscMaximum = 0;

			if(strBookMinimum != ""){
				try{
					bookMinimum = Convert.ToDouble(strBookMinimum);
				}catch{
					strError += "The minimum book fee is not in the 0.00 format.<br />";
					intError++;
				}
			}
			if(strBookMaximum != ""){
				try{
					bookMaximum = Convert.ToDouble(strBookMaximum);
				}catch{
					strError += "The maximum book fee is not in the 0.00 format.<br />";
					intError++;
				}
			}
			if(strSuppliesMinimum != ""){
				try{
					suppliesMinimum = Convert.ToDouble(strSuppliesMinimum);
				}catch{
					strError += "The minimum supplies and equipment fee is not in the 0.00 format.<br />";
					intError++;
				}
			}
			if(strSuppliesMaximum != ""){
				try{
					suppliesMaximum = Convert.ToDouble(strSuppliesMaximum);
				}catch{
					strError += "The maximum supplies and equipment fee is not in the 0.00 format.<br />";
					intError++;
				}
			}
			if(strMiscMinimum != ""){
				try{
					miscMinimum = Convert.ToDouble(strMiscMinimum);
				}catch{
					strError += "The minimum miscellaneous fee is not in the 0.00 format.<br />";
					intError++;
				}
			}
			if(strMiscMaximum != ""){
				try{
					miscMaximum = Convert.ToDouble(strMiscMaximum);
				}catch{
					strError += "The maximum miscellaneous fee is not in the 0.00 format.<br />";
					intError++;
				}
			}

			if(intError > 0){
				lblErrorMsg.Text = "<b>Error</b><br />" + strError;
				panError.Visible = true;
			}else{
				Int32 programVersionID = Convert.ToInt32(Request.Form["hidProgramVersionID"]);
				csProgram.EditProgramFees(programVersionID, strBookMinimum, strBookMaximum, strSuppliesMinimum, strSuppliesMaximum, strMiscMinimum, strMiscMaximum, note);
				lblErrorMsg.Text = "";
				panError.Visible = false;
				hidProgramVersionID.Value = programVersionID.ToString();
				panAdd.Visible = false;
				
				if(bookMinimum != 0){
					if(bookMinimum < bookMaximum){
						lblBookFee.Text = "$" + bookMinimum + "-" + bookMaximum;
					}else{
						lblBookFee.Text = "$" + bookMinimum;
					}
				}else{
					lblBookFee.Text = "None Entered";
				}

				if(suppliesMinimum != 0){
					if(suppliesMinimum < suppliesMaximum){
						lblSupAndEquipFee.Text = "$" + suppliesMinimum + "-" + suppliesMaximum;
					}else{
						lblSupAndEquipFee.Text = "$" + suppliesMinimum;
					}
				}else{
					lblSupAndEquipFee.Text = "None Entered";
				}

				if(miscMinimum != 0){
					if(miscMinimum < miscMaximum){
						lblMiscFee.Text = "$" + miscMinimum + "-" + miscMaximum;
					}else{
						lblMiscFee.Text = "$" + miscMinimum;
					}
				}else{
					lblMiscFee.Text = "None Entered";
				}

				if(note != ""){
					lblNote.Text = note;
				}else{
					lblNote.Text = "None Entered";
				}

				lblProgramTitle.Text = lblProgram.Text;
				lblOfferedAt2.Text = lblOfferedAt.Text;
				lblBeginTerm2.Text = lblBeginTerm.Text;
				lblEndTerm2.Text = lblEndTerm.Text;
				
				panConfirm.Visible = true;
			}				
		}

		protected void cmdOK_Click(object sender, System.EventArgs e){
			panConfirm.Visible = false;
			panAdd.Visible = false;
			GenerateProgramList();
			panDisplay.Visible = true;
		}

		protected void cmdCancel_Click(object sender, System.EventArgs e){
			panAdd.Visible = false;
			hidTodo.Value = "";
			GenerateProgramList();
			panDisplay.Visible = true;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {    
		}
		#endregion
	}
}

