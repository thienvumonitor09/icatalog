USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetActiveCoursesLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 3/8/17
-- Description:	Returns a list of active courses offered for a specific quarter/term (optional) in a specific subject/department (optional)	
--		 INPUT: STRM (optional) - Term Code
--				SUBJECT (optional) - Course Subject: ENGL
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetActiveCoursesLegacy] 
	@STRM varchar(4) = NULL,
	@SUBJECT varchar(5) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT CourseID, CATALOG_NBR, CourseOffering, CourseBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, BeginTerm.TERM_BEGIN_DT, CourseEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, EndTerm.TERM_END_DT, SUBJECT, Course.DESCR, PublishedCourse
	FROM Course 
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = CourseBeginSTRM
	AND BeginTerm.ACAD_CAREER = 'UGRD' /* filter out continuing ed to remove duplicate terms */
	AND BeginTerm.INSTITUTION = 'WA171' /* pull terms for SCC only to avoid duplicates */
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = CourseEndSTRM
	AND EndTerm.ACAD_CAREER = 'UGRD' /* filter out continuing ed to remove duplicate terms */
	AND EndTerm.INSTITUTION = 'WA171' /* pull terms for SCC only to avoid duplicates */
	WHERE (@SUBJECT IS NULL OR (SUBJECT = @SUBJECT OR SUBJECT = @SUBJECT + '&'))
	AND (@STRM IS NULL OR (CourseBeginSTRM <= @STRM
	AND (CourseEndSTRM = 'Z999' OR CourseEndSTRM >= @STRM)))  
	AND PublishedCourse = 1 
	ORDER BY CATALOG_NBR, CourseBeginSTRM DESC, PublishedCourse DESC;
END

GO
