USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionPrerequisites]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 06/14/2019
-- Description:	Returns program degree option prerequisites
--		 INPUT: OptionID - Program degree option primary key
--				BeginSTRM - Program Begin Term Code
--				EndSTRM - Program End Term Code
--				SelectedSTRM - The Term Code Selected
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionPrerequisites]
	@OptionID int,
	@BeginSTRM varchar(4),
	@EndSTRM varchar(4),
	@SelectedSTRM varchar(4)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT SUBJECT, CATALOG_NBR, /*CourseSuffix,*/ OptionPrerequisite.CourseOffering, COURSE_TITLE_LONG, /*CourseBeginSTRM, CourseEndSTRM,*/ FootnoteNumber, OptionPrerequisiteID, OptionPrerequisite.OptionFootnoteID 
	FROM OptionPrerequisite 
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	ON REPLACE(CourseOffering.SUBJECT + CourseOffering.CATALOG_NBR,' ','') = REPLACE(OptionPrerequisite.CourseOffering,' ','')
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID AND CourseCatalog.EFFDT = CourseOffering.EFFDT 
	LEFT OUTER JOIN OptionFootnote 
	ON OptionFootnote.OptionFootnoteID = OptionPrerequisite.OptionFootnoteID 
	WHERE OptionPrerequisite.OptionID = @OptionID
		AND CourseOffering.EFFDT = 
			(
				SELECT MAX(CourseOffering2.EFFDT)
				FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
				WHERE CourseOffering2.SUBJECT = CourseOffering.SUBJECT AND CourseOffering2.CATALOG_NBR = CourseOffering.CATALOG_NBR
					--AND INSTITUTION = 'WA171'
				AND CourseOffering2.EFFDT <= (SELECT * FROM udf_GetCurrentSTRM())
			)
	ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
END
GO
