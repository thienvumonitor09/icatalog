USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramTerms]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 3/3/2017
-- Description:	Returns terms after a specified date
--       Input: StartDate - Only terms that end after this date will be returned
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramTerms] 
	@StartDate Date
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT STRM, DESCR
	FROM Program
	INNER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable
	ON (STRM >= ProgramBeginSTRM) AND (STRM <= ProgramEndSTRM AND ProgramEndSTRM <> 'Z999')
	OR (STRM >= ProgramBeginSTRM) AND (STRM <= ProgramBeginSTRM)
	WHERE TERM_END_DT > @StartDate 
	ORDER BY STRM DESC;
END

GO
