USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionsByTitleLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 6/1/17
-- Description:	Returns an alphabetic program option list by term, letter the option title starts with, location, degree, course subject, and course catalog number. 
--		 INPUT: STRM - Term code
--				LocationID (optional) - Location primary key 
--				DegreeID (optional) - Degree primary key 
--				AlphaCharacter (optional) - Letter of the alphabet the program option titles start with
--				SUBJECT (optional) - Course subject area 
--				CATALOG_NBR (optional) - Course catalog number 
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionsByTitleLegacy] 
	@STRM varchar(4),
	@LocationID int = NULL,
	@DegreeID int = NULL,
	@AlphaCharacter char = NULL,
	@SUBJECT varchar(5) = NULL,
	@CATALOG_NBR varchar(3) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SQL nvarchar(max),
			@Parameters nvarchar(4000)

    SET @SQL = 'SELECT CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID
				FROM ProgramDegree
				INNER JOIN Program 
				ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
				INNER JOIN CareerPlanningGuide 
				ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
				INNER JOIN College 
				ON College.CollegeID = Program.CollegeID 
				INNER JOIN ProgramDegreeOption 
				ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
				INNER JOIN Degree 
				ON Degree.DegreeID = ProgramDegree.DegreeID 
				LEFT OUTER JOIN OptionPrerequisite 
				ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
				LEFT OUTER JOIN OptionElectiveGroup 
				ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
				LEFT OUTER JOIN ElectiveGroupCourse 
				ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
				LEFT OUTER JOIN OptionCourse 
				ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
				LEFT OUTER JOIN Course
				ON Course.CourseOffering = OptionPrerequisite.CourseOffering
				OR Course.CourseOffering = ElectiveGroupCourse.CourseOffering 
				OR Course.CourseOffering = OptionCourse.CourseOffering 
				LEFT OUTER JOIN OptionLocation 
				ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
				LEFT OUTER JOIN Location 
				ON Location.LocationID = OptionLocation.LocationID 
				WHERE ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = ''Z999'' OR ProgramEndSTRM >= @STRM) 
				AND PublishedProgram = 1 '
				
	IF @LocationID IS NOT NULL
		SET @SQL += 'AND OptionLocation.LocationID = @LocationID '
		
	IF @DegreeID IS NOT NULL
		SET @SQL += 'AND ProgramDegree.DegreeID = @DegreeID '
		
	IF @AlphaCharacter IS NOT NULL
		SET @SQL += 'AND (ProgramTitle LIKE @AlphaCharacter + ''%'' OR OptionTitle LIKE @AlphaCharacter + ''%'') '
		
	IF @SUBJECT IS NOT NULL
		SET @SQL += 'AND ((Course.SUBJECT = @SUBJECT OR Course.SUBJECT = @SUBJECT + ''&'') 
					 AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
					 OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM))) '
	
	IF @CATALOG_NBR IS NOT NULL	
		SET @SQL += 'AND (Course.CATALOG_NBR = @CATALOG_NBR 
					 AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
					 OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM))) '
					 
	SET @SQL += 'GROUP BY Program.ProgramVersionID, CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID 
				 ORDER BY ProgramTitle, OptionTitle, DegreeShortTitle, CollegeShortTitle;'
				 
	SET @Parameters = '@STRM varchar(4),
					   @LocationID int,
					   @DegreeID int,
					   @AlphaCharacter char,
					   @SUBJECT varchar(5),
					   @CATALOG_NBR varchar(3)'
						 
	EXEC sp_executesql @SQL, @Parameters, @STRM, @LocationID, @DegreeID, @AlphaCharacter, @SUBJECT, @CATALOG_NBR
	/*
	SELECT CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID
	FROM ProgramDegree
	INNER JOIN Program 
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
	INNER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID 
	INNER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	INNER JOIN Degree 
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Course
	ON Course.CourseOffering = OptionPrerequisite.CourseOffering
	OR Course.CourseOffering = ElectiveGroupCourse.CourseOffering 
	OR Course.CourseOffering = OptionCourse.CourseOffering 
	LEFT OUTER JOIN OptionLocation 
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 
	WHERE ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM) 
		AND PublishedProgram = 1 
		AND 
			(
				@LocationID IS NULL OR OptionLocation.LocationID = @LocationID
			)
		AND
			(
				@DegreeID IS NULL OR ProgramDegree.DegreeID = @DegreeID
			)
		AND
			(
				@AlphaCharacter IS NULL OR (ProgramTitle LIKE @AlphaCharacter + '%' OR OptionTitle LIKE @AlphaCharacter + '%')
			)
		AND
	 
			(
				@SUBJECT IS NULL OR ((Course.SUBJECT = @SUBJECT OR Course.SUBJECT = @SUBJECT + '&') 
						 AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						 OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
			)
		AND
			(
				@CATALOG_NBR IS NULL OR (Course.CATALOG_NBR = @CATALOG_NBR 
						 AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						 OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM))) 
			)
	GROUP BY Program.ProgramVersionID, CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID 
	ORDER BY ProgramTitle, OptionTitle, DegreeShortTitle, CollegeShortTitle;
	*/
END

GO
