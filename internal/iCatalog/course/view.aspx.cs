using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.course
{
	/// <summary>
	/// Summary description for view.
	/// </summary>
	public partial class view : System.Web.UI.Page
	{

		courseData csCourse = new courseData();
        permissionData csPermissions = new permissionData();
		termData csTerm = new termData();

		protected System.Web.UI.WebControls.Button cmdSubmit;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panView.Visible = false;
			panDisplay.Visible = true;

			if(!IsPostBack){				
				DataSet dsTerms = csTerm.GetTerms();
				cboTerm.Items.Add(new ListItem("ALL", ""));
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
                    cboTerm.Items.Add(new ListItem(dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString(), dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString()));
				}
                
                try
                {
                    cboTerm.SelectedValue = csTerm.GetCurrentTerm();
                }
                catch
                {
                    //do nothing
                }
			}

            cboSubject.Items.Clear();
            DataSet dsSubjects = csCourse.GetEffectiveSubjects(cboTerm.SelectedValue);
            for (Int32 intDSRow = 0; intDSRow < dsSubjects.Tables[0].Rows.Count; intDSRow++)
            {
                String subject = dsSubjects.Tables[0].Rows[intDSRow]["SUBJECT"].ToString().Replace("&","");
                if (!cboSubject.Items.Contains(new ListItem(subject)))
                {
                    cboSubject.Items.Add(subject);
                }

                try
                {
                    cboSubject.SelectedValue = Request.Form["cboSubject"];
                }
                catch
                {
                    //do nothing
                }
            }

			if(Request.QueryString["todo"] == "view")
            {
                string CRSE_ID = Request.QueryString["CRSE_ID"];
                string effdtStr = Request.QueryString["EFFDT"];
                DataSet dsCourse = csCourse.GetCourse(CRSE_ID, effdtStr);
                DataTable dtCourse = dsCourse.Tables[0];
                
                if (dtCourse != null && dtCourse.Rows.Count > 0){
                    DataRow drCourse = dtCourse.Rows[0];

                    String strOfferedAt = "", descriptionCourseOffering = "", variableUnits = "";//, descriptionCourseID = drCourse["DescriptionCourseID"].ToString();

                    //DataSet dsCourseCollege = csCourse.GetCourseCollege(courseID); 
                    /*
                    for (Int32 i = 0; i < dsCourseCollege.Tables[0].Rows.Count; i++)
                    {
                        strOfferedAt += ", " + dsCourseCollege.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
                    }
                    if (strOfferedAt != "")
                    {
                        strOfferedAt = strOfferedAt.Substring(2);
                    }
                    */
                    if(Convert.ToDouble(drCourse["UNITS_MINIMUM"]).CompareTo(Convert.ToDouble(drCourse["UNITS_MAXIMUM"])) == 0)
                    {
                        variableUnits = "Fixed";
                    }
                    else
                    {
                        variableUnits = "Variable";
                    }
                    /*
					if(descriptionCourseID.Length > 0){
						descriptionCourseOffering = csCourse.GetCourseOffering(Convert.ToInt32(descriptionCourseID));
						panSameAs.Visible = true;
					}else{
						panSameAs.Visible = false;
					}
                    */

                    lblCourseOffering.Text = drCourse["CourseOffering"].ToString().Replace(" ","&nbsp;");
					lblEFFDT.Text = Convert.ToDateTime(drCourse["EFFDT"]).ToShortDateString();
					//lblCourseEndSTRM.Text = dsCourse.Tables[0].Rows[0]["EndTerm_DESCR"].ToString();
					lblCourseSubject.Text = drCourse["SUBJECT"].ToString();
					lblSubjectDescription.Text = drCourse["DESCR"].ToString();
					lblCatalogNbr.Text = drCourse["CATALOG_NBR"].ToString();
					lblCourseShortTitle.Text = drCourse["DESCR"].ToString();
					//lblCourseSuffix.Text = dsCourse.Tables[0].Rows[0]["CourseSuffix"].ToString();
					lblCourseLongTitle.Text = drCourse["COURSE_TITLE_LONG"].ToString();
					lblVariableUnits.Text = variableUnits;
					lblUnitsMaximum.Text = drCourse["UNITS_MAXIMUM"].ToString();
					lblUnitsMinimum.Text = drCourse["UNITS_MINIMUM"].ToString();
					//lblOfferedAt.Text = strOfferedAt;
					lblDescriptionCourseOffering.Text = descriptionCourseOffering;
					lblCourseLongDescription.Text = drCourse["DESCRLONG"].ToString();	
					//lblLastModifiedEMPLID.Text = csPermissions.GetEmpName(dsCourse.Tables[0].Rows[0]["LastModifiedEMPLID"].ToString());
					//lblLastModifiedDate.Text = dsCourse.Tables[0].Rows[0]["LastModifiedDate"].ToString();
                    
                }

                panView.Visible = true;
				panDisplay.Visible = false;

			}
            else
            {
				DataSet dsCourses = csCourse.GetCourses(cboSubject.SelectedValue, cboTerm.SelectedValue);

				for(Int32 intDSRow = 0; intDSRow < dsCourses.Tables[0].Rows.Count; intDSRow++){
                    DataRow drCourses = dsCourses.Tables[0].Rows[intDSRow];

                    TableRow tr = new TableRow();

					TableCell td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl("<a name=\"" + dsCourses.Tables[0].Rows[intDSRow]["CourseOffering"] + "\">" + dsCourses.Tables[0].Rows[intDSRow]["CourseOffering"].ToString().Replace(" ","&nbsp;") + "</a>"));

					tr.Cells.Add(td);
                    
					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl(dsCourses.Tables[0].Rows[intDSRow]["DESCR"].ToString()));

					tr.Cells.Add(td);
                    
					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl(Convert.ToDateTime(dsCourses.Tables[0].Rows[intDSRow]["EFFDT"]).ToShortDateString()));

					tr.Cells.Add(td);
                    /*
					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl(dsCourses.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString()));

					tr.Cells.Add(td);
                    */
                    td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "text-align:center;vertical-align:top;padding-top:4px;padding-bottom:4px;";
                    var paramStr = String.Format("CRSE_ID={0}&EFFDT={1}", drCourses["CRSE_ID"].ToString(), Convert.ToDateTime(drCourses["EFFDT"].ToString()).ToShortDateString());
					td.Controls.Add(new LiteralControl("<a href=\"view.aspx?todo=view&"+ paramStr + "\">View</a>"));

					tr.Cells.Add(td);
                    
                    tblDisplay.Rows.Add(tr);
				}

				panDisplay.Visible = true;
				panView.Visible = false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
