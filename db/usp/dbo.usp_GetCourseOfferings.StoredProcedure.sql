USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseOfferings]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/17/2019
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseOfferings]
	@SUBJECT varchar(5) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT CourseOffering, CATALOG_NBR FROM udf_GetCourseOfferings()
	WHERE (@SUBJECT IS NULL OR REPLACE(SUBJECT,' ','') = @SUBJECT)
END
GO
