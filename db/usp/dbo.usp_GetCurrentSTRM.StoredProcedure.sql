USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCurrentSTRM]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/16/2019
-- Description:	Return Current STRM
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCurrentSTRM]
	@CurrentSTRM varchar(4) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1  @CurrentSTRM = STRM
	FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
	WHERE TERM_END_DT > GETDATE()
	ORDER BY TERM_END_DT ASC
END
GO
