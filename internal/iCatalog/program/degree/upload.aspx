<%@ Page language="c#" Inherits="ICatalog.program.degree.upload" CodeFile="upload.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
  <head>
    <title>Degree File Upload</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script type="text/javascript" src="../../_phatt3_src_files/trim.js"></script>
    <link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
	<link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
	
	<script type="text/javascript">
	<!--
        /*
		function launchCenter(url, name, height, width) {
			var str = "height=" + height + ",innerHeight=" + height;
			str += ",width=" + width + ",innerWidth=" + width;
			if (window.screen) {
				var ah = screen.availHeight - 30;
				var aw = screen.availWidth - 10;

				var xc = (aw - width) / 2;
				var yc = (ah - height) / 2;

				str += ",left=" + xc + ",screenX=" + xc;
				str += ",top=" + yc + ",screenY=" + yc;
				str += ",resizable=yes,scrollbars=yes";
			}
			return window.open(url, name, str);        }
        */

        function popupCenter(url, title, h, w) {
            // Fixes dual-screen position  
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var top = ((height / 2) - (h / 2)) + dualScreenTop;
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

            // Puts focus on the newWindow  
            if (window.focus) {
                newWindow.focus();
            }
        }
		
		function validate(){
			var strTitle = "";
			var txtDocumentTitle = document.getElementById("txtDocumentTitle");
			var cboDocumentTitle = document.getElementById("cboDocumentTitle");
			if(txtDocumentTitle != null){
				strTitle = txtDocumentTitle.value;
			}else if(cboDocumentTitle != null){
				strTitle = cboDocumentTitle.value;
			}
			if(trim(strTitle) == ""){
				alert("Please enter or select a Requirements/Worksheet Title.");
				return false;
			}else{
				return true;
			}
		}
	//-->
	</script>
  </head>
  <body>
    <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmDegree" runat="server">
					<input type="hidden" id="hidTodo" runat="server" />
					<input type="hidden" id="hidFileName" runat="server" />
					<input type="hidden" id="hidDocumentTitle" runat="server" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Degree File Upload</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
																	<asp:panel id="panView" runat="server">
																		<asp:table id="tblDisplay" runat="server" cellspacing="1" cellpadding="5" style="width:100%;">
																			<asp:tablerow>
																				<asp:TableCell CssClass="portletSecondary" style="width:50px; text-align:center;">
																					<b>Year</b>
																				</asp:TableCell>
																				<asp:tablecell cssclass="portletSecondary" style="width:384px;text-align:center;">
																					<b>Degree Requirements/Worksheet</b>
																				</asp:tablecell>
																				<asp:TableCell cssclass="portletSecondary" style="width:200px;text-align:center;">
																					<b>File Name</b>
																				</asp:TableCell>
																				<asp:tablecell cssclass="portletSecondary" style="width:80px;text-align:center;">
																					<a href="#" onclick="document.frmDegree.hidTodo.value='upload';document.frmDegree.submit();">Upload File</a>
																				</asp:tablecell>
																			</asp:tablerow>
																		</asp:table>
																	</asp:panel>
																	<asp:panel id="panUpload" runat="server">
																		<table cellpadding="0" cellspacing="1" style="width:100%;">
																			<asp:panel ID="panMsg" runat="server">
																				<tr>
																					<td class="portletMain" colspan="2" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
																						<asp:Label ID="lblMsg" Runat="server"></asp:Label>
																					</td>
																				</tr>
																			</asp:panel>
																			<tr>
																				<td class="portletSecondary" style="vertical-align:top;padding-top:9px;text-align:right;width:38%;">
																					<b>Requirements/Worksheet Year:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="padding-top:6px;padding-bottom:6px;padding-left:6px;width:62%">
																					<asp:DropDownList ID="cboEffectiveYear" Runat="server" CssClass="small"></asp:DropDownList>
																				</td>
																			</tr>
																			<tr>
																				<td class="portletSecondary" style="vertical-align:top;padding-top:9px;text-align:right;">
																					<b>Requirements/Worksheet Title:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:Panel ID="panSelectExisting" Runat="server">
																						<asp:DropDownList ID="cboDocumentTitle" Runat="server" CssClass="small"></asp:DropDownList>&nbsp;<asp:LinkButton ID="lnkAddNew" Runat="server">Add New</asp:LinkButton>
																					</asp:Panel>
																					<asp:Panel ID="panAddNew" Runat="server">
																						<asp:textbox id="txtDocumentTitle" maxlength="65" Columns="50" cssclass="small" runat="server"></asp:textbox>&nbsp;<asp:LinkButton ID="lnkSelectExisting" Runat="server">Select Existing</asp:LinkButton>
																					</asp:Panel>
																				</td>
																			</tr>
																			<tr>
																				<td class="portletSecondary" style="vertical-align:top;padding-top:9px;text-align:right;">
																					<b>Requirements/Worksheet File:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<input id="file1" type="file" style="width:350px;" runat="server" />
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2" class="portletMain" style="text-align:center;width:100%;padding:3px;">
																					<input type="button" id="cmdCancel" value="Back" class="small" style="background:#ccccaa;border:1px solid #000000;width:75px;" onclick="location.href='upload.aspx'" />
																					&nbsp;<input type="submit" id="cmdUpload" value="Upload" runat="server" class="small" style="background:#ccccaa;border:1px solid #000000;width:75px;" onclientclick="return validate();" onserverclick="cmdUpload_ServerClick" />
																				</td>
																			</tr>
																		</table>
																	</asp:panel>
																	<asp:panel id="panDegrees" runat="server">
																		<asp:table id="tblDegrees" runat="server" cellspacing="1" cellpadding="5" style="width:100%;"></asp:table>
																	</asp:panel>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</form>
			</asp:panel>
			<DIV class="clearer"></DIV>
		</asp:panel>
	</body>
</html>
