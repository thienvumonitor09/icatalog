using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.popups
{
	/// <summary>
	/// Summary description for addoption.
	/// </summary>
	public partial class addoption : System.Web.UI.Page
	{

		programData csProgram = new programData();
 

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack){
				//CHECK IF ALL THESE VALUES ARE NEEDED FOR THE EDIT SCREEN
				hidMultipleOptions.Value = Request.QueryString["mo"];
				hidProgramVersionID.Value = Request.QueryString["pvid"];
				hidProgramDegreeID.Value = Request.QueryString["pdid"];
				hidCollegeID.Value = Request.QueryString["colid"];
				hidLtr.Value = Request.QueryString["ltr"];
				hidProgramTitle.Value = Request.QueryString["pt"];
				hidOfferedAt.Value = Request.QueryString["oa"];
				hidDegreeID.Value = csProgram.GetDegreeID(Convert.ToInt32(hidProgramDegreeID.Value)).ToString();
				hidOptionID.Value = Request.QueryString["oid"];
				hidProgramDisplay.Value = Request.QueryString["pd"];
				hidProgramBeginSTRM.Value = Request.QueryString["bstrm"];
				hidProgramEndSTRM.Value = Request.QueryString["estrm"];
				hidPublishedProgram.Value = Request.QueryString["pp"];
				hidPublishedProgramVersionID.Value = Request.QueryString["ppvid"];
				hidProgramID.Value = Request.QueryString["id"];
				hidSearchSubject.Value = Request.QueryString["searchsub"];
				hidSearchId.Value = Request.QueryString["searchid"];
				hidSearchSTRM.Value = Request.QueryString["searchstrm"];
				hidSearchText.Value = Request.QueryString["searchtxt"];
			}
			
			if(hidMultipleOptions.Value == "0"){
				panCurrentTitle.Visible = true;
			}else{
				panCurrentTitle.Visible = false;
			}
		}

		protected void cmdAddOption_Click(object sender, System.EventArgs e){
			Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value);
			Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
			Int32 optionID = Convert.ToInt32(hidOptionID.Value);
			Int32 degreeID = Convert.ToInt32(hidDegreeID.Value);
			Int32 intSuccess = 1;
		
			//edit the option description
			Byte bitPrimaryOption = 0;
			if(Request.Form["hidPrimaryOption"] == "true"){
				bitPrimaryOption = 1;
			}

			csProgram.EditOptionValues(optionID, programVersionID, Request.Form["hidOptionDescription"], Request.Form["hidOptionStateApproval"], Request.Form["hidCIP"], Request.Form["hidEPC"], Request.Form["hidAcademicPlan"], bitPrimaryOption, Request.Form["hidGainfulEmploymentID"], Request.Form["hidTotalQuarters"]);
			
			csProgram.DeleteOptionLocations(optionID);
			if(hidLocations.Value != ""){
				String[] strOptionLocations = hidLocations.Value.Substring(1).Split('|');
				for(Int32 i = 0; i < strOptionLocations.Length; i++){
					csProgram.AddOptionLocation(optionID, Convert.ToInt32(strOptionLocations[i]));
				}
			}

			csProgram.DeleteOptionMCodes(optionID);
			if(hidMCodes.Value != ""){
				String[] strMCodes = hidMCodes.Value.Substring(1).Split('|');
				for(Int32 i = 0; i < strMCodes.Length; i++){
					String[] strCollegeMCode = strMCodes[i].Split(';');
					csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0]), strCollegeMCode[1]);
				}
			}

			if(hidMultipleOptions.Value == "0"){
				intSuccess = csProgram.AddOptionTitle(degreeID, programVersionID, txtCurrentOptTitle.Text);
			}

			if(intSuccess == 2){
				//the current program option title entered already exists
				lblErrorMsg.Text = "Error: The current option title entered already exists.";
				panError.Visible = true;
			}else if(intSuccess == 1){
				//add the new program option - change this to get the OptionID that was just inserted
				optionID = csProgram.AddProgOption(programDegreeID, txtNewOptTitle.Text);

				if(optionID < 0){
					//the new program option title entered already exists
					lblErrorMsg.Text = "Error: The new option title entered already exists.";
					panError.Visible = true;
				}else if(optionID == 0){
					//the insert failed
					lblErrorMsg.Text = "Error: The insert failed.";
					panError.Visible = true;
				}else{

					Int32 locationID = csProgram.GetLocationID(Convert.ToInt32(hidCollegeID.Value)); //get LocationID of selected College
					if(locationID > 0){ //if college exists as a location
						//insert college as the default location the program option is offered at
						csProgram.AddOptionLocation(optionID, locationID);
					}

					hidOptionID.Value = optionID.ToString();
					hidMultipleOptions.Value = "1";

					//refresh main window
					Response.Write("<script type=\"text/javascript\">opener.location.href='../edit.aspx?oid=" + hidOptionID.Value + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&todo=pop&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'","\\'") + "&pt=" + hidProgramTitle.Value.Replace("'","\\'") + "&pd=" + hidProgramDisplay.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&","%26") + "&searchid=" + hidSearchId.Value.Replace("&","%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&","%26").Replace("'","\\'") + "';</script>");
					
					//reset the form
					txtCurrentOptTitle.Text = "";
					txtNewOptTitle.Text = "";
					panCurrentTitle.Visible = false;
					panError.Visible = false;
				}
			}else{
				//the current option title updated failed.
				lblErrorMsg.Text = "Error: The insert failed.";
				panError.Visible = true;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
