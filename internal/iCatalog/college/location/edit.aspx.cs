using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.college.location
{
	/// <summary>
	/// Summary description for edit.
	/// </summary>
	public partial class edit : System.Web.UI.Page
	{
		programData csProgram = new programData();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panError.Visible = false;
			panEdit.Visible = false;
			panView.Visible = false;
			panDisplay.Visible = false;

			if(!IsPostBack){
				DataSet dsCollegeList = csProgram.GetCollegeList();
				cboCollege.Items.Add("");
				for(Int32 i = 0; i < dsCollegeList.Tables[0].Rows.Count; i++){
					cboCollege.Items.Add(new ListItem(dsCollegeList.Tables[0].Rows[i]["CollegeLongTitle"].ToString(), dsCollegeList.Tables[0].Rows[i]["CollegeID"].ToString()));
				}

				DataSet dsLocationList = csProgram.GetLocationList();
				Int32 intRowCtr = dsLocationList.Tables[0].Rows.Count;
				
				TableRow tr = new TableRow();

				TableCell td = new TableCell();
				td.Attributes["style"] = "text-align:center;width:525px;font-weight:bold;padding-top:4px;padding-bottom:4px;";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("Location Title"));

				tr.Cells.Add(td);

				td = new TableCell();
				td.Attributes["style"] = "width:68px;text-align:center;";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("<a href=\"add.aspx\">Add New</a>"));

				tr.Cells.Add(td);
				tblDisplay.Rows.Add(tr);

				if(intRowCtr > 0){
					for(Int32 intDSRow = 0; intDSRow < intRowCtr; intDSRow++){
						Int32 locationID = Convert.ToInt32(dsLocationList.Tables[0].Rows[intDSRow]["LocationID"]);
						String locationTitle = dsLocationList.Tables[0].Rows[intDSRow]["LocationTitle"].ToString();

						tr = new TableRow();

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
						td.Controls.Add(new LiteralControl(dsLocationList.Tables[0].Rows[intDSRow]["LocationTitle"].ToString()));

						tr.Cells.Add(td);

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;";
						if(dsLocationList.Tables[0].Rows[intDSRow]["CollegeID"].ToString() == ""){
							td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.getElementById('hidTodo').value='edit';document.getElementById('hidLocationID').value='" + locationID + "';document.frmLocation.submit();\">Edit</a>"));
						}else{
							td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.getElementById('hidTodo').value='view';document.getElementById('hidLocationID').value='" + locationID + "';document.frmLocation.submit();\">View</a>"));
						}
						if(Convert.ToInt32(dsLocationList.Tables[0].Rows[intDSRow]["OptionCount"]) == 0){
							td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete " + locationTitle.Replace("'","\\'") + "?')){document.getElementById('hidTodo').value='delete';document.getElementById('hidLocationID').value='" + locationID + "';document.frmLocation.submit();}\">Delete</a>"));
						}
						tr.Cells.Add(td);
						tblDisplay.Rows.Add(tr);
					}
				}else{
					tr = new TableRow();

					td = new TableCell();
					td.ColumnSpan = 3;
					td.Attributes["style"] = "width:100%;padding:4px;";
					td.CssClass = "portletLight";
					td.Controls.Add(new LiteralControl("No locations currently exist."));

					tr.Cells.Add(td);
					tblDisplay.Rows.Add(tr);
				}
				panDisplay.Visible = true;
			}else if(hidTodo.Value == "edit"){
				DataSet dsLocation = csProgram.GetLocation(Convert.ToInt32(Request.Form["hidLocationID"]));
				if(dsLocation.Tables[0].Rows.Count > 0){
					txtLocationTitle.Text = dsLocation.Tables[0].Rows[0]["LocationTitle"].ToString();
					txtLocationAddress.Text = dsLocation.Tables[0].Rows[0]["LocationAddress"].ToString();
					txtLocationCity.Text = dsLocation.Tables[0].Rows[0]["LocationCity"].ToString();
					txtLocationState.Text = dsLocation.Tables[0].Rows[0]["LocationState"].ToString();
					txtLocationZipCode1.Text = dsLocation.Tables[0].Rows[0]["LocationZipCode1"].ToString();
					txtLocationZipCode2.Text = dsLocation.Tables[0].Rows[0]["LocationZipCode2"].ToString();
					txtLocationWebsiteURL.Text = dsLocation.Tables[0].Rows[0]["LocationWebsiteURL"].ToString();
					txtLocationRegistrationEmail.Text = dsLocation.Tables[0].Rows[0]["LocationRegistrationEmail"].ToString();
				}
				if(cboCollege.SelectedValue != ""){
					DataSet dsCollege = csProgram.GetCollege(Convert.ToInt32(cboCollege.SelectedValue));
					if(dsCollege.Tables[0].Rows.Count > 0){
						txtLocationTitle.Text = dsCollege.Tables[0].Rows[0]["CollegeLongTitle"].ToString();
						txtLocationAddress.Text = dsCollege.Tables[0].Rows[0]["CollegeAddress"].ToString();
						txtLocationZipCode1.Text = dsCollege.Tables[0].Rows[0]["CollegeZipCode1"].ToString();
						txtLocationZipCode2.Text = dsCollege.Tables[0].Rows[0]["CollegeZipCode2"].ToString();
						txtLocationCity.Text = dsCollege.Tables[0].Rows[0]["CollegeCity"].ToString();
						txtLocationState.Text = dsCollege.Tables[0].Rows[0]["CollegeState"].ToString();
						txtLocationWebsiteURL.Text = dsCollege.Tables[0].Rows[0]["CollegeWebsiteURL"].ToString();
						txtLocationRegistrationEmail.Text = dsCollege.Tables[0].Rows[0]["CollegeRegistrationEmail"].ToString();

						txtLocationTitle.Enabled = false;
						txtLocationAddress.Enabled = false;
                        txtLocationCity.Enabled = false;
                        txtLocationState.Enabled = false;
                        txtLocationZipCode1.Enabled = false;
                        txtLocationZipCode2.Enabled = false;
						txtLocationWebsiteURL.Enabled = false;
						txtLocationRegistrationEmail.Enabled = false;
					}
				}
				hidTodo.Value = "";
				panEdit.Visible = true;
				panDisplay.Visible = false;
			}else if(hidTodo.Value == "view"){
				DataSet dsLocation = csProgram.GetLocation(Convert.ToInt32(Request.Form["hidLocationID"]));
				if(dsLocation.Tables[0].Rows.Count > 0){
					lblLocationTitle.Text = dsLocation.Tables[0].Rows[0]["LocationTitle"].ToString();
					lblLocationAddress.Text = dsLocation.Tables[0].Rows[0]["LocationAddress"].ToString();
					lblLocationCity.Text = dsLocation.Tables[0].Rows[0]["LocationCity"].ToString();
					lblLocationState.Text = dsLocation.Tables[0].Rows[0]["LocationState"].ToString();
					lblLocationZipCode1.Text = dsLocation.Tables[0].Rows[0]["LocationZipCode1"].ToString();
					lblLocationZipCode2.Text = dsLocation.Tables[0].Rows[0]["LocationZipCode2"].ToString();
					lblLocationWebsiteURL.Text = dsLocation.Tables[0].Rows[0]["LocationWebsiteURL"].ToString();
					lblLocationRegistrationEmail.Text = dsLocation.Tables[0].Rows[0]["LocationRegistrationEmail"].ToString();
				}
				panView.Visible = true;
				panDisplay.Visible = false;
			}else if(hidTodo.Value == "delete"){
				csProgram.DeleteLocation(Convert.ToInt32(hidLocationID.Value));
				Response.Redirect("edit.aspx");
			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			Int32 locationID = Convert.ToInt32(Request.Form["hidLocationID"]);
			Int16 intSuccess = csProgram.EditLocation(locationID, txtLocationTitle.Text, txtLocationAddress.Text, txtLocationCity.Text, txtLocationState.Text, txtLocationZipCode1.Text, txtLocationZipCode2.Text, txtLocationWebsiteURL.Text, txtLocationRegistrationEmail.Text, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]);
			
			if(intSuccess == 1){
				Response.Redirect("edit.aspx");
			}else if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The location entered already exists.";
				panError.Visible = true;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The update failed.";
				panError.Visible = true;
			}
		}

		public void PopulateFieds(object sender, System.EventArgs e){
			if(cboCollege.SelectedValue != ""){
				DataSet dsCollege = csProgram.GetCollege(Convert.ToInt32(cboCollege.SelectedValue));
				if(dsCollege.Tables[0].Rows.Count > 0){
					txtLocationTitle.Text = dsCollege.Tables[0].Rows[0]["CollegeLongTitle"].ToString();
					txtLocationAddress.Text = dsCollege.Tables[0].Rows[0]["CollegeAddress"].ToString();
					txtLocationZipCode1.Text = dsCollege.Tables[0].Rows[0]["CollegeZipCode1"].ToString();
					txtLocationZipCode2.Text = dsCollege.Tables[0].Rows[0]["CollegeZipCode2"].ToString();
					txtLocationCity.Text = dsCollege.Tables[0].Rows[0]["CollegeCity"].ToString();
					txtLocationState.Text = dsCollege.Tables[0].Rows[0]["CollegeState"].ToString();
					txtLocationWebsiteURL.Text = dsCollege.Tables[0].Rows[0]["CollegeWebsiteURL"].ToString();
					txtLocationRegistrationEmail.Text = dsCollege.Tables[0].Rows[0]["CollegeRegistrationEmail"].ToString();

					txtLocationTitle.Enabled = false;
					txtLocationAddress.Enabled = false;
                    txtLocationCity.Enabled = false;
                    txtLocationState.Enabled = false;
                    txtLocationZipCode1.Enabled = false;
                    txtLocationZipCode2.Enabled = false;
					txtLocationWebsiteURL.Enabled = false;
					txtLocationRegistrationEmail.Enabled = false;
				}
			}else{
				txtLocationTitle.Text = "";
				txtLocationAddress.Text = "";
				txtLocationZipCode1.Text = "";
				txtLocationZipCode2.Text = "";
				txtLocationCity.Text = "";
				txtLocationState.Text = "";
				txtLocationWebsiteURL.Text = "";
				txtLocationRegistrationEmail.Text = "";

				txtLocationTitle.Enabled = true;
				txtLocationAddress.Enabled = true;
                txtLocationCity.Enabled = true;
                txtLocationState.Enabled = true;
				txtLocationZipCode1.Enabled = true;
                txtLocationZipCode2.Enabled = true;
				txtLocationWebsiteURL.Enabled = true;
				txtLocationRegistrationEmail.Enabled = true;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
