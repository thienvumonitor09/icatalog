USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDegreeRequirementWorksheet]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/11/17
-- Description:	Returns degree requirement worksheet file name by title and term.
--		 INPUT: DocumentTitle - Degree Requirement Worksheet Title
--				STRM - Term Code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDegreeRequirementWorksheet]
	@DocumentTitle varchar(100),
	@STRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 AND ACAD_CAREER = 'UGRD'
					 AND INSTITUTION = 'WA171'
					 ORDER BY TERM_END_DT)
					 
	DECLARE @DESCR varchar(30)
	DECLARE @TERM_BEGIN_DT date
    
	SELECT @DESCR = DESCR, @TERM_BEGIN_DT = TERM_BEGIN_DT
	FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
	WHERE STRM = @STRM
	AND ACAD_CAREER = 'UGRD'
	AND INSTITUTION = 'WA171';
	
	IF CHARINDEX('FALL',@DESCR) > 0
		SELECT FileName
		FROM DegreeRequirementWorksheet
		WHERE DocumentTitle = @DocumentTitle
		AND EffectiveYear = YEAR(@TERM_BEGIN_DT)
	ELSE
		SELECT FileName
		FROM DegreeRequirementWorksheet
		WHERE DocumentTitle = @DocumentTitle
		AND EffectiveYear = YEAR(DATEADD(Year, -1, @TERM_BEGIN_DT))
	
END

GO
