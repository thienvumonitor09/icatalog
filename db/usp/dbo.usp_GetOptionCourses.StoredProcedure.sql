USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionCourses]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn, edited by Vu Nguyen
-- Create date: 05/14/2019
-- Description:	Returns program degree option courses
-- Parameters:	
--			@OptionID 
--			@STRM - Term Code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionCourses]
	@OptionID int,
	@STRM varchar(4),
	@INSTITUTION varchar(5) = 'WA171'
AS
BEGIN
	SET NOCOUNT ON;
	SELECT OptionCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseOffering.EFFDT AS CourseOffering_EFFDT, CourseCatalog.EFFDT AS CourseCatalog_EFFDT, RTRIM(REPLACE(COURSE_TITLE_LONG,'[CCN]','')) AS COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum 
	FROM OptionCourse 
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	ON REPLACE(CourseOffering.SUBJECT + CourseOffering.CATALOG_NBR,' ','') = REPLACE(OptionCourse.CourseOffering,' ','')
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	LEFT OUTER JOIN OptionFootnote 
	ON OptionFootnote.OptionFootnoteID = OptionCourse.OptionFootnoteID 
	WHERE OptionCourse.OptionID = @OptionID
		AND CourseOffering.EFFDT = 
		(
			SELECT MAX(CourseOffering2.EFFDT)
			FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
			WHERE CourseOffering2.CRSE_ID = CourseOffering.CRSE_ID /*CourseOffering2.SUBJECT = CourseOffering.SUBJECT
							AND CourseOffering2.CATALOG_NBR = CourseOffering.CATALOG_NBR*/
							AND INSTITUTION = @INSTITUTION
			AND CourseOffering2.EFFDT <= (SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE STRM =  @STRM ORDER BY TERM_END_DT DESC)
		)
		AND CATALOG_PRINT = 'Y'
		AND ACAD_CAREER = 'UGRD'
		AND CATALOG_PRINT = 'Y'
		AND CourseCatalog.EFF_STATUS = 'A'
		AND CourseOffering.INSTITUTION = @INSTITUTION
	GROUP BY OptionCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseOffering.EFFDT, CourseCatalog.EFFDT, RTRIM(REPLACE(COURSE_TITLE_LONG,'[CCN]','')), UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
	ORDER BY Quarter, SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;

	/*
	SELECT OptionCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseOffering.EFFDT AS CourseOffering_EFFDT, CourseCatalog.EFFDT AS CourseCatalog_EFFDT, RTRIM(REPLACE(COURSE_TITLE_LONG,'[CCN]','')) AS COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum 
	FROM OptionCourse 
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	ON REPLACE(CourseOffering.SUBJECT + CourseOffering.CATALOG_NBR,' ','') = REPLACE(OptionCourse.CourseOffering,' ','')
		AND CourseOffering.EFFDT = 
			(SELECT MAX(CourseOffering2.EFFDT) 
			FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
			WHERE CourseOffering2.SUBJECT = CourseOffering.SUBJECT
			AND CourseOffering2.CATALOG_NBR = CourseOffering.CATALOG_NBR
			AND CourseOffering2.EFFDT <= (SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE STRM = @STRM/*SelectedSTRM or CurrentSTRM*/ ORDER BY TERM_END_DT DESC))
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID
		AND CourseCatalog.EFFDT = 
			(SELECT MAX(CourseCatalog2.EFFDT)
			FROM CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog2
			WHERE CourseCatalog2.CRSE_ID = CourseCatalog.CRSE_ID
			AND CourseCatalog2.EFFDT <= (SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE STRM = @STRM/*SelectedSTRM or CurrentSTRM*/ ORDER BY TERM_END_DT DESC))
	LEFT OUTER JOIN CCSiCatalog_Legacy.dbo.OptionFootnote 
	ON OptionFootnote.OptionFootnoteID = OptionCourse.OptionFootnoteID 
	WHERE OptionCourse.OptionID = @OptionID
		AND CATALOG_PRINT = 'Y'
	GROUP BY OptionCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseOffering.EFFDT, CourseCatalog.EFFDT, RTRIM(REPLACE(COURSE_TITLE_LONG,'[CCN]','')), UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
	ORDER BY Quarter, SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
	*/
END
GO
