<%@ Reference Page="~/dept/add.aspx" %>
<%@ Page language="c#" Inherits="ICatalog.dept.edit" CodeFile="edit.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Subject Area Maintenance</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <script type="text/javascript" src="../_phatt3_src_files/trim.js"></script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmDept" runat="server">
                    <input type="hidden" runat="server" id="hidTodo" />
                    <input type="hidden" runat="server" id="hidLtr" />
                    <input type="hidden" runat="server" id="hidSubject" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Subject Area Maintenance</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <asp:panel runat="server" id="panDisplay">
                                                                        <asp:table runat="server" id="tblAlpha" style="width:100%;" cellspacing="0" cellpadding="0"></asp:table>
                                                                        <asp:table runat="server" id="tblDisplay" style="width:100%;" cellspacing="1" cellpadding="2"></asp:table>
                                                                    </asp:panel>
                                                                    <asp:panel runat="server" id="panEdit">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <!-- Subject -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Subject Area:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblSubject"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Subject Description -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Subject Area Description:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtSubjectDescription" cssclass="small" maxlength="80" style="width:350px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
                                                                                <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
                                                                                    <input type="button" id="cmdCancel" value="Cancel" onclick="history.back(1);" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" /> 
                                                                                    &nbsp;<asp:button id="cmdSubmit" runat="server" text="Save Changes" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="cmdSubmit_Click"></asp:button>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <div class="clearer"></div>
        </asp:panel>
    </body>
</html>

