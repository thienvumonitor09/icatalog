USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramsContainingCourse]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn, EDITED BY Vu Nguyen
-- Create date: 4/28/17
-- Description:	Returns a list of programs containing the specified course
--		 INPUT: CourseOffering - Course Subject Area padded to five characters + course suffix + Catalog Nbr (Example: ENG  101)
--				BeginSTRM - Course Begin Term Code (optional)
--				EndSTRM - Course End Term Code (optional)
-- Edited: 
--		05/17/2019 - Vu Nguyen - Remove space in CourseOffering when comparing
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramsContainingCourse] 
	@CourseOffering varchar(9),
	@BeginSTRM varchar(4) = NULL,
	@EndSTRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT AreaOfStudy.Title, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, CollegeShortTitle, ProgramTitle, ProgramBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, ProgramEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, PublishedProgram, PublishedProgramVersionID, ProgramDisplay 
	FROM Program 
	LEFT OUTER JOIN AreaOfStudyProgram
	ON Program.ProgramID = AreaOfStudyProgram.ProgramID
	LEFT OUTER JOIN AreaOfStudy
	ON AreaOfStudy.AreaOfStudyID = AreaOfStudyProgram.AreaOfStudyID
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = ProgramBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = ProgramEndSTRM
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID
	INNER JOIN ProgramDegree 
	ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID 
	LEFT OUTER JOIN Degree
	ON Degree.DegreeID = ProgramDegree.DegreeID
	INNER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	WHERE 
		(
			REPLACE(OptionPrerequisite.CourseOffering,' ','') = REPLACE(@CourseOffering,' ','')
			OR 
			REPLACE(ElectiveGroupCourse.CourseOffering,' ','') = REPLACE(@CourseOffering,' ','')
			OR 
			REPLACE(OptionCourse.CourseOffering,' ','') = REPLACE(@CourseOffering,' ','')
		)
		AND 
			(
				(@BeginSTRM IS NULL AND @EndSTRM IS NULL) 
			OR ((ProgramBeginSTRM >= @BeginSTRM AND ProgramEndSTRM <= @EndSTRM)
			OR (ProgramBeginSTRM <= @BeginSTRM AND ProgramEndSTRM >= @BeginSTRM))
			) 
	ORDER BY Title, DegreeShortTitle, ProgramTitle, CollegeShortTitle, ProgramBeginSTRM DESC, PublishedProgram;
END

GO
