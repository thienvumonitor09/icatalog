using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.popups
{
	/// <summary>
	/// Summary description for editelectgroup.
	/// </summary>
	public partial class editelectgroup : System.Web.UI.Page
	{

		programData csProgram = new programData();


		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack){
				//CHECK IF ALL THESE VALUES ARE NEEDED FOR THE EDIT SCREEN
				hidProgramVersionID.Value = Request.QueryString["pvid"];
				hidProgramDegreeID.Value = Request.QueryString["pdid"];
				hidCollegeID.Value = Request.QueryString["colid"];
				hidLtr.Value = Request.QueryString["ltr"];
				hidProgramTitle.Value = Request.QueryString["pt"];
				hidOfferedAt.Value = Request.QueryString["oa"];
				hidDegreeID.Value = csProgram.GetDegreeID(Convert.ToInt32(hidProgramDegreeID.Value)).ToString();
				hidOptionID.Value = Request.QueryString["oid"];
				hidProgramDisplay.Value = Request.QueryString["pd"];
				hidOptionElectiveGroupID.Value = Request.QueryString["egid"];
				hidProgramBeginSTRM.Value = Request.QueryString["bstrm"];
				hidProgramEndSTRM.Value = Request.QueryString["estrm"];
				hidPublishedProgram.Value = Request.QueryString["pp"];
				hidPublishedProgramVersionID.Value = Request.QueryString["ppvid"];
				hidSTRM.Value = Request.QueryString["strm"];
				hidSelectedCollegeID.Value = Request.QueryString["scolid"];
				hidProgramID.Value = Request.QueryString["id"];
				hidSearchSubject.Value = Request.QueryString["searchsub"];
				hidSearchId.Value = Request.QueryString["searchid"];
				hidSearchSTRM.Value = Request.QueryString["searchstrm"];
				hidSearchText.Value = Request.QueryString["searchtxt"];

				DataSet dsFootnote = csProgram.GetOptFootnotes(Convert.ToInt32(hidOptionID.Value));
				Int32 intFootnoteCtr = dsFootnote.Tables[0].Rows.Count;

				if(intFootnoteCtr > 0){
					cboElectGroupFootnote.Items.Add(new ListItem("", "0"));
					for(Int32 intDSRow = 0; intDSRow < intFootnoteCtr; intDSRow++){
						cboElectGroupFootnote.Items.Add(new ListItem(dsFootnote.Tables[0].Rows[intDSRow]["FootnoteNumber"].ToString(), dsFootnote.Tables[0].Rows[intDSRow]["OptionFootnoteID"].ToString()));
					}
				}else{
					cboElectGroupFootnote.Items.Add(new ListItem("None Entered", "0"));
				}

				DataSet dsElectGroup = csProgram.GetOptionElectiveGroup(Convert.ToInt32(hidOptionElectiveGroupID.Value));

				if(dsElectGroup.Tables[0].Rows.Count > 0){ //may want to add else condition with an error
					String strOptionFootnoteID = dsElectGroup.Tables[0].Rows[0]["OptionFootnoteID"].ToString();

					if(strOptionFootnoteID != ""){
						cboElectGroupFootnote.SelectedValue = strOptionFootnoteID;
						lblElectGroupFootnote.Text = dsElectGroup.Tables[0].Rows[0]["Footnote"].ToString();
					}

					txtElectGroupTitle.Text = dsElectGroup.Tables[0].Rows[0]["ElectiveGroupTitle"].ToString();
				}
			}else{
				if(cboElectGroupFootnote.SelectedValue != "0"){
					lblElectGroupFootnote.Text = csProgram.GetFootnote(Convert.ToInt32(cboElectGroupFootnote.SelectedValue));
				}else{
					lblElectGroupFootnote.Text = "None Selected";
				}
			}
		}

		protected void cmdEditElectGroup_Click(object sender, System.EventArgs e){
			Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value);
			Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
			Int32 optionID = Convert.ToInt32(hidOptionID.Value);
			Int32 degreeID = Convert.ToInt32(hidDegreeID.Value);
		
			Byte bitPrimaryOption = 0;
			if(Request.Form["hidPrimaryOption"] == "true"){
				bitPrimaryOption = 1;
			}

			csProgram.EditOptionValues(optionID, programVersionID, Request.Form["hidOptionDescription"], Request.Form["hidOptionStateApproval"], Request.Form["hidCIP"], Request.Form["hidEPC"], Request.Form["hidAcademicPlan"], bitPrimaryOption, Request.Form["hidGainfulEmploymentID"], Request.Form["hidTotalQuarters"]);
			
			csProgram.DeleteOptionLocations(optionID);
			if(hidLocations.Value != ""){
				String[] strOptionLocations = hidLocations.Value.Substring(1).Split('|');
				for(Int32 i = 0; i < strOptionLocations.Length; i++){
					csProgram.AddOptionLocation(optionID, Convert.ToInt32(strOptionLocations[i]));
				}
			}

			csProgram.DeleteOptionMCodes(optionID);
			if(hidMCodes.Value != ""){
				String[] strMCodes = hidMCodes.Value.Substring(1).Split('|');
				for(Int32 i = 0; i < strMCodes.Length; i++){
					String[] strCollegeMCode = strMCodes[i].Split(';');
					csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0]), strCollegeMCode[1]);
				}
			}

			Int16 intSuccess = csProgram.EditOptionElectiveGroup(Convert.ToInt32(hidOptionID.Value), txtElectGroupTitle.Text, Convert.ToInt32(cboElectGroupFootnote.SelectedValue), Convert.ToInt32(hidOptionElectiveGroupID.Value));

			if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The elective group title entered already exists.";
				panError.Visible = true;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The insert failed.";
				panError.Visible = true;
			}else{
				//refresh main window
				Response.Write("<script type=\"text/javascript\">opener.location.href='../edit.aspx?oid=" + hidOptionID.Value + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&todo=pop&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'","\\'") + "&pt=" + hidProgramTitle.Value.Replace("'","\\'") + "&pd=" + hidProgramDisplay.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&","%26") + "&searchid=" + hidSearchId.Value.Replace("&","%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&","%26").Replace("'","\\'") + "';self.close();</script>");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
