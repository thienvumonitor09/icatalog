USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetColleges]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/9/17
-- Description:	Returns all colleges
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetColleges] 
	
AS
BEGIN
	SET NOCOUNT ON;

    SELECT CollegeID, CollegeShortTitle, CollegeLongTitle
    FROM College;
END

GO
