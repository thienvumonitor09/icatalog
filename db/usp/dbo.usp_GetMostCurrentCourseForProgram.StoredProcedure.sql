USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMostCurrentCourseForProgram]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/08/2019
-- Description:	Returns the most current version of a course in a program term range
-- Parameters:	
--   @CourseOffering - SUBJECT + CATALOG_NBR
--   @INSTITUITION - Instituition name
--   @EndSTRM - Program end term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetMostCurrentCourseForProgram]
	@CourseOffering varchar(9),
	@EndSTRM varchar(4),
	@INSTITUTION varchar(5)
AS
BEGIN
	SET NOCOUNT ON;

    /* CURRENT PROGRAM */
	/* Select the most current active version of the course in the program term range. */
	SELECT TOP 1 COURSE_TITLE_LONG, CourseOffering.EFFDT,UNITS_MINIMUM, UNITS_MAXIMUM
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	WHERE 
		(
			CourseOffering.EFFDT <= ((SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE STRM = @EndSTRM ORDER BY TERM_END_DT DESC))
			OR
			@EndSTRM = 'Z999'
		)
		AND REPLACE(CourseOffering.SUBJECT + CourseOffering.CATALOG_NBR,' ','') = REPLACE(@CourseOffering,' ','')
		AND ACAD_CAREER = 'UGRD'
		AND CATALOG_PRINT = 'Y'
		AND EFF_STATUS = 'A'
		AND INSTITUTION = @INSTITUTION
	ORDER BY EFFDT DESC
END


GO
