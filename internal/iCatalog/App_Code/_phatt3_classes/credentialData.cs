using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;

namespace ICatalog._phatt3_classes{

	/// <summary>
	/// Summary description for credential.
	/// </summary>
	public class credentialData{

		private SqlConnection objCon;
		private String strSQL, strConnection;
		private SqlCommand objCmd;
		private SqlDataAdapter objDA;
		private DataSet objDS;

		public credentialData()
		{
            //development connection string
            //strConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCS-SQL-Dev;Workstation ID=InternalICatalog;Integrated Security=true;";
            strConnection = "Initial Catalog=CCSICatalog;Data Source=CCS-SQL-Dev;Workstation ID=InternalICatalog;Integrated Security=true;";

            //production connection string
            //strConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=InternalICatalog;Integrated Security=true;";
            //strConnection = "Initial Catalog=CCSICatalog;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=InternalICatalog;Integrated Security=true;";
        }


        /******************************************* AddCredential *********************************************
		 * INPUT:
		 * credentialEmployeeID - Instuctor SID
		 * credentialName - Instructor Name
		 * credentialAssignment -  Department Where Instructor is Assigned
		 * String credentialWorkUnit - Unit Where Instructor is Assigned
		 * credentialTenureTrack - Tenure Track Type/Indicator
		 * lastModifiedDate - Date/Time Credential Was Inserted
		 * lastModifiedEmployeeID - SID of User Inserting Credential
		 * credentialDescription - Instructor Description
		 * 
		 * OUTPUT: Int16
		 *		   0 - Insert Failed
		 *		   1 - Insert Was Successful
		 *		   2 - Record Already Exists
		 * 
		 * USED IN: /iCatalog/cred/add.aspx.cs 
		 * 
		 * DESCRIPTION: Inserts a Credential (Instructor Information)
		 * 
		 *******************************************************************************************************/
        public Int16 AddCredential(String credentialEmployeeID, String credentialName, String credentialAssignment, String credentialWorkUnit, String credentialTenureTrack, DateTime lastModifiedDate, String lastModifiedEmployeeID, String credentialDescription){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT * " + 
					"FROM Credential " +
					"WHERE EMPLID = '" + credentialEmployeeID + "';";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				if(objDS.Tables[0].Rows.Count > 0){
					return 2;
				}else{
					strSQL = "INSERT INTO Credential(EMPLID, Name, Assignment, WorkUnit, TenureTrack, LastModifiedDate, LastModifiedEMPLID, Description) " +
						"VALUES ('" + credentialEmployeeID + "','" + credentialName.Replace("'","''") + "','" + credentialAssignment.Replace("'","''") + "','" + credentialWorkUnit + "','" + credentialTenureTrack + "','" + lastModifiedDate + "','" + lastModifiedEmployeeID + "','" + credentialDescription.Replace("'","''") + "');";
				
					objCmd = new SqlCommand(strSQL, objCon);

					Int32 intCtr = 0;
					intCtr = objCmd.ExecuteNonQuery();

					if(intCtr == 0){
						return 0;
					}else{
						return 1;
					}
				}

			}finally{
				objCon.Close();
			}
		}


		/****************************************** GetCredentials ********************************************
		 * INPUT:
		 * chrLtr - Letter of Alphabet as a Char DataType
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: EMPLID - Instructor SID
		 *			   Name - Instructor Name
		 *			   Assignment - Department Where Instructor is Assigned
		 * 
		 * USED IN: /iCatalog/cred/editdelete.aspx.cs 
		 * 
		 * DESCRIPTION: Gets instructor information for instructors who's last name starts with the alphabet 
		 *				letter entered.
		 * 
		 *******************************************************************************************************/
		public DataSet GetCredentials(Char chrLtr){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();
			
				strSQL = "SELECT EMPLID, Name, Assignment " +
					"FROM Credential " +
					"WHERE Name LIKE '" + chrLtr + "%' " +
					"ORDER BY Name;";
				
				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/*************************************** GetCredentialForEdit *****************************************
		 * INPUT:
		 * employeeID - Instructor SID
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: EMPLID - Instructor SID
		 *             Name - Instructor Name
		 *             Assignment - Department Where Instructor is Assigned
		 *             TenureTrack - Unit Where Instructor is Assigned
		 *             LastModifiedDate - Date/Time Credential Was Updated
		 *             LastModifiedEMPLID - SID of User That Last Updated the Credential
		 *             Description - Credential Description
		 * 
		 * USED IN: /iCatalog/cred/editdelete.aspx.cs 
		 * 
		 * DESCRIPTION: Retrieves the Instructor Information for the SID entered.
		 * 
		 *******************************************************************************************************/
		public DataSet GetCredentialForEdit(String employeeID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();
			
				strSQL = "SELECT * " +
					"FROM Credential " +
					"WHERE EMPLID = '" + employeeID + "';";
			
				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/****************************************** EditCredential ********************************************
		 * INPUT:
		 * employeeID - Instructor SID
		 * credentialAssignment - Department Where Instructor is Assigned
		 * credentialTenureTrack - Unit Where Instructor is Assigned
		 * credentialDescription - Credential Description
		 * 
		 * OUTPUT: bool
		 *		   true - Update Was Successful
		 *		   false - Update Failed 
		 * 
		 * USED IN: /iCatalog/cred/editdelete.aspx.cs 
		 * 
		 * DESCRIPTION: Updates Credential information for the SID entered.
		 * 
		 *******************************************************************************************************/
		public bool EditCredential(String employeeID, String credentialAssignment, String credentialWorkUnit, String credentialTenureTrack, String credentialDescription){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "UPDATE Credential " +
					"SET Assignment='" + credentialAssignment.Replace("'","''") + "', WorkUnit='" + credentialWorkUnit + "', TenureTrack = '" + credentialTenureTrack + "', Description = '" + credentialDescription.Replace("'","''") + "' " +
					"WHERE EMPLID='" + employeeID + "';";
	 
				objCmd = new SqlCommand(strSQL, objCon);

				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();

				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		/***************************************** DeleteCredential *******************************************
		 * INPUT:
		 * employeeID - Instructor SID
		 * 
		 * OUTPUT: bool
		 *		   true - Delete Was Successful
		 *		   false - Delete Failed
		 * 
		 * USED IN: /iCatalog/cred/editdelete.aspx.cs 
		 * 
		 * DESCRIPTION: Deletes the credential for the SID entered.
		 * 
		 *******************************************************************************************************/
		public bool DeleteCredential(String employeeID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "DELETE FROM Credential " +
					"WHERE EMPLID = '" + employeeID + "';";

				objCmd = new SqlCommand(strSQL, objCon);

				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();

				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		/**************************************** ExportCredToExcel *******************************************
		 * INPUT:
		 * credentialAssignment - Department Where Instructor is Assigned
		 * credentialTenureTrack - Tenure Track Type/Indicator
		 * 
		 * OUTPUT: (String) Path to Text File Generated
		 * 
		 * USED IN: /iCatalog/cred/excelexport.aspx.cs 
		 * 
		 * DESCRIPTION: Generates an excel (.csv) file containing Credential data pertaining to the parameters entered.
		 * 
		 *******************************************************************************************************/
		public String ExportCredToExcel(String credentialWorkUnit, String credentialTenureTrack){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				if(credentialTenureTrack == "ALL"){
					strSQL = "SELECT * " +
						"FROM Credential " +
						"WHERE WorkUnit LIKE '%" + credentialWorkUnit + "';";
				}else{
					strSQL = "SELECT * " +
						"FROM Credential " +
						"WHERE WorkUnit LIKE '%" + credentialWorkUnit + 
						"' AND TenureTrack = '" + credentialTenureTrack + "';";
				}
			
				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				StreamWriter objSW = File.CreateText(HttpContext.Current.Server.MapPath("../ExcelCredentials.csv"));

                objSW.WriteLine("ctcLink ID, Name, Assigned To, Work Unit, Tenure Track, Credentials, Last Modified Date, Last Modified ctcLink ID");

				for(Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++){
					objSW.WriteLine("\"" + objDS.Tables[0].Rows[intDSRow]["EMPLID"] + "\",\"" + objDS.Tables[0].Rows[intDSRow]["Name"] + 
						"\",\"" + objDS.Tables[0].Rows[intDSRow]["Assignment"] + "\",\"" + objDS.Tables[0].Rows[intDSRow]["WorkUnit"] +
						"\",\"" + objDS.Tables[0].Rows[intDSRow]["TenureTrack"] + "\",\"" + objDS.Tables[0].Rows[intDSRow]["Description"] +
						"\",\"" + objDS.Tables[0].Rows[intDSRow]["LastModifiedDate"] + "\",\"" + objDS.Tables[0].Rows[intDSRow]["LastModifiedEMPLID"] + "\"");
				}

				objSW.Close();
				return "../ExcelCredentials.csv";

			}finally{
				objCon.Close();
			}
		}


		/********************************************** EditSID ************************************************
		 * INPUT:
		 * strOldSID - Old Instructor SID
		 * strNewSID - New Instructor SID
		 * 
		 * OUTPUT: bool
		 *		   true - SID Update Was Successful
		 *		   false - SID Update Failed
		 * 
		 * USED IN: /iCatalog/cred/updatesid.aspx/cs 
		 * 
		 * DESCRIPTION: Updates an Instructor's SID.
		 * 
		 *******************************************************************************************************/
		public bool EditSID(String strOldSID, String strNewSID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "UPDATE Credential " +
					"SET EMPLID = '" + strNewSID + "' " +
					"WHERE EMPLID='" + strOldSID + "';";

				objCmd = new SqlCommand(strSQL, objCon);
				
				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();

				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		public DataSet GetCredentials(String strLtr, String credentialWorkUnit, String strCredSearch){

			objCon = new SqlConnection(strConnection);

			try{

				String strCriteria = "";
				if(strLtr.Trim() != ""){
					strCriteria += "AND Name LIKE '" + strLtr + "%' ";
				}
				if(credentialWorkUnit != ""){
					strCriteria += "AND WorkUnit = '" + credentialWorkUnit + "' ";
				}
				if(strCredSearch.Trim() != ""){
					strCriteria += "AND Description LIKE '%" + strCredSearch + "%' ";
				}

				if(strCriteria != ""){
					strCriteria = "WHERE" + strCriteria.Substring(3);
				}

				objCon.Open();

				strSQL = "SELECT EMPLID, Name, WorkUnit, Assignment, TenureTrack, Description " +
					"FROM Credential " +
					strCriteria + 
					"ORDER BY Name;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}

		public String ExportCredToExcel(String strLtr, String credentialWorkUnit, String strCredSearch){

			objCon = new SqlConnection(strConnection);

			try{

				String strCriteria = "";
				if(strLtr.Trim() != ""){
					strCriteria += "AND Name LIKE '" + strLtr + "%' ";
				}
				if(credentialWorkUnit != ""){
					strCriteria += "AND WorkUnit = '" + credentialWorkUnit + "' ";
				}
				if(strCredSearch.Trim() != ""){
					strCriteria += "AND Description LIKE '%" + strCredSearch + "%' ";
				}
				if(strCriteria != ""){
					strCriteria = "WHERE" + strCriteria.Substring(3);
				}

				objCon.Open();

				strSQL = "SELECT EMPLID, Name, WorkUnit, Assignment, TenureTrack, Description, LastModifiedDate, LastModifiedEMPLID " +
					"FROM Credential " +
					strCriteria + 
					"ORDER BY Name;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				StreamWriter objSW = File.CreateText(HttpContext.Current.Server.MapPath("../ExcelCredentials.csv"));

                objSW.WriteLine("ctcLink ID, Name, Assigned To, Work Unit, Tenure Track, Credentials, Last Modified Date, Last Modified ctcLink ID");

				for(Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++){
					objSW.WriteLine("\"" + objDS.Tables[0].Rows[intDSRow]["EMPLID"] + "\",\"" + objDS.Tables[0].Rows[intDSRow]["Name"] + 
						"\",\"" + objDS.Tables[0].Rows[intDSRow]["Assignment"] + "\",\"" + objDS.Tables[0].Rows[intDSRow]["WorkUnit"] +
						"\",\"" + objDS.Tables[0].Rows[intDSRow]["TenureTrack"] + "\",\"" + objDS.Tables[0].Rows[intDSRow]["Description"] +
						"\",\"" + objDS.Tables[0].Rows[intDSRow]["LastModifiedDate"] + "\",\"" + objDS.Tables[0].Rows[intDSRow]["LastModifiedEMPLID"] + "\"");
				}

				objSW.Close();
				return "../ExcelCredentials.csv";

			}finally{
				objCon.Close();
			}
		}


		public String ExportCredentialsToGraphics(){

			objCon = new SqlConnection(strConnection);

			try{

				StreamWriter objSW = new StreamWriter(HttpContext.Current.Server.MapPath("../Credentials.txt"),false,System.Text.Encoding.GetEncoding("utf-8"));

				objCon.Open();

				//get District Administration
				strSQL = "SELECT * FROM Credential " +
						 "WHERE WorkUnit LIKE '%DIST%' " +
						 "AND (TenureTrack = 'Executive' OR TenureTrack = 'Administrator') " +
						 "ORDER BY TenureTrack DESC, Name;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				objSW.WriteLine("<pstyle:Employee-head>District Administration");
				for(Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++){
					objSW.WriteLine("<pstyle:Employee>" + objDS.Tables[0].Rows[intDSRow]["Name"].ToString());
					objSW.WriteLine("<pstyle:Title><cstyle:Character Style 1>" + objDS.Tables[0].Rows[intDSRow]["WorkUnit"].ToString() + ", " + objDS.Tables[0].Rows[intDSRow]["Assignment"].ToString() + "<cstyle:>");
					objSW.WriteLine("<pstyle:Employee-discrip><cstyle:Character Style 1><ct:Light>" + objDS.Tables[0].Rows[intDSRow]["Description"].ToString() + "<ct:><cstyle:>");
				}

				//get SCC Administration
				strSQL = "SELECT * FROM Credential " +
						 "WHERE WorkUnit LIKE '%SCC%' " +
						 "AND (TenureTrack = 'Executive' OR TenureTrack = 'Administrator') " +
						 "ORDER BY TenureTrack DESC, Name;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				objSW.WriteLine("<pstyle:Employee-head>SCC Administration");
				for(Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++){
					objSW.WriteLine("<pstyle:Employee>" + objDS.Tables[0].Rows[intDSRow]["Name"].ToString());
					objSW.WriteLine("<pstyle:Title><cstyle:Character Style 1>" + objDS.Tables[0].Rows[intDSRow]["WorkUnit"].ToString() + ", " + objDS.Tables[0].Rows[intDSRow]["Assignment"].ToString() + "<cstyle:>");
					objSW.WriteLine("<pstyle:Employee-discrip><cstyle:Character Style 1><ct:Light>" + objDS.Tables[0].Rows[intDSRow]["Description"].ToString() + "<ct:><cstyle:>");
				}

				//get SFCC Administration
				strSQL = "SELECT * FROM Credential " +
					"WHERE WorkUnit LIKE '%SFCC%' " +
					"AND (TenureTrack = 'Executive' OR TenureTrack = 'Administrator') " +
					"ORDER BY TenureTrack DESC, Name;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				objSW.WriteLine("<pstyle:Employee-head>SFCC Administration");
				for(Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++){
					objSW.WriteLine("<pstyle:Employee>" + objDS.Tables[0].Rows[intDSRow]["Name"].ToString());
					objSW.WriteLine("<pstyle:Title><cstyle:Character Style 1>" + objDS.Tables[0].Rows[intDSRow]["WorkUnit"].ToString() + ", " + objDS.Tables[0].Rows[intDSRow]["Assignment"].ToString() + "<cstyle:>");
					objSW.WriteLine("<pstyle:Employee-discrip><cstyle:Character Style 1><ct:Light>" + objDS.Tables[0].Rows[intDSRow]["Description"].ToString() + "<ct:><cstyle:>");
				}

				//get IEL Administration
				strSQL = "SELECT * FROM Credential " +
					"WHERE WorkUnit LIKE '%IEL%' " +
					"AND (TenureTrack = 'Executive' OR TenureTrack = 'Administrator') " +
					"ORDER BY TenureTrack DESC, Name;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				objSW.WriteLine("<pstyle:Employee-head>IEL Administration");
				for(Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++){
					objSW.WriteLine("<pstyle:Employee>" + objDS.Tables[0].Rows[intDSRow]["Name"].ToString());
					objSW.WriteLine("<pstyle:Title><cstyle:Character Style 1>" + objDS.Tables[0].Rows[intDSRow]["WorkUnit"].ToString() + ", " + objDS.Tables[0].Rows[intDSRow]["Assignment"].ToString() + "<cstyle:>");
					objSW.WriteLine("<pstyle:Employee-discrip><cstyle:Character Style 1><ct:Light>" + objDS.Tables[0].Rows[intDSRow]["Description"].ToString() + "<ct:><cstyle:>");
				}

				//get Academic Employees
				strSQL = "SELECT * FROM Credential " +
						 "WHERE TenureTrack <> 'Executive' AND TenureTrack <> 'Administrator' " +
						 "ORDER BY Name;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				objSW.WriteLine("<pstyle:Employee-head>Academic Employees");
				for(Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++){
					objSW.WriteLine("<pstyle:Employee>" + objDS.Tables[0].Rows[intDSRow]["Name"].ToString());
					objSW.WriteLine("<pstyle:Title><cstyle:Character Style 1>" + objDS.Tables[0].Rows[intDSRow]["WorkUnit"].ToString() + ", " + objDS.Tables[0].Rows[intDSRow]["Assignment"].ToString() + "<cstyle:>");
					objSW.WriteLine("<pstyle:Employee-discrip><cstyle:Character Style 1><ct:Light>" + objDS.Tables[0].Rows[intDSRow]["Description"].ToString() + "<ct:><cstyle:>");
				}

				objSW.Close();
				return "../Credentials.txt";

			}finally{
				objCon.Close();
			}
		}
	}
}
