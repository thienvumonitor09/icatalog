using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.course
{
	/// <summary>
	/// Summary description for proof.
	/// </summary>
	public partial class proof : System.Web.UI.Page
	{

		courseData csCourse = new courseData();
		termData csTerm = new termData();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			if(!IsPostBack){
				cboSubject.Items.Add("ALL");
				DataSet dsSubjects = csCourse.GetSubjects();
				for(Int32 intDSRow = 0; intDSRow < dsSubjects.Tables[0].Rows.Count; intDSRow++){
					String subject = dsSubjects.Tables[0].Rows[intDSRow]["SUBJECT"].ToString();
					if(subject.IndexOf("&") < 0){
						cboSubject.Items.Add(subject);
					}
				}

				DataSet dsTerms = csCourse.GetTermsForCourses();
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
					cboTerm.Items.Add(new ListItem(dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString(), dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString()));
				}

				try{
					cboTerm.SelectedValue = csTerm.GetCurrentTerm();
				}catch{
					//do nothing
				}

				panProof.Visible = false;
			}

            //create and populate the college checkboxes
            programData csProgramData = new programData();
            DataSet dsCollegeList = csProgramData.GetCollegeList();
            panCollege.Controls.Clear();
            for (Int32 intDSRow = 0; intDSRow < dsCollegeList.Tables[0].Rows.Count; intDSRow++)
            {
                String collegeShortTitle = dsCollegeList.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
                String collegeID = dsCollegeList.Tables[0].Rows[intDSRow]["CollegeID"].ToString();

                HtmlInputCheckBox chkOfferedAt = new HtmlInputCheckBox();
                chkOfferedAt.Value = collegeID;
                chkOfferedAt.ID = "chk" + collegeID;
                chkOfferedAt.Name = "chkOfferedAt";

                String strSelectedCollege = Request.Form[chkOfferedAt.ID];

                if (strSelectedCollege != null && strSelectedCollege != "")
                {
                    chkOfferedAt.Checked = true;
                }

                chkOfferedAt.Attributes["onclick"] = "addCollege('" + chkOfferedAt.ID + "','" + collegeID + "');";
                panCollege.Controls.Add(new LiteralControl("&nbsp;"));
                panCollege.Controls.Add(chkOfferedAt);
                panCollege.Controls.Add(new LiteralControl(collegeShortTitle));
            }
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			DataSet dsCourses = csCourse.GetCourseDescriptions(cboTerm.SelectedValue, cboSubject.SelectedValue, hidCollegeList.Value, cboOrderBy.SelectedValue, cboStatus.SelectedValue);
			Int32 intRowCtr = dsCourses.Tables[0].Rows.Count, intCrsASN = 0;
			bool blnClearString = true, blnMoveNext = true, blnWriteFile = true;
			String courseLongTitle = "", courseLongDescription = "", strField = "", catalogNbrs = "", variableUnits = "", strOfferedAt = "", strCrsCredits = "", subjectDescription = "";
			Double unitsMinimum = 0.0, unitsMaximum = 0.0;
			Table tblProof = new Table();
			tblProof.CellPadding = 2;
			tblProof.CellSpacing = 0;
			TableRow tr = new TableRow();
			TableCell td = new TableCell();

			if(chkIncludeDesc.Checked){
				strField = ", A.DESCRLONG";
			}

			if(intRowCtr > 0){
				for(Int32 intDSRow = 0; intDSRow < dsCourses.Tables[0].Rows.Count; intDSRow++){
                    //get course colleges
                    Int32 dsCourseID = Convert.ToInt32(dsCourses.Tables[0].Rows[intDSRow]["CourseID"]);
                    DataSet dsCourseCollege = csCourse.GetCourseCollege(dsCourseID);
                    strOfferedAt = "";
                    for (Int32 i = 0; i < dsCourseCollege.Tables[0].Rows.Count; i++)
                    {
                        strOfferedAt += ", " + dsCourseCollege.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
                    }
                    strOfferedAt = "(" + strOfferedAt.Substring(2) + ")";

					//If a title and description have not been stored or have been cleared, store the current record values
					if(courseLongTitle.Length == 0 && courseLongDescription.Length == 0){
						courseLongTitle = dsCourses.Tables[0].Rows[intDSRow]["COURSE_TITLE_LONG"].ToString();

						//if user chose to include description
						if(strField != ""){
							courseLongDescription = dsCourses.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString();
						}
					}

					//------------------ determine where the course is offered used to be here ---------------------

					//if the course is not a duplicate record, the DescriptionCourseID is not null, and the course numbers aren't the same
					//check to see if a string of course numbers exist
					if(intCrsASN != dsCourseID && dsCourses.Tables[0].Rows[intDSRow]["DescriptionCourseID"].ToString() != "" && dsCourses.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() != dsCourses.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString()){

						//if a string of course numbers does not exist
						//start a new string and store all other values needed
						//blnWriteFile is set to false until the record stops repeating
						if(catalogNbrs == ""){
							catalogNbrs = dsCourses.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() + ", " + dsCourses.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString();
							if(strField != ""){
								courseLongDescription = dsCourses.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString();
							}
							courseLongTitle = dsCourses.Tables[0].Rows[intDSRow]["COURSE_TITLE_LONG"].ToString();
							variableUnits = dsCourses.Tables[0].Rows[intDSRow]["VariableUnits"].ToString();
							unitsMinimum = Convert.ToDouble(dsCourses.Tables[0].Rows[intDSRow]["UNITS_MINIMUM"]);
							unitsMaximum = Convert.ToDouble(dsCourses.Tables[0].Rows[intDSRow]["UNITS_MAXIMUM"]);
							blnWriteFile = false;

							//if a string of course numbers does exist, a new course has started
							//assign true to blnWriteFile to write the courses using the previous course as the source
							//assign false to blnClearString which is later used to concatinate course numbers using the current course as the source
						}else{
							blnWriteFile = true;
							blnClearString = false;
							blnMoveNext = true;
						}

						//if the course is a duplicate record, the DescriptionCourseID is not null, and the course numbers aren't the same
						//concatinate the course number to the existing course number string
						//blnWriteFile is set to false until the record stops repeating
					}else if(intCrsASN == Convert.ToInt32(dsCourses.Tables[0].Rows[intDSRow]["CourseID"]) && dsCourses.Tables[0].Rows[intDSRow]["DescriptionCourseID"].ToString() != "" && dsCourses.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() != dsCourses.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString()){
						if(catalogNbrs.Substring(catalogNbrs.Length - 3) != dsCourses.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString()){
							catalogNbrs += ", " + dsCourses.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString();
						}
						blnWriteFile = false;
						blnMoveNext = true;

						//the course is not a duplicate record, assign true to blnWriteFile
						//assign true to blnClearString so the new course values can be stored in courseLongTitle, courseLongDescription, and catalogNbrs
					}else{
						blnWriteFile = true;
						blnClearString = true;

						//if a string of course numbers exist assign false to blnMoveNext so the recordset stays on the current record
						//which will be used in the next loop to store the new course values
						if(catalogNbrs.Length > 3){
							blnMoveNext = false;
							//if a string of course numbers does not exist store the current course values used for the credits and the course number
						}else{
							catalogNbrs = dsCourses.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString();
							variableUnits = dsCourses.Tables[0].Rows[intDSRow]["VariableUnits"].ToString();
							unitsMinimum = Convert.ToDouble(dsCourses.Tables[0].Rows[intDSRow]["UNITS_MINIMUM"]);
							unitsMaximum = Convert.ToDouble(dsCourses.Tables[0].Rows[intDSRow]["UNITS_MAXIMUM"]);
							blnMoveNext = true;
						}
					}
					
					//if a string of course numbers does exist create the string of credits to write to the file
					//for multiple courses using the same source
					if(catalogNbrs.Length > 3){
						if(variableUnits == "Y"){
							strCrsCredits = " (" + unitsMinimum + "-" + unitsMaximum + " cr ea)";
						}else{
							strCrsCredits = " (" + unitsMaximum + " cr ea)";
						}

						//if a string of course numbers does not exist create the string of credits to write to the file
						//for a single course
					}else{
						if(variableUnits == "Y"){
							strCrsCredits = " (" + unitsMinimum + "-" + unitsMaximum + " cr)";
						}else{
							strCrsCredits = " (" + unitsMaximum + " cr)";
						}
					}

					//if a new course has been started in the loop assign the PK to the intCrsASN variable used
					//in the conditions above to create the string of course numbers
					
					if(intCrsASN != Convert.ToInt32(dsCourses.Tables[0].Rows[intDSRow]["CourseID"])){
						intCrsASN = Convert.ToInt32(dsCourses.Tables[0].Rows[intDSRow]["CourseID"]);
					}

					//write the file
					if(blnWriteFile){
			
						//if a new subject is started in the loop write the subject description to the file
						if(subjectDescription != dsCourses.Tables[0].Rows[intDSRow]["DESCR"].ToString()){
							subjectDescription = dsCourses.Tables[0].Rows[intDSRow]["DESCR"].ToString();
							
							tr = new TableRow();

							td = new TableCell();
							td.ColumnSpan = 2;
							td.Attributes["style"] = "font-size:13pt;font-family:arial;text-align:left;";
							td.Controls.Add(new LiteralControl("<br /><br /><b><u>" + subjectDescription + "</u></b>"));

							tr.Cells.Add(td);
							tblProof.Rows.Add(tr);
							tdProof.Controls.Add(tblProof);
						}

						tblProof = new Table();
						
						tr = new TableRow();

						td = new TableCell();
						td.Attributes["style"] = "font-size:10pt;font-family:arial;text-align:left;";
						td.Controls.Add(new LiteralControl("<br /><b>" + dsCourses.Tables[0].Rows[intDSRow]["SUBJECT"] + " " + catalogNbrs + "</b> - "));
						
						tr.Cells.Add(td);

						td = new TableCell();
						td.Attributes["style"] = "font-size:10pt;font-family:arial;text-align:left;";
						td.Controls.Add(new LiteralControl("<br />" + courseLongTitle + strCrsCredits));

						tr.Cells.Add(td);
						tblProof.Rows.Add(tr);

						tr = new TableRow();
						td = new TableCell();	
						tr.Cells.Add(td);

						td = new TableCell();
						td.Attributes["style"] = "width:600px;font-size:10pt;font-family:arial;text-align:left;";
						td.Controls.Add(new LiteralControl(courseLongDescription + " " + strOfferedAt));

						tr.Cells.Add(td);
						tblProof.Rows.Add(tr);	
					
						tdProof.Controls.Add(tblProof);

						//clear the current course values so new values can be stored in the next loop
						if(blnClearString){
							courseLongTitle = "";
							courseLongDescription = "";
							catalogNbrs = "";

							//store the current course values to be written in the next loop
						}else{
							catalogNbrs = dsCourses.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() + ", " + dsCourses.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString();

							//if user chose to include description
							if(strField != ""){
								courseLongDescription = dsCourses.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString();
							}

							courseLongTitle = dsCourses.Tables[0].Rows[intDSRow]["COURSE_TITLE_LONG"].ToString();
							variableUnits = dsCourses.Tables[0].Rows[intDSRow]["VariableUnits"].ToString();
							unitsMinimum = Convert.ToDouble(dsCourses.Tables[0].Rows[intDSRow]["UNITS_MINIMUM"]);
							unitsMaximum = Convert.ToDouble(dsCourses.Tables[0].Rows[intDSRow]["UNITS_MAXIMUM"]);
						}
					}

					if(!blnMoveNext){
						intDSRow = intDSRow - 1;
					}
				}
			}else{
				
				tr = new TableRow();
				td = new TableCell();
				td.Attributes["style"] = "text-align:left;";
				td.Controls.Add(new LiteralControl("<br /><br /><b>No records were found that match your criteria.</b>"));
				tr.Cells.Add(td);
				tblProof.Rows.Add(tr);

				tdProof.Controls.Add(tblProof);
			}

			tr = new TableRow();

			td = new TableCell();
			td.Attributes["style"] = "font-size:10pt;font-family:arial;text-align:left;";
			td.Controls.Add(new LiteralControl("<br /><a href=\"#\" onclick=\"history.back(1);\">Go Back</a>"));
			
			tr.Cells.Add(td);

			tblProof.Rows.Add(tr);

			panProof.Visible = true;
			container.Visible = false;

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
