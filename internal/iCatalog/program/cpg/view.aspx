<%@ Page language="c#" Inherits="ICatalog.program.cpg.view" CodeFile="view.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>View Career Planning Guide</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/CPG.css" />
		<script type="text/javascript">
        <!--
            /*
			function launchCenter(url, name, height, width) {
				var str = "height=" + height + ",innerHeight=" + height;
				str += ",width=" + width + ",innerWidth=" + width;
				if (window.screen) {
					var ah = screen.availHeight - 30;
					var aw = screen.availWidth - 10;

					var xc = (aw - width) / 2;
					var yc = (ah - height) / 2;

					str += ",left=" + xc + ",screenX=" + xc;
					str += ",top=" + yc + ",screenY=" + yc;
					str += ",resizable=yes,scrollbars=yes";
				}
				return window.open(url, name, str);
            }
            */

            function popupCenter(url, title, h, w) {
                // Fixes dual-screen position  
                var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                var top = ((height / 2) - (h / 2)) + dualScreenTop;
                var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

                // Puts focus on the newWindow  
                if (window.focus) {
                    newWindow.focus();
                }
            }
		//-->
		</script>
	</head>
	<body>
		<asp:panel id="panView" runat="server">
	        <script type="text/javascript">
			<!--
				document.body.style.backgroundColor = "#ffffff";
			    document.body.style.backgroundImage = "url('')";
			    function window.onbeforeprint(){
                    document.getElementById("buttons").style.display = "none";
                }
                function window.onafterprint(){
                    document.getElementById("buttons").style.display = "block";
                }
			//-->
			</script>
			<div style="width:660px;margin-left:auto;margin-right:auto" id="buttons" runat="server"></div>
	        <table style="margin-left:auto;margin-right:auto" cellpadding="0" cellspacing="0">
				<tr>
					<td style="background:#000000;">
						<asp:table id="tblCPGFront" runat="server" cellpadding="6" cellspacing="1" style="width:660px;height:910px;"></asp:table>
					</td>
				</tr>
			</table>
			<br class="breakhere" />
			<table style="margin-left:auto;margin-right:auto" cellpadding="0" cellspacing="0">
				<tr>
					<td style="background:#000000;">
						<asp:table id="tblCPGBack" runat="server" cellpadding="8" cellspacing="1" style="width:660px;"></asp:table>
					</td>
				</tr>
			</table>
			<asp:panel id="panCPGBack2" runat="server">
				<br class="breakhere" />
				<table style="margin-left:auto;margin-right:auto" cellpadding="0" cellspacing="0">
					<tr>
						<td style="background:#000000;">
							<asp:table id="tblCPGBack2" runat="server" cellpadding="8" cellspacing="1" style="width:660px;"></asp:table>
						</td>
					</tr>
				</table>
			</asp:panel>
			<DIV class="clearer"></DIV>
		</asp:panel>
	</body>
</html>

