USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramsByCategory]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/26/17
-- Description:	Returns a list of programs ordered by category
--		 INPUT: CollegeID (optional) - College primary key
--				STRM (optional) - Term Code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramsByCategory] 
	@CollegeID int = NULL,
	@STRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)

    SELECT CategoryTitle, ProgramCategory.CategoryID, ProgramTitle, Program.ProgramVersionID, CollegeShortTitle
    FROM Program
    LEFT OUTER JOIN College 
    ON College.CollegeID = Program.CollegeID 
    INNER JOIN ProgramCategory 
    ON ProgramCategory.ProgramVersionID = Program.ProgramVersionID 
    INNER JOIN Category 
    ON Category.CategoryID = ProgramCategory.CategoryID 
    WHERE ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM) 
    AND (@CollegeID IS NULL OR Program.CollegeID = @CollegeID)
    AND PublishedProgram = 1 
    ORDER BY CategoryTitle, ProgramTitle, CollegeShortTitle;
END

GO
