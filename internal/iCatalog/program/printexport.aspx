﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="printexport.aspx.cs" Inherits="program_printexport" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
    <head>
        <title>Export to Printable Catalog</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <script type="text/javascript">
        <!--
            function validate() {
                var cboProgram = document.getElementById("cboProgram");
                if (cboProgram.value == "0") {
                    alert(cboProgram.options[cboProgram.selectedIndex].text + "\nPlease make a different selection.");
                    return false;
                } else {
                    return true;
                }
            }
        //-->
        </script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmCourse" runat="server">
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Export to Printable Catalog</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                        <!-- TERM -->
                                                                        <tr>
                                                                            <td class="portletSecondary" style="text-align:right;width:38%">
                                                                                <b>Term:&nbsp;</b>
                                                                            </td>
                                                                            <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;width:62%">
                                                                                <asp:dropdownlist runat="server" id="cboTerm" cssclass="small" AutoPostBack="True"></asp:dropdownlist>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- CAMPUS -->
                                                                        <tr>
                                                                            <td class="portletSecondary" style="text-align:right;">
                                                                                <b>Offered by:&nbsp;</b>
                                                                            </td>
                                                                            <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                <asp:dropdownlist runat="server" id="cboCollege" cssclass="small" AutoPostBack="True"></asp:dropdownlist>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- BUTTON -->
                                                                        <tr>
														                    <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                    <asp:button id="cmdSubmit" runat="server" text="Export to Printable Catalog" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:155px;" onclientclick="return validate();" onclick="cmdSubmit_Click"></asp:button>
														                    </td>
													                    </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <div class="clearer"></div>
        </asp:panel>
    </body>
</html>
