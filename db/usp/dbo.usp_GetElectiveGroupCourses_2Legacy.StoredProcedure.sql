USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetElectiveGroupCourses_2Legacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/22/2019
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetElectiveGroupCourses_2Legacy]
	@BeginSTRM varchar(4)
	, @EndSTRM varchar(4)
	, @OptionElectiveGroupID int 
	, @CurrentSTRM varchar(4) 
AS
BEGIN
	SET NOCOUNT ON;
	IF @EndSTRM < @CurrentSTRM /* ARCHIVED PROGRAM */
		/*display the last published version of the course in the program strm span */
		SELECT OptionElectiveGroupID, ElectiveGroupCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseSuffix
			, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber
			, ElectiveGroupCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum 
		FROM ElectiveGroupCourse INNER JOIN Course 
		ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering 
		AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999')) 
		OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM AND (CourseEndSTRM <= @EndSTRM OR CourseEndSTRM = 'Z999'))) 
		LEFT OUTER JOIN OptionFootnote 
		ON OptionFootnote.OptionFootnoteID = ElectiveGroupCourse.OptionFootnoteID 
		WHERE OptionElectiveGroupID = @OptionElectiveGroupID
		ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
	ELSE 
		BEGIN
			IF @BeginSTRM > @CurrentSTRM /* FUTURE PROGRAM */
				/*display the first published version of the course in the program strm span*/
				SELECT OptionElectiveGroupID, ElectiveGroupCourse.CourseOffering, SUBJECT
					, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM
					, FootnoteNumber, ElectiveGroupCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
				FROM ElectiveGroupCourse INNER JOIN Course  
				ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering  
				AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999'))  
				OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM AND (CourseEndSTRM <= @EndSTRM OR CourseEndSTRM = 'Z999')))  
				LEFT OUTER JOIN OptionFootnote  
				ON OptionFootnote.OptionFootnoteID = ElectiveGroupCourse.OptionFootnoteID  
				WHERE OptionElectiveGroupID = @OptionElectiveGroupID
				ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR; 
			ELSE /* CURRENT PROGRAM */
				/*display the most current published version of the course in the program strm span*/
				SELECT OptionElectiveGroupID, ElectiveGroupCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM
					, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber, ElectiveGroupCourse.OptionFootnoteID
					, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum 
				FROM ElectiveGroupCourse INNER JOIN Course 
				ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering 
				AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999')) 
				OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM AND (CourseEndSTRM <= @EndSTRM OR CourseEndSTRM = 'Z999'))) 
				AND (CourseBeginSTRM <= @CurrentSTRM AND (CourseEndSTRM >= @CurrentSTRM OR CourseEndSTRM = 'Z999')) 
				LEFT OUTER JOIN OptionFootnote 
				ON OptionFootnote.OptionFootnoteID = ElectiveGroupCourse.OptionFootnoteID 
				WHERE OptionElectiveGroupID = @OptionElectiveGroupID
				ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
		END				


END
GO
