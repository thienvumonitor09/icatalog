USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptions]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/11/17
-- Description:	Returns program degree options
--		 INPUT: ProgramDegreeID - Program degree primary key 
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptions] 
	@ProgramDegreeID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT OptionTitle, OptionDescription, OptionID, GainfulEmploymentID
    FROM ProgramDegreeOption
    WHERE ProgramDegreeID = @ProgramDegreeID
    ORDER BY OptionTitle;
END

GO
