USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDegreeProgramsByCategory]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 06/12/2019
-- Description:	Returns a degree list by term, location, degree, category, course subject, and course catalog number
--		 INPUT: STRM - Term code
--				LocationID (optional) - Location primary key 
--				DegreeID (optional) - Degree primary key 
--				CategoryID (optional) - Category primary key 
--				SUBJECT (optional) - Course subject area 
--				CATALOG_NBR (optional) - Course catalog number 
-- Changes:
--			06/14/2019 - Remove "GROUP BY" because UNION will remove all dups - Vu
--			06/17/2019 - Seperate OR in WHERE - Vu
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDegreeProgramsByCategory] 
	@STRM varchar(4) = NULL,
	@LocationID int = NULL,
	@DegreeID int = NULL,
	@CategoryID int = NULL,
	@SUBJECT varchar(5) = NULL,
	@CATALOG_NBR varchar(3) = NULL,
	@GlobalSearch varchar(150) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	DECLARE
		@local_STRM varchar(4) = @STRM,
		@local_LocationID int = @LocationID,
		@local_DegreeID int = @DegreeID,
		@local_CategoryID int = @CategoryID,
		@local_SUBJECT varchar(5) = @SUBJECT,
		@local_CATALOG_NBR varchar(3) = @CATALOG_NBR,
		@local_GlobalSearch varchar(150) = @GlobalSearch;
   
	IF @local_STRM IS NULL
		SET @local_STRM = (SELECT STRM FROM udf_GetCurrentSTRM())
	
	;WITH tmp (CourseOffering, SUBJECT, CATALOG_NBR, EFFDT, COURSE_TITLE_LONG, DESCR)
	AS 
	(
		SELECT ( CAST(REPLACE(SUBJECT,' ','') AS CHAR(5)) + CAST(REPLACE(CATALOG_NBR,' ','') AS CHAR(3)) ) AS CourseOffering
					, REPLACE(SUBJECT,' ',''), REPLACE(CATALOG_NBR,' ',''), CourseOffering.EFFDT , COURSE_TITLE_LONG, DESCR
		FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
		LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
		ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
		WHERE CATALOG_PRINT = 'Y' AND EFF_STATUS = 'A'
	)

	SELECT CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, Degree.DegreeID, Degree.DocumentTitle, DegreeType
	FROM ProgramDegree 
	INNER JOIN Program 
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
	INNER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID
	INNER JOIN ProgramCategory 
	ON ProgramCategory.ProgramVersionID = Program.ProgramVersionID 
	INNER JOIN Category 
	ON Category.CategoryID = ProgramCategory.CategoryID 
	INNER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	INNER JOIN Degree 
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN tmp
	ON tmp.CourseOffering  = OptionPrerequisite.CourseOffering /* DIFFERENT HERE*/
	LEFT OUTER JOIN OptionLocation 
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 
	WHERE ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM) 
	AND PublishedProgram = 1
	AND
		(
			@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
		)
	AND
		(
			@local_DegreeID IS NULL OR OptionLocation.LocationID = @local_LocationID
		)
	AND 
		(
			@local_CategoryID IS NULL OR ProgramDegree.DegreeID = @local_DegreeID
		)
	AND 
		(
			@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
		)
	AND
		(
			@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
		)
	AND
		(
			@local_GlobalSearch IS NULL
			OR
			( 
				--((LocationTitle +CategoryTitle + ProgramTitle + ProgramDescription + OptionTitle + DegreeShortTitle + DegreeLongTitle + DegreeType) LIKE '%' + @local_GlobalSearch +'%')				
				( LocationTitle LIKE '%' + @GlobalSearch +'%')
				OR (ProgramTitle LIKE '%' + @GlobalSearch + '%')
				OR (ProgramDescription LIKE '%' + @GlobalSearch + '%')
				OR (OptionTitle LIKE '%' + @GlobalSearch + '%')
				OR (DegreeShortTitle LIKE '%' + @GlobalSearch + '%')
				OR (DegreeLongTitle LIKE '%' + @GlobalSearch + '%')
				OR (DegreeType LIKE '%' + @GlobalSearch + '%') 	
				OR ( (tmp.COURSE_TITLE_LONG LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
				OR ((tmp.DESCR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.SUBJECT LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.CATALOG_NBR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + LTRIM(tmp.CATALOG_NBR)) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR (((tmp.SUBJECT + ' ' + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			) 
		)
	--GROUP BY Program.ProgramVersionID, CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, Degree.DegreeID, Degree.DocumentTitle, DegreeType 
	--ORDER BY CategoryTitle, DegreeLongTitle, OptionTitle, ProgramTitle, CollegeShortTitle;

	UNION

	SELECT CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, Degree.DegreeID, Degree.DocumentTitle, DegreeType
	FROM ProgramDegree 
	INNER JOIN Program 
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
	INNER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID
	INNER JOIN ProgramCategory 
	ON ProgramCategory.ProgramVersionID = Program.ProgramVersionID 
	INNER JOIN Category 
	ON Category.CategoryID = ProgramCategory.CategoryID 
	INNER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	INNER JOIN Degree 
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN tmp
	ON  tmp.CourseOffering  = ElectiveGroupCourse.CourseOffering /* DIFFERENT HERE*/
	LEFT OUTER JOIN OptionLocation  
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 
	WHERE ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM) 
	AND PublishedProgram = 1
	AND
		(
			@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
		)
	AND
		(
			@local_DegreeID IS NULL OR OptionLocation.LocationID = @local_LocationID
		)
	AND 
		(
			@local_CategoryID IS NULL OR ProgramDegree.DegreeID = @local_DegreeID
		)
	AND 
		(
			@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
		)
	AND
		(
			@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
		)
	AND
		(
			@local_GlobalSearch IS NULL
			OR
			( 
				--((LocationTitle +CategoryTitle + ProgramTitle + ProgramDescription + OptionTitle + DegreeShortTitle + DegreeLongTitle + DegreeType) LIKE '%' + @local_GlobalSearch +'%')			
				( LocationTitle LIKE '%' + @GlobalSearch +'%')
				OR (ProgramTitle LIKE '%' + @GlobalSearch + '%')
				OR (ProgramDescription LIKE '%' + @GlobalSearch + '%')
				OR (OptionTitle LIKE '%' + @GlobalSearch + '%')
				OR (DegreeShortTitle LIKE '%' + @GlobalSearch + '%')
				OR (DegreeLongTitle LIKE '%' + @GlobalSearch + '%')
				OR (DegreeType LIKE '%' + @GlobalSearch + '%') 	
				OR ( (tmp.COURSE_TITLE_LONG LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
				OR ((tmp.DESCR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.SUBJECT LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.CATALOG_NBR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + LTRIM(tmp.CATALOG_NBR)) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR (((tmp.SUBJECT + ' ' + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			) 
		)
	--GROUP BY Program.ProgramVersionID, CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, Degree.DegreeID, Degree.DocumentTitle, DegreeType 
	--ORDER BY CategoryTitle, DegreeLongTitle, OptionTitle, ProgramTitle, CollegeShortTitle;

	UNION

	SELECT CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, Degree.DegreeID, Degree.DocumentTitle, DegreeType
	FROM ProgramDegree 
	INNER JOIN Program 
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
	INNER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID
	INNER JOIN ProgramCategory 
	ON ProgramCategory.ProgramVersionID = Program.ProgramVersionID 
	INNER JOIN Category 
	ON Category.CategoryID = ProgramCategory.CategoryID 
	INNER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	INNER JOIN Degree 
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN tmp
	ON tmp.CourseOffering  = OptionCourse.CourseOffering /* DIFFERENT HERE*/
	LEFT OUTER JOIN OptionLocation 
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 
	WHERE ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM) 
	AND PublishedProgram = 1
	AND
		(
			@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
		)
	AND
		(
			@local_DegreeID IS NULL OR OptionLocation.LocationID = @local_LocationID
		)
	AND 
		(
			@local_CategoryID IS NULL OR ProgramDegree.DegreeID = @local_DegreeID
		)
	AND 
		(
			@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
		)
	AND
		(
			@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
		)
	AND
		(
			@local_GlobalSearch IS NULL
			OR
			( 
				--((LocationTitle +CategoryTitle + ProgramTitle + ProgramDescription + OptionTitle + DegreeShortTitle + DegreeLongTitle + DegreeType) LIKE '%' + @local_GlobalSearch +'%')			
				( LocationTitle LIKE '%' + @GlobalSearch +'%')
				OR (ProgramTitle LIKE '%' + @GlobalSearch + '%')
				OR (ProgramDescription LIKE '%' + @GlobalSearch + '%')
				OR (OptionTitle LIKE '%' + @GlobalSearch + '%')
				OR (DegreeShortTitle LIKE '%' + @GlobalSearch + '%')
				OR (DegreeLongTitle LIKE '%' + @GlobalSearch + '%')
				OR (DegreeType LIKE '%' + @GlobalSearch + '%') 	
				OR( (tmp.COURSE_TITLE_LONG LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
				OR ((tmp.DESCR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.SUBJECT LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.CATALOG_NBR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + LTRIM(tmp.CATALOG_NBR)) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR (((tmp.SUBJECT + ' ' + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))	
			) 
		)
	--GROUP BY Program.ProgramVersionID, CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, Degree.DegreeID, Degree.DocumentTitle, DegreeType 
	ORDER BY CategoryTitle, DegreeLongTitle, OptionTitle, ProgramTitle, CollegeShortTitle;
END
GO
