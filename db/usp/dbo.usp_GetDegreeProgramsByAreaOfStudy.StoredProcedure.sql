USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDegreeProgramsByAreaOfStudy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 06/14/2019
-- Description:	Returns an alphabetic area of study degree list
--		 INPUT: STRM - Term code
--				LocationID (optional) - Location primary key 
--				DegreeID (optional) - Degree primary key 
--				AlphaCharacter (optional) - Letter of the alphabet the program option titles start with
--				AreaOfStudyID (optional) - Area of Study primary key
--				SUBJECT (optional) - Course subject area 
--				CATALOG_NBR (optional) - Course catalog number 
--	  MODIFIED: 06/17/19 Use tmp table and UNION to speed up - Vu
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDegreeProgramsByAreaOfStudy]
	@STRM varchar(4) = NULL,
	@LocationID int = NULL,
	@DegreeID int = NULL,
	@AlphaCharacter char = NULL,
	@AreaOfStudyID int = NULL,
	@SUBJECT varchar(5) = NULL,
	@CATALOG_NBR varchar(3) = NULL,
	@GlobalSearch varchar(150) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	

	IF @STRM IS NULL
		SET @STRM = (SELECT STRM FROM udf_GetCurrentSTRM())

	DECLARE
		@local_STRM varchar(4) = @STRM,
		@local_LocationID int = @LocationID,
		@local_DegreeID int = @DegreeID,
		@local_AlphaCharacter char = @AlphaCharacter,
		@local_AreaOfStudyID int = @AreaOfStudyID,
		@local_SUBJECT varchar(5) = @SUBJECT,
		@local_CATALOG_NBR varchar(3) = @CATALOG_NBR,
		@local_GlobalSearch varchar(150) = @GlobalSearch;

	;WITH tmp (CourseOffering, SUBJECT, CATALOG_NBR, EFFDT, COURSE_TITLE_LONG, DESCR)
	AS 
	(
		SELECT ( CAST(REPLACE(SUBJECT,' ','') AS CHAR(5)) + CAST(REPLACE(CATALOG_NBR,' ','') AS CHAR(3)) ) AS CourseOffering
					, REPLACE(SUBJECT,' ',''), REPLACE(CATALOG_NBR,' ',''), CourseOffering.EFFDT , COURSE_TITLE_LONG, DESCR
		FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
		LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
		ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
		WHERE CATALOG_PRINT = 'Y' AND EFF_STATUS = 'A'
	)
	SELECT AreaOfStudy.Title, AreaOfStudy.WebsiteURL, CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, DocumentTitle, DegreeType
	FROM AreaOfStudy
	INNER JOIN AreaOfStudyProgram
	ON AreaOfStudyProgram.AreaOfStudyID = AreaOfStudy.AreaOfStudyID
	LEFT OUTER JOIN Program
	ON Program.ProgramID = AreaOfStudyProgram.ProgramID
	LEFT OUTER JOIN ProgramDegree
	ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID				
	LEFT OUTER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
	LEFT OUTER JOIN College 
	ON College.CollegeID = Program.CollegeID 
	LEFT OUTER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	LEFT OUTER JOIN Degree
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN tmp
	ON tmp.CourseOffering  = OptionPrerequisite.CourseOffering /* DIFFERENT HERE*/
	LEFT OUTER JOIN OptionLocation 
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 
	WHERE (ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM)) 
		AND PublishedProgram = 1
		AND
			(
				@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
			)
		AND
			(
				@local_DegreeID IS NULL OR ProgramDegree.DegreeID = @local_DegreeID
			)
		AND
			(
				@local_AlphaCharacter IS NULL OR Title LIKE @local_AlphaCharacter + '%'
			)
		AND
			(
				@local_AreaOfStudyID IS NULL OR AreaOfStudyProgram.AreaOfStudyID = @local_AreaOfStudyID
			)
		AND 
			(
				@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
			)
		AND
			(
				@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			)
		AND
		(
			@local_GlobalSearch IS NULL
			OR
			( 
				--((LocationTitle + AreaOfStudy.Title + ProgramTitle + ProgramDescription + OptionTitle + DegreeShortTitle + DegreeLongTitle + DegreeType) LIKE '%' + @local_GlobalSearch +'%')							
				(LocationTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (AreaOfStudy.Title LIKE '%' + @local_GlobalSearch + '%')
				OR (ProgramTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (ProgramDescription LIKE '%' + @local_GlobalSearch + '%')
				OR (OptionTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (DegreeShortTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (DegreeLongTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (DegreeType LIKE '%' + @local_GlobalSearch + '%')
				OR ( (tmp.COURSE_TITLE_LONG LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
				OR ((tmp.DESCR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.SUBJECT LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.CATALOG_NBR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + LTRIM(tmp.CATALOG_NBR)) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR (((tmp.SUBJECT + ' ' + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			) 
		)

	UNION

	SELECT AreaOfStudy.Title, AreaOfStudy.WebsiteURL, CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, DocumentTitle, DegreeType
	FROM AreaOfStudy
	INNER JOIN AreaOfStudyProgram
	ON AreaOfStudyProgram.AreaOfStudyID = AreaOfStudy.AreaOfStudyID
	LEFT OUTER JOIN Program
	ON Program.ProgramID = AreaOfStudyProgram.ProgramID
	LEFT OUTER JOIN ProgramDegree
	ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID				
	LEFT OUTER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
	LEFT OUTER JOIN College 
	ON College.CollegeID = Program.CollegeID 
	LEFT OUTER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	LEFT OUTER JOIN Degree
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN tmp
	ON tmp.CourseOffering  = ElectiveGroupCourse.CourseOffering /* DIFFERENT HERE*/
	LEFT OUTER JOIN OptionLocation 
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 
	WHERE (ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM)) 
		AND PublishedProgram = 1
		AND
			(
				@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
			)
		AND
			(
				@local_DegreeID IS NULL OR ProgramDegree.DegreeID = @local_DegreeID
			)
		AND
			(
				@local_AlphaCharacter IS NULL OR Title LIKE @local_AlphaCharacter + '%'
			)
		AND
			(
				@local_AreaOfStudyID IS NULL OR AreaOfStudyProgram.AreaOfStudyID = @local_AreaOfStudyID
			)
		AND 
			(
				@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
			)
		AND
			(
				@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			)
		AND
		(
			@local_GlobalSearch IS NULL
			OR
			( 
				--((LocationTitle + AreaOfStudy.Title + ProgramTitle + ProgramDescription + OptionTitle + DegreeShortTitle + DegreeLongTitle + DegreeType) LIKE '%' + @local_GlobalSearch +'%')							
				(LocationTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (AreaOfStudy.Title LIKE '%' + @local_GlobalSearch + '%')
				OR (ProgramTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (ProgramDescription LIKE '%' + @local_GlobalSearch + '%')
				OR (OptionTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (DegreeShortTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (DegreeLongTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (DegreeType LIKE '%' + @local_GlobalSearch + '%')
				OR ( (tmp.COURSE_TITLE_LONG LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
				OR ((tmp.DESCR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.SUBJECT LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.CATALOG_NBR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + LTRIM(tmp.CATALOG_NBR)) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR (((tmp.SUBJECT + ' ' + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			) 
		)

	UNION

	SELECT AreaOfStudy.Title, AreaOfStudy.WebsiteURL, CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, DocumentTitle, DegreeType
	FROM AreaOfStudy
	INNER JOIN AreaOfStudyProgram
	ON AreaOfStudyProgram.AreaOfStudyID = AreaOfStudy.AreaOfStudyID
	LEFT OUTER JOIN Program
	ON Program.ProgramID = AreaOfStudyProgram.ProgramID
	LEFT OUTER JOIN ProgramDegree
	ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID				
	LEFT OUTER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
	LEFT OUTER JOIN College 
	ON College.CollegeID = Program.CollegeID 
	LEFT OUTER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	LEFT OUTER JOIN Degree
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN tmp
	ON tmp.CourseOffering  = OptionCourse.CourseOffering /* DIFFERENT HERE*/
	LEFT OUTER JOIN OptionLocation 
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 
	WHERE (ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM)) 
		AND PublishedProgram = 1
		AND
			(
				@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
			)
		AND
			(
				@local_DegreeID IS NULL OR ProgramDegree.DegreeID = @local_DegreeID
			)
		AND
			(
				@local_AlphaCharacter IS NULL OR Title LIKE @local_AlphaCharacter + '%'
			)
		AND
			(
				@local_AreaOfStudyID IS NULL OR AreaOfStudyProgram.AreaOfStudyID = @local_AreaOfStudyID
			)
		AND 
			(
				@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
			)
		AND
			(
				@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			)
		AND
		(
			@local_GlobalSearch IS NULL
			OR
			( 
				--((LocationTitle + AreaOfStudy.Title + ProgramTitle + ProgramDescription + OptionTitle + DegreeShortTitle + DegreeLongTitle + DegreeType) LIKE '%' + @local_GlobalSearch +'%')							
				(LocationTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (AreaOfStudy.Title LIKE '%' + @local_GlobalSearch + '%')
				OR (ProgramTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (ProgramDescription LIKE '%' + @local_GlobalSearch + '%')
				OR (OptionTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (DegreeShortTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (DegreeLongTitle LIKE '%' + @local_GlobalSearch + '%')
				OR (DegreeType LIKE '%' + @local_GlobalSearch + '%')
				OR ( (tmp.COURSE_TITLE_LONG LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
				OR ((tmp.DESCR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.SUBJECT LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( (tmp.CATALOG_NBR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + LTRIM(tmp.CATALOG_NBR)) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR ( ((tmp.SUBJECT + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
				OR (((tmp.SUBJECT + ' ' + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			) 
		)
/*
	DECLARE @SQL nvarchar(max),
			@Parameters nvarchar(4000)
			
	
   SET @SQL = 'SELECT AreaOfStudy.Title, AreaOfStudy.WebsiteURL, CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, DocumentTitle, DegreeType
			FROM AreaOfStudy
			INNER JOIN AreaOfStudyProgram
			ON AreaOfStudyProgram.AreaOfStudyID = AreaOfStudy.AreaOfStudyID
			LEFT OUTER JOIN Program
			ON Program.ProgramID = AreaOfStudyProgram.ProgramID
			LEFT OUTER JOIN ProgramDegree
			ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID				
			LEFT OUTER JOIN CareerPlanningGuide 
			ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
			LEFT OUTER JOIN College 
			ON College.CollegeID = Program.CollegeID 
			LEFT OUTER JOIN ProgramDegreeOption 
			ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
			LEFT OUTER JOIN Degree
			ON Degree.DegreeID = ProgramDegree.DegreeID 
			LEFT OUTER JOIN OptionPrerequisite 
			ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
			LEFT OUTER JOIN OptionElectiveGroup 
			ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
			LEFT OUTER JOIN ElectiveGroupCourse 
			ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
			LEFT OUTER JOIN OptionCourse 
			ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
			LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
			ON REPLACE(SUBJECT+CATALOG_NBR, '' '','''')  = REPLACE(OptionPrerequisite.CourseOffering,'' '','''') 
				OR  REPLACE(SUBJECT+CATALOG_NBR,'' '' ,'''')  = REPLACE(ElectiveGroupCourse.CourseOffering,'' '','''') 
				OR REPLACE(SUBJECT+CATALOG_NBR, '' '' ,'''')  = REPLACE(OptionCourse.CourseOffering,'' '','''')
			LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
			ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
			LEFT OUTER JOIN OptionLocation 
			ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
			LEFT OUTER JOIN Location 
			ON Location.LocationID = OptionLocation.LocationID 
			WHERE (ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = ''Z999'' OR ProgramEndSTRM >= @STRM)) 
			AND PublishedProgram = 1 '
				
	IF @LocationID IS NOT NULL
		SET @SQL += 'AND OptionLocation.LocationID = @LocationID '
		
	IF @DegreeID IS NOT NULL
		SET @SQL += 'AND ProgramDegree.DegreeID = @DegreeID '
		
	IF @AlphaCharacter IS NOT NULL
		SET @SQL += 'AND Title LIKE @AlphaCharacter + ''%'' '
		
	IF @AreaOfStudyID IS NOT NULL
		SET @SQL += 'AND AreaOfStudyProgram.AreaOfStudyID = @AreaOfStudyID '
		
	IF @SUBJECT IS NOT NULL
		SET @SQL += 'AND ( (SUBJECT = @SUBJECT OR SUBJECT = @SUBJECT + ''&'') 
						AND CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM)) ) '
	
	IF @CATALOG_NBR IS NOT NULL	
		SET @SQL += 'AND (CATALOG_NBR = @CATALOG_NBR 
						AND CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM))) '
				 
	IF @GlobalSearch IS NOT NULL
		SET @SQL += 'AND ( 
							(LocationTitle LIKE ''%'' + @GlobalSearch + ''%'')
							OR (AreaOfStudy.Title LIKE ''%'' + @GlobalSearch + ''%'')
							OR (ProgramTitle LIKE ''%'' + @GlobalSearch + ''%'')
							OR (ProgramDescription LIKE ''%'' + @GlobalSearch + ''%'')
							OR (OptionTitle LIKE ''%'' + @GlobalSearch + ''%'')
							OR (DegreeShortTitle LIKE ''%'' + @GlobalSearch + ''%'')
							OR (DegreeLongTitle LIKE ''%'' + @GlobalSearch + ''%'')
							OR (DegreeType LIKE ''%'' + @GlobalSearch + ''%'')
							OR ( (CourseCatalog.COURSE_TITLE_LONG LIKE ''%'' + @GlobalSearch + ''%'')  AND CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM)))
							OR ((CourseCatalog.DESCR LIKE ''%'' + @GlobalSearch + ''%'') AND CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM)))
							OR ( (CourseOffering.SUBJECT LIKE ''%'' + @GlobalSearch + ''%'')  AND CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM)))
							OR ( (CourseOffering.CATALOG_NBR LIKE ''%'' + @GlobalSearch + ''%'') AND CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM)))
							OR ( ((CourseOffering.SUBJECT + LTRIM(CourseOffering.CATALOG_NBR)) LIKE + ''%'' + @GlobalSearch + ''%'') AND CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM)))
							OR ( ((CourseOffering.SUBJECT + CourseOffering.CATALOG_NBR) LIKE + ''%'' + @GlobalSearch + ''%'') AND CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM)))
							OR (((CourseOffering.SUBJECT + '' '' + CourseOffering.CATALOG_NBR) LIKE + ''%'' + @GlobalSearch + ''%'') AND CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM)))
						) '


	
	SET @SQL += 'GROUP BY AreaOfStudy.Title, AreaOfStudy.WebsiteURL, CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, DocumentTitle, DegreeType  
					ORDER BY AreaOfStudy.Title, DegreeLongTitle, OptionTitle, ProgramTitle, CollegeShortTitle;'
				 
	SET @Parameters = '@STRM varchar(4),
						@LocationID int,
						@DegreeID int,
						@AlphaCharacter char,
						@AreaOfStudyID int,
						@SUBJECT varchar(5),
						@CATALOG_NBR varchar(3),
						@GlobalSearch varchar(150)'
						 
	EXEC sp_executesql @SQL, @Parameters, @STRM, @LocationID, @DegreeID, @AlphaCharacter, @AreaOfStudyID, @SUBJECT, @CATALOG_NBR, @GlobalSearch
*/
END
GO
