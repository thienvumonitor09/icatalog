USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDegreeLinks]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 2/5/18
-- Description:	Returns a list of degrees/programs and their website URLS
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDegreeLinks] 
	@STRM varchar(4) = NULL,
	@Institution varchar(5) = NULL,
	@AreaOfStudyID int = NULL,
	@ProgramID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @STRM IS NULL
		SET @STRM = (SELECT MIN(STRM)
					FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					WHERE DATEADD(day,10,TERM_BEGIN_DT) > GETDATE()
					AND TERM_END_DT >= GETDATE())
   
	SELECT DISTINCT CollegeShortTitle, CollegeLongTitle, AreaOfStudy.AreaOfStudyID, AreaOfStudy.Title, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramTitle, ProgramWebsiteURL  
	FROM AreaOfStudy
	INNER JOIN AreaOfStudyProgram
	ON AreaOfStudyProgram.AreaOfStudyID = AreaOfStudy.AreaOfStudyID
	LEFT OUTER JOIN Program
	ON Program.ProgramID = AreaOfStudyProgram.ProgramID
	LEFT OUTER JOIN ProgramDegree
	ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID				
	LEFT OUTER JOIN College 
	ON College.CollegeID = Program.CollegeID 
	LEFT OUTER JOIN Degree
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
	WHERE (ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM)) 
	AND (@Institution IS NULL OR (AreaOfStudy.Institution = @Institution))
	AND (@AreaOfStudyID IS NULL OR (AReaOfStudy.AreaOfStudyID = @AreaOfStudyID))
	AND (@ProgramID IS NULL OR (Program.ProgramID = @ProgramID))
	AND PublishedProgram = 1 
	ORDER BY Title, DegreeLongTitle, ProgramTitle
END
GO
