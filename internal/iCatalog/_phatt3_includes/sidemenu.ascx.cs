namespace ICatalog._phatt3_includes
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using ICatalog._phatt3_classes;

	/// <summary>
	///		Summary description for sidemenu.
	/// </summary>
	public partial class sidemenu : System.Web.UI.UserControl
	{
		#region MAIN MENU CONTROLS
		//protected System.Web.UI.HtmlControls.HtmlAnchor lnkNonCredMenu;
		#endregion //use to display only the menu links a user can access

		#region CREDENTIALS MENU CONTROLS
		#endregion

		#region COURSE MENUS - CONTROLS
		#endregion

		#region NON-CREDIT MENUS - CONTROLS
		#endregion

		#region PROGRAM MENUS - CONTROLS
		protected System.Web.UI.WebControls.Panel panDegree;
		protected System.Web.UI.WebControls.Panel panProgramFees;
		#endregion
		

		protected void Page_Load(object sender, System.EventArgs e)
		{
            try
            {
                //store employee id of the person logged in
                String employeeID = Request.Cookies["phatt2"]["userctclinkid"], strMenu = Request.QueryString["menu"];
                HttpCookie objCookie = new HttpCookie("menuDisplay");

                if (Request.Cookies["menuDisplay"] == null)
                {
                    Response.Cookies.Add(objCookie);
                    objCookie.Values.Add("menu", strMenu);
                    Response.Cookies["menuDisplay"].Expires = DateTime.Now.AddDays(1);
                    Response.Cookies["menuDisplay"].Path = "/";
                }
                else if (strMenu != null && Request.Cookies["menuDisplay"]["menu"] != strMenu)
                {
                    Response.Cookies["menuDisplay"]["menu"] = strMenu;
                }
                else
                {
                    strMenu = Request.Cookies["menuDisplay"]["menu"];
                }

                //hide all menus
                panCollegeAdmin.Visible = false;
                panCollege.Visible = false;
                panCredUserAdmin.Visible = false;
                panCredentials.Visible = false;
                panCourseUserAdmin.Visible = false;
                panSubject.Visible = false;
                panCourse.Visible = false;
                panProgUserAdmin.Visible = false;
                panProg.Visible = false;

                String strCollegePermission = "0", strCredPermission = "0", strCoursePermission = "0", strProgPermission = "0";

                if (Request.Cookies["phatt2"] != null && Request.Cookies["userPermission"] == null)
                {
                    String ctcLinkID = Request.Cookies["phatt2"]["userctclinkid"];

                    permissionData csPermission = new permissionData();

                    DataSet dsCollegePermission = csPermission.GetCollegeUserPermissionByEmployeeID(ctcLinkID);
                    if (dsCollegePermission.Tables[0].Rows.Count != 0) {
                        strCollegePermission = dsCollegePermission.Tables[0].Rows[0]["Permission"].ToString();
                    }

                    DataSet dsCredPermission = csPermission.GetCredUserPermissionByEmployeeID(ctcLinkID);
                    if (dsCredPermission.Tables[0].Rows.Count != 0) {
                        strCredPermission = dsCredPermission.Tables[0].Rows[0]["Permission"].ToString();
                    }

                    DataSet dsCoursePermission = csPermission.GetCourseUserPermissionByEmployeeID(ctcLinkID);
                    if (dsCoursePermission.Tables[0].Rows.Count != 0) {
                        strCoursePermission = dsCoursePermission.Tables[0].Rows[0]["Permission"].ToString();
                    }

                    DataSet dsProgPermission = csPermission.GetProgUserPermissionByEmployeeID(ctcLinkID);
                    if (dsProgPermission.Tables[0].Rows.Count != 0) {
                        strProgPermission = dsProgPermission.Tables[0].Rows[0]["Permission"].ToString();
                    }

                    //write user permission cookie
                    objCookie = new HttpCookie("userPermission");
                    System.Web.HttpContext.Current.Response.Cookies.Add(objCookie);
                    objCookie.Values.Add("college", strCollegePermission);
                    objCookie.Values.Add("cred", strCredPermission);
                    objCookie.Values.Add("course", strCoursePermission);
                    objCookie.Values.Add("prog", strProgPermission);
                    System.Web.HttpContext.Current.Response.Cookies["userPermission"].Expires = DateTime.Now.AddDays(1);
                    System.Web.HttpContext.Current.Response.Cookies["userPermission"].Path = "/ICatalog";
                }
                
                //get user permission
                strCollegePermission = Request.Cookies["userPermission"]["college"];
                strCredPermission = Request.Cookies["userPermission"]["cred"];
                strCoursePermission = Request.Cookies["userPermission"]["course"];
                strProgPermission = Request.Cookies["userPermission"]["prog"];

                if (strCollegePermission == "0")
                {
                    lnkCollegeMenu.Visible = false;
                }

                if (strCoursePermission == "0")
                {
                    lnkCourseMenu.Visible = false;
                }

                if (strProgPermission == "0")
                {
                    lnkProgMenu.Visible = false;
                }

                if (strCredPermission == "0")
                {
                    lnkCredMenu.Visible = false;
                }

                if (strMenu == "college")
                {
                    if (strCollegePermission == "2")//Add/Edit/Delete
                    {
                        panCollege.Visible = true;
                    }
                    else if (strCollegePermission == "1")//Admin
                    {
                        panCollegeAdmin.Visible = true;
                        panCollege.Visible = true;
                    }
                }
                else if (strMenu == "credentials")
                {
                    if (strCredPermission == "2")
                    {//Add/Edit/Delete
                        panCredentials.Visible = true;
                    }
                    else if (strCredPermission == "1")
                    {//Admin
                        panCredUserAdmin.Visible = true;
                        panCredentials.Visible = true;
                    }
                }
                else if (strMenu == "courses")
                {
                    if (strCoursePermission == "3")
                    {//Read Only
                        lnkEditDeleteCourse.Visible = false;
                        lnkExportToGraphics.Visible = false;
                        lnkCourseArchive.Visible = false;
                        lnkCourseSearch.Visible = false;
                        panCourse.Visible = true;
                    }
                    else if (strCoursePermission == "2")
                    {//Edit/Delete
                        panSubject.Visible = true;
                        panCourse.Visible = true;
                    }
                    else if (strCoursePermission == "1")
                    {//Admin
                        panCourseUserAdmin.Visible = true;
                        panSubject.Visible = true;
                        panCourse.Visible = true;
                    }
                }
                else if (strMenu == "programs")
                {
                    if (strProgPermission == "3")
                    {//Read Only
                        lnkDegMaint.Visible = false;
                        lnkCatMaint.Visible = false;
                        lnkAOSMaint.Visible = false;
                        lnkPathwayMaint.Visible = false;
                        lnkProgMaint.Visible = false;
                        lnkProgArchive.Visible = false;
                        lnkCPGMaint.Visible = false;
                        lnkProgramFees.Visible = false;
                        lnkCrsSearch.Visible = false;
                        lnkCatMaint.Visible = false;
                        lnkGraphExport.Visible = false;
                        lnkRTFExport.Visible = false;
                        lnkFileUpload.Visible = false;
                        lnkPrintExport.Visible = false;
                        panProg.Visible = true;
                    }
                    else if (strProgPermission == "2")
                    {//Edit/Delete
                        panProg.Visible = true;
                    }
                    else if (strProgPermission == "1")
                    {//Admin
                        panProgUserAdmin.Visible = true;
                        panProg.Visible = true;
                    }
                }
            }
            catch
            {
                //hide all menus
                panCollegeAdmin.Visible = false;
                panCollege.Visible = false;
                panCredUserAdmin.Visible = false;
                panCredentials.Visible = false;
                panCourseUserAdmin.Visible = false;
                panSubject.Visible = false;
                panCourse.Visible = false;
                panProgUserAdmin.Visible = false;
                panProg.Visible = false;
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
