<%@ Page language="c#" Inherits="ICatalog.college.add" CodeFile="add.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Add College</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <script type="text/javascript" src="../_phatt3_src_files/trim.js"></script>
        <script type="text/javascript">
        <!--
			function validate(){
				var txtInstitution = document.getElementById("txtInstitution");
				var txtCollegeShortTitle = document.getElementById("txtCollegeShortTitle");
				var txtCollegeLongTitle = document.getElementById("txtCollegeLongTitle");
				var txtCollegeAddress = document.getElementById("txtCollegeAddress");
				var txtCollegeCity = document.getElementById("txtCollegeCity");
				var txtCollegeState = document.getElementById("txtCollegeState");
				var	txtCollegeZipCode1 = document.getElementById("txtCollegeZipCode1");
				var txtCollegeRegistrationEmail = document.getElementById("txtCollegeRegistrationEmail");
				var blnValid = false;
				
				if(trim(txtInstitution.value) == ""){
					alert("Please enter a college code.");
					txtInstitution.select();
				}else if(trim(txtCollegeLongTitle.value) == ""){
					alert("Please enter a college title.");
					txtCollegeLongTitle.select();
				}else if(trim(txtCollegeShortTitle.value) == ""){
					alert("Please enter a college abbreviation.");
					txtCollegeShortTitle.select();
				}else if(trim(txtCollegeAddress.value) == ""){
					alert("Please enter a college address.");
					txtCollegeAddress.select();
				}else if(trim(txtCollegeCity.value) == ""){
					alert("Please enter a city.");
					txtCollegeCity.select();
	            }else if (trim(txtCollegeState.value) == "") {
	                alert("Please enter a state.");
	                txtCollegeState.select();
	            }else if (trim(txtCollegeZipCode1.value) == "") {
	                alert("Please enter a zip code.");
	                txtCollegeZipCode1.select();
				}else if(trim(txtCollegeRegistrationEmail.value) == ""){
					alert("Please enter a registration email address.");
					txtCollegeRegistrationEmail.select();
				}else{
					txtCollegeShortTitle.value = txtCollegeShortTitle.value.toUpperCase();
					blnValid = true;
				}
				
				return blnValid;
			}
        //-->
        </script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmCollege" runat="server">
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Add College</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <asp:panel runat="server" id="panAdd">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <!-- Error Message -->
                                                                            <asp:panel runat="server" id="panError">
                                                                                <tr>
                                                                                    <td colspan="2" class="portletMain" style="color:red;padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                        <asp:label runat="server" id="lblErrorMsg"></asp:label>
                                                                                    </td>
                                                                                </tr>
                                                                            </asp:panel>
                                                                            <!-- College Code -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>College Code:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtInstitution" cssclass="small" maxlength="3" style="width:45px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- College Title -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>College Title:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtCollegeLongTitle" cssclass="small" maxlength="80" style="width:332px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- College Abbreviation -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>College Abbreviation:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtCollegeShortTitle" cssclass="small" maxlength="4" style="width:45px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Street Address -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Street Address:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtCollegeAddress" cssclass="small" maxlength="50" style="width:250px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- City -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>City:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:9px;padding-bottom:9px;padding-left:6px;">
                                                                                    <asp:TextBox runat="server" id="txtCollegeCity" CssClass="small" MaxLength="50" style="width:250px;"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- State -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>State:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:9px;padding-bottom:9px;padding-left:6px;">
                                                                                    <asp:TextBox runat="server" id="txtCollegeState" CssClass="small" MaxLength="2" style="width:30px;"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Zip Code -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Zip Code:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:TextBox runat="server" id="txtCollegeZipCode1" cssclass="small" maxlength="5" style="width:45px;"></asp:TextBox>
                                                                                    &nbsp;-&nbsp;
                                                                                    <asp:TextBox runat="server" id="txtCollegeZipCode2" cssclass="small" maxlength="4" style="width:40px;"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Website URL -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Website URL:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtCollegeWebsiteURL" cssclass="small" style="width:332px;" maxlength="100"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Registration Email Address -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Registration Email Address:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtCollegeRegistrationEmail" cssclass="small" style="width:332px;" maxlength="100"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
																					<input type="button" id="cmdCancel" value="Cancel" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="location.href='edit.aspx';" />
															                        &nbsp;<asp:button id="cmdSubmit" runat="server" text="Save Addition" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclientclick="return validate();" onclick="cmdSubmit_Click"></asp:button>
														                        </td>
													                        </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                    <asp:panel runat="server" id="panConfirm">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <tr>
                                                                                <td class="portletMain" colspan="2" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                    The following college has been successfully added.
                                                                                </td>
                                                                            </tr>
                                                                            <!-- College Code -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:30%;text-align:right;">
                                                                                    <b>College Code:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:70%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblInstitution"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- College Title -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:30%;text-align:right;">
                                                                                    <b>College Title:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:70%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblCollegeLongTitle"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- College Abbreviation -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:30%;text-align:right;">
                                                                                    <b>College Abbreviation:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:70%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblCollegeShortTitle"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- College Address -->
                                                                            <tr>
																				<td class="portletSecondary" style="width:30%;text-align:right;">
																					<b>College Address&nbsp;</b>
																				</td>
																				<td class="portletLight" style="width:70%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:label runat="server" id="lblCollegeAddress"></asp:label>
																				</td>
                                                                            </tr>
                                                                            <!-- City -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:30%;text-align:right;">
                                                                                    <b>City:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:70%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblCollegeCity"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- State -->
                                                                            <tr >
                                                                                <td class="portletSecondary" style="width:30%;text-align:right;">
                                                                                    <b>State:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:70%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblCollegeState"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Zip Code -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:30%;text-align:right;">
                                                                                    <b>Zip Code:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:70%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblCollegeZipCode"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Website URL -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:30%;text-align:right;">
                                                                                    <b>Website URL:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:70%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblCollegeWebsiteURL"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Registration Email Address -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:30%;text-align:right;">
                                                                                    <b>Registration Email Address:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:70%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblCollegeRegistrationEmail"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                        <input type="button" id="cmdOK" value="OK" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="location.href='edit.aspx';" />
														                        </td>
													                        </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <div class="clearer"></div>
        </asp:panel>
    </body>
</html>

