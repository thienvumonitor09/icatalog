USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDegree]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 12/5/17
-- Description:	Returns a degree by OptionID
--		 INPUT: OptionID
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDegree]
	@OptionID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT ProgramTitle, OptionTitle, DegreeShortTitle, DegreeLongTitle, College.CollegeID, CollegeShortTitle, CollegeLongTitle, CareerPlanningGuideID, ProgramEnrollment, ProgramWebsiteURL, ProgramDescription, OptionDescription,
	ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, ProgramBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, ProgramEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, GainfulEmploymentID
	FROM ProgramDegreeOption
	INNER JOIN ProgramDegree
	ON ProgramDegree.ProgramDegreeID = ProgramDegreeOption.ProgramDegreeID
	INNER JOIN Program
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
	INNER JOIN Degree
	ON Degree.DegreeID = ProgramDegree.DegreeID
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = ProgramBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = ProgramEndSTRM
	LEFT OUTER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
	AND PublishedCareerPlanningGuide = '1' 
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID 
	WHERE OptionID = @OptionID
	AND PublishedProgram = 1 
END
GO
