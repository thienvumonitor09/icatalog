USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetLocations]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/9/17
-- Description:	Returns all locations offering programs
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetLocations]
	
AS
BEGIN
	SET NOCOUNT ON;

    SELECT LocationID, LocationTitle
    FROM Location
    WHERE LocationID IN
		(SELECT LocationID 
		FROM OptionLocation
		INNER JOIN ProgramDegreeOption
		ON ProgramDegreeOption.OptionID = OptionLocation.OptionID
		INNER JOIN ProgramDegree
		ON ProgramDegree.ProgramDegreeID = ProgramDegreeOption.ProgramDegreeID
		INNER JOIN Program
		ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
		WHERE PublishedProgram <> '0')
    ORDER BY LocationTitle;
END

GO
