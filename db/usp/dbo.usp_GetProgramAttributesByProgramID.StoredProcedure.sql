USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramAttributesByProgramID]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 4/19/18
-- Description:	Returns degree/certificate total credits, total quarters, tuition, and financial aid eligibility by ProgramID
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramAttributesByProgramID] 
	@STRM int = NULL,
	@ProgramID int
AS
BEGIN
	SET NOCOUNT ON;

	IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
							FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
							WHERE TERM_END_DT > GETDATE()
							ORDER BY TERM_END_DT)
							
	SELECT TotalCredits, TotalQuarters, Tuition, FinancialAidEligible
	FROM CCSiCatalog_Legacy.dbo.ProgramAttributes
	INNER JOIN CCSiCatalog_Legacy.dbo.Program
	ON Program.ProgramID = ProgramAttributes.ProgramID
	WHERE Program.ProgramID = @ProgramID
	AND (ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM))
	AND PublishedProgram = 1
	
END
GO
