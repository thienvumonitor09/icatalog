USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseEndTerms]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 3/2/2017
-- Description:	Gets terms greater than or equal to the begin term of the most current course in a department
--       INPUT: SUBJECT - Course Subject: ENGL 
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseEndTerms]
	@SUBJECT varchar(5)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT STRM, DESCR
	FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
	WHERE STRM >=
	(SELECT TOP 1 CourseBeginSTRM
	FROM Course
	WHERE SUBJECT = @SUBJECT
	ORDER BY CourseBeginSTRM DESC)
	ORDER BY STRM DESC
END

GO
