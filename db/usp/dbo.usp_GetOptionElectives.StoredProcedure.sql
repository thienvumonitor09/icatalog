USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionElectives]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/11/17
-- Description:	Returns program degree option electives
--		 INPUT: OptionID - Program degree option id
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionElectives]
	@OptionID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT ElectiveGroupTitle, OptionElective.OptionElectiveGroupID, Quarter, UnitsMinimum, UnitsMaximum, FootnoteNumber, OptionElective.OptionFootnoteID 
    FROM OptionElective 
    INNER JOIN OptionElectiveGroup 
    ON OptionElectiveGroup.OptionElectiveGroupID = OptionElective.OptionElectiveGroupID 
    LEFT OUTER JOIN OptionFootnote 
    ON OptionFootnote.OptionFootnoteID = OptionElective.OptionFootnoteID
    WHERE OptionElective.OptionID = @OptionID
    ORDER BY Quarter, ElectiveGroupTitle;
END

GO
