using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.popups
{
	/// <summary>
	/// Summary description for editelective.
	/// </summary>
	public partial class editelective : System.Web.UI.Page
	{
		programData csProgram = new programData();
		courseData csCourse = new courseData();

		#region PROTECTED CONTROLS
		protected System.Web.UI.WebControls.Button cmdAddPrereq;
		#endregion

		protected void Page_Load(object sender, System.EventArgs e) {

			panOverwrite.Visible = false;
			panOverwriteUnits.Visible = false;

			if(!IsPostBack){
				//CHECK IF ALL THESE VALUES ARE NEEDED FOR THE EDIT SCREEN
				hidProgramVersionID.Value = Request.QueryString["pvid"];
				hidProgramDegreeID.Value = Request.QueryString["pdid"];
				hidCollegeID.Value = Request.QueryString["colid"];
				hidLtr.Value = Request.QueryString["ltr"];
				hidProgramTitle.Value = Request.QueryString["pt"];
				hidOfferedAt.Value = Request.QueryString["oa"];
				hidDegreeID.Value = csProgram.GetDegreeID(Convert.ToInt32(hidProgramDegreeID.Value)).ToString();
				hidOptionID.Value = Request.QueryString["oid"];
				hidProgramDisplay.Value = Request.QueryString["pd"];
				hidCourseSubject.Value = Request.QueryString["cs"];
				hidOptionElectiveGroupID.Value = Request.QueryString["egid"];
				hidCourseOffering.Value = Request.QueryString["co"];
				hidOptionFootnoteID.Value = Request.QueryString["fid"];
				hidProgramBeginSTRM.Value = Request.QueryString["bstrm"];
				hidProgramEndSTRM.Value = Request.QueryString["estrm"];
				hidPublishedProgram.Value = Request.QueryString["pp"];
				hidPublishedProgramVersionID.Value = Request.QueryString["ppvid"];
				hidSTRM.Value = Request.QueryString["strm"];
				hidSelectedCollegeID.Value = Request.QueryString["scolid"];
				hidProgramID.Value = Request.QueryString["id"];
				hidSearchSubject.Value = Request.QueryString["searchsub"];
				hidSearchId.Value = Request.QueryString["searchid"];
				hidSearchSTRM.Value = Request.QueryString["searchstrm"];
				hidSearchText.Value = Request.QueryString["searchtxt"];

				panEditElective.Visible = true;
				panAddElectGroup.Visible = false;

				Int32 programVersionID = Convert.ToInt32(Request.QueryString["pvid"]), optionID = Convert.ToInt32(hidOptionID.Value), DegreeID = Convert.ToInt32(Request.QueryString["did"]);

				DisplayOptElectGroups(optionID);
				cboElectGroup.SelectedValue = hidOptionElectiveGroupID.Value; //test this			

				DataSet dsCrsDept = csProgram.GetSubjectsForPrograms(hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
				Int32 intCrsDeptCount = dsCrsDept.Tables[0].Rows.Count;

				if(intCrsDeptCount > 0){
					for(Int32 intDSRow = 0; intDSRow < intCrsDeptCount; intDSRow++){
						if(Convert.ToInt32(dsCrsDept.Tables[0].Rows[intDSRow]["CrsCount"]) > 0){
							cboCrsDept.Items.Add(dsCrsDept.Tables[0].Rows[intDSRow]["SUBJECT"].ToString());
						}
					}
					cboCrsDept.SelectedValue = hidCourseSubject.Value;
				}else{
					//do something -- may want to check if the cboCrsDept.length > 0
				}

				DataSet dsFootnotes = csProgram.GetOptFootnotes(optionID);
				Int32 intFootnoteCount = dsFootnotes.Tables[0].Rows.Count;
				
				if(intFootnoteCount > 0){
					cboFootnote.Items.Add(new ListItem("", "0"));
                    cboElectGroupFootnote.Items.Add(new ListItem("", "0"));
					for(Int32 intDSRow = 0; intDSRow < intFootnoteCount; intDSRow++){
						String footnoteNumber = dsFootnotes.Tables[0].Rows[intDSRow]["FootnoteNumber"].ToString();
						String strOptionFootnoteID = dsFootnotes.Tables[0].Rows[intDSRow]["OptionFootnoteID"].ToString();

						cboFootnote.Items.Add(new ListItem(footnoteNumber, strOptionFootnoteID));
                        cboElectGroupFootnote.Items.Add(new ListItem(footnoteNumber, strOptionFootnoteID));
					}
				}
			}

			String strFootnote = Request.Form["cboFootnote"];
			String courseOffering = Request.Form["cboCatalogNbr"];

			if(cboFootnote.Items.Count == 0){
				cboFootnote.Items.Add(new ListItem("None Entered", "0"));
				lblFootnote.Text = "None Selected";
			}else{

				try{
					if(strFootnote == null){
						cboFootnote.SelectedValue = hidOptionFootnoteID.Value;
					}else{
						cboFootnote.SelectedValue = Request.Form["cboFootnote"];
					}
				}catch{
					//do nothing
				}

				if(cboFootnote.SelectedValue == "0"){
					lblFootnote.Text = "None Selected";
				}else{
					lblFootnote.Text = csProgram.GetFootnote(Convert.ToInt32(cboFootnote.SelectedValue));
				}
			}

			if(cboElectGroupFootnote.Items.Count == 0){
				cboElectGroupFootnote.Items.Add(new ListItem("None Entered", "0"));
				lblElectGroupFootnote.Text = "None Selected";
			}else{
				if(cboElectGroupFootnote.SelectedValue == "0"){
					lblElectGroupFootnote.Text = "None Selected";
				}else{
					lblElectGroupFootnote.Text = csProgram.GetFootnote(Convert.ToInt32(cboElectGroupFootnote.SelectedValue));
				}
			}

			DataSet dsCatalogNbrs = csProgram.GetCourseOfferings(cboCrsDept.SelectedValue, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
			Int32 intCrsCount = dsCatalogNbrs.Tables[0].Rows.Count;
			
			cboCatalogNbr.Items.Clear();

			if(intCrsCount > 0){

				for(Int32 intDSRow = 0; intDSRow < intCrsCount; intDSRow++){
                    //cboCatalogNbr.Items.Add(new ListItem(dsCatalogNbrs.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString() + dsCatalogNbrs.Tables[0].Rows[intDSRow]["CourseSuffix"].ToString(), dsCatalogNbrs.Tables[0].Rows[intDSRow]["CourseOffering"].ToString()));
                    cboCatalogNbr.Items.Add(new ListItem(dsCatalogNbrs.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString() , dsCatalogNbrs.Tables[0].Rows[intDSRow]["CourseOffering"].ToString()));
                }

                try
                {
					if(courseOffering == null){
						cboCatalogNbr.SelectedValue = hidCourseOffering.Value;
					}else{
						cboCatalogNbr.SelectedValue = Request.Form["cboCatalogNbr"];
					}
				}catch{
					//do nothing
				}

				DataSet dsCrsToDisplay = csCourse.GetMostCurrentCourseForProgram(cboCatalogNbr.SelectedValue, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
				Int32 intRowCount = dsCrsToDisplay.Tables[0].Rows.Count;

				if(intRowCount > 0){
                    lblEFFDT.Text = Convert.ToDateTime(dsCrsToDisplay.Tables[0].Rows[0]["EFFDT"]).ToShortDateString(); 
					lblCourseLongTitle.Text = dsCrsToDisplay.Tables[0].Rows[0]["COURSE_TITLE_LONG"].ToString();
				
					//use for variable credit overwrite
					double dblCrsMin = Convert.ToDouble(dsCrsToDisplay.Tables[0].Rows[0]["UNITS_MINIMUM"]), dblCrsMax = Convert.ToDouble(dsCrsToDisplay.Tables[0].Rows[0]["UNITS_MAXIMUM"]);
						
					if(dblCrsMin != dblCrsMax)
                    {
						hidVarCr.Value = "Y";
						lblCrsCredits.Text = dblCrsMin + " - " + dblCrsMax;

						if(!IsPostBack){
							//get the overwrite values from the db
							DataSet dsOverwriteUnits = csProgram.GetElectiveOverwriteUnits(Convert.ToInt32(cboElectGroup.SelectedValue), cboCatalogNbr.SelectedValue, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
							if(dsOverwriteUnits.Tables[0].Rows.Count > 0){
								bool blnOverwriteUnits = Convert.ToBoolean(dsOverwriteUnits.Tables[0].Rows[0]["OverwriteUnits"]);
								if(blnOverwriteUnits){
									txtCrsMin.Text = dsOverwriteUnits.Tables[0].Rows[0]["OverwriteUnitsMinimum"].ToString();
									txtCrsMax.Text = dsOverwriteUnits.Tables[0].Rows[0]["OverwriteUnitsMaximum"].ToString();
									chkOverwrite.Checked = true;
								}else{
									txtCrsMin.Text = "";
									txtCrsMax.Text = "";
									chkOverwrite.Checked = false;
								}
							}else{
								chkOverwrite.Checked = false;
								txtCrsMin.Text = "";
								txtCrsMax.Text = "";
							}
						}

						panOverwrite.Visible = true;
						if(chkOverwrite.Checked){
							panOverwriteUnits.Visible = true;
						}
					}else{
						hidVarCr.Value = "N";
						lblCrsCredits.Text = dblCrsMin.ToString();
						chkOverwrite.Checked = false;
					}

					hidUnitsMinimum.Value = dblCrsMin.ToString();
					hidUnitsMaximum.Value = dblCrsMax.ToString();
				}

			}else{
				//do something -- may want to check if the cboCatalogNbrs.length > 0
			}
		}

		protected void cmdAddNew_Click(object sender, System.EventArgs e){
			//store populated values to pass back if the cancel button is clicked
			hidOptionElectiveGroupID.Value = cboElectGroup.SelectedValue;
			hidCourseSubject.Value = cboCrsDept.SelectedValue;
			hidCourseOffering.Value = cboCatalogNbr.SelectedValue;
			hidOptionFootnoteID.Value = cboFootnote.SelectedValue;

			panEditElective.Visible = false;
			panAddElectGroup.Visible = true;
		}

		protected void cmdEditProgElect_Click(object sender, System.EventArgs e){
			Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
			Int32 optionID = Convert.ToInt32(hidOptionID.Value);
			Int32 degreeID = Convert.ToInt32(hidDegreeID.Value);
			byte overwriteUnits = 0;

			if(Request.Form["hidVarCr"] == "Y" && Request.Form["chkOverwrite"] == "on"){
				overwriteUnits = 1;
			}
		
			Byte bitPrimaryOption = 0;
			if(Request.Form["hidPrimaryOption"] == "true"){
				bitPrimaryOption = 1;
			}

			csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), Request.Form["hidOptionDescription"], Request.Form["hidOptionStateApproval"], Request.Form["hidCIP"], Request.Form["hidEPC"], Request.Form["hidAcademicPlan"], bitPrimaryOption, Request.Form["hidGainfulEmploymentID"], Request.Form["hidTotalQuarters"]);
			
			csProgram.DeleteOptionLocations(optionID);
			if(hidLocations.Value != ""){
				String[] strOptionLocations = hidLocations.Value.Substring(1).Split('|');
				for(Int32 i = 0; i < strOptionLocations.Length; i++){
					csProgram.AddOptionLocation(optionID, Convert.ToInt32(strOptionLocations[i]));
				}
			}

			csProgram.DeleteOptionMCodes(optionID);
			if(hidMCodes.Value != ""){
				String[] strMCodes = hidMCodes.Value.Substring(1).Split('|');
				for(Int32 i = 0; i < strMCodes.Length; i++){
					String[] strCollegeMCode = strMCodes[i].Split(';');
					csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0]), strCollegeMCode[1]);
				}
			}

			Int32 intSuccess = csProgram.EditElectiveGroupCourse(Convert.ToInt32(hidOptionElectiveGroupID.Value), Convert.ToInt32(cboElectGroup.SelectedValue), hidCourseOffering.Value, cboCatalogNbr.SelectedValue, Convert.ToInt32(cboFootnote.SelectedValue), overwriteUnits, Convert.ToDouble(Request.Form["txtCrsMin"]), Convert.ToDouble(Request.Form["txtCrsMax"]));

			if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The elective course entered already exists.";
				panError.Visible = true;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The update failed.";
				panError.Visible = true;
			}else{
				//refresh main window
				Response.Write("<script type=\"text/javascript\">opener.location.href='../edit.aspx?oid=" + hidOptionID.Value + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&todo=pop&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'","\\'") + "&pt=" + hidProgramTitle.Value.Replace("'","\\'") + "&pd=" + hidProgramDisplay.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&","%26") + "&searchid=" + hidSearchId.Value.Replace("&","%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&","%26").Replace("'","\\'") + "';self.close();</script>");
			}

		}

		protected void cmdAddElectGroup_Click(object sender, System.EventArgs e){
			Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value);
			Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
			Int32 optionID = Convert.ToInt32(hidOptionID.Value);
			Int32 degreeID = Convert.ToInt32(hidDegreeID.Value);
		
			Byte bitPrimaryOption = 0;
			if(Request.Form["hidPrimaryOption"] == "true"){
				bitPrimaryOption = 1;
			}
			csProgram.EditOptionValues(optionID, programVersionID, Request.Form["hidOptionDescription"], Request.Form["hidOptionStateApproval"], Request.Form["hidCIP"], Request.Form["hidEPC"], Request.Form["hidAcademicPlan"], bitPrimaryOption, Request.Form["hidGainfulEmploymentID"], Request.Form["hidTotalQuarters"]);
			csProgram.DeleteOptionMCodes(optionID);
			if(hidMCodes.Value != ""){
				String[] strMCodes = hidMCodes.Value.Substring(1).Split('|');
				for(Int32 i = 0; i < strMCodes.Length; i++){
					String[] strCollegeMCode = strMCodes[i].Split(';');
					csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0]), strCollegeMCode[1]);
				}
			}
			Int32 intSuccess = csProgram.AddOptionElectiveGroup(optionID, txtElectGroupTitle.Text, Convert.ToInt32(cboElectGroupFootnote.SelectedValue));

			if(intSuccess < 0){
				lblErrorMsg2.Text = "Error: The elective group entered already exists.";
				panError2.Visible = true;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The insert failed.";
				panError2.Visible = true;
			}else{
				//regenerate elective group list in combo box
				DisplayOptElectGroups(optionID);

				//clear the add elective group form fields
				txtElectGroupTitle.Text = "";
				cboElectGroupFootnote.SelectedValue = "0";
				lblElectGroupFootnote.Text = "None Selected";

				//display the add elective panel
				panAddElectGroup.Visible = false;
				panError2.Visible = false;
				panEditElective.Visible = true;

				//refresh main window
				Response.Write("<script type=\"text/javascript\">opener.location.href='../edit.aspx?oid=" + hidOptionID.Value + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&todo=pop&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'","\\'") + "&pt=" + hidProgramTitle.Value.Replace("'","\\'") + "&pd=" + hidProgramDisplay.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&","%26") + "&searchid=" + hidSearchId.Value.Replace("&","%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&","%26").Replace("'","\\'") + "';</script>");
			}
				
		}

		public void DisplayOptElectGroups(Int32 optionID){

			cboElectGroup.Items.Clear();

			//get the elective groups for the program
			DataSet dsElectGroups = csProgram.GetOptionElectiveGroups(optionID);
			Int32 intElectGroupCount = dsElectGroups.Tables[0].Rows.Count;

			if(intElectGroupCount > 0){
				for(Int32 intDSRow = 0; intDSRow < intElectGroupCount; intDSRow++){
					cboElectGroup.Items.Add(new ListItem(dsElectGroups.Tables[0].Rows[intDSRow]["ElectiveGroupTitle"].ToString(), dsElectGroups.Tables[0].Rows[intDSRow]["OptionElectiveGroupID"].ToString()));
				}
			}else{
				cboElectGroup.Items.Add(new ListItem("Add an elective group before continuing", ""));
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {    
		}
		#endregion
	}
}
