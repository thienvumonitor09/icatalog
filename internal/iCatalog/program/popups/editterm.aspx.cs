using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.popups
{
	/// <summary>
	/// Summary description for editterm.
	/// </summary>
	public partial class editterm : System.Web.UI.Page
	{

		programData csProgram = new programData();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			panMsg.Visible = false;

			if(!IsPostBack){
				hidProgramTitle.Value = Request.QueryString["pt"];
				hidCollege.Value = Request.QueryString["colst"];
				hidPublishedProgramVersionID.Value = Request.QueryString["ppvid"];
				hidPublishedProgram.Value = Request.QueryString["pp"];
				hidCollegeID.Value = Request.QueryString["colid"];
				hidProgramBeginSTRM.Value = Request.QueryString["bstrm"];
				hidProgramEndSTRM.Value = Request.QueryString["estrm"];	
				hidPage.Value = Request.QueryString["pg"];

				//populate program effective year quarter dropdownlists
				DataSet dsTerms = csProgram.GetTermsForPrograms();
				cboRevisionBeginTerm.Items.Add("");
				cboRevisionEndTerm.Items.Add("");
				cboRevisionEndTerm.Items.Add("Z999");
				cboCurrentEndTerm.Items.Add("Z999");
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
					String STRM = dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString();
                    String DESCR = dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString();
					cboCurrentBeginTerm.Items.Add(new ListItem(DESCR, STRM));
					cboCurrentEndTerm.Items.Add(new ListItem(DESCR, STRM));
					cboRevisionBeginTerm.Items.Add(new ListItem(DESCR, STRM));
					cboRevisionEndTerm.Items.Add(new ListItem(DESCR, STRM));
				}

				try{
					cboCurrentBeginTerm.SelectedValue = Request.QueryString["bstrm"];
					cboCurrentEndTerm.SelectedValue = Request.QueryString["estrm"];
				}catch{
					//do nothing
				}
			}
		}

		private void cmdSave_Click(object sender, System.EventArgs e){
			Int32 programVersionID = Convert.ToInt32(Request.QueryString["pvid"]);

/* change to look for conflicts offering the same degree type
 * 
			//check if the change to the current program version begin and end effective year quarters conflict with other versions
			String currentConflictingTermSpan = csProgram.GetConflictingTermsForExistingProgram(programVersionID, hidProgramTitle.Value, Convert.ToInt32(hidCollegeID.Value), cboCurrentBeginTerm.SelectedValue, cboCurrentEndTerm.SelectedValue);
			if(hidPublishedProgram.Value == "Working Copy" && hidPublishedProgramVersionID.Value != "" && currentConflictingTermSpan == (hidProgramBeginSTRM.Value + " - " + hidProgramEndSTRM.Value)){
				currentConflictingTermSpan = "";
			}

			//check if the new program version begin and end effective year quarters conflict with other versions
			String newConflictingTermSpan = csProgram.GetConflictingTermsForExistingProgram(programVersionID, hidProgramTitle.Value, Convert.ToInt32(hidCollegeID.Value), cboRevisionBeginTerm.SelectedValue, cboRevisionEndTerm.SelectedValue);
				
			if(currentConflictingTermSpan != ""){
				lblMsg.Text = "Error: The current program begin and end terms selected overlap with an existing \"" + hidProgramTitle.Value + "\" program offered at " + hidCollege.Value + " effective " + currentConflictingTermSpan + ".";
				panMsg.Visible = true;
			}else if(newConflictingTermSpan != ""){
				lblMsg.Text = "Error: The new program begin and end terms selected overlap with an existing \"" + hidProgramTitle.Value + "\" program offered at " + hidCollege.Value + " effective " + newConflictingTermSpan + ".";
				panMsg.Visible = true;
			}else{
 * 
 * uncomment closing bracket when changes are made
 */
				//change current version strm values
				bool blnSuccess = csProgram.EditProgramTerms(programVersionID, cboCurrentBeginTerm.SelectedValue, cboCurrentEndTerm.SelectedValue);

				if(blnSuccess){
					//pass updated begin and end terms back to opener and start program revision process
					String strScript = "<script type=\"text/javascript\">" + 
						"opener.document.frmProgram.hidProgramBeginSTRM.value='" + cboRevisionBeginTerm.SelectedValue + "';" + 
						"opener.document.frmProgram.hidProgramEndSTRM.value='" + cboRevisionEndTerm.SelectedValue + "';" + 
						"opener.document.frmProgram.hidTodo.value='progRevision';" +
						"opener.document.frmProgram.submit();" +
						"self.close();" +
						"</script>";
					script.Controls.Add(new LiteralControl(strScript));
				}else{
					lblMsg.Text = "Error: The current version begin and end terms could not be updated. The update failed.";
					panMsg.Visible = true;
				}
/*			}*/
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.cmdSave.Click += new EventHandler(cmdSave_Click);
		}
		#endregion
	}
}
