USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionFootnotes]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/12/17
-- Description:	Returns program degree option footnotes
--		 INPUT: OptionID - Program degree option primary key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionFootnotes] 
	@OptionID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT OptionFootnoteID, FootnoteNumber, Footnote
    FROM OptionFootnote
    WHERE OptionID = @OptionID;
END

GO
