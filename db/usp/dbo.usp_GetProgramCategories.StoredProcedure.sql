USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramCategories]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/12/17
-- Description:	Returns program categories
--		 INPUT: None
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramCategories] 
	
AS
BEGIN
	SET NOCOUNT ON;

    SELECT Category.CategoryID, CategoryTitle, COUNT(ProgramCategory.ProgramVersionID) AS ProgramCount 
    FROM Category 
    INNER JOIN ProgramCategory
    ON ProgramCategory.CategoryID = Category.CategoryID
    INNER JOIN Program 
    ON Program.ProgramVersionID = ProgramCategory.ProgramVersionID
    WHERE PublishedProgram = 1 
    GROUP BY Category.CategoryID, CategoryTitle 
    ORDER BY CategoryTitle;
END

GO
