USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetElectiveGroupCourses_2]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/22/2019
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetElectiveGroupCourses_2]
	@BeginSTRM varchar(4)
	, @EndSTRM varchar(4)
	, @OptionElectiveGroupID int 
	, @CurrentSTRM varchar(4)
	,@INSTITUTION varchar(5) = 'WA171'
AS
BEGIN
	SET NOCOUNT ON;
	SELECT OptionElectiveGroupID, ElectiveGroupCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseOffering.EFFDT AS CourseOffering_EFFDT, CourseCatalog.EFFDT AS CourseCatalog_EFFDT, RTRIM(REPLACE(COURSE_TITLE_LONG,'[CCN]','')) AS COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber, ElectiveGroupCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum 
	FROM ElectiveGroupCourse 
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	ON REPLACE(CourseOffering.SUBJECT + CourseOffering.CATALOG_NBR,' ','') = REPLACE(ElectiveGroupCourse.CourseOffering,' ','')
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	LEFT OUTER JOIN OptionFootnote 
	ON OptionFootnote.OptionFootnoteID = ElectiveGroupCourse.OptionFootnoteID
	WHERE OptionElectiveGroupID = @OptionElectiveGroupID
		AND CourseOffering.EFFDT = 
		(
			SELECT MAX(CourseOffering2.EFFDT)
			FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
			WHERE CourseOffering2.CRSE_ID = CourseOffering.CRSE_ID 
							AND INSTITUTION = @INSTITUTION
				AND CourseOffering2.EFFDT <= (SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE (@EndSTRM = 'Z999' OR STRM = @EndSTRM) ORDER BY TERM_END_DT DESC)
		)
		AND CATALOG_PRINT = 'Y'
		AND ACAD_CAREER = 'UGRD'
		AND CATALOG_PRINT = 'Y'
		--AND CourseCatalog.EFF_STATUS = 'A'
		AND CourseOffering.INSTITUTION = @INSTITUTION
	GROUP BY OptionElectiveGroupID, ElectiveGroupCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseOffering.EFFDT, CourseCatalog.EFFDT, RTRIM(REPLACE(COURSE_TITLE_LONG,'[CCN]','')), UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber, ElectiveGroupCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum 
	ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;		


END
GO
