USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionPrerequisitesLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/11/17
-- Description:	Returns program degree option prerequisites
--		 INPUT: OptionID - Program degree option primary key
--				BeginSTRM - Program Begin Term Code
--				EndSTRM - Program End Term Code
--				SelectedSTRM - The Term Code Selected
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionPrerequisitesLegacy] 
	@OptionID int,
	@BeginSTRM varchar(4),
	@EndSTRM varchar(4),
	@SelectedSTRM varchar(4)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT SUBJECT, CATALOG_NBR, CourseSuffix, OptionPrerequisite.CourseOffering, COURSE_TITLE_LONG, CourseBeginSTRM, CourseEndSTRM, FootnoteNumber, OptionPrerequisiteID, OptionPrerequisite.OptionFootnoteID 
    FROM OptionPrerequisite 
    INNER JOIN Course 
    ON Course.CourseOffering = OptionPrerequisite.CourseOffering 
    AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999')) 
    OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM AND (CourseEndSTRM <= @EndSTRM OR CourseEndSTRM = 'Z999')))
    AND (CourseBeginSTRM <= @SelectedSTRM AND (CourseEndSTRM >= @SelectedSTRM OR CourseEndSTRM = 'Z999')) 
    LEFT OUTER JOIN OptionFootnote 
    ON OptionFootnote.OptionFootnoteID = OptionPrerequisite.OptionFootnoteID 
    WHERE OptionPrerequisite.OptionID = @OptionID
    ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
END

GO
