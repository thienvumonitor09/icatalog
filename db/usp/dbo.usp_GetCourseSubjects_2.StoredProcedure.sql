USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseSubjects_2]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/14/2019
-- Description:	Return active subjects
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseSubjects_2]
	@STRM varchar(4) = NULL,
	@INSTITUTION varchar(5),
	@ChrLtr varchar(2) = NULL,
	@SUBJECT varchar(5) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    ;WITH SUBJECT_tbl (SUBJECT, CrsCount)
	-- First table to retrieve list of subjects.  
	AS
	(
		SELECT DISTINCT CourseOffering.SUBJECT, COUNT(CATALOG_NBR) AS CrsCount
		FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
		LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
		ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
		WHERE CourseOffering.EFFDT = 
			(
				SELECT MAX(CourseOffering2.EFFDT) 
				FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
				WHERE CourseOffering2.CRSE_ID = CourseOffering.CRSE_ID /*CourseOffering2.SUBJECT = CourseOffering.SUBJECT
					AND CourseOffering2.CATALOG_NBR = CourseOffering.CATALOG_NBR*/
					AND INSTITUTION = @INSTITUTION
					AND
					(
						@STRM IS NULL 
						OR
						CourseOffering2.EFFDT <= (SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE STRM = @STRM ORDER BY TERM_END_DT DESC)
						OR
						@STRM = 'Z999'	
					)
			) 
			AND ACAD_CAREER = 'UGRD'
			AND CATALOG_PRINT = 'Y'
			AND CourseCatalog.EFF_STATUS = 'A'
			AND CourseOffering.INSTITUTION = @INSTITUTION
		GROUP BY CourseOffering.SUBJECT
		--ORDER BY SUBJECT ASC
	)
	,
	-- SECOND CTE to retrieve list of Subject's most recent description 
	SUBJECT_DESCR (INSTITUTION, SUBJECT, EFFDT, EFF_STATUS, DESCR)
	AS
	(
		SELECT INSTITUTION, SUBJECT, EFFDT, EFF_STATUS, DESCR
		 FROM [CCSGen_ctcLink_ODS].[dbo].[vw_SubjectTable] as SubjectTable
		 WHERE EFFDT=
		 (
			SELECT MAX(EFFDT)
			FROM [CCSGen_ctcLink_ODS].[dbo].[vw_SubjectTable] as SubjectTable2 
			WHERE SubjectTable.INSTITUTION = SubjectTable2.INSTITUTION AND SubjectTable.SUBJECT = SubjectTable2.SUBJECT AND EFF_STATUS = 'A' AND INSTITUTION='WA171'
		 )
	)

	SELECT *
	FROM SUBJECT_tbl
	LEFT OUTER JOIN SUBJECT_DESCR
	ON SUBJECT_tbl.SUBJECT = SUBJECT_DESCR.SUBJECT
	WHERE 
		(
			(@ChrLtr IS NULL OR SUBJECT_tbl.SUBJECT LIKE @ChrLtr+'%')
			AND
			(@SUBJECT IS NULL OR SUBJECT_tbl.SUBJECT = @SUBJECT )
		)
	ORDER BY SUBJECT_tbl.SUBJECT ASC;
END
GO
