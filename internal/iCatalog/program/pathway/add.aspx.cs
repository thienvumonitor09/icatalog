﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

public partial class program_pathway_add : System.Web.UI.Page {

    programData csProgram = new programData();

    protected void Page_Load(object sender, System.EventArgs e) {
        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        panAdd.Visible = false;
        panError.Visible = false;
        panConfirm.Visible = false;

        if (!IsPostBack) {
            panAdd.Visible = true;
        }
    }

    protected void cmdSubmit_Click(object sender, System.EventArgs e) {
        String pathwayTitle = txtPathwayTitle.Text;
        String institution = optOfferedAt.SelectedValue;
        Int16 intSuccess = csProgram.AddPathway(institution, pathwayTitle);

        if (intSuccess == 0) {
            lblErrorMsg.Text = "Error: The insert failed.";
            panError.Visible = true;
            panAdd.Visible = true;
        } else if (intSuccess == 2) {
            lblErrorMsg.Text = "Error: The guided pathway entered already exists.";
            panError.Visible = true;
            panAdd.Visible = true;
        } else if (intSuccess == 1) {
            if(institution == "WA171") {
                lblOfferedBy.Text = "SCC";
            }else if(institution == "WA172") {
                lblOfferedBy.Text = "SFCC";
            }
            lblPathwayTitle.Text = pathwayTitle;
            panConfirm.Visible = true;
        }
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e) {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
    }
    #endregion
}
