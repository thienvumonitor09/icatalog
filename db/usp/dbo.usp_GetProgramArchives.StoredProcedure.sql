USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramArchives]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/1/17
-- Description:	Returns a list of archived programs that have a title starting with the alpha character passed in
--		 INPUT: AlphaCharacter - Alpha character program titles start with (A-Z)
--				STRM - Term code
--				CollegeID - College primary key
--	  MODIFIED: 2/27/18 to include Degree Titles
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramArchives]
	@AlphaCharacter char(1),
	@STRM varchar(4) = NULL,
	@CollegeID varchar(3) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	/*Store the current term STRM value*/ 
	DECLARE @CurrentSTRM varchar(4);
	SET @CurrentSTRM = (SELECT TOP 1(STRM)
						FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
						WHERE TERM_END_DT > GETDATE()
						ORDER BY TERM_END_DT)
						
    SELECT DISTINCT Title, DegreeLongTitle, DegreeShortTitle, ProgramTitle, Program.ProgramID, Program.ProgramVersionID, CollegeShortTitle, ProgramBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, ProgramEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, ProgramDisplay, College.CollegeID, PublishedProgram, PublishedProgramVersionID
	FROM Program 
	LEFT OUTER JOIN AreaOfStudyProgram
	ON Program.ProgramID = AreaOfStudyProgram.ProgramID
	LEFT OUTER JOIN AreaOfStudy
	ON AreaOfStudy.AreaOfStudyID = AreaOfStudyProgram.AreaOfStudyID
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = ProgramBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = ProgramEndSTRM
	LEFT OUTER JOIN College
	ON College.CollegeID = Program.CollegeID
	LEFT OUTER JOIN ProgramDegree
	ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID
	LEFT OUTER JOIN Degree
	ON Degree.DegreeID = ProgramDegree.DegreeID
	WHERE ((Title LIKE @AlphaCharacter + '%') OR (Title IS NULL AND ProgramTitle LIKE @AlphaCharacter + '%'))
	AND (@CollegeID IS NULL OR Program.CollegeID = @CollegeID)
	AND ((@STRM IS NOT NULL AND (ProgramBeginSTRM <= @STRM AND ProgramEndSTRM >= @STRM AND ProgramEndSTRM < @CurrentSTRM)) 
	OR (@STRM IS NULL AND (ProgramBeginSTRM < @CurrentSTRM AND ProgramEndSTRM < @CurrentSTRM)))
	ORDER BY Title, DegreeShortTitle, ProgramTitle, CollegeShortTitle, ProgramBeginSTRM DESC, PublishedProgram DESC;
END

GO
