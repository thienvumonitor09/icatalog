<%@ Page language="c#" Inherits="ICatalog.program.view" CodeFile="view.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
    <head>
        <title>View Programs</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/CPG.css" />
        <script type="text/javascript">
        <!--
            /*
			function launchCenter(url, name, height, width) {
				var str = "height=" + height + ",innerHeight=" + height;
				str += ",width=" + width + ",innerWidth=" + width;
				if (window.screen) {
					var ah = screen.availHeight - 30;
					var aw = screen.availWidth - 10;

					var xc = (aw - width) / 2;
					var yc = (ah - height) / 2;

					str += ",left=" + xc + ",screenX=" + xc;
					str += ",top=" + yc + ",screenY=" + yc;
					str += ",resizable=yes,scrollbars=yes";
				}
				return window.open(url, name, str);
            }
            */

            function popupCenter(url, title, h, w) {
                // Fixes dual-screen position  
                var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                var top = ((height / 2) - (h / 2)) + dualScreenTop;
                var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

                // Puts focus on the newWindow  
                if (window.focus) {
                    newWindow.focus();
                }
            }
		//-->
		</script>
    </head>
    <body>
		<asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmProgram" runat="server">
					<input type="hidden" id="hidProgramVersionID" runat="server" />
					<input type="hidden" id="hidTodo" runat="server" />
					<input type="hidden" id="hidLtr" runat="server" />
					<input type="hidden" id="hidProgramDisplay" runat="server" />
                    <input type="hidden" id="hidProgramBeginSTRM" runat="server" />
                    <input type="hidden" id="hidProgramEndSTRM" runat="server" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;View Programs</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
																	<asp:panel runat="server" id="panDisplay">
																		<table style="width:100%;" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="portletDark" style="width:100%;">
                                                                                    <asp:Table runat="server" ID="tblSearch" style="width:100%;" cellspacing="0" cellpadding="0">
                                                                                        <asp:tablerow>
																				            <asp:tablecell style="width:100%;font-weight:bold;padding:10px;" cssclass="portletMain">
                                                                                                Offered by:&nbsp;
																								<asp:dropdownlist id="cboOfferedAt" cssclass="small" runat="server" autopostback="true">
																									<asp:listitem value="">ALL</asp:listitem>
																								</asp:dropdownlist>&nbsp;&nbsp;
																								Term:&nbsp;
																								<asp:dropdownlist id="cboTerm" cssclass="small" runat="server" autopostback="true">
																									<asp:listitem value="">ALL</asp:listitem>
																								</asp:dropdownlist>
																							</asp:tablecell>
																			            </asp:tablerow>
                                                                                    </asp:Table>
																					<asp:table runat="server" id="tblAlpha" style="width:100%;" cellspacing="0" cellpadding="0"></asp:table>
																					<asp:table runat="server" id="tblDisplay" style="width:100%;" cellspacing="1" cellpadding="2"></asp:table>
																				</td>
																			</tr>
																		</table>
																	</asp:panel>
																	<!-- VIEW PROGRAM -->
																	<asp:panel id="panView" runat="server">
																		<table cellspacing="1" cellpadding="0" style="width:100%;">
																			<tr>
																				<td class="portletMain" style="padding:6px;padding-top:5px;">
																					<b>&nbsp;Last Modified by:</b>&nbsp;<asp:label runat="server" id="lblLastModifiedName"></asp:label>&nbsp;<b>on:</b>&nbsp;<asp:label runat="server" id="lblLastModifiedDate"></asp:label>
																				</td>
																			</tr>
																			<tr>
																				<td class="portletLight" style="width:100%;vertical-align:text-top;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<tr>
																							<td style="padding-bottom:15px;">
																								<table cellpadding="0" cellspacing="0" style="width:100%;">
																									<!-- DISPLAY PROGRAM TITLE -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Program Title:</b>&nbsp;
																											<asp:label id="lblProgramTitle" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- STATE APPROVAL DATE -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>State Approval Date:</b>&nbsp;
																											<asp:label id="lblPrimaryStateApproval" runat="server" cssclass="small"></asp:label>
																											&nbsp;&nbsp;<b>CIP:</b>&nbsp;
																											<asp:label id="lblPrimaryCIP" runat="server" cssclass="small"></asp:label>
																											&nbsp;&nbsp;<b>EPC:</b>&nbsp;
																											<asp:label id="lblPrimaryEPC" runat="server" cssclass="small"></asp:label>
                                                                                                            &nbsp;&nbsp;<b>Academic Plan:</b>&nbsp;
																											<asp:label id="lblPrimaryAcademicPlan" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- DISPLAY OFFERED AT -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Offered by:</b>&nbsp;
																											<asp:label id="lblOfferedAt" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- TERM -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Term:</b>&nbsp;&nbsp;&nbsp;
																											<b>Begin</b>&nbsp;&nbsp;<asp:label runat="server" id="lblBeginTerm"></asp:label>
																											&nbsp;&nbsp;&nbsp;<b>End</b>&nbsp;&nbsp;<asp:label runat="server" id="lblEndTerm"></asp:label>
																										</td>
																									</tr>
																									<!-- PRIMARY CATEGORY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Primary Category:</b>&nbsp;
																											<asp:label id="lblCategory" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- ADDITIONAL CATEGORIES -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Additional Categories:</b>&nbsp;
																											<asp:label id="lblAddCategories" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- DISPLAY COMPLETION AWARD -->
																									<asp:panel id="panDegTitle" runat="server">
																										<tr>
																											<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																												<b>Completion Award:</b>&nbsp;
																												<asp:label id="lblDegTitle" runat="server" cssclass="small"></asp:label>
																											</td>
																										</tr>
																									</asp:panel>
																									<!-- DISPLAY PROGRAM DESCRIPTION -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Program Description:</b>&nbsp;
																										</td>
																									</tr>
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;width:100%;padding-top:5px;">
																											<asp:label id="lblProgramDescription" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- DISPLAY PROGRAM OUTLINE -->
																									<asp:panel id="panOutline" runat="server">
																										<tr>
																											<td style="padding-left:10px;padding-right:10px;padding-top:15px;width:100%;">
																												<asp:table runat="server" id="tblProgOutline" cellpadding="0" cellspacing="0" style="width:100%;"></asp:table>	
																											</td>
																										</tr>
																									</asp:panel>	
																									<!-- DISPLAY PROGRAM TEXT -->
																									<asp:panel id="panTextDisplay" runat="server">
																										<!-- DISPLAY PROGRAM TEXT -->
																										<tr>
																											<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																												<b>Program Text:</b>
																											</td>
																										</tr>
																										<tr>
																											<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																												<asp:label id="lblProgText" runat="server" cssclass="small"></asp:label>
																											</td>
																										</tr>
																									</asp:panel>														
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</asp:panel>
																</td>
															</tr>
															<asp:panel id="panButtons" runat="server">
																<tr>
																	<td class="portletMain" style="text-align:center;padding:3px;">
																		<asp:button id="cmdBack" runat="server" text="Back" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:100px;" onclick="cmdBack_Click"></asp:button>
																		&nbsp;<asp:button id="cmdCPG" runat="server" text="View CPG" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:100px;" onclick="cmdCPG_Click"></asp:button>
																	</td>
																</tr>
															</asp:panel>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</form>
			</asp:panel>
			<div class="clearer"></div>
		</asp:panel>
		<asp:panel id="panCPG" runat="server">
				<script type="text/javascript">
				<!--
					document.body.style.backgroundColor = "#ffffff";
					document.body.style.backgroundImage = "url('')";
					function window.onbeforeprint(){
						document.getElementById("buttons").style.display = "none";
					}
					function window.onafterprint(){
						document.getElementById("buttons").style.display = "block";
					}
				//-->
				</script>
				<div style="width:660px;text-align:right;" id="buttons">
					<a style="font-size:10pt;color:blue;" href="javascript:history.back(1);">Back</a> | <a style="font-size:10pt;color:blue;" href="javascript:window.print();">Print</a>
				</div>
				<table align="center" cellpadding="0" cellspacing="0">
					<tr>
						<td style="background:#000000;">
							<asp:table id="tblCPGFront" runat="server" cellpadding="6" cellspacing="1" style="width:660px;height:910px;"></asp:table>
						</td>
					</tr>
				</table>
				<br class="breakhere" />
				<table align="center" cellpadding="0" cellspacing="0">
					<tr>
						<td style="background:#000000;">
							<asp:table id="tblCPGBack" runat="server" cellpadding="8" cellspacing="1" style="width:660px;"></asp:table>
						</td>
					</tr>
				</table>
				<asp:panel id="panCPGBack2" runat="server">
					<br class="breakhere" />
					<table align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td style="background:#000000;">
								<asp:table id="tblCPGBack2" runat="server" cellpadding="8" cellspacing="1" style="width:660px;"></asp:table>
							</td>
						</tr>
					</table>
				</asp:panel>
			</asp:panel>
	</body>
</html>
