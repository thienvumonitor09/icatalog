USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseCatalogNumbers]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 06/10/2019
-- Description:	Returns active course catalog numbers by term, college, and subject area.
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseCatalogNumbers]
	@STRM varchar(4),
	@SUBJECT varchar(5) = NULL,
	@INSTITUITION varchar(5) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT CATALOG_NBR
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
		ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	WHERE CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM))
	AND CATALOG_PRINT = 'Y'
	AND EFF_STATUS = 'A'
	AND ACAD_CAREER = 'UGRD'
	AND (@Subject IS NULL OR (SUBJECT = @Subject OR SUBJECT = @Subject + '&'))
	AND (@INSTITUITION IS NULL OR INSTITUTION = @INSTITUITION)
	GROUP BY CATALOG_NBR
	ORDER BY CATALOG_NBR;
END
GO
