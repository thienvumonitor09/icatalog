USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_iSchdInstrRmD_LookupInstrId]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kamori Cattadoris
-- Create date: October 25, 2012
-- Description:	Lookup alternate instructor ID (SSN) by class ID and college code
-- Used in student ISAC submission form
-- =============================================
CREATE PROCEDURE [dbo].[usp_iSchdInstrRmD_LookupInstrId]
	@ClassID char(8)
	,@ColCd char(3)
	,@InstrID char(9) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT @InstrID = [InstrId] 
	FROM [CCSiCatalog].[dbo].[iSchdInstrRmD]
	WHERE ClassId = @ClassID AND ColCd = @ColCd
END
GO
