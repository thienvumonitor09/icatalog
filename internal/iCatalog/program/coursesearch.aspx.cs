using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program
{
	/// <summary>
	/// Summary description for coursecheck.
	/// </summary>
	public partial class coursesearch : System.Web.UI.Page
	{

		courseData csCourse = new courseData();
		programData csProgram = new programData();
		termData csTerm = new termData();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			#region CHECK USER LOGIN

			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			#endregion

			String strSearchText = "";
			String strSearchId = "";
			String strSearchDept = "";
			String searchSTRM = "";

			if(!IsPostBack){
				panResults.Visible = false;
				optCrsSearch.Checked = true;
				
				strSearchText = Request.QueryString["searchtxt"];
				strSearchId = Request.QueryString["searchid"];
				strSearchDept = Request.QueryString["searchsub"];
				searchSTRM = Request.QueryString["searchstrm"];

                /*
				DataSet dsCrsDept = csCourse.GetSubjects();
				for(Int32 intDSRow = 0; intDSRow < dsCrsDept.Tables[0].Rows.Count; intDSRow++){
					if(Convert.ToInt32(dsCrsDept.Tables[0].Rows[intDSRow]["CrsCount"]) > 0){
						cboCrsDept.Items.Add(dsCrsDept.Tables[0].Rows[intDSRow]["SUBJECT"].ToString());
					}
				}
                */
                cboCrsDept.DataSource = csCourse.GetSubjects().Tables[0];
                cboCrsDept.DataValueField = "SUBJECT";
                //cboCrsDept.DataTextField = "SUBJECT";
                cboCrsDept.DataBind();

                try
                {
					if(strSearchDept != null && strSearchDept != ""){
						cboCrsDept.SelectedValue = strSearchDept;
					}
				}catch{
					//do nothing
				}
			}
            /*
			cboCatalogNbr.Items.Clear();
			DataSet dsCatalogNbr = csCourse.GetCourseOfferings(cboCrsDept.SelectedValue);
			for(Int32 intDSRow = 0; intDSRow < dsCatalogNbr.Tables[0].Rows.Count; intDSRow++){
                //cboCatalogNbr.Items.Add(new ListItem(dsCatalogNbr.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString() + dsCatalogNbr.Tables[0].Rows[intDSRow]["CourseSuffix"].ToString(), dsCatalogNbr.Tables[0].Rows[intDSRow]["CourseOffering"].ToString()));
                cboCatalogNbr.Items.Add(new ListItem(dsCatalogNbr.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString(), dsCatalogNbr.Tables[0].Rows[intDSRow]["CourseOffering"].ToString()));
            }
            */
            cboCatalogNbr.DataSource = csCourse.GetCourseOfferings(cboCrsDept.SelectedValue).Tables[0];
            cboCatalogNbr.DataValueField = "CourseOffering";
            cboCatalogNbr.DataTextField = "CATALOG_NBR";
            cboCatalogNbr.DataBind();

            try
            {
				if(strSearchId != null && strSearchId != ""){
					cboCatalogNbr.SelectedValue = strSearchId;
				}else{
					cboCatalogNbr.SelectedValue = Request.Form["cboCatalogNbr"];
				}
			}catch{
				//do nothing
			}

            /*
			cboTerm.Items.Clear();
			cboTerm.Items.Add("");
			DataSet dsTerm = csCourse.GetCourseOfferingTerms(cboCatalogNbr.SelectedValue);
			for(Int32 intDSRow = 0; intDSRow < dsTerm.Tables[0].Rows.Count; intDSRow++){
				//cboTerm.Items.Add(new ListItem(dsTerm.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString() + " - " + dsTerm.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString(),  dsTerm.Tables[0].Rows[intDSRow]["CourseBeginSTRM"].ToString() + "," + dsTerm.Tables[0].Rows[intDSRow]["CourseEndSTRM"].ToString()));
			}
            */
            //cboTerm.Items.Add("");
            DataTable dtTerm = csCourse.GetCourseOfferingTerms(cboCatalogNbr.SelectedValue).Tables[0];
            dtTerm.Columns.Add("FormatEFFDT", typeof(string));
            foreach (DataRow drTerm in dtTerm.Rows)
            {
                drTerm["FormatEFFDT"] = Convert.ToDateTime(drTerm["EFFDT"]).ToShortDateString();
            }
            cboTerm.DataSource = dtTerm;
            cboTerm.DataValueField = "FormatEFFDT";

            //cboTerm.DataTextField = "EFFDT";
            cboTerm.DataBind();
            cboTerm.Items.Insert(0, new ListItem("", ""));

            try
            {
				if(searchSTRM != null && searchSTRM != ""){
					cboTerm.SelectedValue = searchSTRM;
				}else{
					cboTerm.SelectedValue = Request.Form["cboTerm"];
				}
			}catch{
				//do nothing
			}

			if(strSearchText != null && strSearchText != ""){
				optTxtSearch.Checked = true;
				optCrsSearch.Checked = false;
				txtTextSearch.Text = strSearchText;
				hidTodo.Value = "search";
			}else if(strSearchId != null && strSearchId != ""){
				optCrsSearch.Checked = true;
				optTxtSearch.Checked = false;
				hidTodo.Value = "search";
			}
			
			if(hidTodo.Value == "deleteProgram"){
				//delete the selected program
				csProgram.DeleteProgram(Convert.ToInt32(hidProgramVersionID.Value));
				hidTodo.Value = "";

			}else if(hidTodo.Value == "search"){
				if(optCrsSearch.Checked){
					DataSet dsPrograms = csProgram.GetProgramsContainingCourse(cboCatalogNbr.SelectedValue, cboTerm.SelectedValue);
					Int32 intProgCount = dsPrograms.Tables[0].Rows.Count;
					TableRow tr = new TableRow();
					TableCell td = new TableCell();
					if(intProgCount > 0){
						tr = new TableRow();
						td = new TableCell();
						td.Attributes["style"] = "padding-left:5px;padding-top:4px;padding-bottom:4px;";
						td.CssClass = "portletMain";
						td.ColumnSpan = 6;
						if(cboTerm.SelectedValue == ""){
							td.Controls.Add(new LiteralControl("The following programs contain " + cboCatalogNbr.SelectedValue + "."));
						}else{
							td.Controls.Add(new LiteralControl("The following programs contain " + cboCatalogNbr.SelectedValue + " effective " + cboTerm.SelectedItem.Text + "."));
						}
						tr.Cells.Add(td);
						tblDisplay.Rows.Add(tr);

						tr = new TableRow();

                        td = new TableCell();
                        td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:105px;";
                        td.CssClass = "portletSecondary";
                        td.Controls.Add(new LiteralControl("Award Type"));

                        tr.Cells.Add(td);

						td = new TableCell();
                        td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:243px;";
						td.CssClass = "portletSecondary";
						td.Controls.Add(new LiteralControl("Program Title"));

						tr.Cells.Add(td);

						td = new TableCell();
                        td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;white-space:nowrap;width:63px;";
						td.CssClass = "portletSecondary";
						td.Controls.Add(new LiteralControl("Offered by"));

						tr.Cells.Add(td);

						td = new TableCell();
                        td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;white-space:nowrap;width:158px;";
						td.CssClass = "portletSecondary";
						td.Controls.Add(new LiteralControl("Term"));

						tr.Cells.Add(td);

						td = new TableCell();
                        td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:82px;";
						td.CssClass = "portletSecondary";
						td.Controls.Add(new LiteralControl("Status"));

						tr.Cells.Add(td);

						td = new TableCell();
						td.Attributes["style"] = "width:72px;";
						td.CssClass = "portletSecondary";
						tr.Cells.Add(td);
						tblDisplay.Rows.Add(tr);

                        String oldAreaOfStudyTitle = "";

						for(Int32 intDSRow = 0; intDSRow < dsPrograms.Tables[0].Rows.Count; intDSRow++){		
						
							Int32 programVersionID = Convert.ToInt32(dsPrograms.Tables[0].Rows[intDSRow]["ProgramVersionID"]);
                            Int32 programID = Convert.ToInt32(dsPrograms.Tables[0].Rows[intDSRow]["ProgramID"]);
							String programTitle = dsPrograms.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString();
							String publishedProgram = dsPrograms.Tables[0].Rows[intDSRow]["PublishedProgram"].ToString();
							String publishedProgramVersionID = dsPrograms.Tables[0].Rows[intDSRow]["PublishedProgramVersionID"].ToString();
							String collegeShortTitle = dsPrograms.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
							String programBeginSTRM = dsPrograms.Tables[0].Rows[intDSRow]["ProgramBeginSTRM"].ToString();
							String programEndSTRM = dsPrograms.Tables[0].Rows[intDSRow]["ProgramEndSTRM"].ToString();
                            String beginTerm = dsPrograms.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString();
                            String endTerm = dsPrograms.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString();
							String programDisplay = dsPrograms.Tables[0].Rows[intDSRow]["ProgramDisplay"].ToString();
                            String degreeShortTitle = dsPrograms.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
                            String areaOfStudyTitle = dsPrograms.Tables[0].Rows[intDSRow]["Title"].ToString();

							if(publishedProgram == "0"){
								publishedProgram = "Working Copy";
							}else if(publishedProgram == "1"){
								publishedProgram = "Published Copy";
							}

                            if (areaOfStudyTitle != oldAreaOfStudyTitle) {
                                tr = new TableRow();
                                td = new TableCell();
                                td.CssClass = "portletLight";
                                td.Attributes["style"] = "font-weight:bold;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;width:100%;";
                                td.ColumnSpan = 6;
                                td.Controls.Add(new LiteralControl(areaOfStudyTitle));
                                tr.Cells.Add(td);
                                tblDisplay.Rows.Add(tr);
                            }

							tr = new TableRow();

                            td = new TableCell();
                            td.CssClass = "portletLight";
                            td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:105px;";
                            td.Controls.Add(new LiteralControl(degreeShortTitle));

                            tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:243px;";
							td.Controls.Add(new LiteralControl(programTitle));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:63px;";
							td.Controls.Add(new LiteralControl(collegeShortTitle));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:158px;";
							td.Controls.Add(new LiteralControl(beginTerm + " - " + endTerm));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:82px;";
							td.Controls.Add(new LiteralControl(publishedProgram));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;white-space:nowrap;width:72px;";
							if(!csProgram.HasWorkingProgramCopy(programVersionID)){
								if(csProgram.ExpiredSTRM(programEndSTRM, csTerm.GetCurrentTerm())){	
									td.Controls.Add(new LiteralControl("<a href=\"view.aspx?id=" + programID + "&pvid=" + programVersionID + "&pd=" + programDisplay + "&searchsub=" + cboCrsDept.SelectedValue.Replace("&","%26") + "&searchid=" + cboCatalogNbr.SelectedValue.Replace("&","%26") + "&searchstrm=" + cboTerm.SelectedValue + "\">View</a>"));
								}else{
									td.Controls.Add(new LiteralControl("&nbsp;<a href=\"edit.aspx?id=" + programID + "&pvid=" + programVersionID + "&ppvid=" + publishedProgramVersionID + "&searchsub=" + cboCrsDept.SelectedValue.Replace("&","%26") + "&searchid=" + cboCatalogNbr.SelectedValue.Replace("&","%26") + "&searchstrm=" + cboTerm.SelectedValue + "\">Edit</a>"));
								}
								td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete the " + publishedProgram + " \\n" + programTitle.Replace("'","\\'") + " program offered at " + collegeShortTitle + " \\neffective " + beginTerm + " - " + endTerm + "?')){document.frmProgram.hidTodo.value='deleteProgram';document.frmProgram.hidProgramVersionID.value='" + programVersionID + "';document.frmProgram.submit();}\">Delete</a>"));
							}else{
								td.Controls.Add(new LiteralControl("<a href=\"view.aspx?id=" + programID + "&pvid=" + programVersionID + "&pd=" + programDisplay + "&searchsub=" + cboCrsDept.SelectedValue.Replace("&","%26") + "&searchid=" + cboCatalogNbr.SelectedValue.Replace("&","%26") + "&searchstrm=" + cboTerm.SelectedValue + "\">View</a>"));
							}
							tr.Cells.Add(td);
							tblDisplay.Rows.Add(tr);

                            oldAreaOfStudyTitle = areaOfStudyTitle;
						}
					}else{
						tr = new TableRow();
						td = new TableCell();
						td.Attributes["style"] = "padding-left:5px;padding-top:4px;padding-bottom:4px;";
						td.CssClass = "portletMain";
						td.ColumnSpan = 6;
						if(cboTerm.SelectedValue == ""){
							td.Controls.Add(new LiteralControl("No programs were found containing " + cboCatalogNbr.SelectedValue + "."));
						}else{
							td.Controls.Add(new LiteralControl("No programs were found containing " + cboCatalogNbr.SelectedValue + " effective " + cboTerm.SelectedItem.Text + "."));
						}
						tr.Cells.Add(td);
						tblDisplay.Rows.Add(tr);
					}

					panResults.Visible = true;

				}else if(optTxtSearch.Checked){

					DataSet dsPrograms = csProgram.GetProgramsContainingText(txtTextSearch.Text);
					Int32 intProgCount = dsPrograms.Tables[0].Rows.Count;
					TableRow tr = new TableRow();
					TableCell td = new TableCell();
					if(intProgCount > 0){
						tr = new TableRow();
						td = new TableCell();
						td.Attributes["style"] = "padding-left:5px;padding-top:4px;padding-bottom:4px;";
						td.CssClass = "portletMain";
						td.ColumnSpan = 6;
						td.Controls.Add(new LiteralControl("The following programs contain " + txtTextSearch.Text + "."));
						tr.Cells.Add(td);
						tblDisplay.Rows.Add(tr);

						tr = new TableRow();

                        td = new TableCell();
                        td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:105px;";
                        td.CssClass = "portletSecondary";
                        td.Controls.Add(new LiteralControl("Award Type"));

                        tr.Cells.Add(td);

						td = new TableCell();
                        td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:243px;";
						td.CssClass = "portletSecondary";
						td.Controls.Add(new LiteralControl("Program Title"));

						tr.Cells.Add(td);

						td = new TableCell();
                        td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:63px;";
						td.CssClass = "portletSecondary";
						td.Controls.Add(new LiteralControl("Offered by"));

						tr.Cells.Add(td);

						td = new TableCell();
                        td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;white-space:nowrap;width:158px;";
						td.CssClass = "portletSecondary";
						td.Controls.Add(new LiteralControl("Term"));

						tr.Cells.Add(td);

						td = new TableCell();
                        td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:82px;";
						td.CssClass = "portletSecondary";
						td.Controls.Add(new LiteralControl("Status"));

						tr.Cells.Add(td);

						td = new TableCell();
						td.Attributes["style"] = "width:72px;";
						td.CssClass = "portletSecondary";
						tr.Cells.Add(td);
						tblDisplay.Rows.Add(tr);

                        String oldAreaOfStudyTitle = "";

						for(Int32 intDSRow = 0; intDSRow < dsPrograms.Tables[0].Rows.Count; intDSRow++){		
						
							Int32 programVersionID = Convert.ToInt32(dsPrograms.Tables[0].Rows[intDSRow]["ProgramVersionID"]);
                            Int32 programID = Convert.ToInt32(dsPrograms.Tables[0].Rows[intDSRow]["ProgramID"]);
							String programTitle = dsPrograms.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString();
							String publishedProgram = dsPrograms.Tables[0].Rows[intDSRow]["PublishedProgram"].ToString();
							String publishedProgramVersionID = dsPrograms.Tables[0].Rows[intDSRow]["PublishedProgramVersionID"].ToString();
							String collegeShortTitle = dsPrograms.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
							String programBeginSTRM = dsPrograms.Tables[0].Rows[intDSRow]["ProgramBeginSTRM"].ToString();
							String programEndSTRM = dsPrograms.Tables[0].Rows[intDSRow]["ProgramEndSTRM"].ToString();
                            String beginTerm = dsPrograms.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString();
                            String endTerm = dsPrograms.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString();
							String programDisplay = dsPrograms.Tables[0].Rows[intDSRow]["ProgramDisplay"].ToString();
                            String degreeShortTitle = dsPrograms.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
                            String areaOfStudyTitle = dsPrograms.Tables[0].Rows[intDSRow]["Title"].ToString();

							if(publishedProgram == "0"){
								publishedProgram = "Working Copy";
							}else if(publishedProgram == "1"){
								publishedProgram = "Published Copy";
							}

                            if (areaOfStudyTitle != oldAreaOfStudyTitle) {
                                tr = new TableRow();
                                td = new TableCell();
                                td.CssClass = "portletLight";
                                td.Attributes["style"] = "font-weight:bold;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;width:100%;";
                                td.ColumnSpan = 6;
                                td.Controls.Add(new LiteralControl(areaOfStudyTitle));
                                tr.Cells.Add(td);
                                tblDisplay.Rows.Add(tr);
                            }

							tr = new TableRow();

                            td = new TableCell();
                            td.CssClass = "portletLight";
                            td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:105px;";
                            td.Controls.Add(new LiteralControl(degreeShortTitle));

                            tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:243px;";
							td.Controls.Add(new LiteralControl(programTitle));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:63px;";
							td.Controls.Add(new LiteralControl(collegeShortTitle));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:158px;";
							td.Controls.Add(new LiteralControl(beginTerm + " - " + endTerm));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "text-align:center;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:82px;";
							td.Controls.Add(new LiteralControl(publishedProgram));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;white-space:nowrap;width:72px;";
							if(!csProgram.HasWorkingProgramCopy(programVersionID)){
								if(csProgram.ExpiredSTRM(programEndSTRM, csTerm.GetCurrentTerm())){	
									td.Controls.Add(new LiteralControl("<a href=\"view.aspx?id=" + programID + "&pvid=" + programVersionID + "&pd=" + programDisplay + "&searchtxt=" + txtTextSearch.Text.Replace("&","%26") + "\">View</a>"));
								}else{
									td.Controls.Add(new LiteralControl("&nbsp;<a href=\"edit.aspx?id=" + programID + "&pvid=" + programVersionID + "&ppvid=" + publishedProgramVersionID + "&searchtxt=" + txtTextSearch.Text.Replace("&","%26") + "\">Edit</a>"));
								}
								td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete the " + publishedProgram + " \\n" + programTitle.Replace("'","\\'") + " program offered at " + collegeShortTitle + " \\neffective " + beginTerm + " - " + endTerm + "?')){document.frmProgram.hidTodo.value='deleteProgram';document.frmProgram.hidProgramVersionID.value='" + programVersionID + "';document.frmProgram.submit();}\">Delete</a>"));
							}else{
								td.Controls.Add(new LiteralControl("<a href=\"view.aspx?id=" + programID + "&pvid=" + programVersionID + "&pd=" + programDisplay + "&searchtxt=" + txtTextSearch.Text.Replace("&","%26") + "\">View</a>"));
							}
							tr.Cells.Add(td);
							tblDisplay.Rows.Add(tr);

                            oldAreaOfStudyTitle = areaOfStudyTitle;

						}
					}else{
						tr = new TableRow();
						td = new TableCell();
						td.Attributes["style"] = "padding-left:5px;padding-top:4px;padding-bottom:4px;";
						td.CssClass = "portletMain";
						td.ColumnSpan = 6;
						td.Controls.Add(new LiteralControl("No programs were found containing " + txtTextSearch.Text + "."));
						tr.Cells.Add(td);
						tblDisplay.Rows.Add(tr);
					}

					panResults.Visible = true;
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
