﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

public partial class program_areaofstudy_add : System.Web.UI.Page {

    programData csProgram = new programData();

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            panError.Visible = false;
            panConfirm.Visible = false;
        }

        //create the guided pathway checkboxes
        DataSet dsPathways = csProgram.GetPathways(optOfferedAt.SelectedValue);
        Int32 intPathwayCount = dsPathways.Tables[0].Rows.Count;

        panPathway.Controls.Clear();
        cboPathway.Items.Clear();
        if (intPathwayCount > 0) {
            cboPathway.Items.Add("");
            for (Int32 intDSRow = 0; intDSRow < intPathwayCount; intDSRow++) {
                String title = dsPathways.Tables[0].Rows[intDSRow]["PathwayTitle"].ToString();
                String pathwayID = dsPathways.Tables[0].Rows[intDSRow]["PathwayID"].ToString();

                HtmlInputCheckBox chkPathway = new HtmlInputCheckBox();
                chkPathway.Value = pathwayID;
                chkPathway.ID = "chkPathway" + pathwayID;
                chkPathway.Name = "chkPathway";
                chkPathway.Attributes["onclick"] = "addPathway('" + chkPathway.ID + "','" + pathwayID + "');";
                panPathway.Controls.Add(new LiteralControl("<div>"));
                panPathway.Controls.Add(chkPathway);
                panPathway.Controls.Add(new LiteralControl("&nbsp;" + title + "</div>"));
                cboPathway.Items.Add(new ListItem(title, pathwayID));
            }
        } else {
            cboPathway.Items.Add(new ListItem("No guided pathways currently exist", ""));
        }
    }

    protected void cmdSubmit_Click(object sender, System.EventArgs e) {
        String areaOfStudyTitle = txtAreaOfStudyTitle.Text;
        String webPageURL = txtWebPageURL.Text;
        String pathwayList = Request.Form["hidPathwayList"];
        String primaryPathway = Request.Form["cboPathway"];

        Int32 areaOfStudyID = csProgram.AddAreaOfStudy(optOfferedAt.SelectedValue, primaryPathway, areaOfStudyTitle, webPageURL);

        if (areaOfStudyID == 0) {
            lblErrorMsg.Text = "Error: The insert failed.";
            cboPathway.SelectedValue = primaryPathway;
            panError.Visible = true;
            panAdd.Visible = true;
        } else if (areaOfStudyID == -1) {
            lblErrorMsg.Text = "Error: The area of study entered already exists.";
            cboPathway.SelectedValue = primaryPathway;
            panError.Visible = true;
            panAdd.Visible = true;
        } else {

            if (pathwayList != "") {
                String[] aryPathwayList = pathwayList.Split(',');
                for (Int16 i = 0; i < aryPathwayList.Length; i++) {
                    if (primaryPathway != aryPathwayList[i]) {
                        csProgram.AddAreaOfStudyPathway(areaOfStudyID, Convert.ToInt32(aryPathwayList[i]));
                    }
                }
            }

            DataSet dsPathways = csProgram.GetAreaOfStudyPathways(areaOfStudyID);
            lblAddPathways.Text = "";
            for (Int32 intDSRow = 0; intDSRow < dsPathways.Tables[0].Rows.Count; intDSRow++) {
                if (Convert.ToByte(dsPathways.Tables[0].Rows[intDSRow]["PrimaryPathway"]) == 1) {
                    lblPathway.Text = dsPathways.Tables[0].Rows[intDSRow]["PathwayTitle"].ToString();
                } else {
                    lblAddPathways.Text += "; " + dsPathways.Tables[0].Rows[intDSRow]["PathwayTitle"].ToString();
                }
            }

            if (lblAddPathways.Text.IndexOf(";") == 0) {
                lblAddPathways.Text = lblAddPathways.Text.Substring(1, lblAddPathways.Text.Length - 1);
            }

            lblOfferedBy.Text = optOfferedAt.SelectedItem.Text;
            lblAreaOfStudyTitle.Text = areaOfStudyTitle;
            lblWebPageURL.Text = webPageURL;
            panConfirm.Visible = true;
            panAdd.Visible = false;
        }
    }
}