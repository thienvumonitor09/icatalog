<%@ Reference Page="~/program/cpg/view.aspx" %>
<%@ Page language="c#" Inherits="ICatalog.program.cpg.edit" ValidateRequest="false" CodeFile="edit.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Career Planning Guide Maintenance</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/CPG.css" />
		<script type="text/javascript" src="../../_phatt3_src_files/trim.js"></script>
		<script type="text/javascript" src="https://internal.spokane.edu/ckeditor_4/ckeditor.js"></script>
		<script type="text/javascript">
        <!-- 
            /*
            function launchCenter(url, name, height, width) {
				var str = "height=" + height + ",innerHeight=" + height;
				str += ",width=" + width + ",innerWidth=" + width;
				if (window.screen) {
					var ah = screen.availHeight - 30;
					var aw = screen.availWidth - 10;

					var xc = (aw - width) / 2;
					var yc = (ah - height) / 2;

					str += ",left=" + xc + ",screenX=" + xc;
					str += ",top=" + yc + ",screenY=" + yc;
					str += ",resizable=yes,scrollbars=yes";
				}
				return window.open(url, name, str);
            }
            */

            function popupCenter(url, title, h, w) {
                // Fixes dual-screen position  
                var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                var top = ((height / 2) - (h / 2)) + dualScreenTop;
                var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

                // Puts focus on the newWindow  
                if (window.focus) {
                    newWindow.focus();
                }
            }

        //-->
		</script>
	</head>
	<body>
		<asp:panel id="container" runat="server">
			<asp:panel id="header" runat="server">
				<uc1:header id="Header1" runat="server"></uc1:header>
			</asp:panel>
			<asp:panel id="sidemenu" runat="server">
				<uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
			</asp:panel>
			<asp:panel id="content" runat="server">
				<form id="frmProgram" runat="server">
					<input type="hidden" id="hidTodo" runat="server" /> 
					<input type="hidden" id="hidLtr" runat="server" />
					<input type="hidden" id="hidProgramVersionID" runat="server" />
					<input type="hidden" id="hidCareerPlanningGuideID" runat="server" />
					<input type="hidden" id="hidFocus" />
					<input type="hidden" id="hidPublishedCareerPlanningGuideID" runat="server" />
					<table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
						<tr>
							<td style="padding-left:4px;padding-right:0px;">
								<table style="width:100%;" cellpadding="0" cellspacing="0">
									<tr>
										<td style="background-color:#000000;">
											<table style="width:100%;" cellpadding="1" cellspacing="1">
												<tr>
													<td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;"
														class="portletHeader">
														<table style="width:100%;" cellpadding="0" cellspacing="0">
															<tr>
																<td class="portletHeader" style="width:100%;">&nbsp;Edit/Delete Career Planning Guide</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="portletMain" style="width:100%">
														<asp:panel runat="server" id="panDisplay">
                                                            <table style="width:100%;" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="portletDark" style="width:100%;">
                                                                        <asp:Table runat="server" ID="tblSearch" style="width:100%;" cellspacing="0" cellpadding="0">
                                                                            <asp:tablerow>
																			    <asp:tablecell style="width:100%;font-weight:bold;padding:10px;" cssclass="portletMain">
                                                                                    Offered by:&nbsp;
																					<asp:dropdownlist id="cboOfferedAt" cssclass="small" runat="server" autopostback="true">
																						<asp:listitem value="">ALL</asp:listitem>
																					</asp:dropdownlist>&nbsp;&nbsp;
																					Term:&nbsp;
																					<asp:dropdownlist id="cboTerm" cssclass="small" runat="server" autopostback="true">
																						<asp:listitem value="">ALL</asp:listitem>
																					</asp:dropdownlist>
                                                                                </asp:tablecell>
																		    </asp:tablerow>
                                                                        </asp:Table>
                                                                        <asp:table runat="server" id="tblAlpha" style="width:100%;" cellspacing="0" cellpadding="0"></asp:table>
                                                                        <asp:table runat="server" id="tblDisplay" style="width:100%;" cellspacing="1" cellpadding="2"></asp:table>
																	</td>
																</tr>
															</table>
														</asp:panel>
														<asp:panel runat="server" id="panEdit">
															<table style="width:100%;" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="portletDark" style="width:100%;">
																		<table cellspacing="1" cellpadding="0" style="width:100%;">
																			<tr>
																				<td class="portletLight" style="width:100%;vertical-align:text-top;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<!-- Error Message -->
																						<span runat="server" id="panError">
																							<tr>
																								<td colspan="2" style="color:red;padding-left:10px;padding-top:15px;">
																									<asp:label runat="server" id="lblErrorMsg"></asp:label>
																								</td>
																							</tr>
																						</span>
																						<tr>
																							<td style="padding-left:5px;padding-right:5px;padding-bottom:14px;">
																								<table cellpadding="0" cellspacing="0" style="width:100%;">
																									<tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Program Title:</b>&nbsp;
																										</td>
																										<td style="padding-top:15px;">
																											<asp:label runat="server" id="lblProgramTitle" cssclass="small"></asp:label>
																										</td>
																									</tr>
                                                                                                    <tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Offered by:</b>&nbsp;
																										</td>
																										<td style="padding-top:15px;">
																											<asp:label runat="server" id="lblOfferedAt" cssclass="small"></asp:label>
																										</td>
																									</tr>
                                                                                                    <tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Term:</b>&nbsp;
																										</td>
																										<td style="padding-top:15px;">
																											<b>Begin</b>&nbsp;<asp:label id="lblProgramBeginSTRM" cssclass="small" runat="server"></asp:label>&nbsp;&nbsp;&nbsp;
																											<b>End</b>&nbsp;<asp:label id="lblProgramEndSTRM" cssclass="small" runat="server"></asp:label>
																										</td>
																									</tr>
                                                                                                    <tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Completion Award:</b>&nbsp;
																										</td>
																										<td style="padding-top:15px;">
																											<asp:label runat="server" id="lblDegree" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Category:</b>&nbsp;
																										</td>
																										<td style="padding-top:15px;">
																											<asp:label runat="server" id="lblCategory" cssclass="small"></asp:label>
																										</td>
																									</tr>
                                                                                                    <tr>
																										<td style="width:125px;padding-left:5px;padding-top:10px;text-align:right;">
																											<b>Status:</b>&nbsp;
																										</td>
																										<td style="padding-top:10px;">
																											<asp:radiobuttonlist id="optPublishedCareerPlanningGuide" runat="server" repeatdirection="horizontal">
																												<asp:listitem Value="0" selected="true">Working Copy</asp:listitem>
																												<asp:listitem Value="1">Published Copy</asp:listitem>
																											</asp:radiobuttonlist>
																										</td>
																									</tr>
																									<tr>
																										<td style="width:125px;padding-left:5px;padding-top:10px;text-align:right;">
																											<b>Enrollment:</b>&nbsp;
																										</td>
																										<td style="padding-top:10px;">
																											<asp:textbox runat="server" id="txtProgramEnrollment" columns="50" maxlength="75" cssclass="small"></asp:textbox>
																										</td>
																									</tr>
																									<tr>
																										<td style="width:125px;padding-left:5px;padding-top:10px;text-align:right;">
																											<b>Program URL:</b>&nbsp;
																										</td>
																										<td style="padding-top:10px;">
																											<asp:textbox runat="server" id="txtProgramWebsiteURL" columns="75" maxlength="100" cssclass="small"></asp:textbox>
																										</td>
																									</tr>
																									<tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>CPG Format:</b>&nbsp;
																										</td>
																										<td style="padding-top:15px;">
																											<asp:dropdownlist runat="server" id="cboCareerPlanningGuideFormat" cssclass="small"></asp:dropdownlist>
																										</td>
																									</tr>
																									<tr>
																										<td colspan="2" style="width:100%;padding-top:15px;">
																											<div style="padding-bottom:2px;"><strong>Program Course of Study (optional)</strong></div>
																											<textarea name="txtProgramCourseOfStudy" id="txtProgramCourseOfStudy" runat="server" class="defaultText" cols="10" rows="5"></textarea>
																											<script type="text/javascript">
																											    CKEDITOR.replace('txtProgramCourseOfStudy',
																												{
																												    customConfig: '/iCatalog/ckeditor_config.js',
																												    toolbar:
																													[
																														{ name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', '-',
																														'NumberedList', 'BulletedList', '-',
																														'SpecialChar', '-',
                                                                                                                        'Link', 'Unlink', '-',
																														'SpellChecker', 'Scayt']
																														}
																													],
																												    height: "150px"
																												});
																											</script>
																										</td>
																									</tr>
																									<tr>
																										<td colspan="2" style="width:100%;padding-top:15px;">
																											<div style="padding-bottom:2px;"><strong>Program Learning Outcomes (optional)</strong></div>
																											<textarea name="txtProgramGoals" id="txtProgramGoals" runat="server" class="defaultText" cols="10" rows="5"></textarea>
																											<script type="text/javascript">
																											    CKEDITOR.replace('txtProgramGoals',
																												{
																												    customConfig: '/iCatalog/ckeditor_config.js',
																												    toolbar:
																													[
																														{ name: 'baslicstyles', items: ['Bold', 'Italic', 'Underline', '-',
																														'NumberedList', 'BulletedList', '-',
																														'SpecialChar', '-',
                                                                                                                        'Link', 'Unlink', '-',
																														'SpellChecker', 'Scayt']
																														}
																													],
																												    height: "150px"
																												});
																											</script>
																										</td>
																									</tr>
																									<tr>
																										<td colspan="2" style="width:100%;padding-top:15px;">
																											<div style="padding-bottom:2px;"><strong>Program Career Opportunities (optional)</strong></div>
																											<textarea name="txtProgramCareerOpportunities" id="txtProgramCareerOpportunities" runat="server" class="defaultText" cols="10" rows="5"></textarea>
																											<script type="text/javascript">
																											    CKEDITOR.replace('txtProgramCareerOpportunities',
																												{
																												    customConfig: '/iCatalog/ckeditor_config.js',
																												    toolbar:
																													[
																														{ name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', '-',
																														'NumberedList', 'BulletedList', '-',
																														'SpecialChar', '-',
                                                                                                                        'Link', 'Unlink', '-',
																														'SpellChecker', 'Scayt']
																														}
																													],
																												    height: "150px"
																												});
																											</script>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td class="portletMain" style="text-align:center;padding:3px;">
																		<input type="button" id="cmdCancel" value="Cancel" class="small" style="background:#ccccaa;border:1px solid #000000;width:125px;" onclick="document.frmProgram.hidTodo.value='search';document.frmProgram.submit();" /> 
																		&nbsp;<asp:button id="cmdPreview" runat="server" text="Preview CPG" onfocus="document.frmProgram.hidFocus.value=this.id;" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:125px;" onclick="cmdPreview_Click"></asp:button>
																		&nbsp;<asp:button id="cmdSubmit" runat="server" text="Submit"  onfocus="document.frmProgram.hidFocus.value=this.id;" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:125px;" onclick="cmdSubmit_Click"></asp:button>
																	</td>
																</tr>
															</table>
														</asp:panel>
														<asp:panel runat="server" id="panConfirm">
															<table style="width:100%;" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="portletDark" style="width:100%;">
																		<table cellspacing="1" cellpadding="0" style="width:100%;">
																			<tr>
																				<td class="portletMain" colspan="2" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
																					The following CPG data has been successfully submitted.
																				</td>
																			</tr>
																			<tr>
																				<td class="portletLight" style="width:100%;vertical-align:text-top;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<tr>
																							<td style="padding-left:10px;padding-right:5px;padding-bottom:14px;">
																								<table cellpadding="0" cellspacing="0" style="width:100%;">
																									<tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Program Title:</b>&nbsp;
																										</td>
																										<td style="width:430px;padding-top:15px;">
																											<asp:label id="lblProgramTitle2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
                                                                                                    <tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Offered by:</b>&nbsp;
																										</td>
																										<td style="width:430px;padding-top:15px;">
																											<asp:label id="lblOfferedAt2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
                                                                                                    <tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Term:</b>&nbsp;
																										</td>
																										<td style="width:430px;padding-top:15px;">
																											<b>Begin</b>&nbsp;<asp:label id="lblProgramBeginSTRM2" cssclass="small" runat="server"></asp:label>&nbsp;&nbsp;&nbsp;
																											<b>End</b>&nbsp;<asp:label id="lblProgramEndSTRM2" cssclass="small" runat="server"></asp:label>
																										</td>
																									</tr>
                                                                                                    <tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Completion Award:</b>&nbsp;
																										</td>
																										<td style="width:430px;padding-top:15px;">
																											<asp:label id="lblDegree2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Category:</b>&nbsp;
																										</td>
																										<td style="width:430px;padding-top:15px;">
																											<asp:label id="lblCategory2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
                                                                                                    <tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Status:</b>&nbsp;
																										</td>
																										<td style="width:430px;padding-top:15px;">
																											<asp:label id="lblPublishedCareerPlanningGuide" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Enrollment:</b>&nbsp;
																										</td>
																										<td style="width:430px;padding-top:15px;">
																											<asp:label id="lblProgramEnrollment" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>Program URL:</b>&nbsp;
																										</td>
																										<td style="width:430px;padding-top:15px;">
																											<asp:label id="lblProgramWebsiteURL" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<tr>
																										<td style="width:125px;padding-left:5px;padding-top:15px;text-align:right;">
																											<b>CPG Format:</b>&nbsp;
																										</td>
																										<td style="width:430px;padding-top:15px;">
																											<asp:label runat="server" id="lblCareerPlanningGuideFormat" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<tr>
																										<td colspan="2" style="width:100%;padding-top:15px;">
																											<b>Program Course of Study:</b>&nbsp;
																										</td>
																									</tr>
																									<tr>
																										<td colspan="2" style="width:100%;padding-top:10px;">
																											<asp:label id="lblProgramCourseOfStudy" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<tr>
																										<td colspan="2" style="width:100%;padding-top:15px;">
																											<b>Program Learning Outcomes:</b>&nbsp;
																										</td>
																									</tr>
																									<tr>
																										<td colspan="2" style="width:100%;padding-top:10px;">
																											<asp:label id="lblProgramGoals" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<tr>
																										<td colspan="2" style="width:100%;padding-top:15px;">
																											<b>Program Career Opportunity:</b>&nbsp;
																										</td>
																									</tr>
																									<tr>
																										<td colspan="2" style="width:100%;padding-top:10px;">
																											<asp:label id="lblProgramCareerOpportunities" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td class="portletMain" style="text-align:center;padding:3px;">
																		<input type="button" id="cmdOK" runat="server" value="OK" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="document.frmProgram.hidTodo.value='search';document.frmProgram.submit();" />
																	</td>
																</tr>
															</table>
														</asp:panel>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</form>
			</asp:panel>
			<div class="clearer"></div>
		</asp:panel>
		<asp:panel id="panPreview" runat="server">
	        <script type="text/javascript">
			<!--
				document.body.style.backgroundColor = "#ffffff";
			    document.body.style.backgroundImage = "url('')";
			    function window.onbeforeprint(){
                    document.getElementById("buttons").style.display = "none";
                }
                function window.onafterprint(){
                    document.getElementById("buttons").style.display = "block";
                }
			//-->
			</script>
			<div style="width:660px;text-align:right;" id="buttons">
				<a style="font-size:10pt;color:blue;" href="javascript:history.back(1);">Back</a> | <a style="font-size:10pt;color:blue;" href="javascript:window.print();">Print</a>
			</div>
	        <table align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td style="background:#000000;">
						<asp:table id="tblCPGFront" runat="server" cellpadding="6" cellspacing="1" style="width:660px;height:910px;"></asp:table>
					</td>
				</tr>
			</table>
			<br class="breakhere" />
			<table align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td style="background:#000000;">
						<asp:table id="tblCPGBack" runat="server" cellpadding="8" cellspacing="1" style="width:660px;"></asp:table>
					</td>
				</tr>
			</table>
			<asp:panel id="panCPGBack2" runat="server">
				<br class="breakhere" />
				<table align="center" cellpadding="0" cellspacing="0">
					<tr>
						<td style="background:#000000;">
							<asp:table id="tblCPGBack2" runat="server" cellpadding="8" cellspacing="1" style="width:660px;"></asp:table>
						</td>
					</tr>
				</table>
			</asp:panel>
		</asp:panel>
	</body>
</html>

