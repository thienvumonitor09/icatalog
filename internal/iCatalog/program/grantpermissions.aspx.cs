using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program
{
	/// <summary>
	/// Summary description for grantpermissions.
	/// </summary>
	public partial class grantpermissions : System.Web.UI.Page
	{

		permissionData csPermission = new permissionData();

		protected void Page_Load(object sender, System.EventArgs e) {
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panAdd.Visible = false;
			panEdit.Visible = false;
			panView.Visible = false;
			hidError.Value = "0";

			String strToDo = Request.QueryString["todo"];

			//if first time to the page
			if(!IsPostBack){

				if(strToDo == "edit"){

					//get permissions for the selected user
					DataSet dsUserPermission = csPermission.GetProgUserPermissionByPK(Convert.ToInt32(Request.QueryString["id"]));
					
					if(dsUserPermission.Tables[0].Rows.Count > 0){
						//display the user's info
						String employeeID = dsUserPermission.Tables[0].Rows[0]["EMPLID"].ToString();
						lblName2.Text = dsUserPermission.Tables[0].Rows[0]["Name"].ToString();
						lblEmployeeID.Text = employeeID;
						cboPermissions2.SelectedValue = dsUserPermission.Tables[0].Rows[0]["Permission"].ToString();
						panEdit.Visible = true;
					}

				}else if(strToDo == "delete"){
					//delete the selected user
					csPermission.DeleteProgUserPermission(Convert.ToInt32(Request.QueryString["id"]));
					//re-generate the user permission list
					GeneratePermissionsList();
					panView.Visible = true;

				}else if(strToDo == "add"){
					panAdd.Visible = true;

				}else{
					GeneratePermissionsList();
					panView.Visible = true;
				}

			}else{

				if(strToDo == "add"){
					//get the user's name by the ctcLink ID entered
					String strName = csPermission.GetEmpName(txtEmployeeID.Text);
					if(strName != null){
						lblName1.Text = strName;
					}else{
						lblName1.Text = "An invalid ctcLink ID has been entered.";
					}
					panAdd.Visible = true;
				}

			}
		}

		protected void cmdAdd_Click(object sender, System.EventArgs e){
			if(lblName1.Text == "An invalid ctcLink ID has been entered." || txtEmployeeID.Text.Trim() == "" || lblName1.Text == ""){
				hidError.Value = "1";
				panAdd.Visible = true;
			}else{
				//get the user's HRO permission
				Char permission = Convert.ToChar(cboPermissions1.SelectedValue);
				DataSet dsUserPermission = csPermission.GetProgUserPermissionByEmployeeID(txtEmployeeID.Text);
				
				//if the user already exists in theDB
				if(dsUserPermission.Tables[0].Rows.Count > 0){
					//edit the user's permission
					csPermission.EditProgUserPermission(Convert.ToInt32(dsUserPermission.Tables[0].Rows[0]["PermissionID"]), permission);
				}else{
					//insert the user's permission
					csPermission.AddProgUserPermission(txtEmployeeID.Text, lblName1.Text, permission);
				}

				//re-generate the HRO Permissions list
				GeneratePermissionsList();
				panView.Visible = true;
				panAdd.Visible = false;
			}
		}

		protected void cmdSave_Click(object sender, System.EventArgs e){
			//edit the user's permission
			csPermission.EditProgUserPermission(Convert.ToInt32(Request.QueryString["id"]), Convert.ToChar(cboPermissions2.SelectedValue));
			
			//re-generate the HRO Permissions list
			GeneratePermissionsList();
			panView.Visible = true;
			panEdit.Visible = false;
		}

		private void GeneratePermissionsList(){
			//get all user permissions
			DataSet dsPermissions = csPermission.GetProgramPermissions();

			//loop through the user permissions
			for(Int16 intDSRow = 0; intDSRow < dsPermissions.Tables[0].Rows.Count; intDSRow++){
				String employeeID = dsPermissions.Tables[0].Rows[intDSRow]["EMPLID"].ToString();
				String strName = dsPermissions.Tables[0].Rows[intDSRow]["Name"].ToString(), permission = dsPermissions.Tables[0].Rows[intDSRow]["Permission"].ToString();
				Int32 permissionID = Convert.ToInt32(dsPermissions.Tables[0].Rows[intDSRow]["PermissionID"]);
				
				TableRow tr = new TableRow();
				
				TableCell td = new TableCell();
				td.CssClass = "portletLight";
				td.Attributes["style"] = "text-align:center;";
				td.Controls.Add(new LiteralControl(employeeID));
				
				tr.Cells.Add(td);
				
				td = new TableCell();
				td.CssClass = "portletLight";
				td.Attributes["style"] = "padding-left:5px;";
				td.Controls.Add(new LiteralControl(strName));
				
				tr.Cells.Add(td);
				
				td = new TableCell();
				td.CssClass = "portletLight";
				td.Attributes["style"] = "padding-left:5px;";
				if(permission == "1"){
					td.Controls.Add(new LiteralControl("Admin"));
				}else if(permission == "2"){
					td.Controls.Add(new LiteralControl("Add/Edit/Delete"));
				}else if(permission == "3"){
					td.Controls.Add(new LiteralControl("Read Only"));
				}
				
				tr.Cells.Add(td);
				
				td = new TableCell();
				td.CssClass = "portletLight";
				td.Attributes["style"] = "text-align:center;";
				td.Controls.Add(new LiteralControl("<a href=\"grantpermissions.aspx?todo=edit&id=" + permissionID + "\">Edit</a> | <a href=\"grantpermissions.aspx?todo=delete&id=" + permissionID + "\" onclick=\"return(confirm('Are you sure you want to delete permissions for " + strName + "?'));\">Delete</a>"));
				
				tr.Cells.Add(td);
				tblUsers.Rows.Add(tr);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {    
		}
		#endregion
	}
}

