using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.course
{
	/// <summary>
	/// Summary description for courseinfo.
	/// </summary>
	public partial class courseinfo : System.Web.UI.Page
	{
		courseData csCourse = new courseData();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			String strTodo = Request.QueryString["todo"], subject = Request.QueryString["subject"], descriptionCourseID = Request.QueryString["dcid"], strReturn = "";

			if(strTodo == "subjectDescription"){
				String subjectDescription = csCourse.GetSubjectDescription(subject);
				if(subjectDescription.Length == 0){
					strReturn = "auto generated";
				}else{
					strReturn = subjectDescription;
				}
			}else if(strTodo == "DescriptionCourses"){
				DataSet dsDescriptionCourses = csCourse.GetDescriptionCourses(subject);
                Int32 intRowCount = dsDescriptionCourses.Tables[0].Rows.Count;
				for(Int32 intDSRow = 0; intDSRow < intRowCount; intDSRow++){
					strReturn += "|" + dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseOffering"].ToString() + " - " + 
							     dsDescriptionCourses.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString() + "," +
						         dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseID"].ToString();
				}
                if (intRowCount > 0)
                {
                    strReturn = strReturn.Substring(1);
                }
			}else if(strTodo == "getDescriptionByCourseID"){
				strReturn = csCourse.GetDescriptionByCourseID(Convert.ToInt32(descriptionCourseID));
			}
			Response.Write(strReturn);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
