<%@ Page language="c#" Inherits="ICatalog.cred.view" CodeFile="view.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
	<head>
		<title>View Credentials</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
		<link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
	</head>
	<body>
		<asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
				<form id="frmCred" method="post" runat="server">
					<input type="hidden" id="hidLtr" runat="server" />
					<table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;View Credentials</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
																	<asp:table runat="server" id="tblAlpha" style="width:100%;" cellspacing="0" cellpadding="0"></asp:table>
																	<table cellpadding="0" cellspacing="1" style="width:100%">
																		<tr>
																			<td style="padding:10px 5px 10px 5px;" class="portletMain">
																				<b>Work Unit:</b>&nbsp;
																				<asp:dropdownlist id="cboCredentialWorkUnit" runat="server" cssclass="small" autopostback="true">
																					<asp:ListItem Value="" Selected="True">ALL</asp:ListItem>
																					<asp:ListItem Value="SCC">SCC</asp:ListItem>
																					<asp:ListItem Value="SFCC">SFCC</asp:ListItem>
																					<asp:ListItem Value="IEL">IEL</asp:ListItem>
																					<asp:ListItem Value="DIST">DIST</asp:ListItem>
																				</asp:dropdownlist>
																				&nbsp;&nbsp;&nbsp;<b>Search Credentials:</b>&nbsp;
																				<asp:TextBox ID="txtCredSearch" Runat="server" CssClass="small" Columns="50"></asp:TextBox>
																				&nbsp;
																				<input type="button" value="Go" class="small" onclick="document.frmCred.submit();" style="background:#ccccaa;border:1px solid #000000;width:30px;" />
																			</td>
																		</tr>
																		<tr>
																			<td class="portletMain" style="text-align:center;padding:3px;">
																				<asp:Button ID="cmdExport" Text="Export to Excel" Runat="server" CssClass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;"></asp:Button>
																				&nbsp;<asp:Button ID="cmdGraphics" Text="Export to Graphics" Runat="server" CssClass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;"></asp:Button>
																			</td>
																		</tr>
																	</table>
																	<table cellpadding="0" cellspacing="0" style="width:100%">
																		<tr>
																			<td style="padding:10px;padding-top:15px;" class="portletLight">
																				<asp:panel ID="panDisplay" Runat="server"></asp:panel>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</form>
			</asp:panel>
			<div class="clearer"></div>
		</asp:panel>
	</body>
</html>
