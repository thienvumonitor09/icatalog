USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramTermList]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/18/17
-- Description:	Returns term codes and descriptions for versions of programs available. (Past four years, current and future)
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramTermList]
	
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT STRM, DESCR, TERM_BEGIN_DT, TERM_END_DT
	FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable 
	WHERE TERM_END_DT > DATEADD(Year, -4, GETDATE()) 
	AND TERM_BEGIN_DT <= DATEADD(Year, 1, GETDATE()) 
	AND CCSGen_ctcLink_ODS.dbo.vw_TermTable.ACAD_CAREER = 'UGRD' /* filter out continuing ed to remove duplicate terms */
	AND CCSGen_ctcLink_ODS.dbo.vw_TermTable.INSTITUTION = 'WA171' /* pull terms for SCC only to avoid duplicates */ 
	ORDER BY TERM_END_DT;
END

GO
