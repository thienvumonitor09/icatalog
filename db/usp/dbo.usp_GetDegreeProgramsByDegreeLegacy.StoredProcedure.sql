USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDegreeProgramsByDegreeLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 12/11/17
-- Description:	Returns a list of degree programs by degree
--		 INPUT: STRM - Term code
--				LocationID (optional) - Location primary key 
--				DegreeID (optional) - Degree primary key 
--				DegreeType (optional) - Degree Type (Transfer or Career Technical)
--				SUBJECT (optional) - Course subject area 
--				CATALOG_NBR (optional) - Course catalog number 
--	  MODIFIED: 3/22/18 to include DegreeType as an input parameter by Brandy Vaughn
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDegreeProgramsByDegreeLegacy] 
	@STRM varchar(4) = NULL,
	@LocationID int = NULL,
	@DegreeID int = NULL,
	@DegreeType varchar(50) = NULL,
	@SUBJECT varchar(5) = NULL,
	@CATALOG_NBR varchar(3) = NULL,
	@GlobalSearch varchar(150) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SQL nvarchar(max),
			@Parameters nvarchar(4000)
			
	IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
							FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
							WHERE TERM_END_DT > GETDATE()
							ORDER BY TERM_END_DT)

    SET @SQL = 'SELECT CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, DocumentTitle, DegreeType, DegreeDescription
				FROM Degree
				INNER JOIN ProgramDegree
				ON ProgramDegree.DegreeID = Degree.DegreeID				
				INNER JOIN Program
				ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
				LEFT OUTER JOIN CareerPlanningGuide 
				ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
				INNER JOIN College 
				ON College.CollegeID = Program.CollegeID 
				INNER JOIN ProgramDegreeOption 
				ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
				LEFT OUTER JOIN OptionPrerequisite 
				ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
				LEFT OUTER JOIN OptionElectiveGroup 
				ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
				LEFT OUTER JOIN ElectiveGroupCourse 
				ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
				LEFT OUTER JOIN OptionCourse 
				ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
				LEFT OUTER JOIN Course
				ON Course.CourseOffering = OptionPrerequisite.CourseOffering
				OR Course.CourseOffering = ElectiveGroupCourse.CourseOffering 
				OR Course.CourseOffering = OptionCourse.CourseOffering 
				LEFT OUTER JOIN OptionLocation 
				ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
				LEFT OUTER JOIN Location 
				ON Location.LocationID = OptionLocation.LocationID 
				WHERE (ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = ''Z999'' OR ProgramEndSTRM >= @STRM)) 
				AND PublishedProgram = 1 '
				
	IF @LocationID IS NOT NULL
		SET @SQL += 'AND OptionLocation.LocationID = @LocationID '
		
	IF @DegreeID IS NOT NULL
		SET @SQL += 'AND Degree.DegreeID = @DegreeID '
		
	IF @DegreeType IS NOT NULL
		SET @SQL += 'AND Degree.DegreeType = @DegreeType '
		
	IF @SUBJECT IS NOT NULL
		SET @SQL += 'AND ((Course.SUBJECT = @SUBJECT OR Course.SUBJECT = @SUBJECT + ''&'') 
					 AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
					 OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM))) '
	
	IF @CATALOG_NBR IS NOT NULL	
		SET @SQL += 'AND (Course.CATALOG_NBR = @CATALOG_NBR 
					 AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
					 OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM))) '
	
	IF @GlobalSearch IS NOT NULL
		SET @SQL += 'AND ((LocationTitle LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (ProgramTitle LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (ProgramDescription LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (OptionTitle LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (DegreeShortTitle LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (DegreeLongTitle LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (DegreeType LIKE ''%'' + @GlobalSearch + ''%'')
					 OR ((Course.COURSE_TITLE_LONG LIKE ''%'' + @GlobalSearch + ''%'') 
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR ((Course.DESCR LIKE ''%'' + @GlobalSearch + ''%'') 
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR ((Course.SUBJECT LIKE ''%'' + @GlobalSearch + ''%'') 
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR ((Course.CATALOG_NBR LIKE ''%'' + @GlobalSearch + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR (((Course.SUBJECT + LTRIM(Course.CATALOG_NBR)) LIKE + ''%'' + @GlobalSearch + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR (((Course.SUBJECT + Course.CATALOG_NBR) LIKE + ''%'' + @GlobalSearch + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR (((Course.SUBJECT + '' '' + Course.CATALOG_NBR) LIKE + ''%'' + @GlobalSearch + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))) '
	
	SET @SQL += 'GROUP BY CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, DocumentTitle, DegreeType, DegreeDescription  
				 ORDER BY DegreeLongTitle, OptionTitle, ProgramTitle, CollegeShortTitle;'
				 
	SET @Parameters = '@STRM varchar(4),
					   @LocationID int,
					   @DegreeID int,
					   @DegreeType varchar(50),
					   @SUBJECT varchar(5),
					   @CATALOG_NBR varchar(3),
					   @GlobalSearch varchar(150)'
						 
	EXEC sp_executesql @SQL, @Parameters, @STRM, @LocationID, @DegreeID, @DegreeType, @SUBJECT, @CATALOG_NBR, @GlobalSearch
	
END

GO
