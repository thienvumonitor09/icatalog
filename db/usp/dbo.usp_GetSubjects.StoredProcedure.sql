USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSubjects]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/14/2019
-- Description:	Return all the course departments (starting with the alphabet letter entered)
-- Parameters:	
--   @ChrLtr (OPTIONAL) - Letter of Alphabet as a Char DataType
--   @SUBJECT (OPTIONAL) - Course Subject: ENGL
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetSubjects]
	@ChrLtr varchar(2) = NULL,
	@SUBJECT varchar(5) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT INSTITUTION, SUBJECT, EFF_STATUS, DESCR, EFFDT
	FROM [CCSGen_ctcLink_ODS].[dbo].[vw_SubjectTable] as SubjectTable
	WHERE EFF_STATUS = 'A' AND INSTITUTION='WA171' 
		AND 
			(
				(@ChrLtr IS NULL OR SUBJECT LIKE @ChrLtr+'%')
				AND
				(@SUBJECT IS NULL OR SUBJECT = @SUBJECT )
			)
	ORDER BY SUBJECT
END
GO
