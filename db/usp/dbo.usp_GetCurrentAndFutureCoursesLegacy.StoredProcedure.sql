USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCurrentAndFutureCoursesLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 3/16/17
-- Description:	Returns a list of courses for a specific quarter/term (optional) in a specific subject/department (optional)	
--		 INPUT: STRM (optional) - Term Code (If STRM is omitted, all current and future courses are returned)
--				SUBJECT (optional) - Course Subject: ENGL (If SUBJECT is omitted, courses in all subjects/departments are returned)
--				Published (optional) - '0' for unpublished courses (in edit mode, not displayed to the public), '1' for published courses (displayed to the public)
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCurrentAndFutureCoursesLegacy] 
	-- Add the parameters for the stored procedure here
	@STRM varchar(4) = NULL,
	@SUBJECT varchar(5) = NULL,
	@Published char = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	/*Store the current term STRM value*/ 
	DECLARE @CurrentSTRM varchar(4);
	IF @STRM IS NULL
		SET @CurrentSTRM = (SELECT TOP 1(STRM)
							FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
							WHERE TERM_END_DT > GETDATE()
							ORDER BY TERM_END_DT)

	SELECT DISTINCT CourseID, CATALOG_NBR, CourseOffering, CourseBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, BeginTerm.TERM_BEGIN_DT, CourseEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, EndTerm.TERM_END_DT, SUBJECT, Course.DESCR, PublishedCourse, PublishedCourseID
	FROM Course 
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = CourseBeginSTRM
	AND BeginTerm.ACAD_CAREER = 'UGRD' /* filter out continuing ed to remove duplicate terms */
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = CourseEndSTRM
	AND EndTerm.ACAD_CAREER = 'UGRD' /* filter out continuing ed to remove duplicate terms */
	WHERE (@SUBJECT IS NULL OR SUBJECT = @SUBJECT)
	AND (@Published IS NULL OR PublishedCourse = @Published)
	AND ((@STRM IS NOT NULL AND (CourseBeginSTRM <= @STRM AND (CourseEndSTRM = 'Z999' OR CourseEndSTRM >= @STRM))) /*get courses effective for the STRM entered*/
	OR (@STRM IS NULL AND (CourseBeginSTRM >= @CurrentSTRM OR (CourseBeginSTRM <= @CurrentSTRM AND (CourseEndSTRM = 'Z999' OR CourseEndSTRM >= @CurrentSTRM))))) /*get current and future courses if STRM is null*/
	ORDER BY CourseOffering, CourseBeginSTRM DESC, PublishedCourse DESC;
END

GO
