USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionCoursesLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/11/17
-- Description:	Returns program degree option courses
--		 INPUT: OptionID - Program degree option id (ProgOptASN)
--				BeginSTRM - Program begin term code
--				EndSTRM - Program end term code
--				SelectedSTRM - Selected term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionCoursesLegacy] 
	@OptionID int,
	@BeginSTRM varchar(4),
	@EndSTRM varchar(4),
	@SelectedSTRM varchar(4)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT OptionCourse.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
    FROM OptionCourse 
    INNER JOIN Course 
    ON Course.CourseOffering = OptionCourse.CourseOffering 
    AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999'))
    OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM)) 
    AND (CourseBeginSTRM <= @SelectedSTRM AND (CourseEndSTRM >= @SelectedSTRM OR CourseEndSTRM = 'Z999'))
    LEFT OUTER JOIN OptionFootnote 
    ON OptionFootnote.OptionFootnoteID = OptionCourse.OptionFootnoteID 
    WHERE OptionCourse.OptionID = @OptionID
    GROUP BY OptionCourse.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
    ORDER BY Quarter, SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
END

GO
