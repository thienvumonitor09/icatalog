<%@ Reference Page="~/program/edit.aspx" %>
<%@ Page language="c#" Inherits="ICatalog.program.popups.editoption" CodeFile="editoption.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Edit Option Title</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
		<script type="text/javascript" src="../../_phatt3_src_files/expand.js"></script>
		<script type="text/javascript" src="../../_phatt3_src_files/trim.js"></script>
		<script type="text/javascript">
		<!--
			function validate(){
				var txtOptTitle = document.getElementById("txtOptTitle");
				var isValid = true;

				//Comment out to allow an empty option title to be entered.
				/* 
				if(trim(txtOptTitle.value) == ""){
				    alert("Please enter the current option title.");
				    txtOptTitle.select();
				    isValid = false;
				}else{
				    isValid = true;
				}
				*/
				
				if(isValid){
					getProgOptValues();
				}
				
				return isValid;
			}
			
			function getProgOptValues(){ //could use below code to avoid passing values through the querystring
				document.getElementById("hidOptionDescription").value = window.opener.CKEDITOR.instances['txtOptionDescription'].getData();
				document.getElementById("hidGainfulEmploymentID").value = window.opener.document.getElementById('txtGainfulEmploymentID').value;
				document.getElementById("hidOptionStateApproval").value = window.opener.document.getElementById('txtOptionStateApproval').value;
				document.getElementById("hidCIP").value = window.opener.document.getElementById('txtCIP').value;
				document.getElementById("hidEPC").value = window.opener.document.getElementById('txtEPC').value;
				document.getElementById("hidAcademicPlan").value = window.opener.document.getElementById('txtAcademicPlan').value;
				document.getElementById("hidTotalQuarters").value = window.opener.document.getElementById('txtTotalQuarters').value;
				document.getElementById("hidPrimaryOption").value = window.opener.document.getElementById('chkPrimary').checked;
				
				var strOptionLocationFields = window.opener.document.getElementById('hidLocationFields').value;
				var hidLocations = document.getElementById("hidLocations");
				hidLocations.value = "";
				if(strOptionLocationFields != null && strOptionLocationFields != ""){
					var aryOptionLocationFields = strOptionLocationFields.substring(1).split("|");
					for(var i = 0; i < aryOptionLocationFields.length; i++){

						if(window.opener.document.getElementById(aryOptionLocationFields[i]).checked){
							hidLocations.value += "|" + aryOptionLocationFields[i].replace("chkLocation_" + document.getElementById("hidOptionID").value + "_","");
						}
					}
				}
				
				var strOptionMCodeFields = window.opener.document.getElementById('hidMCodeFields').value;
				var hidMCodes = document.getElementById("hidMCodes");
				hidMCodes.value = "";
				if(strOptionMCodeFields != null && strOptionMCodeFields != ""){
					var aryOptionMCodeFields = strOptionMCodeFields.substring(1).split("|");
					for(var i = 0; i < aryOptionMCodeFields.length; i++){
						var aryCollegeMCodeFields = aryOptionMCodeFields[i].split(";");
						if(window.opener.document.getElementById(aryCollegeMCodeFields[0]).checked){
							hidMCodes.value += "|" + aryCollegeMCodeFields[0].replace("chkMCode_" + document.getElementById("hidOptionID").value + "_","") + ";" + window.opener.document.getElementById(aryCollegeMCodeFields[1]).value;
						}						
					}
				}
			}
			
		//-->
		</script>
	</head>
	<body style="BACKGROUND: #ffffff">
		<form id="frmProgOption" runat="server">
			<input type="hidden" id="hidProgramDegreeID" runat="server" />
			<input type="hidden" id="hidProgramVersionID" runat="server" />
			<input type="hidden" id="hidCollegeID" runat="server" />
			<input type="hidden" id="hidLtr" runat="server" />
			<input type="hidden" id="hidProgramTitle" runat="server" />
			<input type="hidden" id="hidOfferedAt" runat="server" />
			<input type="hidden" id="hidOptionDescription" runat="server" />
			<input type="hidden" id="hidOptionStateApproval" runat="server" />
			<input type="hidden" id="hidCIP" runat="server" />
			<input type="hidden" id="hidEPC" runat="server" />
            <input type="hidden" id="hidAcademicPlan" runat="server" />
            <input type="hidden" id="hidTotalQuarters" runat="server" />
			<input type="hidden" id="hidPrimaryOption" runat="server" />
			<input type="hidden" id="hidDegreeID" runat="server" />
			<input type="hidden" id="hidOptionID" runat="server" />
			<input type="hidden" id="hidProgramDisplay" runat="server" />
			<input type="hidden" id="hidOldOptTitle" runat="server" />
			<input type="hidden" id="hidProgramBeginSTRM" runat="server" />
			<input type="hidden" id="hidProgramEndSTRM" runat="server" />
			<input type="hidden" id="hidPublishedProgram" runat="server" />
			<input type="hidden" id="hidPublishedProgramVersionID" runat="server" />
			<input type="hidden" id="hidSTRM" runat="server" />
			<input type="hidden" id="hidSelectedCollegeID" runat="server" />
			<input type="hidden" id="hidProgramID" runat="server" />
			<input type="hidden" id="hidMCodes" runat="server" />
			<input type="hidden" id="hidLocations" runat="server" />
			<input type="hidden" id="hidGainfulEmploymentID" runat="server" />
			<input type="hidden" id="hidSearchSubject" runat="server" />
			<input type="hidden" id="hidSearchId" runat="server" />
			<input type="hidden" id="hidSearchSTRM" runat="server" />
			<input type="hidden" id="hidSearchText" runat="server" />
			<table class="centeredTable" style="width:280px" cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding-right:0px;padding-left:4px">
						<table style="width:100%" cellpadding="0" cellspacing="0">
							<tr>
								<td style="background-color:#000000;text-align:left">
									<table style="width:100%" cellpadding="1" cellspacing="1">
										<tr>
											<td style="background-image:url(../../Images/Themes/Blue/gradient.gif);width:100%;background-color:#5f7568" class="portletHeader">
												<table style="width:100%" cellpadding="0" cellspacing="0">
													<tr>
														<td class="portletHeader" style="width:100%">&nbsp;Edit Option Title</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="portletMain" style="width:100%">
												<table style="width:100%" cellspacing="0" cellpadding="0">
													<tr>
														<td class="portletDark" style="width:100%">
															<table cellspacing="1" cellpadding="0" style="width:100%">
																<tr>
																	<td class="portletLight" style="vertical-align:text-top;width:100%">
																		<table cellpadding="0" cellspacing="0" style="width:100%">
																			<tr>
																				<td style="padding-bottom:15px;width:100%">
																					<table cellpadding="0" cellspacing="0" style="width:100%">
																						<asp:panel runat="server" id="panError">
																							<tr>
																								<td class="portletLight" style="padding-left:10px;color:red;padding-top:15px" colspan="2">
																									<asp:label id="lblErrorMsg" runat="server"></asp:label>
																								</td>
																							</tr>
																						</asp:panel>
																						<tr>
																							<td class="portletLight" style="padding-left:10px;width:100%;padding-top:15px;text-align:left">
																								<b>Option Title:</b> 
																							</td>
																						</tr>
																						<tr>
																							<td class="portletLight" style="padding-right:10px;padding-left:10px;width:100%;padding-top:5px;text-align:left">
																								<asp:textbox id="txtOptTitle" runat="server" cssclass="small" style="width:97%;"></asp:textbox>
																							</td>
																						</tr>		
																						<!-- BUTTONS -->
																						<tr>
																							<td class="portletLight" style="width:100%;padding-top:20px;text-align:center">
																								<input type="button" id="cmdClose" value="Close Window" class="small" style="border-right:#000000 1px solid;border-top:#000000 1px solid;background:#ccccaa;border-left:#000000 1px solid;width:85px;border-bottom:#000000 1px solid" onclick="self.close();" />
																								&nbsp;<asp:button runat="server" id="cmdSave" text="Save Changes" cssclass="small" style="border-right:#000000 1px solid;border-top:#000000 1px solid;background:#ccccaa;border-left:#000000 1px solid;border-bottom:#000000 1px solid" width="85" OnClientClick="return validate();"></asp:button>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

