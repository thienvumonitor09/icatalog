﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="program_pathway_edit" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
    <head>
        <title>Edit/Delete Guided Pathway</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
        <script type="text/javascript" src="../../_phatt3_src_files/trim.js"></script>
        <script type="text/javascript">
        <!--
			function validate(){
				var isValid = false;
                var txtPathwayTitle = document.getElementById("txtPathwayTitle");
				
                if (trim(txtPathwayTitle.value) == ""){
                    alert("Please enter a Pathway Title.");
					txtPathwayTitle.select();
				}else{
					isValid = true;
				}
				
				return isValid;					
			}
        //-->
        </script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmPathway" runat="server">
                    <input type="hidden" runat="server" id="hidTodo" />
                    <input type="hidden" runat="server" id="hidPathwayID" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Edit/Delete Guided Pathway</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <asp:panel runat="server" id="panDisplay">
                                                                        <asp:table runat="server" id="tblDisplay" style="width:100%;" cellspacing="1" cellpadding="2"></asp:table>
                                                                    </asp:panel>
                                                                    <asp:panel runat="server" id="panEdit">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <!-- Error Message -->
                                                                            <asp:panel runat="server" id="panError">
                                                                                <tr>
                                                                                    <td colspan="2" class="portletMain" style="color:red;padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                        <asp:label runat="server" id="lblErrorMsg"></asp:label>
                                                                                    </td>
                                                                                </tr>
                                                                            </asp:panel>
                                                                            <!-- Offered by -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Offered by:</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:radiobuttonlist id="optOfferedAt" runat="server" repeatdirection="horizontal">
                                                                                        <asp:ListItem Value="WA171" selected="True">SCC</asp:ListItem>
                                                                                        <asp:ListItem Value="WA172">SFCC</asp:ListItem>
                                                                                    </asp:radiobuttonlist>
                                                                                    <asp:Label ID="lblOfferedAt" runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Pathway Title -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Pathway Title:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtPathwayTitle" cssclass="small" maxlength="50" style="width:270px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;width:100%;">
															                        <input type="button" id="cmdCancel" value="Cancel" onclick="location.href='edit.aspx';" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" /> 
                                                                                    &nbsp;<asp:button id="cmdSubmit" runat="server" text="Save Changes" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclientclick="return validate();" onclick="cmdSubmit_Click"></asp:button>
														                        </td>
													                        </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                    <asp:panel runat="server" id="panConfirm">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <tr>
                                                                                <td class="portletMain" colspan="2" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                    The following guided pathway has been successfully updated.
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Offered by -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Offered by:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblOfferedBy"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Pathway Title -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Pathway Title:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblPathwayTitle"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                        <input type="button" id="cmdOK" value="OK" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="location.href = 'edit.aspx';" />
														                        </td>
													                        </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <DIV class="clearer"></DIV>
        </asp:panel>
    </body>
</html>




