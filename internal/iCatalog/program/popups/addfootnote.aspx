<%@ Reference Page="~/program/edit.aspx" %>
<%@ Page language="c#" Inherits="ICatalog.program.popups.addfootnote" CodeFile="addfootnote.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Add Program Footnote</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
		<script type="text/javascript" src="../../_phatt3_src_files/expand.js"></script>
		<script type="text/javascript" src="../../_phatt3_src_files/trim.js"></script>
		<script type="text/javascript">
		<!--
			function validate(){
				var txtFootnote = document.getElementById("txtFootnote");
				var isValid;
				
				if(trim(txtFootnote.value) == ""){
					alert("Please enter a footnote.");
					txtFootnote.select();
					isValid = false;
				}else{
					isValid = true;
				}
				
				if(isValid){
					getProgOptValues();
				}
				
				return isValid;
			}
			
			function getProgOptValues(){ //could use below code to avoid passing values through the querystring
				document.getElementById("hidOptionDescription").value = window.opener.CKEDITOR.instances['txtOptionDescription'].getData();
				document.getElementById("hidGainfulEmploymentID").value = window.opener.document.getElementById('txtGainfulEmploymentID').value;
				document.getElementById("hidOptionStateApproval").value = window.opener.document.getElementById('txtOptionStateApproval').value;
				document.getElementById("hidCIP").value = window.opener.document.getElementById('txtCIP').value;
				document.getElementById("hidEPC").value = window.opener.document.getElementById('txtEPC').value;
				document.getElementById("hidAcademicPlan").value = window.opener.document.getElementById('txtAcademicPlan').value;
				document.getElementById("hidTotalQuarters").value = window.opener.document.getElementById('txtTotalQuarters').value;
				document.getElementById("hidPrimaryOption").value = window.opener.document.getElementById('chkPrimary').checked;
				
				var strOptionLocationFields = window.opener.document.getElementById('hidLocationFields').value;
				var hidLocations = document.getElementById("hidLocations");
				hidLocations.value = "";
				if(strOptionLocationFields != null && strOptionLocationFields != ""){
					var aryOptionLocationFields = strOptionLocationFields.substring(1).split("|");
					for(var i = 0; i < aryOptionLocationFields.length; i++){

						if(window.opener.document.getElementById(aryOptionLocationFields[i]).checked){
							hidLocations.value += "|" + aryOptionLocationFields[i].replace("chkLocation_" + document.getElementById("hidOptionID").value + "_","");
						}
					}
				}
				
				var strOptionMCodeFields = window.opener.document.getElementById('hidMCodeFields').value;
				var hidMCodes = document.getElementById("hidMCodes");
				hidMCodes.value = "";
				if(strOptionMCodeFields != null && strOptionMCodeFields != ""){
					var aryOptionMCodeFields = strOptionMCodeFields.substring(1).split("|");
					for(var i = 0; i < aryOptionMCodeFields.length; i++){
						var aryCollegeMCodeFields = aryOptionMCodeFields[i].split(";");
						if(window.opener.document.getElementById(aryCollegeMCodeFields[0]).checked){
							hidMCodes.value += "|" + aryCollegeMCodeFields[0].replace("chkMCode_" + document.getElementById("hidOptionID").value + "_","") + ";" + window.opener.document.getElementById(aryCollegeMCodeFields[1]).value;
						}						
					}
				}
			}
		//-->
		</script>
	</head>
	<body style="background: #ffffff url('');">
		<form id="frmFootnote" runat="server">
			<input type="hidden" id="hidProgramDegreeID" runat="server" />
			<input type="hidden" id="hidProgramVersionID" runat="server" />
			<input type="hidden" id="hidCollegeID" runat="server" />
			<input type="hidden" id="hidLtr" runat="server" />
			<input type="hidden" id="hidProgramTitle" runat="server" />
			<input type="hidden" id="hidOfferedAt" runat="server" />
			<input type="hidden" id="hidOptionDescription" runat="server" />
			<input type="hidden" id="hidOptionStateApproval" runat="server" />
			<input type="hidden" id="hidCIP" runat="server" />
			<input type="hidden" id="hidEPC" runat="server" />
            <input type="hidden" id="hidAcademicPlan" runat="server" />
            <input type="hidden" id="hidTotalQuarters" runat="server" />
			<input type="hidden" id="hidPrimaryOption" runat="server" />
			<input type="hidden" id="hidDegreeID" runat="server" />
			<input type="hidden" id="hidOptionID" runat="server" />
			<input type="hidden" id="hidProgramDisplay" runat="server" />
			<input type="hidden" id="hidProgramBeginSTRM" runat="server" />
			<input type="hidden" id="hidProgramEndSTRM" runat="server" />
			<input type="hidden" id="hidPublishedProgram" runat="server" />
			<input type="hidden" id="hidPublishedProgramVersionID" runat="server" />
			<input type="hidden" id="hidSTRM" runat="server" />
			<input type="hidden" id="hidSelectedCollegeID" runat="server" />
			<input type="hidden" id="hidProgramID" runat="server" />
			<input type="hidden" id="hidMCodes" runat="server" />
			<input type="hidden" id="hidLocations" runat="server" />
			<input type="hidden" id="hidGainfulEmploymentID" runat="server" />
			<input type="hidden" id="hidSearchSubject" runat="server" />
			<input type="hidden" id="hidSearchId" runat="server" />
			<input type="hidden" id="hidSearchSTRM" runat="server" />
			<input type="hidden" id="hidSearchText" runat="server" />
			<table class="centeredTable" style="width:280px;" cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding-left:4px;padding-right:0px;">
						<table style="width:100%;" cellpadding="0" cellspacing="0">
							<tr>
								<td style="background-color:#000000;text-align:left;">
									<table style="width:100%;" cellpadding="1" cellspacing="1">
										<tr>
											<td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;"
												class="portletHeader">
												<table style="width:100%;" cellpadding="0" cellspacing="0">
													<tr>
														<td class="portletHeader" style="width:100%;">&nbsp;Add Program Footnote</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="portletMain" style="width:100%">
												<table style="width:100%;" cellspacing="0" cellpadding="0">
													<tr>
														<td class="portletDark" style="width:100%;">
															<table cellspacing="1" cellpadding="0" style="width:100%;">
																<tr>
																	<td class="portletLight" style="width:100%;vertical-align:text-top;">
																		<table cellpadding="0" cellspacing="0" style="width:100%;">
																			<tr>
																				<td style="width:100%;padding-bottom:15px;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<asp:panel runat="server" id="panError">
																							<tr>
																								<td class="portletLight" colspan="2" style="color:red;padding-left:10px;padding-top:15px;">
																									<asp:label runat="server" id="lblErrorMsg"></asp:label>
																								</td>
																							</tr>
																						</asp:panel>
																						<!-- FOOTNOTE NUMBER -->
																						<tr>
																							<td class="portletLight" style="padding-top:15px;padding-left:10px;width:100%;">
																								<b>Footnote Number:</b>&nbsp;<asp:label id="lblFootnoteNum" runat="server" cssclass="small"></asp:label>
																							</td>
																						</tr>
																						<!-- FOOTNOTE INPUT TEXT -->
																						<tr>
																							<td class="portletLight" style="padding-top:15px;padding-left:10px;width:100%;text-align:left;">
																								<b>Footnote:</b>
																							</td>
																						</tr>
																						<tr>
																							<td class="portletLight" style="padding-top:5px;padding-left:10px;padding-right:10px;width:100%;text-align:left;">
																								<textarea runat="server" id="txtFootnote" style="width:250px;height:26px;" rows="2" class="small" ondblclick="expandTextArea(this.id,26);" title="Double click to expand this text area when the text overflows. Double click again to return to the original size."></textarea>
																							</td>
																						</tr>
																						<!-- BUTTONS -->
																						<tr>
																							<td class="portletLight" style="padding-top:20px;width:100%;text-align:center;">
																								<input type="button" id="cmdClose" value="Close Window" class="small" style="background:#ccccaa;border:1px solid #000000;width:85px;" onclick="self.close();" />
																								&nbsp;<asp:button runat="server" id="cmdAddFootnote" text="Add Program Footnote" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:135px;" onclientclick="return validate();"></asp:button>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<script type="text/javascript">
				<!--
					document.frmFootnote.txtFootnote.focus();
				//-->
			</script>
		</form>
	</body>
</html>

