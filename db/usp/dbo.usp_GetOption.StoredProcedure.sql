USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOption]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/11/17
-- Description:	Returns a program degree option
--		 INPUT: OptionID - Program degree option primary key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOption] 
	@OptionID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT OptionTitle, OptionDescription, OptionID, GainfulEmploymentID
    FROM ProgramDegreeOption
    WHERE OptionID = @OptionID;
END

GO
