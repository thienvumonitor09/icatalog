using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.course.popups
{
	/// <summary>
	/// Summary description for editterm.
	/// </summary>
	public partial class editterm : System.Web.UI.Page
	{

		courseData csCourse = new courseData();

		protected void Page_Load(object sender, System.EventArgs e) {
			panMsg.Visible = false;

			if(!IsPostBack){
				hidCourseOffering.Value = Request.QueryString["co"];
				hidPublishedCourseID.Value = Request.QueryString["pcid"];
				hidPublishedCourse.Value = Request.QueryString["pc"];
				hidCourseBeginSTRM.Value = Request.QueryString["bstrm"];
				hidCourseEndSTRM.Value = Request.QueryString["estrm"];	
				hidPage.Value = Request.QueryString["pg"];

				//populate course effective year quarter dropdownlists

				DataSet dsTerms = csCourse.GetTermsForCourses();
				cboRevisionBeginTerm.Items.Add("");
				cboRevisionEndTerm.Items.Add("");
				cboRevisionEndTerm.Items.Add("Z999");
				cboCurrentEndTerm.Items.Add("Z999");
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
					String STRM = dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString();
                    String DESCR = dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString();
					cboCurrentBeginTerm.Items.Add(new ListItem(DESCR, STRM));
					cboCurrentEndTerm.Items.Add(new ListItem(DESCR, STRM));
					cboRevisionBeginTerm.Items.Add(new ListItem(DESCR, STRM));
					cboRevisionEndTerm.Items.Add(new ListItem(DESCR, STRM));
				}

				try{
					cboCurrentBeginTerm.SelectedValue = Request.QueryString["bstrm"];
					cboCurrentEndTerm.SelectedValue = Request.QueryString["estrm"];
				}catch{
					//do nothing
				}
			}
		}

		private void cmdSave_Click(object sender, System.EventArgs e){
			Int32 courseID = Convert.ToInt32(Request.QueryString["cid"]);

			//check if the change to the current course version begin and end terms conflict with other versions
			String currentConflictingTermSpan = csCourse.GetConflictingTermsForExistingCourse(courseID, hidCourseOffering.Value, cboCurrentBeginTerm.SelectedValue, cboCurrentEndTerm.SelectedValue, hidPublishedCourseID.Value);

			//check if the new course version begin and end terms conflict with other versions
			String newConflictingTermSpan = csCourse.GetConflictingTermsForExistingCourse(courseID, hidCourseOffering.Value, cboRevisionBeginTerm.SelectedValue, cboRevisionEndTerm.SelectedValue, hidPublishedCourseID.Value);
				
			if(currentConflictingTermSpan != ""){
				lblMsg.Text = "Error: The current course begin and end terms overlap with \"" + hidCourseOffering.Value + "\" effective " + currentConflictingTermSpan + ".";
				panMsg.Visible = true;
			}else if(newConflictingTermSpan != ""){
				lblMsg.Text = "Error: The new course begin and end terms overlap with \"" + hidCourseOffering.Value + "\" effective " + newConflictingTermSpan + ".";
				panMsg.Visible = true;
			}else{
				//change current program begin and end terms
				bool blnSuccess = csCourse.EditCourseTerms(courseID, cboCurrentBeginTerm.SelectedValue, cboCurrentEndTerm.SelectedValue);

				if(blnSuccess){
					//pass updated begin and end terms back to opener and start course revision process
					String strScript = "<script type=\"text/javascript\">" + 
						"opener.document.frmCourse.hidCourseBeginSTRM.value='" + cboRevisionBeginTerm.SelectedValue + "';" + 
						"opener.document.frmCourse.hidCourseEndSTRM.value='" + cboRevisionEndTerm.SelectedValue + "';" + 
						"opener.document.frmCourse.hidTodo.value='revision';" +
						"opener.document.frmCourse.submit();" +
						"self.close();" +
						"</script>";
					script.Controls.Add(new LiteralControl(strScript));
				}else{
					lblMsg.Text = "Error: The current course begin and end terms could not be updated. The update failed.";
					panMsg.Visible = true;
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {    
			this.cmdSave.Click += new EventHandler(cmdSave_Click);
		}
		#endregion
	}
}

