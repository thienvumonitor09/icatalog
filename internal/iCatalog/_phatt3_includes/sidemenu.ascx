<%@ Control Language="c#" Inherits="ICatalog._phatt3_includes.sidemenu" CodeFile="sidemenu.ascx.cs" %>

<div class="portlet">
    <h2>&nbsp;Main Menu</h2>
    <a href="/ICatalog/" class="menu">Main Page</a> 
    <a id="lnkCollegeMenu" runat="server" href="/ICatalog/college/edit.aspx?menu=college" class="menu">College</a>
    <a id="lnkCredMenu" runat="server" href="/ICatalog/cred/edit.aspx?menu=credentials" class="menu">Credentials</a>
	<a id="lnkCourseMenu" runat="server" href="/ICatalog/course/view.aspx?menu=courses" class="menu">Courses</a>
	<a id="lnkProgMenu" runat="server" href="/ICatalog/program/view.aspx?menu=programs" class="menu">Programs</a>
    <a href="https://ccsnet.ccs.spokane.edu/lo.aspx" class="menu">Switch CCSNet User</a>
</div>

<asp:Panel ID="panCollegeAdmin" runat="server">
    <div class="portlet">
        <h2>&nbsp;Security</h2>
        <a href="/ICatalog/college/grantpermissions.aspx" class="menu">Grant Permissions</a>
    </div>
</asp:Panel>

<asp:Panel ID="panCollege" runat="server">
    <div class="portlet">
        <h2>&nbsp;College</h2>
        <a href="/ICatalog/college/edit.aspx" class="menu">College Maintenance</a>
        <a href="/ICatalog/college/location/edit.aspx" class="menu">Location Maintenance</a>
    </div>
</asp:Panel>

<asp:panel id="panCredUserAdmin" runat="server"><!-- 1 only -->
	<div class="portlet">
		<h2>&nbsp;Security</h2>
		<a href="/ICatalog/cred/grantpermissions.aspx" class="menu">Grant Permissions</a>
	</div>
</asp:panel>

<asp:panel id="panCredentials" runat="server">
    <div class="portlet">
        <h2>&nbsp;Credentials</h2>
        <a href="/ICatalog/cred/view.aspx" class="menu">View Credentials</a>
        <a href="/ICatalog/cred/edit.aspx" class="menu">Credential Maintenance</a>
        <a href="/ICatalog/cred/excelexport.aspx" class="menu">Export to Excel</a>
        <a href="/ICatalog/cred/updatesid.aspx" class="menu">ctcLink ID Update</a>
    </div>
</asp:panel>

<asp:panel id="panCourseUserAdmin" runat="server"><!-- 1 only -->
	<div class="portlet">
		<h2>&nbsp;Security</h2>
		<a href="/ICatalog/course/grantpermissions.aspx" class="menu">Grant Permissions</a>
	</div>
</asp:panel>

<asp:panel id="panSubject" runat="server">
    <div class="portlet">
        <h2>&nbsp;Subject Areas</h2>
        <a href="/ICatalog/dept/edit.aspx" class="menu" style="text-align:left">Subject Area Maintenance</a>
        <!-- <a href="/ICatalog/dept/copy.aspx" class="menu">Copy Courses</a> -->
        <a href="/ICatalog/dept/proof.aspx" class="menu">View Subject Areas</a>
    </div>
</asp:panel>

<asp:panel id="panCourse" runat="server">
    <div class="portlet">
        <h2>&nbsp;Courses</h2><!-- Used to be Titled ICatalog Review for Read Only and Course Admin for all others -->
        <a href="/ICatalog/course/view.aspx" class="menu">View Courses</a><!-- 3 -->
        <a id="lnkEditDeleteCourse" runat="server" href="/ICatalog/course/edit.aspx" class="menu">Course Maintenance</a>
        <a id="lnkCourseArchive" runat="server" href="/ICatalog/course/archive.aspx" class="menu">Course Archives</a>
        <a href="/ICatalog/course/proof.aspx" class="menu">View Descriptions</a><!-- 3 -->
        <a id="lnkCourseSearch" runat="server" href="/ICatalog/course/search.aspx" class="menu">Search Descriptions</a>
        <a href="/ICatalog/course/textexport.aspx" class="menu">Export to Text</a><!-- 3 -->
        <a id="lnkExportToGraphics" runat="server" href="/ICatalog/course/graphicsexport.aspx" class="menu">Export to Graphics</a>
        <a id="lnkExportToCatalog" runat="server" href="/ICatalog/course/printexport.aspx" class="menu">Export to Printable Catalog</a>
    </div>
</asp:panel>

<asp:panel id="panProgUserAdmin" runat="server"><!-- 1 only -->
	<div class="portlet">
		<h2>&nbsp;Security</h2>
		<a href="/ICatalog/program/grantpermissions.aspx" class="menu">Grant Permissions</a>
	</div>
</asp:panel>

<asp:panel id="panProg" runat="server">
	<div class="portlet">
		<h2>&nbsp;Programs</h2>
		<a href="/ICatalog/program/view.aspx" class="menu">View Programs</a>
		<a id="lnkFileUpload" runat="server" href="/ICatalog/program/degree/upload.aspx" class="menu">Degree File Upload</a>
		<a id="lnkDegMaint" runat="server" href="/ICatalog/program/degree/edit.aspx" class="menu">Degree Maintenance</a>
		<a id="lnkCatMaint" runat="server" href="/ICatalog/program/category/edit.aspx" class="menu">Category Maintenance</a>
        <a id="lnkPathwayMaint" runat="server" href="/ICatalog/program/pathway/edit.aspx" class="menu">Guided Pathway Maintenance</a>
        <a id="lnkAOSMaint" runat="server" href="/ICatalog/program/areaofstudy/edit.aspx" class="menu">Area of Study Maintenance</a>
		<a id="lnkProgMaint" runat="server" href="/ICatalog/program/edit.aspx" class="menu">Program Maintenance</a>
		<a id="lnkCrsSearch" runat="server" href="/ICatalog/program/coursesearch.aspx" class="menu">Course Search</a>
		<a id="lnkProgArchive" runat="server" href="/ICatalog/program/archive.aspx" class="menu">Program Archive</a>
		<a id="lnkCPGMaint" runat="server" href="/ICatalog/program/cpg/edit.aspx" class="menu">Career Planning Guide Maintenance</a>
		<a id="lnkProgramFees" runat="server" href="/ICatalog/program/fee/programfees.aspx" class="menu">Program Fees</a>
		<a id="lnkGraphExport" runat="server" href="/ICatalog/program/export.aspx" class="menu">Export to Graphics</a>
		<a id="lnkRTFExport" runat="server" href="/ICatalog/program/rtfexport.aspx" class="menu">Export to Word</a>
        <a id="lnkPrintExport" runat="server" href="/ICatalog/program/printexport.aspx" class="menu">Export to Printable Catalog</a>
	</div>
</asp:panel>

