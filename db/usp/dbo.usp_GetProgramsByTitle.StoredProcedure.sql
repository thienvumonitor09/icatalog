USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramsByTitle]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/30/17
-- Description:	Returns programs ordered by title
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramsByTitle] 
	@CollegeID int = NULL,
	@STRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)
					 
    SELECT ProgramTitle, ProgramVersionID, ProgramID, CollegeShortTitle, CollegeLongTitle 
    FROM Program 
    LEFT OUTER JOIN College 
    ON College.CollegeID = Program.CollegeID
    WHERE ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM) 
    AND (@CollegeID IS NULL OR Program.CollegeID = @CollegeID)
    AND PublishedProgram = 1 
    ORDER BY ProgramTitle, CollegeShortTitle;
END

GO
