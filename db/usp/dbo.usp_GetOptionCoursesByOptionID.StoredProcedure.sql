USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionCoursesByOptionID]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 06/14/2019
-- Description:	Returns program degree option courses
--		 INPUT: OptionID - Program degree option id 
--				SelectedSTRM - Selected term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionCoursesByOptionID]
	@OptionID int,
	@SelectedSTRM varchar (4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @SelectedSTRM IS NULL
	SET @SelectedSTRM = (SELECT STRM FROM udf_GetCurrentSTRM());
    
	SELECT OptionCourse.CourseOffering,/* Course.CourseID,*/ SUBJECT, CATALOG_NBR, /*CourseSuffix, CourseEndSTRM,*/ COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
	FROM OptionCourse 
	INNER JOIN ProgramDegreeOption
	ON ProgramDegreeOption.OptionID = OptionCourse.OptionID
	INNER JOIN ProgramDegree
	ON ProgramDegree.ProgramDegreeID = ProgramDegreeOption.ProgramDegreeID
	INNER JOIN Program 
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
	--LEFT OUTER JOIN tmp ON tmp.CourseOffering = OptionCourse.CourseOffering 
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
		ON REPLACE(CourseOffering.SUBJECT + CourseOffering.CATALOG_NBR,' ','') = REPLACE(OptionCourse.CourseOffering,' ','')
		LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
		ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	LEFT OUTER JOIN OptionFootnote 
	ON OptionFootnote.OptionFootnoteID = OptionCourse.OptionFootnoteID 
	WHERE OptionCourse.OptionID = @OptionID
		AND CourseOffering.EFFDT = 
			(
				SELECT MAX(CourseOffering2.EFFDT)
				FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
				WHERE CourseOffering2.CRSE_ID = CourseOffering.CRSE_ID 
					--AND INSTITUTION = 'WA171'
				AND CourseOffering2.EFFDT <= (SELECT * FROM udf_GetCurrentSTRM())
			)
		AND CATALOG_PRINT = 'Y'
	GROUP BY OptionCourse.CourseOffering,/* Course.CourseID,*/ SUBJECT, CATALOG_NBR, /*CourseSuffix, CourseEndSTRM,*/ COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
	ORDER BY Quarter, SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
END
GO
