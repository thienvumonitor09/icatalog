using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;
using System.Text.RegularExpressions;

namespace ICatalog.program {
    /// <summary>
    /// Summary description for add.
    /// </summary>
    public partial class edit : System.Web.UI.Page {

        programData csProgram = new programData();
        permissionData csPermissions = new permissionData();
        termData csTerm = new termData();

        #region PAGE LEVEL CONTROLS

        protected System.Web.UI.HtmlControls.HtmlInputHidden hidProgASN;

        #endregion

        #region SELECT PANEL CONTROLS


        #endregion

        #region ADD PANEL CONTROLS


        #endregion

        #region EDIT PANEL CONTROLS

        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkRevision2;

        #endregion

        #region STEP 2 PANEL CONTROLS

        protected System.Web.UI.WebControls.TextBox txtOptionTitle;

        #endregion

        #region CONFIRM PANEL CONTROLS

        //program outline confirmation fields

        //large text display comfirmation fields

        #endregion

        protected void Page_Load(object sender, System.EventArgs e) {

            #region CHECK USER LOGIN

            //check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

            #endregion

            #region HIDE PANELS

            panSelect.Visible = false;
            panAdd.Visible = false;
            panEdit.Visible = false;
            panStep2.Visible = false;
            panConfirm1.Visible = false;
            panConfirm2.Visible = false;
            panError2.Visible = false;

            #endregion

            if (!IsPostBack) {
                //get querystring search values from course search screen
                hidSearchText.Value = Request.QueryString["searchtxt"];
                hidSearchSubject.Value = Request.QueryString["searchsub"];
                hidSearchId.Value = Request.QueryString["searchid"];
                hidSearchSTRM.Value = Request.QueryString["searchstrm"];

                //validate form input values before submit
                cmdNext1.Attributes.Add("onclick", "return validate();");
                cmdNext2.Attributes.Add("onclick", "return validate();");
                cmdSubmitOutline.Attributes.Add("onclick", "return validate();");

                //populate offered by dropdownlists
                DataSet dsCollegeList = csProgram.GetCollegeList();
                Int32 intCollegeCount = dsCollegeList.Tables[0].Rows.Count;

                if (intCollegeCount > 0) {
                    for (Int32 intDSRow = 0; intDSRow < intCollegeCount; intDSRow++) {
                        String collegeShortTitle = dsCollegeList.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
                        String collegeID = dsCollegeList.Tables[0].Rows[intDSRow]["CollegeID"].ToString();
                        optOfferedAt1.Items.Add(new ListItem(collegeShortTitle, collegeID));
                        optOfferedAt2.Items.Add(new ListItem(collegeShortTitle, collegeID));
                        cboOfferedAt.Items.Add(new ListItem(collegeShortTitle, collegeID));
                    }
                    optOfferedAt1.SelectedIndex = 0; //select first college by default so areas of study can be populated by college
                    cboOfferedAt.SelectedValue = Request.QueryString["scolid"];
                }

                //hide error panels
                panError1.Visible = false;
                panError2.Visible = false;

                //populate program effective year quarter dropdownlists
                DataSet dsTerms = csProgram.GetTermsForPrograms();
                cboProgramEndSTRM1.Items.Add("Z999");
                cboProgramEndSTRM2.Items.Add("Z999");
                for (Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++) {
                    String STRM = dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString();
                    String DESCR = dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString();
                    cboProgramBeginSTRM1.Items.Add(new ListItem(DESCR, STRM));
                    cboProgramEndSTRM1.Items.Add(new ListItem(DESCR, STRM));
                    cboProgramBeginSTRM2.Items.Add(new ListItem(DESCR, STRM));
                    cboProgramEndSTRM2.Items.Add(new ListItem(DESCR, STRM));
                    cboTerm.Items.Add(new ListItem(DESCR, STRM));
                }

                String currentSTRM = csTerm.GetCurrentTerm();
                try {
                    cboProgramBeginSTRM1.SelectedValue = currentSTRM;
                    cboProgramEndSTRM1.SelectedValue = "Z999"; //currentSTRM; //changed 11/19/09
                } catch {
                    //do nothing
                }

                cboTerm.SelectedValue = Request.QueryString["strm"];

                //add degrees/certificates to dropdownlist
                DataSet dsDegreeList = csProgram.GetDegreeList();
                Int32 intDegreeCount = dsDegreeList.Tables[0].Rows.Count;
                if (intDegreeCount > 0) {
                    cboDegree1.Items.Add("");
                    cboDegree2.Items.Add("");
                    for (Int32 intDSRow = 0; intDSRow < intDegreeCount; intDSRow++) {
                        String degreeShortTitle = dsDegreeList.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
                        String strDegreeID = dsDegreeList.Tables[0].Rows[intDSRow]["DegreeID"].ToString();
                        /*
                        String documentTitle = dsDegreeList.Tables[0].Rows[intDSRow]["DocumentTitle"].ToString();
                        if (documentTitle != null && documentTitle != "") {
                            String fileName = csProgram.GetDegreeRequirementWorksheetFileNameBySTRM(documentTitle, "", "");
                            if (fileName != "") {
                                panDegree1.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick=\"popupCenter('degworksheet/" + fileName + "','Requirements', 950, 950);\">Requirements</a>"));
                            }
                        }
                        */
                        cboDegree1.Items.Add(new ListItem(degreeShortTitle, strDegreeID));
                        cboDegree2.Items.Add(new ListItem(degreeShortTitle, strDegreeID));
                    }
                } else {
                    cboDegree1.Items.Add(new ListItem("No degrees/certificates currently exist", ""));
                    cboDegree2.Items.Add(new ListItem("No degrees/certificates currently exist", ""));
                }

                //add areas of study to dropdownlist
                DataSet dsAreasOfStudy = csProgram.GetAreasOfStudy(optOfferedAt1.SelectedItem.Text);
                Int32 intAreaOfStudyCount = dsAreasOfStudy.Tables[0].Rows.Count;
                if (intAreaOfStudyCount > 0) {
                    cboAreaOfStudy1.Items.Add("");
                    cboAreaOfStudy2.Items.Add("");
                    for (Int32 intDSRow = 0; intDSRow < intAreaOfStudyCount; intDSRow++) {
                        String title = dsAreasOfStudy.Tables[0].Rows[intDSRow]["Title"].ToString();
                        String areaOfStudyID = dsAreasOfStudy.Tables[0].Rows[intDSRow]["AreaOfStudyID"].ToString();
                        cboAreaOfStudy1.Items.Add(new ListItem(title, areaOfStudyID));
                        cboAreaOfStudy2.Items.Add(new ListItem(title, areaOfStudyID));
                    }
                } else {
                    cboAreaOfStudy1.Items.Add(new ListItem("No areas of study currently exist", ""));
                    cboAreaOfStudy2.Items.Add(new ListItem("No areas of study currently exist", ""));
                }

                //add categories to dropdownlist
                DataSet dsCategory = csProgram.GetCategories();
                Int32 intCategoryCount = dsCategory.Tables[0].Rows.Count;
                if (intCategoryCount > 0) {
                    cboCategory1.Items.Add("");
                    cboCategory2.Items.Add("");
                    for (Int32 intDSRow = 0; intDSRow < intCategoryCount; intDSRow++) {
                        String categoryTitle = dsCategory.Tables[0].Rows[intDSRow]["CategoryTitle"].ToString();
                        String categoryID = dsCategory.Tables[0].Rows[intDSRow]["CategoryID"].ToString();
                        cboCategory1.Items.Add(new ListItem(categoryTitle, categoryID));
                        cboCategory2.Items.Add(new ListItem(categoryTitle, categoryID));
                    }
                } else {
                    cboCategory1.Items.Add(new ListItem("No categories currently exist", ""));
                    cboCategory2.Items.Add(new ListItem("No categories currently exist", ""));
                }

                String strProgramVersionID = Request.QueryString["pvid"];
                if (strProgramVersionID != null && strProgramVersionID != "" && Request.QueryString["todo"] != "pop") {
                    hidProgramVersionID.Value = strProgramVersionID;
                    hidPublishedProgramVersionID.Value = Request.QueryString["ppvid"];
                    hidProgramID.Value = Request.QueryString["id"];
                    panEdit.Visible = true;
                    hidTodo.Value = "editscreen";
                    GenerateEditScreen();
                }
            }

            String strTodo = Request.QueryString["todo"];
            if (strTodo == "pop" && hidTodo.Value == "") {

                #region REASSIGN FORM VALUES FROM POPUP WINDOW & GENERATE EDIT SCREEN
                /* Abbreviated QueryString Keys
				 * todo
				 * pvid = ProgramVersionID
				 * pdid = ProgramDegreeID
				 * oa = offered at
				 * oid = OptionID
				 * did = DegreeID
				 * PD = ProgramDisplay
				 * pt = Program title
				 * CASN = CollegeID - DOESN'T SEEM TO BE USED - CHECK INTO THIS!!!
				 * EASN = OptionElectiveGroupID
				 * FASN = OptionFootnoteID
				 * CDASN = CourseID
				 * BYRQ = Program Begin Effective Year Quarter
				 * EYRQ = Program End Effective Year Quarter
				 * STAT = Program Status (Working Copy/Published Copy)
				 * SASN = SameAsASN PK for Published Copy of the program the Working Copy of the program is tied to
				 * scasn = College Selected From Program List
				 * strm = Year Quarter Selected From Program List
				 * ltr = Letter Selected From Program List
				 * C = Category Title * REMOVE FROM QUERYSTRING AND RETRIEVE FROM DB! CAN CAUSE TO MANY CHARACTERS IN QUERYSTRING
				 * ID = Non-changing Program ID (ProgramID)
				 */

                hidTodo.Value = "step2";
                hidProgramVersionID.Value = Request.QueryString["pvid"];
                hidProgramDegreeID.Value = Request.QueryString["pdid"];
                hidLtr.Value = Request.QueryString["ltr"];
                hidSelectedCollegeID.Value = Request.QueryString["scolid"];
                hidSTRM.Value = Request.QueryString["strm"];
                hidProgramTitle.Value = Request.QueryString["pt"];
                txtProgramTitle2.Text = hidProgramTitle.Value;
                hidOfferedAt.Value = Request.QueryString["oa"];
                hidOptionID.Value = Request.QueryString["oid"];
                hidProgramDisplay.Value = Request.QueryString["pd"];
                hidDegreeID.Value = Request.QueryString["did"];
                hidProgramBeginSTRM.Value = Request.QueryString["bstrm"];
                hidProgramEndSTRM.Value = Request.QueryString["estrm"];
                hidPublishedProgram.Value = Request.QueryString["pp"];
                hidPublishedProgramVersionID.Value = Request.QueryString["ppvid"];
                hidProgramID.Value = Request.QueryString["id"];
                hidCollegeID.Value = Request.QueryString["colid"];
                GenerateEditScreen();

                #endregion

            }

            if (hidTodo.Value == "add") {

                #region show the add program panel

                //create the area of study checkboxes
                DataSet dsAreaOfStudy = csProgram.GetAreasOfStudy(optOfferedAt1.SelectedItem.Text);
                Int32 intAreaOfStudyCount = dsAreaOfStudy.Tables[0].Rows.Count;

                panAreasOfStudy1.Controls.Clear();
                cboAreaOfStudy1.Items.Clear();
                if (intAreaOfStudyCount > 0) {
                    cboAreaOfStudy1.Items.Add("");
                    for (Int32 intDSRow = 0; intDSRow < intAreaOfStudyCount; intDSRow++) {
                        String title = dsAreaOfStudy.Tables[0].Rows[intDSRow]["Title"].ToString();
                        String areaOfStudyID = dsAreaOfStudy.Tables[0].Rows[intDSRow]["AreaOfStudyID"].ToString();

                        HtmlInputCheckBox chkAreaOfStudy1 = new HtmlInputCheckBox();
                        chkAreaOfStudy1.Value = areaOfStudyID;
                        chkAreaOfStudy1.ID = "chkAreaOfStudy" + areaOfStudyID;
                        chkAreaOfStudy1.Name = "chkAreaOfStudy1";
                        chkAreaOfStudy1.Attributes["onclick"] = "addAreaOfStudy('" + chkAreaOfStudy1.ID + "','" + areaOfStudyID + "');";
                        panAreasOfStudy1.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:50%;vertical-align:top;\">"));
                        panAreasOfStudy1.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:20px;vertical-align:top;\">"));
                        panAreasOfStudy1.Controls.Add(chkAreaOfStudy1);
                        panAreasOfStudy1.Controls.Add(new LiteralControl("</div>"));
                        panAreasOfStudy1.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:334px;margin-top:2px;\">"));
                        panAreasOfStudy1.Controls.Add(new LiteralControl(title));
                        panAreasOfStudy1.Controls.Add(new LiteralControl("</div>"));
                        panAreasOfStudy1.Controls.Add(new LiteralControl("</div>"));

                        cboAreaOfStudy1.Items.Add(new ListItem(title, areaOfStudyID));
                    }
                } else {
                    cboCategory1.Items.Add(new ListItem("No areas of study currently exist", ""));
                }

                //create the category checkboxes
                DataSet dsCategory = csProgram.GetCategories();
                Int32 intCatCount = dsCategory.Tables[0].Rows.Count;

                panCategories1.Controls.Clear();

                if (intCatCount > 0) {
                    for (Int32 intDSRow = 0; intDSRow < intCatCount; intDSRow++) {
                        String categoryTitle = dsCategory.Tables[0].Rows[intDSRow]["CategoryTitle"].ToString();
                        String categoryID = dsCategory.Tables[0].Rows[intDSRow]["CategoryID"].ToString();

                        HtmlInputCheckBox chkCategory1 = new HtmlInputCheckBox();
                        chkCategory1.Value = categoryID;
                        chkCategory1.ID = "chk" + categoryID;
                        chkCategory1.Name = "chkCategory1";
                        chkCategory1.Attributes["onclick"] = "addCategory('" + chkCategory1.ID + "','" + categoryID + "');";
                        panCategories1.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:50%;vertical-align:top;\">"));
                        panCategories1.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:20px;vertical-align:top;\">"));
                        panCategories1.Controls.Add(chkCategory1);
                        panCategories1.Controls.Add(new LiteralControl("</div>"));
                        panCategories1.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:334px;margin-top:2px;\">"));
                        panCategories1.Controls.Add(new LiteralControl(categoryTitle));
                        panCategories1.Controls.Add(new LiteralControl("</div>"));
                        panCategories1.Controls.Add(new LiteralControl("</div>"));
                    }
                }

                //create the degree checkboxes for selection
                /*
				DataSet dsDegreeList = csProgram.GetDegreeList();
				Int32 intDegreeCount = dsDegreeList.Tables[0].Rows.Count;

				panDegree1.Controls.Clear();

				if(intDegreeCount > 0){
					for(Int32 intDSRow = 0; intDSRow < intDegreeCount; intDSRow++){
						String degreeShortTitle = dsDegreeList.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
						String strDegreeID = dsDegreeList.Tables[0].Rows[intDSRow]["DegreeID"].ToString();
						String documentTitle = dsDegreeList.Tables[0].Rows[intDSRow]["DocumentTitle"].ToString();

						HtmlInputCheckBox chkDegree1 = new HtmlInputCheckBox();
						chkDegree1.Value = strDegreeID;
						chkDegree1.ID = "chk" + strDegreeID;
						chkDegree1.Name = "chkDegree1";
						chkDegree1.Attributes["onclick"] = "addProgramDegree('" + chkDegree1.ID + "','" + strDegreeID + "');";		
						panDegree1.Controls.Add(chkDegree1);
						panDegree1.Controls.Add(new LiteralControl(degreeShortTitle));	
						if(documentTitle != null && documentTitle != ""){
							String fileName = csProgram.GetDegreeRequirementWorksheetFileNameBySTRM(documentTitle, "", "");
							if(fileName != ""){
								panDegree1.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick=\"popupCenter('degworksheet/" + fileName + "','Requirements', 950, 950);\">Requirements</a>"));
							}
						}
						panDegree1.Controls.Add(new LiteralControl("<br />"));
					}
				}
                */

                panAdd.Visible = true;

                #endregion

            } else if (hidTodo.Value == "editscreen") { //show the edit program panel (Step 1 Define Program)

                //generate and show screen 2
                panEdit.Visible = true;
                GenerateEditScreen();

            } else if (hidTodo.Value == "deleteProgram") {

                #region delete the selected program

                //delete the selected program
                csProgram.DeleteProgram(Convert.ToInt32(hidProgramVersionID.Value));

                //show the program list
                hidTodo.Value = "search";
                GenerateProgramList();
                panSelect.Visible = true;

                #endregion

            } else if (hidTodo.Value == "deletePrereq") {

                #region delete the selected prerequisite

                Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
                Int32 optionID = Convert.ToInt32(hidOptionID.Value);
                String optionDescription = txtOptionDescription.Value;

                //remove text search highlighting before DB submit
                if (hidSearchText.Value != null && hidSearchText.Value != "" && optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                    optionDescription = Regex.Replace(optionDescription, "<span style=\"background-color:yellow\">" + optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
                }

                //save the program option description
                Byte bitPrimaryOption = 0;
                if (chkPrimary.Checked == true) {
                    bitPrimaryOption = 1;
                }
                csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), optionDescription, txtOptionStateApproval.Text, txtCIP.Text, txtEPC.Text, txtAcademicPlan.Text, bitPrimaryOption, txtGainfulEmploymentID.Text, txtTotalQuarters.Text);

                //store locations
                csProgram.DeleteOptionLocations(optionID);
                if (hidLocationFields.Value != "") {
                    String[] strLocations = hidLocationFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strLocations.Length; i++) {
                        if (Request.Form[strLocations[i]] == "on") {
                            csProgram.AddOptionLocation(optionID, Convert.ToInt32(strLocations[i].Replace("chkLocation_" + optionID + "_", "")));
                        }
                    }
                }

                //store M Codes
                csProgram.DeleteOptionMCodes(optionID);
                if (hidMCodeFields.Value != "") {
                    String[] strMCodeFields = hidMCodeFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strMCodeFields.Length; i++) {
                        String[] strCollegeMCode = strMCodeFields[i].Split(';');
                        if (Request.Form[strCollegeMCode[0]] == "on") {
                            csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0].Replace("chkMCode_" + optionID + "_", "")), Request.Form[strCollegeMCode[1]]);
                        }
                    }
                }

                //delete the selected prerequisite
                csProgram.DeleteOptPrereq(Convert.ToInt32(hidOptionPrerequisiteID.Value));
                hidTodo.Value = "step2";
                DisplayProgDeg(Convert.ToInt32(hidProgramVersionID.Value));
                panStep2.Visible = true;

                #endregion

            } else if (hidTodo.Value == "deleteOptCrs") {

                #region delete the selected option course

                Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
                Int32 optionID = Convert.ToInt32(hidOptionID.Value);
                String optionDescription = txtOptionDescription.Value;

                //remove text search highlighting before DB submit
                if (hidSearchText.Value != null && hidSearchText.Value != "" && optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                    optionDescription = Regex.Replace(optionDescription, "<span style=\"background-color:yellow\">" + optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
                }

                //save the program option description
                Byte bitPrimaryOption = 0;
                if (chkPrimary.Checked == true) {
                    bitPrimaryOption = 1;
                }
                csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), optionDescription, txtOptionStateApproval.Text, txtCIP.Text, txtEPC.Text, txtAcademicPlan.Text, bitPrimaryOption, txtGainfulEmploymentID.Text, txtTotalQuarters.Text);

                //store locations
                csProgram.DeleteOptionLocations(optionID);
                if (hidLocationFields.Value != "") {
                    String[] strLocations = hidLocationFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strLocations.Length; i++) {
                        if (Request.Form[strLocations[i]] == "on") {
                            csProgram.AddOptionLocation(optionID, Convert.ToInt32(strLocations[i].Replace("chkLocation_" + optionID + "_", "")));
                        }
                    }
                }

                //store M Codes
                csProgram.DeleteOptionMCodes(optionID);
                if (hidMCodeFields.Value != "") {
                    String[] strMCodeFields = hidMCodeFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strMCodeFields.Length; i++) {
                        String[] strCollegeMCode = strMCodeFields[i].Split(';');
                        if (Request.Form[strCollegeMCode[0]] == "on") {
                            csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0].Replace("chkMCode_" + optionID + "_", "")), Request.Form[strCollegeMCode[1]]);
                        }
                    }
                }

                //delete the selected option course
                csProgram.DeleteOptCourse(Convert.ToInt32(hidOptionID.Value), csProgram.GetDegreeID(Convert.ToInt32(hidProgramDegreeID.Value)), hidCourseOffering.Value, Convert.ToInt16(hidQuarter.Value));
                hidTodo.Value = "step2";
                DisplayProgDeg(Convert.ToInt32(hidProgramVersionID.Value));
                panStep2.Visible = true;

                #endregion

            } else if (hidTodo.Value == "deleteElectGroupRef") {

                #region delete the selected option elective

                Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
                Int32 optionID = Convert.ToInt32(hidOptionID.Value);
                String optionDescription = txtOptionDescription.Value;

                //remove text search highlighting before DB submit
                if (hidSearchText.Value != null && hidSearchText.Value != "" && optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                    optionDescription = Regex.Replace(optionDescription, "<span style=\"background-color:yellow\">" + optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
                }

                //save the program option description
                Byte bitPrimaryOption = 0;
                if (chkPrimary.Checked == true) {
                    bitPrimaryOption = 1;
                }
                csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), optionDescription, txtOptionStateApproval.Text, txtCIP.Text, txtEPC.Text, txtAcademicPlan.Text, bitPrimaryOption, txtGainfulEmploymentID.Text, txtTotalQuarters.Text);

                //store locations
                csProgram.DeleteOptionLocations(optionID);
                if (hidLocationFields.Value != "") {
                    String[] strLocations = hidLocationFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strLocations.Length; i++) {
                        if (Request.Form[strLocations[i]] == "on") {
                            csProgram.AddOptionLocation(optionID, Convert.ToInt32(strLocations[i].Replace("chkLocation_" + optionID + "_", "")));
                        }
                    }
                }

                //store M Codes
                csProgram.DeleteOptionMCodes(optionID);
                if (hidMCodeFields.Value != "") {
                    String[] strMCodeFields = hidMCodeFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strMCodeFields.Length; i++) {
                        String[] strCollegeMCode = strMCodeFields[i].Split(';');
                        if (Request.Form[strCollegeMCode[0]] == "on") {
                            csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0].Replace("chkMCode_" + optionID + "_", "")), Request.Form[strCollegeMCode[1]]);
                        }
                    }
                }

                //delete the selected elective group reference
                csProgram.DeleteElectGroupRef(Convert.ToInt32(hidOptionID.Value), csProgram.GetDegreeID(Convert.ToInt32(hidProgramDegreeID.Value)), Convert.ToInt32(hidOptionElectiveGroupID.Value), Convert.ToInt16(hidQuarter.Value));
                hidTodo.Value = "step2";
                DisplayProgDeg(Convert.ToInt32(hidProgramVersionID.Value));
                panStep2.Visible = true;

                #endregion

            } else if (hidTodo.Value == "deleteElectGroupCrs") {

                #region delete the selected elective group course

                Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
                Int32 optionID = Convert.ToInt32(hidOptionID.Value);
                String optionDescription = txtOptionDescription.Value;

                //save the program option description
                Byte bitPrimaryOption = 0;
                if (chkPrimary.Checked == true) {
                    bitPrimaryOption = 1;
                }
                csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), optionDescription, txtOptionStateApproval.Text, txtCIP.Text, txtEPC.Text, txtAcademicPlan.Text, bitPrimaryOption, txtGainfulEmploymentID.Text, txtTotalQuarters.Text);

                //store locations
                csProgram.DeleteOptionLocations(optionID);
                if (hidLocationFields.Value != "") {
                    String[] strLocations = hidLocationFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strLocations.Length; i++) {
                        if (Request.Form[strLocations[i]] == "on") {
                            csProgram.AddOptionLocation(optionID, Convert.ToInt32(strLocations[i].Replace("chkLocation_" + optionID + "_", "")));
                        }
                    }
                }

                //store M Codes
                csProgram.DeleteOptionMCodes(optionID);
                if (hidMCodeFields.Value != "") {
                    String[] strMCodeFields = hidMCodeFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strMCodeFields.Length; i++) {
                        String[] strCollegeMCode = strMCodeFields[i].Split(';');
                        if (Request.Form[strCollegeMCode[0]] == "on") {
                            csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0].Replace("chkMCode_" + optionID + "_", "")), Request.Form[strCollegeMCode[1]]);
                        }
                    }
                }

                //delete the elective group course
                csProgram.DeleteElectiveGroupCourse(Convert.ToInt32(hidOptionElectiveGroupID.Value), hidCourseOffering.Value);
                hidTodo.Value = "step2";
                DisplayProgDeg(Convert.ToInt32(hidProgramVersionID.Value));
                panStep2.Visible = true;

                #endregion

            } else if (hidTodo.Value == "deleteElectGroup") {

                #region delete the selected elective group

                Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
                Int32 optionID = Convert.ToInt32(hidOptionID.Value);
                String optionDescription = txtOptionDescription.Value;

                //save the program option description
                Byte bitPrimaryOption = 0;
                if (chkPrimary.Checked == true) {
                    bitPrimaryOption = 1;
                }
                csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), optionDescription, txtOptionStateApproval.Text, txtCIP.Text, txtEPC.Text, txtAcademicPlan.Text, bitPrimaryOption, txtGainfulEmploymentID.Text, txtTotalQuarters.Text);

                //store locations
                csProgram.DeleteOptionLocations(optionID);
                if (hidLocationFields.Value != "") {
                    String[] strLocations = hidLocationFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strLocations.Length; i++) {
                        if (Request.Form[strLocations[i]] == "on") {
                            csProgram.AddOptionLocation(optionID, Convert.ToInt32(strLocations[i].Replace("chkLocation_" + optionID + "_", "")));
                        }
                    }
                }

                //store M Codes
                csProgram.DeleteOptionMCodes(optionID);
                if (hidMCodeFields.Value != "") {
                    String[] strMCodeFields = hidMCodeFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strMCodeFields.Length; i++) {
                        String[] strCollegeMCode = strMCodeFields[i].Split(';');
                        if (Request.Form[strCollegeMCode[0]] == "on") {
                            csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0].Replace("chkMCode_" + optionID + "_", "")), Request.Form[strCollegeMCode[1]]);
                        }
                    }
                }

                //delete the selected elective group, elective group courses and elective group references
                csProgram.DeleteOptionElectiveGroup(Convert.ToInt32(hidOptionElectiveGroupID.Value));
                hidTodo.Value = "step2";
                DisplayProgDeg(Convert.ToInt32(hidProgramVersionID.Value));
                panStep2.Visible = true;

                #endregion

            } else if (hidTodo.Value == "deleteFootnote") {

                #region delete the selected footnote

                Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value);
                Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
                Int32 optionID = Convert.ToInt32(hidOptionID.Value);
                String optionDescription = txtOptionDescription.Value;

                //remove text search highlighting before DB submit
                if (hidSearchText.Value != null && hidSearchText.Value != "" && optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                    optionDescription = Regex.Replace(optionDescription, "<span style=\"background-color:yellow\">" + optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
                }

                //save the program option description
                Byte bitPrimaryOption = 0;
                if (chkPrimary.Checked == true) {
                    bitPrimaryOption = 1;
                }
                csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), optionDescription, txtOptionStateApproval.Text, txtCIP.Text, txtEPC.Text, txtAcademicPlan.Text, bitPrimaryOption, txtGainfulEmploymentID.Text, txtTotalQuarters.Text);

                //store locations
                csProgram.DeleteOptionLocations(optionID);
                if (hidLocationFields.Value != "") {
                    String[] strLocations = hidLocationFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strLocations.Length; i++) {
                        if (Request.Form[strLocations[i]] == "on") {
                            csProgram.AddOptionLocation(optionID, Convert.ToInt32(strLocations[i].Replace("chkLocation_" + optionID + "_", "")));
                        }
                    }
                }

                //store M Codes
                csProgram.DeleteOptionMCodes(optionID);
                if (hidMCodeFields.Value != "") {
                    String[] strMCodeFields = hidMCodeFields.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strMCodeFields.Length; i++) {
                        String[] strCollegeMCode = strMCodeFields[i].Split(';');
                        if (Request.Form[strCollegeMCode[0]] == "on") {
                            csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0].Replace("chkMCode_" + optionID + "_", "")), Request.Form[strCollegeMCode[1]]);
                        }
                    }
                }

                //delete the selected footnote and reorder footnote numbers higher than the one deleted
                csProgram.DeleteFootnote(optionID, Convert.ToInt32(hidOptionFootnoteID.Value), Convert.ToInt16(hidFootnoteNumber.Value));
                hidTodo.Value = "step2";
                DisplayProgDeg(programVersionID);
                panStep2.Visible = true;

                #endregion

            } else if (hidTodo.Value == "deleteOption") {

                #region delete the selected program option

                Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value);
                Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
                Int32 degreeID = Convert.ToInt32(hidDegreeID.Value);
                Int32 optionID = Convert.ToInt32(hidOptionID.Value);

                //delete the selected program option
                csProgram.DeleteProgOption(degreeID, optionID, cboOptionTitle.SelectedItem.Text);
                hidTodo.Value = "step2";
                DisplayProgDeg(programVersionID);
                if (cboOptionTitle.Items.Count == 1) {
                    csProgram.UpdateOptionTitle(Convert.ToInt32(cboOptionTitle.SelectedValue));
                }
                panStep2.Visible = true;

                #endregion

            } else if (hidTodo.Value == "deleteDegree") {

                #region delete the selected program degree
                /* old code supporting multiple degrees
                Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value), programID = Convert.ToInt32(hidProgramID.Value), intOfferedAt = Convert.ToInt32(optOfferedAt2.SelectedValue);
                String programTitle = txtProgramTitle2.Text, programBeginSTRM = cboProgramBeginSTRM2.SelectedValue, strProgramEndSTRM = cboProgramEndSTRM2.SelectedValue, strCategoryID = cboCategory2.SelectedValue, strAreaOfStudyID = cboAreaOfStudy2.SelectedValue;

                //remove text search highlighting before DB submit
                if (hidSearchText.Value != null && hidSearchText.Value != "" && txtProgramDescription2.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                    txtProgramDescription2.Value = Regex.Replace(txtProgramDescription2.Value, "<span style=\"background-color:yellow\">" + txtProgramDescription2.Value.Substring(txtProgramDescription2.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", txtProgramDescription2.Value.Substring(txtProgramDescription2.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
                }

                //may want to show the error screen depending upon the return value
                Int32 intReturn = csProgram.SaveSelectedValues(programVersionID, programID, programTitle, strAreaOfStudyID, strCategoryID, programBeginSTRM, strProgramEndSTRM, intOfferedAt, txtProgramDescription2.Value, hidPublishedProgramVersionID.Value);

                if (intReturn > 0) {
                    //delete the degree with the ProgramDegreeID = hidDegreeID.Value
                    csProgram.DeleteProgDeg(Convert.ToInt32(hidDegreeID.Value), programVersionID);
                } else {
                    panEdit.Controls.Add(new LiteralControl("<script type=\"text/javascript\">document.getElementById(\"chk" + hidDegreeID.Value + "\").checked = true;</script>"));
                    hidDegreeID.Value = "";

                    if (intReturn == -1) {
                        //get begin and end terms of overlapping program
                        String termSpan = csProgram.GetConflictingTermsForExistingProgram(programVersionID, programTitle, intOfferedAt, programBeginSTRM, strProgramEndSTRM);

                        //display the error message
                        lblErrorMsg2.Text = "Error: The begin and end terms entered overlap an existing \"" + programTitle + "\" program offered by " + optOfferedAt2.Items.FindByValue(Request.Form["optOfferedAt2"]).Text;

                        if (termSpan != "") {
                            lblErrorMsg2.Text += " effective " + termSpan + ".";
                        } else {
                            lblErrorMsg2.Text += ".";
                        }
                    } else {
                        lblErrorMsg2.Text = "Error: The program update failed.";
                    }

                    //show error panel
                    panError2.Visible = true;
                }

                panEdit.Visible = true;
                hidTodo.Value = "editscreen";
                hidProgramDegreeID.Value = "";
                GenerateEditScreen();
                */
                #endregion

            } else if (hidTodo.Value == "changeStatus") {

                #region change the program status from Working Copy to Published Copy or vice-versa

                Int32 programVersionID = Convert.ToInt32(Request.Form["hidProgramVersionID"]), programID = Convert.ToInt32(Request.Form["hidProgramID"]), intOfferedAt = Convert.ToInt32(Request.Form["optOfferedAt2"]), degreeID = Convert.ToInt32(Request.Form["cboDegree2"]);
                String programDescription = Request.Form["txtProgramDescription2"].Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&");
                String programTitle = Request.Form["txtProgramTitle2"], programBeginSTRM = Request.Form["cboProgramBeginSTRM2"], strProgramEndSTRM = Request.Form["cboProgramEndSTRM2"];
                String publishedProgram = Request.Form["optPublishedProgram2"], strCategoryID = Request.Form["cboCategory2"], strAreaOfStudyID = Request.Form["cboAreaOfStudy2"];
                String strAddCategories = Request.Form["hidCategoryList"], strAddAreasOfStudy = Request.Form["hidAreaOfStudyList"];

                //remove text search highlighting before DB submit
                if (hidSearchText.Value != null && hidSearchText.Value != "" && programDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                    programDescription = Regex.Replace(programDescription, "<span style=\"background-color:yellow\">" + programDescription.Substring(programDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", programDescription.Substring(programDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
                }

                //copy all the program data from the Working Copy of the program to the Published Copy of the program or vice-versa
                Int32 intReturn = csProgram.EditProgDefinition(programVersionID, programID, degreeID, strAreaOfStudyID, strCategoryID, intOfferedAt, programTitle, programBeginSTRM, strProgramEndSTRM, publishedProgram, Request.Form["hidPublishedProgramVersionID"], programDescription, Convert.ToChar(Request.Form["optProgramDisplay2"]), Request.Cookies["phatt2"]["userctclinkid"], DateTime.Now);

                if (intReturn > 0) { //if the program data was successfully copied
                    if (publishedProgram == "0") {
                        //stores the Published ProgramVersionID when the program is changed to Working Copy
                        hidPublishedProgramVersionID.Value = programVersionID.ToString();
                        hidPublishedProgram.Value = "Working Copy";
                    } else if (publishedProgram == "1") {
                        hidPublishedProgramVersionID.Value = "";
                        hidPublishedProgram.Value = "Published Copy";
                    }
                    programVersionID = intReturn;
                    hidProgramVersionID.Value = programVersionID.ToString();

                    csProgram.DeleteAdditionalProgramAreasOfStudy(programID);
                    if (strAddAreasOfStudy != "") {
                        String[] aryAreaOfStudyList = strAddAreasOfStudy.Split(',');
                        for (Int16 i = 0; i < aryAreaOfStudyList.Length; i++) {
                            if (strAreaOfStudyID != aryAreaOfStudyList[i]) {
                                String areaOfStudyID = aryAreaOfStudyList[i];
                                csProgram.AddProgramAreaOfStudy(programID, Convert.ToInt32(areaOfStudyID));
                            }
                        }
                    }

                    csProgram.DeleteAdditionalProgramCategories(programVersionID);
                    if (strAddCategories != "") {
                        String[] aryCategoryList = strAddCategories.Split(',');
                        for (Int16 i = 0; i < aryCategoryList.Length; i++) {
                            if (strCategoryID != aryCategoryList[i]) {
                                String categoryID = aryCategoryList[i];
                                csProgram.AddProgramCategory(programVersionID, Convert.ToInt32(categoryID));
                            }
                        }
                    }

                    hidError.Value = "";

                    csProgram.UpdateProgramDegree(programVersionID, degreeID);

                    /* old code that supported multiple degrees/certificates
                    if (hidDegreeList.Value != "") {
                        if (hidExistingDegrees.Value != "") {
                            String[] aryExistingDegrees = hidExistingDegrees.Value.Split(',');
                            for (Int16 i = 0; i < aryExistingDegrees.Length; i++) {
                                if (hidDegreeList.Value.IndexOf("," + aryExistingDegrees[i], 0) > 0) {
                                    hidDegreeList.Value = hidDegreeList.Value.Replace("," + aryExistingDegrees[i], "");
                                } else {
                                    hidDegreeList.Value = hidDegreeList.Value.Replace(aryExistingDegrees[i], "");
                                }

                                if (hidDegreeList.Value.IndexOf(",") == 0) {
                                    hidDegreeList.Value = hidDegreeList.Value.Substring(1, hidDegreeList.Value.Length - 1);
                                }
                            }
                        }

                        
                        String[] aryDegreeList = hidDegreeList.Value.Split(',');
                        for (Int16 i = 0; i < aryDegreeList.Length; i++) {

                            Int32 degreeID = Convert.ToInt32(aryDegreeList[i]);

                            //Insert Program College Degrees
                            csProgram.AddProgramDegree(programVersionID, degreeID);

                            //store the ProgramDegreeID
                            hidProgramDegreeID.Value = csProgram.GetProgramDegreeID(programVersionID, degreeID).ToString();

                            //Insert Program Options
                            csProgram.AddProgOption(Convert.ToInt32(hidProgramDegreeID.Value));
                        }

                        /* clear hidDegreeList value so the application doesn't try 
                         * inserting the degrees again in cmdNext2_Click */
                        /* hidDegreeList.Value = "";
                        
                    }*/

                } else {

                    //reselect the appropriate status
                    if (hidPublishedProgram.Value == "Working Copy") {
                        optPublishedProgram2.SelectedValue = "0";
                    } else if (hidPublishedProgram.Value == "Published Copy") {
                        optPublishedProgram2.SelectedValue = "1";
                    }

                    if (intReturn == -1) {
                        //get begin and end terms of overlapping program
                        String termSpan = csProgram.GetConflictingTermsForExistingProgram(programVersionID, programTitle, intOfferedAt, degreeID, programBeginSTRM, strProgramEndSTRM);

                        //display the error message
                        lblErrorMsg2.Text = "Error: The begin and end terms entered overlap an existing \"" + cboDegree2.Items.FindByValue(degreeID.ToString()).Text + " - " + programTitle + "\" program offered by " + optOfferedAt2.Items.FindByValue(Request.Form["optOfferedAt2"]).Text;

                        if (termSpan != "") {
                            lblErrorMsg2.Text += " effective " + termSpan + ".";
                        } else {
                            lblErrorMsg2.Text += ".";
                        }
                    } else {
                        lblErrorMsg2.Text = "Error: The program update failed.";
                    }

                    //show error panels
                    panError2.Visible = true;
                }

                hidTodo.Value = "editscreen";
                panEdit.Visible = true;
                GenerateEditScreen();

                #endregion

            } else if (hidTodo.Value == "changeProgramDisplay") {

                #region change the program display

                Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value), programID = Convert.ToInt32(hidProgramID.Value), intOfferedAt = Convert.ToInt32(optOfferedAt2.SelectedValue), degreeID = Convert.ToInt32(cboDegree2.SelectedValue); 
                Char chrProgramDisplay = Convert.ToChar(optProgramDisplay2.SelectedValue);
                String programTitle = txtProgramTitle2.Text, programBeginSTRM = cboProgramBeginSTRM2.SelectedValue, strProgramEndSTRM = cboProgramEndSTRM2.SelectedValue, strCategoryID = cboCategory2.SelectedValue, strAreaOfStudyID = cboAreaOfStudy2.SelectedValue;

                //remove text search highlighting before DB submit
                if (hidSearchText.Value != null && hidSearchText.Value != "" && txtProgramDescription2.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                    txtProgramDescription2.Value = Regex.Replace(txtProgramDescription2.Value, "<span style=\"background-color:yellow\">" + txtProgramDescription2.Value.Substring(txtProgramDescription2.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", txtProgramDescription2.Value.Substring(txtProgramDescription2.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
                }

                //may want to show the error screen depending upon the return value
                Int32 intReturn = csProgram.SaveSelectedValues(programVersionID, programID, degreeID, programTitle, strAreaOfStudyID, strCategoryID, programBeginSTRM, strProgramEndSTRM, intOfferedAt, txtProgramDescription2.Value, hidPublishedProgramVersionID.Value);

                if (intReturn > 0) {

                    //add additional areas of study selected
                    csProgram.DeleteAdditionalProgramAreasOfStudy(programID);
                    if (hidAreaOfStudyList.Value != "") {
                        String[] aryAreaOfStudyList = hidAreaOfStudyList.Value.Split(',');
                        for (Int16 i = 0; i < aryAreaOfStudyList.Length; i++) {
                            if (strAreaOfStudyID != aryAreaOfStudyList[i]) {
                                String areaOfStudyID = aryAreaOfStudyList[i];
                                csProgram.AddProgramAreaOfStudy(programID, Convert.ToInt32(areaOfStudyID));
                            }
                        }
                    }

                    //add additional categories selected
                    csProgram.DeleteAdditionalProgramCategories(programVersionID);
                    if (hidCategoryList.Value != "") { //test this may need to use hidCategoryList from Request.Form
                        String[] aryCategoryList = hidCategoryList.Value.Split(',');
                        for (Int16 i = 0; i < aryCategoryList.Length; i++) {
                            if (strCategoryID != aryCategoryList[i]) {
                                String categoryID = aryCategoryList[i];
                                csProgram.AddProgramCategory(programVersionID, Convert.ToInt32(categoryID));
                            }
                        }
                    }

                    if (chrProgramDisplay == '1') {

                        //set large text field for the display to an empty string
                        csProgram.UpdateProgramTextOutline(programVersionID, "");
                        hidProgramDisplay.Value = "1";

                    } else if (chrProgramDisplay == '2') {
                        /* old code that supported multiple degrees/certificates
                        if (hidExistingDegrees.Value.IndexOf(",") > 0) {
                            String[] strDegrees = hidExistingDegrees.Value.Split(',');

                            for (Int32 i = 0; i < strDegrees.Length; i++) {
                                Int32 degreeID = Convert.ToInt32(strDegrees[i]);

                                //delete existing degree prereqs and program options
                                csProgram.DeleteProgOptions(degreeID, programVersionID);

                                //add default program options for each college/degree to avoid errors
                                csProgram.AddProgOption(csProgram.GetProgramDegreeID(programVersionID, degreeID));
                            }
                        } else if (hidExistingDegrees.Value != "") {

                            Int32 degreeID = Convert.ToInt32(hidExistingDegrees.Value);

                            //delete existing degree prereqs and program options
                            csProgram.DeleteProgOptions(degreeID, programVersionID);

                            //add default program options for each college/degree to avoid errors
                            csProgram.AddProgOption(csProgram.GetProgramDegreeID(programVersionID, degreeID));
                        }
                        */

                        //update selected degree
                        csProgram.UpdateProgramDegree(programVersionID, degreeID);

                        //delete existing degree prereqs and program options
                        csProgram.DeleteProgOptions(degreeID, programVersionID);

                        //add default program options for each college/degree to avoid errors
                        csProgram.AddProgOption(csProgram.GetProgramDegreeID(programVersionID, degreeID));

                        hidProgramDisplay.Value = "2";
                    }

                    //update the ProgramDisplay identifier
                    csProgram.UpdateProgramDisplay(programVersionID, chrProgramDisplay);

                } else {

                    optProgramDisplay2.SelectedValue = hidProgramDisplay.Value;

                    if (intReturn == -1) {
                        //get begin and end terms of overlapping program
                        String termSpan = csProgram.GetConflictingTermsForExistingProgram(programVersionID, programTitle, intOfferedAt, degreeID, programBeginSTRM, strProgramEndSTRM);

                        //display the error message
                        lblErrorMsg2.Text = "Error: The begin and end terms entered overlap an existing \"" + cboDegree2.Items.FindByValue(degreeID.ToString()).Text + " - " + programTitle + "\" program offered by " + optOfferedAt2.Items.FindByValue(Request.Form["optOfferedAt2"]).Text;

                        if (termSpan != "") {
                            lblErrorMsg2.Text += " effective " + termSpan + ".";
                        } else {
                            lblErrorMsg2.Text += ".";
                        }
                    } else {
                        lblErrorMsg2.Text = "Error: The program update failed.";
                    }

                    //show error panel
                    panError2.Visible = true;
                }

                panEdit.Visible = true;
                hidTodo.Value = "editscreen";
                GenerateEditScreen();

                #endregion

            } else if (hidTodo.Value == "progRevision") {

                #region add a new version of the current program
                Int32 programVersionID = Convert.ToInt32(Request.Form["hidProgramVersionID"]), programID = Convert.ToInt32(Request.Form["hidProgramID"]), intOfferedBy = Convert.ToInt32(Request.Form["optOfferedAt2"]), degreeID = Convert.ToInt32(Request.Form["cboDegree2"]);
                String programDescription = Request.Form["txtProgramDescription2"].Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&"), programTitle = spanRevisionProgramTitle.InnerHtml;
                String publishedProgram = Request.Form["optPublishedProgram2"], programBeginSTRM = Request.Form["hidProgramBeginSTRM"], strProgramEndSTRM = Request.Form["hidProgramEndSTRM"];
                String strAreaOfStudyID = Request.Form["cboAreaOfStudy2"], strAddAreasOfStudy = Request.Form["hidAreaOfStudyList"];
                String strCategoryID = Request.Form["cboCategory2"], strAddCategories = Request.Form["hidCategoryList"];
                Char chrProgramDisplay = Convert.ToChar(Request.Form["optProgramDisplay2"]);
                String lastModifiedEmployeeID = Request.Cookies["phatt2"]["userctclinkid"];
                DateTime lastModifiedDate = DateTime.Now;

                //remove text search highlighting before DB submit
                if (hidSearchText.Value != null && hidSearchText.Value != "" && programDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                    programDescription = Regex.Replace(programDescription, "<span style=\"background-color:yellow\">" + programDescription.Substring(programDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", programDescription.Substring(programDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
                }

                //copy over all program data to a new version of the program
                Int32 newProgramVersionID = csProgram.SaveProgRevision(programVersionID, programID, degreeID, strAreaOfStudyID, strCategoryID, intOfferedBy, programTitle, programBeginSTRM, strProgramEndSTRM, "0", programDescription, chrProgramDisplay, lastModifiedEmployeeID, lastModifiedDate);

                if (newProgramVersionID > 0) {
                    //get previous program CPG data
                    DataSet dsPreviousCPG = csProgram.GetCareerPlanningGuide(programVersionID);

                    //get previous program fees
                    DataSet dsProgramFees = csProgram.GetProgramFees(programVersionID);

                    programVersionID = newProgramVersionID;
                    hidProgramVersionID.Value = programVersionID.ToString();

                    //add program CPG data for new program - get all data from previous program
                    Int32 careerPlanningGuideID = csProgram.AddProgCPGData(programVersionID, Request.Cookies["phatt2"]["userctclinkid"], DateTime.Now);

                    if (dsPreviousCPG.Tables[0].Rows.Count > 0) {
                        //update new version CPG data with previous version CPG data
                        csProgram.EditProgCPGData(careerPlanningGuideID, programVersionID, dsPreviousCPG.Tables[0].Rows[0]["ProgramEnrollment"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["ProgramWebsiteURL"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["CareerPlanningGuideFormat"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["ProgramCourseOfStudy"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["ProgramGoals"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["ProgramCareerOpportunities"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["PublishedCareerPlanningGuide"].ToString(), "", lastModifiedEmployeeID, lastModifiedDate);
                    }

                    //add program fees to new version of program
                    if (dsProgramFees.Tables[0].Rows.Count > 0) {
                        csProgram.EditProgramFees(programVersionID, dsProgramFees.Tables[0].Rows[0]["BookMinimum"].ToString(), dsProgramFees.Tables[0].Rows[0]["BookMaximum"].ToString(), dsProgramFees.Tables[0].Rows[0]["SuppliesMinimum"].ToString(), dsProgramFees.Tables[0].Rows[0]["SuppliesMaximum"].ToString(), dsProgramFees.Tables[0].Rows[0]["MiscMinimum"].ToString(), dsProgramFees.Tables[0].Rows[0]["MiscMaximum"].ToString(), dsProgramFees.Tables[0].Rows[0]["Note"].ToString());
                    }

                    //store additional areas of study selected
                    csProgram.DeleteAdditionalProgramAreasOfStudy(programID);
                    if (strAddAreasOfStudy != "") {
                        String[] aryAreaOfStudyList = strAddAreasOfStudy.Split(',');
                        for (Int16 i = 0; i < aryAreaOfStudyList.Length; i++) {
                            if (strAreaOfStudyID != aryAreaOfStudyList[i]) {
                                String areaOfStudyID = aryAreaOfStudyList[i];
                                csProgram.AddProgramAreaOfStudy(programID, Convert.ToInt32(areaOfStudyID));
                            }
                        }
                    }

                    //store additional categories selected
                    csProgram.DeleteAdditionalProgramCategories(programVersionID);
                    if (strAddCategories != "") {
                        String[] aryCategoryList = strAddCategories.Split(',');
                        for (Int16 i = 0; i < aryCategoryList.Length; i++) {
                            if (strCategoryID != aryCategoryList[i]) {
                                String categoryID = aryCategoryList[i];
                                csProgram.AddProgramCategory(programVersionID, Convert.ToInt32(categoryID));
                            }
                        }
                    }

                    /* Old code that supported multiple degrees/certificates
                    if (hidDegreeList.Value != "") {
                        if (hidExistingDegrees.Value != "") {
                            String[] aryExistingDegrees = hidExistingDegrees.Value.Split(',');
                            for (Int16 i = 0; i < aryExistingDegrees.Length; i++) {
                                if (hidDegreeList.Value.IndexOf("," + aryExistingDegrees[i], 0) > 0) {
                                    hidDegreeList.Value = hidDegreeList.Value.Replace("," + aryExistingDegrees[i], "");
                                } else {
                                    hidDegreeList.Value = hidDegreeList.Value.Replace(aryExistingDegrees[i], "");
                                }

                                if (hidDegreeList.Value.IndexOf(",") == 0) {
                                    hidDegreeList.Value = hidDegreeList.Value.Substring(1, hidDegreeList.Value.Length - 1);
                                }
                            }
                        }

                        String[] aryDegreeList = hidDegreeList.Value.Split(',');
                        for (Int16 i = 0; i < aryDegreeList.Length; i++) {

                            Int32 degreeID = Convert.ToInt32(aryDegreeList[i]);

                            //Insert Program College Degrees
                            csProgram.AddProgramDegree(programVersionID, degreeID);

                            //store the ProgramDegreeID
                            hidProgramDegreeID.Value = csProgram.GetProgramDegreeID(programVersionID, degreeID).ToString();

                            //Insert Program Options
                            csProgram.AddProgOption(Convert.ToInt32(hidProgramDegreeID.Value));
                        }
                    }
                    */

                } else {

                    if (newProgramVersionID == -1) {
                        //get begin and end terms of overlapping program
                        String termSpan = csProgram.GetConflictingTermsForExistingProgram(programVersionID, programTitle, intOfferedBy, degreeID, programBeginSTRM, strProgramEndSTRM);

                        //display the error message
                        lblErrorMsg2.Text = "Error: The begin and end terms entered overlap an existing \"" + cboDegree2.Items.FindByValue(degreeID.ToString()).Text + " - " + programTitle + "\" program offered by " + optOfferedAt2.Items.FindByValue(Request.Form["optOfferedAt2"]).Text;

                        if (termSpan != "") {
                            lblErrorMsg2.Text += " effective " + termSpan + ".";
                        } else {
                            lblErrorMsg2.Text += ".";
                        }
                    } else {
                        lblErrorMsg2.Text = "Error: The program update failed.";
                    }

                    //show error panels
                    panError2.Visible = true;
                }

                hidTodo.Value = "editscreen";
                panEdit.Visible = true;
                GenerateEditScreen();
                #endregion

            } else if (hidTodo.Value == "step2") { //figure out when this is called

                #region generate and show program outline or large text display screen

                //String strProgramDegreeID = Request.Form["cboDegree"];
                String strOptionID = Request.Form["cboOptionTitle"];

                //if ((strProgramDegreeID != null && strProgramDegreeID != Request.Form["hidProgramDegreeID"]) || (strOptionID != null && strOptionID != Request.Form["hidOptionID"])) {
                if (strOptionID != null && strOptionID != Request.Form["hidOptionID"]) {
                    Byte bitPrimaryOption = 0;
                    if (Request.Form["chkPrimary"] == "on") {
                        bitPrimaryOption = 1;
                    }

                    Int32 optionID = Convert.ToInt32(Request.Form["hidOptionID"]);
                    String optionDescription = Request.Form["txtOptionDescription"];

                    //remove text search highlighting before DB submit
                    if (hidSearchText.Value != null && hidSearchText.Value != "" && optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                        optionDescription = Regex.Replace(optionDescription, "<span style=\"background-color:yellow\">" + optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
                    }

                    csProgram.EditOptionValues(optionID, Convert.ToInt32(Request.Form["hidProgramVersionID"]), optionDescription, Request.Form["txtOptionStateApproval"], Request.Form["txtCIP"], Request.Form["txtEPC"], Request.Form["txtAcademicPlan"], bitPrimaryOption, Request.Form["txtGainfulEmploymentID"], Request.Form["txtTotalQuarters"]);

                    //store locations
                    csProgram.DeleteOptionLocations(optionID);
                    if (hidLocationFields.Value != "") {
                        String[] strLocations = hidLocationFields.Value.Substring(1).Split('|');
                        for (Int32 i = 0; i < strLocations.Length; i++) {
                            if (Request.Form[strLocations[i]] == "on") {
                                csProgram.AddOptionLocation(optionID, Convert.ToInt32(strLocations[i].Replace("chkLocation_" + optionID + "_", "")));
                            }
                        }
                    }

                    //store M Codes
                    csProgram.DeleteOptionMCodes(optionID);
                    if (hidMCodeFields.Value != "") {
                        String[] strMCodeFields = hidMCodeFields.Value.Substring(1).Split('|');
                        for (Int32 i = 0; i < strMCodeFields.Length; i++) {
                            String[] strCollegeMCode = strMCodeFields[i].Split(';');
                            if (Request.Form[strCollegeMCode[0]] == "on") {
                                csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0].Replace("chkMCode_" + optionID + "_", "")), Request.Form[strCollegeMCode[1]]);
                            }
                        }
                    }
                }

                //generate and show screen 3
                DisplayProgDeg(Convert.ToInt32(hidProgramVersionID.Value));
                panStep2.Visible = true;

                #endregion

            } else {

                #region generate and show the program list

                //generate and show the program list alphabetically
                if (hidTodo.Value != "search") {
                    hidLtr.Value = Request.QueryString["ltr"];
                    hidSelectedCollegeID.Value = Request.QueryString["scolid"];
                    hidSTRM.Value = Request.QueryString["strm"];
                }
                GenerateProgramList();
                panSelect.Visible = true;

                #endregion

            }
        }

        public void GenerateProgramList() { //generate and show the program list alphabetically
            TableRow tr = new TableRow();
            TableCell td = new TableCell();

            //create the alphabet selection list
            for (Int16 intAlphaCtr = 65; intAlphaCtr <= 90; intAlphaCtr++) {
                td = new TableCell();
                td.Attributes["style"] = "width:23px;text-align:center;";
                td.Attributes["onclick"] = "document.frmProgram.hidTodo.value='search';document.frmProgram.hidLtr.value='" + Convert.ToChar(intAlphaCtr) + "';document.frmProgram.submit();return false;";
                td.CssClass = "portletMain";
                td.Controls.Add(new LiteralControl("<a class=\"ltr\" href=\"#\" onclick=\"document.frmProgram.hidTodo.value='search';document.frmProgram.hidLtr.value='" + Convert.ToChar(intAlphaCtr) + "';document.frmProgram.submit();return false;\">" + Convert.ToChar(intAlphaCtr) + "</a>"));
                tr.Cells.Add(td);
            }

            //td = new TableCell();
            //td.Attributes["style"] = "padding:1px;width:1px;";
            //tr.Cells.Add(td);

            tblAlpha.Rows.Add(tr);

            //select letter 'A' as the default
            String strLtr = hidLtr.Value;
            if (strLtr == "") {
                strLtr = "A";
            }

            //get a dataset of programs starting with the selected letter
            DataSet dsPrograms = csProgram.GetCurrentAndFuturePrograms(strLtr, cboTerm.SelectedValue, cboOfferedAt.SelectedValue);
            Int32 intRowCtr = dsPrograms.Tables[0].Rows.Count;

            //display the program header information
            tr = new TableRow();

            td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:105px;";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("Award Type"));

            tr.Cells.Add(td);

            td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:218px;";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("Program Title"));

            tr.Cells.Add(td);

            td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;white-space:nowrap;width:69px;";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("Offered by"));

            tr.Cells.Add(td);

            td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:85px;white-space:nowrap";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("Begin Term"));

            tr.Cells.Add(td);

            td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:85px;white-space:nowrap";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("End Term"));

            tr.Cells.Add(td);

            td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:82px;";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("Status"));

            tr.Cells.Add(td);

            td = new TableCell();
            td.Attributes["style"] = "text-align:center;width:73px;";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.frmProgram.hidTodo.value='add';document.frmProgram.hidSTRM.value=document.frmProgram.cboTerm.value;document.frmProgram.hidSelectedCollegeID.value=document.frmProgram.cboOfferedAt.value;document.frmProgram.submit();return false;\">Add New</a>"));

            tr.Cells.Add(td);
            tblDisplay.Rows.Add(tr);

            String oldAreaOfStudyTitle = "";

            if (intRowCtr > 0) { //if area of study titles starting with the selected letter exist

                //loop through and display the programs
                for (Int32 intDSRow = 0; intDSRow < intRowCtr; intDSRow++) {
                    Int32 programVersionID = Convert.ToInt32(dsPrograms.Tables[0].Rows[intDSRow]["ProgramVersionID"]);
                    Int32 programID = Convert.ToInt32(dsPrograms.Tables[0].Rows[intDSRow]["ProgramID"]);
                    String programTitle = dsPrograms.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString();
                    String degreeShortTitle = dsPrograms.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
                    String publishedProgram = dsPrograms.Tables[0].Rows[intDSRow]["PublishedProgram"].ToString();
                    String publishedProgramVersionID = dsPrograms.Tables[0].Rows[intDSRow]["PublishedProgramVersionID"].ToString();
                    String collegeShortTitle = dsPrograms.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
                    String beginTerm = dsPrograms.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString();
                    String endTerm = dsPrograms.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString();
                    String programDisplay = dsPrograms.Tables[0].Rows[intDSRow]["ProgramDisplay"].ToString();
                    String areaOfStudyTitle = dsPrograms.Tables[0].Rows[intDSRow]["Title"].ToString();

                    if (publishedProgram == "0") {
                        publishedProgram = "Working Copy";
                    } else if (publishedProgram == "1") {
                        publishedProgram = "Published Copy";
                    }

                    if (areaOfStudyTitle != oldAreaOfStudyTitle) {
                        tr = new TableRow();
                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "font-weight:bold;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;width:100%;";
                        td.ColumnSpan = 7;
                        td.Controls.Add(new LiteralControl(areaOfStudyTitle));
                        tr.Cells.Add(td);
                        tblDisplay.Rows.Add(tr);
                    }

                    tr = new TableRow();

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:105px;";
                    td.Controls.Add(new LiteralControl(degreeShortTitle));

                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:218px;";
                    td.Controls.Add(new LiteralControl(programTitle));

                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:69px;";
                    td.Controls.Add(new LiteralControl(collegeShortTitle));

                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:85px;";
                    td.Controls.Add(new LiteralControl(beginTerm));

                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:85px;";
                    td.Controls.Add(new LiteralControl(endTerm));

                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;text-align:right;width:82px;";
                    td.Controls.Add(new LiteralControl(publishedProgram));

                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;white-space:nowrap;width:73px;";
                    if (!csProgram.HasWorkingProgramCopy(programVersionID)) {
                        td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.frmProgram.hidTodo.value='editscreen';document.frmProgram.hidSTRM.value=document.frmProgram.cboTerm.value;document.frmProgram.hidSelectedCollegeID.value=document.frmProgram.cboOfferedAt.value;document.frmProgram.hidProgramVersionID.value='" + programVersionID + "';document.frmProgram.hidProgramID.value='" + programID + "';document.frmProgram.hidPublishedProgramVersionID.value='" + publishedProgramVersionID + "';document.frmProgram.submit();return false;\">Edit</a>"));
                        //if(strProgramVersionID != ""){
                        td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete the " + publishedProgram + " \\n" + programTitle.Replace("'", "\\'") + " program offered by " + collegeShortTitle + " \\neffective " + beginTerm + " - " + endTerm + "?')){document.frmProgram.hidTodo.value='deleteProgram';document.frmProgram.hidSTRM.value=document.frmProgram.cboTerm.value;document.frmProgram.hidSelectedCollegeID.value=document.frmProgram.cboOfferedAt.value;document.frmProgram.hidProgramVersionID.value='" + programVersionID + "';document.frmProgram.hidProgramID.value='" + programID + "';document.frmProgram.submit();}\">Delete</a>"));
                        //}
                    } else {
                        td.Controls.Add(new LiteralControl("<a href=\"view.aspx?pvid=" + programVersionID + "&pd=" + programDisplay + "\">View</a>"));
                    }
                    tr.Cells.Add(td);
                    tblDisplay.Rows.Add(tr);

                    oldAreaOfStudyTitle = areaOfStudyTitle;
                }

            } else { //else if program titles starting with the selected letter do not exist

                //display "selected letter returned 0 results"
                tr = new TableRow();
                td = new TableCell();
                td.CssClass = "portletLight";
                td.ColumnSpan = 7;
                td.Attributes["style"] = "padding-top:4px;padding-bottom:4px;";
                td.Controls.Add(new LiteralControl("&nbsp;" + strLtr + " returned 0 results."));
                tr.Cells.Add(td);
                tblDisplay.Rows.Add(tr);

            }
        }

        public void GenerateEditScreen() {
            /*******************************************************************************************************
             * Get the program title, effective year quarters, colleges offering the program, degrees offered, 
             * program display type and program description for the selected program.
             *******************************************************************************************************/

            Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value);
            Int32 programID = Convert.ToInt32(hidProgramID.Value); //could be null, check !IsPostBack to make sure value is assigned - BV 3-19-18

            if (panError2.Visible) {
                txtProgramTitle2.Text = Request.Form["txtProgramTitle2"];
                optOfferedAt2.SelectedValue = Request.Form["optOfferedAt2"];
                cboProgramBeginSTRM2.SelectedValue = Request.Form["cboProgramBeginSTRM2"];
                cboProgramEndSTRM2.SelectedValue = Request.Form["cboProgramEndSTRM2"];
                cboCategory2.SelectedValue = Request.Form["cboCategory2"];
                txtProgramDescription2.Value = Request.Form["txtProgramDescription2"];
                cboDegree2.SelectedValue = Request.Form["cboDegree2"];
                cboAreaOfStudy2.SelectedValue = Request.Form["cboAreaOfStudy2"];
            } else {
                //get the program data
                DataSet dsProgramDescription = csProgram.GetProgramDescription(programVersionID);
                Int32 intProgCount = dsProgramDescription.Tables[0].Rows.Count;

                if (intProgCount > 0) {

                    DataSet dsAreasOfStudy = csProgram.GetAreasOfStudy(dsProgramDescription.Tables[0].Rows[0]["CollegeShortTitle"].ToString());
                    Int32 areaOfStudyCount = dsAreasOfStudy.Tables[0].Rows.Count;
                    if (areaOfStudyCount > 0) {
                        cboAreaOfStudy2.Items.Clear();
                        cboAreaOfStudy2.Items.Add("");
                        for (Int32 intDSRow = 0; intDSRow < areaOfStudyCount; intDSRow++) {
                            String title = dsAreasOfStudy.Tables[0].Rows[intDSRow]["Title"].ToString();
                            String areaOfStudyID = dsAreasOfStudy.Tables[0].Rows[intDSRow]["AreaOfStudyID"].ToString();
                            cboAreaOfStudy2.Items.Add(new ListItem(title, areaOfStudyID));
                        }
                    } else {
                        cboAreaOfStudy2.Items.Add(new ListItem("No areas of study currently exist", ""));
                    }

                    txtProgramTitle2.Text = dsProgramDescription.Tables[0].Rows[0]["ProgramTitle"].ToString();
                    spanRevisionProgramTitle.InnerHtml = txtProgramTitle2.Text;
                    hidProgramTitle.Value = txtProgramTitle2.Text;
                    hidProgramID.Value = dsProgramDescription.Tables[0].Rows[0]["ProgramID"].ToString();
                    hidCollegeID.Value = dsProgramDescription.Tables[0].Rows[0]["CollegeID"].ToString();
                    txtProgramDescription2.Value = dsProgramDescription.Tables[0].Rows[0]["ProgramDescription"].ToString();
                    optPublishedProgram2.SelectedValue = dsProgramDescription.Tables[0].Rows[0]["PublishedProgram"].ToString();
                    cboProgramBeginSTRM2.SelectedValue = dsProgramDescription.Tables[0].Rows[0]["ProgramBeginSTRM"].ToString();
                    cboProgramEndSTRM2.SelectedValue = dsProgramDescription.Tables[0].Rows[0]["ProgramEndSTRM"].ToString();
                    cboAreaOfStudy2.SelectedValue = dsProgramDescription.Tables[0].Rows[0]["AreaOfStudyID"].ToString();
                    cboCategory2.SelectedValue = dsProgramDescription.Tables[0].Rows[0]["CategoryID"].ToString();
                    lblProgModSID.Text = csPermissions.GetEmpName(dsProgramDescription.Tables[0].Rows[0]["LastModifiedEMPLID"].ToString());
                    lblProgModTS.Text = dsProgramDescription.Tables[0].Rows[0]["LastModifiedDate"].ToString();
                    lblCategory.Text = cboCategory2.SelectedItem.Text;
                    lblAreaOfStudy.Text = cboAreaOfStudy2.SelectedItem.Text;

                    if (hidSearchText.Value != null && hidSearchText.Value != "" && txtProgramDescription2.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1 && txtProgramDescription2.Value.ToLower().IndexOf("background-color:yellow") == -1) {
                        txtProgramDescription2.Value = Regex.Replace(txtProgramDescription2.Value, hidSearchText.Value, "<span style=\"background-color:yellow\">" + txtProgramDescription2.Value.Substring(txtProgramDescription2.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", RegexOptions.IgnoreCase);
                    }

                    if (optPublishedProgram2.SelectedValue == "0") {
                        hidPublishedProgram.Value = "Working Copy";
                    } else if (optPublishedProgram2.SelectedValue == "1") {
                        hidPublishedProgram.Value = "Published Copy";
                    }
                    optPublishedProgram2.Attributes["onclick"] = "checkStatus('optPublishedProgram2')";

                    hidProgramBeginSTRM.Value = cboProgramBeginSTRM2.SelectedValue;
                    hidProgramEndSTRM.Value = cboProgramEndSTRM2.SelectedValue;
                    hidBeginTerm.Value = cboProgramBeginSTRM2.SelectedItem.Text;
                    hidEndTerm.Value = cboProgramEndSTRM2.SelectedItem.Text;
                }

                //Get the ProgramDisplay here for use throughout the application -- insert selection on step2
                DataSet dsProgram = csProgram.GetProgram(programVersionID);
                Int32 intProgramCount = dsProgram.Tables[0].Rows.Count;

                //populate and select ProgramDisplay from the db
                hidProgramDisplay.Value = dsProgram.Tables[0].Rows[0]["ProgramDisplay"].ToString();
                optProgramDisplay2.SelectedValue = hidProgramDisplay.Value;
                optProgramDisplay2.Attributes["onclick"] = "checkDisplay('optProgramDisplay2');";

                if (hidCollegeChanged.Value != "true") {
                    hidOfferedAt.Value = dsProgram.Tables[0].Rows[0]["CollegeShortTitle"].ToString();
                    optOfferedAt2.SelectedValue = dsProgram.Tables[0].Rows[0]["CollegeID"].ToString();
                } else {
                    //re-populate the primary area of study selectio list
                    DataSet dsAreasOfStudy = csProgram.GetAreasOfStudy(optOfferedAt2.Items.FindByValue(optOfferedAt2.SelectedValue).Text);
                    Int32 areaOfStudyCount = dsAreasOfStudy.Tables[0].Rows.Count;
                    if (areaOfStudyCount > 0) {
                        cboAreaOfStudy2.Items.Clear();
                        cboAreaOfStudy2.Items.Add("");
                        for (Int32 intDSRow = 0; intDSRow < areaOfStudyCount; intDSRow++) {
                            String title = dsAreasOfStudy.Tables[0].Rows[intDSRow]["Title"].ToString();
                            String areaOfStudyID = dsAreasOfStudy.Tables[0].Rows[intDSRow]["AreaOfStudyID"].ToString();
                            cboAreaOfStudy2.Items.Add(new ListItem(title, areaOfStudyID));
                        }
                        try {
                            cboAreaOfStudy2.SelectedValue = dsProgramDescription.Tables[0].Rows[0]["AreaOfStudyID"].ToString();
                        } catch {
                            //do nothing
                        }
                    } else {
                        cboAreaOfStudy2.Items.Add(new ListItem("No areas of study currently exist", ""));
                    }
                }
                hidCollegeChanged.Value = "";
            }

            //store areas of study in arraylist for contains comparison
            ArrayList lstExistingAreaOfStudy = new ArrayList();
            String[] aryAreaOfStudyList = hidAreaOfStudyList.Value.Split(',');
            for (Int32 i = 0; i < aryAreaOfStudyList.Length; i++) {
                lstExistingAreaOfStudy.Add(aryAreaOfStudyList[i]);
            }

            ArrayList lstAreasOfStudy = new ArrayList();
            DataSet dsProgramAreasOfStudy = csProgram.GetProgramAreasOfStudy(Convert.ToInt32(hidProgramID.Value)); //test this
            Int32 programAreaOfStudyCount = dsProgramAreasOfStudy.Tables[0].Rows.Count;
            if (programAreaOfStudyCount > 0) {
                for (Int32 intDSRow = 0; intDSRow < programAreaOfStudyCount; intDSRow++) {
                    String areaOfStudyID = dsProgramAreasOfStudy.Tables[0].Rows[intDSRow]["AreaOfStudyID"].ToString();
                    lstAreasOfStudy.Add(dsProgramAreasOfStudy.Tables[0].Rows[intDSRow]["Title"].ToString());
                    if (!lstExistingAreaOfStudy.Contains(areaOfStudyID) && cboAreaOfStudy2.SelectedValue != areaOfStudyID) { //make sure the area of study doesn't already exist
                        hidAreaOfStudyList.Value += "," + areaOfStudyID;
                    }
                }
                if (hidAreaOfStudyList.Value.IndexOf(",") == 0) {
                    hidAreaOfStudyList.Value = hidAreaOfStudyList.Value.Substring(1, hidAreaOfStudyList.Value.Length - 1);
                }
            }

            //create and populate the area of study checkboxes
            DataSet dsAreaOfStudy = csProgram.GetAreasOfStudy(optOfferedAt2.SelectedItem.Text); //test this
            Int32 intAreaOfStudyCount = dsAreaOfStudy.Tables[0].Rows.Count;

            panAreasOfStudy2.Controls.Clear();

            if (intAreaOfStudyCount > 0) {
                for (Int32 intDSRow = 0; intDSRow < intAreaOfStudyCount; intDSRow++) {
                    String title = dsAreaOfStudy.Tables[0].Rows[intDSRow]["Title"].ToString();
                    String areaOfStudyID = dsAreaOfStudy.Tables[0].Rows[intDSRow]["AreaOfStudyID"].ToString();

                    HtmlInputCheckBox chkAreaOfStudy2 = new HtmlInputCheckBox();
                    chkAreaOfStudy2.Value = areaOfStudyID;
                    chkAreaOfStudy2.ID = "chkAreaOfStudy" + areaOfStudyID;
                    chkAreaOfStudy2.Name = "chkAreaOfStudy2";

                    String strSelectedAreaOfStudy = Request.Form[chkAreaOfStudy2.ID];

                    if (strSelectedAreaOfStudy != null && strSelectedAreaOfStudy != "" && strSelectedAreaOfStudy != lblAreaOfStudy.Text) {
                        chkAreaOfStudy2.Checked = true;
                    }

                    if (lstAreasOfStudy.Contains(title) && title != lblAreaOfStudy.Text) {
                        chkAreaOfStudy2.Checked = true;
                    }

                    chkAreaOfStudy2.Attributes["onclick"] = "addAreaOfStudy('" + chkAreaOfStudy2.ID + "','" + areaOfStudyID + "');";
                    panAreasOfStudy2.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:50%;vertical-align:top;\">"));
                    panAreasOfStudy2.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:20px;vertical-align:top;\">"));
                    panAreasOfStudy2.Controls.Add(chkAreaOfStudy2);
                    panAreasOfStudy2.Controls.Add(new LiteralControl("</div>"));
                    panAreasOfStudy2.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:334px;margin-top:2px;\">"));
                    panAreasOfStudy2.Controls.Add(new LiteralControl(title));
                    panAreasOfStudy2.Controls.Add(new LiteralControl("</div>"));
                    panAreasOfStudy2.Controls.Add(new LiteralControl("</div>"));
                }
            }

            //store categories in arraylist for contains comparison
            ArrayList lstExistingCat = new ArrayList();
            String[] aryCatList = hidCategoryList.Value.Split(',');
            for (Int32 i = 0; i < aryCatList.Length; i++) {
                lstExistingCat.Add(aryCatList[i]);
            }

            ArrayList lstCategories = new ArrayList();
            DataSet dsProgramCategories = csProgram.GetProgramCategories(programVersionID);
            Int32 programCategoryCount = dsProgramCategories.Tables[0].Rows.Count;
            if (programCategoryCount > 0) {
                for (Int32 intDSRow = 0; intDSRow < programCategoryCount; intDSRow++) {
                    String categoryID = dsProgramCategories.Tables[0].Rows[intDSRow]["CategoryID"].ToString();
                    lstCategories.Add(dsProgramCategories.Tables[0].Rows[intDSRow]["CategoryTitle"].ToString());
                    if (!lstExistingCat.Contains(categoryID) && cboCategory2.SelectedValue != categoryID) { //make sure the category doesn't already exist
                        hidCategoryList.Value += "," + categoryID;
                    }
                }
                if (hidCategoryList.Value.IndexOf(",") == 0) {
                    hidCategoryList.Value = hidCategoryList.Value.Substring(1, hidCategoryList.Value.Length - 1);
                }
            }

            //create and populate the category checkboxes
            DataSet dsCategory = csProgram.GetCategories();
            Int32 intCatCount = dsCategory.Tables[0].Rows.Count;

            panCategories2.Controls.Clear();

            if (intCatCount > 0) {
                for (Int32 intDSRow = 0; intDSRow < intCatCount; intDSRow++) {
                    String categoryTitle = dsCategory.Tables[0].Rows[intDSRow]["CategoryTitle"].ToString();
                    String categoryID = dsCategory.Tables[0].Rows[intDSRow]["CategoryID"].ToString();

                    HtmlInputCheckBox chkCategory2 = new HtmlInputCheckBox();
                    chkCategory2.Value = categoryID;
                    chkCategory2.ID = "chk" + categoryID;
                    chkCategory2.Name = "chkCategory2";

                    String strSelectedCategory = Request.Form[chkCategory2.ID];

                    if (strSelectedCategory != null && strSelectedCategory != "" && strSelectedCategory != lblCategory.Text) {
                        chkCategory2.Checked = true;
                    }

                    if (lstCategories.Contains(categoryTitle) && categoryTitle != lblCategory.Text) {
                        chkCategory2.Checked = true;
                    }

                    chkCategory2.Attributes["onclick"] = "addCategory('" + chkCategory2.ID + "','" + categoryID + "');";
                    panCategories2.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:50%;vertical-align:top;\">"));
                    panCategories2.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:20px;vertical-align:top;\">"));
                    panCategories2.Controls.Add(chkCategory2);
                    panCategories2.Controls.Add(new LiteralControl("</div>"));
                    panCategories2.Controls.Add(new LiteralControl("<div style=\"display:inline-block;width:334px;margin-top:2px;\">"));
                    panCategories2.Controls.Add(new LiteralControl(categoryTitle));
                    panCategories2.Controls.Add(new LiteralControl("</div>"));
                    panCategories2.Controls.Add(new LiteralControl("</div>"));
                }
            }

            DataSet dsProgramDegree = csProgram.GetProgramDegree(programVersionID);
            Int32 intProgramDegreeCount = dsProgramDegree.Tables[0].Rows.Count;
            if (intProgramDegreeCount > 0) {
                cboDegree2.SelectedValue = dsProgramDegree.Tables[0].Rows[0]["DegreeID"].ToString();
                hidProgramDegreeID.Value = dsProgramDegree.Tables[0].Rows[0]["ProgramDegreeID"].ToString();
                lblDegree.Text = dsProgramDegree.Tables[0].Rows[0]["DegreeShortTitle"].ToString();
            }

            /* old code that supported multiple degrees
			//create and populate the degree checkboxes
			hidExistingDegrees.Value = "";
			ArrayList lstDegrees = new ArrayList();
			DataSet dsProgramDegree = csProgram.GetProgramDegree(programVersionID);
			Int32 intProgramDegreeCount = dsProgramDegree.Tables[0].Rows.Count;

			if(intProgramDegreeCount > 0){
				for(Int32 intDSRow = 0; intDSRow < dsProgramDegree.Tables[0].Rows.Count; intDSRow++){
					lstDegrees.Add(dsProgramDegree.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString());
					hidExistingDegrees.Value += dsProgramDegree.Tables[0].Rows[intDSRow]["DegreeID"].ToString() + ",";
				}
				hidExistingDegrees.Value = hidExistingDegrees.Value.Substring(0,hidExistingDegrees.Value.Length-1);
			}else{
				//set hidden form element value so deleted ProgramDegree values are not used from the querystring on page load
				hidDegreesRemoved.Value = "True";
			}

			DataSet dsDegreeList = csProgram.GetDegreeList();
			Int32 intDegreeCount = dsDegreeList.Tables[0].Rows.Count;

			panDegree2.Controls.Clear();

			if(intDegreeCount > 0){
				
				for(Int32 intDSRow = 0; intDSRow < intDegreeCount; intDSRow++){
					String degreeShortTitle = dsDegreeList.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
					String strDegreeID = dsDegreeList.Tables[0].Rows[intDSRow]["DegreeID"].ToString();
					String documentTitle = dsDegreeList.Tables[0].Rows[intDSRow]["DocumentTitle"].ToString();

					HtmlInputCheckBox chkDegree2 = new HtmlInputCheckBox();
					chkDegree2.Value = strDegreeID;
					chkDegree2.ID = "chk" + strDegreeID;
					chkDegree2.Name = "chkDegree2";

					if(Request.Form[chkDegree2.ID] != null){
						chkDegree2.Checked = true;
					}
					
					if(lstDegrees.Contains(degreeShortTitle)){
						chkDegree2.Checked = true;
						chkDegree2.Attributes["onclick"] = "deleteProgramDegree('" + chkDegree2.ID + "','" + degreeShortTitle.Replace("'","\\'") + "');";
					}else{
						chkDegree2.Attributes["onclick"] = "addProgramDegree('" + chkDegree2.ID + "','" + strDegreeID + "');";
					}
					
					panDegree2.Controls.Add(chkDegree2);
					panDegree2.Controls.Add(new LiteralControl(degreeShortTitle));	
					if(documentTitle != null && documentTitle != ""){
						String fileName = csProgram.GetDegreeRequirementWorksheetFileNameBySTRM(documentTitle, cboProgramBeginSTRM2.SelectedValue, cboProgramEndSTRM2.SelectedValue);
						if(fileName != ""){
							panDegree2.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick=\"popupCenter('degworksheet/" + fileName + "','Requirements', 950, 950);\">Requirements</a>"));
						}
					}
					panDegree2.Controls.Add(new LiteralControl("<br />"));
				}
			}
            */
        }

        private void DisplayProgDeg(Int32 programVersionID) { //Get the degree(s) for the selected program

            lblProgramTitle.Text = hidProgramTitle.Value;
            lblOfferedAt.Text = hidOfferedAt.Value;
            lblProgramBeginSTRM.Text = hidBeginTerm.Value; //hidProgramBeginSTRM.Value;
            lblProgramEndSTRM.Text = hidEndTerm.Value; //hidProgramEndSTRM.Value;
            lblPublishedProgram.Text = hidPublishedProgram.Value;

            DataSet dsAreasOfStudy = csProgram.GetProgramAreasOfStudy(Convert.ToInt32(hidProgramID.Value));
            lblAddAreasOfStudy.Text = "";
            for (Int32 intDSRow = 0; intDSRow < dsAreasOfStudy.Tables[0].Rows.Count; intDSRow++) {
                if (Convert.ToByte(dsAreasOfStudy.Tables[0].Rows[intDSRow]["PrimaryAreaOfStudy"]) == 1) {
                    lblAreaOfStudy.Text = dsAreasOfStudy.Tables[0].Rows[intDSRow]["Title"].ToString();
                } else {
                    lblAddAreasOfStudy.Text += "; " + dsAreasOfStudy.Tables[0].Rows[intDSRow]["Title"].ToString();
                }
            }
            if (lblAddAreasOfStudy.Text.IndexOf(";") == 0) {
                lblAddAreasOfStudy.Text = lblAddAreasOfStudy.Text.Substring(1, lblAddAreasOfStudy.Text.Length - 1);
            }

            DataSet dsCategories = csProgram.GetProgramCategories(programVersionID);
            lblAddCategories.Text = "";
            for (Int32 intDSRow = 0; intDSRow < dsCategories.Tables[0].Rows.Count; intDSRow++) {
                if (Convert.ToByte(dsCategories.Tables[0].Rows[intDSRow]["PrimaryCategory"]) == 1) {
                    lblCategory.Text = dsCategories.Tables[0].Rows[intDSRow]["CategoryTitle"].ToString();
                } else {
                    lblAddCategories.Text += "; " + dsCategories.Tables[0].Rows[intDSRow]["CategoryTitle"].ToString();
                }
            }
            if (lblAddCategories.Text.IndexOf(";") == 0) {
                lblAddCategories.Text = lblAddCategories.Text.Substring(1, lblAddCategories.Text.Length - 1);
            }

            DataSet dsProgramDegree = csProgram.GetProgramDegree(programVersionID);
            //String degreeShortTitles = "";

            //cboDegree.Items.Clear();
            tblOptCourses.Rows.Clear();
            tblDegPrereqs.Rows.Clear();
            tblOptionElectives.Rows.Clear();
            tblProgFootnotes.Rows.Clear();

            /*
            for (Int32 intDSRow = 0; intDSRow < dsProgramDegree.Tables[0].Rows.Count; intDSRow++) {
                Int32 programDegreeID = Convert.ToInt32(dsProgramDegree.Tables[0].Rows[intDSRow]["ProgramDegreeID"]);
                String degreeShortTitle = dsProgramDegree.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
                cboDegree.Items.Add(new ListItem(dsProgramDegree.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString(), programDegreeID.ToString()));
                degreeShortTitles += ", " + degreeShortTitle;

                //used for large text display program only
                String fileName = csProgram.GetDegreeRequirementWorksheetByProgramDegreeID(programDegreeID);
                if (fileName != null && fileName != "") {
                    degreeShortTitles += ": <a href=\"#\" onclick=\"popupCenter('degworksheet/" + fileName + "','Requirements', 950, 950);\">Requirements/Worksheet</a>";
                }
            }
            degreeShortTitles = degreeShortTitles.Substring(2);

            try {
                //select the previously selected degree
                String strSelected = Request.Form["cboDegree"];

                if (strSelected == null) {
                    cboDegree.SelectedValue = hidProgramDegreeID.Value;
                } else {
                    cboDegree.SelectedValue = strSelected;
                }

            } catch {
                //do nothing
            }

            hidProgramDegreeID.Value = cboDegree.SelectedValue;
            hidDegreeID.Value = csProgram.GetDegreeID(Convert.ToInt32(hidProgramDegreeID.Value)).ToString();
            */

            lblSelectedDeg.Text = lblDegree.Text;
            Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);

            if (Request.Form["hidProgramDisplay"] == "1" || hidProgramDisplay.Value == "1") {

                String fileName = csProgram.GetDegreeRequirementWorksheetByProgramDegreeID(programDegreeID);
                if (fileName != null && fileName != "") {
                    lblDegreeRequirementWorksheetLink.Text = "<a href=\"#\" onclick=\"popupCenter('degworksheet/" + fileName + "','Requirements', 950, 950);\">Requirements</a>";
                } else {
                    lblDegreeRequirementWorksheetLink.Text = "";
                }

                //Get the program options for the selected degree
                DataSet dsProgOptions = csProgram.GetProgOptions(programDegreeID);
                Int32 intProgOptionCount = dsProgOptions.Tables[0].Rows.Count;

                cboOptionTitle.Items.Clear();

                if (intProgOptionCount > 1) {

                    for (Int32 intDSRow = 0; intDSRow < dsProgOptions.Tables[0].Rows.Count; intDSRow++) {
                        cboOptionTitle.Items.Add(new ListItem(dsProgOptions.Tables[0].Rows[intDSRow]["OptionTitle"].ToString(), dsProgOptions.Tables[0].Rows[intDSRow]["OptionID"].ToString()));
                    }
                    hidMultipleOptions.Value = "1";
                    spanOptDeleteLnk.Visible = true;
                    spanOptLnks.Visible = true;

                } else {

                    String optionTitle = dsProgOptions.Tables[0].Rows[0]["OptionTitle"].ToString();
                    String strOptionID = dsProgOptions.Tables[0].Rows[0]["OptionID"].ToString();

                    if (optionTitle.Trim() != "") {
                        cboOptionTitle.Items.Add(optionTitle);
                    } else {
                        cboOptionTitle.Items.Add("");
                        cboOptionTitle.Width = Unit.Pixel(150);
                    }
                    cboOptionTitle.Items[0].Value = strOptionID;

                    hidMultipleOptions.Value = "0";
                    spanOptDeleteLnk.Visible = false;
                    spanOptLnks.Visible = true;

                }

                try {
                    //select the previously selected program option
                    String strSelectedOpt = Request.Form["cboOptionTitle"];

                    if (strSelectedOpt == null) {
                        cboOptionTitle.SelectedValue = hidOptionID.Value;
                    } else {
                        cboOptionTitle.SelectedValue = strSelectedOpt;
                    }
                } catch {
                    //do nothing
                }

                //populate option values
                hidOptionID.Value = cboOptionTitle.SelectedValue;
                Int32 optionID = Convert.ToInt32(hidOptionID.Value);
                DataSet dsProgOptValues = csProgram.GetProgOptValues(optionID);
                if (dsProgOptValues.Tables[0].Rows.Count > 0) {
                    txtOptionDescription.Value = dsProgOptValues.Tables[0].Rows[0]["OptionDescription"].ToString();
                    txtOptionStateApproval.Text = dsProgOptValues.Tables[0].Rows[0]["OptionStateApproval"].ToString();
                    txtGainfulEmploymentID.Text = dsProgOptValues.Tables[0].Rows[0]["GainfulEmploymentID"].ToString();
                    txtCIP.Text = dsProgOptValues.Tables[0].Rows[0]["CIP"].ToString();
                    txtEPC.Text = dsProgOptValues.Tables[0].Rows[0]["EPC"].ToString();
                    txtAcademicPlan.Text = dsProgOptValues.Tables[0].Rows[0]["ACAD_PLAN"].ToString();
                    txtTotalQuarters.Text = dsProgOptValues.Tables[0].Rows[0]["TotalQuarters"].ToString();
                    String strPrimary = dsProgOptValues.Tables[0].Rows[0]["PrimaryOption"].ToString();
                    if (strPrimary == "True") {
                        chkPrimary.Checked = true;
                    } else {
                        chkPrimary.Checked = false;
                    }

                    if (hidSearchText.Value != null && hidSearchText.Value != "" && txtOptionDescription.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                        txtOptionDescription.Value = Regex.Replace(txtOptionDescription.Value, hidSearchText.Value, "<span style=\"background-color:yellow\">" + txtOptionDescription.Value.Substring(txtOptionDescription.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", RegexOptions.IgnoreCase);
                    }
                }

                //populate location checkboxes
                DataSet dsLocations = csProgram.GetLocationList();
                hidLocationFields.Value = "";
                locationColumn.Controls.Clear();
                for (Int32 i = 0; i < dsLocations.Tables[0].Rows.Count; i++) {
                    String strLocationID = dsLocations.Tables[0].Rows[i]["LocationID"].ToString();

                    CheckBox chkLocation = new CheckBox();
                    chkLocation.ID = "chkLocation_" + optionID.ToString() + "_" + strLocationID;
                    chkLocation.Text = dsLocations.Tables[0].Rows[i]["LocationTitle"].ToString();

                    if (i == 0) {
                        locationColumn.Controls.Add(new LiteralControl("<div>"));
                    } else {
                        locationColumn.Controls.Add(new LiteralControl("<div style=\"padding-top:5px;\">"));
                    }
                    locationColumn.Controls.Add(chkLocation);
                    locationColumn.Controls.Add(new LiteralControl("</div>"));

                    hidLocationFields.Value += "|" + chkLocation.ID;
                }

                DataSet dsOptionLocations = csProgram.GetOptionLocations(optionID);
                for (Int32 i = 0; i < dsOptionLocations.Tables[0].Rows.Count; i++) {
                    CheckBox chk = (CheckBox)locationColumn.FindControl("chkLocation_" + optionID + "_" + dsOptionLocations.Tables[0].Rows[i]["LocationID"].ToString());
                    chk.Checked = true;
                }

                //populate campus checkboxes and EPC/M-Code textboxes for each campus
                lblMCodeDesc.Text = "<b>Classes that apply to this program option at " + lblOfferedAt.Text + " can be taken at the following campuses:</b>";

                DataSet dsCollegeList = csProgram.GetCollegeList();
                bool blnOtherCollegeExists = false;
                hidMCodeFields.Value = "";
                mcodeColumn.Controls.Clear();
                for (Int32 i = 0; i < dsCollegeList.Tables[0].Rows.Count; i++) {
                    String collegeShortTitle = dsCollegeList.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
                    String collegeID = dsCollegeList.Tables[0].Rows[i]["CollegeID"].ToString();
                    if (collegeShortTitle != lblOfferedAt.Text) {
                        blnOtherCollegeExists = true;

                        CheckBox chkCollege = new CheckBox();
                        chkCollege.ID = "chkMCode" + "_" + optionID.ToString() + "_" + collegeID;
                        chkCollege.Attributes["style"] = "font-weight:bold;";
                        chkCollege.Text = collegeShortTitle;

                        TextBox txtMCode = new TextBox();
                        txtMCode.ID = "txtMCode" + "_" + optionID.ToString() + "_" + collegeID;
                        txtMCode.CssClass = "small";
                        txtMCode.MaxLength = 4;
                        txtMCode.Columns = 10;

                        String strMCode = csProgram.GetOptionCollegeMCode(optionID, Convert.ToInt16(collegeID));
                        if (strMCode != null && strMCode != "") {
                            chkCollege.Checked = true;
                            txtMCode.Text = strMCode;
                        }

                        hidMCodeFields.Value += "|" + chkCollege.ID + ";" + txtMCode.ID;

                        if (i == 0) {
                            mcodeColumn.Controls.Add(new LiteralControl("<div>"));
                        } else {
                            mcodeColumn.Controls.Add(new LiteralControl("<div style=\"padding-top:5px;\">"));
                        }
                        mcodeColumn.Controls.Add(chkCollege);
                        mcodeColumn.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;<b>M-Code:&nbsp;</b>"));
                        mcodeColumn.Controls.Add(txtMCode);
                        mcodeColumn.Controls.Add(new LiteralControl("</div>"));
                    }
                }
                if (blnOtherCollegeExists) {
                    panMCode.Visible = true;
                } else {
                    panMCode.Visible = false;
                }

                #region GET PREREQUISITES FROM THE DB FOR DISPLAY

                DataSet dsPrereqs = csProgram.GetOptPrereqs(Convert.ToInt32(cboOptionTitle.SelectedValue), hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
                DataTable dtPrereqs = dsPrereqs.Tables[0];
                //Int32 intPrereqCount = dsPrereqs.Tables[0].Rows.Count;
                TableRow tr = new TableRow();
                TableCell td = new TableCell();

                if (dtPrereqs.Rows.Count > 0) {
                    String oldCourseOffering = "";

                    foreach (DataRow drPrereqs in dtPrereqs.Rows) {
                        String courseOffering = drPrereqs["CourseOffering"].ToString();
                        String courseSubject = drPrereqs["SUBJECT"].ToString();
                        String courseLongTitle = drPrereqs["COURSE_TITLE_LONG"].ToString();
                        String strOptionPrerequisiteID = drPrereqs["OptionPrerequisiteID"].ToString();
                        String strOptionFootnoteID = drPrereqs["OptionFootnoteID"].ToString();
                        //String courseBeginSTRM = dsPrereqs.Tables[0].Rows[intDSRow]["CourseBeginSTRM"].ToString();
                        //String courseEndSTRM = dsPrereqs.Tables[0].Rows[intDSRow]["CourseEndSTRM"].ToString();
                        String catalogNbr = drPrereqs["CATALOG_NBR"].ToString();
                        String courseSubjectDisplay = courseSubject;

                        if (hidSearchId.Value == courseOffering) {
                            courseSubjectDisplay = "<span style=\"background-color:yellow\">" + courseSubjectDisplay + "</span>";
                            catalogNbr = "<span style=\"background-color:yellow\">" + catalogNbr + "</span>";
                        }

                        if (oldCourseOffering != courseOffering) {
                            /*
                            bool expiredSTRM = csProgram.ExpiredSTRM(courseEndSTRM);
                            String strStyle = "", note = "";
                            if (expiredSTRM) {
                                strStyle = "color:red;";
                                note = "&nbsp;&nbsp;[Course Ended " + csTerm.GetTermDescription(courseEndSTRM) + "]";
                            }
                            */

                            tr = new TableRow();

                            td = new TableCell();
                            td.Attributes["style"] = "width:55px;vertical-align:top;";// +strStyle;
                            td.CssClass = "portletLight";
                            td.Controls.Add(new LiteralControl("�&nbsp;&nbsp;" + courseSubjectDisplay));

                            tr.Cells.Add(td);
                            
                            td = new TableCell();
                            td.Attributes["style"] = "width:30px;vertical-align:top;";// +strStyle;
                            td.CssClass = "portletLight";
                            //td.Controls.Add(new LiteralControl(catalogNbr + dsPrereqs.Tables[0].Rows[intDSRow]["CourseSuffix"].ToString()));
                            td.Controls.Add(new LiteralControl(""));
                            tr.Cells.Add(td);
                            
                            td = new TableCell();
                            td.CssClass = "portletLight";
                            td.Attributes["style"] = "width:350px;vertical-align:top;";// +strStyle;
                            //remove course details until the new course data (lab, lecture, clinical, other, pay type, intent, etc...) can be figured out - 5/8/16
                            //td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/coursedetail.aspx?id=" + courseOffering.Replace("&", "%26") + "&bstrm=" + courseBeginSTRM + "&estrm=" + courseEndSTRM + "&colid=" + hidCollegeID.Value + "&TITLE=" + courseLongTitle.Replace("'", "\'").Replace("#", "%23") + "','CourseDetail', 570, 400);\">" + courseLongTitle + "</a>"));
                            td.Controls.Add(new LiteralControl(courseLongTitle));
                            td.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + dsPrereqs.Tables[0].Rows[intDSRow]["FootnoteNumber"].ToString() + "</i></span>"));// + note));

                            tr.Cells.Add(td);

                            td = new TableCell();
                            td.CssClass = "portletLight";
                            td.Attributes["style"] = "width:70px;vertical-align:top;text-align:right;";
                            //if (!expiredSTRM) {
                                td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/editprereq.aspx?pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'", "\\'") + "&pt=" + hidProgramTitle.Value.Replace("'", "\\'") + "&pid=" + strOptionPrerequisiteID + "&fid=" + strOptionFootnoteID + "&co=" + courseOffering.Replace("&", "%26") + "&cs=" + courseSubject.Replace("&", "%26") + "&oid=" + hidOptionID.Value + "&pd=" + hidProgramDisplay.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&", "%26").Replace("'", "\\'") + "', 'AddDegreePrereq', 355, 325);return false;\">Edit</a> | "));
                            //}
                            td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete " + courseOffering + " from the prerequisites?')){document.frmProgram.hidTodo.value='deletePrereq';document.frmProgram.hidOptionPrerequisiteID.value='" + strOptionPrerequisiteID + "';document.frmProgram.hidCourseOffering.value='" + courseOffering + "';document.frmProgram.submit();}return false;\">Delete</a>"));
                            tr.Cells.Add(td);

                            tblDegPrereqs.Rows.Add(tr);
                        }
                        oldCourseOffering = courseOffering;
                    }
                } else {

                    tr = new TableRow();

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Controls.Add(new LiteralControl("None Selected"));

                    tr.Cells.Add(td);

                    tblDegPrereqs.Rows.Add(tr);

                }

                #endregion

                #region GET OPTION COURSES AND ELECTIVES FROM THE DB FOR DISPLAY

                //get option courses and electives
                DataRow[] drCourses = csProgram.GetOptionCoursesAndElectives(Convert.ToInt32(cboOptionTitle.SelectedValue), hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
                Double dblTotalMinCred = 0, dblTotalMaxCred = 0, dblDegMinCred = 0, dblDegMaxCred = 0;
                Int16 intQuarter = 0; //This value was -1

                if (drCourses.Length > 0) {
                    String oldCourseOffering = "";

                    //loop through option courses
                    for (Int32 intCrsCtr = 0; intCrsCtr < drCourses.Length; intCrsCtr++) {
                        String courseOffering = drCourses[intCrsCtr]["CourseOffering"].ToString();

                        if (oldCourseOffering != courseOffering || courseOffering == "ZZZ") {
                            String strQuarter = drCourses[intCrsCtr]["Quarter"].ToString();

                            //determine quarter begins
                            if (strQuarter != "") {

                                if (intQuarter.ToString() != strQuarter) {
                                    intQuarter = Convert.ToInt16(strQuarter);
                                    tr = new TableRow();

                                    td = new TableCell();
                                    td.CssClass = "portletLight";
                                    td.ColumnSpan = 4;
                                    td.Attributes["style"] = "width:313px;";

                                    if (intQuarter == 1) {
                                        td.Controls.Add(new LiteralControl("<b>First Quarter</b>"));
                                    } else if (intQuarter == 2) {
                                        td.Controls.Add(new LiteralControl("<b>Second Quarter</b>"));
                                    } else if (intQuarter == 3) {
                                        td.Controls.Add(new LiteralControl("<b>Third Quarter</b>"));
                                    } else if (intQuarter == 4) {
                                        td.Controls.Add(new LiteralControl("<b>Fourth Quarter</b>"));
                                    } else if (intQuarter == 5) {
                                        td.Controls.Add(new LiteralControl("<b>Fifth Quarter</b>"));
                                    } else if (intQuarter == 6) {
                                        td.Controls.Add(new LiteralControl("<b>Sixth Quarter</b>"));
                                    } else if (intQuarter == 7) {
                                        td.Controls.Add(new LiteralControl("<b>Seventh Quarter</b>"));
                                    } else if (intQuarter == 8) {
                                        td.Controls.Add(new LiteralControl("<b>Eighth Quarter</b>"));
                                    } else if (intQuarter == 9) {
                                        td.Controls.Add(new LiteralControl("<b>Ninth Quarter</b>"));
                                    } else if (intQuarter == 10) {
                                        td.Controls.Add(new LiteralControl("<b>Tenth Quarter</a>"));
                                    }

                                    dblTotalMinCred = 0;
                                    dblTotalMaxCred = 0;

                                    tr.Cells.Add(td);
                                    tblOptCourses.Rows.Add(tr);
                                }

                            }
                            //determine quarter ends

                            //determine credit display

                            //added on 10/12/06 to allow variable credit overwrite
                            Double unitsMinimum = 0, unitsMaximum = 0;
                            bool blnOverwriteUnits = false;

                            if (Convert.ToByte(drCourses[intCrsCtr]["OverwriteUnits"]) == 1) {
                                unitsMinimum = Convert.ToDouble(drCourses[intCrsCtr]["OverwriteUnitsMinimum"]);
                                unitsMaximum = Convert.ToDouble(drCourses[intCrsCtr]["OverwriteUnitsMaximum"]);
                                blnOverwriteUnits = true;
                            } else {
                                unitsMinimum = Convert.ToDouble(drCourses[intCrsCtr]["UNITS_MINIMUM"]);
                                unitsMaximum = Convert.ToDouble(drCourses[intCrsCtr]["UNITS_MAXIMUM"]);
                            }

                            String strCredits = "";

                            if (unitsMinimum == unitsMaximum) {
                                strCredits = unitsMinimum.ToString();
                                dblTotalMinCred += unitsMinimum;
                                dblTotalMaxCred += unitsMinimum;
                            } else {
                                strCredits = unitsMinimum + "-" + unitsMaximum;
                                dblTotalMinCred += unitsMinimum;
                                dblTotalMaxCred += unitsMaximum;
                            }

                            //display courses begins
                            String courseID = drCourses[intCrsCtr]["CourseID"].ToString();
                            //String optionCourseID = drCourses[intCrsCtr]["OptionCourseID"].ToString();
                            String courseLongTitle = drCourses[intCrsCtr]["COURSE_TITLE_LONG"].ToString();
                            String strOptionFootnoteID = drCourses[intCrsCtr]["OptionFootnoteID"].ToString();
                            String courseSubject = drCourses[intCrsCtr]["SUBJECT"].ToString();
                            //String courseBeginSTRM = drCourses[intCrsCtr]["CourseBeginSTRM"].ToString();
                            //String courseEndSTRM = drCourses[intCrsCtr]["CourseEndSTRM"].ToString();
                            String catalogNbr = drCourses[intCrsCtr]["CATALOG_NBR"].ToString();
                            String courseSubjectDisplay = courseSubject;

                            if (hidSearchId.Value == courseOffering) {
                                courseSubjectDisplay = "<span style=\"background-color:yellow\">" + courseSubjectDisplay + "</span>";
                                catalogNbr = "<span style=\"background-color:yellow\">" + catalogNbr + "</span>";
                            }

                            /*
                            bool expiredSTRM = csProgram.ExpiredSTRM(courseEndSTRM);
                            String strStyle = "", note = "";
                            if (expiredSTRM && courseOffering != "ZZZ") {
                                strStyle = "color:red;";
                                note = "&nbsp;&nbsp;[Course Ended " + csTerm.GetTermDescription(courseEndSTRM) + "]";
                            }
                            */

                            tr = new TableRow();

                            td = new TableCell();
                            td.Attributes["style"] = "width:38px;vertical-align:top;";// +strStyle;
                            td.CssClass = "portletLight";

                            td.Controls.Add(new LiteralControl(courseSubjectDisplay));

                            tr.Cells.Add(td);

                           
                            td = new TableCell();
                            td.Attributes["style"] = "width:30px;vertical-align:top;";// +strStyle;
                            td.CssClass = "portletLight";
                            //td.Controls.Add(new LiteralControl(catalogNbr + drCourses[intCrsCtr]["CourseSuffix"].ToString()));
                            td.Controls.Add(new LiteralControl(""));
                            tr.Cells.Add(td);
                            
                            td = new TableCell();
                            td.CssClass = "portletLight";
                            td.Attributes["style"] = "width:315px;vertical-align:top;";// +strStyle;
                            if (courseOffering == "ZZZ") {
                                //td.Controls.Add(new LiteralControl(courseLongTitle));
                            } else {
                                //remove course details until the new course data (lab, lecture, clinical, other, pay type, intent, etc...) can be figured out - 5/8/16
                                //td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/coursedetail.aspx?id=" + courseOffering.Replace("&", "%26") + "&bstrm=" + courseBeginSTRM + "&estrm=" + courseEndSTRM + "&colid=" + hidCollegeID.Value + "&TITLE=" + courseLongTitle.Replace("'", "\'").Replace("#", "%23") + "','CourseDetail', 570, 400);\">" + courseLongTitle + "</a>"));
                                //td.Controls.Add(new LiteralControl(courseLongTitle));
                            }
                            td.Controls.Add(new LiteralControl(courseLongTitle));
                            td.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + drCourses[intCrsCtr]["FootnoteNumber"].ToString() + "</i></span>"));// + note));

                            tr.Cells.Add(td);

                            td = new TableCell();
                            td.CssClass = "smallText";
                            td.Attributes["style"] = "vertical-align:top;width:50px;text-align:right;";
                            if (blnOverwriteUnits) {
                                td.Attributes["style"] += "color:red;";
                            }
                            td.Controls.Add(new LiteralControl(strCredits));

                            tr.Cells.Add(td);

                            td = new TableCell();
                            td.CssClass = "portletLight";
                            td.Attributes["style"] = "width:70px;vertical-align:top;text-align:right;";

                            if (courseOffering == "ZZZ") 
                            {
                                //For Elective Group
                                //td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/editoptcrs.aspx?todo=editCrsElect&egid=" + courseID + "&Quarter=" + intQuarter + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'", "\\'") + "&CrMin=" + unitsMinimum.ToString() + "&CrMax=" + unitsMaximum.ToString() + "&pt=" + hidProgramTitle.Value.Replace("'", "\\'") + "&fid=" + strOptionFootnoteID + "&co=" + courseOffering.Replace("&", "%26") + "&cs=" + courseSubject.Replace("&", "%26") + "&oid=" + hidOptionID.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&", "%26").Replace("'", "\\'") + "', 'EditCrsElect', 460, 325);return false;\">Edit</a> | "));
                                //td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete " + courseLongTitle.Replace("'", "\\'") + " from the course list?')){document.frmProgram.hidTodo.value='deleteElectGroupRef';document.frmProgram.hidOptionElectiveGroupID.value='" + "courseID" + "';document.frmProgram.hidQuarter.value='" + intQuarter + "';document.frmProgram.submit();}return false;\">Delete</a>"));
                                td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/editoptcrs.aspx?todo=editCrsElect&egid=" + courseID + "&Quarter=" + intQuarter + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'", "\\'") + "&CrMin=" + unitsMinimum.ToString() + "&CrMax=" + unitsMaximum.ToString() + "&pt=" + hidProgramTitle.Value.Replace("'", "\\'") + "&fid=" + strOptionFootnoteID + "&co=" + courseOffering.Replace("&", "%26") + "&cs=" + courseSubject.Replace("&", "%26") + "&oid=" + hidOptionID.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&", "%26").Replace("'", "\\'") + "', 'EditCrsElect', 460, 325);return false;\">Edit</a> | "));
                                td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete " + courseLongTitle.Replace("'", "\\'") + " from the course list?')){document.frmProgram.hidTodo.value='deleteElectGroupRef';document.frmProgram.hidOptionElectiveGroupID.value='" + courseID + "';document.frmProgram.hidQuarter.value='" + intQuarter + "';document.frmProgram.submit();}return false;\">Delete</a>"));
                            }
                            else 
                            {
                                //For individual courses
                                //if (!expiredSTRM) {
                                //td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/editoptcrs.aspx?todo=editProgCrs&cid=" + courseID + "&Quarter=" + intQuarter + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'", "\\'") + "&CrMin=" + unitsMinimum.ToString() + "&CrMax=" + unitsMaximum.ToString() + "&pt=" + hidProgramTitle.Value.Replace("'", "\\'") + "&fid=" + strOptionFootnoteID + "&co=" + courseOffering.Replace("&", "%26") + "&cs=" + courseSubject.Replace("&", "%26") + "&oid=" + hidOptionID.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&", "%26").Replace("'", "\\'") + "', 'EditOptCrs', 460, 325);return false;\">Edit</a> | "));
                                //}
                                td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/editoptcrs.aspx?todo=editProgCrs&cid=" + courseID + "&Quarter=" + intQuarter + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'", "\\'") + "&CrMin=" + unitsMinimum.ToString() + "&CrMax=" + unitsMaximum.ToString() + "&pt=" + hidProgramTitle.Value.Replace("'", "\\'") + "&fid=" + strOptionFootnoteID + "&co=" + courseOffering.Replace("&", "%26") + "&cs=" + courseSubject.Replace("&", "%26") + "&oid=" + hidOptionID.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&", "%26").Replace("'", "\\'") + "', 'EditCrsElect', 460, 325);return false;\">Edit</a> | "));
                                td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete " + courseOffering + " from quarter " + intQuarter + " of the course list?')){document.frmProgram.hidTodo.value='deleteOptCrs';document.frmProgram.hidCourseOffering.value='" + drCourses[intCrsCtr]["CourseOffering"].ToString() + "';document.frmProgram.hidCourseID.value='" + courseID + "';document.frmProgram.hidQuarter.value='" + intQuarter + "';document.frmProgram.submit();}return false;\">Delete</a>"));
                            }

                            //td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/editoptcrs.aspx?todo=editProgCrs&egid=" + optionCourseID + "&Quarter=" + intQuarter + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'", "\\'") + "&CrMin=" + unitsMinimum.ToString() + "&CrMax=" + unitsMaximum.ToString() + "&pt=" + hidProgramTitle.Value.Replace("'", "\\'") + "&fid=" + strOptionFootnoteID + "&co=" + courseOffering.Replace("&", "%26") + "&cs=" + courseSubject.Replace("&", "%26") + "&oid=" + hidOptionID.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&", "%26").Replace("'", "\\'") + "', 'EditCrsElect', 460, 325);return false;\">Edit</a> | "));
                            //td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/editoptcrs.aspx?egid=" + "id" + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'", "\\'") + "&pt=" + hidProgramTitle.Value.Replace("'", "\\'") + "&pd=" + hidProgramDisplay.Value + "&fid=" + strOptionFootnoteID + "&co=" + courseOffering.Replace("&", "%26") + "&cs=" + courseSubject.Replace("&", "%26") + "&oid=" + hidOptionID.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&", "%26").Replace("'", "\\'") + "', 'EditProgElect', 440, 420);return false;\">Edit</a> | "));
                            //}
                            //td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"if(confirm('Are you sure you want to remove " + courseOffering + " from the elective group " + strElectGroupTitle.Replace("'", "\\'") + "?')){document.frmProgram.hidTodo.value='deleteElectGroupCrs';document.frmProgram.hidOptionElectiveGroupID.value='" + optionElectiveGroupID + "';document.frmProgram.hidCourseOffering.value='" + courseOffering + "';document.frmProgram.submit();}return false;\">Delete</a>"));

                            tr.Cells.Add(td);
                            tblOptCourses.Rows.Add(tr);
                        }

                        //check if the quarter or group of courses/electives is on the last course/elective
                        if ((intCrsCtr == (drCourses.Length - 1)) || (intCrsCtr < (drCourses.Length - 1) && drCourses[intCrsCtr + 1]["Quarter"].ToString() != drCourses[intCrsCtr]["Quarter"].ToString())) {
                            //display the total min and max credits for the quarter
                            String strTotalCred = "";

                            if (dblTotalMinCred < dblTotalMaxCred) {
                                dblDegMinCred += dblTotalMinCred;
                                dblDegMaxCred += dblTotalMaxCred;
                                strTotalCred = dblTotalMinCred + "-" + dblTotalMaxCred;
                            } else {
                                dblDegMinCred += dblTotalMinCred;
                                dblDegMaxCred += dblTotalMinCred;
                                strTotalCred = dblTotalMaxCred.ToString();
                            }

                            tr = new TableRow();
                            td = new TableCell();
                            td.ColumnSpan = 3;
                            tr.Cells.Add(td);
                            td = new TableCell();
                            td.Attributes["style"] = "width:50px;text-align:right;padding-top:2px;border-top: 1px solid #000000;";
                            td.Controls.Add(new LiteralControl(strTotalCred));
                            tr.Cells.Add(td);
                            td = new TableCell();
                            tr.Cells.Add(td);
                            tblOptCourses.Rows.Add(tr);
                        }

                        if (intCrsCtr == (drCourses.Length - 1)) {
                            //calculate and display total credits for the selected degree
                            String strDegCred = "";

                            if (dblDegMinCred < dblDegMaxCred) {
                                strDegCred = dblDegMinCred + "-" + dblDegMaxCred;
                            } else {
                                strDegCred = dblDegMaxCred.ToString();
                            }

                            strDegCred += " credits are required for the " + lblDegree.Text;//cboDegree.SelectedItem.Text;

                            tr = new TableRow();
                            td = new TableCell();
                            td.ColumnSpan = 5;
                            td.Controls.Add(new LiteralControl(strDegCred));
                            td.Attributes["style"] = "width:100%;";
                            tr.Cells.Add(td);
                            tblOptCourses.Rows.Add(tr);
                        }

                        oldCourseOffering = courseOffering;
                    }
                } else {

                    tr = new TableRow();
                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Controls.Add(new LiteralControl("None Selected"));
                    tr.Cells.Add(td);
                    tblOptCourses.Rows.Add(tr);

                }

                #endregion

                #region GET PROGRAM ELECTIVE GROUPS FROM THE DB FOR DISPLAY

                //get program electives
                DataSet dsElectGroup = csProgram.GetOptionElectiveGroups(optionID);
                Int32 intElectGroupCount = dsElectGroup.Tables[0].Rows.Count;

                /*electives are being added to td and tr after td and tr are added to tblCPGBack
                    The display and table format seems ok, check into this more*/
                if (intElectGroupCount > 0) {
                    for (Int16 intElectGroupCtr = 0; intElectGroupCtr < dsElectGroup.Tables[0].Rows.Count; intElectGroupCtr++) {
                        Int32 optionElectiveGroupID = Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroupCtr]["OptionElectiveGroupID"]);
                        Int32 intElectiveCount = Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroupCtr]["ElectiveCount"]);
                        String strElectGroupTitle = dsElectGroup.Tables[0].Rows[intElectGroupCtr]["ElectiveGroupTitle"].ToString();

                        tr = new TableRow();

                        td = new TableCell();
                        td.ColumnSpan = 4;
                        td.CssClass = "smallText";
                        td.Attributes["style"] = "padding-top:5px;";
                        td.Controls.Add(new LiteralControl("<b><u>" + strElectGroupTitle + "</u></b>&nbsp;<i><font style=\"font-size:7pt;\">" + dsElectGroup.Tables[0].Rows[intElectGroupCtr]["FootnoteNumber"].ToString() + "</font></i>"));

                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "smallText";
                        td.Attributes["style"] = "width:70px;vertical-align:top;text-align:right;padding-top:5px;";
                        td.Controls.Add(new LiteralControl("<b><a href=\"#\" onclick=\"popupCenter('popups/editelectgroup.aspx?egid=" + optionElectiveGroupID + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'", "\\'") + "&pt=" + hidProgramTitle.Value.Replace("'", "\\'") + "&pd=" + hidProgramDisplay.Value + "&oid=" + hidOptionID.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&", "%26").Replace("'", "\\'") + "', 'EditElectGroup', 440, 420);return false;\">Edit</a></b> | "));

                        //Include whether or not elective group references exist in the confirmation before deleting
                        if (intElectiveCount > 0) {
                            td.Controls.Add(new LiteralControl("<b><a href=\"#\" onclick=\"if(confirm('All electives and elective group references for " + strElectGroupTitle.Replace("'", "\\'") + " will be deleted.\\nAre you sure you want to delete " + strElectGroupTitle.Replace("'", "\\'") + " and all of its electives and references?')){document.frmProgram.hidTodo.value='deleteElectGroup';document.frmProgram.hidOptionElectiveGroupID.value='" + optionElectiveGroupID + "';document.frmProgram.submit();}return false;\">Delete</a></b>"));
                        } else {
                            td.Controls.Add(new LiteralControl("<b><a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete " + strElectGroupTitle.Replace("'", "\\'") + "?')){document.frmProgram.hidTodo.value='deleteElectGroup';document.frmProgram.hidOptionElectiveGroupID.value='" + optionElectiveGroupID + "';document.frmProgram.submit();}return false;\">Delete</a></b>"));
                        }

                        tr.Cells.Add(td);
                        tblOptionElectives.Rows.Add(tr);

                        DataSet dsElectiveGroupCourses = csProgram.GetElectiveGroupCourses(optionElectiveGroupID, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);

                        for (Int32 intElectCtr = 0; intElectCtr < dsElectiveGroupCourses.Tables[0].Rows.Count; intElectCtr++) {
                            //display electives begin
                            Double dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["UNITS_MINIMUM"]);
                            Double dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["UNITS_MAXIMUM"]);
                            String courseOffering = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CourseOffering"].ToString();
                            String courseLongTitle = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["COURSE_TITLE_LONG"].ToString();
                            String courseSubject = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["SUBJECT"].ToString();
                            String strOptionFootnoteID = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OptionFootnoteID"].ToString();
                            //Int32 courseID = Convert.ToInt32(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CourseID"]);
                            bool blnOverwriteUnits = false;
                            //String courseBeginSTRM = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CourseBeginSTRM"].ToString();
                            String courseBeginSTRM = "";
                            //String courseEndSTRM = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CourseEndSTRM"].ToString();
                            String catalogNbr = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CATALOG_NBR"].ToString();
                            String courseSubjectDisplay = courseSubject;

                            if (hidSearchId.Value == courseOffering) {
                                courseSubjectDisplay = "<span style=\"background-color:yellow\">" + courseSubjectDisplay + "</span>";
                                catalogNbr = "<span style=\"background-color:yellow\">" + catalogNbr + "</span>";
                            }

                            /*
                            bool expiredSTRM = csProgram.ExpiredSTRM(courseEndSTRM);
                            String strStyle = "", note = "";
                            if (expiredSTRM) {
                                strStyle = "color:red;";
                                note = "&nbsp;&nbsp;[Course Ended " + csTerm.GetTermDescription(courseEndSTRM) + "]";
                            }
                            */

                            if (Convert.ToByte(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnits"]) == 1) {
                                dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnitsMinimum"]);
                                dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnitsMaximum"]);
                                blnOverwriteUnits = true;
                            }

                            String strElectCredits = "";

                            if (dblElectCredMin == dblElectCredMax) {
                                strElectCredits = dblElectCredMin.ToString();
                            } else {
                                strElectCredits = dblElectCredMin + "-" + dblElectCredMax;
                            }

                            tr = new TableRow();

                            td = new TableCell();
                            td.Attributes["style"] = "width:38px;vertical-align:top;";// +strStyle;
                            td.CssClass = "portletLight";

                            td.Controls.Add(new LiteralControl(courseSubjectDisplay));

                            tr.Cells.Add(td);

                            td = new TableCell();
                            td.Attributes["style"] = "width:30px;vertical-align:top;";// +strStyle;
                            td.CssClass = "portletLight";
                            td.Controls.Add(new LiteralControl(catalogNbr));

                            tr.Cells.Add(td);

                            td = new TableCell();
                            td.CssClass = "portletLight";
                            td.Attributes["style"] = "width:315px;vertical-align:top;";// +strStyle;
                            //remove course details until the new course data (lab, lecture, clinical, other, pay type, intent, etc...) can be figured out - 5/8/16
                            //td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/coursedetail.aspx?id=" + courseOffering.Replace("&", "%26") + "&bstrm=" + courseBeginSTRM + "&estrm=" + courseEndSTRM + "&colid=" + hidCollegeID.Value + "&TITLE=" + courseLongTitle.Replace("'", "\'").Replace("#","%23") + "','CourseDetail', 570, 400);\">" + courseLongTitle + "</a>"));
                            td.Controls.Add(new LiteralControl(courseLongTitle));
                            td.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["FootnoteNumber"].ToString() + "</i></span>"));// + note));

                            tr.Cells.Add(td);

                            td = new TableCell();
                            td.CssClass = "portletLight";
                            td.Attributes["style"] = "vertical-align:top;width:50px;text-align:right;";
                            if (blnOverwriteUnits) {
                                td.Attributes["style"] += "color:red;";
                            }
                            td.Controls.Add(new LiteralControl(strElectCredits));

                            tr.Cells.Add(td);

                            td = new TableCell();
                            td.CssClass = "portletLight";
                            td.Attributes["style"] = "width:70px;vertical-align:top;text-align:right;";
                            //if (!expiredSTRM) {
                            td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/editelective.aspx?egid=" + optionElectiveGroupID + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'", "\\'") + "&pt=" + hidProgramTitle.Value.Replace("'", "\\'") + "&pd=" + hidProgramDisplay.Value + "&fid=" + strOptionFootnoteID + "&co=" + courseOffering.Replace("&", "%26") + "&cs=" + courseSubject.Replace("&", "%26") + "&oid=" + hidOptionID.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&", "%26").Replace("'", "\\'") + "', 'EditProgElect', 440, 420);return false;\">Edit</a> | "));
                            //}
                            td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"if(confirm('Are you sure you want to remove " + courseOffering + " from the elective group " + strElectGroupTitle.Replace("'", "\\'") + "?')){document.frmProgram.hidTodo.value='deleteElectGroupCrs';document.frmProgram.hidOptionElectiveGroupID.value='" + optionElectiveGroupID + "';document.frmProgram.hidCourseOffering.value='" + courseOffering + "';document.frmProgram.submit();}return false;\">Delete</a>"));

                            tr.Cells.Add(td);
                            tblOptionElectives.Rows.Add(tr);
                        }
                    }
                } else {

                    tr = new TableRow();

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Controls.Add(new LiteralControl("None Selected"));

                    tr.Cells.Add(td);
                    tblOptionElectives.Rows.Add(tr);
                }

                #endregion

                #region GET PROGRAM FOOTNOTES FROM THE DB FOR DISPLAY

                //get the program footnotes
                DataSet dsFootnotes = csProgram.GetOptFootnotes(optionID);
                Int32 intFootnoteCount = dsFootnotes.Tables[0].Rows.Count;

                if (intFootnoteCount > 0) {
                    for (Int32 intFootnoteCtr = 0; intFootnoteCtr < intFootnoteCount; intFootnoteCtr++) {
                        Int16 intFootnoteNum = Convert.ToInt16(dsFootnotes.Tables[0].Rows[intFootnoteCtr]["FootnoteNumber"]);
                        Int32 optionFootnoteID = Convert.ToInt32(dsFootnotes.Tables[0].Rows[intFootnoteCtr]["OptionFootnoteID"]);
                        String footnote = dsFootnotes.Tables[0].Rows[intFootnoteCtr]["Footnote"].ToString().Replace("&amp;", "&");

                        if (hidSearchText.Value != null && hidSearchText.Value != "" && footnote.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                            footnote = Regex.Replace(footnote, hidSearchText.Value, "<span style=\"background-color:yellow\">" + footnote.Substring(footnote.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", RegexOptions.IgnoreCase);
                        }

                        tr = new TableRow();

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "width:18px;font-size:7pt;vertical-align:top;";
                        if (intFootnoteCtr == 0) {
                            td.Attributes["style"] += "padding-top:5px;";
                        }
                        td.Controls.Add(new LiteralControl("<i>" + intFootnoteNum + "</i>"));

                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        if (intFootnoteCtr == 0) {
                            td.Attributes["style"] = "padding-top:5px;";
                        }
                        td.Controls.Add(new LiteralControl("<i>" + footnote + "</i>"));

                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "width:70px;vertical-align:top;text-align:right;padding-right:8px;";
                        td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('popups/editfootnote.aspx?pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'", "\\'") + "&pt=" + hidProgramTitle.Value.Replace("'", "\\'") + "&pd=" + hidProgramDisplay.Value + "&fid=" + optionFootnoteID + "&fn=" + intFootnoteNum + "&oid=" + hidOptionID.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&", "%26").Replace("'", "\\'") + "', 'EditProgFootnote', 200, 325);return false;\">Edit</a> | "));
                        td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"if(confirm('All references to footnote #" + intFootnoteNum + " will be deleted from the program.\\nAre you sure you want to delete footnote #" + intFootnoteNum + "?\\n\\nNOTE: Footnotes with a reference number greater than " + intFootnoteNum + " will be automatically re-numbered.')){document.frmProgram.hidTodo.value='deleteFootnote';document.frmProgram.hidOptionFootnoteID.value='" + optionFootnoteID + "';document.frmProgram.hidFootnoteNumber.value='" + intFootnoteNum + "';document.frmProgram.submit();}return false;\">Delete</a>"));

                        tr.Cells.Add(td);
                        tblProgFootnotes.Rows.Add(tr);
                    }
                } else {

                    tr = new TableRow();
                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Controls.Add(new LiteralControl("None Entered"));
                    tr.Cells.Add(td);
                    tblProgFootnotes.Rows.Add(tr);

                }

                #endregion

                panProgOutline.Visible = true;
                //cboDegree.Visible = true;
                //lblDegree.Visible = false;
                //lblDegreeRequirementWorksheetLink.Visible = true;
                panTxtDisplay.Visible = false;
                spnSubmitTextDesc.Visible = false;
                spnSubmitOutline.Visible = true;

            } else {

                txtDisplay.Value = csProgram.GetProgramTextOutline(programVersionID);
                //get degree titles to show in label
                //lblDegree.Text = degreeShortTitles; //need to show degree requirements for each degree type
                //lblDegree.Visible = true;
                //lblDegreeRequirementWorksheetLink.Visible = false;
                //cboDegree.Visible = false;
                panTxtDisplay.Visible = true;
                panProgOutline.Visible = false;
                spnSubmitTextDesc.Visible = true;
                spnSubmitOutline.Visible = false;

            }
        }

        private void cmdNext1_Click(object sender, System.EventArgs e) {
            Int32 programVersionID = 0, collegeID = Convert.ToInt32(Request.Form["optOfferedAt1"]), degreeID = Convert.ToInt32(Request.Form["cboDegree1"]);
            String strAreaOfStudyID = Request.Form["cboAreaOfStudy1"];
            String[] aryAreaOfStudyList = hidAreaOfStudyList.Value.Split(',');
            String strCategoryID = Request.Form["cboCategory1"];
            //String[] aryDegreeList = hidDegreeList.Value.Split(',');
            String[] aryCategoryList = hidCategoryList.Value.Split(',');
            String programDescription = Request.Form["txtProgramDescription1"].Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&");
            String programDisplay = Request.Form["optProgramDisplay1"], programTitle = Request.Form["txtProgramTitle1"];
            String programBeginSTRM = Request.Form["cboProgramBeginSTRM1"], programEndSTRM = Request.Form["cboProgramEndSTRM1"];
            String lastModifiedEmployeeID = Request.Cookies["phatt2"]["userctclinkid"], publishedProgram = Request.Form["optPublishedProgram1"];
            String fileName = Request.Form["cboTransDeg1"]; //what was this used for?
            DateTime lastModifiedDate = DateTime.Now;
            hidDegreeID.Value = degreeID.ToString();
            hidProgramDisplay.Value = programDisplay;

            programVersionID = csProgram.AddProgram(programTitle, strAreaOfStudyID, strCategoryID, programDescription, collegeID, degreeID, programBeginSTRM, programEndSTRM, publishedProgram, Convert.ToChar(programDisplay), lastModifiedEmployeeID, lastModifiedDate);

            if (programVersionID > 0) {
                hidProgramID.Value = programVersionID.ToString();

                if (publishedProgram == "1") {
                    csProgram.AddProgCPGData(programVersionID, lastModifiedEmployeeID, lastModifiedDate);
                }
                hidProgramVersionID.Value = programVersionID.ToString();

                if (hidAreaOfStudyList.Value != "") {
                    for (Int16 j = 0; j < aryAreaOfStudyList.Length; j++) {
                        if (strAreaOfStudyID != aryAreaOfStudyList[j]) {
                            //pass non-changing programid to csProgram.AddProgramAreaOfStudy, if the program is new the ProgramID and ProgramVersionID are the same
                            //write a method to get the programID by programVersionID?
                            csProgram.AddProgramAreaOfStudy(programVersionID, Convert.ToInt32(aryAreaOfStudyList[j]));
                        }
                    }
                }

                if (hidCategoryList.Value != "") { //Added 12/9/2010; There was an error if no additional categories were selected.
                    for (Int16 j = 0; j < aryCategoryList.Length; j++) {
                        if (strCategoryID != aryCategoryList[j]) {
                            csProgram.AddProgramCategory(programVersionID, Convert.ToInt32(aryCategoryList[j]));
                        }
                    }
                }

                //get LocationID of selected College
                Int32 locationID = csProgram.GetLocationID(collegeID);

                //insert program degree/certificate
                Int32 programDegreeID = csProgram.AddProgramDegree(programVersionID, degreeID);
                hidProgramDegreeID.Value = programDegreeID.ToString();

                //insert program options
                Int32 optionID = csProgram.AddProgOption(programDegreeID);

                if (locationID > 0) { //if college exists as a location
                    //insert college as the default location the program option is offered at
                    csProgram.AddOptionLocation(optionID, locationID);
                }

                /* old code that supported multiple degrees/certificates
                for(Int16 j = 0; j < aryDegreeList.Length; j++){
                    Int32 degreeID = Convert.ToInt32(aryDegreeList[j]);
							
                    //insert program college degrees
                    Int32 programDegreeID = csProgram.AddProgramDegree(programVersionID, degreeID);

                    //insert program options
                    Int32 optionID = csProgram.AddProgOption(programDegreeID);

                    if(locationID > 0){ //if college exists as a location
                        //insert college as the default location the program option is offered at
                        csProgram.AddOptionLocation(optionID, locationID);
                    }					
                }
                */

                //Check that selected completion award and areas of study are displayed.
                DisplayProgDeg(programVersionID);
                hidTodo.Value = "step2";
                hidProgramTitle.Value = programTitle;
                hidCollegeID.Value = collegeID.ToString();
                hidOfferedAt.Value = optOfferedAt1.Items.FindByValue(Request.Form["optOfferedAt1"]).Text;
                lblDegree.Text = cboDegree1.Items.FindByValue(degreeID.ToString()).Text;
                lblAreaOfStudy.Text = cboAreaOfStudy1.Items.FindByValue(strAreaOfStudyID).Text;
                lblCategory.Text = cboCategory1.Items.FindByValue(strCategoryID).Text;
                hidProgramBeginSTRM.Value = cboProgramBeginSTRM1.SelectedValue;
                hidProgramEndSTRM.Value = cboProgramEndSTRM1.SelectedValue;
                hidPublishedProgram.Value = optPublishedProgram1.SelectedItem.Text;
                hidBeginTerm.Value = cboProgramBeginSTRM1.SelectedItem.Text;
                hidEndTerm.Value = cboProgramEndSTRM1.SelectedItem.Text;
                lblProgramBeginSTRM.Text = hidBeginTerm.Value; //hidProgramBeginSTRM.Value;
                lblProgramEndSTRM.Text = hidEndTerm.Value; //hidProgramEndSTRM.Value;
                lblPublishedProgram.Text = hidPublishedProgram.Value;
                lblProgramTitle.Text = hidProgramTitle.Value;
                lblOfferedAt.Text = hidOfferedAt.Value;
                panStep2.Visible = true;
                panAdd.Visible = false;

            } else {

                if (programVersionID == 0) {
                    lblErrorMsg1.Text = "Error: The program update failed.";
                } else {
                    //get begin and end terms of overlapping program
                    String termSpan = csProgram.GetConflictingTermsForNewProgram(programTitle, collegeID, degreeID, programBeginSTRM, programEndSTRM);

                    //display the error message
                    lblErrorMsg1.Text = "Error: The begin and end terms entered overlap an existing \"" + cboDegree1.Items.FindByValue(degreeID.ToString()).Text + " - " + programTitle + "\" program offered by " + optOfferedAt1.Items.FindByValue(Request.Form["optOfferedAt1"]).Text;

                    if (termSpan != "") {
                        lblErrorMsg1.Text += " effective " + termSpan + ".";
                    } else {
                        lblErrorMsg1.Text += ".";
                    }
                }

                //hide/show panels
                panError1.Visible = true;
                panAdd.Visible = true;
                panStep2.Visible = false;
            }
        }

        private void cmdNext2_Click(object sender, System.EventArgs e) {

            Int32 programVersionID = Convert.ToInt32(Request.Form["hidProgramVersionID"]), programID = Convert.ToInt32(Request.Form["hidProgramID"]), intOfferedAt = Convert.ToInt32(Request.Form["optOfferedAt2"]), degreeID = Convert.ToInt32(Request.Form["cboDegree2"]);
            String programDescription = Request.Form["txtProgramDescription2"].Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&").Replace("&quot;", "\"");
            String programTitle = Request.Form["txtProgramTitle2"], programBeginSTRM = Request.Form["cboProgramBeginSTRM2"], programEndSTRM = Request.Form["cboProgramEndSTRM2"];
            String publishedProgram = Request.Form["optPublishedProgram2"], strCategoryID = Request.Form["cboCategory2"], strAreaOfStudyID = Request.Form["cboAreaOfStudy2"];
            String fileName = Request.Form["cboTransDeg2"]; //what was this used for?
            Char chrProgramDisplay = Convert.ToChar(Request.Form["optProgramDisplay2"]);
            String strAddCategories = Request.Form["hidCategoryList"], strAddAreasOfStudy = Request.Form["hidAreaOfStudyList"];
            hidProgramDisplay.Value = chrProgramDisplay.ToString();
            hidDegreeID.Value = degreeID.ToString();

            //remove text search highlighting before DB submit
            if (hidSearchText.Value != null && hidSearchText.Value != "" && programDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                programDescription = Regex.Replace(programDescription, "<span style=\"background-color:yellow\">" + programDescription.Substring(programDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", programDescription.Substring(programDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
            }

            //will need to change return value to retrieve ProgramVersionID if a new hidden program is created
            Int32 intReturn = csProgram.EditProgDefinition(programVersionID, programID, degreeID, strAreaOfStudyID, strCategoryID, intOfferedAt, programTitle, programBeginSTRM, programEndSTRM, publishedProgram, Request.Form["hidPublishedProgramVersionID"], programDescription, chrProgramDisplay, Request.Cookies["phatt2"]["userctclinkid"], DateTime.Now);

            if (intReturn > 0) {

                programVersionID = intReturn;
                hidProgramVersionID.Value = programVersionID.ToString();

                csProgram.DeleteAdditionalProgramAreasOfStudy(programID);
                if (strAddAreasOfStudy != "") {
                    String[] aryAreaOfStudyList = strAddAreasOfStudy.Split(',');
                    for (Int16 i = 0; i < aryAreaOfStudyList.Length; i++) {
                        if (strAreaOfStudyID != aryAreaOfStudyList[i]) {
                            String areaOfStudyID = aryAreaOfStudyList[i];
                            csProgram.AddProgramAreaOfStudy(programID, Convert.ToInt32(areaOfStudyID));
                        }
                    }
                }

                csProgram.DeleteAdditionalProgramCategories(programVersionID);
                if (strAddCategories != "") {
                    String[] aryCategoryList = strAddCategories.Split(',');
                    for (Int16 i = 0; i < aryCategoryList.Length; i++) {
                        if (strCategoryID != aryCategoryList[i]) {
                            String categoryID = aryCategoryList[i];
                            csProgram.AddProgramCategory(programVersionID, Convert.ToInt32(categoryID));
                        }
                    }
                }

                hidError.Value = "";
                /* old code that supported multiple degrees
				if(hidDegreeList.Value != ""){
					if(hidExistingDegrees.Value != ""){
						String[] aryExistingDegrees = hidExistingDegrees.Value.Split(',');
						for(Int16 i = 0; i < aryExistingDegrees.Length; i++){
							if(hidDegreeList.Value.IndexOf("," + aryExistingDegrees[i], 0) > 0){
								hidDegreeList.Value = hidDegreeList.Value.Replace("," + aryExistingDegrees[i], "");
							}else{
								hidDegreeList.Value = hidDegreeList.Value.Replace(aryExistingDegrees[i], "");
							}
							
							if(hidDegreeList.Value.IndexOf(",") == 0){
								hidDegreeList.Value = hidDegreeList.Value.Substring(1, hidDegreeList.Value.Length-1);
							}
						}
					}

					String[] aryDegreeList = hidDegreeList.Value.Split(',');
					for(Int16 i = 0; i < aryDegreeList.Length; i++){
						
						Int32 degreeID = Convert.ToInt32(aryDegreeList[i]);
						
						//Insert Program College Degrees
						csProgram.AddProgramDegree(programVersionID, degreeID);
						
						//store the ProgramDegreeID
						hidProgramDegreeID.Value = csProgram.GetProgramDegreeID(programVersionID, degreeID).ToString();

						//Insert Program Options
						csProgram.AddProgOption(Convert.ToInt32(hidProgramDegreeID.Value));
					}
				}
                */

                csProgram.UpdateProgramDegree(programVersionID, degreeID);

                DisplayProgDeg(programVersionID);
                hidTodo.Value = "step2";
                hidProgramTitle.Value = programTitle;
                hidOfferedAt.Value = optOfferedAt2.Items.FindByValue(Request.Form["optOfferedAt2"]).Text;
                lblDegree.Text = cboDegree2.Items.FindByValue(degreeID.ToString()).Text;
                lblAreaOfStudy.Text = cboAreaOfStudy2.Items.FindByValue(strAreaOfStudyID).Text;
                lblCategory.Text = cboCategory2.Items.FindByValue(strCategoryID).Text;
                hidProgramBeginSTRM.Value = programBeginSTRM;
                hidProgramEndSTRM.Value = programEndSTRM;
                hidBeginTerm.Value = cboProgramBeginSTRM2.Items.FindByValue(programBeginSTRM).Text;
                hidEndTerm.Value = cboProgramEndSTRM2.Items.FindByValue(programEndSTRM).Text;
                if (publishedProgram == "0") {
                    publishedProgram = "Working Copy";
                } else if (publishedProgram == "1") {
                    publishedProgram = "Published Copy";
                }
                hidPublishedProgram.Value = publishedProgram;
                lblProgramTitle.Text = hidProgramTitle.Value;
                lblOfferedAt.Text = hidOfferedAt.Value;
                lblProgramBeginSTRM.Text = hidBeginTerm.Value;
                lblProgramEndSTRM.Text = hidEndTerm.Value;
                lblPublishedProgram.Text = publishedProgram;
                lblErrorMsg2.Text = "";
                panError2.Visible = false;
                panStep2.Visible = true;
                panEdit.Visible = false;

            } else {

                if (intReturn == -1) {
                    //get begin and end terms of overlapping program
                    String termSpan = csProgram.GetConflictingTermsForExistingProgram(programVersionID, programTitle, intOfferedAt, degreeID, programBeginSTRM, programEndSTRM);

                    //display the error message
                    lblErrorMsg2.Text = "Error: The begin and end terms entered overlap an existing \"" + cboDegree2.Items.FindByValue(degreeID.ToString()).Text + " - " + programTitle + "\" program offered by " + optOfferedAt2.Items.FindByValue(Request.Form["optOfferedAt2"]).Text;

                    if (termSpan != "") {
                        lblErrorMsg2.Text += " effective " + termSpan + ".";
                    } else {
                        lblErrorMsg2.Text += ".";
                    }
                } else {
                    lblErrorMsg2.Text = "Error: The program update failed.";
                }

                //hide/show panels
                panError2.Visible = true;
                panEdit.Visible = true;
                GenerateEditScreen();
                panStep2.Visible = false;
            }
        }

        private void cmdSubmitTextDesc_Click(object sender, System.EventArgs e) {
            Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value);
            csProgram.UpdateProgramTextOutline(programVersionID, Request.Form["txtDisplay"]);
            DisplayProgDeg(programVersionID);

            //get the program values for display
            lblConfirmProgramTitle2.Text = lblProgramTitle.Text;
            lblConfOfferedAt2.Text = lblOfferedAt.Text;
            lblConfirmProgramBeginSTRM2.Text = lblProgramBeginSTRM.Text;
            lblConfirmProgramEndSTRM2.Text = lblProgramEndSTRM.Text;
            lblConfStatus2.Text = lblPublishedProgram.Text;
            lblConfirmProgramDescription2.Text = csProgram.GetProgramDescriptionAsString(programVersionID);
            lblConfDegTitle2.Text = lblDegree.Text;
            lblConfProgText.Text = Request.Form["txtDisplay"];
            lblConfAreaOfStudy2.Text = lblAreaOfStudy.Text;
            lblConfAddAreasOfStudy2.Text = lblAddAreasOfStudy.Text;
            lblConfCategory2.Text = lblCategory.Text;
            lblConfAddCategories2.Text = lblAddCategories.Text;

            panStep2.Visible = false;
            panConfirm2.Visible = true;
        }

        private void cmdPrevious_Click(object sender, System.EventArgs e) {
            GenerateEditScreen();
            //hidDegreeList.Value = ""; //empty degrees to insert so application doesn't attempt to reinsert them if the page is reloaded
            panEdit.Visible = true;
            panStep2.Visible = false;
        }

        private void cmdCancel2_Click(object sender, System.EventArgs e) {
            if (hidSearchText.Value != null && hidSearchText.Value != "") {
                Response.Redirect("coursesearch.aspx?searchtxt=" + hidSearchText.Value.Replace("&", "%26"));
            } else if (hidSearchId.Value != null && hidSearchId.Value != "") {
                Response.Redirect("coursesearch.aspx?searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value);
            } else {
                Response.Redirect("edit.aspx?ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value);
            }
        }

        private void cmdConfirm1_Click(object sender, System.EventArgs e) {
            if (hidSearchText.Value != null && hidSearchText.Value != "") {
                Response.Redirect("coursesearch.aspx?searchtxt=" + hidSearchText.Value.Replace("&", "%26"));
            } else if (hidSearchId.Value != null && hidSearchId.Value != "") {
                Response.Redirect("coursesearch.aspx?searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value);
            } else {
                Response.Redirect("edit.aspx?ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value);
            }
        }

        private void cmdExport1_Click(object sender, System.EventArgs e) {
            String publishedProgram = Request.Form["hidPublishedProgram"];
            if (publishedProgram == "Working Copy") {
                publishedProgram = "0";
            } else if (publishedProgram == "Published Copy") {
                publishedProgram = "1";
            }

            Response.Redirect(csProgram.ExportProgramsToRTF(publishedProgram, Request.Form["hidProgramBeginSTRM"], "", Request.Form["hidProgramVersionID"]));
        }

        private void cmdExport2_Click(object sender, System.EventArgs e) {
            String publishedProgram = Request.Form["hidPublishedProgram"];
            if (publishedProgram == "Working Copy") {
                publishedProgram = "0";
            } else if (publishedProgram == "Published Copy") {
                publishedProgram = "1";
            }

            Response.Redirect(csProgram.ExportProgramsToRTF(publishedProgram, Request.Form["hidProgramBeginSTRM"], "", Request.Form["hidProgramVersionID"]));
        }

        private void cmdSubmitOutline_Click(object sedner, System.EventArgs e) {
            //this function will start the routing/approval process later

            Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value);
            Int32 optionID = Convert.ToInt32(hidOptionID.Value);
            String optionDescription = Request.Form["txtOptionDescription"];

            //remove text search highlighting before DB submit
            if (hidSearchText.Value != null && hidSearchText.Value != "" && optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1) {
                optionDescription = Regex.Replace(optionDescription, "<span style=\"background-color:yellow\">" + optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", optionDescription.Substring(optionDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
            }

            //save the program option description
            Byte bitPrimaryOption = 0;
            if (Request.Form["chkPrimary"] == "on") {
                bitPrimaryOption = 1;
            }
            csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), optionDescription, Request.Form["txtOptionStateApproval"], Request.Form["txtCIP"], Request.Form["txtEPC"], Request.Form["txtAcademicPlan"], bitPrimaryOption, Request.Form["txtGainfulEmploymentID"], Request.Form["txtTotalQuarters"]);

            //store locations
            csProgram.DeleteOptionLocations(optionID);
            if (hidLocationFields.Value != "") {
                String[] strLocations = hidLocationFields.Value.Substring(1).Split('|');
                for (Int32 i = 0; i < strLocations.Length; i++) {
                    if (Request.Form[strLocations[i]] == "on") {
                        csProgram.AddOptionLocation(optionID, Convert.ToInt32(strLocations[i].Replace("chkLocation_" + optionID + "_", "")));
                    }
                }
            }

            //store M Codes
            csProgram.DeleteOptionMCodes(optionID);
            if (hidMCodeFields.Value != "") {
                String[] strMCodeFields = hidMCodeFields.Value.Substring(1).Split('|');
                for (Int32 i = 0; i < strMCodeFields.Length; i++) {
                    String[] strCollegeMCode = strMCodeFields[i].Split(';');
                    if (Request.Form[strCollegeMCode[0]] == "on") {
                        csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0].Replace("chkMCode_" + optionID + "_", "")), Request.Form[strCollegeMCode[1]]);
                    }
                }
            }

            //display the program data confirmation
            lblConfirmProgramTitle1.Text = lblProgramTitle.Text;
            lblConfOfferedAt1.Text = lblOfferedAt.Text;
            lblConfirmProgramBeginSTRM1.Text = lblProgramBeginSTRM.Text;
            lblConfirmProgramEndSTRM1.Text = lblProgramEndSTRM.Text;
            lblConfStatus1.Text = lblPublishedProgram.Text;
            lblConfDegree1.Text = lblDegree.Text;
            lblConfirmProgramDescription1.Text = csProgram.GetProgramDescriptionAsString(programVersionID);
            lblConfAreaOfStudy1.Text = lblAreaOfStudy.Text;
            lblConfAddAreasOfStudy1.Text = lblAddAreasOfStudy.Text;
            lblConfCategory1.Text = lblCategory.Text;
            lblConfAddCategories1.Text = lblAddCategories.Text;

            //get values for program outline display
            lblProgramTitle.Text = hidProgramTitle.Value;
            lblOfferedAt.Text = hidOfferedAt.Value;
            lblProgramBeginSTRM.Text = hidBeginTerm.Value; //hidProgramBeginSTRM.Value;
            lblProgramEndSTRM.Text = hidEndTerm.Value; //hidProgramEndSTRM.Value;
            lblPublishedProgram.Text = hidPublishedProgram.Value;

            DataSet dsProgramDegree = csProgram.GetProgramDegree(programVersionID);

            tblOptCourses.Rows.Clear();
            tblDegPrereqs.Rows.Clear();
            tblOptionElectives.Rows.Clear();
            tblProgFootnotes.Rows.Clear();

            for (Int32 i = 0; i < dsProgramDegree.Tables[0].Rows.Count; i++) {
                Int32 programDegreeID = Convert.ToInt32(dsProgramDegree.Tables[0].Rows[i]["ProgramDegreeID"]);
                String degreeShortTitle = dsProgramDegree.Tables[0].Rows[i]["DegreeShortTitle"].ToString();

                //Add a blank row for space
                TableRow trProgOutline = new TableRow();

                TableCell tdProgOutline = new TableCell();
                tdProgOutline.Attributes["style"] = "width:100%;";
                tdProgOutline.CssClass = "portletLight";
                tdProgOutline.Controls.Add(new LiteralControl("&nbsp;"));

                trProgOutline.Cells.Add(tdProgOutline);
                tblProgOutline.Rows.Add(trProgOutline);

                //Display the degree title
                trProgOutline = new TableRow();

                tdProgOutline = new TableCell();
                tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:4px;padding-top:4px;padding-left:6px;";
                tdProgOutline.CssClass = "portletMain";
                tdProgOutline.Controls.Add(new LiteralControl("<b>" + degreeShortTitle + "</b>"));

                trProgOutline.Cells.Add(tdProgOutline);
                tblProgOutline.Rows.Add(trProgOutline);

                //Show the Degree Requirement/Worksheet if one exists
                String fileName = csProgram.GetDegreeRequirementWorksheetByProgramDegreeID(programDegreeID);
                if (fileName != null && fileName != "") {
                    trProgOutline = new TableRow();

                    tdProgOutline = new TableCell();
                    tdProgOutline.Attributes["style"] = "width:100%;padding-top:10px;padding-left:6px;";
                    tdProgOutline.CssClass = "portletLight";
                    tdProgOutline.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('degworksheet/" + fileName + "','Requirements', 950, 950);\">Requirements/Worksheet</a>"));

                    trProgOutline.Cells.Add(tdProgOutline);
                    tblProgOutline.Rows.Add(trProgOutline);
                }

                //Add a blank row for space
                trProgOutline = new TableRow();

                tdProgOutline = new TableCell();
                tdProgOutline.Attributes["style"] = "width:100%;";
                tdProgOutline.CssClass = "portletLight";
                tdProgOutline.Controls.Add(new LiteralControl("&nbsp;"));

                trProgOutline.Cells.Add(tdProgOutline);
                tblProgOutline.Rows.Add(trProgOutline);

                //Get the program options for the selected degree
                DataSet dsProgOptions = csProgram.GetProgOptions(programDegreeID);

                for (Int32 intDSRow = 0; intDSRow < dsProgOptions.Tables[0].Rows.Count; intDSRow++) {
                    optionID = Convert.ToInt32(dsProgOptions.Tables[0].Rows[intDSRow]["OptionID"]);
                    String optionTitle = dsProgOptions.Tables[0].Rows[intDSRow]["OptionTitle"].ToString();
                    String optionStateApproval = dsProgOptions.Tables[0].Rows[intDSRow]["OptionStateApproval"].ToString();
                    String optionCIP = dsProgOptions.Tables[0].Rows[intDSRow]["CIP"].ToString();
                    String optionEPC = dsProgOptions.Tables[0].Rows[intDSRow]["EPC"].ToString();
                    String academicPlan = dsProgOptions.Tables[0].Rows[intDSRow]["ACAD_PLAN"].ToString();
                    String gainfulEmploymentID = dsProgOptions.Tables[0].Rows[intDSRow]["GainfulEmploymentID"].ToString();
                    String totalQuarters = dsProgOptions.Tables[0].Rows[intDSRow]["TotalQuarters"].ToString();
                    optionDescription = dsProgOptions.Tables[0].Rows[intDSRow]["OptionDescription"].ToString();

                    if (dsProgOptions.Tables[0].Rows[intDSRow]["PrimaryOption"].ToString() == "True") {
                        lblPrimaryStateApproval.Text = optionStateApproval;
                        lblPrimaryCIP.Text = optionCIP;
                        lblPrimaryAcademicPlan.Text = academicPlan;
                        lblPrimaryEPC.Text = optionEPC;
                    }

                    if (optionTitle != "") {
                        //display the program option title
                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "width:100%;padding-left:6px;padding-top:5px;border-bottom:1px solid black;font-weight:bold;";
                        tdProgOutline.CssClass = "portletLight";
                        tdProgOutline.Controls.Add(new LiteralControl(optionTitle));

                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);

                        //add a blank row for space
                        trProgOutline = new TableRow();
                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "padding-top:5px;";
                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);
                    }

                    if ((dsProgOptions.Tables[0].Rows.Count > 0) && (optionStateApproval != "" || optionCIP != "" || optionEPC != "" || academicPlan != "")) {
                        //display option state approval, cip, academic plan, and epc
                        String strDisplay = "";
                        if (optionStateApproval != "") {
                            strDisplay += "<b>State Approval Date:</b> " + optionStateApproval + "&nbsp;&nbsp;";
                        }
                        if (optionCIP != "") {
                            strDisplay += "<b>CIP:</b> " + optionCIP + "&nbsp;&nbsp;";
                        }
                        if (optionEPC != "") {
                            strDisplay += "<b>EPC:</b> " + optionEPC + "&nbsp;&nbsp;";
                        }
                        if (academicPlan != "") {
                            strDisplay += "<b>Academic Plan:</b> " + academicPlan + "&nbsp;&nbsp;";
                        }

                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:6px;";
                        tdProgOutline.CssClass = "portletLight";
                        tdProgOutline.Controls.Add(new LiteralControl(strDisplay));

                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);

                    }

                    if (gainfulEmploymentID != null && gainfulEmploymentID.Trim() != "") {
                        //display the Gainful employment courseid
                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:6px;";
                        tdProgOutline.CssClass = "portletLight";
                        tdProgOutline.Controls.Add(new LiteralControl("<b>Gainful Employment CourseID:</b> " + gainfulEmploymentID));

                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);
                    }

                    if (totalQuarters != null && totalQuarters != "") {
                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:6px;";
                        tdProgOutline.CssClass = "portletLight";
                        tdProgOutline.Controls.Add(new LiteralControl("<b>Total Quarters:</b> " + totalQuarters));

                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);
                    }

                    //DISPLAY Locations Here
                    DataSet dsOptionLocations = csProgram.GetOptionLocations(optionID);

                    trProgOutline = new TableRow();

                    tdProgOutline = new TableCell();
                    tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:6px;";
                    tdProgOutline.CssClass = "portletLight";
                    tdProgOutline.Controls.Add(new LiteralControl("<b>Offered at:</b>&nbsp;"));

                    String strLocations = "";
                    for (Int32 j = 0; j < dsOptionLocations.Tables[0].Rows.Count; j++) {
                        strLocations += ",&nbsp;" + dsOptionLocations.Tables[0].Rows[j]["LocationTitle"].ToString();
                    }

                    if (strLocations != "") {
                        tdProgOutline.Controls.Add(new LiteralControl(strLocations.Substring(1)));
                    }
                    trProgOutline.Cells.Add(tdProgOutline);
                    tblProgOutline.Rows.Add(trProgOutline);


                    //DISPLAY M CODES HERE
                    DataSet dsOptionMCodes = csProgram.GetOptionMCodes(optionID);
                    Int32 intMCodeCount = dsOptionMCodes.Tables[0].Rows.Count;
                    if (intMCodeCount > 0) {
                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:3px;padding-left:6px;";
                        tdProgOutline.CssClass = "portletLight";
                        tdProgOutline.Controls.Add(new LiteralControl("<b>Classes that apply to this program option at " + lblOfferedAt.Text + " can be taken at the following campuses:</b>"));

                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);

                        for (Int32 j = 0; j < intMCodeCount; j++) {
                            trProgOutline = new TableRow();

                            tdProgOutline = new TableCell();
                            tdProgOutline.Attributes["style"] = "width:100%;padding-left:30px;";
                            if (j == (intMCodeCount - 1)) {
                                tdProgOutline.Attributes["style"] += "padding-bottom:10px;";
                            } else {
                                tdProgOutline.Attributes["style"] += "padding-bottom:3px;";
                            }
                            tdProgOutline.CssClass = "portletLight";
                            tdProgOutline.Controls.Add(new LiteralControl("<b>Campus:</b>&nbsp;" + dsOptionMCodes.Tables[0].Rows[j]["CollegeShortTitle"].ToString() + "&nbsp;&nbsp;<b>M-Code:</b>&nbsp;" + dsOptionMCodes.Tables[0].Rows[j]["MCode"].ToString()));

                            trProgOutline.Cells.Add(tdProgOutline);
                            tblProgOutline.Rows.Add(trProgOutline);
                        }
                    }

                    if (optionDescription != "") {
                        //display the program option description
                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:6px;";
                        tdProgOutline.CssClass = "portletLight";
                        tdProgOutline.Controls.Add(new LiteralControl(optionDescription));

                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);
                    }

                    #region GET PREREQUISITES FROM THE DB FOR DISPLAY

                    DataSet dsPrereqs = csProgram.GetOptPrereqs(optionID, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
                    Int32 intPrereqCount = dsPrereqs.Tables[0].Rows.Count;

                    Table tblDegPrereqs2 = new Table();
                    tblDegPrereqs2.Attributes["style"] = "width:100%;padding-bottom:10px;";
                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();

                    if (intPrereqCount > 0) {
                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "width:100%;padding-left:6px;font-weight:bold;";
                        tdProgOutline.CssClass = "portletLight";
                        tdProgOutline.Controls.Add(new LiteralControl("Prerequisites:"));

                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);

                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "width:100%;padding-left:6px;padding-bottom:10px;";
                        tdProgOutline.CssClass = "portletLight";

                        String oldCourseOffering = "";

                        //get the option prerequisites for display
                        for (Int32 k = 0; k < intPrereqCount; k++) {
                            String courseOffering = dsPrereqs.Tables[0].Rows[k]["CourseOffering"].ToString();
                            String courseSubject = dsPrereqs.Tables[0].Rows[k]["SUBJECT"].ToString();
                            String strOptionPrerequisiteID = dsPrereqs.Tables[0].Rows[k]["OptionPrerequisiteID"].ToString();
                            String strOptionFootnoteID = dsPrereqs.Tables[0].Rows[k]["OptionFootnoteID"].ToString();
                            //String courseEndSTRM = dsPrereqs.Tables[0].Rows[k]["CourseEndSTRM"].ToString();

                            if (oldCourseOffering != courseOffering) {

                                /*
                                bool expiredSTRM = csProgram.ExpiredSTRM(courseEndSTRM);
                                String strStyle = "", note = "";
                                if (expiredSTRM) {
                                    strStyle = "color:red;";
                                    note = "&nbsp;&nbsp;[Course Ended " + csTerm.GetTermDescription(courseEndSTRM) + "]";
                                }
                                */

                                tr = new TableRow();

                                td = new TableCell();
                                td.Attributes["style"] = "width:55px;vertical-align:top;";// +strStyle;
                                td.CssClass = "portletLight";
                                td.Controls.Add(new LiteralControl("�&nbsp;&nbsp;" + courseSubject));

                                tr.Cells.Add(td);

                                td = new TableCell();
                                td.Attributes["style"] = "width:25px;vertical-align:top;";// +strStyle;
                                td.CssClass = "portletLight";
                                td.Controls.Add(new LiteralControl(dsPrereqs.Tables[0].Rows[k]["CATALOG_NBR"].ToString()));

                                tr.Cells.Add(td);

                                td = new TableCell();
                                td.CssClass = "portletLight";
                                td.Attributes["style"] = "width:425px;vertical-align:top;";// +strStyle;
                                td.Controls.Add(new LiteralControl(dsPrereqs.Tables[0].Rows[k]["COURSE_TITLE_LONG"].ToString()));
                                td.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + dsPrereqs.Tables[0].Rows[k]["FootnoteNumber"].ToString() + "</i></span>"));// + note));

                                tr.Cells.Add(td);

                                tblDegPrereqs2.Rows.Add(tr);
                            }
                            oldCourseOffering = courseOffering;
                        }

                        tdProgOutline.Controls.Add(tblDegPrereqs2);
                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);
                    }

                    #endregion

                    #region GET OPTION COURSES AND ELECTIVES FROM THE DB FOR DISPLAY

                    //get option courses and electives
                    DataRow[] drCourses = csProgram.GetOptionCoursesAndElectives(optionID, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
                    Double dblTotalMinCred = 0, dblTotalMaxCred = 0, dblDegMinCred = 0, dblDegMaxCred = 0;
                    Int16 intQuarter = 0;

                    if (drCourses.Length > 0) {
                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:4px;";
                        tdProgOutline.CssClass = "portletLight";

                        Table tblOptCourses2 = new Table();
                        tblOptCourses2.Attributes["style"] = "width:100%;";

                        String oldCourseOffering = "";

                        //loop through option courses
                        for (Int32 intCrsCtr = 0; intCrsCtr < drCourses.Length; intCrsCtr++) {
                            String courseOffering = drCourses[intCrsCtr]["CourseOffering"].ToString();

                            if (oldCourseOffering != courseOffering || courseOffering == "ZZZ") {
                                String strQuarter = drCourses[intCrsCtr]["Quarter"].ToString();

                                //determine quarter begins
                                if (strQuarter != "") {

                                    if (intQuarter.ToString() != strQuarter) {
                                        intQuarter = Convert.ToInt16(strQuarter);
                                        tr = new TableRow();

                                        td = new TableCell();
                                        td.CssClass = "portletLight";
                                        td.ColumnSpan = 4;
                                        td.Attributes["style"] = "width:313px;";

                                        if (intQuarter == 1) {
                                            td.Controls.Add(new LiteralControl("<b>First Quarter</b>"));
                                        } else if (intQuarter == 2) {
                                            td.Controls.Add(new LiteralControl("<b>Second Quarter</b>"));
                                        } else if (intQuarter == 3) {
                                            td.Controls.Add(new LiteralControl("<b>Third Quarter</b>"));
                                        } else if (intQuarter == 4) {
                                            td.Controls.Add(new LiteralControl("<b>Fourth Quarter</b>"));
                                        } else if (intQuarter == 5) {
                                            td.Controls.Add(new LiteralControl("<b>Fifth Quarter</b>"));
                                        } else if (intQuarter == 6) {
                                            td.Controls.Add(new LiteralControl("<b>Sixth Quarter</b>"));
                                        } else if (intQuarter == 7) {
                                            td.Controls.Add(new LiteralControl("<b>Seventh Quarter</b>"));
                                        } else if (intQuarter == 8) {
                                            td.Controls.Add(new LiteralControl("<b>Eighth Quarter</b>"));
                                        } else if (intQuarter == 9) {
                                            td.Controls.Add(new LiteralControl("<b>Ninth Quarter</b>"));
                                        } else if (intQuarter == 10) {
                                            td.Controls.Add(new LiteralControl("<b>Tenth Quarter</a>"));
                                        }

                                        dblTotalMinCred = 0;
                                        dblTotalMaxCred = 0;

                                        tr.Cells.Add(td);
                                        tblOptCourses2.Rows.Add(tr);
                                    }

                                }
                                //determine quarter ends

                                //determine credit display

                                //added on 10/12/06 to allow variable credit overwrite
                                Double unitsMinimum = 0, unitsMaximum = 0;
                                bool blnOverwriteUnits = false;

                                if (Convert.ToByte(drCourses[intCrsCtr]["OverwriteUnits"]) == 1) {
                                    unitsMinimum = Convert.ToDouble(drCourses[intCrsCtr]["OverwriteUnitsMinimum"]);
                                    unitsMaximum = Convert.ToDouble(drCourses[intCrsCtr]["OverwriteUnitsMaximum"]);
                                    blnOverwriteUnits = true;
                                } else {
                                    unitsMinimum = Convert.ToDouble(drCourses[intCrsCtr]["UNITS_MINIMUM"]);
                                    unitsMaximum = Convert.ToDouble(drCourses[intCrsCtr]["UNITS_MAXIMUM"]);
                                }

                                String strCredits = "";

                                if (unitsMinimum == unitsMaximum) {
                                    strCredits = unitsMinimum.ToString();
                                    dblTotalMinCred += unitsMinimum;
                                    dblTotalMaxCred += unitsMinimum;
                                } else {
                                    strCredits = unitsMinimum + "-" + unitsMaximum;
                                    dblTotalMinCred += unitsMinimum;
                                    dblTotalMaxCred += unitsMaximum;
                                }

                                //display courses begins
                                String courseLongTitle = drCourses[intCrsCtr]["COURSE_TITLE_LONG"].ToString();
                                //String courseID = drCourses[intCrsCtr]["CourseID"].ToString();
                                String strOptionFootnoteID = drCourses[intCrsCtr]["OptionFootnoteID"].ToString();
                                String courseSubject = drCourses[intCrsCtr]["SUBJECT"].ToString();
                                //String courseEndSTRM = drCourses[intCrsCtr]["CourseEndSTRM"].ToString();

                                /*
                                bool expiredSTRM = csProgram.ExpiredSTRM(courseEndSTRM);
                                String strStyle = "", note = "";
                                if (expiredSTRM && courseOffering != "ZZZ") {
                                    strStyle = "color:red;";
                                    note = "&nbsp;&nbsp;[Course Ended " + csTerm.GetTermDescription(courseEndSTRM) + "]";
                                }
                                */

                                tr = new TableRow();

                                td = new TableCell();
                                td.Attributes["style"] = "width:38px;vertical-align:top;";// +strStyle;
                                td.CssClass = "portletLight";

                                td.Controls.Add(new LiteralControl(drCourses[intCrsCtr]["SUBJECT"].ToString()));

                                tr.Cells.Add(td);

                                td = new TableCell();
                                td.Attributes["style"] = "width:25px;vertical-align:top;";// +strStyle;
                                td.CssClass = "portletLight";
                                td.Controls.Add(new LiteralControl(drCourses[intCrsCtr]["CATALOG_NBR"].ToString()));

                                tr.Cells.Add(td);

                                td = new TableCell();
                                td.CssClass = "portletLight";
                                td.Attributes["style"] = "width:390px;vertical-align:top;";// +strStyle;
                                td.Controls.Add(new LiteralControl(courseLongTitle));
                                td.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + drCourses[intCrsCtr]["FootnoteNumber"].ToString() + "</i></span>"));// + note));

                                tr.Cells.Add(td);

                                td = new TableCell();
                                td.CssClass = "smallText";
                                td.Attributes["style"] = "vertical-align:top;width:50px;text-align:right;";
                                if (blnOverwriteUnits) {
                                    td.Attributes["style"] += "color:red;";
                                }
                                td.Controls.Add(new LiteralControl(strCredits));

                                tr.Cells.Add(td);
                                tblOptCourses2.Rows.Add(tr);
                            }

                            //check if the quarter or group of courses/electives is on the last course/elective
                            if ((intCrsCtr == (drCourses.Length - 1)) || (intCrsCtr < (drCourses.Length - 1) && drCourses[intCrsCtr + 1]["Quarter"].ToString() != drCourses[intCrsCtr]["Quarter"].ToString())) {
                                //display the total min and max credits for the quarter
                                String strTotalCred = "";

                                if (dblTotalMinCred < dblTotalMaxCred) {
                                    dblDegMinCred += dblTotalMinCred;
                                    dblDegMaxCred += dblTotalMaxCred;
                                    strTotalCred = dblTotalMinCred + "-" + dblTotalMaxCred;
                                } else {
                                    dblDegMinCred += dblTotalMinCred;
                                    dblDegMaxCred += dblTotalMinCred;
                                    strTotalCred = dblTotalMaxCred.ToString();
                                }

                                tr = new TableRow();
                                td = new TableCell();
                                td.ColumnSpan = 3;
                                tr.Cells.Add(td);
                                td = new TableCell();
                                td.Attributes["style"] = "width:50px;text-align:right;padding-top:2px;border-top: 1px solid #000000;";
                                td.Controls.Add(new LiteralControl(strTotalCred));
                                tr.Cells.Add(td);
                                td = new TableCell();
                                tr.Cells.Add(td);
                                tblOptCourses2.Rows.Add(tr);
                            }

                            if (intCrsCtr == (drCourses.Length - 1)) {
                                //calculate and display total credits for the selected degree
                                String strDegCred = "";

                                if (dblDegMinCred < dblDegMaxCred) {
                                    strDegCred = dblDegMinCred + "-" + dblDegMaxCred;
                                } else {
                                    strDegCred = dblDegMaxCred.ToString();
                                }

                                strDegCred += " credits are required for the " + degreeShortTitle;

                                tr = new TableRow();
                                td = new TableCell();
                                td.ColumnSpan = 5;
                                td.Controls.Add(new LiteralControl(strDegCred));
                                td.Attributes["style"] = "width:100%;";
                                tr.Cells.Add(td);
                                tblOptCourses2.Rows.Add(tr);
                            }

                            oldCourseOffering = courseOffering;
                        }
                        tdProgOutline.Controls.Add(tblOptCourses2);
                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);
                    }

                    #endregion

                    #region GET PROGRAM ELECTIVE GROUPS FROM THE DB FOR DISPLAY

                    //get program electives
                    DataSet dsElectGroup = csProgram.GetOptionElectiveGroups(optionID);
                    Int32 intElectGroupCount = dsElectGroup.Tables[0].Rows.Count;

                    if (intElectGroupCount > 0) {

                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        //tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:4px;padding-top:4px;padding-left:6px;";
                        //tdProgOutline.CssClass = "portletMain";
                        //tdProgOutline.Controls.Add(new LiteralControl("<b>Elective Groups</b>"));

                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);

                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:4px;padding-right:6px;";
                        tdProgOutline.CssClass = "portletLight";

                        Table tblOptionElectives2 = new Table();
                        tblOptionElectives2.Attributes["style"] = "width:100%;";

                        for (Int16 intElectGroupCtr = 0; intElectGroupCtr < dsElectGroup.Tables[0].Rows.Count; intElectGroupCtr++) {
                            Int32 optionElectiveGroupID = Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroupCtr]["OptionElectiveGroupID"]);
                            Int32 intElectiveCount = Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroupCtr]["ElectiveCount"]);
                            String strElectGroupTitle = dsElectGroup.Tables[0].Rows[intElectGroupCtr]["ElectiveGroupTitle"].ToString();

                            if (intElectiveCount > 0) {
                                tr = new TableRow();

                                td = new TableCell();
                                td.ColumnSpan = 4; //check into this
                                td.CssClass = "smallText";
                                td.Attributes["style"] = "padding-top:5px;width:313px;";
                                td.Controls.Add(new LiteralControl("<b><u>" + strElectGroupTitle + "</u></b>&nbsp;<i><font style=\"font-size:7pt;\">" + dsElectGroup.Tables[0].Rows[intElectGroupCtr]["FootnoteNumber"].ToString() + "</font></i>"));

                                tr.Cells.Add(td);
                                tblOptionElectives2.Rows.Add(tr);

                                DataSet dsElectiveGroupCourses = csProgram.GetElectiveGroupCourses(optionElectiveGroupID, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);

                                for (Int32 intElectCtr = 0; intElectCtr < dsElectiveGroupCourses.Tables[0].Rows.Count; intElectCtr++) {
                                    //display electives begin
                                    Double dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["UNITS_MINIMUM"]);
                                    Double dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["UNITS_MAXIMUM"]);
                                    String courseOffering = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CourseOffering"].ToString();
                                    String courseSubject = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["SUBJECT"].ToString();
                                    String strOptionFootnoteID = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OptionFootnoteID"].ToString();
                                    bool blnOverwriteUnits = false;
                                    //String courseEndSTRM = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CourseEndSTRM"].ToString();

                                    /*
                                    bool expiredSTRM = csProgram.ExpiredSTRM(courseEndSTRM);
                                    String strStyle = "", note = "";
                                    if (expiredSTRM) {
                                        strStyle = "color:red;";
                                        note = "&nbsp;&nbsp;[Course Ended " + csTerm.GetTermDescription(courseEndSTRM) + "]";
                                    }
                                    */

                                    if (Convert.ToByte(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnits"]) == 1) {
                                        dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnitsMinimum"]);
                                        dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnitsMaximum"]);
                                        blnOverwriteUnits = true;
                                    }

                                    String strElectCredits = "";

                                    if (dblElectCredMin == dblElectCredMax) {
                                        strElectCredits = dblElectCredMin.ToString();
                                    } else {
                                        strElectCredits = dblElectCredMin + "-" + dblElectCredMax;
                                    }

                                    tr = new TableRow();

                                    td = new TableCell();
                                    td.Attributes["style"] = "width:38px;vertical-align:top;";// +strStyle;
                                    td.CssClass = "portletLight";

                                    td.Controls.Add(new LiteralControl(courseSubject));

                                    tr.Cells.Add(td);

                                    td = new TableCell();
                                    td.Attributes["style"] = "width:25px;vertical-align:top;";// +strStyle;
                                    td.CssClass = "portletLight";
                                    td.Controls.Add(new LiteralControl(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CATALOG_NBR"].ToString()));

                                    tr.Cells.Add(td);

                                    td = new TableCell();
                                    td.CssClass = "portletLight";
                                    td.Attributes["style"] = "width:390px;vertical-align:top;";// +strStyle;
                                    td.Controls.Add(new LiteralControl(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["COURSE_TITLE_LONG"].ToString()));
                                    td.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["FootnoteNumber"].ToString() + "</i></span>"));// + note));

                                    tr.Cells.Add(td);

                                    td = new TableCell();
                                    td.CssClass = "portletLight";
                                    td.Attributes["style"] = "vertical-align:top;width:50px;text-align:right;";
                                    if (blnOverwriteUnits) {
                                        td.Attributes["style"] += "color:red;";
                                    }
                                    td.Controls.Add(new LiteralControl(strElectCredits));

                                    tr.Cells.Add(td);

                                    tblOptionElectives2.Rows.Add(tr);
                                }
                            }
                        }
                        tdProgOutline.Controls.Add(tblOptionElectives2);
                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);
                    }
                    #endregion

                    #region GET PROGRAM FOOTNOTES FROM THE DB FOR DISPLAY

                    //get the program footnotes
                    DataSet dsFootnotes = csProgram.GetOptFootnotes(optionID);
                    Int32 intFootnoteCount = dsFootnotes.Tables[0].Rows.Count;

                    if (intFootnoteCount > 0) {

                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        //tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:4px;padding-top:4px;padding-left:6px;";
                        //tdProgOutline.CssClass = "portletMain";
                        //tdProgOutline.Controls.Add(new LiteralControl("<b>Footnotes</b>"));

                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);

                        trProgOutline = new TableRow();

                        tdProgOutline = new TableCell();
                        tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:4px;";
                        tdProgOutline.CssClass = "portletLight";

                        Table tblProgFootnotes2 = new Table();
                        tblProgFootnotes2.Attributes["style"] = "width:100%;";

                        for (Int32 intFootnoteCtr = 0; intFootnoteCtr < intFootnoteCount; intFootnoteCtr++) {
                            Int16 intFootnoteNum = Convert.ToInt16(dsFootnotes.Tables[0].Rows[intFootnoteCtr]["FootnoteNumber"]);
                            Int32 optionFootnoteID = Convert.ToInt32(dsFootnotes.Tables[0].Rows[intFootnoteCtr]["OptionFootnoteID"]);

                            tr = new TableRow();

                            td = new TableCell();
                            td.CssClass = "portletLight";
                            td.Attributes["style"] = "width:18px;font-size:7pt;vertical-align:top;";
                            if (intFootnoteCtr == 0) {
                                td.Attributes["style"] += "padding-top:5px;";
                            }
                            td.Controls.Add(new LiteralControl("<i>" + intFootnoteNum + "</i>"));

                            tr.Cells.Add(td);

                            td = new TableCell();
                            td.CssClass = "portletLight";
                            if (intFootnoteCtr == 0) {
                                td.Attributes["style"] = "padding-top:5px;";
                            }
                            td.Controls.Add(new LiteralControl("<i>" + dsFootnotes.Tables[0].Rows[intFootnoteCtr]["Footnote"].ToString() + "</i>"));

                            tr.Cells.Add(td);

                            tblProgFootnotes2.Rows.Add(tr);
                        }

                        tdProgOutline.Controls.Add(tblProgFootnotes2);
                        trProgOutline.Cells.Add(tdProgOutline);
                        tblProgOutline.Rows.Add(trProgOutline);
                    }

                    #endregion

                }
            }

            panStep2.Visible = false;
            panConfirm1.Visible = true;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.cmdNext1.Click += new EventHandler(cmdNext1_Click);
            this.cmdNext2.Click += new EventHandler(this.cmdNext2_Click);
            this.cmdPrevious.Click += new EventHandler(this.cmdPrevious_Click);
            this.cmdCancel2.Click += new EventHandler(this.cmdCancel2_Click);
            this.cmdCancel1.Click += new EventHandler(this.cmdCancel2_Click);
            this.cmdSubmitTextDesc.Click += new EventHandler(this.cmdSubmitTextDesc_Click);
            this.cmdSubmitOutline.Click += new EventHandler(this.cmdSubmitOutline_Click);
            this.cmdExport1.Click += new EventHandler(this.cmdExport1_Click);
            this.cmdExport2.Click += new EventHandler(this.cmdExport2_Click);
            this.cmdConfirm1.Click += new EventHandler(this.cmdConfirm1_Click);
        }
        #endregion
    }
}
