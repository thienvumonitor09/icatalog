<%@ Page language="c#" Inherits="ICatalog.cred.excelexport" CodeFile="excelexport.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
    <head>
        <title>Export to Excel</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmCred" runat="server">
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Export to Excel</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                        <!-- ASSIGNED TO -->
                                                                        <tr>
                                                                            <td class="portletSecondary" style="width:42%;text-align:right;">
                                                                                <b>Work Unit:&nbsp;</b>
                                                                            </td>
                                                                            <td class="portletLight" style="width:58%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                <asp:dropdownlist runat="server" id="cboCredentialWorkUnit" cssclass="small">
                                                                                    <asp:listitem value="">ALL</asp:listitem>
                                                                                    <asp:listitem value="DIST">DIST</asp:listitem>
                                                                                    <asp:listitem value="IEL">IEL</asp:listitem>
                                                                                    <asp:listitem value="SCC">SCC</asp:listitem>
                                                                                    <asp:listitem value="SFCC">SFCC</asp:listitem>
                                                                                </asp:dropdownlist>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- TENURE TRACK -->
                                                                        <tr>
                                                                            <td class="portletSecondary" style="text-align:right;">
                                                                                <b>Tenure Status:&nbsp;</b>
                                                                            </td>
                                                                            <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                <asp:dropdownlist runat="server" id="cboCredentialTenureTrack" cssclass="small">
                                                                                    <asp:listitem value="ALL">ALL</asp:listitem>
                                                                                    <asp:listitem value="Tenure Track">Tenure Track</asp:listitem>
                                                                                    <asp:listitem value="Non-Tenure Track">Non-Tenure Track</asp:listitem>
                                                                                    <asp:listitem Value="Executive">Executive</asp:listitem>
                                                                                    <asp:listitem value="Administrator">Administrator</asp:listitem>
                                                                                    <asp:listitem value="Non-Applicable">Non-Applicable</asp:listitem>
                                                                                </asp:dropdownlist>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- BUTTON -->
                                                                        <tr>
														                    <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                    <asp:button id="cmdSubmit" runat="server" text="Export to Excel" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="cmdSubmit_Click"></asp:button>
														                    </td>
													                    </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <div class="clearer"></div>
        </asp:panel>
    </body>
</html>

