<%@ Page language="c#" Inherits="ICatalog.course.add" ValidateRequest="false" CodeFile="add.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Add Course</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <script type="text/javascript" src="../_phatt3_src_files/expand.js"></script>
        <script type="text/javascript" src="../_phatt3_src_files/trim.js"></script>
        <script type="text/javascript" src="../_phatt3_src_files/courseinfo.js"></script>
        <script type="text/javascript" src="https://internal.spokane.edu/ckeditor_4/ckeditor.js"></script>
        <script type="text/javascript">
        <!--
            function addCollege(ID, collegeID) {
                //add/remove college to/from a comma delimited string
                var CollegeList = document.getElementById("hidCollegeList");
                if (document.getElementById(ID).checked) {
                    if (CollegeList.value.length == 0) {
                        CollegeList.value += collegeID;
                    } else {
                        CollegeList.value += "," + collegeID;
                    }
                } else {
                    if (CollegeList.value.indexOf("," + collegeID, 0) > 0) {
                        CollegeList.value = CollegeList.value.replace("," + collegeID, "");
                    } else {
                        CollegeList.value = CollegeList.value.replace(collegeID, "");
                    }
                }

                if (CollegeList.value.indexOf(",") == 0) {
                    CollegeList.value = CollegeList.value.substr(1, CollegeList.value.length);
                }
            }
        //-->
        </script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmCourse" runat="server">
                    <input type="hidden" id="hidTodo" runat="server" />
                    <input type="hidden" id="hidGenerateFromScript" runat="server" />
                    <input type="hidden" id="hidCollegeList" runat="server" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Add Course</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <asp:panel runat="server" id="panAdd">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <!-- Error Message -->
                                                                            <asp:panel runat="server" id="panError">
                                                                                <tr>
                                                                                    <td colspan="4" class="portletMain" style="color:red;padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                        <asp:label runat="server" id="lblErrorMsg"></asp:label>
                                                                                    </td>
                                                                                </tr>
                                                                            </asp:panel>
                                                                            <!-- COURSE OFFERING, TERM -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:15%;vertical-align:top;text-align:right;padding-top:9px;padding-bottom:6px;white-space:nowrap;">
                                                                                    <b>Course Offering:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:18%;vertical-align:top;padding-left:6px;padding-top:9px;padding-bottom:6px;">
                                                                                    <div runat="server" id="divCourseOffering">auto generated</div>
                                                                                </td>
                                                                                <td class="portletSecondary" style="width:23%;vertical-align:top;text-align:right;padding-top:8px;padding-bottom:6px;white-space:nowrap;">
                                                                                    <b>Term:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:44%;vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td><b>Begin</b><br /><asp:dropdownlist runat="server" id="cboCourseBeginSTRM" cssclass="small"></asp:dropdownlist></td> 
																					        <td>&nbsp;<b>End</b><br />&nbsp;<asp:dropdownlist runat="server" id="cboCourseEndSTRM" cssclass="small"></asp:dropdownlist></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Subject, Subject Description -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;padding-bottom:6px;white-space:nowrap;">
                                                                                    <b>Subject Area:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:7px;padding-bottom:7px;">
                                                                                    <select runat="server" id="cboCourseSubject" class="small" onchange="generateCourseID();getSubjectDescription();selectDescriptionSubject();"></select>
                                                                                </td>
                                                                                <td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;padding-bottom:6px;white-space:nowrap;">
                                                                                    <b>Subject Area Description:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:9px;padding-bottom:6px;">
                                                                                    <div id="divSubjectDescription" runat="server">auto generated</div>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- COURSE NUM, COURSE SHORT TITLE -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;padding-top:6px;padding-bottom:6px;white-space:nowrap;">
                                                                                    <b>Catalog Nbr:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <input type="text" runat="server" id="txtCatalogNbr" maxlength="3" style="width:30px;" class="small" onblur="generateCourseID();" />
                                                                                </td>
                                                                                <td class="portletSecondary" style="text-align:right;padding-top:6px;padding-bottom:6px;white-space:nowrap;">
                                                                                    <b>Description:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:textbox runat="server" id="txtCourseShortTitle" maxlength="30" style="width:260px;" cssclass="small"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- SUFFIX, COURSE LONG TITLE -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="vertical-align:top;text-align:right;padding-top:9px;padding-bottom:6px;white-space:nowrap;">
                                                                                    <b>Suffix:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:7px;padding-bottom:6px;">
                                                                                    <select runat="server" id="cboCourseSuffix" class="small" onchange="generateCourseID();">
                                                                                        <option value=""></option>
                                                                                        <option value="H">H</option>
                                                                                        <option value="J">J</option>
                                                                                        <option value="L">L</option>
                                                                                        <option value="R">R</option>
                                                                                        <option value="T">T</option>
                                                                                        <option value="Y">Y</option>
                                                                                    </select>
                                                                                </td>
                                                                                <td class="portletSecondary" style="vertical-align:top;text-align:right;padding-top:9px;padding-bottom:6px;white-space:nowrap;">
                                                                                    <b>Long Course Title:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="padding-left:6px;padding-right:5px;padding-top:6px;padding-bottom:6px;">
                                                                                    <textarea runat="server" id="txtCourseLongTitle" style="width:260px;height:13px;" rows="1" class="small" ondblclick="expandTextArea(this.id,13);" title="Double click to expand this text area when the text overflows. Double click again to return to the original size." name="txtCourseLongTitle"></textarea>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- CREDIT VALUE, OFFERED AT -->
                                                                            <tr>
                                                                                <td rowspan="2" class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;padding-bottom:6px;white-space:nowrap;">
                                                                                    <b>Units:&nbsp;</b>
                                                                                </td>
                                                                                <td rowspan="2" class="portletLight" style="vertical-align:top;">
                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td colspan="2" style="padding-top:2px;" class="portletLight">
                                                                                                <input type="radio" runat="server" name="optVariableUnits" id="optVariableUnitsN" onfocus="disable(0)" checked />Fixed
                                                                                                <input type="radio" runat="server" name="optVariableUnits" id="optVariableUnitsY" onfocus="disable(1)" />Variable
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
																							<td class="portletLight" style="vertical-align:top;padding-left:9px;padding-top:7px;padding-bottom:6px;">
                                                                                                Min&nbsp;<input type="text" runat="server" id="txtUnitsMinimum" maxlength="4" style="width:25px;" onblur="generateFixed();" class="small" />
                                                                                            </td>
                                                                                            <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:7px;padding-bottom:6px;">
                                                                                                Max&nbsp;<input type="text" runat="server" id="txtUnitsMaximum" maxlength="4" style="width:25px;" class="small" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;padding-bottom:6px;white-space:nowrap;">
                                                                                    <b>Offered at:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:0px;padding-right:6px;padding-bottom:6px;">
                                                                                    <asp:Panel ID="panCollege" runat="server"></asp:Panel>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- STATUS -->
                                                                            <tr>
																				<td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;padding-bottom:6px;white-space:nowrap;">
                                                                                    <b>Status:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:0px;padding-right:6px;padding-bottom:6px;padding-top:1px;">
                                                                                    <asp:radiobuttonlist runat="server" id="optPublishedCourse" repeatdirection="horizontal">
                                                                                        <asp:listitem value="0" Selected="True">Working Copy</asp:listitem>
                                                                                        <asp:listitem value="1">Published Copy</asp:listitem>
                                                                                    </asp:radiobuttonlist>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" class="portletSecondary" style="vertical-align:bottom;padding:6px;padding-bottom:2px;">
                                                                                    <table cellpadding="0" cellspacing="0" style="width:100%;">
                                                                                        <tr>
                                                                                            <td class="portletSecondary">&nbsp;<b>Long Description:</b></td>
                                                                                            <td class="portletSecondary" style="text-align:right;padding-right:5px;" runat="server"><b>Same Long Description As:</b>
                                                                                                <select runat="server" id="cboDescriptionSubject" class="small" onchange="getDescriptionCourses()"></select>
                                                                                            </td>
                                                                                            <td class="portletSecondary" style="text-align:right;padding-right:5px;width:150px;white-space:nowrap" runat="server"><b>Course</b>
                                                                                                <select runat="server" id="cboDescriptionCourse" class="small" onchange="getDescriptionByCourseID()">
                                                                                                    <option value="">Select a Subject Area</option>
                                                                                                </select>
                                                                                            </td>
                                                                                            
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" class="portletLight" style="padding:0px;">
                                                                                    <textarea id="txtCourseLongDescription" runat="server" class="small" style="width:99%;" rows="5" cols="10" ondblclick="expandTextArea(this.id,63);" title="Double click to expand this text area when the text overflows. Double click again to return to the original size." name="txtCourseLongDescription"></textarea>
                                                                                    <script type="text/javascript">
                                                                                        CKEDITOR.replace('txtCourseLongDescription',
																						{
																						    customConfig: '/iCatalog/ckeditor_config.js',
																						    toolbar: 'Custom',
																						    removePlugins: 'toolbar,elementspath,resize',
																						    height: "150px",
																						    width: "715px",
																						    allowedContent: 'span{!background-color}'
																						});
																					</script>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
                                                                                <td class="portletMain" colspan="4" style="text-align:center;padding:3px;">
																					<input type="button" id="cmdCancel" value="Cancel" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="location.href='edit.aspx?subject=' + document.getElementById('cboCourseSubject').value.replace('&','%26')" />
                                                                                    &nbsp;<asp:button runat="server" id="cmdSubmit" text="Save Addition" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclientclick="return validate();" onclick="cmdSubmit_Click"></asp:button>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                    <asp:panel runat="server" id="panConfirm">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <tr>
                                                                                <td class="portletMain" colspan="4" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                    The following course has been successfully added.
                                                                                </td>
                                                                            </tr>
                                                                            <!-- COURSE OFFERING, TERM -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:15%;vertical-align:top;text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Course Offering:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:18%;vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCourseOffering"></asp:label>
                                                                                </td>
                                                                                <td class="portletSecondary" style="width:23%;text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Term:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:44%;vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Begin</b>&nbsp;&nbsp;<asp:label runat="server" id="lblCourseBeginSTRM"></asp:label>
                                                                                    &nbsp;&nbsp;&nbsp;<b>End</b>&nbsp;&nbsp;<asp:label runat="server" id="lblCourseEndSTRM"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Subject, Subject Description -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Subject Area:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCourseSubject"></asp:label>
                                                                                </td>
                                                                                <td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Subject Area Description:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblSubjectDescription"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Catalog Number, Course Description -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Catalog Nbr:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCatalogNbr"></asp:label>
                                                                                </td>
                                                                                <td class="portletSecondary" style="text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Description:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCourseShortTitle"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Suffix, Long Course Title -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="vertical-align:top;text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Suffix:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCourseSuffix"></asp:label>
                                                                                </td>
                                                                                <td class="portletSecondary" style="vertical-align:top;text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Long Course Title:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="padding-left:6px;padding-right:5px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCourseLongTitle"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Units, Offered At -->
                                                                            <tr>
                                                                                <td rowspan="2" class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Units:&nbsp;</b>
                                                                                </td>
                                                                                <td rowspan="2" class="portletLight" style="vertical-align:top;padding:6px;">
                                                                                    <asp:label runat="server" id="lblVariableUnits"></asp:label><br />
                                                                                    Min:&nbsp;<asp:label runat="server" id="lblUnitsMinimum"></asp:label> 
                                                                                    &nbsp;Max:&nbsp;<asp:label runat="server" id="lblUnitsMaximum"></asp:label>
                                                                                </td>
                                                                                <td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Offered at:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding:6px;">
                                                                                    <asp:label runat="server" id="lblOfferedAt"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- STATUS -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Status:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding:6px;">
                                                                                    <asp:label runat="server" id="lblPublishedCourse"></asp:label>  
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" class="portletSecondary" style="vertical-align:bottom;padding:6px;padding-bottom:2px;">
                                                                                    <table cellpadding="0" cellspacing="0" style="width:100%;">
                                                                                        <tr>
                                                                                            <td style="width:230px;">&nbsp;<b>Long Description:</b></td>
                                                                                            <asp:panel runat="server" id="panSameAs2">
                                                                                                <td style="text-align:right;"><b>Same As:&nbsp;</b>
                                                                                                    <asp:label runat="server" id="lblDescriptionCourseOffering"></asp:label>   
                                                                                                </td>
                                                                                            </asp:panel>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" class="portletLight" style="padding:8px;">
                                                                                    <asp:label runat="server" id="lblCourseLongDescription"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
                                                                                <td class="portletMain" colspan="4" style="text-align:center;padding:3px;">
                                                                                    <input type="button" id="cmdOK" value="OK" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="location.href='edit.aspx?subject=' + document.getElementById('lblCourseSubject').innerHTML.replace('&','%26')" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                    <!--
						if(document.getElementById("hidGenerateFromScript").value == "true"){
							generateCourseID();getSubjectDescription();selectDescriptionSubject();
						}
                    //-->
                    </script>
                </form>
            </asp:panel>
            <div class="clearer"></div>
        </asp:panel>
    </body>
</html>
