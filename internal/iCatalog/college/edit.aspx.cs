using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.college
{
	/// <summary>
	/// Summary description for edit.
	/// </summary>
	public partial class edit : System.Web.UI.Page
	{
		programData csProgram = new programData();

		protected System.Web.UI.WebControls.Label lblInstitution;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panError.Visible = false;
			panEdit.Visible = false;
			panDisplay.Visible = false;

			if(!IsPostBack){
				DataSet dsCollegeList = csProgram.GetCollegeList();
				Int32 intRowCtr = dsCollegeList.Tables[0].Rows.Count;
				
				TableRow tr = new TableRow();

				TableCell td = new TableCell();
				td.Attributes["style"] = "text-align:center;width:140px;font-weight:bold;padding-top:4px;padding-bottom:4px;";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("College Abbreviation"));

				tr.Cells.Add(td);

				td = new TableCell();
				td.Attributes["style"] = "text-align:center;width:385px;font-weight:bold;padding-top:4px;padding-bottom:4px;";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("College Title"));

				tr.Cells.Add(td);

				td = new TableCell();
				td.Attributes["style"] = "width:68px;text-align:center;";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("<a href=\"add.aspx\">Add New</a>"));

				tr.Cells.Add(td);
				tblDisplay.Rows.Add(tr);

				if(intRowCtr > 0){
					for(Int32 intDSRow = 0; intDSRow < intRowCtr; intDSRow++){
						Int32 collegeID = Convert.ToInt32(dsCollegeList.Tables[0].Rows[intDSRow]["CollegeID"]);
						String collegeLongTitle = dsCollegeList.Tables[0].Rows[intDSRow]["CollegeLongTitle"].ToString();

						tr = new TableRow();

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
						td.Controls.Add(new LiteralControl(dsCollegeList.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString()));

						tr.Cells.Add(td);

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
						td.Controls.Add(new LiteralControl(dsCollegeList.Tables[0].Rows[intDSRow]["CollegeLongTitle"].ToString()));

						tr.Cells.Add(td);

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;";
						td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.getElementById('hidTodo').value='edit';document.getElementById('hidCollegeID').value='" + collegeID + "';document.frmCollege.submit();\">Edit</a>"));
						if(Convert.ToInt32(dsCollegeList.Tables[0].Rows[intDSRow]["ProgCount"]) == 0 && Convert.ToInt32(dsCollegeList.Tables[0].Rows[intDSRow]["CrsCount"]) == 0){
							td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete " + collegeLongTitle.Replace("'","\\'") + "?')){document.getElementById('hidTodo').value='delete';document.getElementById('hidCollegeID').value='" + collegeID + "';document.frmCollege.submit();}\">Delete</a>"));
						}
						tr.Cells.Add(td);
						tblDisplay.Rows.Add(tr);
					}
				}else{
					tr = new TableRow();

					td = new TableCell();
					td.ColumnSpan = 3;
					td.Attributes["style"] = "width:100%;padding:4px;";
					td.CssClass = "portletLight";
					td.Controls.Add(new LiteralControl("No colleges currently exist."));

					tr.Cells.Add(td);
					tblDisplay.Rows.Add(tr);
				}
				panDisplay.Visible = true;
			}else if(hidTodo.Value == "edit"){
				DataSet dsCollege = csProgram.GetCollege(Convert.ToInt32(Request.Form["hidCollegeID"]));
				if(dsCollege.Tables[0].Rows.Count > 0){
					txtInstitution.Text = dsCollege.Tables[0].Rows[0]["Institution"].ToString();
					txtCollegeLongTitle.Text = dsCollege.Tables[0].Rows[0]["CollegeLongTitle"].ToString();
					txtCollegeShortTitle.Text = dsCollege.Tables[0].Rows[0]["CollegeShortTitle"].ToString();
					txtCollegeAddress.Text = dsCollege.Tables[0].Rows[0]["CollegeAddress"].ToString();
					txtCollegeCity.Text = dsCollege.Tables[0].Rows[0]["CollegeCity"].ToString();
					txtCollegeState.Text = dsCollege.Tables[0].Rows[0]["CollegeState"].ToString();
					txtCollegeZipCode1.Text = dsCollege.Tables[0].Rows[0]["CollegeZipCode1"].ToString();
					txtCollegeZipCode2.Text = dsCollege.Tables[0].Rows[0]["CollegeZipCode2"].ToString();
					txtCollegeWebsiteURL.Text = dsCollege.Tables[0].Rows[0]["CollegeWebsiteURL"].ToString();
					txtCollegeRegistrationEmail.Text = dsCollege.Tables[0].Rows[0]["CollegeRegistrationEmail"].ToString();
				}
				panEdit.Visible = true;
				panDisplay.Visible = false;
			}else if(hidTodo.Value == "delete"){
				csProgram.DeleteCollege(Convert.ToInt32(hidCollegeID.Value));
				Response.Redirect("edit.aspx");
			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			String institution = Request.Form["txtInstitution"], collegeShortTitle = Request.Form["txtCollegeShortTitle"], collegeLongTitle = Request.Form["txtCollegeLongTitle"], collegeAddress = Request.Form["txtCollegeAddress"], collegeCity = Request.Form["txtCollegeCity"], collegeState = Request.Form["txtCollegeState"], collegeZipCode1 = Request.Form["txtCollegeZipCode1"], collegeZipCode2 = Request.Form["txtCollegeZipCode2"], collegeWebsiteURL = Request.Form["txtCollegeWebsiteURL"], collegeRegistrationEmail = Request.Form["txtCollegeRegistrationEmail"];
			Int32 collegeID = Convert.ToInt32(Request.Form["hidCollegeID"]);

			Int16 intSuccess = csProgram.EditCollege(collegeID, institution, collegeShortTitle, collegeLongTitle, collegeAddress, collegeCity, collegeState, collegeZipCode1, collegeZipCode2, collegeWebsiteURL, collegeRegistrationEmail, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]);

			if(intSuccess == 1){
				Response.Redirect("edit.aspx");
			}else if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The college entered already exists.";
				panError.Visible = true;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The update failed.";
				panError.Visible = true;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
