using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.degree
{
	/// <summary>
	/// Summary description for add.
	/// </summary>
	public partial class add : System.Web.UI.Page
	{

		programData csProgram = new programData();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			if(!IsPostBack){
				panAdd.Visible = true;
				panError.Visible = false;
				panConfirm.Visible = false;
			}

			cboTransDeg.Items.Clear();
			DataSet dsDegreeRequirementWorksheet = csProgram.GetDocumentTitles();
			Int32 intRecordCount = dsDegreeRequirementWorksheet.Tables[0].Rows.Count;
			if(intRecordCount > 0){
				for(Int32 intDSRow = 0; intDSRow < intRecordCount; intDSRow++){
					cboTransDeg.Items.Add(dsDegreeRequirementWorksheet.Tables[0].Rows[intDSRow]["DocumentTitle"].ToString());
				}
				chkTransDeg.Enabled = true;
			}else{
				cboTransDeg.Items.Add(new ListItem("No degree requirement documents currently exist.", ""));
				chkTransDeg.Checked = false;
				chkTransDeg.Enabled = false;
			}

			try{
				cboTransDeg.SelectedValue = Request.Form["cboTransDeg"];
			}catch{
				//do nothing
			}

			cboTransDegYear.Items.Clear();
			DataSet dsEffectiveYears = csProgram.GetEffectiveYears(cboTransDeg.SelectedValue);
			for(Int32 i = 0; i < dsEffectiveYears.Tables[0].Rows.Count; i++){
				cboTransDegYear.Items.Add(dsEffectiveYears.Tables[0].Rows[i]["EffectiveYear"].ToString());
			}

			try{
				cboTransDegYear.SelectedValue = Request.Form["cboTransDegYear"];
			}catch{
				//do nothing
			}

			if(cboTransDegYear.SelectedValue != ""){
				lblViewLink.Text = "&nbsp;<a href=\"#\" onclick=\"popupCenter('http://internal.spokane.edu/iCatalog/program/degworksheet/" + csProgram.GetDegreeRequirementWorksheetFileName(cboTransDegYear.SelectedValue, cboTransDeg.SelectedValue) + "','Requirements', 950, 950);\">View Requirements for the Selected Year</a>";
			}else{
				lblViewLink.Text = "";
			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			bool blnIncludeDegWkst = false;
			if(Request.Form["chkTransDeg"] == "on"){
				blnIncludeDegWkst = true;
				lblDegreeRequirementWorksheet.Text = cboTransDeg.SelectedValue;
			}else{
				lblDegreeRequirementWorksheet.Text = "none selected";
			}

			Int16 intSuccess = csProgram.AddDegree(txtDegreeShortTitle.Text, txtDegreeLongTitle.Text, cboDegreeType.SelectedValue, blnIncludeDegWkst, cboTransDeg.SelectedValue, txtDegreeDescription.Value, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]);

			if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The insert failed.";
				panError.Visible = true;
			}else if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The degree entered already exists.";
				panError.Visible = true;
			}else if(intSuccess == 1){
				lblDegreeShortTitle.Text = txtDegreeShortTitle.Text;
				lblDegreeLongTitle.Text = txtDegreeLongTitle.Text;
				if(lblDegreeLongTitle.Text == ""){
					lblDegreeLongTitle.Text = "&nbsp;";
				}
				lblDegreeDescription.Text = txtDegreeDescription.Value;
				if(lblDegreeDescription.Text == ""){
					lblDegreeDescription.Text = "&nbsp;";
				}
				panError.Visible = false;
				panAdd.Visible = false;
				panConfirm.Visible = true;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
