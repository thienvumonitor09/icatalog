using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace ICatalog
{
	/// <summary>
	/// Summary description for DataTransfer.
	/// </summary>
	

	public partial class DataTransfer : System.Web.UI.Page
	{

		private SqlConnection objCon;
		private String strSQL, strConnection;
		private SqlCommand objCmd;
		private SqlDataAdapter objDA;

		protected void Page_Load(object sender, System.EventArgs e)
		{

            strConnection = "Initial Catalog=ICatalog;Data Source=DIST17-SQL3;Workstation ID=DIST17_CCSNET;Integrated Security=true;";
            //strConnection = "Initial Catalog=CCSICatalog;Data Source=DIST17-SQL3;Workstation ID=DIST17_CCSNET;Integrated Security=true;";
			
			objCon = new SqlConnection(strConnection);
			
			//try{
				objCon.Open();

/* FOOTNOTE QUERIES - STEP 1 - START (foreign keys not changed yet) */
/*				String strSQL2 = "";

				//get all program options to insert footnotes into
				strSQL = "SELECT Footnote.ProgColASN, ProgColDeg.ProgColDegASN, ProgramOption.ProgOptASN " +
						 "FROM CCSiCatalog.dbo.Footnote " +
						 "INNER JOIN CCSiCatalog.dbo.ProgColDeg " +
						 "ON ProgColDeg.ProgColASN = Footnote.ProgColASN " +
						 "INNER JOIN CCSiCatalog.dbo.ProgramOption " +
						 "ON ProgramOption.ProgColDegASN = ProgColDeg.ProgColDegASN " +
						 "GROUP BY Footnote.ProgColASN, ProgColDeg.ProgColDegASN, ProgramOption.ProgOptASN " + 
						 "ORDER BY Footnote.ProgColASN;";
				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				DataSet dsProgOptions = new DataSet();
				objDA.Fill(dsProgOptions);

				Int32 intLastProgColASN = 0;
				for(Int32 intProgOpt = 0; intProgOpt < dsProgOptions.Tables[0].Rows.Count; intProgOpt++){
					Int32 intProgColASN = Convert.ToInt32(dsProgOptions.Tables[0].Rows[intProgOpt]["ProgColASN"]);
					Int32 intProgColDegASN = Convert.ToInt32(dsProgOptions.Tables[0].Rows[intProgOpt]["ProgColDegASN"]);
					Int32 intProgOptASN = Convert.ToInt16(dsProgOptions.Tables[0].Rows[intProgOpt]["ProgOptASN"]);
					//panResults.Controls.Add(new LiteralControl("<div style=\"padding-top:10px;\"><br />ProgColASN = " + intProgColASN + "<br />ProgColDegASN = " + intProgColDegASN + "<br />ProgOptASN = " + intProgOptASN + "</div>"));

					//get all the footnotes to enter for each program option
					strSQL = "SELECT * FROM CCSiCatalog.dbo.Footnote WHERE ProgColASN = " + intProgColASN + " ORDER BY ProgColASN, FootnoteNumber;";
					objCmd = new SqlCommand(strSQL, objCon);
					objDA = new SqlDataAdapter(objCmd);
					DataSet dsFootnotes = new DataSet();
					objDA.Fill(dsFootnotes);

					//loop through footnotes in each ProgCol to add to ProgramOption
					for(Int32 i = 0; i < dsFootnotes.Tables[0].Rows.Count; i++){
						Int32 intFootnoteASN = Convert.ToInt32(dsFootnotes.Tables[0].Rows[i]["FootnoteASN"]);
						Int32 intFootnoteProgColASN = Convert.ToInt32(dsFootnotes.Tables[0].Rows[i]["ProgColASN"]);
						Int16 intFootnoteNumber = Convert.ToInt16(dsFootnotes.Tables[0].Rows[i]["FootnoteNumber"]);
						String strFootnote = dsFootnotes.Tables[0].Rows[i]["Footnote"].ToString();
						//panResults.Controls.Add(new LiteralControl("<div style=\"padding-left:20px;\"><br />FootnoteASN = " + intFootnoteASN + "<br />Footnote.ProgColASN = " + intFootnoteProgColASN + "<br />Footnote Number = " + intFootnoteNumber + "<br />Footnote = " + strFootnote + "</div>"));
					
						if(intLastProgColASN != intProgColASN){
							//panResults.Controls.Add(new LiteralControl("<div style=\"padding-left:20px;font-weight:bold\">EDIT ProgOptASN in Footnote records to " + intProgOptASN + ".</div>"));
							//panResults.Controls.Add(new LiteralControl("<div style=\"padding-left:20px;font-wieght:bold\">UPDATE CCSiCatalog.dbo.Footnote SET ProgOptASN = " + intProgOptASN + " WHERE FootnoteASN = " + intFootnoteASN + ";</div>"));
							
							strSQL2 += "UPDATE CCSiCatalog.dbo.Footnote " +
								"SET ProgOptASN = " + intProgOptASN +
								" WHERE FootnoteASN = " + intFootnoteASN + 
								" AND FootnoteNumber = " + intFootnoteNumber + 
								" AND ProgColASN = " + intFootnoteProgColASN + ";<br /><br />";
						}else{
							//panResults.Controls.Add(new LiteralControl("<div style=\"padding-left:20px;text-decoration:underline;\">Add New Footnote record for ProgOptASN " + intProgOptASN + ".<br />Update all foreign key references for program option to the new footnote PK value.</div>"));
							//panResults.Controls.Add(new LiteralControl("<div style=\"padding-left:20px;text-decoration:underline;\">INSERT INTO CCSiCatalog.dbo.Footnote(ProgColASN, ProgOptASN, FootnoteNumber, Footnote) VALUES(" + intFootnoteProgColASN + ", " + intProgOptASN + ", " + intFootnoteNumber + ", '" + strFootnote.Replace("'","''") + "');</div>"));
							//Do the foreign key references later - DONT FORGET TO UPDATE all FootnoteASN foreign keys in other tables to reference footnotes to the new footnote asn at the ProgOption level.
							
							strSQL2 += "INSERT INTO CCSiCatalog.dbo.Footnote(ProgColASN, ProgOptASN, FootnoteNumber, Footnote) " +
								"<br />VALUES(" + intFootnoteProgColASN + ", " + intProgOptASN + ", " + intFootnoteNumber + ", '" + strFootnote.Replace("'","''") + "');<br /><br />";
						}
					}

					intLastProgColASN = intProgColASN;
				}

				//write out footnote queries (updating the progcolasn in existing footnotes and duplicating existing footnotes (add records) for each program option
				panResults.Controls.Add(new LiteralControl(strSQL2));
*/
/* FOOTNOTE QUERIES - STEP 1 - END */

/* ELECTIVE GROUP QUERIES - STEP 2 - START */
/*				String strSQL3 = "";

				//get all program options to insert elective groups and electives into
				strSQL = "SELECT ElectiveGroup.ProgColASN, ProgColDeg.ProgColDegASN, ProgramOption.ProgOptASN " +
					"FROM CCSiCatalog.dbo.ElectiveGroup " +
					"INNER JOIN CCSiCatalog.dbo.ProgColDeg " +
					"ON ProgColDeg.ProgColASN = ElectiveGroup.ProgColASN " +
					"INNER JOIN CCSiCatalog.dbo.ProgramOption " +
					"ON ProgramOption.ProgColDegASN = ProgColDeg.ProgColDegASN " +
					"GROUP BY ElectiveGroup.ProgColASN, ProgColDeg.ProgColDegASN, ProgramOption.ProgOptASN " + 
					"ORDER BY ElectiveGroup.ProgColASN;";
				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				DataSet dsProgOptions = new DataSet();
				objDA.Fill(dsProgOptions);

				Int32 intLastProgColASN = 0;
				for(Int32 intProgOpt = 0; intProgOpt < dsProgOptions.Tables[0].Rows.Count; intProgOpt++){
					Int32 intProgColASN = Convert.ToInt32(dsProgOptions.Tables[0].Rows[intProgOpt]["ProgColASN"]);
					Int32 intProgColDegASN = Convert.ToInt32(dsProgOptions.Tables[0].Rows[intProgOpt]["ProgColDegASN"]);
					Int32 intProgOptASN = Convert.ToInt16(dsProgOptions.Tables[0].Rows[intProgOpt]["ProgOptASN"]);
					//panResults.Controls.Add(new LiteralControl("<div style=\"padding-top:10px;\"><br />ProgColASN = " + intProgColASN + "<br />ProgColDegASN = " + intProgColDegASN + "<br />ProgOptASN = " + intProgOptASN + "</div>"));

					//get all the elective groups to enter for each program option
					strSQL = "SELECT * FROM CCSiCatalog.dbo.ElectiveGroup WHERE ProgColASN = " + intProgColASN + " ORDER BY ProgColASN, ElectiveGroupTitle;";
					objCmd = new SqlCommand(strSQL, objCon);
					objDA = new SqlDataAdapter(objCmd);
					DataSet dsElectiveGroups = new DataSet();
					objDA.Fill(dsElectiveGroups);

					//loop through footnotes in each ProgCol to add to ProgramOption
					for(Int32 i = 0; i < dsElectiveGroups.Tables[0].Rows.Count; i++){
						Int32 intElectiveASN = Convert.ToInt32(dsElectiveGroups.Tables[0].Rows[i]["ElectiveASN"]);
						Int32 intElectiveGroupProgColASN = Convert.ToInt32(dsElectiveGroups.Tables[0].Rows[i]["ProgColASN"]);
						String strElectiveGroupTitle = dsElectiveGroups.Tables[0].Rows[i]["ElectiveGroupTitle"].ToString();
						String strFootnoteASN = dsElectiveGroups.Tables[0].Rows[i]["FootnoteASN"].ToString();

						if(intLastProgColASN != intProgColASN){
							//panResults.Controls.Add(new LiteralControl("<div style=\"padding-left:20px;font-weight:bold\">EDIT ProgOptASN in ElectiveGroup records to " + intProgOptASN + ".</div>"));
							panResults.Controls.Add(new LiteralControl("<div style=\"padding-left:20px;font-weight:bold\">UPDATE CCSiCatalog.dbo.ElectiveGroup SET ProgOptASN = " + intProgOptASN + " WHERE ElectiveASN = " + intElectiveASN + " AND ProgColASN = " + intElectiveGroupProgColASN + ";</div>"));
						}else{
							//panResults.Controls.Add(new LiteralControl("<div style=\"padding-left:20px;text-decoration:underline;\">Add New ElectiveGroup record for ProgOptASN " + intProgOptASN + ".</div>"));
							//Do the foreign key references later - DONT FORGET TO UPDATE all ElectiveASN foreign keys in other tables to reference electivegroups to the new electiveasn at the ProgOption level.
							
							if(strFootnoteASN != null && strFootnoteASN != ""){
								panResults.Controls.Add(new LiteralControl("<div style=\"padding-left:20px;text-decoration:underline;\">INSERT INTO CCSiCatalog.dbo.ElectiveGroup(ProgColASN, ProgOptASN, ElectiveGroupTitle, FootnoteASN) VALUES(" + intElectiveGroupProgColASN + ", " + intProgOptASN + ", '" + strElectiveGroupTitle.Replace("'","''") + "', " + strFootnoteASN + ");</div>"));
							}else{
								panResults.Controls.Add(new LiteralControl("<div style=\"padding-left:20px;text-decoration:underline;\">INSERT INTO CCSiCatalog.dbo.ElectiveGroup(ProgColASN, ProgOptASN, ElectiveGroupTitle) VALUES(" + intElectiveGroupProgColASN + ", " + intProgOptASN + ", '" + strElectiveGroupTitle.Replace("'","''") + "');</div>"));
							}
						}
					}

					intLastProgColASN = intProgColASN;
				}
*/
/* ELECTIVE GROUP QUERIES - STEP 2 END */

/* ELECTIVE QUERIES - STEP 3 START */
/*
				strSQL = "SELECT Elective.ElectiveASN, ElectiveGroup.ProgColASN, ElectiveGroup.ElectiveGroupTitle, CrsDescASN, Elective.FootnoteASN, OverwriteCr, OverwriteCrMin, OverwriteCrMax " +
						 "FROM CCSiCatalog.dbo.Elective " +
						 "INNER JOIN CCSiCatalog.dbo.ElectiveGroup " +
						 "ON ElectiveGroup.ElectiveASN = Elective.ElectiveASN " +
						 "ORDER BY ProgColASN, ElectiveGroupTitle;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				DataSet dsElectives = new DataSet();
				objDA.Fill(dsElectives);

				String strSQL2 = "SELECT * FROM CCSiCatalog.dbo.ElectiveGroup " +
						  "WHERE ElectiveASN NOT IN " +
						  "(SELECT ElectiveASN FROM CCSiCatalog.dbo.Elective) " +
						  "ORDER BY ProgColASN, ElectiveGroupTitle, ProgOptASN;";

				objCmd = new SqlCommand(strSQL2, objCon);
				objDA = new SqlDataAdapter(objCmd);
				DataSet dsElectiveGroups = new DataSet();
				objDA.Fill(dsElectiveGroups);

				//loop through the electives
				for(Int32 i = 0; i < dsElectives.Tables[0].Rows.Count; i++){
					//store elective fields in variables
					Int32 intElectiveASN = Convert.ToInt32(dsElectives.Tables[0].Rows[i]["ElectiveASN"]);
					Int32 intElective_ProgColASN = Convert.ToInt32(dsElectives.Tables[0].Rows[i]["ProgColASN"]);
					String strElective_ElectiveGroupTitle = dsElectives.Tables[0].Rows[i]["ElectiveGroupTitle"].ToString();
					Int32 intCrsDescASN = Convert.ToInt32(dsElectives.Tables[0].Rows[i]["CrsDescASN"]);
					String strFootnoteASN = dsElectives.Tables[0].Rows[i]["FootnoteASN"].ToString();
					Byte bitOverwriteCr = Convert.ToByte(dsElectives.Tables[0].Rows[i]["OverwriteCr"]);
					String strOverwriteCrMin = dsElectives.Tables[0].Rows[i]["OverwriteCrMin"].ToString();
					String strOverwriteCrMax = dsElectives.Tables[0].Rows[i]["OverwriteCrMax"].ToString();

					//loop through program option elective groups
					for(Int32 j = 0; j < dsElectiveGroups.Tables[0].Rows.Count; j++){
						Int32 intElectiveGroup_ProgColASN = Convert.ToInt32(dsElectiveGroups.Tables[0].Rows[j]["ProgColASN"]);
						String strElectiveGroup_ElectiveGroupTitle = dsElectiveGroups.Tables[0].Rows[j]["ElectiveGroupTitle"].ToString();
						Int32 intElectiveGroup_ElectiveASN = Convert.ToInt32(dsElectiveGroups.Tables[0].Rows[j]["ElectiveASN"]);

						//if the elective progcolasn = elective group progcolasn and elective electivegrouptitle = electivegroup electivegrouptitle
						if(intElective_ProgColASN == intElectiveGroup_ProgColASN && strElective_ElectiveGroupTitle == strElectiveGroup_ElectiveGroupTitle){
							//add the electives for the new option elective group to the electives table
							if(strFootnoteASN == "" && bitOverwriteCr == 1){
								Double dblOverwriteCrMin = Convert.ToDouble(strOverwriteCrMin);
								Double dblOverwriteCrMax = Convert.ToDouble(strOverwriteCrMax);
								panResults.Controls.Add(new LiteralControl("INSERT INTO CCSiCatalog.dbo.Elective(ElectiveASN, CrsDescASN, OverwriteCr, OverwriteCrMin, OverwriteCrMax) " +
									"VALUES(" + intElectiveGroup_ElectiveASN+ ", " + intCrsDescASN + ", " + bitOverwriteCr + ", " + dblOverwriteCrMin + ", " + dblOverwriteCrMax + ");<br />"));
							}else if(strFootnoteASN == "" && bitOverwriteCr == 0){
								panResults.Controls.Add(new LiteralControl("INSERT INTO CCSiCatalog.dbo.Elective(ElectiveASN, CrsDescASN, OverwriteCr) " +
									"VALUES(" + intElectiveGroup_ElectiveASN + ", " + intCrsDescASN + ", " + bitOverwriteCr + ");<br />"));
							}else if(strFootnoteASN != "" && bitOverwriteCr == 1){
								Double dblOverwriteCrMin = Convert.ToDouble(strOverwriteCrMin);
								Double dblOverwriteCrMax = Convert.ToDouble(strOverwriteCrMax);
								panResults.Controls.Add(new LiteralControl("INSERT INTO CCSiCatalog.dbo.Elective(ElectiveASN, CrsDescASN, FootnoteASN, OverwriteCr, OverwriteCrMin, OverwriteCrMax) " +
									"VALUES(" + intElectiveGroup_ElectiveASN + ", " + intCrsDescASN + ", " + strFootnoteASN + ", " + bitOverwriteCr + ", " + dblOverwriteCrMin + ", " + dblOverwriteCrMax + ");<br />"));
							}else if(strFootnoteASN != "" && bitOverwriteCr == 0){
								panResults.Controls.Add(new LiteralControl("INSERT INTO CCSiCatalog.dbo.Elective(ElectiveASN, CrsDescASN, FootnoteASN, OverwriteCr) " +
									"VALUES(" + intElectiveGroup_ElectiveASN + ", " + intCrsDescASN + ", " + strFootnoteASN + ", " + bitOverwriteCr + ");<br />"));
							}
						}
					}
				}
*/
/* ELECTIVE QUERIES - STEP 3 END */

/* PREREQUISITE QUERIES - STEP 4 START */
/*		
			String strSQL2 = "";

			//get all program options to insert prerequisites into
			strSQL = "SELECT Prerequisite.ProgColDegASN, ProgramOption.ProgOptASN " +
					 "FROM CCSiCatalog.dbo.Prerequisite " +
					 "INNER JOIN CCSiCatalog.dbo.ProgColDeg " +
					 "ON ProgColDeg.ProgColDegASN = Prerequisite.ProgColDegASN " +
					 "INNER JOIN CCSiCatalog.dbo.ProgramOption " +
					 "ON ProgramOption.ProgColDegASN = ProgColDeg.ProgColDegASN " +
					 "GROUP BY Prerequisite.ProgColDegASN, ProgramOption.ProgOptASN " +
					 "ORDER BY Prerequisite.ProgColDegASN;";
			objCmd = new SqlCommand(strSQL, objCon);
			objDA = new SqlDataAdapter(objCmd);
			DataSet dsProgOptions = new DataSet();
			objDA.Fill(dsProgOptions);

			Int32 intLastProgColDegASN = 0;
			for(Int32 intProgOpt = 0; intProgOpt < dsProgOptions.Tables[0].Rows.Count; intProgOpt++){
				Int32 intProgColDegASN = Convert.ToInt32(dsProgOptions.Tables[0].Rows[intProgOpt]["ProgColDegASN"]);
				Int32 intProgOptASN = Convert.ToInt16(dsProgOptions.Tables[0].Rows[intProgOpt]["ProgOptASN"]);
				//panResults.Controls.Add(new LiteralControl("<div style=\"padding-top:10px;\"><br />ProgColASN = " + intProgColASN + "<br />ProgColDegASN = " + intProgColDegASN + "<br />ProgOptASN = " + intProgOptASN + "</div>"));

				//get all the prerequisites to enter for each program option
				strSQL = "SELECT * FROM CCSiCatalog.dbo.Prerequisite WHERE ProgColDegASN = " + intProgColDegASN + " ORDER BY ProgColDegASN, CrsDescASN;";
				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				DataSet dsPrereqs = new DataSet();
				objDA.Fill(dsPrereqs);

				//loop through prerequisites in each ProgColDeg to add to ProgramOption
				for(Int32 i = 0; i < dsPrereqs.Tables[0].Rows.Count; i++){
					Int32 intPrereqASN = Convert.ToInt32(dsPrereqs.Tables[0].Rows[i]["PrereqASN"]);
					Int32 intCrsDescASN = Convert.ToInt32(dsPrereqs.Tables[0].Rows[i]["CrsDescASN"]);
					Int32 intPrereqProgColDegASN = Convert.ToInt32(dsPrereqs.Tables[0].Rows[i]["ProgColDegASN"]);
					String strFootnoteASN = dsPrereqs.Tables[0].Rows[i]["FootnoteASN"].ToString();
					//panResults.Controls.Add(new LiteralControl("<div style=\"padding-left:20px;\"><br />PrereqASN = " + intPrereqASN + "<br />Prereqs.ProgColDegASN = " + intPrereqProgColDegASN + "<br />CrsDescASN = " + intCrsDescASN + "<br />FootnoteASN = " + strFootnoteASN + "</div>"));
					
					if(intLastProgColDegASN != intProgColDegASN){
						strSQL2 += "UPDATE CCSiCatalog.dbo.Prerequisite " +
							"SET ProgOptASN = " + intProgOptASN +
							" WHERE PrereqASN = " + intPrereqASN + 
							" AND CrsDescASN = " + intCrsDescASN + 
							" AND ProgColDegASN = " + intPrereqProgColDegASN + ";<br /><br />";
					}else{
						if(strFootnoteASN == ""){
							strSQL2 += "INSERT INTO CCSiCatalog.dbo.Prerequisite(ProgColDegASN, ProgOptASN, CrsDescASN) " +
								"<br />VALUES(" + intPrereqProgColDegASN + ", " + intProgOptASN + ", " + intCrsDescASN + ");<br /><br />";
						}else{
							strSQL2 += "INSERT INTO CCSiCatalog.dbo.Prerequisite(ProgColDegASN, ProgOptASN, CrsDescASN, FootnoteASN) " +
								"<br />VALUES(" + intPrereqProgColDegASN + ", " + intProgOptASN + ", " + intCrsDescASN + ", " + strFootnoteASN + ");<br /><br />";
						}
					}
				}

				intLastProgColDegASN = intProgColDegASN;
			}
			//write out prerequisite queries
			panResults.Controls.Add(new LiteralControl(strSQL2));
*/
/* PREREQUISITE QUERIES - STEP 4 END */

/* STEP 5 Update FootnoteASN FK in ElectiveGroup table START */
/*			strSQL = "SELECT ElectiveGroup.ProgOptASN AS ElectiveGroup_ProgOptASN, " +
					 "Footnote.ProgOptASN AS Footnote_ProgOptASN, ElectiveGroup.FootnoteASN AS ElectiveGroup_FootnoteASN, " +
					 "Footnote.FootnoteASN AS Footnote_FootnoteASN, ElectiveASN, ElectiveGroupTitle, " +
					 "FootnoteNumber " +
					 "FROM CCSiCatalog.dbo.ElectiveGroup " +
					 "INNER JOIN CCSiCatalog.dbo.Footnote " +
					 "ON Footnote.FootnoteASN = ElectiveGroup.FootnoteASN " +
					 "WHERE Footnote.ProgOptASN <> ElectiveGroup.ProgOptASN " +
					 "ORDER BY ElectiveGroup_FootnoteASN;";

			objCmd = new SqlCommand(strSQL, objCon);
			objDA = new SqlDataAdapter(objCmd);
			DataSet dsElectiveGroupFootnotes = new DataSet();
			objDA.Fill(dsElectiveGroupFootnotes);

			for(Int32 i = 0; i < dsElectiveGroupFootnotes.Tables[0].Rows.Count; i++){
				Int16 intFootnoteNumber = Convert.ToInt16(dsElectiveGroupFootnotes.Tables[0].Rows[i]["FootnoteNumber"]);
				Int32 intElectiveASN = Convert.ToInt32(dsElectiveGroupFootnotes.Tables[0].Rows[i]["ElectiveASN"]);

				strSQL = "SELECT FootnoteASN " +
						 "FROM CCSiCatalog.dbo.Footnote " +
						 "WHERE ProgOptASN = " + dsElectiveGroupFootnotes.Tables[0].Rows[i]["ElectiveGroup_ProgOptASN"].ToString() +
						 "AND FootnoteNumber = " + intFootnoteNumber + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				Int32 intNewFootnoteASN = Convert.ToInt32(objCmd.ExecuteScalar());

				panResults.Controls.Add(new LiteralControl("UPDATE CCSiCatalog.dbo.ElectiveGroup SET FootnoteASN = " + intNewFootnoteASN + " WHERE ElectiveASN = " + intElectiveASN + ";<br />"));

			}
*/
/* STEP 5 Update FootnoteASN FK in ElectiveGroup table END */

/* STEP 6 Update FootnoteASN FK in Elective table START */
/*
			strSQL = "SELECT Elective.ElectiveASN, ElectiveGroup.ProgOptASN, CrsDescASN, Elective.FootnoteASN AS Elective_FootnoteASN, Footnote.FootnoteASN AS Footnote_FootnoteASN, FootnoteNumber " +
				 "FROM CCSiCatalog.dbo.Elective " +
				 "INNER JOIN CCSiCatalog.dbo.ElectiveGroup " +
				 "ON ElectiveGroup.ElectiveASN = Elective.ElectiveASN " +
				 "INNER JOIN CCSiCatalog.dbo.Footnote " +
				 "ON Footnote.FootnoteASN = Elective.FootnoteASN " +
				 "WHERE Footnote.ProgOptASN <> ElectiveGroup.ProgOptASN " +
				 "ORDER BY Elective_FootnoteASN;";

			objCmd = new SqlCommand(strSQL, objCon);
			objDA = new SqlDataAdapter(objCmd);
			DataSet dsElectiveFootnotes = new DataSet();
			objDA.Fill(dsElectiveFootnotes);

			for(Int32 i = 0; i < dsElectiveFootnotes.Tables[0].Rows.Count; i++){
				Int16 intFootnoteNumber = Convert.ToInt16(dsElectiveFootnotes.Tables[0].Rows[i]["FootnoteNumber"]);
				Int32 intElectiveASN = Convert.ToInt32(dsElectiveFootnotes.Tables[0].Rows[i]["ElectiveASN"]);
				Int32 intCrsDescASN = Convert.ToInt16(dsElectiveFootnotes.Tables[0].Rows[i]["CrsDescASN"]);

				strSQL = "SELECT FootnoteASN " +
					"FROM CCSiCatalog.dbo.Footnote " +
					"WHERE ProgOptASN = " + dsElectiveFootnotes.Tables[0].Rows[i]["ProgOptASN"].ToString() +
					"AND FootnoteNumber = " + intFootnoteNumber + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				Int32 intNewFootnoteASN = Convert.ToInt32(objCmd.ExecuteScalar());

				panResults.Controls.Add(new LiteralControl("UPDATE CCSiCatalog.dbo.Elective SET FootnoteASN = " + intNewFootnoteASN + " WHERE ElectiveASN = " + intElectiveASN + " AND CrsDescASN = " + intCrsDescASN + ";<br />"));
			}
*/
/* STEP 6 Update FootnoteASN FK in Elective table END */

/* STEP 7 Update FootnoteASN FK in Prerequisite table START */
/*
			strSQL = "SELECT PrereqASN, Prerequisite.FootnoteASN, Prerequisite.ProgOptASN AS Prerequisite_ProgOptASN, Footnote.ProgOptASN AS Footnote_ProgOptASN, FootnoteNumber " +
				 "FROM CCSiCatalog.dbo.Prerequisite " +
				 "INNER JOIN CCSiCatalog.dbo.Footnote " +
				 "ON Footnote.FootnoteASN = Prerequisite.FootnoteASN " +
				 "WHERE Prerequisite.ProgOptASN <> Footnote.ProgOptASN;";

			objCmd = new SqlCommand(strSQL, objCon);
			objDA = new SqlDataAdapter(objCmd);
			DataSet dsPrereqFootnotes = new DataSet();
			objDA.Fill(dsPrereqFootnotes);

			for(Int32 i = 0; i < dsPrereqFootnotes.Tables[0].Rows.Count; i++){
				Int16 intFootnoteNumber = Convert.ToInt16(dsPrereqFootnotes.Tables[0].Rows[i]["FootnoteNumber"]);
				Int32 intPrereqASN = Convert.ToInt32(dsPrereqFootnotes.Tables[0].Rows[i]["PrereqASN"]);

				strSQL = "SELECT FootnoteASN " +
					"FROM CCSiCatalog.dbo.Footnote " +
					"WHERE ProgOptASN = " + dsPrereqFootnotes.Tables[0].Rows[i]["Prerequisite_ProgOptASN"].ToString() +
					"AND FootnoteNumber = " + intFootnoteNumber + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				Int32 intNewFootnoteASN = Convert.ToInt32(objCmd.ExecuteScalar());

				panResults.Controls.Add(new LiteralControl("UPDATE CCSiCatalog.dbo.Prerequisite SET FootnoteASN = " + intNewFootnoteASN + " WHERE PrereqASN = " + intPrereqASN + ";<br />"));
			}
*/
/* STEP 7 Update FootnoteASN FK in Prerequisite table END */

/* STEP 8 Update ElectiveASN in ProgramOptionElective table to match the new ElectiveASN START */
/*			
			strSQL = "SELECT ProgramOptionElective.ProgOptASN AS ProgOptElect_ProgOptASN, ElectiveGroup.ProgOptASN AS ElectGroup_ProgOptASN, ProgramOptionElective.ElectiveASN AS ProgOptElect_ElectiveASN, ElectiveGroup.ElectiveASN AS ElectGroup_ElectiveASN, ElectiveGroupTitle " +
					 "FROM CCSiCatalog.dbo.ProgramOptionElective " +
					 "INNER JOIN CCSiCatalog.dbo.ElectiveGroup " +
					 "ON ElectiveGroup.ElectiveASN = ProgramOptionElective.ElectiveASN " +
					 "GROUP BY ProgramOptionElective.ProgOptASN, ElectiveGroup.ProgOptASN, ProgramOptionElective.ElectiveASN, ElectiveGroup.ElectiveASN, ElectiveGroupTitle " +
					 "ORDER BY ProgramOptionElective.ProgOptASN";

			objCmd = new SqlCommand(strSQL, objCon);
			objDA = new SqlDataAdapter(objCmd);
			DataSet dsProgOptElectives = new DataSet();
			objDA.Fill(dsProgOptElectives);

			for(Int32 i = 0; i < dsProgOptElectives.Tables[0].Rows.Count; i++){
				Int32 intProgOptElect_ElectiveASN = Convert.ToInt32(dsProgOptElectives.Tables[0].Rows[i]["ProgOptElect_ElectiveASN"]);
				Int32 intProgOptElect_ProgOptASN = Convert.ToInt32(dsProgOptElectives.Tables[0].Rows[i]["ProgOptElect_ProgOptASN"]);
				String strElectiveGroupTitle = dsProgOptElectives.Tables[0].Rows[i]["ElectiveGroupTitle"].ToString().Replace("'","''");

				strSQL = "SELECT ElectiveASN " +
						 "FROM CCSiCatalog.dbo.ElectiveGroup " +
						 "WHERE ProgOptASN = " + intProgOptElect_ProgOptASN +
						 " AND ElectiveGroupTitle = '" + strElectiveGroupTitle + "';";
				
				objCmd = new SqlCommand(strSQL, objCon);
				Int32 intNewElectiveASN = Convert.ToInt32(objCmd.ExecuteScalar());

				//LOOK AT RESULTS CLOSESLY TO MAKE SURE QUERY IS CORRECT
				panResults.Controls.Add(new LiteralControl("UPDATE CCSiCatalog.dbo.ProgramOptionElective<br />SET ProgramOptionElective.ElectiveASN = " + intNewElectiveASN + "<br />FROM CCSiCatalog.dbo.ProgramOptionElective<br />INNER JOIN CCSiCatalog.dbo.ElectiveGroup<br />ON ElectiveGroup.ElectiveASN = ProgramOptionElective.ElectiveASN<br />WHERE ProgramOptionElective.ElectiveASN = " + intProgOptElect_ElectiveASN + "<br />AND ProgramOptionElective.ProgOptASN = " + intProgOptElect_ProgOptASN + "<br />AND ElectiveGroupTitle = '" + strElectiveGroupTitle + "';<br /><br />"));

			}
*/
/* STEP 8 Update ElectiveASN in ProgramOptionElective table to match the new ElectiveASN END */

/* STEP 9 Update Footnote FK in ProgramOptionElective table START */
/*
			strSQL = "SELECT ProgramOptionElective.ProgOptASN AS ProgOptElect_ProgOptASN, " +
					 "Footnote.ProgOptASN AS Footnote_ProgOptASN, ProgramOptionElective.FootnoteASN AS ProgOptElect_FootnoteASN, " +
					 "Footnote.FootnoteASN AS Footnote_FootnoteASN, ElectiveASN, " +
					 "FootnoteNumber " +
					 "FROM CCSiCatalog.dbo.ProgramOptionElective " +
					 "INNER JOIN CCSiCatalog.dbo.Footnote " +
					 "ON Footnote.FootnoteASN = ProgramOptionElective.FootnoteASN " +
					 "WHERE Footnote.ProgOptASN <> ProgramOptionElective.ProgOptASN " +
					 "ORDER BY ProgOptElect_FootnoteASN;";

			objCmd = new SqlCommand(strSQL, objCon);
			objDA = new SqlDataAdapter(objCmd);
			DataSet dsProgOptElectFootnotes = new DataSet();
			objDA.Fill(dsProgOptElectFootnotes);

			for(Int32 i = 0; i < dsProgOptElectFootnotes.Tables[0].Rows.Count; i++){
				Int16 intFootnoteNumber = Convert.ToInt16(dsProgOptElectFootnotes.Tables[0].Rows[i]["FootnoteNumber"]);
				Int32 intFootnoteASN = Convert.ToInt32(dsProgOptElectFootnotes.Tables[0].Rows[i]["ProgOptElect_FootnoteASN"]);
				Int32 intElectiveASN = Convert.ToInt32(dsProgOptElectFootnotes.Tables[0].Rows[i]["ElectiveASN"]);
				Int32 intProgOptASN = Convert.ToInt32(dsProgOptElectFootnotes.Tables[0].Rows[i]["ProgOptElect_ProgOptASN"]);

				strSQL = "SELECT FootnoteASN " +
					"FROM CCSiCatalog.dbo.Footnote " +
					"WHERE ProgOptASN = " + intProgOptASN +
					"AND FootnoteNumber = " + intFootnoteNumber + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				Int32 intNewFootnoteASN = Convert.ToInt32(objCmd.ExecuteScalar());
				
				//LOOK AT RESULTS CLOSESLY TO MAKE SURE QUERY IS CORRECT
				panResults.Controls.Add(new LiteralControl("UPDATE CCSiCatalog.dbo.ProgramOptionElective SET FootnoteASN = " + intNewFootnoteASN + " WHERE ElectiveASN = " + intElectiveASN + " AND ProgOptASN = " + intProgOptASN + " AND FootnoteASN = " + intFootnoteASN + ";<br />"));
			}
*/
/* STEP 9 Update Footnote FK in ProgramOptionElective table END */

/* STEP 10 Update Footnote FK in OptionCourse START */
/*
			strSQL = "SELECT OptionCourse.CrsDescASN, OptionCourse.FootnoteASN, OptionCourse.ProgOptASN AS OptCrs_ProgOptASN, Footnote.ProgOptASN AS Footnote_ProgOptASN, FootnoteNumber " +
					 "FROM CCSiCatalog.dbo.OptionCourse " +
					 "INNER JOIN CCSiCatalog.dbo.Footnote " +
					 "ON Footnote.FootnoteASN = OptionCourse.FootnoteASN " +
					 "WHERE OptionCourse.ProgOptASN <> Footnote.ProgOptASN;";

			objCmd = new SqlCommand(strSQL, objCon);
			objDA = new SqlDataAdapter(objCmd);
			DataSet dsOptionCrsFootnotes = new DataSet();
			objDA.Fill(dsOptionCrsFootnotes);

			for(Int32 i = 0; i < dsOptionCrsFootnotes.Tables[0].Rows.Count; i++){
				Int16 intFootnoteNumber = Convert.ToInt16(dsOptionCrsFootnotes.Tables[0].Rows[i]["FootnoteNumber"]);
				Int32 intCrsDescASN = Convert.ToInt32(dsOptionCrsFootnotes.Tables[0].Rows[i]["CrsDescASN"]);
				Int32 intProgOptASN = Convert.ToInt32(dsOptionCrsFootnotes.Tables[0].Rows[i]["OptCrs_ProgOptASN"]);
				Int32 intFootnoteASN = Convert.ToInt32(dsOptionCrsFootnotes.Tables[0].Rows[i]["FootnoteASN"]);

				strSQL = "SELECT FootnoteASN " +
					"FROM CCSiCatalog.dbo.Footnote " +
					"WHERE ProgOptASN = " + intProgOptASN +
					"AND FootnoteNumber = " + intFootnoteNumber + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				Int32 intNewFootnoteASN = Convert.ToInt32(objCmd.ExecuteScalar());

				panResults.Controls.Add(new LiteralControl("UPDATE CCSiCatalog.dbo.OptionCourse SET FootnoteASN = " + intNewFootnoteASN + " WHERE ProgOptASN = " + intProgOptASN + " AND CrsDescASN = " + intCrsDescASN + " AND FootnoteASN = " + intFootnoteASN + ";<br />"));
			}
*/
/* STEP 10 Update Footnote FK in OptionCourse END */

/* STEP 11 - Set Program Option Locations to be the same as the College offering the program BEGIN */
/*
			strSQL = "SELECT CollegeAbrv, LocationASN, ProgramOption.ProgOptASN " +
					 "FROM CCSiCatalog.dbo.ProgramOption " +
					 "INNER JOIN CCSiCatalog.dbo.ProgColDeg " +
					 "ON ProgColDeg.ProgColDegASN = ProgramOption.ProgColDegASN " +
					 "INNER JOIN CCSiCatalog.dbo.ProgCol " +
					 "ON ProgCol.ProgColASN = ProgColDeg.ProgColASN " +
					 "INNER JOIN CCSiCatalog.dbo.College " +
					 "ON ProgCol.CollegeASN = College.CollegeASN " +
					 "INNER JOIN CCSiCatalog.dbo.Location " +
					 "ON Location.CollegeASN = College.CollegeASN " +
					 "WHERE CollegeAbrv <> 'IEL' " +
					 "GROUP BY CollegeAbrv, LocationASN, ProgramOption.ProgOptASN " +
					 "ORDER BY College.CollegeAbrv, LocationASN, ProgramOption.ProgOptASN;";

			objCmd = new SqlCommand(strSQL, objCon);
			objDA = new SqlDataAdapter(objCmd);
			DataSet dsProgOptions = new DataSet();
			objDA.Fill(dsProgOptions);

			for(Int32 i = 0; i < dsProgOptions.Tables[0].Rows.Count; i++){
				panResults.Controls.Add(new LiteralControl("INSERT INTO CCSiCatalog.dbo.OptionLocation(ProgOptASN, LocationASN) VALUES(" + dsProgOptions.Tables[0].Rows[i]["ProgOptASN"].ToString() + "," + dsProgOptions.Tables[0].Rows[i]["LocationASN"].ToString() + ");<br />"));
			}
*/
/* STEP 11 - Update Program Option Locations END */

/* --------------------------------------------------------------------------------------------------------------
			//get prereq courses to update crsid field
			strSQL = "SELECT Prerequisite.CrsDescASN, CrsDesc.CrsId " +
				 "FROM CCSiCatalog.dbo.Prerequisite " +
				 "LEFT OUTER JOIN CCSiCatalog.dbo.CrsDesc " +
				 "ON CrsDesc.CrsDescASN = Prerequisite.CrsDescASN " +
				 "GROUP BY Prerequisite.CrsDescASN, CrsDesc.CrsId " +
				 "ORDER BY CrsDesc.CrsId;";

			objCmd = new SqlCommand(strSQL, objCon);
			objDA = new SqlDataAdapter(objCmd);
			DataSet dsPrereqCourses = new DataSet();
			objDA.Fill(dsPrereqCourses);

			for(Int32 i = 0; i < dsPrereqCourses.Tables[0].Rows.Count; i++){
				panResults.Controls.Add(new LiteralControl("UPDATE CCSiCatalog.dbo.Prerequisite SET CrsId = '" + dsPrereqCourses.Tables[0].Rows[i]["CrsId"].ToString().Replace(" ","&nbsp;") + "' WHERE CrsDescASN = " + dsPrereqCourses.Tables[0].Rows[i]["CrsDescASN"].ToString() + ";<br />"));
			}

			panResults.Controls.Add(new LiteralControl("<br />"));

			//get option courses to update crsid field
			strSQL = "SELECT OptionCourse.CrsDescASN, CrsDesc.CrsId " +
					 "FROM CCSiCatalog.dbo.OptionCourse " +
					 "LEFT OUTER JOIN CCSiCatalog.dbo.CrsDesc " +
					 "ON CrsDesc.CrsDescASN = OptionCourse.CrsDescASN " +
					 "GROUP BY OptionCourse.CrsDescASN, CrsDesc.CrsId " +
					 "ORDER BY CrsDesc.CrsId;";

			objCmd = new SqlCommand(strSQL, objCon);
			objDA = new SqlDataAdapter(objCmd);
			DataSet dsOptionCourses = new DataSet();
			objDA.Fill(dsOptionCourses);

			for(Int32 i = 0; i < dsOptionCourses.Tables[0].Rows.Count; i++){
				panResults.Controls.Add(new LiteralControl("UPDATE CCSiCatalog.dbo.OptionCourse SET CrsId = '" + dsOptionCourses.Tables[0].Rows[i]["CrsId"].ToString().Replace(" ","&nbsp;") + "' WHERE CrsDescASN = " + dsOptionCourses.Tables[0].Rows[i]["CrsDescASN"].ToString() + ";<br />"));
			}

			panResults.Controls.Add(new LiteralControl("<br />"));

			//get elective courses to update crsid field
			strSQL = "SELECT Elective.CrsDescASN, CrsDesc.CrsId " +
					 "FROM CCSiCatalog.dbo.Elective " +
					 "LEFT OUTER JOIN CCSiCatalog.dbo.CrsDesc " +
					 "ON CrsDesc.CrsDescASN = Elective.CrsDescASN " +
					 "GROUP BY Elective.CrsDescASN, CrsDesc.CrsId " +
					 "ORDER BY CrsDesc.CrsId;";

			objCmd = new SqlCommand(strSQL, objCon);
			objDA = new SqlDataAdapter(objCmd);
			DataSet dsElectiveCourses = new DataSet();
			objDA.Fill(dsElectiveCourses);

			for(Int32 i = 0; i < dsElectiveCourses.Tables[0].Rows.Count; i++){
				panResults.Controls.Add(new LiteralControl("UPDATE CCSiCatalog.dbo.Elective SET CrsId = '" + dsElectiveCourses.Tables[0].Rows[i]["CrsId"].ToString().Replace(" ","&nbsp;") + "' WHERE CrsDescASN = " + dsElectiveCourses.Tables[0].Rows[i]["CrsDescASN"].ToString() + ";<br />"));
			}

			//}catch{
			//	panResults.Controls.Add(new LiteralControl("Error"));
			//}
            */

/* Changing course college db structure */
            //get IEL courses
                //panResults.Controls.Add(new LiteralControl("IEL COURSES<br />"));
                strSQL = "SELECT * FROM CrsDesc WHERE CrsIelInd = 1 ORDER BY CrsDescASN;";
                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                DataSet dsIELCourses = new DataSet();
                objDA.Fill(dsIELCourses);
                for (Int32 i = 0; i < dsIELCourses.Tables[0].Rows.Count; i++)
                {
                    //panResults.Controls.Add(new LiteralControl("CrsDescASN = " + dsIELCourses.Tables[0].Rows[i]["CrsDescASN"].ToString() + " | CrsId = " + dsIELCourses.Tables[0].Rows[i]["CrsId"].ToString() + " | " + dsIELCourses.Tables[0].Rows[i]["CrsTitle"].ToString() + " | Effective: " + dsIELCourses.Tables[0].Rows[i]["CrsBegEffYrq"].ToString() + " - " + dsIELCourses.Tables[0].Rows[i]["CrsEndEffYrq"].ToString() + "<br />"));
                    panResults.Controls.Add(new LiteralControl("INSERT INTO CCSiCatalog.dbo.CollegeCourse(CollegeASN, CrsDescASN) VALUES(21," + dsIELCourses.Tables[0].Rows[i]["CrsDescASN"].ToString() + ");<br />"));
                }

                panResults.Controls.Add(new LiteralControl("<br /><br />"));
            //get SCC courses
                strSQL = "SELECT * FROM CrsDesc WHERE CrsSCCInd = 1 ORDER BY CrsDescASN;";
                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                DataSet dsSCCCourses = new DataSet();
                objDA.Fill(dsSCCCourses);
                for (Int32 i = 0; i < dsSCCCourses.Tables[0].Rows.Count; i++)
                {
                    //panResults.Controls.Add(new LiteralControl("CrsDescASN = " + dsSCCCourses.Tables[0].Rows[i]["CrsDescASN"].ToString() + " | CrsId = " + dsSCCCourses.Tables[0].Rows[i]["CrsId"].ToString() + " | " + dsSCCCourses.Tables[0].Rows[i]["CrsTitle"].ToString() + " | Effective: " + dsSCCCourses.Tables[0].Rows[i]["CrsBegEffYrq"].ToString() + " - " + dsSCCCourses.Tables[0].Rows[i]["CrsEndEffYrq"].ToString() + "<br />"));
                    panResults.Controls.Add(new LiteralControl("INSERT INTO CCSiCatalog.dbo.CollegeCourse(CollegeASN, CrsDescASN) VALUES(19," + dsSCCCourses.Tables[0].Rows[i]["CrsDescASN"].ToString() + ");<br />"));
                }

                panResults.Controls.Add(new LiteralControl("<br /><br />"));

                //get SFCC courses
                strSQL = "SELECT * FROM CrsDesc WHERE CrsSFCCInd = 1 ORDER BY CrsDescASN;";
                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                DataSet dsSFCCCourses = new DataSet();
                objDA.Fill(dsSFCCCourses);
                for (Int32 i = 0; i < dsSFCCCourses.Tables[0].Rows.Count; i++)
                {
                    //panResults.Controls.Add(new LiteralControl("CrsDescASN = " + dsSFCCCourses.Tables[0].Rows[i]["CrsDescASN"].ToString() + " | CrsId = " + dsSFCCCourses.Tables[0].Rows[i]["CrsId"].ToString() + " | " + dsSFCCCourses.Tables[0].Rows[i]["CrsTitle"].ToString() + " | Effective: " + dsSFCCCourses.Tables[0].Rows[i]["CrsBegEffYrq"].ToString() + " - " + dsSFCCCourses.Tables[0].Rows[i]["CrsEndEffYrq"].ToString() + "<br />"));
                    panResults.Controls.Add(new LiteralControl("INSERT INTO CCSiCatalog.dbo.CollegeCourse(CollegeASN, CrsDescASN) VALUES(20," + dsSFCCCourses.Tables[0].Rows[i]["CrsDescASN"].ToString() + ");<br />"));
                }
		}

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
