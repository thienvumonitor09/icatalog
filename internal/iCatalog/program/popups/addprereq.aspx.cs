using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program
{
	/// <summary>
	/// Summary description for addprereq.
	/// </summary>
	public partial class addprereq : System.Web.UI.Page
	{
		programData csProgram = new programData();
		courseData csCourse = new courseData();

		#region PROTECTED CONTROLS


		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
            panAddCourse.Visible = false;
            panAddElectGroup.Visible = false;
            panError.Visible = false;
            panError2.Visible = false;

            if (!IsPostBack){
				hidProgramVersionID.Value = Request.QueryString["pvid"];
				hidProgramDegreeID.Value = Request.QueryString["pdid"];
				hidLtr.Value = Request.QueryString["ltr"];
				hidProgramTitle.Value = Request.QueryString["pt"];
				hidOfferedAt.Value = Request.QueryString["oa"];
				hidOptionID.Value = Request.QueryString["oid"];
				hidDegreeID.Value = Request.QueryString["did"];
				hidProgramDisplay.Value = Request.QueryString["pd"];
				hidProgramBeginSTRM.Value = Request.QueryString["bstrm"];
				hidProgramEndSTRM.Value = Request.QueryString["estrm"];
				hidPublishedProgram.Value = Request.QueryString["pp"];
				hidPublishedProgramVersionID.Value = Request.QueryString["ppvid"];
				hidSTRM.Value = Request.QueryString["strm"];
				hidSelectedCollegeID.Value = Request.QueryString["scolid"];
				hidProgramID.Value = Request.QueryString["id"];
				hidCollegeID.Value = Request.QueryString["colid"];
				hidSearchSubject.Value = Request.QueryString["searchsub"];
				hidSearchId.Value = Request.QueryString["searchid"];
				hidSearchSTRM.Value = Request.QueryString["searchstrm"];
				hidSearchText.Value = Request.QueryString["searchtxt"];

                optTodo.SelectedIndex = 0;

                Int32 programVersionID = Convert.ToInt32(Request.QueryString["pvid"]), DegreeID = Convert.ToInt32(hidDegreeID.Value);
                
                PopulateFootnoteDropdown();
            }
            
            if (optTodo.SelectedValue == "AddCourse")
            {
                panAddCourse.Visible = true;

                //Populate cboCrsDept for displaying SUBJECT
                DataSet dsCrsDept = csProgram.GetSubjectsForPrograms(hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
                DataTable dtCrsDept = dsCrsDept.Tables[0];
                if (dtCrsDept.Rows.Count > 0)
                {
                    foreach (DataRow drCrsDept in dtCrsDept.Rows)
                    {
                        if (Convert.ToInt32(drCrsDept["CrsCount"]) > 0)
                        {
                            cboCrsDept.Items.Add(drCrsDept["SUBJECT"].ToString());
                        }
                    }
                }
                else
                {
                    //do something -- may want to check if the cboCrsDept.length > 0
                }

                //Populate cboCatalogNbr for displaying CATALOG_NBR

                //DataSet dsCatalogNbrs = csProgram.GetCourseOfferings(cboCrsDept.SelectedValue, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
                //Int32 intCrsCount = dsCatalogNbrs.Tables[0].Rows.Count;
                DataSet dsCatalogNbrs = csProgram.GetCourseOfferings(cboCrsDept.SelectedValue, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
                DataTable dtCatalogNbrs = dsCatalogNbrs.Tables[0];


                cboCatalogNbr.Items.Clear();

                if (dtCatalogNbrs.Rows.Count > 0)
                {
                    foreach (DataRow drCatalogNbr in dtCatalogNbrs.Rows)
                    {
                        //Remove below ListItem because CourseSuffix is not used in ctcLink
                        //cboCatalogNbr.Items.Add(new ListItem(dsCatalogNbrs.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString() + dsCatalogNbrs.Tables[0].Rows[intDSRow]["CourseSuffix"].ToString(), dsCatalogNbrs.Tables[0].Rows[intDSRow]["CourseOffering"].ToString())); 
                        cboCatalogNbr.Items.Add(new ListItem(drCatalogNbr["CATALOG_NBR"].ToString(), drCatalogNbr["CourseOffering"].ToString()));
                    }

                    try
                    {
                        cboCatalogNbr.SelectedValue = Request.Form["cboCatalogNbr"];
                    }
                    catch
                    {
                        //do nothing
                    }

                    //get most current published version of course to display in program strm span
                    DataSet dsCrsToDisplay = csCourse.GetMostCurrentCourseForProgram(cboCatalogNbr.SelectedValue, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
                    DataTable dtCrsToDisplay = dsCrsToDisplay.Tables[0];
                    //Int32 intRowCount = dsCrsToDisplay.Tables[0].Rows.Count;

                    if (dtCrsToDisplay.Rows.Count > 0)
                    {
                        //lblTerm.Text = dsCrsToDisplay.Tables[0].Rows[0]["EFFDT"].ToString() + " - " + dsCrsToDisplay.Tables[0].Rows[0]["EndTerm_DESCR"].ToString();
                        lblEFFDT.Text = Convert.ToDateTime(dtCrsToDisplay.Rows[0]["EFFDT"]).ToShortDateString();
                        lblCourseLongTitle.Text = dtCrsToDisplay.Rows[0]["COURSE_TITLE_LONG"].ToString();
                    }
                    else
                    {
                        lblEFFDT.Text = "";
                        lblCourseLongTitle.Text = "<div style=\"color:red\">The selected course is not offered during the program begin and end terms.</div>";
                        //lblCourseLongTitle.Text = "<div style=\"color:red\">The most current version of the course selected could not be found.</div>";
                    }

                }
                else
                {
                    //do something -- may want to check if the cboCatalogNbrs.length > 0
                }

                if (cboFootnote.Items.Count == 0)
                {
                    cboFootnote.Items.Add(new ListItem("None Entered", "0"));
                    lblFootnote.Text = "None Selected";
                }
                else
                {
                    try
                    {
                        cboFootnote.SelectedValue = Request.Form["cboFootnote"];
                    }
                    catch
                    {
                        //do nothing
                    }

                    if (cboFootnote.SelectedValue == "0")
                    {
                        lblFootnote.Text = "None Selected";
                    }
                    else
                    {
                        lblFootnote.Text = csProgram.GetFootnote(Convert.ToInt32(cboFootnote.SelectedValue));
                    }
                } 
            }
            else if (optTodo.SelectedValue == "AddOptionElectiveGroup")
            {

                panAddElectGroup.Visible = true;

                //Populate cboElectGroupTitle 
                DataSet dsElectGroup = csProgram.GetOptionElectiveGroups(Convert.ToInt32(hidOptionID.Value));
                DataTable dtElectGroup = dsElectGroup.Tables[0];
                //Int32 intElectGroupCount = dsElectGroup.Tables[0].Rows.Count;

                cboElectGroupTitle.Items.Clear();

                if (dtElectGroup.Rows.Count > 0)
                {
                    foreach (DataRow drElectGroup in dtElectGroup.Rows)
                    {
                        cboElectGroupTitle.Items.Add(new ListItem(drElectGroup["ElectiveGroupTitle"].ToString(), drElectGroup["OptionElectiveGroupID"].ToString()));
                    }

                    try
                    {
                        cboElectGroupTitle.SelectedValue = Request.Form["cboElectGroupTitle"];
                    }
                    catch
                    {
                        //do nothing
                    }

                }
                else
                {
                    cboElectGroupTitle.Items.Add(new ListItem("No program electives currently exist", "0"));
                }

                if (cboElectGroupFootnote.Items.Count == 0)
                {
                    cboElectGroupFootnote.Items.Add(new ListItem("None Enteredhere", "0"));
                    lblElectGroupFootnote.Text = "None Selected";
                }
                else
                {
                    if (cboElectGroupFootnote.SelectedValue == "0")
                    {
                        lblElectGroupFootnote.Text = "None Selected";
                    }
                    else
                    {
                        lblElectGroupFootnote.Text = csProgram.GetFootnote(Convert.ToInt32(cboElectGroupFootnote.SelectedValue));
                    }
                }
            }

            

			
		}

		protected void cmdAddPrereq_Click(object sender, System.EventArgs e){

            UpdateValues();
            
            Int16 intSuccess = csProgram.AddOptPrereqs(Convert.ToInt32(hidOptionID.Value), Request.Form["cboCatalogNbr"], Convert.ToInt32(Request.Form["cboFootnote"]));
			
			if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The prerequisite entered already exists.";
				panError.Visible = true;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The insert failed.";
				panError.Visible = true;
			}else{
				Response.Write("<script type=\"text/javascript\">opener.location.href='../edit.aspx?pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&todo=pop&oid=" + hidOptionID.Value + "&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'","\\'") + "&pt=" + hidProgramTitle.Value.Replace("'","\\'") + "&pd=" + hidProgramDisplay.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&","%26") + "&searchid=" + hidSearchId.Value.Replace("&","%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&","%26").Replace("'","\\'") + "';</script>");
				
				//clear the footnote selection
				cboFootnote.SelectedIndex = 0;
				lblFootnote.Text = "None Selected";
			}
			hidFocus.Value = "cboCrsDept";
		}

        

        protected void cmdAddElectGroup_Click(object sender, System.EventArgs e)
        {
            UpdateValues();

            Int16 intSuccess = csProgram.AddElectGroupRefForPrereq(Convert.ToInt32(hidOptionID.Value), Convert.ToInt32(Request.Form["cboElectGroupTitle"]), Convert.ToInt32(Request.Form["cboElectGroupFootnote"]));

            if (intSuccess == 2)
            {
                lblErrorMsg2.Text = "Error: The elective group selected already exists."; //use a better message including the quarter
                panError2.Visible = true;
            }
            else if (intSuccess == 0)
            {
                lblErrorMsg2.Text = "Error: The insert failed.";
                panError2.Visible = true;
            }
            else
            {
                Response.Write("<script type=\"text/javascript\">opener.location.href='../edit.aspx?oid=" + hidOptionID.Value + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&todo=pop&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'", "\\'") + "&pt=" + hidProgramTitle.Value.Replace("'", "\\'") + "&pd=" + hidProgramDisplay.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&", "%26") + "&searchid=" + hidSearchId.Value.Replace("&", "%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&", "%26").Replace("'", "\\'") + "';</script>");
            }
            hidFocus.Value = "optTodo_1";
        }

        #region private method
        private void UpdateValues()
        {
            Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
            Int32 degreeID = Convert.ToInt32(hidDegreeID.Value);
            Int32 optionID = Convert.ToInt32(hidOptionID.Value);

            Byte bitPrimaryOption = 0;
            if (Request.Form["hidPrimaryOption"] == "true")
            {
                bitPrimaryOption = 1;
            }

            csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), Request.Form["hidOptionDescription"], Request.Form["hidOptionStateApproval"], Request.Form["hidCIP"], Request.Form["hidEPC"], Request.Form["hidAcademicPlan"], bitPrimaryOption, Request.Form["hidGainfulEmploymentID"], Request.Form["hidTotalQuarters"]);

            csProgram.DeleteOptionLocations(optionID);
            if (hidLocations.Value != "")
            {
                String[] strOptionLocations = hidLocations.Value.Substring(1).Split('|');
                for (Int32 i = 0; i < strOptionLocations.Length; i++)
                {
                    csProgram.AddOptionLocation(optionID, Convert.ToInt32(strOptionLocations[i]));
                }
            }

            csProgram.DeleteOptionMCodes(optionID);
            if (hidMCodes.Value != "")
            {
                String[] strMCodes = hidMCodes.Value.Substring(1).Split('|');
                for (Int32 i = 0; i < strMCodes.Length; i++)
                {
                    String[] strCollegeMCode = strMCodes[i].Split(';');
                    csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0]), strCollegeMCode[1]);
                }
            }
        }
        private void PopulateFootnoteDropdown()
        {
            //Populate dropdown for FootNote and ElectGroupFootNote
            DataSet dsFootnotes = csProgram.GetOptFootnotes(Convert.ToInt32(hidOptionID.Value));
            DataTable dtFootnotes = dsFootnotes.Tables[0];

            cboFootnote.DataSource = dtFootnotes;
            cboFootnote.DataTextField = "FootnoteNumber";
            cboFootnote.DataValueField = "OptionFootnoteID";
            cboFootnote.DataBind();
            cboFootnote.Items.Insert(0, new ListItem("", "0"));

            cboElectGroupFootnote.DataSource = dtFootnotes;
            cboElectGroupFootnote.DataTextField = "FootnoteNumber";
            cboElectGroupFootnote.DataValueField = "OptionFootnoteID";
            cboElectGroupFootnote.DataBind();
            cboElectGroupFootnote.Items.Insert(0, new ListItem("", "0"));
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
