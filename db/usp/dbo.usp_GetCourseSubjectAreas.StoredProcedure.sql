USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseSubjectAreas]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 06/10/2019
-- Description:	Returns subject areas of courses offered during the term entered.
--		 INPUT: INSTITUTION (optional) - 
--				STRM (optional) - Term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseSubjectAreas]
	@INSTITUTION varchar(5) = NULL,
	@STRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)
	SELECT Replace(CourseOffering.SUBJECT,'&','') AS SUBJECT, SubjectArea.DESCR AS SubjectArea_DESCR
    FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
		ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT 
    INNER JOIN SubjectArea 
    ON SubjectArea.SUBJECT = CourseOffering.SUBJECT 
	WHERE CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM))
	AND CATALOG_PRINT='Y'
	AND EFF_STATUS = 'A'
	AND (@INSTITUTION IS NULL OR INSTITUTION = @INSTITUTION)
	AND ACAD_CAREER = 'UGRD'
	GROUP BY Replace(CourseOffering.SUBJECT,'&',''), SubjectArea.DESCR
    ORDER BY SUBJECT;
END
GO
