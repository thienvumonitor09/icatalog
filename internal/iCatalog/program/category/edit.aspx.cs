using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.category
{
	/// <summary>
	/// Summary description for edit.
	/// </summary>
	public partial class edit : System.Web.UI.Page
	{
		programData csProgram = new programData();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panError.Visible = false;
			panEdit.Visible = false;
			panDisplay.Visible = false;

			if(!IsPostBack)
			{
				DataSet dsCategories = csProgram.GetCategories();
				Int32 intCategoryCount = dsCategories.Tables[0].Rows.Count;

				TableRow tr = new TableRow();

				TableCell td = new TableCell();
				td.Attributes["style"] = "text-align:center;width:525px;font-weight:bold;padding-top:4px;padding-bottom:4px;";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("Category Title"));

				tr.Cells.Add(td);

				td = new TableCell();
				td.Attributes["style"] = "width:68px;text-align:center";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("<a href=\"add.aspx\">Add New</a>"));

				tr.Cells.Add(td);
				tblDisplay.Rows.Add(tr);

				if(intCategoryCount > 0)
				{

					for(Int32 intDSRow = 0; intDSRow < intCategoryCount; intDSRow++)
					{
						Int32 intCatASN = Convert.ToInt32(dsCategories.Tables[0].Rows[intDSRow]["CategoryID"]);
						String categoryTitle = dsCategories.Tables[0].Rows[intDSRow]["CategoryTitle"].ToString();

						tr = new TableRow();

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
						td.Controls.Add(new LiteralControl(categoryTitle));

						tr.Cells.Add(td);

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;";
						td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.getElementById('hidTodo').value='edit';document.getElementById('hidCatASN').value='" + intCatASN + "';document.frmCategory.submit();\">Edit</a>"));
						if(Convert.ToInt32(dsCategories.Tables[0].Rows[intDSRow]["ProgCount"]) == 0)
						{
							td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete " + categoryTitle.Replace("'","\\'") + "?')){document.getElementById('hidTodo').value='delete';document.getElementById('hidCatASN').value='" + intCatASN + "';document.frmCategory.submit();}\">Delete</a>"));
						}
						tr.Cells.Add(td);
						tblDisplay.Rows.Add(tr);
					}
				}
				else
				{
					tr = new TableRow();

					td = new TableCell();
					td.ColumnSpan = 2;
					td.Attributes["style"] = "width:100%;padding:4px;";
					td.CssClass = "portletLight";
					td.Controls.Add(new LiteralControl("No categories currently exist."));

					tr.Cells.Add(td);
					tblDisplay.Rows.Add(tr);
				}
				panDisplay.Visible = true;
			}
			else if(hidTodo.Value == "edit")
			{
				DataSet dsCategory = csProgram.GetCategory(Convert.ToInt32(hidCatASN.Value));

				if(dsCategory.Tables[0].Rows.Count > 0)
				{
					txtCategoryTitle.Text = dsCategory.Tables[0].Rows[0]["CategoryTitle"].ToString();
					panEdit.Visible = true;
				}
				hidTodo.Value = "";
			}
			else if(hidTodo.Value == "delete")
			{
				csProgram.DeleteCategory(Convert.ToInt32(hidCatASN.Value));
				Response.Redirect("edit.aspx");
			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e)
		{
			Int16 intSuccess = csProgram.EditCategory(Convert.ToInt32(hidCatASN.Value), txtCategoryTitle.Text);
				
			if(intSuccess == 0)
			{
				lblErrorMsg.Text = "Error: The update failed.";
				panError.Visible = true;
				panEdit.Visible = true;
			}
			else if(intSuccess == 2)
			{
				lblErrorMsg.Text = "Error: The category entered already exists.";
				panError.Visible = true;
				panEdit.Visible = true;
			}
			else if(intSuccess == 1)
			{
				Response.Redirect("edit.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
