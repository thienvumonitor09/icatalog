USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMostCurrentCourseForProgramLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 4/21/17
-- Description:	Returns the most current version of a course in a program term range
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetMostCurrentCourseForProgramLegacy] 
	@CourseOffering varchar(9),
	@BeginSTRM varchar(4),
	@EndSTRM varchar(4)
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @CurrentSTRM varchar(4);
	SET @CurrentSTRM = (SELECT TOP 1(STRM)
						FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
						WHERE TERM_END_DT > GETDATE()
						ORDER BY TERM_END_DT)
						
	IF @EndSTRM < @CurrentSTRM /* ARCHIVED PROGRAM */
		/* Select the last active version of the course in the program term range. */
		SELECT TOP 1 CourseID, COURSE_TITLE_LONG, CourseBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, CourseEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, UNITS_MINIMUM, UNITS_MAXIMUM, VariableUnits
		FROM Course
		LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
		ON BeginTerm.STRM = CourseBeginSTRM
		LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
		ON EndTerm.STRM = CourseEndSTRM
		WHERE CourseOffering = @CourseOffering 
		AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999')) 
		OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM AND (CourseEndSTRM <= @EndSTRM OR CourseEndSTRM = 'Z999')))
		AND PublishedCourse = '1'
		ORDER BY CourseEndSTRM DESC;
	ELSE 
		BEGIN
			IF @BeginSTRM > @CurrentSTRM /* FUTURE PROGRAM */
				/* Select the first active version of the course in the program term range. */
				SELECT TOP 1 CourseID, COURSE_TITLE_LONG, CourseBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, CourseEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, UNITS_MINIMUM, UNITS_MAXIMUM, VariableUnits
				FROM Course
				LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
				ON BeginTerm.STRM = CourseBeginSTRM
				LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
				ON EndTerm.STRM = CourseEndSTRM
				WHERE CourseOffering = @CourseOffering 
				AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999'))
				OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM AND (CourseEndSTRM <= @EndSTRM OR CourseEndSTRM = 'Z999')))
				AND PublishedCourse = '1'
				ORDER BY CourseEndSTRM ASC;
			ELSE /* CURRENT PROGRAM */
				/* Select the most current active version of the course in the program term range. */
				SELECT CourseID, COURSE_TITLE_LONG, CourseBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, CourseEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, UNITS_MINIMUM, UNITS_MAXIMUM, VariableUnits
				FROM Course
				LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
				ON BeginTerm.STRM = CourseBeginSTRM
				LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
				ON EndTerm.STRM = CourseEndSTRM
				WHERE CourseOffering = @CourseOffering
				AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999'))
				OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM AND (CourseEndSTRM <= @EndSTRM OR CourseEndSTRM = 'Z999')))
				AND (CourseBeginSTRM <= @CurrentSTRM AND (CourseEndSTRM >= @CurrentSTRM OR CourseEndSTRM = 'Z999'))
				AND PublishedCourse = '1';
		END			
END

GO
