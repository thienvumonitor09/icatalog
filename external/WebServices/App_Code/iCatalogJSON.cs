﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Web.Script.Services;

/// <summary>
/// Summary description for iCatalogJSON
/// </summary>
[WebService(Namespace = "http://external.spokane.edu/WebServices/iCatalogJSON/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class iCatalogJSON : System.Web.Services.WebService {

    private SqlConnection objCon;
    private String strSQL, iCatalogConnection, ctcLinkConnection;
    private SqlCommand objCmd;
    private SqlDataAdapter objDA;
    private DataSet objDS;

    public iCatalogJSON() {
        /*production database connections*/
        ctcLinkConnection = "Initial Catalog=CCSGen_ctcLink_ODS;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=DIST17_CCSNET;Integrated Security=true;";

        //production connection string
        iCatalogConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=DIST17_CCSNET;Integrated Security=true;";

        //development connection string
        //iCatalogConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCS-SQL-DEV;Workstation ID=DIST17_CCSNET;Integrated Security=true;";
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns all colleges.</p>" +
             "<p><strong>Input:</strong> None</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: CollegeID, CollegeShortTitle, CollegeLongTitle</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetColleges(String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {

            objCon.Open();
            objCmd = new SqlCommand("usp_GetColleges", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns a program list ordered by title.</p>" +
             "<p><strong>Input:</strong><br />" +
             "String CollegeID - College primary key (Enter an empty string to return programs for all colleges.)<br />" +
             "String STRM - Term code (Enter an empty string to return the current term.)</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: ProgramTitle, ProgramVersionID, ProgramID, CollegeShortTitle</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetProgramsByTitle(String CollegeID, String STRM, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {

            objCon.Open();
            objCmd = new SqlCommand("usp_GetProgramsByTitle", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (CollegeID != null && CollegeID != "") {
                objCmd.Parameters.Add("@CollegeID", SqlDbType.Int);
                objCmd.Parameters["@CollegeID"].Value = CollegeID;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program details by non-changing program id.</p>" +
             "<p><strong>Input:</strong><br />" +
             "Int32 ProgramID - Non-changing Program ID (ProgID)<br />" +
             "String STRM - Term Code (Enter an empty string to return the current term.)</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: ProgramVersionID, ProgramTitle, CollegeID, CollegeShortTitle, CollegeLongTitle, CareerPlanningGuideID, ProgramEnrollment, ProgramWebsiteURL, ProgramDescription, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, ProgramBeginSTRM, BeginTerm_DESCR, ProgramEndSTRM, EndTerm_DESCR</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetProgramByProgramID(Int32 ProgramID, String STRM, String callback) {
        /*Add DegreeShortTitle, DegreeLongTitle, GainfulEmploymentID to select field list when data cleanup is complete and there is a one to one relation ship between programs and degrees */
        objCon = new SqlConnection(iCatalogConnection);
        try {

            objCon.Open();
            objCmd = new SqlCommand("usp_GetProgramByProgramID", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@ProgramID", SqlDbType.Int);
            objCmd.Parameters["@ProgramID"].Value = ProgramID;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns all degrees offered in a program</p>" +
             "<p><strong>Input:</strong><br />" +
             "Int32 ProgramID - Non-Changing Program ID<br />" +
             "String STRM - Term code</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: DegreeShortTitle, DegreeLongTitle, ProgramDegreeID, OptionTitle, OptionID</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetProgramDegrees(Int32 ProgramID, String STRM, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {

            objCon.Open();
            objCmd = new SqlCommand("usp_GetProgramDegreesByProgramID", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@ProgramID", SqlDbType.Int);
            objCmd.Parameters["@ProgramID"].Value = ProgramID;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns all degree options</p>" +
             "<p><strong>Input:</strong><br />" +
             "Int32 ProgramDegreeID - Program degree primary key<br />" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: OptionTitle, OptionDescription, OptionID, GainfulEmploymentID</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetDegreeOptions(Int32 ProgramDegreeID, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {

            objCon.Open();
            objCmd = new SqlCommand("usp_GetOptions", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@ProgramDegreeID", SqlDbType.Int);
            objCmd.Parameters["@ProgramDegreeID"].Value = ProgramDegreeID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns a program degree option.</p>" +
             "<p><strong>Input:</strong><br />" +
             "Int32 OptionID - Program degree option primary key</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: OptionTitle, OptionDescription, OptionID, GainfulEmploymentID</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetOption(Int32 OptionID, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetOption", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program option locations.</p>" +
             "<p><strong>Input:</strong><br />" +
             "Int32 OptionID - Program option primary key (ProgOptASN)</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: LocationID, LocationTitle, LocationWebsiteURL</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetOptionLocations(Int32 OptionID, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {

            objCon.Open();
            objCmd = new SqlCommand("usp_GetOptionLocations", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns option prerequisites</p>" +
             "<p><strong>Input:</strong><br />" +
             "Int32 OptionID - Option primary key<br />" +
             "String SelectedSTRM - Selected term code</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: SUBJECT, CATALOG_NBR, CourseSuffix, CourseOffering, COURSE_TITLE_LONG, CourseBeginSTRM, CourseEndSTRM, FootnoteNumber, OptionPrerequisiteID, CourseID, OptionFootnoteID, PrerequisiteCount</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetOptionPrerequisites(Int32 OptionID, String SelectedSTRM, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {

            objCon.Open();
            objCmd = new SqlCommand("usp_GetOptionPrerequisitesByOptionID", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            if (SelectedSTRM != null && SelectedSTRM != "") {
                objCmd.Parameters.Add("@SelectedSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@SelectedSTRM"].Value = SelectedSTRM;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program degree option courses and electives combined in one dataset.</p>" +
             "<p><strong>Input:</strong><br />" +
             "Int32 OptionID - Program degree option primary key<br />" +
             "String SelectedSTRM - Selected term code</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: CourseOffering (ZZZ will be stored as CourseOffering if elective), CourseID (OptionElectiveGroupID will be stored as CourseID if elective), SUBJECT (\"\" if elective), CATALOG_NBR (\"\" if elective), CourseSuffix (\"\" if elective), CourseEndSTRM (\"\" if elective), COURSE_TITLE_LONG (ElectiveGroupTitle will be stored as COURSE_TITLE_LONG if elective), UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionFootnoteID, OverwriteUnits (0 if elective), OverwriteUnitsMinumum (0 if elective), OverwriteUnitsMaximum (0 if elective)</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetOptionCoursesAndElectives(Int32 OptionID, String SelectedSTRM, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetOptionCoursesByOptionID", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            if (SelectedSTRM != null && SelectedSTRM != "") { 
                objCmd.Parameters.Add("@SelectedSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@SelectedSTRM"].Value = SelectedSTRM;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            objCmd = new SqlCommand("usp_GetOptionElectives", objCon);
            DataSet objDSElectives = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDSElectives);

            DataRow dr;
            //combine program option courses and electives in a dataset
            for (Int32 intRowCtr = 0; intRowCtr < objDSElectives.Tables[0].Rows.Count; intRowCtr++) {
                dr = objDS.Tables[0].NewRow();
                dr["CourseOffering"] = "ZZZ";
                dr["CourseID"] = objDSElectives.Tables[0].Rows[intRowCtr]["OptionElectiveGroupID"];
                dr["SUBJECT"] = "";
                dr["CATALOG_NBR"] = "";
                dr["CourseSuffix"] = "";
                dr["CourseEndSTRM"] = "";
                dr["COURSE_TITLE_LONG"] = objDSElectives.Tables[0].Rows[intRowCtr]["ElectiveGroupTitle"];
                dr["UNITS_MINIMUM"] = objDSElectives.Tables[0].Rows[intRowCtr]["UnitsMinimum"];
                dr["UNITS_MAXIMUM"] = objDSElectives.Tables[0].Rows[intRowCtr]["UnitsMaximum"];
                dr["Quarter"] = objDSElectives.Tables[0].Rows[intRowCtr]["Quarter"];
                dr["FootnoteNumber"] = objDSElectives.Tables[0].Rows[intRowCtr]["FootnoteNumber"];
                dr["OptionFootnoteID"] = objDSElectives.Tables[0].Rows[intRowCtr]["OptionFootnoteID"];
                dr["OverwriteUnits"] = 0;
                dr["OverwriteUnitsMinimum"] = 0;
                dr["OverwriteUnitsMaximum"] = 0;
                objDS.Tables[0].Rows.Add(dr);
            }

            //sort data by quarter
            objDS.Tables[0].DefaultView.Sort = "Quarter ASC";

            //store sorted data in a new datatable
            DataTable objDT = objDS.Tables[0].DefaultView.ToTable();

            //add datatable to dataset
            objDS = new DataSet();
            if (objDT.Rows.Count > 0) {
                objDS.Tables.Add(objDT);
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program degree option elective groups.</p>" +
             "<p><strong>Input:</strong><br />" +
             "Int32 OptionID - Program degree option primary key</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: ElectiveGroupTitle, FootnoteNumber, OptionFootnoteID, OptionElectiveGroupID, ElectiveCount</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetOptionElectiveGroups(Int32 OptionID, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetOptionElectiveGroups", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns elective group courses.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 OptionElectiveGroupID - Elective Group primary key<br />" +
         "String SelectedSTRM - Selected term code</p>" +
         "<p><strong>Output:</strong> JSONP<br />" +
         "FIELD LIST: OptionElectiveGroupID, CourseOffering, SUBJECT, CATALOG_NBR, CourseSuffix, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber, OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetElectiveGroupCourses(Int32 OptionElectiveGroupID, String SelectedSTRM, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetElectiveGroupCoursesByID", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionElectiveGroupID", SqlDbType.Int);
            objCmd.Parameters["@OptionElectiveGroupID"].Value = OptionElectiveGroupID;
            if (SelectedSTRM != null && SelectedSTRM != "") {
                objCmd.Parameters.Add("@SelectedSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@SelectedSTRM"].Value = SelectedSTRM;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program degree option footnotes.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 OptionID - Program degree option primary key </p>" +
         "<p><strong>Output:</strong> JSONP<br />" +
         "FIELD LIST: OptionFootnoteID, FootnoteNumber, Footnote</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetOptionFootnotes(Int32 OptionID, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetOptionFootnotes", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns a degree by OptionID.</p>" +
             "<p><strong>Input:</strong><br />" +
             "Int32 OptionID - Option/Degree ID<br />" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: ProgramTitle, OptionTitle, OptionDescription, CollegeID, CollegeShortTitle, CollegeLongTitle, CareerPlanningGuideID, ProgramEnrollment, ProgramWebsiteURL, ProgramDescription, OptionDescription, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, ProgramBeginSTRM, BeginTerm_DESCR, ProgramEndSTRM, EndTerm_DESCR</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetDegree(Int32 OptionID, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {

            objCon.Open();
            objCmd = new SqlCommand("usp_GetDegree", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Checks if a remote file exists.</p>" +
             "<p><strong>Input:</strong><br />" +
             "String URL - URL of the Remote File<br />" +
             "<p><strong>Output:</strong> JSONP</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void RemoteFileExists(String URL, String callback) {
        bool returnValue = false;

        try {
            //Creating the HttpWebRequest
            HttpWebRequest request = WebRequest.Create(URL) as HttpWebRequest;
            //Setting the Request method HEAD, you can also use GET too.
            request.Method = "HEAD";
            //Getting the Web Response.
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            //Returns TRUE if the Status code == 200
            returnValue = (response.StatusCode == HttpStatusCode.OK);
        } catch {
            //Any exception will return false.
            returnValue = false;
        }

        StringBuilder sb = new StringBuilder();
        sb.Append(callback + "(");
        sb.Append(JsonConvert.SerializeObject(returnValue, Formatting.Indented));
        sb.Append(");");

        Context.Response.Clear();
        Context.Response.ContentType = "application/json";
        Context.Response.Write(sb.ToString());
        Context.Response.End();
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns areas of study offering degrees/certificates for a specified term.</p>" +
             "<p><strong>Input:</strong><br />" +
             "String STRM - Term code (The current term is used if an empty string is entered.)<br />" +
             "String Institution - Institution: WA171 = SCC, WA172 = SFCC (Enter an empty string for both colleges.)</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: AreaOfStudyID, Title, Institution</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetAreasOfStudy(String STRM, String Institution, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {

            objCon.Open();
            objCmd = new SqlCommand("usp_GetAreasOfStudy", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (Institution != null && Institution != "") {
                objCmd.Parameters.Add("@Institution", SqlDbType.VarChar);
                objCmd.Parameters["@Institution"].Value = Institution;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns an alphabetic area of study list by keyword.</p>" +
             "<p><strong>Input:</strong><br />" +
             "String STRM - Term code (The current term is used if an empty string is entered.)<br />" +
             "String Institution - Institution: WA171 = SCC, WA172 = SFCC (Enter an empty string for both colleges.)</p>" +
             "String AlphaCharacter - Letter of the alphabet the area of study starts with (Enter an empty string to return all areas of study.)<br />" +
             "String Keyword - Search Text</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: AreaOfStudyID, Title, WebsiteURL, Institution, CollegeShortTitle</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetAreasOfStudyByKeyword(String STRM, String Institution, String AlphaCharacter, String Keyword, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {

            objCon.Open();
            objCmd = new SqlCommand("usp_GetAreasOfStudyByKeyword", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (Institution != null && Institution != "") {
                objCmd.Parameters.Add("@Institution", SqlDbType.VarChar);
                objCmd.Parameters["@Institution"].Value = Institution;
            }
            if (AlphaCharacter != null && AlphaCharacter != "") {
                objCmd.Parameters.Add("@AlphaCharacter", SqlDbType.Char);
                objCmd.Parameters["@AlphaCharacter"].Value = AlphaCharacter;
            }
            if (Keyword != null && Keyword != "") {
                objCmd.Parameters.Add("@Keyword", SqlDbType.VarChar);
                objCmd.Parameters["@Keyword"].Value = Keyword;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns an alphabetic area of study degree list.</p>" +
             "<p><strong>Input:</strong><br />" +
             "String STRM - Term Code<br />" +
             "String LocationID - Location primary key (Enter an empty string to return program options for all locations.)<br />" +
             "String DegreeID - Degree primary key (Enter an empty string to return all degrees.)<br />" +
             "String AlphaCharacter - Letter of the alphabet the area of study starts with (Enter an empty string to return all areas of study.)<br />" +
             "String AreaOfStudyID - Area of Study Primary key (Enter an empty string to return degrees in all areas of study.)</br />" +
             "String SUBJECT - Course subject area (Enter an empty string to return program options containing all course subject areas.)<br />" +
             "String CATALOG_NBR - Coruse catalog number (Enter an empty string to return program options containing all course catalog numbers.)<br />" +
             "String GlobalSearch - Search Text</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: Title, CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, ProgramID, ProgramVersionID, ProgramDegreeID, OptionID, DocumentTitle, DegreeType</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetDegreesByAreaOfStudy(String STRM, String LocationID, String DegreeID, String AlphaCharacter, String AreaOfStudyID, String SUBJECT, String CATALOG_NBR, String GlobalSearch, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();

            //get programs by area of study
            objCmd = new SqlCommand("usp_GetDegreeProgramsByAreaOfStudy", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (LocationID != null && LocationID != "") {
                objCmd.Parameters.Add("@LocationID", SqlDbType.Int);
                objCmd.Parameters["@LocationID"].Value = LocationID;
            }
            if (DegreeID != null && DegreeID != "") {
                objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                objCmd.Parameters["@DegreeID"].Value = DegreeID;
            }
            if (AlphaCharacter != null && AlphaCharacter != "") {
                objCmd.Parameters.Add("@AlphaCharacter", SqlDbType.Char);
                objCmd.Parameters["@AlphaCharacter"].Value = AlphaCharacter;
            }
            if (AreaOfStudyID != null && AreaOfStudyID != "") {
                objCmd.Parameters.Add("@AreaOfStudyID", SqlDbType.Int);
                objCmd.Parameters["@AreaOfStudyID"].Value = AreaOfStudyID;
            }
            if (SUBJECT != null && SUBJECT != "") {
                objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
            }
            if (CATALOG_NBR != null && CATALOG_NBR != "") {
                objCmd.Parameters.Add("@CATALOG_NBR", SqlDbType.VarChar);
                objCmd.Parameters["@CATALOG_NBR"].Value = CATALOG_NBR;
            }
            if (GlobalSearch != null && GlobalSearch != "") {
                objCmd.Parameters.Add("@GlobalSearch", SqlDbType.VarChar);
                objCmd.Parameters["@GlobalSearch"].Value = GlobalSearch;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            if ((SUBJECT == null || SUBJECT == "") && (CATALOG_NBR == null || CATALOG_NBR == "")) {
                //get general degrees by area of study
                objCmd = new SqlCommand("usp_GetDegreesByAreaOfStudy", objCon);
                DataSet objDSDegrees = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                if (STRM != null && STRM != "") {
                    objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                    objCmd.Parameters["@STRM"].Value = STRM;
                }
                if (DegreeID != null && DegreeID != "") {
                    objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                    objCmd.Parameters["@DegreeID"].Value = DegreeID;
                }
                if (AlphaCharacter != null && AlphaCharacter != "") {
                    objCmd.Parameters.Add("@AlphaCharacter", SqlDbType.Char);
                    objCmd.Parameters["@AlphaCharacter"].Value = AlphaCharacter;
                }
                if (AreaOfStudyID != null && AreaOfStudyID != "") {
                    objCmd.Parameters.Add("@AreaOfStudyID", SqlDbType.Int);
                    objCmd.Parameters["@AreaOfStudyID"].Value = AreaOfStudyID;
                }
                if (GlobalSearch != null && GlobalSearch != "") {
                    objCmd.Parameters.Add("@GlobalSearch", SqlDbType.VarChar);
                    objCmd.Parameters["@GlobalSearch"].Value = GlobalSearch;
                }
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDSDegrees);

                //if objDS dataset is not empty
                if (objDS.Tables.Count > 0) {

                    DataRow dr;
                    //combine program degrees and general degrees
                    for (Int32 i = 0; i < objDSDegrees.Tables[0].Rows.Count; i++) {
                        dr = objDS.Tables[0].NewRow();
                        dr["Title"] = objDSDegrees.Tables[0].Rows[i]["Title"];
                        dr["CollegeShortTitle"] = "";
                        dr["CollegeLongTitle"] = "";
                        dr["ProgramTitle"] = "";
                        dr["ProgramWebsiteURL"] = "";
                        dr["OptionTitle"] = "";
                        dr["DegreeID"] = objDSDegrees.Tables[0].Rows[i]["DegreeID"];
                        dr["DegreeShortTitle"] = objDSDegrees.Tables[0].Rows[i]["DegreeShortTitle"];
                        dr["DegreeLongTitle"] = objDSDegrees.Tables[0].Rows[i]["DegreeLongTitle"];
                        dr["ProgramID"] = 0;
                        dr["ProgramVersionID"] = 0;
                        dr["ProgramDegreeID"] = 0;
                        dr["OptionID"] = 0;
                        dr["DocumentTitle"] = objDSDegrees.Tables[0].Rows[i]["DocumentTitle"];
                        dr["DegreeType"] = objDSDegrees.Tables[0].Rows[i]["DegreeType"];
                        objDS.Tables[0].Rows.Add(dr);
                    }

                    //sort data by quarter
                    objDS.Tables[0].DefaultView.Sort = "Title, DegreeLongTitle, ProgramTitle, OptionTitle, CollegeShortTitle";

                    //store sorted data in a new datatable
                    DataTable objDT = objDS.Tables[0].DefaultView.ToTable();

                    //add datatable to dataset
                    objDS = new DataSet();
                    if (objDT.Rows.Count > 0) {
                        objDS.Tables.Add(objDT);
                    }

                    StringBuilder sb = new StringBuilder();
                    sb.Append(callback + "(");
                    sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
                    sb.Append(");");

                    Context.Response.Clear();
                    Context.Response.ContentType = "application/json";
                    Context.Response.Write(sb.ToString());
                    Context.Response.End();

                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(callback + "(");
                    sb.Append(JsonConvert.SerializeObject(objDSDegrees, Formatting.Indented));
                    sb.Append(");");

                    Context.Response.Clear();
                    Context.Response.ContentType = "application/json";
                    Context.Response.Write(sb.ToString());
                    Context.Response.End();
                }

            } else {
                StringBuilder sb = new StringBuilder();
                sb.Append(callback + "(");
                sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
                sb.Append(");");

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(sb.ToString());
                Context.Response.End();
            }

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns an alphabetic area of study degree list.</p>" +
             "<p><strong>Input:</strong><br />" +
             "String STRM - Term Code (enter an empty string to return the current term)<br />" +
             "String Institution - SCC: WA171, SFCC WA172 (enter an empty string to return all areas of study.)<br />" +
             "String AreaOfStudyID - Area of Study Primary key (Enter an empty string to return degrees in all areas of study.)</br />" +
             "String ProgramID - Non-changing Program ID</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: CollegeShortTitle, CollegeLongTitle, AreaOfStudyID, Title, ProgramID, ProgramVersionID, DegreeShortTitle, DegreeLongTitle, ProgramTitle, ProgramWebsiteURL</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetDegreeLinks(String STRM, String Institution, String AreaOfStudyID, String ProgramID, String callback) {
        //temporary, change to production URL when data cleanup is complete - BV
        //String iCatalogConnectionDev = "Initial Catalog=CCSICatalog_Cleanup;Data Source=CCS-SQLDEV;Workstation ID=DIST17_CCSNET;Integrated Security=true;";
        //objCon = new SqlConnection(iCatalogConnectionDev);

        objCon = new SqlConnection(iCatalogConnection);

        try {
            objCon.Open();

            //get programs by area of study
            objCmd = new SqlCommand("usp_GetDegreeLinks", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (Institution != null && Institution != "") {
                objCmd.Parameters.Add("@Institution ", SqlDbType.VarChar);
                objCmd.Parameters["@Institution "].Value = Institution;
            }
            if (AreaOfStudyID != null && AreaOfStudyID != "") {
                objCmd.Parameters.Add("@AreaOfStudyID", SqlDbType.Int);
                objCmd.Parameters["@AreaOfStudyID"].Value = AreaOfStudyID;
            }
            if (ProgramID != null && ProgramID != "") {
                objCmd.Parameters.Add("@ProgramID", SqlDbType.Int);
                objCmd.Parameters["@ProgramID"].Value = ProgramID;
            }
            
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns a list of locations offering degrees/certificates in an area of study.</p>" +
             "<p><strong>Input:</strong><br />" +
             "String STRM - Term Code (enter an empty string to return the current term)<br />" +
             "String AreaOfStudyID - Area of Study Primary key (Enter an empty string to return locations for all areas of study.)</br />" +
             "String ProgramID - Non-changing Program ID (Enter an empty string to return locations for all degrees/certificates.)</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: Title, LocationTitle, LocationWebsiteURL, LocationDegreeCount, AreaOfStudyDegreeCount</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetAreaOfStudyDegreeLocations(String STRM, String AreaOfStudyID, String ProgramID, String callback) {

        objCon = new SqlConnection(iCatalogConnection);

        try {
            objCon.Open();

            //get programs by area of study
            objCmd = new SqlCommand("usp_GetAreaOfStudyDegreeLocations", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (AreaOfStudyID != null && AreaOfStudyID != "") {
                objCmd.Parameters.Add("@AreaOfStudyID", SqlDbType.Int);
                objCmd.Parameters["@AreaOfStudyID"].Value = AreaOfStudyID;
            }
            if (ProgramID != null && ProgramID != "") {
                objCmd.Parameters.Add("@ProgramID", SqlDbType.Int);
                objCmd.Parameters["@ProgramID"].Value = ProgramID;
            }

            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns degree/certificate program attributes.</p>" +
             "<p><strong>Input:</strong><br />" +
             "String STRM - Term Code (enter an empty string to return the current term)<br />" +
             "String ProgramID - Non-changing Program ID</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: TotalCredits, TotalQuarters, Tuition, FinancialAidEligible</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetProgramAttributesByProgramID(String STRM, Int32 ProgramID, String callback) {
        objCon = new SqlConnection(iCatalogConnection);
        try {

            objCon.Open();
            objCmd = new SqlCommand("usp_GetProgramAttributesByProgramID", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;

            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }

            objCmd.Parameters.Add("@ProgramID", SqlDbType.Int);
            objCmd.Parameters["@ProgramID"].Value = ProgramID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();

        } finally {
            objCon.Close();
        }
    }
    [WebMethod(Description = "<p><strong>Description:</strong> Returns Tution.</p>" +
            "<p><strong>Input:</strong><br />" +
            "String OptionID - OptionID<br />" +
            "<p><strong>Output:</strong> JSONP<br />" +
            "FIELD LIST: TutionResident, TuitionNonResident, TuitionNonResidentInternational, BookCosts, PlanFees, CourseFees, OptionTitle</p>")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetTuition(String OptionID, String callback)
    {
        objCon = new SqlConnection(iCatalogConnection);
        try
        {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetTuition", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            //objCmd.Parameters.AddWithValue("@ACAD_PLAN", ACAD_PLAN);
            objCmd.Parameters.AddWithValue("@OptionID", OptionID);
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            StringBuilder sb = new StringBuilder();
            sb.Append(callback + "(");
            sb.Append(JsonConvert.SerializeObject(objDS, Formatting.Indented));
            sb.Append(");");

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(sb.ToString());
            Context.Response.End();
        }
        finally
        {
            objCon.Close();
        }
    }

}

