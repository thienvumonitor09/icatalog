using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.degree
{
	/// <summary>
	/// Summary description for edit.
	/// </summary>
	public partial class edit : System.Web.UI.Page
	{
		programData csProgram = new programData();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panError.Visible = false;
			panEdit.Visible = false;
			panDisplay.Visible = false;

			if(!IsPostBack){
				DataSet dsDegreeList = csProgram.GetDegreeList();
				Int32 intRowCtr = dsDegreeList.Tables[0].Rows.Count;
				
				TableRow tr = new TableRow();

				TableCell td = new TableCell();
				td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("Degree Short Title"));

				tr.Cells.Add(td);

				td = new TableCell();
				td.Attributes["style"] = "text-align:center;width:370px;font-weight:bold;padding-top:4px;padding-bottom:4px;";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("Degree Long Title"));

				tr.Cells.Add(td);

				td = new TableCell();
				td.Attributes["style"] = "text-align:center;width:100px;font-weight:bold;padding-top:4px;padding-bottom:4px;";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("Degree Type"));

				tr.Cells.Add(td);

				td = new TableCell();
				td.Attributes["style"] = "width:68px;text-align:center";
				td.CssClass = "portletSecondary";
				td.Controls.Add(new LiteralControl("<a href=\"add.aspx\">Add New</a>"));

				tr.Cells.Add(td);
				tblDisplay.Rows.Add(tr);

				if(intRowCtr > 0){
					for(Int32 intDSRow = 0; intDSRow < intRowCtr; intDSRow++){
						Int32 degreeID = Convert.ToInt32(dsDegreeList.Tables[0].Rows[intDSRow]["DegreeID"]);
						String strDegreeShortTitle = dsDegreeList.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();

						tr = new TableRow();

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "vertical-align:top;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
						td.Controls.Add(new LiteralControl(strDegreeShortTitle));

						tr.Cells.Add(td);

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "vertical-align:top;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
						td.Controls.Add(new LiteralControl(dsDegreeList.Tables[0].Rows[intDSRow]["DegreeLongTitle"].ToString()));

						tr.Cells.Add(td);

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "vertical-align:top;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
						td.Controls.Add(new LiteralControl(dsDegreeList.Tables[0].Rows[intDSRow]["DegreeType"].ToString()));

						tr.Cells.Add(td);

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;";
						td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.getElementById('hidTodo').value='edit';document.getElementById('hidDegreeID').value='" + degreeID + "';document.frmDegree.submit();\">Edit</a>"));
						if(Convert.ToInt32(dsDegreeList.Tables[0].Rows[intDSRow]["ProgCount"]) == 0){
							td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete " + strDegreeShortTitle.Replace("'","\\'") + "?')){document.getElementById('hidTodo').value='delete';document.getElementById('hidDegreeID').value='" + degreeID + "';document.frmDegree.submit();}\">Delete</a>"));
						}
						tr.Cells.Add(td);
						tblDisplay.Rows.Add(tr);
					}
				}else{
					tr = new TableRow();

					td = new TableCell();
					td.ColumnSpan = 4;
					td.Attributes["style"] = "width:100%;padding:4px;";
					td.CssClass = "portletLight";
					td.Controls.Add(new LiteralControl("No degrees currently exist."));

					tr.Cells.Add(td);
					tblDisplay.Rows.Add(tr);
				}
				panDisplay.Visible = true;

			}else if(hidTodo.Value == "edit"){

				cboTransDeg.Items.Clear();
				DataSet dsDegreeRequirementWorksheet = csProgram.GetDocumentTitles();
				Int32 intRecordCount = dsDegreeRequirementWorksheet.Tables[0].Rows.Count;
				if(intRecordCount > 0){
					for(Int32 intDSRow = 0; intDSRow < intRecordCount; intDSRow++){
						cboTransDeg.Items.Add(dsDegreeRequirementWorksheet.Tables[0].Rows[intDSRow]["DocumentTitle"].ToString());
					}
					chkTransDeg.Enabled = true;
				}else{
					cboTransDeg.Items.Add(new ListItem("No degree requirement documents currently exist.", ""));
					chkTransDeg.Checked = false;
					chkTransDeg.Enabled = false;
				}

				String strSelectedWkst = Request.Form["cboTransdeg"];
				DataSet dsDegree = csProgram.GetDegree(Convert.ToInt32(Request.Form["hidDegreeID"]));
				if(dsDegree.Tables[0].Rows.Count > 0){
					txtDegreeShortTitle.Text = dsDegree.Tables[0].Rows[0]["DegreeShortTitle"].ToString();
					txtDegreeLongTitle.Text = dsDegree.Tables[0].Rows[0]["DegreeLongTitle"].ToString();
					txtDegreeDescription.Value = dsDegree.Tables[0].Rows[0]["DegreeDescription"].ToString();
					String documentTitle = dsDegree.Tables[0].Rows[0]["DocumentTitle"].ToString();
					if(strSelectedWkst != null && strSelectedWkst != ""){
						cboTransDeg.SelectedValue = strSelectedWkst;
					}else if(documentTitle != null & documentTitle != ""){
						chkTransDeg.Checked = true;
						cboTransDeg.SelectedValue = documentTitle;
					}
					cboDegreeType.SelectedValue = dsDegree.Tables[0].Rows[0]["DegreeType"].ToString();
				}

				cboTransDegYear.Items.Clear();
				DataSet dsEffectiveYears = csProgram.GetEffectiveYears(cboTransDeg.SelectedValue);
				for(Int32 i = 0; i < dsEffectiveYears.Tables[0].Rows.Count; i++){
					cboTransDegYear.Items.Add(dsEffectiveYears.Tables[0].Rows[i]["EffectiveYear"].ToString());
				}

				try{
					cboTransDegYear.SelectedValue = Request.Form["cboTransDegYear"];
				}catch{
					//do nothing
				}

				if(cboTransDegYear.SelectedValue != ""){
					lblViewLink.Text = "&nbsp;<a href=\"#\" onclick=\"popupCenter('http://internal.spokane.edu/iCatalog/program/degworksheet/" + csProgram.GetDegreeRequirementWorksheetFileName(cboTransDegYear.SelectedValue, cboTransDeg.SelectedValue) + "','Requirements', 950, 950);\">View Requirements for the Selected Year</a>";
				}else{
					lblViewLink.Text = "";
				}
				
				panEdit.Visible = true;
				panDisplay.Visible = false;

			}else if(hidTodo.Value == "delete"){

				csProgram.DeleteDegree(Convert.ToInt32(hidDegreeID.Value));
				Response.Redirect("edit.aspx");

			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			String degreeShortTitle = Request.Form["txtDegreeShortTitle"], degreeLongTitle = Request.Form["txtDegreeLongTitle"], degreeDescription = Request.Form["txtDegreeDescription"];
			Int32 degreeID = Convert.ToInt32(Request.Form["hidDegreeID"]);
			bool blnIncludeDegWkst = false;
			if(Request.Form["chkTransDeg"] == "on"){
				blnIncludeDegWkst = true;
			}

			Int16 intSuccess = csProgram.EditDegree(degreeID, degreeShortTitle, degreeLongTitle, Request.Form["cboDegreeType"], blnIncludeDegWkst, Request.Form["cboTransDeg"], degreeDescription, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]);

			if(intSuccess == 1){
				Response.Redirect("edit.aspx");
			}else if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The degree entered already exists.";
				panError.Visible = true;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The update failed.";
				panError.Visible = true;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
