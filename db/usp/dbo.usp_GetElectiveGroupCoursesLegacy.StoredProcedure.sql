USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetElectiveGroupCoursesLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/12/17
-- Description:	Returns elective group courses
--		 INPUT: OptionElectiveGroupID - Elective Group primary key
--				BeginSTRM - Program begin term code
--				EndSTRM - Program end term code
--				SelectedSTRM - Selected term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetElectiveGroupCoursesLegacy] 
	@OptionElectiveGroupID int,
	@BeginSTRM varchar(4),
	@EndSTRM varchar(4),
	@SelectedSTRM varchar(4)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT OptionElectiveGroupID, ElectiveGroupCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseSuffix, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber, ElectiveGroupCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
    FROM ElectiveGroupCourse 
    INNER JOIN Course 
    ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering 
    AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999'))
    OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM))
    AND (CourseBeginSTRM <= @SelectedSTRM AND (CourseEndSTRM >= @SelectedSTRM OR CourseEndSTRM = 'Z999')) 
    LEFT OUTER JOIN OptionFootnote 
    ON OptionFootnote.OptionFootnoteID = ElectiveGroupCourse.OptionFootnoteID
    WHERE OptionElectiveGroupID = @OptionElectiveGroupID
    ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
END

GO
