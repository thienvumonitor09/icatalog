USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseDescriptionLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/12/17
-- Description:	Returns a course description
--		 INPUT: CourseID - Course primary key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseDescriptionLegacy]
	@CourseID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT COURSE_TITLE_LONG, DESCRLONG, UNITS_MINIMUM, UNITS_MAXIMUM, VariableUnits, CourseOffering, Course.SUBJECT, CATALOG_NBR, SubjectArea.DESCR AS SubjectArea_DESCR,
		(SELECT DESCRLONG 
         FROM Course 
         WHERE CourseID IN
		(SELECT DescriptionCourseID 
         FROM Course 
         WHERE CourseID = @CourseID)) AS AlternateDESCRLONG 
    FROM Course 
    INNER JOIN SubjectArea 
    ON Course.SUBJECT = SubjectArea.SUBJECT 
    WHERE CourseID = @CourseID;
END

GO
