USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionsByCategory]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionsByCategory]
	@STRM varchar(4),
	@LocationID int = NULL,
	@DegreeID int = NULL,
	@CategoryID int = NULL,
	@SUBJECT varchar(5) = NULL,
	@CATALOG_NBR varchar(3) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@local_STRM varchar(4) = @STRM,
		@local_LocationID int = @LocationID,
		@local_DegreeID int = @DegreeID,
		@local_CategoryID int = @CategoryID,
		@local_SUBJECT varchar(5) = @SUBJECT,
		@local_CATALOG_NBR varchar(3) = @CATALOG_NBR;

    ;WITH tmp (CourseOffering, SUBJECT, CATALOG_NBR, EFFDT, COURSE_TITLE_LONG, DESCR)
	AS 
	(
		SELECT ( CAST(REPLACE(SUBJECT,' ','') AS CHAR(5)) + CAST(REPLACE(CATALOG_NBR,' ','') AS CHAR(3)) ) AS CourseOffering
					, REPLACE(SUBJECT,' ',''), REPLACE(CATALOG_NBR,' ',''), CourseOffering.EFFDT , COURSE_TITLE_LONG, DESCR
		FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
		LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
		ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
		WHERE CATALOG_PRINT = 'Y' AND EFF_STATUS = 'A'
	)

	SELECT CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID
	FROM ProgramDegree 
	INNER JOIN Program 
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
	INNER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID
	INNER JOIN ProgramCategory 
	ON ProgramCategory.ProgramVersionID = Program.ProgramVersionID 
	INNER JOIN Category 
	ON Category.CategoryID = ProgramCategory.CategoryID 
	INNER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	INNER JOIN Degree 
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN tmp 
	ON tmp.CourseOffering = OptionPrerequisite.CourseOffering 
	LEFT OUTER JOIN OptionLocation 
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 
	WHERE ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM) 
		AND PublishedProgram = 1 
		AND
			(
				@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
			)
		AND
			(
				@local_DegreeID IS NULL OR OptionLocation.LocationID = @local_LocationID
			)
		AND 
			(
				@local_CategoryID IS NULL OR ProgramDegree.DegreeID = @local_DegreeID
			)
		AND 
			(
				@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
			)
		AND
			(
				@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			)
	UNION

	SELECT CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID
	FROM ProgramDegree 
	INNER JOIN Program 
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
	INNER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID
	INNER JOIN ProgramCategory 
	ON ProgramCategory.ProgramVersionID = Program.ProgramVersionID 
	INNER JOIN Category 
	ON Category.CategoryID = ProgramCategory.CategoryID 
	INNER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	INNER JOIN Degree 
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN tmp 
	ON tmp.CourseOffering = ElectiveGroupCourse.CourseOffering 
	LEFT OUTER JOIN OptionLocation 
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 
	WHERE ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM) 
		AND PublishedProgram = 1 
		AND
			(
				@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
			)
		AND
			(
				@local_DegreeID IS NULL OR OptionLocation.LocationID = @local_LocationID
			)
		AND 
			(
				@local_CategoryID IS NULL OR ProgramDegree.DegreeID = @local_DegreeID
			)
		AND 
			(
				@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
			)
		AND
			(
				@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			)	

	UNION

	SELECT CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID
	FROM ProgramDegree 
	INNER JOIN Program 
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
	INNER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID
	INNER JOIN ProgramCategory 
	ON ProgramCategory.ProgramVersionID = Program.ProgramVersionID 
	INNER JOIN Category 
	ON Category.CategoryID = ProgramCategory.CategoryID 
	INNER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	INNER JOIN Degree 
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN tmp 
	ON  tmp.CourseOffering = OptionCourse.CourseOffering 
	LEFT OUTER JOIN OptionLocation 
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 
	WHERE ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM) 
		AND PublishedProgram = 1 
		AND
			(
				@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
			)
		AND
			(
				@local_DegreeID IS NULL OR OptionLocation.LocationID = @local_LocationID
			)
		AND 
			(
				@local_CategoryID IS NULL OR ProgramDegree.DegreeID = @local_DegreeID
			)
		AND 
			(
				@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
			)
		AND
			(
				@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			)

	ORDER BY CategoryTitle, ProgramTitle, OptionTitle, DegreeShortTitle, CollegeShortTitle
END
GO
