﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="program_areaofstudy_edit" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
    <head>
        <title>Edit/Delete Area of Study</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
        <script type="text/javascript" src="../../_phatt3_src_files/trim.js"></script>
        <script type="text/javascript">
        <!--
            function validate() {
                var isValid = false;
                var cboPathway = document.getElementById("cboPathway");
                var txtAreaOfStudyTitle = document.getElementById("txtAreaOfStudyTitle");
                var optOfferedAt_0 = document.getElementById("optOfferedAt_0");
                var optOfferedAt_1 = document.getElementById("optOfferedAt_1");

                if (optOfferedAt_0.checked == false && optOfferedAt_1.checked == false) {
                    alert("Please select where the area of study is offered.");
                } else if (cboPathway.value == "") {
                    alert("Please select a primary guided pathway.");
                    cboPathway.focus();
                } else if (trim(txtAreaOfStudyTitle.value) == "") {
                    alert("Please enter an area of study title.");
                    txtAreaOfStudyTitle.select();
                } else {
                    isValid = true;
                }

                return isValid;
            }

            function addPathway(ID, pathwayID) {
                //add/remove area of study to/from a comma delimited string
                var pathwayList = document.getElementById("hidPathwayList");
                if (document.getElementById(ID).checked) {
                    if (pathwayList.value.length == 0) {
                        pathwayList.value += pathwayID;
                    } else {
                        pathwayList.value += "," + pathwayID;
                    }
                } else {
                    if (pathwayList.value.indexOf("," + pathwayID, 0) > 0) {
                        pathwayList.value = pathwayList.value.replace("," + pathwayID, "");
                    } else {
                        pathwayList.value = pathwayList.value.replace(pathwayID, "");
                    }
                }

                if (pathwayList.value.indexOf(",") == 0) {
                    pathwayList.value = pathwayList.value.substr(1, pathwayList.value.length);
                }
            }
        //-->
        </script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmAreaOfStudy" runat="server">
                    <input type="hidden" runat="server" id="hidTodo" />
                    <input type="hidden" runat="server" id="hidAreaOfStudyID" />
                    <input type="hidden" runat="server" id="hidCollegeChanged" />
                    <input type="hidden" runat="server" id="hidPathwayList" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Edit/Delete Area of Study</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <asp:panel runat="server" id="panDisplay">
                                                                        <asp:table runat="server" id="tblDisplay" style="width:100%;" cellspacing="1" cellpadding="2"></asp:table>
                                                                    </asp:panel>
                                                                    <asp:panel runat="server" id="panEdit">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <!-- Error Message -->
                                                                            <asp:panel runat="server" id="panError">
                                                                                <tr>
                                                                                    <td colspan="2" class="portletMain" style="color:red;padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                        <asp:label runat="server" id="lblErrorMsg"></asp:label>
                                                                                    </td>
                                                                                </tr>
                                                                            </asp:panel>
                                                                            <!-- Offered by -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Offered by</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:radiobuttonlist id="optOfferedAt" runat="server" repeatdirection="horizontal" AutoPostBack="true" onclick="document.getElementById('hidTodo').value='edit';document.getElementById('hidCollegeChanged').value='true'">
                                                                                        <asp:ListItem Value="WA171">SCC</asp:ListItem>
                                                                                        <asp:ListItem Value="WA172">SFCC</asp:ListItem>
                                                                                    </asp:radiobuttonlist>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Guided Pathway -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Primary Guided Pathway:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:DropDownList runat="server" ID="cboPathway" CssClass="small"></asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;vertical-align:top;padding-top:9px;">
                                                                                    <b>Additional Guided Pathways (optional):&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:panel id="panPathway" runat="server" style="width:100%;"></asp:panel>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Area of Study Title -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Area of Study Title:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtAreaOfStudyTitle" cssclass="small" maxlength="75" style="width:300px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Web Page URL -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Web Page URL:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtWebPageURL" cssclass="small" maxlength="150" style="width:450px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;width:100%;">
															                        <input type="button" id="cmdCancel" value="Cancel" onclick="location.href='edit.aspx';" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" /> 
                                                                                    &nbsp;<asp:button id="cmdSubmit" runat="server" text="Save Changes" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclientclick="return validate();" onclick="cmdSubmit_Click"></asp:button>
														                        </td>
													                        </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                    <asp:panel runat="server" id="panConfirm">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <tr>
                                                                                <td colspan="2" class="portletMain" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                    The following Area of Study information was successfully updated.
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Offered by -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Offered by</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:Label ID="lblOfferedBy" runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Guided Pathway -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Primary Guided Pathway:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:Label runat="server" ID="lblPathway"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <b>Additional Guided Pathways (optional):&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:Label id="lblAddPathways" runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Area of Study Title -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Area of Study Title:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:Label runat="server" id="lblAreaOfStudyTitle"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Web Page URL -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Web Page URL:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:Label runat="server" id="lblWebPageURL"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;width:100%;">
															                        <input type="button" id="cmdOK" value="OK" onclick="location.href = 'edit.aspx';" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" /> 
                                                                                </td>
													                        </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <DIV class="clearer"></DIV>
        </asp:panel>
    </body>
</html>





