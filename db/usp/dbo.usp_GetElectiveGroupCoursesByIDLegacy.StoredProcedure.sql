USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetElectiveGroupCoursesByIDLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/12/17
-- Description:	Returns elective group courses
--		 INPUT: OptionElectiveGroupID - Elective Group primary key
--				SelectedSTRM - Selected term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetElectiveGroupCoursesByIDLegacy] 
	@OptionElectiveGroupID int,
	@SelectedSTRM varchar (4) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @SelectedSTRM IS NULL
		SET @SelectedSTRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)

    SELECT ElectiveGroupCourse.OptionElectiveGroupID, ElectiveGroupCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseSuffix, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber, ElectiveGroupCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
    FROM ElectiveGroupCourse 
    INNER JOIN OptionElectiveGroup
    ON OptionElectiveGroup.OptionElectiveGroupID = ElectiveGroupCourse.OptionElectiveGroupID
    INNER JOIN ProgramDegreeOption
    ON ProgramDegreeOption.OptionID = OptionElectiveGroup.OptionID
    INNER JOIN ProgramDegree
    ON ProgramDegree.ProgramDegreeID = ProgramDegreeOption.ProgramDegreeID
    INNER JOIN Program 
    ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
    INNER JOIN Course 
    ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering 
    AND ((CourseBeginSTRM <= ProgramBeginSTRM AND (CourseEndSTRM >= ProgramBeginSTRM OR CourseEndSTRM = 'Z999')) 
    OR (CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM))
    AND (CourseBeginSTRM <= @SelectedSTRM AND (CourseEndSTRM >= @SelectedSTRM OR CourseEndSTRM = 'Z999')) 
    LEFT OUTER JOIN OptionFootnote 
    ON OptionFootnote.OptionFootnoteID = ElectiveGroupCourse.OptionFootnoteID
    WHERE ElectiveGroupCourse.OptionElectiveGroupID = @OptionElectiveGroupID
    ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
END
GO
