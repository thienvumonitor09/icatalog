<%@ Reference Page="~/program/view.aspx" %>
<%@ Page language="c#" Inherits="ICatalog.program.edit" ValidateRequest="false" CodeFile="edit.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Edit Program</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
		<link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
		<link rel="stylesheet" type="text/css" href="../_phatt3_css/CPG.css" />
		<script type="text/javascript" src="../_phatt3_src_files/trim.js"></script>
		<script type="text/javascript" src="../_phatt3_src_files/expand.js"></script>
		<script type="text/javascript" src="https://internal.spokane.edu/ckeditor_4/ckeditor.js"></script>
		<script type="text/javascript">
        <!-- 
			function validate(){
				var todo = document.getElementById("hidTodo");
				var hidProgramDisplay = document.getElementById("hidProgramDisplay");
				//var hidExistingDegrees = document.getElementById("hidExistingDegrees");
				//var hidDegreeList = document.getElementById("hidDegreeList");
				var blnCollegeSelected = false;

				if(todo.value == "add"){
					var txtProgramTitle1 = document.getElementById("txtProgramTitle1");
					var optProgramDisplay1_0 = document.getElementById("optProgramDisplay1_0");
					var optProgramDisplay1_1 = document.getElementById("optProgramDisplay1_1");
					var optOfferedAt = document.getElementsByName("optOfferedAt1");
					var txtProgramDescription1 = document.getElementById("txtProgramDescription1");
					var cboDegree1 = document.getElementById("cboDegree1");
                    var cboAreaOfStudy1 = document.getElementById("cboAreaOfStudy1");
					
					for(i = 0; i < optOfferedAt.length; i++){
						if(optOfferedAt[i].checked){
							blnCollegeSelected = true;
						}
					}
					
					if(trim(txtProgramTitle1.value) == ""){
						alert("Please enter the program title.");
						txtProgramTitle1.select();
						return false;
					}else if(blnCollegeSelected == false){
						alert("Please select the college where the program is offered.");
						return false;
		            } else if (cboDegree1.value == "") {
		                alert("Please select a completion award for the program.");
		                return false;
		            //} else if (cboAreaOfStudy1.value == "") {
		            //    alert("Please select a primary area of study.");
		            //    return false;
					}else if(optProgramDisplay1_0.checked == false && optProgramDisplay1_1.checked == false){
						alert("Please select a program display.");
						return false;
		            } else if (trim(CKEDITOR.instances.txtProgramDescription1.getData()) == "") {
						alert("Please enter the program description.");
						return false;
					}else{
						if(optProgramDisplay1_0.checked == true){
							hidProgramDisplay.value = "1";
						}else if(optProgramDisplay1_1.checked == true){
							hidProgramDisplay.value = "2";
						}
						return true;
					}
				}else if(todo.value == "editscreen"){
					var txtProgramTitle2 = document.getElementById("txtProgramTitle2");
					var optProgramDisplay2_0 = document.getElementById("optProgramDisplay2_0");
					var optProgramDisplay2_1 = document.getElementById("optProgramDisplay2_1");
					var optOfferedAt = document.getElementsByName("optOfferedAt2");
					var cboDegree2 = document.getElementById("cboDegree2");
					var txtProgramDescription2 = document.getElementById("txtProgramDescription2");
					var cboAreaOfStudy2 = document.getElementById("cboAreaOfStudy2");
					
					for(i = 0; i < optOfferedAt.length; i++){
						if(optOfferedAt[i].checked){
							blnCollegeSelected = true;
						}
					}
				
					if(trim(txtProgramTitle2.value) == ""){
						alert("Please enter the program title.");
						txtProgramTitle2.select();
						return false;
					}else if(blnCollegeSelected == false){
						alert("Please select the college where the program is offered.");
						return false;
					}else if(cboDegree2.value == ""){
						alert("Please select the completion award for the program.");
						return false;
                    //} else if (cboAreaOfStudy2.value == "") {
		            //    alert("Please select a primary area of study.");
		            //    return false;
					}else if(optProgramDisplay2_0.checked == false && optProgramDisplay2_1.checked == false){
						alert("Please select a program display.");
						return false;
		            } else if (trim(CKEDITOR.instances.txtProgramDescription2.getData()) == "") {
						alert("Please enter the program description.");
						return false;
					}else{
						if(optProgramDisplay2_0.checked == true){
							hidProgramDisplay.value = "1";
						}else if(optProgramDisplay2_1.checked == true){
							hidProgramDisplay.value = "2";
						}
						return true;
					}
				}else if(todo.value == "step2"){
					//valdate m-code fields
					var strOptionMCodeFields = document.getElementById('hidMCodeFields').value;
					var isValid = true;
					if(strOptionMCodeFields != null && strOptionMCodeFields != ""){
						var aryOptionMCodeFields = strOptionMCodeFields.substring(1).split("|");
						for(var i = 0; i < aryOptionMCodeFields.length; i++){
							var aryCollegeMCodeFields = aryOptionMCodeFields[i].split(";");
							var chkCampus = document.getElementById(aryCollegeMCodeFields[0]);
							var txtMCode = document.getElementById(aryCollegeMCodeFields[1]);
							if(chkCampus.checked && trim(txtMCode.value) == ""){
								alert("Please enter the " + chkCampus.nextSibling.innerHTML + " M-Code.");
								txtMCode.select();
								isValid = false;
							}						
						}
					}
					return isValid;
				}

			}

			function addAreaOfStudy(ID, areaOfStudyID) {
			    //add/remove area of study to/from a comma delimited string
			    var areaOfStudyList = document.getElementById("hidAreaOfStudyList");
			    if (document.getElementById(ID).checked) {
			        if (areaOfStudyList.value.length == 0) {
			            areaOfStudyList.value += areaOfStudyID;
			        } else {
			            areaOfStudyList.value += "," + areaOfStudyID;
			        }
			    } else {
			        if (areaOfStudyList.value.indexOf("," + areaOfStudyID, 0) > 0) {
			            areaOfStudyList.value = areaOfStudyList.value.replace("," + areaOfStudyID, "");
			        } else {
			            areaOfStudyList.value = areaOfStudyList.value.replace(areaOfStudyID, "");
			        }
			    }

			    if (areaOfStudyList.value.indexOf(",") == 0) {
			        areaOfStudyList.value = areaOfStudyList.value.substr(1, areaOfStudyList.value.length);
			    }
			    //alert("AreaOfStudyList = " + areaOfStudyList.value);
			}

			function addCategory(ID, categoryID){
				//add/remove category to/from a comma delimited string
				var CatList = document.getElementById("hidCategoryList");
				if(document.getElementById(ID).checked){
					if(CatList.value.length == 0){
						CatList.value += categoryID;
					}else{
						CatList.value += "," + categoryID;
					}
				}else{
					if(CatList.value.indexOf("," + categoryID, 0) > 0){
						CatList.value = CatList.value.replace("," + categoryID, "");
					}else{
						CatList.value = CatList.value.replace(categoryID, "");
					}
				}
				
				if(CatList.value.indexOf(",") == 0){
					CatList.value = CatList.value.substr(1, CatList.value.length);
				}
			}
			
			function addProgramDegree(ID, strDegreeID){
				//add/remove degree(s) to/from a comma delimited string to be added to the database in code behind
				var DegreeList = document.getElementById("hidDegreeList");
				if(document.getElementById(ID).checked){
					if(DegreeList.value.length == 0){
						DegreeList.value += strDegreeID;
					}else{
						DegreeList.value += "," + strDegreeID;
					}
				}else{	
					if(DegreeList.value.indexOf("," + strDegreeID, 0) > 0){
						DegreeList.value = DegreeList.value.replace("," + strDegreeID, "");
					}else{
						DegreeList.value = DegreeList.value.replace(strDegreeID, "");
					}
				}
				
				if(DegreeList.value.indexOf(",") == 0){
					DegreeList.value = DegreeList.value.substr(1, DegreeList.value.length);
				}
			}
			
			function deleteProgramDegree(ID, degreeShortTitle){
				var chkDegree = document.getElementById(ID);
				if(confirm("Program options and courses for the " + degreeShortTitle + " offered by this program will be deleted.\n" +
				"Are you sure you want to delete the " + degreeShortTitle + " for this program?")){
					document.frmProgram.hidTodo.value = "deleteDegree";
					document.frmProgram.hidDegreeID.value = chkDegree.value;
					document.frmProgram.submit();
				}else{
					chkDegree.checked = true;
				}
			}
			
			function checkStatus(id){
				var optPublishedProgram0 = document.getElementById(id + "_0");
				var optPublishedProgram1 = document.getElementById(id + "_1");
				var hidPublishedProgram = document.getElementById("hidPublishedProgram");
				
				if(optPublishedProgram0.checked == true){ //Working Copy is selected
					if(hidPublishedProgram.value != "Working Copy"){
						document.getElementById("hidTodo").value = "changeStatus";
						document.frmProgram.submit();
					}
				}else if(optPublishedProgram1.checked == true){ //Published Copy is selected
					if(hidPublishedProgram.value != "Published Copy"){
						document.getElementById("hidTodo").value = "changeStatus";
						document.frmProgram.submit();
					}
				}
			}
			
			function checkDisplay(id){
				var optProgramDisplay0 = document.getElementById(id + "_0");
				var optProgramDisplay1 = document.getElementById(id + "_1");
				var hidProgramDisplay = document.getElementById("hidProgramDisplay");
				
				if(optProgramDisplay0.checked == true){
					if(hidProgramDisplay.value != "1"){
						if(confirm("Are you sure you want to change the program display?\nAny existing text in the large text display will be deleted.")){
							document.getElementById("hidTodo").value = "changeProgramDisplay";
							//hidProgramDisplay.value = "1";
							document.frmProgram.submit();
						}else{
							optProgramDisplay1.checked = true;
						}
					}
				}else if(optProgramDisplay1.checked == true){
					if(hidProgramDisplay.value != "2"){
						if(document.getElementById("cboDegree2").value == ""){
							alert("Please select the completion award for the program.");
							optProgramDisplay0.checked = true;
						}else{
							if(confirm("Are you sure you want to change the program display?\nAny existing program options, prerequisites, courses, electives and footnotes will be deleted.")){
									
								document.getElementById("hidTodo").value = "changeProgramDisplay";
								//hidProgramDisplay.value = "2";
								document.frmProgram.submit();
									
							}else{
								optProgramDisplay0.checked = true;
							}
						}
					}
				}
			}
			
			function prepareRevision(checked){
				if(checked){
					//hide program title text box and show label
					document.getElementById("txtProgramTitle2").style.display = "none";
					document.getElementById("spanRevisionProgramTitle").style.display = "";
					document.frmProgram.chkRevision.disabled = true;
					
					var optOfferedAt = document.getElementsByName("optOfferedAt2"), CASN = "";
					for(i = 1; i < optOfferedAt.length; i++){
						if(optOfferedAt[i].checked){
							CASN = optOfferedAt[i].value;
						}
					}
					
					//show effective year quarter pop up
					popupCenter('popups/editterm.aspx?pvid=' + document.frmProgram.hidProgramVersionID.value + '&ppvid=' + document.frmProgram.hidPublishedProgramVersionID.value + '&pp=' + document.frmProgram.hidPublishedProgram.value + '&colid=' + document.frmProgram.hidCollegeID.value + '&colst=' + document.frmProgram.hidOfferedAt.value + '&pt=' + document.getElementById("spanRevisionProgramTitle").innerHTML.replace("'","\'") + '&bstrm=' + document.frmProgram.hidProgramBeginSTRM.value + '&estrm=' + document.frmProgram.hidProgramEndSTRM.value + '&pg=edit', 'EditTerm', 260, 460);
				}else{
					document.getElementById("txtProgramTitle2").style.display = "";
					document.getElementById("spanRevisionProgramTitle").style.display = "none";
				}
			}
			
            /*
			function launchCenter(url, name, height, width) {
				var str = "height=" + height + ",innerHeight=" + height;
				str += ",width=" + width + ",innerWidth=" + width;
				if (window.screen) {
					var ah = screen.availHeight - 30;
					var aw = screen.availWidth - 10;

					var xc = (aw - width) / 2;
					var yc = (ah - height) / 2;

					str += ",left=" + xc + ",screenX=" + xc;
					str += ",top=" + yc + ",screenY=" + yc;
					str += ",resizable=yes,scrollbars=yes";
				}
				return window.open(url, name, str);
			}
            */

			function popupCenter(url, title, h, w) {
			    // Fixes dual-screen position  
			    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
			    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

			    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
			    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

			    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
			    var top = ((height / 2) - (h / 2)) + dualScreenTop;
			    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

			    // Puts focus on the newWindow  
			    if (window.focus) {
			        newWindow.focus();
			    }
			}
		//-->
		</script>
	</head>
	<body>
		<asp:panel id="container" runat="server">
			<asp:panel id="header" runat="server">
				<uc1:header id="Header1" runat="server"></uc1:header>
			</asp:panel>
			<asp:panel id="sidemenu" runat="server">
				<uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
			</asp:panel>
			<asp:panel id="content" runat="server">
				<form id="frmProgram" runat="server">
					<input type="hidden" id="hidTodo" runat="server" /> 
					<input type="hidden" id="hidLtr" runat="server" />
					<input type="hidden" id="hidSelectedCollegeID" runat="server" />
					<input type="hidden" id="hidSTRM" runat="server" />
					<input type="hidden" id="hidProgramVersionID" runat="server" />
					<input type="hidden" id="hidCollegeID" runat="server" />
					<input type="hidden" id="hidProgramDegreeID" runat="server" /> 
					<input type="hidden" id="hidProgramTitle" runat="server" />
					<input type="hidden" id="hidOfferedAt" runat="server" />
					<input type="hidden" id="hidOptionPrerequisiteID" runat="server" />
					<input type="hidden" id="hidCourseID" runat="server" />
					<input type="hidden" id="hidOptionID" runat="server" />
					<input type="hidden" id="hidCourseOffering" runat="server" />
					<input type="hidden" id="hidQuarter" runat="server" />
					<input type="hidden" id="hidOptionElectiveGroupID" runat="server" />
					<input type="hidden" id="hidDegreeID" runat="server" />
                    <input type="hidden" id="hidExistingDegreeID" runat="server" />
					<input type="hidden" id="hidDegreeList" runat="server" />
					<input type="hidden" id="hidExistingDegrees" runat="server" />
					<input type="hidden" id="hidOptionFootnoteID" runat="server" />
					<input type="hidden" id="hidFootnoteNumber" runat="server" />
					<input type="hidden" id="hidProgramDisplay" runat="server" />
					<input type="hidden" id="hidMultipleOptions" runat="server" />
					<input type="hidden" id="hidDegreesRemoved" runat="server" />
					<input type="hidden" id="hidProgramBeginSTRM" runat="server" />
					<input type="hidden" id="hidProgramEndSTRM" runat="server" />
					<input type="hidden" id="hidPublishedProgram" runat="server" />
					<input type="hidden" id="hidError" runat="server" />
					<input type="hidden" id="hidPublishedProgramVersionID" runat="server" />
					<input type="hidden" id="hidProgramID" runat="server" />
                    <input type="hidden" id="hidAreaOfStudyList" runat="server" />
					<input type="hidden" id="hidCategoryList" runat="server" />
					<input type="hidden" id="hidExistingCategories" runat="server" />
					<input type="hidden" id="hidMCodeFields" runat="server" />
					<input type="hidden" id="hidLocationFields" runat="server" />
					<input type="hidden" id="hidSearchSubject" runat="server" />
					<input type="hidden" id="hidSearchId" runat="server" />
					<input type="hidden" id="hidSearchSTRM" runat="server" />
					<input type="hidden" id="hidSearchText" runat="server" />
                    <input type="hidden" id="hidBeginTerm" runat="server" />
                    <input type="hidden" id="hidEndTerm" runat="server" />
                    <input type="hidden" id="hidCollegeChanged" runat="server" />
					<table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
						<tr>
							<td style="padding-left:4px;padding-bottom:8px;padding-right:0px;">
								<table style="width:100%;" cellpadding="0" cellspacing="0">
									<tr>
										<td style="background-color:#000000;">
                                            <!-- ADD PROGRAM -->
											<asp:panel runat="server" id="panAdd">
												<table style="width:100%;" cellpadding="1" cellspacing="1">
													<tr>
														<td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
															class="portletHeader">
															<table style="width:100%;" cellpadding="0" cellspacing="0">
																<tr>
																	<td class="portletHeader" style="width:100%;">&nbsp;Program Maintenance: Step 1 Define Program</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="portletMain" style="width:100%">
															<table style="width:100%;" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="portletDark" style="width:100%;">
																		<table cellspacing="1" cellpadding="0" style="width:100%;">
																			<tr>
																				<td class="portletLight" style="width:100%;vertical-align:text-top;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<!-- Error Message -->
																						<asp:panel runat="server" id="panError1">
																							<tr>
																								<td style="color:red;padding-left:10px;padding-top:15px;">
																									<asp:label runat="server" id="lblErrorMsg1"></asp:label>
																								</td>
																							</tr>
																						</asp:panel>
																						<tr>
																							<td style="padding-bottom:15px;">
																								<table cellpadding="0" cellspacing="0" style="width:100%;">
																									<!-- PROGRAM TITLE -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Program Title:</b>&nbsp; <asp:textbox id="txtProgramTitle1" runat="server" columns="70" maxlength="80" cssclass="small"></asp:textbox>
																										</td>
																									</tr>
																									<!-- OFFERED BY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;">
																														<b>Offered by:</b>&nbsp;
																													</td>
																													<td>
																														<asp:radiobuttonlist id="optOfferedAt1" runat="server" repeatdirection="horizontal" AutoPostBack="true" onclick="document.getElementById('hidAreaOfStudyList').value=''"></asp:radiobuttonlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- TERM -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;">
																														<b>Term:</b>&nbsp;
																													</td>
																													<td style="padding-left:5px;">
																														<b>Begin</b>&nbsp;<asp:dropdownlist id="cboProgramBeginSTRM1" cssclass="small" runat="server"></asp:dropdownlist>&nbsp;&nbsp;&nbsp;
																														<b>End</b>&nbsp;<asp:dropdownlist id="cboProgramEndSTRM1" cssclass="small" runat="server"></asp:dropdownlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- STATUS -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;">
																														<b>Status:</b>&nbsp;
																													</td>
																													<td>
																														<asp:radiobuttonlist id="optPublishedProgram1" runat="server" repeatdirection="horizontal">
																															<asp:listitem Value="0" selected="true">Working Copy</asp:listitem>
																															<asp:listitem Value="1">Published Copy</asp:listitem>
																														</asp:radiobuttonlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
                                                                                                    <!-- COMPLETION AWARD -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;padding-bottom:3px;vertical-align:top;">
																														<b>Completion Award:</b>&nbsp;
																													</td>
                                                                                                                    <td>
                                                                                                                        <asp:DropDownList ID="cboDegree1" runat="server" CssClass="small"></asp:DropDownList>
                                                                                                                    </td>
																												</tr>
                                                                                                                <!--
																												<tr>
																													<td>
																														<asp:panel id="panDegree1" runat="server"></asp:panel>
																													</td>
																												</tr>
                                                                                                                -->
																											</table>
																										</td>
																									</tr>
                                                                                                    <!-- PRIMARY AREA OF STUDY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;">
																														<b>Primary Area of Study:</b>&nbsp;&nbsp;
																													</td>
																													<td>
																														<asp:dropdownlist id="cboAreaOfStudy1" runat="server" cssclass="small"></asp:dropdownlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- ADDITIONAL AREAS OF STUDY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0" style="width:100%;">
																												<tr>
																													<td style="padding-top:3px;padding-bottom:3px;vertical-align:top;">
																														<b>Additional Areas of Study (optional):</b>&nbsp;
																													</td>
																												</tr>
																												<tr>
																													<td style="width:100%;">
																														<asp:panel id="panAreasOfStudy1" runat="server" style="width:100%;"></asp:panel>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- CATEGORY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;">
																														<b>Primary Category:</b>&nbsp;&nbsp;
																													</td>
																													<td>
																														<asp:dropdownlist id="cboCategory1" runat="server" cssclass="small"></asp:dropdownlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- CATEGORIES -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0" style="width:100%;">
																												<tr>
																													<td style="padding-top:3px;padding-bottom:3px;vertical-align:top;">
																														<b>Additional Categories (optional):</b>&nbsp;
																													</td>
																												</tr>
																												<tr>
																													<td style="width:100%;">
																														<asp:panel id="panCategories1" runat="server" style="width:100%;"></asp:panel>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- PROGRAM DISPLAY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:6px;vertical-align:top;">
																														<b>Program Display:</b>&nbsp;
																													</td>
																													<td>
																														<asp:radiobuttonlist id="optProgramDisplay1" runat="server">
																															<asp:listitem value="1">Program Outline</asp:listitem>
																															<asp:listitem value="2">Large Text Display</asp:listitem>
																														</asp:radiobuttonlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- PROGRAM DESCRIPTION -->
																									<tr>
																										<td style="width:100%;padding-top:15px;padding-left:5px;padding-right:5px;">
																											<div style="padding-bottom:2px;"><strong>Program Description</strong></div>
																											<textarea name="txtProgramDescription" id="txtProgramDescription1" runat="server" class="defaultText" cols="10" rows="5"></textarea>
																											<script type="text/javascript">
																											    CKEDITOR.replace('txtProgramDescription1',
																												{
																												    customConfig: '/iCatalog/ckeditor_config.js',
																												    toolbar:
																													[
																														{ name: 'basicstyles', items: ['Bold', '-',
																														'BulletedList', '-',
																														'SpecialChar', '-',
                                                                                                                        'Link', 'Unlink', '-',
																														'SpellChecker', 'Scayt']
																														}
																													],
																												    height: "270px"
																												});
																											</script>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="portletMain" style="text-align:center;padding:3px;">
															<asp:button id="cmdCancel1" runat="server" text="Cancel" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:100px;"></asp:button>
															&nbsp;<asp:button id="cmdNext1" runat="server" text="Next" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:100px;"></asp:button>
														</td>
													</tr>
												</table>
											</asp:panel>
										
											<!-- PANEL 1 - SELECT PROGRAM TO ADD, EDIT OR DELETE -->
											<asp:panel runat="server" id="panSelect">
												<table style="width:100%;" cellpadding="1" cellspacing="1">
													<tr>
														<td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;" class="portletHeader">
															<table style="width:100%;" cellpadding="0" cellspacing="0">
																<tr>
																	<td class="portletHeader" style="width:100%;">&nbsp;Program Maintenance: Select Program to Edit/Delete</td>
																</tr>
															</table>
														</td>
													</tr>
													<!-- DISPLAY PROGRAM LIST -->
													<tr>
														<td class="portletMain" style="width:100%">
															<table style="width:100%;" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="portletDark" style="width:100%;">
                                                                        <asp:Table runat="server" ID="tblSearch" style="width:100%;" cellspacing="0" cellpadding="0">
                                                                            <asp:tablerow>
																				<asp:tablecell style="width:100%;font-weight:bold;padding:10px;" cssclass="portletMain">
                                                                                    <!--View by:&nbsp;
                                                                                    <asp:DropDownList ID="cboView" CssClass="small" runat="server" AutoPostBack="true">
                                                                                        <asp:ListItem Value="areaOfStudy">Area of Study A-Z</asp:ListItem>
                                                                                        <asp:ListItem Value="degree">Degree/Certificate</asp:ListItem>
                                                                                        <asp:ListItem Value="category">Category</asp:ListItem>
                                                                                    </asp:DropDownList>&nbsp;&nbsp;-->
																					Offered by:&nbsp;
																					<asp:dropdownlist id="cboOfferedAt" cssclass="small" runat="server" autopostback="true">
																						<asp:listitem value="">ALL</asp:listitem>
																					</asp:dropdownlist>&nbsp;&nbsp;
																					Term:&nbsp;
																					<asp:dropdownlist id="cboTerm" cssclass="small" runat="server" autopostback="true">
																						<asp:listitem value="">ALL</asp:listitem>
																					</asp:dropdownlist>
																				</asp:tablecell>
																			</asp:tablerow>
                                                                        </asp:Table>
																		<asp:table runat="server" id="tblAlpha" style="width:100%;" cellspacing="0" cellpadding="0"></asp:table>
																		<asp:table runat="server" id="tblDisplay" style="width:100%;" cellspacing="1" cellpadding="2"></asp:table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</asp:panel>
											
											<!-- EDIT PANEL - EDIT WHERE PROGRAM IS OFFERED, COMPLETION AWARDS AND THE PROGRAM DISPLAY -->
											<asp:panel runat="server" id="panEdit">
												<table style="width:100%;" cellpadding="1" cellspacing="1">
													<tr>
														<td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;" class="portletHeader">
															<table style="width:100%;" cellpadding="0" cellspacing="0">
																<tr>
																	<td class="portletHeader" style="width:100%;">&nbsp;Program Maintenance: Step 1 Define Program</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="portletMain" style="width:100%">
															<table style="width:100%;" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="portletDark" style="width:100%;">
																		<table cellspacing="1" cellpadding="0" style="width:100%;">
																			<!-- Error Message -->
																			<asp:panel runat="server" id="panError2">
																				<tr>
																					<td class="portletMain" style="color:red;padding:10px;padding-top:8px;">
																						<asp:label runat="server" id="lblErrorMsg2"></asp:label>
																					</td>
																				</tr>
																			</asp:panel>
																			<tr>
																				<td class="portletMain" style="padding:6px;padding-top:5px;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<tr>
																							<td style="width:70%;">
																								<b>&nbsp;Last Modified by:</b>&nbsp;<asp:label runat="server" id="lblProgModSID"></asp:label>&nbsp;<b>on:</b>&nbsp;<asp:label runat="server" id="lblProgModTS"></asp:label>
																							</td>
																							<td style="width:30%;text-align:right;padding-right:2px;">
																								<input type="checkbox" id="chkRevision" runat="server" onclick="prepareRevision(this.checked);" />&nbsp;<b>Program Revision</b>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<td class="portletLight" style="width:100%;vertical-align:text-top;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<tr>
																							<td style="padding-bottom:15px;">
																								<table cellpadding="0" cellspacing="0" style="width:100%;">
																									<!-- PROGRAM TITLE -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellspacing="0" cellpadding="0" style="width:100%;">
																												<tr>
																													<td style="width:100%;">
																														<b>Program Title:</b>&nbsp;
																														<asp:textbox id="txtProgramTitle2" runat="server" columns="75" maxlength="80" cssclass="small"></asp:textbox>
																														<span id="spanRevisionProgramTitle" runat="server"></span>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- OFFERED AT -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;">
																														<b>Offered by:</b>&nbsp;
																													</td>
																													<td>
																														<asp:radiobuttonlist id="optOfferedAt2" runat="server" repeatdirection="horizontal" AutoPostBack="true" onclick="document.getElementById('hidAreaOfStudyList').value='';document.getElementById('hidCollegeChanged').value='true';"></asp:radiobuttonlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- TERM -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;">
																														<b>Term:</b>&nbsp;
																													</td>
																													<td style="padding-left:5px;">
																														<b>Begin</b>&nbsp;<asp:dropdownlist id="cboProgramBeginSTRM2" cssclass="small" runat="server"></asp:dropdownlist>&nbsp;&nbsp;&nbsp;
																														<b>End</b>&nbsp;<asp:dropdownlist id="cboProgramEndSTRM2" cssclass="small" runat="server"></asp:dropdownlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- STATUS -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;">
																														<b>Status:</b>&nbsp;
																													</td>
																													<td>
																														<asp:radiobuttonlist id="optPublishedProgram2" runat="server" repeatdirection="horizontal">
																															<asp:listitem Value="0" selected="true">Working Copy</asp:listitem>
																															<asp:listitem Value="1">Published Copy</asp:listitem>
																														</asp:radiobuttonlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
                                                                                                    <!-- COMPLETION AWARD -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;padding-bottom:3px;vertical-align:top;">
																														<b>Completion Award:</b>&nbsp;
																													</td>
                                                                                                                    <td>
                                                                                                                        <asp:DropDownList ID="cboDegree2" runat="server" CssClass="small"></asp:DropDownList>
                                                                                                                    </td>
																												</tr>
                                                                                                                <!--
																												<tr>
																													<td>
																														<asp:panel id="panDegree2" runat="server"></asp:panel>
																													</td>
																												</tr>
                                                                                                                -->
																											</table>
																										</td>
																									</tr>
                                                                                                    <!-- PRIMARY AREA OF STUDY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;">
																														<b>Primary Area of Study:</b>&nbsp;&nbsp;
																													</td>
																													<td>
																														<asp:dropdownlist id="cboAreaOfStudy2" runat="server" cssclass="small"></asp:dropdownlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- ADDITIONAL AREAS OF STUDY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0" style="width:100%;">
																												<tr>
																													<td style="padding-top:3px;padding-bottom:3px;vertical-align:top;">
																														<b>Additional Areas of Study (optional):</b>&nbsp;
																													</td>
																												</tr>
																												<tr>
																													<td style="width:100%;">
																														<asp:panel id="panAreasOfStudy2" runat="server" style="width:100%;"></asp:panel>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- CATEGORY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:3px;">
																														<b>Primary Category:</b>&nbsp;&nbsp;
																													</td>
																													<td>
																														<asp:dropdownlist id="cboCategory2" runat="server" cssclass="small"></asp:dropdownlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- CATEGORIES -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0" style="width:100%">
																												<tr>
																													<td style="padding-top:3px;padding-bottom:3px;vertical-align:top;">
																														<b>Additional Categories (optional):</b>&nbsp;
																													</td>
																												</tr>
																												<tr>
																													<td>
																														<asp:panel id="panCategories2" runat="server"></asp:panel>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- PROGRAM DISPLAY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<table cellpadding="0" cellspacing="0">
																												<tr>
																													<td style="padding-top:6px;vertical-align:top;">
																														<b>Program Display:</b>&nbsp;
																													</td>
																													<td>
																														<asp:radiobuttonlist id="optProgramDisplay2" runat="server">
																															<asp:listitem value="1">Program Outline</asp:listitem>
																															<asp:listitem value="2">Large Text Display</asp:listitem>
																														</asp:radiobuttonlist>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<!-- PROGRAM DESCRIPTION -->
																									<tr>
																										<td style="width:100%;padding-top:15px;padding-left:5px;padding-right:5px;">
																											<div style="padding-bottom:2px;"><strong>Program Description</strong></div>
																											<textarea name="txtProgramDescription2" id="txtProgramDescription2" runat="server" class="defaultText" cols="10" rows="5"></textarea>
																											<script type="text/javascript">
																											    CKEDITOR.replace('txtProgramDescription2',
																												{
																												    customConfig: '/iCatalog/ckeditor_config.js',
                                                                                                                    allowedContent: 'span{!background-color}; strong; ul ol li; a[!href, target]',
																												    toolbar:
																													[
																														{ name: 'basicstyles', items: ['Bold', '-',
																														'BulletedList', '-',
																														'SpecialChar', '-',
                                                                                                                        'Link', 'Unlink', '-',
																														'SpellChecker', 'Scayt']
																														}
																													],
																												    height: "270px"
																												});
																											</script>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<!-- PANEL 2 BUTTONS -->
																<tr>
																	<td class="portletMain" style="text-align:center;padding:3px;">
																		<asp:button id="cmdCancel2" runat="server" text="Cancel" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:100px;"></asp:button>
																		&nbsp;<asp:button id="cmdNext2" runat="server" text="Next" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:100px;"></asp:button>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</asp:panel>
											
											<!-- PANEL 3 - ADD, EDIT, DELETE PROGRAM OUTLINE -->
											<asp:panel id="panStep2" runat="server">
												<table style="width:100%;" cellpadding="1" cellspacing="1">
													<tr>
														<td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
															class="portletHeader">
															<table style="width:100%;" cellpadding="0" cellspacing="0">
																<tr>
																	<td class="portletHeader" style="width:100%;">&nbsp;Program Maintenance: Step 2 Create 
																		Program Option for
																		<asp:label id="lblSelectedDeg" runat="server"></asp:label></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="portletMain" style="width:100%">
															<table style="width:100%;" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="portletDark" style="width:100%;">
																		<table cellspacing="1" cellpadding="0" style="width:100%;">
																			<tr>
																				<td class="portletLight" style="width:100%;vertical-align:text-top;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<tr>
																							<td style="padding-bottom:15px;">
																								<table cellpadding="0" cellspacing="0" style="width:100%;">
																									<!-- DISPLAY PROGRAM TITLE -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Program Title:</b>&nbsp;
																											<asp:label id="lblProgramTitle" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- DISPLAY OFFERED AT -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Offered by:</b>&nbsp;
																											<asp:label id="lblOfferedAt" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- EFFECTIVE YEAR QUARTER -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Term:</b>&nbsp;&nbsp;&nbsp;
																											<b>Begin</b>&nbsp;&nbsp;<asp:label runat="server" id="lblProgramBeginSTRM"></asp:label>
																											&nbsp;&nbsp;&nbsp;<b>End</b>&nbsp;&nbsp;<asp:label runat="server" id="lblProgramEndSTRM"></asp:label>
																										</td>
																									</tr>
																									<!-- STATUS -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Status:</b>&nbsp;
																											<asp:label id="lblPublishedProgram" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
                                                                                                    <!-- COMPLETION AWARD SELECTION -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Completion Award:</b>&nbsp;
																											<asp:label id="lblDegree" runat="server" cssclass="small"></asp:label>&nbsp;
																											<asp:label id="lblDegreeRequirementWorksheetLink" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
                                                                                                    <!-- PRIMARY AREA OF STUDY -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Primary Area of Study:</b>&nbsp;
																											<asp:label id="lblAreaOfStudy" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- ADDITIONAL AREAS OF STUDY -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Additional Areas of Study:</b>&nbsp;
																											<asp:label ID="lblAddAreasOfStudy" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- PRIMARY CATEGORY -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Primary Category:</b>&nbsp;
																											<asp:label id="lblCategory" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- ADDITIONAL CATEGORIES -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Additional Categories:</b>&nbsp;
																											<asp:label ID="lblAddCategories" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<asp:panel id="panProgOutline" runat="server">
																										<!-- PROGRAM OPTION SELECTION -->
																										<tr>
																											<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;">
																												<table cellpadding="0" cellspacing="0" style="width:100%;">
																													<tr>
																														<td>
																															<b>Option Title:</b>&nbsp;
																															<asp:dropdownlist id="cboOptionTitle" runat="server" cssclass="small" autopostback="true"></asp:dropdownlist>
																															<span id="spanOptLnks" runat="server">
																															&nbsp;<a href="#" onclick="popupCenter('popups/editoption.aspx?pvid=' + document.frmProgram.hidProgramVersionID.value + '&pdid=' + document.frmProgram.hidProgramDegreeID.value + '&did=' + document.frmProgram.hidDegreeID.value + '&ltr=' + document.frmProgram.hidLtr.value + '&strm=' + document.frmProgram.hidSTRM.value + '&scolid=' + document.frmProgram.hidSelectedCollegeID.value + '&oa=' + document.frmProgram.hidOfferedAt.value + '&pt=' + document.frmProgram.hidProgramTitle.value + '&oid=' + document.frmProgram.hidOptionID.value + '&pd=' + document.frmProgram.hidProgramDisplay.value + '&bstrm=' + document.frmProgram.hidProgramBeginSTRM.value + '&estrm=' + document.frmProgram.hidProgramEndSTRM.value + '&pp=' + document.frmProgram.hidPublishedProgram.value + '&ppvid=' + document.frmProgram.hidPublishedProgramVersionID.value + '&id=' + document.frmProgram.hidProgramID.value + '&colid=' + document.frmProgram.hidCollegeID.value + '&searchsub=' + document.frmProgram.hidSearchSubject.value.replace('&','%26') + '&searchid=' + document.frmProgram.hidSearchId.value.replace('&','%26') + '&searchstrm=' + document.frmProgram.hidSearchSTRM.value + '&searchtxt=' + document.frmProgram.hidSearchText.value.replace('&','%26'), 'EditOptTitle', 220, 325);">Edit</a>
                                                                                                                            <span id="spanOptDeleteLnk" runat="server">|&nbsp;<a href="#" onclick="if(confirm('Are you sure you want to delete the option \'' + document.frmProgram.cboOptionTitle[document.frmProgram.cboOptionTitle.selectedIndex].text + '\' and all of its courses?')){document.frmProgram.hidTodo.value='deleteOption';document.frmProgram.submit();}return false;">Delete</a></span>
																															</span>
																														</td>
																														<td style="width:115px;">
																															<input type="button" id="cmdAddOpt" runat="server" value="Add New Option" class="small" style="background:#ccccaa;border:1px solid #000000;width:115px;" 
																															onclick="popupCenter('popups/addoption.aspx?pvid=' + document.frmProgram.hidProgramVersionID.value + '&pdid=' + document.frmProgram.hidProgramDegreeID.value + '&did=' + document.frmProgram.hidDegreeID.value + '&ltr=' + document.frmProgram.hidLtr.value + '&strm=' + document.frmProgram.hidSTRM.value + '&scolid=' + document.frmProgram.hidSelectedCollegeID.value + '&oa=' + document.frmProgram.hidOfferedAt.value + '&pt=' + document.frmProgram.hidProgramTitle.value + '&oid=' + document.frmProgram.hidOptionID.value + '&pd=' + document.frmProgram.hidProgramDisplay.value + '&mo=' + document.frmProgram.hidMultipleOptions.value + '&bstrm=' + document.frmProgram.hidProgramBeginSTRM.value + '&estrm=' + document.frmProgram.hidProgramEndSTRM.value + '&pp=' + document.frmProgram.hidPublishedProgram.value + '&ppvid=' + document.frmProgram.hidPublishedProgramVersionID.value + '&id=' + document.frmProgram.hidProgramID.value + '&colid=' + document.frmProgram.hidCollegeID.value + '&searchsub=' + document.frmProgram.hidSearchSubject.value.replace('&','%26') + '&searchid=' + document.frmProgram.hidSearchId.value.replace('&','%26') + '&searchstrm=' + document.frmProgram.hidSearchSTRM.value + '&searchtxt=' + document.frmProgram.hidSearchText.value.replace('&','%26'), 'AddProgOption', 220, 325);" />
																														</td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																										<!-- STATE APPROVAL DATE -->
																										<tr>
																											<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																												<b>State Approval Date:</b>&nbsp;
																												<asp:textbox id="txtOptionStateApproval" runat="server" Columns="20" maxlength="20" cssclass="small"></asp:textbox>
																											</td>
																										</tr>
																										<!-- CIP -->
																										<tr>
																											<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																												<b>CIP:</b>&nbsp;
																												<asp:textbox id="txtCIP" runat="server" Columns="10" maxlength="8" cssclass="small"></asp:textbox>
																												<asp:CheckBox ID="chkPrimary" Runat="server"></asp:CheckBox><b>Primary</b>	
																											</td>
																										</tr>
																										<!-- EPC -->
																										<tr>
																											<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																												<b>EPC:</b>&nbsp;
																												<asp:textbox id="txtEPC" runat="server" Columns="10" maxlength="4" cssclass="small"></asp:textbox>
																											</td>
																										</tr>
                                                                                                        <!-- ACADEMIC PLAN -->
                                                                                                        <tr>
																											<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																												<b>Academic Plan:</b>&nbsp;
																												<asp:textbox id="txtAcademicPlan" runat="server" Columns="10" maxlength="10" cssclass="small"></asp:textbox>
																											</td>
																										</tr>
																										<!-- GAINFUL EMPLOYMENT ID -->
																										<tr>
																											<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																												<b>Gainful Employment CourseID:</b>&nbsp;
																												<asp:TextBox ID="txtGainfulEmploymentID" Runat="server" Columns="8" MaxLength="6" CssClass="small"></asp:TextBox>
																											</td>
																										</tr>
                                                                                                        <!-- TOTAL QUARTERS -->
                                                                                                        <tr>
                                                                                                            <td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																												<b>Total Quarters:</b>&nbsp;
																												<asp:TextBox ID="txtTotalQuarters" Runat="server" Columns="8" MaxLength="6" CssClass="small"></asp:TextBox>
																											</td>
                                                                                                        </tr>
																										<!-- OPTION LOCATIONS -->
																										<tr>
																											<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;padding-bottom:5px;width:100%;">
																												<b>Offered at:</b>
																											</td>
																										</tr>
																										<tr>
																											<td id="locationColumn" runat="server" colspan="2" style="padding-left:30px;padding-right:5px;width:100%;"></td>
																										</tr>
																										<!-- CLASSES OFFERED ON ANOTHER CAMPUS; INTENT CODE OF M -->
																										<asp:Panel ID="panMCode" Runat="server">
																											<tr>
																												<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																													<asp:Label ID="lblMCodeDesc" Runat="server"></asp:Label>
																												</td>
																											</tr>
																											<tr>
																												<td id="mcodeColumn" runat="server" colspan="2" style="padding-left:32px;padding-right:5px;padding-top:5px;width:100%;"></td>
																											</tr>
																										</asp:Panel>
																										<!-- OPTION DESCRIPTION -->
																										<tr>
																											<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																												<b>Option Description (optional):</b>&nbsp;
																											</td>
																										</tr>
																										<tr>
																											<td colspan="2" style="padding-left:10px;padding-right:5px;padding-bottom:15px;padding-top:3px;width:100%;">
																												<textarea runat="server" id="txtOptionDescription" class="defaultText" cols="10" rows="5"></textarea>
																												<script type="text/javascript">
																												    CKEDITOR.replace('txtOptionDescription',
																												{
																												    customConfig: '/iCatalog/ckeditor_config.js',
																												    allowedContent: 'span{!background-color}; strong; ul ol li; a[!href, target]',
																												    toolbar:
																													[
																														{ name: 'basicstyles', items: ['Bold', '-',
																														'BulletedList', '-',
																														'SpecialChar', '-',
                                                                                                                        'Link', 'Unlink', '-',
																														'SpellChecker', 'Scayt']
																														}
																													],
																												    height: "100px",
																												    width: "690px"
																												});
																											</script>
																											</td>
																										</tr>
																										<!-- DEGREE PREREQUISITES -->
																										<tr>
																											<td class="portletMain" style="padding-left:5px;padding-top:4px;padding-bottom:4px;width:50%;">
																												<b>Option Prerequisites:</b>
																											</td>
																											<td class="portletMain" style="padding-top:4px;padding-bottom:4px;padding-right:5px;width:50%;text-align:right;">
																												<input type="button" id="cmdAddToPrereqs" value="Add Prerequisites" class="small" style="background:#ccccaa;border:1px solid #000000;width:115px;"
																													onclick="popupCenter('popups/addprereq.aspx?pvid=' + document.frmProgram.hidProgramVersionID.value + '&pdid=' + document.frmProgram.hidProgramDegreeID.value + '&did=' + document.frmProgram.hidDegreeID.value + '&ltr=' + document.frmProgram.hidLtr.value + '&strm=' + document.frmProgram.hidSTRM.value + '&scolid=' + document.frmProgram.hidSelectedCollegeID.value + '&oa=' + document.frmProgram.hidOfferedAt.value + '&pt=' + document.frmProgram.hidProgramTitle.value + '&oid=' + document.frmProgram.hidOptionID.value + '&pd=' + document.frmProgram.hidProgramDisplay.value + '&bstrm=' + document.frmProgram.hidProgramBeginSTRM.value + '&estrm=' + document.frmProgram.hidProgramEndSTRM.value + '&pp=' + document.frmProgram.hidPublishedProgram.value + '&ppvid=' + document.frmProgram.hidPublishedProgramVersionID.value + '&id=' + document.frmProgram.hidProgramID.value + '&colid=' + document.frmProgram.hidCollegeID.value + '&searchsub=' + document.frmProgram.hidSearchSubject.value.replace('&','%26') + '&searchid=' + document.frmProgram.hidSearchId.value.replace('&','%26') + '&searchstrm=' + document.frmProgram.hidSearchSTRM.value + '&searchtxt=' + document.frmProgram.hidSearchText.value.replace('&','%26'), 'AddDegreePrereq', 355, 325);" />
																											</td>
																										</tr>
																										<tr>
																											<td colspan="2" style="padding-left:5px;padding-right:5px;padding-top:15px;padding-bottom:15px;width:100%;">
																												<asp:table id="tblDegPrereqs" runat="server" style="width:100%;"></asp:table>
																											</td>
																										</tr>
																										<!-- OPTION COURSES -->
																										<tr>
																											<td class="portletMain" style="padding-left:5px;padding-top:4px;padding-bottom:4px;width:50%;">
																												<b>Option Courses:</b>
																											</td>
																											<td class="portletMain" style="padding-right:5px;padding-top:4px;padding-bottom:4px;width:50%;text-align:right;">
																												<input type="button" id="cmdAddOptCourses" value="Add Option Courses" class="small" style="background:#ccccaa;border:1px solid #000000;width:115px;" 
																												onclick="popupCenter('popups/addoptcrs.aspx?pvid=' + document.frmProgram.hidProgramVersionID.value + '&pdid=' + document.frmProgram.hidProgramDegreeID.value + '&did=' + document.frmProgram.hidDegreeID.value + '&ltr=' + document.frmProgram.hidLtr.value + '&strm=' + document.frmProgram.hidSTRM.value + '&scolid=' + document.frmProgram.hidSelectedCollegeID.value + '&oa=' + document.frmProgram.hidOfferedAt.value + '&pt=' + document.frmProgram.hidProgramTitle.value + '&oid=' + document.frmProgram.hidOptionID.value + '&pd=' + document.frmProgram.hidProgramDisplay.value + '&bstrm=' + document.frmProgram.hidProgramBeginSTRM.value + '&estrm=' + document.frmProgram.hidProgramEndSTRM.value + '&pp=' + document.frmProgram.hidPublishedProgram.value + '&ppvid=' + document.frmProgram.hidPublishedProgramVersionID.value + '&id=' + document.frmProgram.hidProgramID.value + '&colid=' + document.frmProgram.hidCollegeID.value + '&searchsub=' + document.frmProgram.hidSearchSubject.value.replace('&','%26') + '&searchid=' + document.frmProgram.hidSearchId.value.replace('&','%26') + '&searchstrm=' + document.frmProgram.hidSearchSTRM.value + '&searchtxt=' + document.frmProgram.hidSearchText.value.replace('&','%26'), 'AddOptCrs', 460, 325);" />																										
																											</td>
																										</tr>
																										<tr>
																											<td colspan="2" style="padding-left:5px;padding-right:5px;padding-top:15px;padding-bottom:15px;width:100%;">
																												<asp:table id="tblOptCourses" runat="server" style="width:100%;"></asp:table>
																											</td>
																										</tr>
																										<!-- PROGRAM ELECTIVES -->
																										<tr>
																											<td class="portletMain" style="padding-left:5px;padding-top:4px;padding-bottom:4px;width:50%;">
																												<b>Option Electives:</b>
																											</td>
																											<td class="portletMain" style="padding-right:5px;padding-top:4px;padding-bottom:4px;width:50%;text-align:right;">
																												<input type="button" id="cmdAddElectives" value="Add Electives" class="small" style="background:#ccccaa;border:1px solid #000000;width:115px;" 
																												onclick="popupCenter('popups/addelective.aspx?pvid=' + document.frmProgram.hidProgramVersionID.value + '&pdid=' + document.frmProgram.hidProgramDegreeID.value + '&did=' + document.frmProgram.hidDegreeID.value + '&ltr=' + document.frmProgram.hidLtr.value + '&strm=' + document.frmProgram.hidSTRM.value + '&scolid=' + document.frmProgram.hidSelectedCollegeID.value + '&oa=' + document.frmProgram.hidOfferedAt.value + '&pt=' + document.frmProgram.hidProgramTitle.value + '&oid=' + document.frmProgram.hidOptionID.value + '&pd=' + document.frmProgram.hidProgramDisplay.value + '&bstrm=' + document.frmProgram.hidProgramBeginSTRM.value + '&estrm=' + document.frmProgram.hidProgramEndSTRM.value + '&pp=' + document.frmProgram.hidPublishedProgram.value + '&ppvid=' + document.frmProgram.hidPublishedProgramVersionID.value + '&id=' + document.frmProgram.hidProgramID.value + '&colid=' + document.frmProgram.hidCollegeID.value + '&searchsub=' + document.frmProgram.hidSearchSubject.value.replace('&','%26') + '&searchid=' + document.frmProgram.hidSearchId.value.replace('&','%26') + '&searchstrm=' + document.frmProgram.hidSearchSTRM.value + '&searchtxt=' + document.frmProgram.hidSearchText.value.replace('&','%26'), 'AddProgElect', 440, 420);" />
																											</td>
																										</tr>
																										<tr>
																											<td colspan="2" style="padding-left:5px;padding-right:5px;padding-top:15px;padding-bottom:15px;width:100%;">
																												<asp:table id="tblOptionElectives" runat="server" style="width:100%;"></asp:table>
																											</td>
																										</tr>
																										<!-- PROGRAM FOOTNOTES -->
																										<tr>
																											<td class="portletMain" style="padding-left:5px;padding-top:4px;padding-bottom:4px;width:50%;">
																												<b>Option Footnotes:</b>
																											</td>
																											<td class="portletMain" style="padding-right:5px;padding-top:4px;padding-bottom:4px;width:50%;text-align:right;">
																												<input type="button" id="cmdAddFootnotes" value="Add Footnotes" class="small" style="background:#ccccaa;border:1px solid #000000;width:115px;" 
																												onclick="popupCenter('popups/addfootnote.aspx?pvid=' + document.frmProgram.hidProgramVersionID.value + '&pdid=' + document.frmProgram.hidProgramDegreeID.value + '&did=' + document.frmProgram.hidDegreeID.value + '&ltr=' + document.frmProgram.hidLtr.value + '&strm=' + document.frmProgram.hidSTRM.value + '&scolid=' + document.frmProgram.hidSelectedCollegeID.value + '&oa=' + document.frmProgram.hidOfferedAt.value + '&pt=' + document.frmProgram.hidProgramTitle.value + '&oid=' + document.frmProgram.hidOptionID.value + '&pd=' + document.frmProgram.hidProgramDisplay.value + '&bstrm=' + document.frmProgram.hidProgramBeginSTRM.value + '&estrm=' + document.frmProgram.hidProgramEndSTRM.value + '&pp=' + document.frmProgram.hidPublishedProgram.value + '&ppvid=' + document.frmProgram.hidPublishedProgramVersionID.value + '&id=' + document.frmProgram.hidProgramID.value + '&colid=' + document.frmProgram.hidCollegeID.value + '&searchsub=' + document.frmProgram.hidSearchSubject.value.replace('&','%26') + '&searchid=' + document.frmProgram.hidSearchId.value.replace('&','%26') + '&searchstrm=' + document.frmProgram.hidSearchSTRM.value + '&searchtxt=' + document.frmProgram.hidSearchText.value.replace('&','%26'), 'AddProgFootnote', 200, 325);" />
																											</td>
																										</tr>
																										<tr>
																											<td colspan="2" style="padding-left:5px;padding-right:5px;padding-top:15px;width:100%;">
																												<asp:table id="tblProgFootnotes" runat="server" style="width:100%;"></asp:table>
																											</td>
																										</tr>
																									</asp:panel>
																									
																									<!-- PROGRAM LARGE TEXT DISPLAY -->
																									<asp:panel id="panTxtDisplay" runat="server">
																										<tr>
																											<td colspan="2" style="width:100%;padding-top:15px;padding-left:5px;padding-right:5px;">
																												<div style="padding-bottom:2px;"><strong>Program Text</strong></div>
																												<textarea name="txtDisplay" id="txtDisplay" runat="server" class="defaultText" cols="10" rows="5"></textarea>
																												<script type="text/javascript">
																												    CKEDITOR.replace('txtDisplay',
																													{
																													    customConfig: '/iCatalog/ckeditor_config.js',
																													    toolbar:
																														[
																															{ name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', '-',
																														    'NumberedList', 'BulletedList', '-',
																														    'SpecialChar', '-',
                                                                                                                            'Link', 'Unlink', '-',
																														    'SpellChecker', 'Scayt']
																															}
																														],
																													    height: "500px"
																													});
																												</script>
																											</td>
																										</tr>
																									</asp:panel>
																									
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<!-- PANEL 3 BUTTONS -->
																<tr>
																	<td class="portletMain" style="text-align:center;padding:3px;">
																		<asp:button runat="server" id="cmdPrevious" text="Previous" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:100px;"></asp:button>
																		<span id="spnSubmitTextDesc" runat="server">
																			&nbsp;<asp:button runat="server" id="cmdSubmitTextDesc" text="Submit" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:100px;"></asp:button>
																		</span>
																		<span id="spnSubmitOutline" runat="server">
																			&nbsp;<asp:button runat="server" id="cmdSubmitOutline" text="Submit" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:100px;"></asp:button>
																		</span>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</asp:panel>
											
											<!-- PANEL 4 - PROGRAM OUTLINE CONFIRMATION -->
											<asp:panel id="panConfirm1" runat="server">
												<table style="width:100%;" cellpadding="1" cellspacing="1">
													<tr>
														<td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
															class="portletHeader">
															<table style="width:100%;" cellpadding="0" cellspacing="0">
																<tr>
																	<td class="portletHeader" style="width:100%;">&nbsp;Program Outline Confirmation</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="portletMain" style="width:100%">
															<table style="width:100%;" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="portletDark" style="width:100%;">
																		<table cellspacing="1" cellpadding="0" style="width:100%;">
																			<tr>
																				<td class="portletMain" colspan="2" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
																					The following program data has been successfully submitted.
																				</td>
																			</tr>
																			<tr>
																				<td class="portletLight" style="width:100%;vertical-align:text-top;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<tr>
																							<td style="padding-bottom:15px;">
																								<table cellpadding="0" cellspacing="0" style="width:100%;">
																									<!-- DISPLAY PROGRAM TITLE -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Program Title:</b>&nbsp;
																											<asp:label id="lblConfirmProgramTitle1" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- PRIMARY STATE APPROVAL DATE, CIP, ACADEMIC PLAN, and EPC -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>State Approval Date:</b>&nbsp;
																											<asp:label id="lblPrimaryStateApproval" runat="server" cssclass="small"></asp:label>
																											&nbsp;&nbsp;
																											<b>CIP:</b>&nbsp;
																											<asp:label id="lblPrimaryCIP" runat="server" cssclass="small"></asp:label>
                                                                                                            &nbsp;&nbsp;
																											<b>EPC:</b>&nbsp;
																											<asp:label id="lblPrimaryEPC" runat="server" cssclass="small"></asp:label>
																											&nbsp;&nbsp;
                                                                                                            <b>Academic Plan:</b>&nbsp;
																											<asp:label id="lblPrimaryAcademicPlan" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- DISPLAY OFFERED AT -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Offered by:</b>&nbsp;
																											<asp:label id="lblConfOfferedAt1" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- TERM -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Term:</b>&nbsp;&nbsp;&nbsp;
																											<b>Begin</b>&nbsp;&nbsp;<asp:label runat="server" id="lblConfirmProgramBeginSTRM1"></asp:label>
																											&nbsp;&nbsp;&nbsp;<b>End</b>&nbsp;&nbsp;<asp:label runat="server" id="lblConfirmProgramEndSTRM1"></asp:label>
																										</td>
																									</tr>
																									<!-- STATUS -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Status:</b>&nbsp;
																											<asp:label id="lblConfStatus1" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
                                                                                                    <!-- COMPLETION AWARD -->
                                                                                                    <tr>
                                                                                                        <td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
                                                                                                            <b>Completion Award:</b>&nbsp;
                                                                                                            <asp:Label ID="lblConfDegree1" runat="server" CssClass="small"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <!-- PRIMARY AREA OF STUDY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Primary Area of Study:</b>&nbsp;
																											<asp:label id="lblConfAreaOfStudy1" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- ADDITIONAL AREAS OF STUDY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Additional Areas of Study:</b>&nbsp;
																											<asp:Label ID="lblConfAddAreasOfStudy1" runat="server" cssclass="small"></asp:Label>
																										</td>
																									</tr>
																									<!-- PRIMARY CATEGORY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Primary Category:</b>&nbsp;
																											<asp:label id="lblConfCategory1" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- ADDITIONAL CATEGORIES -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Additional Categories:</b>&nbsp;
																											<asp:Label ID="lblConfAddCategories1" runat="server" cssclass="small"></asp:Label>
																										</td>
																									</tr>
																									<!-- DISPLAY PROGRAM DESCRIPTION -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Program Description:</b>&nbsp;
																										</td>
																									</tr>
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;width:100%;padding-top:5px;">
																											<asp:label id="lblConfirmProgramDescription1" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- DISPLAY PROGRAM OUTLINE -->
																									<tr>
																										<td style="padding-left:10px;padding-right:10px;padding-top:15px;width:100%;">
																											<asp:table runat="server" id="tblProgOutline" cellpadding="0" cellspacing="0" style="width:100%;"></asp:table>	
																										</td>
																									</tr>															
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td class="portletMain" style="text-align:center;padding:3px;">
																		<input type="button" id="cmdBack1" value="Back" class="small" style="background:#ccccaa;border:1px solid #000000;width:100px;" onclick="history.back(1);" />
																		&nbsp;<asp:Button ID="cmdExport1" Runat="server" CssClass="small" text="Export to Word" style="background:#ccccaa;border:1px solid #000000;width:100px;"></asp:Button>
																		&nbsp;<asp:Button ID="cmdConfirm1" Runat="server" Text="OK" CssClass="small" style="background:#ccccaa;border:1px solid #000000;width:100px;"></asp:Button>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</asp:panel>
											
											<!-- PANEL 4 - LARGE TEXT DISPLAY CONFIRMATION -->
											<asp:panel id="panConfirm2" runat="server">
												<table style="width:100%;" cellpadding="1" cellspacing="1">
													<tr>
														<td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
															class="portletHeader">
															<table style="width:100%;" cellpadding="0" cellspacing="0">
																<tr>
																	<td class="portletHeader" style="width:100%;">&nbsp;Large Text Display Confirmation</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="portletMain" style="width:100%">
															<table style="width:100%;" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="portletDark" style="width:100%;">
																		<table cellspacing="1" cellpadding="0" style="width:100%;">
																			<tr>
																				<td class="portletMain" colspan="2" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
																					The following program data has been successfully submitted.
																				</td>
																			</tr>
																			<tr>
																				<td class="portletLight" style="width:100%;vertical-align:text-top;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<tr>
																							<td style="padding-bottom:15px;">
																								<table cellpadding="0" cellspacing="0" style="width:100%;">
																									<!-- DISPLAY PROGRAM TITLE -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Program Title:</b>&nbsp;
																											<asp:label id="lblConfirmProgramTitle2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- DISPLAY OFFERED AT -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Offered by:</b>&nbsp;
																											<asp:label id="lblConfOfferedAt2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- TERM -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Term:</b>&nbsp;&nbsp;&nbsp;
																											<b>Begin</b>&nbsp;&nbsp;<asp:label runat="server" id="lblConfirmProgramBeginSTRM2"></asp:label>
																											&nbsp;&nbsp;&nbsp;<b>End</b>&nbsp;&nbsp;<asp:label runat="server" id="lblConfirmProgramEndSTRM2"></asp:label>
																										</td>
																									</tr>
																									<!-- STATUS -->
																									<tr>
																										<td colspan="2" style="padding-left:10px;padding-right:5px;padding-top:15px;width:80%;">
																											<b>Status:</b>&nbsp;
																											<asp:label id="lblConfStatus2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
                                                                                                    <!-- PRIMARY AREA OF STUDY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Primary Area of Study:</b>&nbsp;
																											<asp:label id="lblConfAreaOfStudy2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- ADDITIONAL AREAS OF STUDY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Additional Areas of Study:</b>&nbsp;
																											<asp:label id="lblConfAddAreasOfStudy2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- PRIMARY CATEGORY -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Primary Category:</b>&nbsp;
																											<asp:label id="lblConfCategory2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- ADDITIONAL CATEGORIES -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Additional Categories:</b>&nbsp;
																											<asp:label id="lblConfAddCategories2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- DISPLAY PROGRAM DESCRIPTION -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Program Description:</b>&nbsp;
																										</td>
																									</tr>
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:5px;width:100%;">
																											<asp:label id="lblConfirmProgramDescription2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- DISPLAY COMPLETION AWARD -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Completion Award:</b>&nbsp;
																											<asp:label id="lblConfDegTitle2" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																									<!-- DISPLAY PROGRAM TEXT -->
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<b>Program Text:</b>
																										</td>
																									</tr>
																									<tr>
																										<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																											<asp:label id="lblConfProgText" runat="server" cssclass="small"></asp:label>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td class="portletMain" style="text-align:center;padding:3px;">
																		<input type="button" id="cmdBack2" value="Back" class="small" style="background:#ccccaa;border:1px solid #000000;width:100px;" onclick="history.back(1);" />
																		&nbsp;<asp:Button ID="cmdExport2" Runat="server" CssClass="small" text="Export to Word" style="background:#ccccaa;border:1px solid #000000;width:100px;"></asp:Button>
																		&nbsp;<input type="button" id="cmdConfirm2" value="OK" class="small" style="background:#ccccaa;border:1px solid #000000;width:100px;" onclick="location.href='edit.aspx'" />
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</asp:panel>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<script type="text/javascript">
					<!--
						var chkRevision = document.getElementById("chkRevision");
								
						if(chkRevision != null){
							if(document.getElementById("chkRevision").checked){
								document.getElementById("txtProgramTitle2").style.display = "none";
								document.getElementById("spanRevisionProgramTitle").style.display = "";
							}else{
								document.getElementById("txtProgramTitle2").style.display = "";
								document.getElementById("spanRevisionProgramTitle").style.display = "none";
							}
						}
					//-->
					</script>
				</form>
			</asp:panel>
			<div class="clearer"></div>
		</asp:panel>
	</body>
</html>
