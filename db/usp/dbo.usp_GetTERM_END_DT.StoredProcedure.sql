USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTERM_END_DT]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/16/2019
-- Description:	Return TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable by STRM
-- Parameters: 
--		@STRM - Term Code		
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetTERM_END_DT]
	@STRM varchar(4) 
	--@TERM_END_DT DATE OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT *
	FROM [dbo].[udf_GetTERM_END_DT](@STRM)
END
GO
