using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.dept
{
	/// <summary>
	/// Summary description for copy.
	/// </summary>
	public partial class copy : System.Web.UI.Page
	{

		courseData csCourse = new courseData();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panError.Visible = false;
			panConfirm.Visible = false;

			if(!IsPostBack){
                panDescriptionCourses.Visible = false;

				cboOldSubject.Items.Add("");
				cboNewSubject.Items.Add("");

				DataSet dsSubjects = csCourse.GetSubjects();
				for(Int32 intDSRow = 0; intDSRow < dsSubjects.Tables[0].Rows.Count; intDSRow++){
					Int16 intCourseCtr = Convert.ToInt16(dsSubjects.Tables[0].Rows[intDSRow]["CrsCount"]);

					if(intCourseCtr > 0){
						cboOldSubject.Items.Add(dsSubjects.Tables[0].Rows[intDSRow]["SUBJECT"].ToString());
					}
					
					cboNewSubject.Items.Add(dsSubjects.Tables[0].Rows[intDSRow]["SUBJECT"].ToString());
				}

				if(cboOldSubject.Items.Count == 1){
					cboOldSubject.Items[0].Text = "There are no subject areas with courses.";
					cboOldSubject.Items[0].Value = "";
				}
			}

			cboOldCourseEndTerm.Items.Clear();
			if(cboOldSubject.SelectedValue != ""){
				DataSet dsEndTerms = csCourse.GetCourseEndTerms(cboOldSubject.SelectedValue);
				for(Int32 intDSRow = 0; intDSRow < dsEndTerms.Tables[0].Rows.Count; intDSRow++){
					cboOldCourseEndTerm.Items.Add(new ListItem(dsEndTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString(), dsEndTerms.Tables[0].Rows[intDSRow]["STRM"].ToString()));
				}
				try{
					cboOldCourseEndTerm.SelectedValue = Request.Form["cboOldCourseEndTerm"];
				}catch{
					//do nothing
				}

                panDescriptionCourses.Controls.Clear();
                hidCopyCrsList.Value = "";
                DataSet dsSubjectAreaCourses = csCourse.GetSubjectAreaCourses(cboOldSubject.SelectedValue);
                Int32 intRecordCount = dsSubjectAreaCourses.Tables[0].Rows.Count;

                if (intRecordCount > 0)
                {
                    HtmlInputCheckBox chkAll = new HtmlInputCheckBox();
                    chkAll.ID = "chkAll";
                    chkAll.Attributes["title"] = "Check/Uncheck ALL";
                    chkAll.Attributes["onclick"] = "checkAll();";
                    panDescriptionCourses.Controls.Add(new LiteralControl("<div>"));
                    panDescriptionCourses.Controls.Add(chkAll);
                    panDescriptionCourses.Controls.Add(new LiteralControl("&nbsp;ALL</div>"));

                    for (Int32 intDSRow = 0; intDSRow < dsSubjectAreaCourses.Tables[0].Rows.Count; intDSRow++)
                    {
                        CheckBox chkCourse = new CheckBox();
                        chkCourse.ID = "chkCourse" + dsSubjectAreaCourses.Tables[0].Rows[intDSRow]["CourseID"].ToString();
                        panDescriptionCourses.Controls.Add(new LiteralControl("<div>"));
                        panDescriptionCourses.Controls.Add(chkCourse);
                        panDescriptionCourses.Controls.Add(new LiteralControl("&nbsp;" + dsSubjectAreaCourses.Tables[0].Rows[intDSRow]["CourseOffering"].ToString().Replace(" ","&nbsp;")));
                        panDescriptionCourses.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dsSubjectAreaCourses.Tables[0].Rows[intDSRow]["DESCR"].ToString()));
                        panDescriptionCourses.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dsSubjectAreaCourses.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString()));
                        panDescriptionCourses.Controls.Add(new LiteralControl("&nbsp;&ndash;&nbsp;" + dsSubjectAreaCourses.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString()));
                        panDescriptionCourses.Controls.Add(new LiteralControl("</div>"));

                        hidCopyCrsList.Value += "|" + chkCourse.ID;
                    }
                    panDescriptionCourses.Visible = true;
                }
			}

			cboNewCourseBeginTerm.Items.Clear();
			cboNewCourseEndTerm.Items.Clear();
			if(cboNewSubject.SelectedValue != "" && cboOldCourseEndTerm.SelectedValue != ""){
				DataSet dsTerms = csCourse.GetTermsForCourses();
				cboNewCourseEndTerm.Items.Add("Z999");
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
					String STRM = dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString();
                    String DESCR = dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString();
					cboNewCourseBeginTerm.Items.Add(new ListItem(DESCR, STRM));
					cboNewCourseEndTerm.Items.Add(new ListItem(DESCR, STRM));
				}
				try{
					cboNewCourseBeginTerm.SelectedValue = Request.Form["cboNewCourseBeginTerm"];
				}catch{
					//do nothing
				}
				try{
					cboNewCourseEndTerm.SelectedValue = Request.Form["cboNewCourseEndTerm"];
				}catch{
					//do nothing
				}
			}

			panCopy.Visible = true;
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){

            if (hidCopyCrsList.Value != "")
            {
                String[] strCrsList = hidCopyCrsList.Value.Substring(1).Split('|');
                for (Int32 i = 0; i < strCrsList.Length; i++)
                {
                    if (Request.Form[strCrsList[i]] == "on")
                    {
                        Int32 courseID = Convert.ToInt32(strCrsList[i].Replace("chkCourse",""));
                        Int16 intSuccess = csCourse.CopySubjectCourse(courseID, cboOldSubject.SelectedValue, cboNewSubject.SelectedValue, cboOldCourseEndTerm.SelectedValue, cboNewCourseBeginTerm.SelectedValue, cboNewCourseEndTerm.SelectedValue, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]);

                        if (intSuccess == 1)
                        {
                            hidSuccessfulCrsList.Value += "|" + courseID;
                        }
                        else if (intSuccess == 2)
                        {
                            hidExistsCrsList.Value += "|" + courseID;
                        }
                        else if (intSuccess == 0)
                        {
                            hidFailedCrsList.Value += "|" + courseID;
                        }
                    }
                }

                if (hidFailedCrsList.Value != "")
                {
                    lblConfirm.Text += "<br /><span style=\"color:red;font-weight:bold;\">Error: The following courses failed to be copied:</span><br /><br />";
                    String[] strFailedCrsList = hidFailedCrsList.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strFailedCrsList.Length; i++)
                    {
                        DataSet dsCourse = csCourse.GetCourse(Convert.ToInt32(strFailedCrsList[i]));
                        if(dsCourse.Tables[0].Rows.Count > 0){
                            lblConfirm.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dsCourse.Tables[0].Rows[0]["CourseOffering"].ToString().Replace(" ","&nbsp;");
                            lblConfirm.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dsCourse.Tables[0].Rows[0]["DESCR"].ToString();
                            lblConfirm.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dsCourse.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString() + "&nbsp;&ndash;&nbsp;" + dsCourse.Tables[0].Rows[0]["EndTerm_DESCR"].ToString() + "<br />";
                        }
                    }
                }

                if (hidExistsCrsList.Value != "")
                {
                    lblConfirm.Text += "<br /><span style=\"color:red;font-weight:bold;\">Error: The following " + cboOldSubject.SelectedValue + " courses conflict with existing courses in " + cboNewSubject.SelectedValue + " for the term " + cboNewCourseBeginTerm.SelectedItem.Text + " - " + cboNewCourseEndTerm.SelectedItem.Text + ":</span><br /><br />";
                    String[] strExistsCrsList = hidExistsCrsList.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strExistsCrsList.Length; i++)
                    {
                        DataSet dsCourse = csCourse.GetCourse(Convert.ToInt32(strExistsCrsList[i]));
                        if (dsCourse.Tables[0].Rows.Count > 0)
                        {
                            lblConfirm.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dsCourse.Tables[0].Rows[0]["CourseOffering"].ToString().Replace(" ", "&nbsp;");
                            lblConfirm.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dsCourse.Tables[0].Rows[0]["DESCR"].ToString();
                            lblConfirm.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dsCourse.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString() + "&nbsp;&ndash;&nbsp;" + dsCourse.Tables[0].Rows[0]["EndTerm_DESCR"].ToString() + "<br />";
                        }
                    }
                }

                if (hidSuccessfulCrsList.Value != "")
                {
                    lblConfirm.Text += "<br /><b>The following courses have successfully been copied from " + cboOldSubject.SelectedValue + " to " + cboNewSubject.SelectedValue + " effective " + cboNewCourseBeginTerm.SelectedItem.Text + " - " + cboNewCourseEndTerm.SelectedItem.Text + ":</b><br /><br />";
                    String[] strSuccessfulCrsList = hidSuccessfulCrsList.Value.Substring(1).Split('|');
                    for (Int32 i = 0; i < strSuccessfulCrsList.Length; i++)
                    {
                        DataSet dsCourse = csCourse.GetCourse(Convert.ToInt32(strSuccessfulCrsList[i]));
                        if (dsCourse.Tables[0].Rows.Count > 0)
                        {
                            lblConfirm.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dsCourse.Tables[0].Rows[0]["CourseOffering"].ToString().Replace(" ", "&nbsp;");
                            lblConfirm.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dsCourse.Tables[0].Rows[0]["DESCR"].ToString();
                            lblConfirm.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dsCourse.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString() + "&nbsp;&ndash;&nbsp;" + dsCourse.Tables[0].Rows[0]["EndTerm_DESCR"].ToString() + "<br />";
                        }
                    }
                }

                panConfirm.Visible = true;
                panCopy.Visible = false;

            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
