<%@ Page language="c#" Inherits="ICatalog.dept.copy" CodeFile="copy.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Copy Courses to a New Subject Area</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <script type="text/javascript">
        <!--
            function checkAll() {
                var chkAll = document.getElementById("chkAll");
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    var element = document.forms[0].elements[i];
                    if (element.type == 'checkbox') {
                        var chk = document.forms[0].elements[i];
                        if (chkAll.checked) {
                            if (chk.id.indexOf("chkCourse") > -1) {
                                chk.checked = true;
                            }
                        } else {
                            if (chk.id.indexOf("chkCourse") > -1) {
                                chk.checked = false;
                            }
                        }
                    }
                }
            }

            function validate() {
                var blnCourseSelected = false;
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    var element = document.forms[0].elements[i];
                    if (element.type == 'checkbox') {
                        var chk = document.forms[0].elements[i];
                        if (chk.id.indexOf("chkCourse") > -1 && chk.checked == true) {
                            blnCourseSelected = true;
                        }
                    }
                }

                isValid = false;
                var cboNewCourseBeginTerm = document.getElementById("cboNewCourseBeginTerm");
                var cboNewCourseEndTerm = document.getElementById("cboNewCourseEndTerm");
                if(document.frmDept.cboOldSubject.value == ""){
                    alert("Please select the subject area to copy courses from.");
                } else if (blnCourseSelected == false) {
                    alert("Please select a course to be copied.");
                }else if(document.frmDept.cboOldCourseEndTerm.value == ""){
					alert("Please select the old course end term.");
                }else if(document.frmDept.cboNewSubject.value == ""){
                    alert("Please select the subject area to copy courses to.");
                }else if(cboNewCourseBeginTerm.value == ""){
					alert("Please select the new course begin term.");
				}else if(cboNewCourseEndTerm.value == ""){
					alert("Please select the new course end term.");
				}else if(cboNewCourseBeginTerm.value > cboNewCourseEndTerm.value){
					alert("The new course begin term cannot be after the new course end term.");
	            } else {
                    isValid = true;
                }
                return isValid;
            }
        //-->
        </script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmDept" runat="server">
                    <input type="hidden" id="hidCopyCrsList" runat="server" />
                    <input type="hidden" id="hidFailedCrsList" runat="server" />
                    <input type="hidden" id="hidExistsCrsList" runat="server" />
                    <input type="hidden" id="hidSuccessfulCrsList" runat="server" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Copy Courses to a New Subject Area</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <asp:panel runat="server" id="panCopy">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <!-- Error Message -->
                                                                            <asp:panel runat="server" id="panError">
                                                                                <tr>
                                                                                    <td colspan="2" class="portletMain" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                        <asp:Label Runat="server" ID="lblErrorMsg"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </asp:panel>
                                                                            <tr>
																				<td class="portletSecondary" style="width:42%;padding-top:10px;text-align:right;vertical-align:top;">
																					<b>Copy Courses From:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="width:58%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:dropdownlist runat="server" id="cboOldSubject" cssclass="small" AutoPostBack="True"></asp:dropdownlist>
                                                                                    <asp:Panel ID="panDescriptionCourses" runat="server" style="padding-top:10px;"></asp:Panel>
																				</td>
																			</tr>
																			<tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Old Course End Term:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:DropDownList Runat="server" ID="cboOldCourseEndTerm" style="width:105px;" CssClass="small" AutoPostBack="True"></asp:DropDownList>
																				</td>
																			</tr>
																			<tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Copy Courses To:&nbsp;&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:dropdownlist runat="server" id="cboNewSubject" cssclass="small" AutoPostBack="True"></asp:dropdownlist>
																				</td>
																			</tr>
																			<tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>New Course Term:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<b>Begin</b>&nbsp;
																					<asp:dropdownlist runat="server" id="cboNewCourseBeginTerm" style="width:105px;" cssclass="small"></asp:dropdownlist>
																					&nbsp;&nbsp;&nbsp;<b>End</b>&nbsp;
																					<asp:dropdownlist runat="server" id="cboNewCourseEndTerm" style="width:105px;" cssclass="small"></asp:dropdownlist>
																				</td>
																			</tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                        <asp:button id="cmdSubmit" runat="server" text="Copy Courses" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclientclick="return validate();" onclick="cmdSubmit_Click"></asp:button>
														                        </td>
													                        </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                    <asp:panel runat="server" id="panConfirm">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <tr>
                                                                                <td class="portletLight" style="padding-left:15px;padding-bottom:15px;width:100%;">
                                                                                    <asp:Label ID="lblConfirm" runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" style="text-align:center;padding:3px;">
															                        <input type="button" id="cmdOK" runat="server" value="OK" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="location.href='copy.aspx';" name="cmdOK" />
														                        </td>
													                        </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <div class="clearer"></div>
        </asp:panel>
    </body>
</html>


