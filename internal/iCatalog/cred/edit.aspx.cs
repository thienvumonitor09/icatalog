using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.cred
{
	/// <summary>
	/// Summary description for edit.
	/// </summary>
	public partial class edit : System.Web.UI.Page
	{

		credentialData csCredential = new credentialData();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panEdit.Visible = false;
			panDisplay.Visible = false;

			if(!IsPostBack || hidTodo.Value == "search"){
				GenerateCredentialList();
				panDisplay.Visible = true;
			}else if(hidTodo.Value == "edit"){
				hidTodo.Value = "";
				DataSet dsCredential = csCredential.GetCredentialForEdit(hidEmployeeID.Value);	
				if(dsCredential.Tables[0].Rows.Count > 0){
					lblCredentialEmployeeID.Text = dsCredential.Tables[0].Rows[0]["EMPLID"].ToString();
					lblCredentialName.Text = dsCredential.Tables[0].Rows[0]["Name"].ToString();
					txtCredentialAssignment.Text = dsCredential.Tables[0].Rows[0]["Assignment"].ToString();
					cboCredentialTenureTrack.SelectedValue = dsCredential.Tables[0].Rows[0]["TenureTrack"].ToString().Trim();
					txtCredentialDescription.Value = dsCredential.Tables[0].Rows[0]["Description"].ToString();

					String credentialWorkUnit = dsCredential.Tables[0].Rows[0]["WorkUnit"].ToString().Trim();
					String[] strSelected = credentialWorkUnit.Split('/');
					for(Int16 i = 0; i < strSelected.Length; i++){
						ListItem li = chkCredentialWorkUnit.Items.FindByValue(strSelected[i].Trim());
						li.Selected = true;
					}
				}
				panEdit.Visible = true;
			}else if(hidTodo.Value == "delete"){
				hidTodo.Value = "";
				csCredential.DeleteCredential(hidEmployeeID.Value);
				GenerateCredentialList();
				panDisplay.Visible = true;
			}
		}

		public void GenerateCredentialList(){
			TableRow tr = new TableRow();				
			TableCell td = new TableCell();

			for(Int16 intAlphaCtr = 65; intAlphaCtr <= 90; intAlphaCtr++){
				td = new TableCell();
				td.Attributes["style"] = "width:23px;text-align:center;";
				td.Attributes["onclick"] = "document.getElementById(\"hidTodo\").value='search';document.getElementById(\"hidLtr\").value='" + Convert.ToChar(intAlphaCtr) + "';document.frmCred.submit();";
				td.CssClass = "portletMain";
				td.Controls.Add(new LiteralControl("<a class=\"ltr\" href=\"#\" onclick=\"document.getElementById(\"hidTodo\").value='search';document.frmCred.submit();\">" + Convert.ToChar(intAlphaCtr) + "</a>"));
				tr.Cells.Add(td);
			}

			tblAlpha.Rows.Add(tr);

			String strLtr = hidLtr.Value;
			if(strLtr == ""){
				String strRequestLtr = Request.QueryString["ltr"];
				if(strRequestLtr == null || strRequestLtr == ""){
					strLtr = "A";
				}else{
					strLtr = strRequestLtr;
				}
			}

			DataSet dsCredentials = csCredential.GetCredentials(Convert.ToChar(strLtr));
			Int32 intRowCtr = dsCredentials.Tables[0].Rows.Count;

			tr = new TableRow();

			td = new TableCell();
			td.CssClass = "portletSecondary";
			td.Attributes["style"] = "text-align:center;width:60px;padding-top:4px;padding-bottom:4px;";
			td.Controls.Add(new LiteralControl("<b>ctcLink ID</b>"));

			tr.Cells.Add(td);

			td = new TableCell();
			td.CssClass = "portletSecondary";
			td.Attributes["style"] = "text-align:center;width:180px;padding-top:4px;padding-bottom:4px;";
			td.Controls.Add(new LiteralControl("<b>Name</b>"));

			tr.Cells.Add(td);

			td = new TableCell();
			td.CssClass = "portletSecondary";
			td.Attributes["style"] = "text-align:center;width:290px;padding-top:4px;padding-bottom:4px;";
			td.Controls.Add(new LiteralControl("<b>Assigned To</b>"));

			tr.Cells.Add(td);

			td = new TableCell();
			td.CssClass = "portletSecondary";
			td.Attributes["style"] = "width:67px;text-align:center";
			td.Controls.Add(new LiteralControl("<a href=\"add.aspx?ltr=" + hidLtr.Value + "\">Add New</a>"));

			tr.Cells.Add(td);
			tblDisplay.Rows.Add(tr);

			if(intRowCtr > 0){

				for(Int32 intDSRow = 0; intDSRow < intRowCtr; intDSRow++){
					String employeeID = dsCredentials.Tables[0].Rows[intDSRow]["EMPLID"].ToString(), strName = dsCredentials.Tables[0].Rows[intDSRow]["Name"].ToString();

					tr = new TableRow();

					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "text-align:center;vertical-align:top;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl(employeeID));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "vertical-align:top;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl(strName));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl(dsCredentials.Tables[0].Rows[intDSRow]["Assignment"].ToString()));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "text-align:center;vertical-align:top;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.getElementById('hidTodo').value='edit';document.getElementById('hidEmployeeID').value='" + employeeID + "';document.frmCred.submit();\">Edit</a> | "));
					td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete credentials for " + strName.Replace("'","\\'") + "?')){document.getElementById('hidTodo').value='delete';document.getElementById('hidEmployeeID').value='" + employeeID + "';document.frmCred.submit();}\">Delete</a>"));

					tr.Cells.Add(td);

					tblDisplay.Rows.Add(tr);
				}
			}else{
				tr = new TableRow();
				td = new TableCell();
				td.CssClass = "portletLight";
				td.ColumnSpan = 4;
				td.Attributes["style"] = "padding-top:4px;padding-bottom:4px;";
				td.Controls.Add(new LiteralControl("&nbsp;" + strLtr + " returned 0 results."));
				tr.Cells.Add(td);
				tblDisplay.Rows.Add(tr);
			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			
			String credentialWorkUnit = "";
			foreach(ListItem li in chkCredentialWorkUnit.Items){
				if(li.Selected){
					credentialWorkUnit += "/" + li.Value;
				}
			}
			if(credentialWorkUnit != ""){
				credentialWorkUnit = credentialWorkUnit.Substring(1);
			}

			csCredential.EditCredential(hidEmployeeID.Value, txtCredentialAssignment.Text, credentialWorkUnit, cboCredentialTenureTrack.SelectedValue, txtCredentialDescription.Value);
			GenerateCredentialList();
			panDisplay.Visible = true;
			panEdit.Visible = false;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
