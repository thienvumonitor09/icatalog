using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;
using System.Text.RegularExpressions;

namespace ICatalog.course
{
	/// <summary>
	/// Summary description for edit.
	/// </summary>
	public partial class edit : System.Web.UI.Page
	{

		courseData csCourse = new courseData();
        permissionData csPermissions = new permissionData();
        termData csTerm = new termData();

		#region PROTECTED CONTROLS
		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panEdit.Visible = false;
			panView.Visible = false;
			panWorkingCopy.Visible = false;
			panError.Visible = false;
			panDisplay.Visible = true;
			
			String strTodo = Request.QueryString["todo"], selectedSubject = Request.QueryString["subject"], selectedSTRM = Request.QueryString["strm"];
			if(strTodo == "edit" && hidTodo.Value == "revision"){
				strTodo = "revision";
			}

			if(!IsPostBack){
				hidSearchText.Value = Request.QueryString["searchtxt"];

                DataSet dsSubjects = csCourse.GetSubjects();
                cboDescriptionSubject.Items.Add("");
                cboDescriptionSubject.Items[0].Value = "";
                for (Int32 intDSRow = 0; intDSRow < dsSubjects.Tables[0].Rows.Count; intDSRow++)
                {
                    String subject = dsSubjects.Tables[0].Rows[intDSRow]["SUBJECT"].ToString();
                    cboCourseSubject.Items.Add(subject);
                    if (Convert.ToInt16(dsSubjects.Tables[0].Rows[intDSRow]["CrsCount"]) > 0)
                    {
                        cboDescriptionSubject.Items.Add(subject);
                    }
                }

				DataSet dsTerms = csCourse.GetTermsForCourses();
				cboCourseEndSTRM.Items.Add("Z999");
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
					String STRM = dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString();
                    String DESCR = dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString();
                    cboCourseBeginSTRM.Items.Add(new ListItem(DESCR, STRM));
                    cboCourseEndSTRM.Items.Add(new ListItem(DESCR, STRM));
                    cboTerm.Items.Add(new ListItem(DESCR, STRM));
				}

                // uncomment code to have current strm selected by default, comment code for "ALL" to be selected by default
                /*
                if (selectedSTRM != null && selectedSTRM != "")
                {
                    try
                    {
                        cboTerm.SelectedValue = Request.QueryString["strm"];
                    }
                    catch
                    {
                        //do nothing
                    }
                }
                else
                {
                    try
                    {
                        cboTerm.SelectedValue = csTerm.GetCurrentTerm();
                    }
                    catch
                    {
                        //do nothing
                    }
                }
                */
			}

            cboSubject.Items.Clear();
            DataSet dsEffectiveSubjects = csCourse.GetEffectiveSubjects(cboTerm.SelectedValue);
            for (Int32 intDSRow = 0; intDSRow < dsEffectiveSubjects.Tables[0].Rows.Count; intDSRow++)
            {
                cboSubject.Items.Add(dsEffectiveSubjects.Tables[0].Rows[intDSRow]["SUBJECT"].ToString());
            }

            String postSubject = Request.Form["cboSubject"];
            if ((selectedSubject != null && selectedSubject != "") && (postSubject == null || postSubject == ""))
            {
                
                try
                {
                    cboSubject.SelectedValue = selectedSubject;
                }
                catch
                {
                    //do nothing
                }
            }
            else
            {
                try
                {
                    cboSubject.SelectedValue = postSubject;
                }
                catch
                {
                    //do nothing
                }
            }

			if(strTodo == "edit"){
                /*
				String strSameAs = Request.QueryString["sameas"];
				DataSet dsCourse = new DataSet();
				
				if(Request.QueryString["sameas"] != ""){
					dsCourse = csCourse.GetCourse(Convert.ToInt32(strSameAs));

					if(dsCourse.Tables[0].Rows.Count > 0){
						String strOfferedAt = "", descriptionCourseOffering = "", variableUnits = dsCourse.Tables[0].Rows[0]["VariableUnits"].ToString(), descriptionCourseID = dsCourse.Tables[0].Rows[0]["DescriptionCourseID"].ToString();

                        DataSet dsCourseCollege = csCourse.GetCourseCollege(Convert.ToInt32(strSameAs)); //store strSame as in Int32 variable
                        for (Int32 i = 0; i < dsCourseCollege.Tables[0].Rows.Count; i++)
                        {
                            strOfferedAt += ", " + dsCourseCollege.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
                        }
                        if (strOfferedAt != "")
                        {
                            strOfferedAt = strOfferedAt.Substring(2);
                        }

						if(variableUnits == "Y"){
							variableUnits = "Variable";
						}else{
							variableUnits = "Fixed";
						}
						if(descriptionCourseID.Length > 0){
							descriptionCourseOffering = csCourse.GetCourseOffering(Convert.ToInt32(descriptionCourseID));
							panSameAs.Visible = true;
						}else{
							panSameAs.Visible = false;
						}
						lblCourseOffering.Text = dsCourse.Tables[0].Rows[0]["CourseOffering"].ToString().Replace(" ","&nbsp;");
						lblCourseBeginSTRM.Text = dsCourse.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString();
						lblCourseEndSTRM.Text = dsCourse.Tables[0].Rows[0]["EndTerm_DESCR"].ToString();
						lblCourseSubject.Text = dsCourse.Tables[0].Rows[0]["SUBJECT"].ToString();
						lblSubjectDescription.Text = dsCourse.Tables[0].Rows[0]["DESCR"].ToString();
						lblCatalogNbr.Text = dsCourse.Tables[0].Rows[0]["CATALOG_NBR"].ToString();
						lblCourseShortTitle.Text = dsCourse.Tables[0].Rows[0]["DESCR"].ToString();
						lblCourseSuffix.Text = dsCourse.Tables[0].Rows[0]["CourseSuffix"].ToString();
						lblCourseLongTitle.Text = dsCourse.Tables[0].Rows[0]["COURSE_TITLE_LONG"].ToString();
						lblVariableUnits.Text = variableUnits;
						lblUnitsMaximum.Text = dsCourse.Tables[0].Rows[0]["UNITS_MAXIMUM"].ToString();
						lblUnitsMinimum.Text = dsCourse.Tables[0].Rows[0]["UNITS_MINIMUM"].ToString();
						lblOfferedAt.Text = strOfferedAt;
						lblDescriptionCourseOffering.Text = descriptionCourseOffering;
						lblCourseLongDescription.Text = dsCourse.Tables[0].Rows[0]["DESCRLONG"].ToString();	
						lblLastModifiedEMPLID.Text = csPermissions.GetEmpName(dsCourse.Tables[0].Rows[0]["LastModifiedEMPLID"].ToString());
						lblLastModifiedDate.Text = dsCourse.Tables[0].Rows[0]["LastModifiedDate"].ToString();

						if(hidSearchText.Value != null && hidSearchText.Value != "" && lblCourseLongDescription.Text.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1){
							lblCourseLongDescription.Text = Regex.Replace(lblCourseLongDescription.Text, hidSearchText.Value, "<span style=\"background-color:yellow\">" + lblCourseLongDescription.Text.Substring(lblCourseLongDescription.Text.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", RegexOptions.IgnoreCase);
						}
					}
					panWorkingCopy.Visible = true;
					panView.Visible = true;
				}

				hidSTRM.Value = Request.QueryString["strm"];
				hidCourseID.Value = Request.QueryString["course"];
				hidPublishedCourseID.Value = strSameAs;
                Int32 courseID = Convert.ToInt32(hidCourseID.Value);
				dsCourse = csCourse.GetCourse(courseID);

				if(dsCourse.Tables[0].Rows.Count > 0){

					String descriptionSubject = "", descriptionCourseID = dsCourse.Tables[0].Rows[0]["DescriptionCourseID"].ToString(), variableUnits = dsCourse.Tables[0].Rows[0]["VariableUnits"].ToString(), courseSubject = dsCourse.Tables[0].Rows[0]["SUBJECT"].ToString();

                    if (hidCollegeList.Value == "")
                    {
                        DataSet dsCourseCollege = csCourse.GetCourseCollege(courseID);
                        for (Int32 intDSRow = 0; intDSRow < dsCourseCollege.Tables[0].Rows.Count; intDSRow++)
                        {
                            String collegeID = dsCourseCollege.Tables[0].Rows[intDSRow]["CollegeID"].ToString();
                            if (hidCollegeList.Value.IndexOf(collegeID) == -1)
                            {
                                hidCollegeList.Value += "," + collegeID;
                            }
                        }
                        if (hidCollegeList.Value.IndexOf(",") == 0)
                        {
                            hidCollegeList.Value = hidCollegeList.Value.Substring(1, hidCollegeList.Value.Length - 1);
                        }
                    }

                    //store colleges in arraylist for contains comparison
                    ArrayList lstExistingColleges = new ArrayList();
                    String[] aryCollegeList = hidCollegeList.Value.Split(',');
                    for (Int32 i = 0; i < aryCollegeList.Length; i++)
                    {
                        lstExistingColleges.Add(aryCollegeList[i]);
                    }

                    //create and populate the college checkboxes
                    programData csProgramData = new programData();
                    DataSet dsCollegeList = csProgramData.GetCollegeList();
                    panCollege.Controls.Clear();
                    for (Int32 intDSRow = 0; intDSRow < dsCollegeList.Tables[0].Rows.Count; intDSRow++)
                    {
                        String collegeShortTitle = dsCollegeList.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
                        String collegeID = dsCollegeList.Tables[0].Rows[intDSRow]["CollegeID"].ToString();

                        HtmlInputCheckBox chkOfferedAt = new HtmlInputCheckBox();
                        chkOfferedAt.Value = collegeID;
                        chkOfferedAt.ID = "chk" + collegeID;
                        chkOfferedAt.Name = "chkOfferedAt";

                        String strSelectedCollege = Request.Form[chkOfferedAt.ID];

                        if (strSelectedCollege != null && strSelectedCollege != "")
                        {
                            chkOfferedAt.Checked = true;
                        }

                        if (lstExistingColleges.Contains(collegeID))
                        {
                            chkOfferedAt.Checked = true;
                        }

                        chkOfferedAt.Attributes["onclick"] = "addCollege('" + chkOfferedAt.ID + "','" + collegeID + "');";
                        panCollege.Controls.Add(new LiteralControl("&nbsp;"));
                        panCollege.Controls.Add(chkOfferedAt);
                        panCollege.Controls.Add(new LiteralControl(collegeShortTitle));
                    }
                    
					String strSelectedStatus = Request.Form["optPublishedCourse"];
					if(strSelectedStatus != null){
						optPublishedCourse.SelectedValue = strSelectedStatus;
					}else{
						optPublishedCourse.SelectedValue = dsCourse.Tables[0].Rows[0]["PublishedCourse"].ToString();
					}
					hidPublishedCourse.Value = optPublishedCourse.SelectedValue;
					
					if(descriptionCourseID.Length > 0){
						DataSet dsDescriptionCourse = csCourse.GetCourse(Convert.ToInt32(descriptionCourseID));
						descriptionSubject = dsDescriptionCourse.Tables[0].Rows[0]["SUBJECT"].ToString();
						cboDescriptionSubject.Value = descriptionSubject;
						DataSet dsDescriptionCourses = csCourse.GetDescriptionCourses(descriptionSubject);
						cboDescriptionCourse.Items.Clear();
						cboDescriptionCourse.Items.Add("");
						for(Int32 intDSRow = 0; intDSRow < dsDescriptionCourses.Tables[0].Rows.Count; intDSRow++){
							cboDescriptionCourse.Items.Add(new ListItem(dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseOffering"].ToString() + " " + dsDescriptionCourses.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString(), dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseID"].ToString()));
						}
						cboDescriptionCourse.Value = descriptionCourseID;
					}else{
						cboDescriptionSubject.Value = courseSubject;
						DataSet dsDescriptionCourses = csCourse.GetDescriptionCourses(courseSubject);
						cboDescriptionCourse.Items.Clear();
						cboDescriptionCourse.Items.Add("");
						for(Int32 intDSRow = 0; intDSRow < dsDescriptionCourses.Tables[0].Rows.Count; intDSRow++){
							cboDescriptionCourse.Items.Add(new ListItem(dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseOffering"].ToString() + " " + dsDescriptionCourses.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString(), dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseID"].ToString()));
						}
					}
					
					if(variableUnits == "Y"){
						optVariableUnitsY.Checked = true;
					}else{
						optVariableUnitsN.Checked = true;
						txtUnitsMaximum.Disabled = true;
					}
					
					hidOldCourseOffering.Value = dsCourse.Tables[0].Rows[0]["CourseOffering"].ToString().Replace(" ", "&nbsp;");
					divCourseOffering.InnerHtml = hidOldCourseOffering.Value;
					cboCourseBeginSTRM.SelectedValue = dsCourse.Tables[0].Rows[0]["CourseBeginSTRM"].ToString();
					cboCourseEndSTRM.SelectedValue = dsCourse.Tables[0].Rows[0]["CourseEndSTRM"].ToString();
					hidCourseBeginSTRM.Value = cboCourseBeginSTRM.SelectedValue;
					hidCourseEndSTRM.Value = cboCourseEndSTRM.SelectedValue;
					cboCourseSubject.Value = courseSubject;
					divSubjectDescription.InnerHtml = dsCourse.Tables[0].Rows[0]["DESCR"].ToString();
					txtCatalogNbr.Value = dsCourse.Tables[0].Rows[0]["CATALOG_NBR"].ToString();
					txtCourseShortTitle.Text = dsCourse.Tables[0].Rows[0]["DESCR"].ToString();
					cboCourseSuffix.Value = dsCourse.Tables[0].Rows[0]["CourseSuffix"].ToString();
					txtCourseLongTitle.Value = dsCourse.Tables[0].Rows[0]["COURSE_TITLE_LONG"].ToString();
					txtUnitsMaximum.Value = dsCourse.Tables[0].Rows[0]["UNITS_MAXIMUM"].ToString();
					txtUnitsMinimum.Value = dsCourse.Tables[0].Rows[0]["UNITS_MINIMUM"].ToString();
					txtCourseLongDescription.Value = dsCourse.Tables[0].Rows[0]["DESCRLONG"].ToString();	
					lblLastModifiedEMPLID.Text = csPermissions.GetEmpName(dsCourse.Tables[0].Rows[0]["LastModifiedEMPLID"].ToString());
					lblLastModifiedDate.Text = dsCourse.Tables[0].Rows[0]["LastModifiedDate"].ToString();

					if(hidSearchText.Value != null && hidSearchText.Value != "" && txtCourseLongDescription.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1 && txtCourseLongDescription.Value.IndexOf("<span style=\"background-color:yellow\">") == -1){
						txtCourseLongDescription.Value = Regex.Replace(txtCourseLongDescription.Value, hidSearchText.Value, "<span style=\"background-color:yellow\">" + txtCourseLongDescription.Value.Substring(txtCourseLongDescription.Value.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", RegexOptions.IgnoreCase);
                    }

					//populate edit labels incase of revision
					lblRevisionCourseOffering.Text = hidOldCourseOffering.Value;
					divRevisionSubject.InnerHtml = courseSubject;
					divRevisionCatalogNbr.InnerHtml = txtCatalogNbr.Value;
					divRevisionCourseSuffix.InnerHtml = cboCourseSuffix.Value;
					divRevisionSubjectDescription.InnerHtml = divSubjectDescription.InnerHtml;
					hidOldSTRMSpan.Value = cboCourseBeginSTRM.SelectedValue + " - " + cboCourseEndSTRM.SelectedValue;
				}
				
				panEdit.Visible = true;
				panDisplay.Visible = false;
                */
			}
            else if(strTodo == "delete")
            {
				/*
				csCourse.DeleteCourse(Convert.ToInt32(Request.QueryString["course"]));
				if(hidSearchText.Value != null && hidSearchText.Value != ""){
					Response.Redirect("search.aspx?searchtxt=" + hidSearchText.Value.Replace("&","%26"));
				}else{
					Response.Redirect("edit.aspx?subject=" + Request.QueryString["cs"].Replace("&","%26") + "&strm=" + Request.QueryString["strm"]);
				}
                */
			}else if(strTodo == "revision"){
                /*
                String variableUnits = "N";
				String courseBeginSTRM = hidCourseBeginSTRM.Value; 
				String courseEndSTRM = hidCourseEndSTRM.Value; 
				String courseShortTitle = txtCourseShortTitle.Text; 
				String courseLongTitle = txtCourseLongTitle.Value; 
				String publishedCourse = optPublishedCourse.SelectedValue; 
				String descriptionCourseID = cboDescriptionCourse.Value; 
				String descriptionSubject = cboDescriptionSubject.Value; 
				String courseLongDescription = txtCourseLongDescription.Value.Replace("&lt;","<").Replace("&gt;",">").Replace("&quot;","\"").Replace("&amp;","&"); 
				String publishedCourseID = hidPublishedCourseID.Value; 
				String courseSubject = divRevisionSubject.InnerHtml;
				String courseOffering = courseSubject;
				String subjectDescription = csCourse.GetSubjectDescription(courseSubject);
				String catalogNbr = divRevisionCatalogNbr.InnerHtml;
				String courseSuffix = divRevisionCourseSuffix.InnerHtml;
                String strCollegeList = Request.Form["hidCollegeList"];
				courseLongDescription = courseLongDescription.Replace("&amp;","&");

				//remove text search highlighting before DB submit
				if(hidSearchText.Value != null && hidSearchText.Value != "" && courseLongDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1){
					courseLongDescription = Regex.Replace(courseLongDescription, "<span style=\"background-color:yellow\">" + courseLongDescription.Substring(courseLongDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", courseLongDescription.Substring(courseLongDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
				}

				Double unitsMinimum = Convert.ToDouble(txtUnitsMinimum.Value), unitsMaximum = 0;
				if(txtUnitsMaximum.Value == ""){
					unitsMaximum = unitsMinimum;
				}else{
					unitsMaximum = Convert.ToDouble(txtUnitsMaximum.Value);
				}

				//pad the dept div up to 5 spaces
				while(courseOffering.Length < 5){
					courseOffering += " ";
				}
				courseOffering = courseOffering.Replace(" ","&nbsp;") + catalogNbr + courseSuffix;

				//check if the credits are fixed or variable
				if(Request.Form["optVariableUnits" ] == "optVariableUnitsY"){
					variableUnits = "Y";
					txtUnitsMaximum.Disabled = false;
					optVariableUnitsY.Checked = true;
					optVariableUnitsN.Checked = false;
				}else{
					unitsMaximum = unitsMinimum;
					txtUnitsMaximum.Disabled = true;
					optVariableUnitsN.Checked = true;
					optVariableUnitsY.Checked = false;
				}
                
				//repopulate the source course combo boxes
				if(descriptionSubject != ""){
					DataSet dsDescriptionCourses = csCourse.GetDescriptionCourses(descriptionSubject);
					cboDescriptionCourse.Items.Clear();
					cboDescriptionCourse.Items.Add("");
					for(Int32 intDSRow = 0; intDSRow < dsDescriptionCourses.Tables[0].Rows.Count; intDSRow++){
						String dsDescriptionCourseID = dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseID"].ToString();
						cboDescriptionCourse.Items.Add(new ListItem(dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseOffering"].ToString() + " " + dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseBeginSTRM"].ToString(), dsDescriptionCourseID));
						if(descriptionCourseID == dsDescriptionCourseID){
							cboDescriptionCourse.Items[intDSRow + 1].Selected = true;
						}
					}

				}else{
					cboDescriptionCourse.Items.Clear();
					cboDescriptionCourse.Items.Add(new ListItem("Select a Long Description Subject", ""));
				}

				//repopulate form values
				divCourseOffering.InnerHtml = courseOffering;
				cboCourseBeginSTRM.SelectedValue = courseBeginSTRM;
				cboCourseEndSTRM.SelectedValue = courseEndSTRM;
				cboCourseSubject.Value = courseSubject;
				txtCatalogNbr.Value = catalogNbr;
				cboCourseSuffix.Value = courseSuffix;
				divSubjectDescription.InnerHtml = subjectDescription;
				txtCourseShortTitle.Text = courseShortTitle;
				txtCourseLongTitle.Value = courseLongTitle;
				txtUnitsMinimum.Value = unitsMinimum.ToString();
				txtUnitsMaximum.Value = unitsMaximum.ToString();
				txtCourseLongDescription.Value = courseLongDescription;	
				cboDescriptionSubject.Value = descriptionSubject;

				//insert new version of the course
                Int32 courseID = csCourse.AddCourse(courseOffering, courseBeginSTRM, courseEndSTRM, courseSubject, catalogNbr, courseSuffix, courseShortTitle, courseLongTitle, variableUnits, unitsMinimum, unitsMaximum, publishedCourse, csCourse.Strip(courseLongDescription).Replace("&nbsp;", " "), descriptionSubject, descriptionCourseID, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]);
				
				if(courseID > 0){

                    csCourse.DeleteCourseColleges(courseID);
                    if (strCollegeList != "")
                    {
                        String[] aryCollegeList = strCollegeList.Split(',');
                        for (Int16 i = 0; i < aryCollegeList.Length; i++)
                        {
                            csCourse.AddCollegeCourse(Convert.ToInt32(aryCollegeList[i]), courseID);
                        }
                    }

					Response.Redirect("edit.aspx?todo=edit&strm=" + cboTerm.SelectedValue + "&course=" + courseID.ToString() + "&searchtxt=" + hidSearchText.Value.Replace("&","%26") + "&sameas=");
				}else if(courseID == -1){
					//get beg and end term of overlapping course
					String termSpan = csCourse.GetConflictingTermsForNewCourse(courseOffering.Replace("&nbsp;"," "), courseBeginSTRM, courseEndSTRM);

					//display error message
					lblErrorMsg.Text = "Error: The term entered overlaps an existing " + courseOffering + " course";
					if(termSpan != ""){
						lblErrorMsg.Text += " effective " + termSpan + ".";
					}else{
						lblErrorMsg.Text = "Error: The term entered overlaps the previous \"" + courseOffering + "\" course effective " + hidOldSTRMSpan.Value + ".";
					}

					panError.Visible = true;
				}else if(courseID == 0){
					lblErrorMsg.Text = "Error: The insert failed.";
					panError.Visible = true;
				}
                */
			}else{ 
				
				if(selectedSubject == null || selectedSubject != cboSubject.SelectedValue){
					selectedSubject = cboSubject.SelectedValue;
				}

				DataSet dsCourses = csCourse.GetCurrentAndFutureCourses(selectedSubject, cboTerm.SelectedValue);
                DataTable dtCourses = dsCourses.Tables[0];
                Int32 intRowCtr = dtCourses.Rows.Count;

				if(intRowCtr > 0)
                {
                    foreach (DataRow drCourse in dtCourses.Rows)
                    {
                        String courseOffering = drCourse["CourseOffering"].ToString().Replace(" ", "&nbsp;");
                        String CATALOG_PRINT = drCourse["CATALOG_PRINT"].ToString();
                        TableRow tr = new TableRow();

                        TableCell td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
                        td.Controls.Add(new LiteralControl("<a name=\"" + courseOffering + "\">" + courseOffering + "</a>"));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
                        td.Controls.Add(new LiteralControl(drCourse["DESCR"].ToString()));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
                        td.Controls.Add(new LiteralControl(Convert.ToDateTime(drCourse["EFFDT"]).ToShortDateString()));
                        tr.Cells.Add(td);
                        /*
                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
                        td.Controls.Add(new LiteralControl(""));
                        tr.Cells.Add(td);
                        */
                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;text-align:right;vertical-align:top;";
                        td.Controls.Add(new LiteralControl(CATALOG_PRINT)); //Status Column
                        tr.Cells.Add(td);

                        tblDisplay.Rows.Add(tr);

                    }
                    /*
					for(Int32 intDSRow = 0; intDSRow < dsCourses.Tables[0].Rows.Count; intDSRow++){
						Int32 courseID = Convert.ToInt32(dsCourses.Tables[0].Rows[intDSRow]["CourseID"]);
						String courseOffering = dsCourses.Tables[0].Rows[intDSRow]["CourseOffering"].ToString().Replace(" ","&nbsp;");
						String beginTerm = dsCourses.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString();
						String endTerm = dsCourses.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString();
						String strCourses = csCourse.GetCoursesUsingSource(courseID), publishedCourse = "";
						String publishedCourseID = dsCourses.Tables[0].Rows[intDSRow]["PublishedCourseID"].ToString();
						Int16 intPublishedCourse = Convert.ToInt16(dsCourses.Tables[0].Rows[intDSRow]["PublishedCourse"]);

						if(!csCourse.HasWorkingCourseCopy(courseID)){
							
							if(intPublishedCourse == 0){
								publishedCourse = "Working Copy";
							}else if(intPublishedCourse == 1){
								publishedCourse = "Published Copy";
							}

							TableRow tr = new TableRow();

							TableCell td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
							td.Controls.Add(new LiteralControl("<a name=\"" + courseID + "\">" + courseOffering + "</a>"));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
							td.Controls.Add(new LiteralControl(dsCourses.Tables[0].Rows[intDSRow]["DESCR"].ToString()));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
							td.Controls.Add(new LiteralControl(beginTerm));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
							td.Controls.Add(new LiteralControl(endTerm));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;text-align:right;vertical-align:top;";
							td.Controls.Add(new LiteralControl(publishedCourse));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:10px;";
							td.Controls.Add(new LiteralControl("<a href=\"edit.aspx?todo=edit&strm=" + cboTerm.SelectedValue + "&course=" + courseID.ToString() + "&sameas=" + publishedCourseID + "\">Edit</a>"));

                            if (!csCourse.UsedInProgram(courseOffering.Replace("&nbsp;"," "))) {
								if(strCourses == ""){
									td.Controls.Add(new LiteralControl("&nbsp;|&nbsp;<a href=\"edit.aspx?todo=delete&cs=" + cboSubject.SelectedValue.Replace("&","%26") + "&strm=" + cboTerm.SelectedValue + "&course=" + courseID.ToString() + "\" onclick=\"return confirm('Are you sure you want to delete " + courseOffering + " " + beginTerm + "?');\">Delete</a>"));
								}else{
									td.Controls.Add(new LiteralControl("&nbsp;|&nbsp;<a href=\"#\" onclick=\"alert('" + courseOffering + " " + beginTerm + " cannot be deleted because the following courses are using " + courseOffering + " " + beginTerm + " as the source for their descriptions.\\n" + strCourses + "');\">Delete</a>"));
								}
							}

							tr.Cells.Add(td);

							tblDisplay.Rows.Add(tr);
						}
					}
                    */
				}
                else
                {

					TableRow tr = new TableRow();

					TableCell td = new TableCell();
					td.ColumnSpan = 6;
					td.CssClass = "portletLight";
					td.Attributes["style"] = "padding-left:5px;padding-top:4px;padding-bottom:4px;width:100%;";
					td.Controls.Add(new LiteralControl(selectedSubject + " returned 0 results."));

					tr.Cells.Add(td);

					tblDisplay.Rows.Add(tr);
				}

				panDisplay.Visible = true;
				panEdit.Visible = false;
			}
		}

		protected void cmdCancel_Click(object sender, System.EventArgs e){
			if(hidSearchText.Value != null && hidSearchText.Value != ""){
				Response.Redirect("search.aspx?searchtxt=" + hidSearchText.Value.Replace("&","%26"));
			}else{
				Response.Redirect("edit.aspx?subject=" + cboCourseSubject.Value.Replace("&","%26") + "&strm=" + cboTerm.SelectedValue + "&#" + hidCourseID.Value);
			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			String variableUnits = "N";
			String courseBeginSTRM = Request.Form["cboCourseBeginSTRM"];
			String courseEndSTRM = Request.Form["cboCourseEndSTRM"];
			String courseShortTitle = Request.Form["txtCourseShortTitle"];
			String courseLongTitle = Request.Form["txtCourseLongTitle"];
			String publishedCourse = Request.Form["optPublishedCourse"];
			Double unitsMinimum = Convert.ToDouble(Request.Form["txtUnitsMinimum"]), unitsMaximum = Convert.ToDouble(Request.Form["txtUnitsMaximum"]);
			String descriptionCourseID = Request.Form["cboDescriptionCourse"];
			String descriptionSubject = Request.Form["cboDescriptionSubject"];
			String courseLongDescription = Request.Form["txtCourseLongDescription"].Replace("&lt;","<").Replace("&gt;",">").Replace("&quot;","\"").Replace("&amp;","&");
			Int32 courseID = Convert.ToInt32(Request.Form["hidCourseID"]);
			String publishedCourseID = Request.QueryString["sameas"];
			String courseOffering, courseSubject, subjectDescription, catalogNbr, courseSuffix;
	
			//remove text search highlighting before DB submit
			if(hidSearchText.Value != null && hidSearchText.Value != "" && courseLongDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()) > -1){
				courseLongDescription = Regex.Replace(courseLongDescription, "<span style=\"background-color:yellow\">" + courseLongDescription.Substring(courseLongDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length) + "</span>", courseLongDescription.Substring(courseLongDescription.ToLower().IndexOf(hidSearchText.Value.ToLower()), hidSearchText.Value.Length), RegexOptions.IgnoreCase);
			}

			if(chkRevision.Checked){ //if the edit is a course revision
				courseSubject = divRevisionSubject.InnerHtml;
				courseOffering = courseSubject;
				subjectDescription = csCourse.GetSubjectDescription(courseSubject);
				catalogNbr = divRevisionCatalogNbr.InnerHtml;
				courseSuffix = divRevisionCourseSuffix.InnerHtml;
			}else{ //if house keeping
			    courseSubject = Request.Form["cboCourseSubject"];
				courseOffering = courseSubject;
				subjectDescription = csCourse.GetSubjectDescription(courseSubject);
				catalogNbr = Request.Form["txtCatalogNbr"];
				courseSuffix = Request.Form["cboCourseSuffix"];
			}

			//pad the dept div up to 5 spaces
			while(courseOffering.Length < 5){
				courseOffering += " ";
			}
			courseOffering = courseOffering.Replace(" ","&nbsp;") + catalogNbr + courseSuffix;

			//check if the credits are fixed or variable
			if(Request.Form["optVariableUnits" ] == "optVariableUnitsY"){
				variableUnits = "Y";
				txtUnitsMaximum.Disabled = false;
				optVariableUnitsY.Checked = true;
				optVariableUnitsN.Checked = false;
			}else{
				unitsMaximum = unitsMinimum;
				txtUnitsMaximum.Disabled = true;
				optVariableUnitsN.Checked = true;
				optVariableUnitsY.Checked = false;
			}

			//repopulate the source course combo boxes
            if (descriptionSubject != null && descriptionSubject != "") {
				DataSet dsDescriptionCourses = csCourse.GetDescriptionCourses(descriptionSubject);
				cboDescriptionCourse.Items.Clear();
				cboDescriptionCourse.Items.Add("");
				for(Int32 intDSRow = 0; intDSRow < dsDescriptionCourses.Tables[0].Rows.Count; intDSRow++){
					String dsDescriptionCourseID = dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseID"].ToString();
                    cboDescriptionCourse.Items.Add(new ListItem(dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseOffering"].ToString() + " " + dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseBeginSTRM"].ToString(), dsDescriptionCourseID));
					if(descriptionCourseID == dsDescriptionCourseID){
						cboDescriptionCourse.Items[intDSRow + 1].Selected = true;
					}
				}
			}else{
				cboDescriptionCourse.Items.Clear();
                cboDescriptionCourse.Items.Add(new ListItem("Select a Long Description Subject", ""));
			}

			//repopulate form values
			divCourseOffering.InnerHtml = courseOffering;
			cboCourseBeginSTRM.SelectedValue = courseBeginSTRM;
			cboCourseEndSTRM.SelectedValue = courseEndSTRM;
			cboCourseSubject.Value = courseSubject;
			txtCatalogNbr.Value = catalogNbr;
			cboCourseSuffix.Value = courseSuffix;
			divSubjectDescription.InnerHtml = subjectDescription;
			txtCourseShortTitle.Text = courseShortTitle;
			txtCourseLongTitle.Value = courseLongTitle;
			txtUnitsMinimum.Value = unitsMinimum.ToString();
			txtUnitsMaximum.Value = unitsMaximum.ToString();
			txtCourseLongDescription.Value = courseLongDescription;	
			cboDescriptionSubject.Value = descriptionSubject;

			//update the course
			Int32 newCourseID = csCourse.EditCourse(courseID, Request.Form["hidOldCourseOffering"].Replace("&nbsp;"," "), courseOffering.Replace("&nbsp;"," "), courseBeginSTRM, courseEndSTRM, courseSubject, catalogNbr, courseSuffix, courseShortTitle, courseLongTitle, variableUnits, unitsMinimum, unitsMaximum, publishedCourse, publishedCourseID, csCourse.Strip(courseLongDescription).Replace("&nbsp;"," "), descriptionSubject, descriptionCourseID, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]);

			if(newCourseID > 0){
                //delete college course join data
                csCourse.DeleteCourseColleges(newCourseID);
                if(hidCollegeList.Value != ""){
                    String[] strCollegeList = hidCollegeList.Value.Split(',');
                    for (Int16 i = 0; i < strCollegeList.Length; i++)
                    {
                        csCourse.AddCollegeCourse(Convert.ToInt32(strCollegeList[i]), newCourseID);
                    }
                }

				if(hidSearchText.Value != null && hidSearchText.Value != ""){
					Response.Redirect("search.aspx?searchtxt=" + hidSearchText.Value.Replace("&","%26"));
				}else{
					Response.Redirect("edit.aspx?subject=" + cboCourseSubject.Value.Replace("&","%26") + "&strm=" + cboTerm.SelectedValue + "&#" + hidCourseID.Value);
				}

			}else if(newCourseID == -1){
				//get begin and end terms of overlapping course
				String termSpan = csCourse.GetConflictingTermsForExistingCourse(courseID, courseOffering.Replace("&nbsp;"," "), courseBeginSTRM, courseEndSTRM, publishedCourseID);

				//display error message
				lblErrorMsg.Text = "Error: The begin and end terms entered overlap an existing \"" + courseOffering + "\" course";
				if(termSpan != ""){
					lblErrorMsg.Text += " effective " + termSpan + ".";
				}else{
					lblErrorMsg.Text += ".";
				}

				panError.Visible = true;
			}else if(newCourseID == 0){
				lblErrorMsg.Text = "Error: The update failed.";
				panError.Visible = true;
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
