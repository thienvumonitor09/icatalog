USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCareerPlanningGuide]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/2/17
-- Description:	Returns career planning guide details for a program
--		 INPUT:	ProgramVersionID - Program version primary key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCareerPlanningGuide] 
	@ProgramVersionID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT CareerPlanningGuideID, ProgramTitle, DegreeLongTitle, DegreeShortTitle, CategoryTitle, ProgramBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, ProgramEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, ProgramDisplay, CollegeShortTitle, ProgramEnrollment, ProgramWebsiteURL, CareerPlanningGuideFormat, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, PublishedCareerPlanningGuide, PublishedCareerPlanningGuideID 
	FROM CareerPlanningGuide 
	INNER JOIN Program 
	ON Program.ProgramVersionID = CareerPlanningGuide.ProgramVersionID 
	INNER JOIN ProgramDegree
	ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID
	INNER JOIN Degree
	ON Degree.DegreeID = ProgramDegree.DegreeID
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = ProgramBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = ProgramEndSTRM
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID 
	LEFT OUTER JOIN ProgramCategory
	ON ProgramCategory.ProgramVersionID = Program.ProgramVersionID
	LEFT OUTER JOIN Category 
	ON Category.CategoryID = ProgramCategory.CategoryID
	WHERE Program.ProgramVersionID = @ProgramVersionID
	ORDER BY PublishedCareerPlanningGuide;
END

GO
