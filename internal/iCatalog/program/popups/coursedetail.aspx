﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="coursedetail.aspx.cs" Inherits="program_popups_coursedetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
	<link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
    <title>Course Detail</title>
</head>
<body style="background: #ffffff url('');">
    <form id="frmCourseDetail" runat="server">
        <table class="centeredTable" style="width:350px;" cellpadding="0" cellspacing="0">
			<tr>
				<td style="padding-left:4px;padding-right:0px;">
					<table style="width:100%;" cellpadding="0" cellspacing="0">
						<tr>
							<td style="background-color:#000000;text-align:left;">
								<table style="width:100%;" cellpadding="1" cellspacing="1">
									<tr>
										<td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;" class="portletHeader">
											<table style="width:100%;" cellpadding="0" cellspacing="0">
												<tr>
													<td class="portletHeader" style="width:100%;">&nbsp;Course Detail</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td class="portletMain" style="width:100%">
											<table style="width:100%;" cellspacing="0" cellpadding="0">
												<tr>
													<td class="portletDark" style="width:100%;">
														<table cellspacing="1" cellpadding="0" style="width:100%;">
															<tr>
																<td class="portletLight" style="width:100%;vertical-align:text-top;padding:10px 10px 5px 10px;">
																	<p><strong>Course Offering:</strong>&nbsp;<asp:Label ID="lblCourseOffering" runat="server"></asp:Label></p>
                                                                    <p><strong>Description:</strong>&nbsp;<asp:Label ID="lblCourseShortTitle" runat="server"></asp:Label></p>
                                                                    <p><strong>Offered at:</strong>&nbsp;<asp:Label ID="lblOfferedAt" runat="server"></asp:Label></p>
                                                                    <p><strong>Term:</strong>&nbsp;<asp:Label id="lblTerm" runat="server"></asp:Label></p>
                                                                    <p><strong>Contact Hours Lecture:</strong>&nbsp;<asp:Label ID="lblContactHrLec" runat="server"></asp:Label></p>
                                                                    <p><strong>Contact Hours Lab</strong>:&nbsp;<asp:Label ID="lblContactHrLab" runat="server"></asp:Label></p>
                                                                    <p><strong>Contact Hours Clinical:</strong>&nbsp;<asp:Label ID="lblContactHrClin" runat="server"></asp:Label></p>
                                                                    <p><strong>Contact Hours Other:</strong>&nbsp;<asp:Label ID="lblContactHrOthr" runat="server"></asp:Label></p>
                                                                    <p><strong>Course Pay Type:</strong>&nbsp;<asp:Label ID="lblCoursPayType" runat="server"></asp:Label></p>
                                                                    <p><strong>Institutional Intent:</strong>&nbsp;<asp:Label ID="lblInstitInt" runat="server"></asp:Label></p>
                                                                    <p><strong>Misc3:</strong>&nbsp;<asp:Label ID="lblMisc3" runat="server"></asp:Label></p>
                                                                    <p><strong>Long Description:</strong><br /><asp:Label ID="lblCourseLongDescription" runat="server"></asp:Label></p>
                                                                    <div style="width:100%;text-align:center;padding:10px;"><input type="button" id="cmdClose" value="Close Window" class="small" style="background:#ccccaa;border:1px solid #000000;width:85px;" onclick="self.close();" /></div>
																</td>
														    </tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>                                                
    </form>
</body>
</html>
