USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 3/8/17
-- Description:	Returns Course Details
--		 INPUT:	CourseID - Course Primary Key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseLegacy] 
	@CourseID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT Course.*, SubjectArea.DESCR AS SubjectArea_DESCR, BeginTerm.DESCR AS BeginTerm_DESCR, EndTerm.DESCR AS EndTerm_DESCR
	FROM Course 
	INNER JOIN SubjectArea
	ON SubjectArea.SUBJECT = Course.SUBJECT
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = CourseBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = CourseEndSTRM
	WHERE CourseID = @CourseID
	
END

GO
