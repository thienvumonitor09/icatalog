using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.dept
{
	/// <summary>
	/// Summary description for edit.
	/// </summary>
	public partial class edit : System.Web.UI.Page
	{

		courseData csCourse = new courseData();

		protected void Page_Load(object sender, System.EventArgs e) {
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panEdit.Visible = false;
			panDisplay.Visible = false;

			if(!IsPostBack || hidTodo.Value == "search"){
				GenerateSubjectList();
				panDisplay.Visible = true;
			}else if(hidTodo.Value == "edit"){
				hidTodo.Value = "";
				DataSet dsSubject = csCourse.GetSubjectForEdit(hidSubject.Value);	
				if(dsSubject.Tables[0].Rows.Count > 0){
					lblSubject.Text = dsSubject.Tables[0].Rows[0]["SUBJECT"].ToString();
					txtSubjectDescription.Text = dsSubject.Tables[0].Rows[0]["DESCR"].ToString();
				}
				panEdit.Visible = true;
			}else if(hidTodo.Value == "delete"){
				hidTodo.Value = "";
				csCourse.DeleteSubject(hidSubject.Value);
				GenerateSubjectList();
				panDisplay.Visible = true;
			}
		}

		public void GenerateSubjectList(){
			TableRow tr = new TableRow();
			TableCell td = new TableCell();

			for(Int16 intAlphaCtr = 65; intAlphaCtr <= 90; intAlphaCtr++){
				td = new TableCell();
				td.Attributes["style"] = "width:23px;text-align:center;";
				td.Attributes["onclick"] = "document.getElementById(\"hidTodo\").value='search';document.getElementById(\"hidLtr\").value='" + Convert.ToChar(intAlphaCtr) + "';document.frmDept.submit();";
				td.CssClass = "portletMain";
				td.Controls.Add(new LiteralControl("<a class=\"ltr\" href=\"#\" onclick=\"document.getElementById(\"hidTodo\").value='search';document.frmDept.submit();\">" + Convert.ToChar(intAlphaCtr) + "</a>"));
				tr.Cells.Add(td);
			}

			tblAlpha.Rows.Add(tr);

			String strLtr = hidLtr.Value;
			if(strLtr == ""){
				String strRequestLtr = Request.QueryString["ltr"];
				if(strRequestLtr == null || strRequestLtr == ""){
					strLtr = "A";
				}else{
					strLtr = strRequestLtr;
				}
			}

            DataSet dsSubjects = csCourse.GetSubjectsForDisplay(Convert.ToChar(strLtr));
            DataTable dtSubjects = dsSubjects.Tables[0];
			//Int32 intRowCtr = dsSubjects.Tables[0].Rows.Count;

			tr = new TableRow();

			td = new TableCell();
			td.Attributes["style"] = "text-align:center;width:140px;font-weight:bold;padding-top:4px;padding-bottom:4px;";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("Subject Area"));

			tr.Cells.Add(td);

			td = new TableCell();
			td.Attributes["style"] = "text-align:center;width:385px;font-weight:bold;padding-top:4px;padding-bottom:4px;";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("Subject Area Description"));

			tr.Cells.Add(td);

            /*
			td = new TableCell();
			td.Attributes["style"] = "width:68px;text-align:center;";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("<a href=\"add.aspx?ltr=" + hidLtr.Value + "\">Add New</a>"));
            */
			tr.Cells.Add(td);
			tblDisplay.Rows.Add(tr);

			if(dtSubjects.Rows.Count > 0){
				foreach(DataRow drSubject in dtSubjects.Rows){
					String subject = drSubject["SUBJECT"].ToString(), strConfirm = "";
                    Int16 intCourseCtr = Convert.ToInt16(drSubject["CrsCount"]);
                    if (intCourseCtr > 0){
						strConfirm = subject + " has " + intCourseCtr + " course(s).\\nAre you sure you want to delete " + subject + " and all of its courses?";
					}else{
						strConfirm = "Are you sure you want to delete " + subject + "?";
					}

					tr = new TableRow();

					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl(subject));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl(drSubject["DESCR"].ToString()));

					tr.Cells.Add(td);

                    /*
					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;";
					td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.getElementById('hidTodo').value='edit';document.getElementById('hidSubject').value='" + subject + "';document.frmDept.submit();\">Edit</a>"));
					if(!csCourse.UsedInProgram(subject)){
						td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"if(confirm('" + strConfirm + "')){document.getElementById('hidTodo').value='delete';document.getElementById('hidSubject').value='" + subject + "';document.frmDept.submit();}\">Delete</a>"));
					}
                    */
					tr.Cells.Add(td);
					tblDisplay.Rows.Add(tr);
				}
			}else{
				tr = new TableRow();
				td = new TableCell();
				td.ColumnSpan = 3;
				td.CssClass = "portletLight";
				td.Attributes["style"] = "padding-top:4px;padding-bottom:4px;";
				td.Controls.Add(new LiteralControl("&nbsp;" + strLtr + " returned 0 results."));
				tr.Cells.Add(td);
				tblDisplay.Rows.Add(tr);
			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			csCourse.EditSubject(hidSubject.Value, txtSubjectDescription.Text);
			GenerateSubjectList();
			panDisplay.Visible = true;
			panEdit.Visible = false;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
