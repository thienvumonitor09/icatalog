using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.course
{
	/// <summary>
	/// Summary description for archive.
	/// </summary>
	public partial class archive : System.Web.UI.Page
	{

		courseData csCourse = new courseData();
        permissionData csPermissions = new permissionData();
		termData csTerm = new termData();

		#region PROTECTED CONTROLS
		protected System.Web.UI.HtmlControls.HtmlSelect cboCourseSubject;
		#endregion

		protected void Page_Load(object sender, System.EventArgs e) {
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panView.Visible = false;
			panError.Visible = false;
			panDisplay.Visible = true;
			
			String strTodo = Request.QueryString["todo"], selectedSubject = Request.QueryString["subject"];
			if(strTodo == "view" && hidTodo.Value == "revision"){
				strTodo = "revision";
			}

			if(!IsPostBack){				
				DataSet dsTerms = csTerm.GetPastTerms();
				cboTerm.Items.Add(new ListItem("ALL", ""));
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
                    cboTerm.Items.Add(new ListItem(dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString(), dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString()));
				}
			}

            cboSubject.Items.Clear();
            DataSet dsSubjects = csCourse.GetEffectiveSubjects(cboTerm.SelectedValue);
            for (Int32 intDSRow = 0; intDSRow < dsSubjects.Tables[0].Rows.Count; intDSRow++)
            {
                String subject = dsSubjects.Tables[0].Rows[intDSRow]["SUBJECT"].ToString().Replace("&", "");
                if (!cboSubject.Items.Contains(new ListItem(subject)))
                {
                    cboSubject.Items.Add(subject);
                }

                try
                {
                    cboSubject.SelectedValue = Request.Form["cboSubject"];
                }
                catch
                {
                    //do nothing
                }
            }

			if(strTodo == "view"){
                string CRSE_ID = Request.QueryString["CRSE_ID"];
                string effdtStr = Request.QueryString["EFFDT"];
                DataSet dsCourse = csCourse.GetCourse(CRSE_ID, effdtStr);
                DataTable dtCourse = dsCourse.Tables[0];

                if (dtCourse != null && dtCourse.Rows.Count > 0)
                {
                    DataRow drCourse = dtCourse.Rows[0];
                    String strOfferedAt = "", descriptionCourseOffering = "", variableUnits = "";
                    if (Convert.ToDouble(drCourse["UNITS_MINIMUM"]).CompareTo(Convert.ToDouble(drCourse["UNITS_MAXIMUM"])) == 0)
                    {
                        variableUnits = "Fixed";
                    }
                    else
                    {
                        variableUnits = "Variable";
                    }
                    lblEFFDT.Text = Convert.ToDateTime(drCourse["EFFDT"]).ToShortDateString();
                    lblCourseOffering.Text = drCourse["CourseOffering"].ToString();
                    lblCourseSubject.Text = drCourse["SUBJECT"].ToString();
                    lblSubjectDescription.Text = drCourse["DESCR"].ToString();
                    lblCatalogNbr.Text = drCourse["CATALOG_NBR"].ToString();
                    lblCourseShortTitle.Text = drCourse["DESCR"].ToString();
                    lblCourseLongTitle.Text = drCourse["COURSE_TITLE_LONG"].ToString();
                    lblVariableUnits.Text = variableUnits;
                    lblUnitsMaximum.Text = drCourse["UNITS_MAXIMUM"].ToString();
                    lblUnitsMinimum.Text = drCourse["UNITS_MINIMUM"].ToString();
                    lblCourseLongDescription.Text = dsCourse.Tables[0].Rows[0]["DESCRLONG"].ToString();

                    /*
                    lblCourseBeginSTRM.Text = dsCourse.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString();
                    lblCourseEndSTRM.Text = dsCourse.Tables[0].Rows[0]["EndTerm_DESCR"].ToString();
                    lblCourseSubject.Text = dsCourse.Tables[0].Rows[0]["SUBJECT"].ToString();
                    lblSubjectDescription.Text = dsCourse.Tables[0].Rows[0]["DESCR"].ToString();
                    lblCatalogNbr.Text = dsCourse.Tables[0].Rows[0]["CATALOG_NBR"].ToString();
                    lblCourseShortTitle.Text = dsCourse.Tables[0].Rows[0]["DESCR"].ToString();
                    lblCourseSuffix.Text = dsCourse.Tables[0].Rows[0]["CourseSuffix"].ToString();
                    lblCourseLongTitle.Text = dsCourse.Tables[0].Rows[0]["COURSE_TITLE_LONG"].ToString();
                    lblVariableUnits.Text = variableUnits;
                    lblUnitsMaximum.Text = dsCourse.Tables[0].Rows[0]["UNITS_MAXIMUM"].ToString();
                    lblUnitsMinimum.Text = dsCourse.Tables[0].Rows[0]["UNITS_MINIMUM"].ToString();
                    lblOfferedAt.Text = strOfferedAt;
                    lblDescriptionCourseOffering.Text = descriptionCourseOffering;
                    lblCourseLongDescription.Text = dsCourse.Tables[0].Rows[0]["DESCRLONG"].ToString();
                    lblLastModifiedEMPLID.Text = csPermissions.GetEmpName(dsCourse.Tables[0].Rows[0]["LastModifiedEMPLID"].ToString());
                    lblLastModifiedDate.Text = dsCourse.Tables[0].Rows[0]["LastModifiedDate"].ToString();
                    */
                    lblPublishedCourse.Text = (drCourse["CATALOG_NBR"].ToString().Equals("Y")) ? "Published Copy" : "Working Copy";
                }
                    

                /*
				Int32 courseID = Convert.ToInt32(Request.QueryString["course"]);
				DataSet dsCourse = csCourse.GetCourse(courseID);

				if(dsCourse.Tables[0].Rows.Count > 0){
					String strOfferedAt = "", descriptionCourseOffering = "", variableUnits = dsCourse.Tables[0].Rows[0]["VariableUnits"].ToString(), descriptionCourseID = dsCourse.Tables[0].Rows[0]["DescriptionCourseID"].ToString(), publishedCourse = dsCourse.Tables[0].Rows[0]["PublishedCourse"].ToString();

					if(variableUnits == "Y"){
						variableUnits = "Variable";
						hidVariableUnits.Value = "Y";
					}else{
						variableUnits = "Fixed";
						hidVariableUnits.Value = "N";
					}
					if(descriptionCourseID.Length > 0){
						descriptionCourseOffering = csCourse.GetCourseOffering(Convert.ToInt32(descriptionCourseID));
						panSameAs.Visible = true;
					}else{
						panSameAs.Visible = false;
					}
					if(publishedCourse == "0"){
						publishedCourse = "Working Copy";
					}else if(publishedCourse == "1"){
						publishedCourse = "Published Copy";
					}

                    DataSet dsCourseCollege = csCourse.GetCourseCollege(courseID); 
                    for (Int32 i = 0; i < dsCourseCollege.Tables[0].Rows.Count; i++)
                    {
                        strOfferedAt += ", " + dsCourseCollege.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
                        hidCollegeList.Value += "," + dsCourseCollege.Tables[0].Rows[i]["CollegeID"].ToString();
                    }
                    if (strOfferedAt != "")
                    {
                        strOfferedAt = strOfferedAt.Substring(2);
                    }
                    if (hidCollegeList.Value.IndexOf(",") == 0)
                    {
                        hidCollegeList.Value = hidCollegeList.Value.Substring(1, hidCollegeList.Value.Length - 1);
                    }

					lblCourseOffering.Text = dsCourse.Tables[0].Rows[0]["CourseOffering"].ToString().Replace(" ","&nbsp;");
					lblCourseBeginSTRM.Text = dsCourse.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString();
					lblCourseEndSTRM.Text = dsCourse.Tables[0].Rows[0]["EndTerm_DESCR"].ToString();
					lblCourseSubject.Text = dsCourse.Tables[0].Rows[0]["SUBJECT"].ToString();
					lblSubjectDescription.Text = dsCourse.Tables[0].Rows[0]["DESCR"].ToString();
					lblCatalogNbr.Text = dsCourse.Tables[0].Rows[0]["CATALOG_NBR"].ToString();
					lblCourseShortTitle.Text = dsCourse.Tables[0].Rows[0]["DESCR"].ToString();
					lblCourseSuffix.Text = dsCourse.Tables[0].Rows[0]["CourseSuffix"].ToString();
					lblCourseLongTitle.Text = dsCourse.Tables[0].Rows[0]["COURSE_TITLE_LONG"].ToString();
					lblVariableUnits.Text = variableUnits;
					lblUnitsMaximum.Text = dsCourse.Tables[0].Rows[0]["UNITS_MAXIMUM"].ToString();
					lblUnitsMinimum.Text = dsCourse.Tables[0].Rows[0]["UNITS_MINIMUM"].ToString();
					lblOfferedAt.Text = strOfferedAt;
					lblDescriptionCourseOffering.Text = descriptionCourseOffering;
					lblCourseLongDescription.Text = dsCourse.Tables[0].Rows[0]["DESCRLONG"].ToString();	
					lblLastModifiedEMPLID.Text = csPermissions.GetEmpName(dsCourse.Tables[0].Rows[0]["LastModifiedEMPLID"].ToString());
					lblLastModifiedDate.Text = dsCourse.Tables[0].Rows[0]["LastModifiedDate"].ToString();	
					lblPublishedCourse.Text = publishedCourse;
			
					hidCourseID.Value = courseID.ToString();
					hidPublishedCourseID.Value = dsCourse.Tables[0].Rows[0]["PublishedCourseID"].ToString();
					hidPublishedCourse.Value = publishedCourse;
					hidCourseOffering.Value = lblCourseOffering.Text;
					hidCourseBeginSTRM.Value = lblCourseBeginSTRM.Text;
					hidCourseEndSTRM.Value = lblCourseEndSTRM.Text;
					hidDescriptionCourseID.Value = dsCourse.Tables[0].Rows[0]["DescriptionCourseID"].ToString();
				}
                */
                panView.Visible = true;
				panDisplay.Visible = false;
                
			}else if(strTodo == "delete"){
				
				csCourse.DeleteCourse(Convert.ToInt32(Request.QueryString["course"]));
				Response.Redirect("archive.aspx?subject=" + Request.QueryString["cs"]);

			}else if(strTodo == "revision")
            {

				//insert new version of the course
				Int32 courseID = csCourse.AddCourse(lblCourseOffering.Text, hidCourseBeginSTRM.Value, hidCourseEndSTRM.Value, lblCourseSubject.Text, lblCatalogNbr.Text, lblCourseSuffix.Text, lblCourseShortTitle.Text, lblCourseLongTitle.Text, hidVariableUnits.Value, Convert.ToDouble(lblUnitsMinimum.Text), Convert.ToDouble(lblUnitsMaximum.Text), "0", csCourse.Strip(lblCourseLongDescription.Text).Replace("&nbsp;", " "), lblCourseSubject.Text, hidDescriptionCourseID.Value, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]);
				
				if(courseID > 0){
                    csCourse.DeleteCourseColleges(courseID);
                    if (hidCollegeList.Value != "")
                    {
                        String[] aryCollegeList = hidCollegeList.Value.Split(',');
                        for (Int16 i = 0; i < aryCollegeList.Length; i++)
                        {
                            csCourse.AddCollegeCourse(Convert.ToInt32(aryCollegeList[i]), courseID);
                        }
                    }

					Response.Redirect("edit.aspx?todo=edit&strm=" + cboTerm.SelectedValue + "&course=" + courseID.ToString() + "&sameas=");
				}else if(courseID == -1){
					//get beg and end terms of overlapping course
					String termSpan = csCourse.GetConflictingTermsForNewCourse(lblCourseOffering.Text.Replace("&nbsp;"," "), hidCourseBeginSTRM.Value, hidCourseEndSTRM.Value);

					//display error message
					lblErrorMsg.Text = "Error: The term entered overlaps an existing " + lblCourseOffering.Text + " course effective " + termSpan + ".";
					panError.Visible = true;

				}else if(courseID == 0){
					lblErrorMsg.Text = "Error: The insert failed.";
					panError.Visible = true;
				}
			
			}
            
            else
            {
                
			
				if(selectedSubject == null || selectedSubject != cboSubject.SelectedValue){
					selectedSubject = cboSubject.SelectedValue;
				}
                
				DataSet dsCourses = csCourse.GetCourseArchives(selectedSubject, cboTerm.SelectedValue);
                DataTable dtCourses = dsCourses.Tables[0];
                //Int32 intRowCtr = dsCourses.Tables[0].Rows.Count;
                
				if(dtCourses != null || dtCourses.Rows.Count > 0)
                {
                    foreach (DataRow drCourse in dtCourses.Rows)
                    {
                        String courseOffering = drCourse["CourseOffering"].ToString().Replace(" ", "&nbsp;");
                        
                        string publishedCourse = "";
                        if (drCourse["CATALOG_PRINT"].ToString().Equals("N"))
                        {
                            publishedCourse = "Working Copy";
                        }
                        else 
                        {
                            publishedCourse = "Published Copy";
                        }
                        TableRow tr = new TableRow();

                        TableCell td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
                        td.Controls.Add(new LiteralControl("<a name=\"" + courseOffering + "\">" + courseOffering + "</a>"));

                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
                        td.Controls.Add(new LiteralControl(drCourse["DESCR"].ToString()));

                        tr.Cells.Add(td);

                        

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
                        td.Controls.Add(new LiteralControl(Convert.ToDateTime(drCourse["EFFDT"]).ToShortDateString()));

                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
                        td.Controls.Add(new LiteralControl("End term"));

                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;text-align:right;vertical-align:top;";
                        td.Controls.Add(new LiteralControl(publishedCourse));

                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:7px;";
                        var paramStr = String.Format("CRSE_ID={0}&EFFDT={1}", drCourse["CRSE_ID"].ToString(), Convert.ToDateTime(drCourse["EFFDT"]).ToShortDateString());
                        td.Controls.Add(new LiteralControl("<a href=\"archive.aspx?todo=view&" + paramStr + "\">View</a>"));
                        //td.Controls.Add(new LiteralControl("&nbsp;|&nbsp;<a href=\"edit.aspx?todo=edit&strm=" + cboTerm.SelectedValue + "&course=" + courseID.ToString() + "&sameas=" + publishedCourseID + "\">Edit</a>"));

                        

                        tr.Cells.Add(td);

                        tblDisplay.Rows.Add(tr);
                    }
                    /*
					for(Int32 intDSRow = 0; intDSRow < dsCourses.Tables[0].Rows.Count; intDSRow++){
						Int32 courseID = Convert.ToInt32(dsCourses.Tables[0].Rows[intDSRow]["CourseID"]);
						String courseOffering = dsCourses.Tables[0].Rows[intDSRow]["CourseOffering"].ToString().Replace(" ","&nbsp;");
						String beginTerm = dsCourses.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString();
						String endTerm = dsCourses.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString();
						String strCourses = csCourse.GetCoursesUsingSource(courseID), publishedCourse = "";
						String publishedCourseID = dsCourses.Tables[0].Rows[intDSRow]["PublishedCourseID"].ToString();
						Int16 intPublishedCourse = Convert.ToInt16(dsCourses.Tables[0].Rows[intDSRow]["PublishedCourse"]);

						if(!csCourse.HasWorkingCourseCopy(courseID)){
							
							if(intPublishedCourse == 0){
								publishedCourse = "Working Copy";
							}else if(intPublishedCourse == 1){
								publishedCourse = "Published Copy";
							}

							TableRow tr = new TableRow();

							TableCell td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
							td.Controls.Add(new LiteralControl("<a name=\"" + courseID + "\">" + courseOffering + "</a>"));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
							td.Controls.Add(new LiteralControl(dsCourses.Tables[0].Rows[intDSRow]["DESCR"].ToString()));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
							td.Controls.Add(new LiteralControl(beginTerm));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;";
							td.Controls.Add(new LiteralControl(endTerm));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;text-align:right;vertical-align:top;";
							td.Controls.Add(new LiteralControl(publishedCourse));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:7px;";
							td.Controls.Add(new LiteralControl("<a href=\"archive.aspx?todo=view&course=" + courseID.ToString() + "\">View</a>"));
                            //td.Controls.Add(new LiteralControl("&nbsp;|&nbsp;<a href=\"edit.aspx?todo=edit&strm=" + cboTerm.SelectedValue + "&course=" + courseID.ToString() + "&sameas=" + publishedCourseID + "\">Edit</a>"));

                            if (!csCourse.UsedInProgram(courseOffering.Replace("&nbsp;"," "))) {
								if(strCourses == ""){
									td.Controls.Add(new LiteralControl("&nbsp;|&nbsp;<a href=\"archive.aspx?todo=delete&cs=" + cboSubject.SelectedValue + "&course=" + courseID.ToString() + "\" onclick=\"return confirm('Are you sure you want to delete " + courseOffering + " " + beginTerm + "?');\">Delete</a>"));
								}else{
									td.Controls.Add(new LiteralControl("&nbsp;|&nbsp;<a href=\"#\" onclick=\"alert('" + courseOffering + " " + beginTerm + " cannot be deleted because the following courses are using " + courseOffering + " " + beginTerm + " as the source for their long descriptions.\\n" + strCourses + "');\">Delete</a>"));
								}
							}

							tr.Cells.Add(td);

							tblDisplay.Rows.Add(tr);
						}
					}
                    */
				}else{

					TableRow tr = new TableRow();

					TableCell td = new TableCell();
					td.ColumnSpan = 6;
					td.CssClass = "portletLight";
					td.Attributes["style"] = "padding-left:5px;padding-top:4px;padding-bottom:4px;width:100%;";
					td.Controls.Add(new LiteralControl(selectedSubject + " returned 0 results."));

					tr.Cells.Add(td);

					tblDisplay.Rows.Add(tr);
				}

				panDisplay.Visible = true;
				panView.Visible = false;
                
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {    
		}
		#endregion
	}
}

