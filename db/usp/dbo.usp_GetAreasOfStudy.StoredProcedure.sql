USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAreasOfStudy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 1/12/18
-- Description:	Get Areas of Study offering degrees/certificates for a selected quarter
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetAreasOfStudy]
	@STRM varchar(4) = NULL,
	@Institution varchar(5) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	/*Store the current term STRM value*/ 
	IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
							FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
							WHERE TERM_END_DT > GETDATE()
							ORDER BY TERM_END_DT)

    SELECT DISTINCT AreaOfStudy.AreaOfStudyID, Title, WebsiteURL, Institution 
	FROM AreaOfStudy
	INNER JOIN AreaOfStudyProgram
	ON AreaOfStudyProgram.AreaOfStudyID = AreaOfStudy.AreaOfStudyID
	INNER JOIN Program
	ON Program.ProgramID = AreaOfStudyProgram.ProgramID
	WHERE (ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM)) 
	AND PublishedProgram = 1
	AND (@Institution IS NULL OR Institution = @Institution)
	ORDER BY Title

END
GO
