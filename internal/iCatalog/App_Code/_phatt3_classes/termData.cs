using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;

namespace ICatalog._phatt3_classes{
	/// <summary>
	/// Summary description for termData.
	/// </summary>
	public class termData{

		private SqlConnection objCon;
		private String strSQL, ctcLinkConnection, iCatalogConnection;
		private SqlCommand objCmd;
		private SqlDataAdapter objDA;
		private DataSet objDS;

		public termData()
		{
            //ctcLinkConnection = "Initial Catalog=CCSGen_ctcLink_ODS;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=InternalICatalog;Integrated Security=true;";
            ctcLinkConnection = "Initial Catalog=CCSGen_ctcLink_ODS;Data Source=CCS-SQL-Dev;Workstation ID=InternalICatalog;Integrated Security=true;";
            //development connection string
            //iCatalogConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCS-SQL-Dev;Workstation ID=InternalICatalog;Integrated Security=true;";
            iCatalogConnection = "Initial Catalog=CCSICatalog;Data Source=CCS-SQL-Dev;Workstation ID=InternalICatalog;Integrated Security=true;";

            //production connection string
            //iCatalogConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=InternalICatalog;Integrated Security=true;";
            //iCatalogConnection = "Initial Catalog=CCSICatalog;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=InternalICatalog;Integrated Security=true;";
        }


        /// <summary>
        /// Get the current term STRM value - if inbetween quarters/terms, the next term is returned
        /// </summary>
        /// <returns>String: STRM (Term code)</returns>
		public String GetCurrentTerm(){

			objCon = new SqlConnection(ctcLinkConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_ClassSchedule_GetCurrentTerm", objCon);
                objCmd.CommandType = CommandType.StoredProcedure;
                return objCmd.ExecuteScalar().ToString();

            }catch{
                return "";
			}finally{
				objCon.Close();
			}	
		}


		/// <summary>
		/// Returns a term description (Example: Spring 2016)
		/// </summary>
		/// <param name="STRM">String: Term code (Example: 2163)</param>
		/// <returns>String: DESCR (Example: Spring 2016)</returns>
		public String GetTermDescription(String STRM){

			objCon = new SqlConnection(ctcLinkConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_ClassSchedule_GetTermDesc", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;

                SqlParameter objParam = new SqlParameter();
                objParam.ParameterName = "@RETURNVAL";
                objParam.SqlDbType = SqlDbType.VarChar;
                objParam.Size = 30;
                objParam.Direction = ParameterDirection.Output;
                objCmd.Parameters.Add(objParam);
                objCmd.ExecuteScalar();

                return objCmd.Parameters["@RETURNVAL"].Value.ToString();

			}finally{
				objCon.Close();
			}	
		}
		

		/// <summary>
        /// Gets terms later than Summer 2000 and earlier than the current term
		/// </summary>
		/// <returns>
        /// DataSet 
        /// Field List: STRM (Example: 2173)
        ///             DESCR (Example: SPRING 2017)
        /// </returns>
		public DataSet GetPastTerms(){

			String strCurrentTerm = GetCurrentTerm();
			objCon = new SqlConnection(iCatalogConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_GetTermsInRange", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@STRMStart", SqlDbType.VarChar);
                objCmd.Parameters["@STRMStart"].Value = "2005";

                objCmd.Parameters.Add("@STRMEnd", SqlDbType.VarChar);
                objCmd.Parameters["@STRMEnd"].Value = strCurrentTerm;

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);

                return objDS;

			}finally{
				objCon.Close();
			}
		}


		/// <summary>
        /// Gets terms later than Summer 2000
        /// </summary>
        /// <returns>
        /// DataSet 
        /// Field List: STRM (Example: 2173)
        ///             DESCR (Example: SPRING 2017)
        /// </returns>
		public DataSet GetTerms(){
			
			objCon = new SqlConnection(iCatalogConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_GetTermsInRange", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@STRMStart", SqlDbType.VarChar);
                objCmd.Parameters["@STRMStart"].Value = "2005";

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);

                return objDS;

			}finally{
				objCon.Close();
			}
		}


        /// <summary>
        /// Returns term description, start date, and end date
        /// </summary>
        /// <param name="STRM">String: Term code (Example: 2163)</param>
        /// <returns>
        /// DataSet 
        /// Field List: DESCR - Term Description (Example: SPRING 2017) 
        ///             TERM_BEGIN_DT - Term Begin Date
        ///             TERM_END_DT - Term End Date
        /// </returns>
		public DataSet GetTermDetails(String STRM){

			objCon = new SqlConnection(iCatalogConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_GetTermDetails", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);

                return objDS;

			}finally{
				objCon.Close();
			}
		}

	}
}
