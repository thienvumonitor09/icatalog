USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseArchivesLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 4/4/17
-- Description:	Returns a list of archived courses by subject area and term
--		 INPUT: SUBJECT (optional) - Course Subject Area: ENGL
--				STRM (optional) - Term Code	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseArchivesLegacy]
	@SUBJECT varchar(5) = NULL,
	@STRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	/*Store the current term STRM value*/ 
	DECLARE @CurrentSTRM varchar(4);
	SET @CurrentSTRM = (SELECT TOP 1(STRM)
						FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
						WHERE TERM_END_DT > GETDATE()
						ORDER BY TERM_END_DT)

    SELECT DISTINCT CourseID, CourseOffering, CATALOG_NBR, CourseBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, CourseEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, SUBJECT, Course.DESCR, PublishedCourse, PublishedCourseID 
	FROM Course
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = CourseBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = CourseEndSTRM
	WHERE (@SUBJECT IS NULL OR (SUBJECT = @SUBJECT OR SUBJECT = @SUBJECT + '&'))
	AND ((@STRM IS NOT NULL AND (CourseBeginSTRM <= @STRM AND CourseEndSTRM >= @STRM AND CourseEndSTRM < @CurrentSTRM))
	OR  (@STRM IS NULL AND (CourseBeginSTRM < @CurrentSTRM AND CourseEndSTRM < @CurrentSTRM))) 
	ORDER BY CATALOG_NBR, CourseBeginSTRM DESC, PublishedCourse DESC;
END

GO
