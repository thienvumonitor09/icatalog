USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseOfferingTermsLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 4/28/17
-- Description:	Returns the begin and end term for a course 
--		 INPUT: CourseOffering - Subject area padded to five spaces + course suffix + Catalog Nbr (Example: ENG  101)
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseOfferingTermsLegacy] 
	@CourseOffering varchar(9)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT CourseID, CourseBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, CourseEndSTRM, EndTerm.DESCR AS EndTerm_DESCR
	FROM Course 
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = CourseBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = CourseEndSTRM
	WHERE CourseOffering = @CourseOffering
	ORDER BY CourseBeginSTRM;
END

GO
