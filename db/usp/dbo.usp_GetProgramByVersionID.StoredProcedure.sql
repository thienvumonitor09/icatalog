USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramByVersionID]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/9/17
-- Description:	Returns program details by program version id
--		 INPUT: ProgramVersionID - program version primary key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramByVersionID]
	@ProgramVersionID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT Program.ProgramVersionID, ProgramTitle, Program.CollegeID, CollegeShortTitle, CareerPlanningGuideID, ProgramEnrollment, ProgramWebsiteURL, ProgramDescription, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, ProgramBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, ProgramEndSTRM, EndTerm.DESCR AS EndTerm_DESCR
	FROM Program
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = ProgramBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = ProgramEndSTRM
	LEFT OUTER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
	AND PublishedCareerPlanningGuide = '1' 
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID 
	WHERE Program.ProgramVersionID = @ProgramVersionID
END

GO
