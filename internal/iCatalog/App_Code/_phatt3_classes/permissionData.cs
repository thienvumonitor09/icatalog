using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Collections;

namespace ICatalog._phatt3_classes{

	/// <summary>
	/// Summary description for iCatalogData.
	/// </summary>
	public class permissionData {

		private SqlConnection objCon;
		private String strSQL, strConnection;
		private SqlCommand objCmd;
		private SqlDataAdapter objDA;
		private DataSet objDS;

		public permissionData() {
            //development connection string
            //strConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCS-SQL-Dev;Workstation ID=InternalICatalog;Integrated Security=true;";
            strConnection = "Initial Catalog=CCSICatalog;Data Source=CCS-SQL-Dev;Workstation ID=InternalICatalog;Integrated Security=true;";

            //production connection string
            //strConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=InternalICatalog;Integrated Security=true;";
            //strConnection = "Initial Catalog=CCSICatalog;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=InternalICatalog;Integrated Security=true;";
        }


        /// <summary>
        /// Takes an EMPLID and returns an employee's full name
        /// </summary>
        /// <param name="employeeID">String: Employee's EMPLID</param>
        /// <returns>String: Employee's full name</returns>
		public String GetEmpName(String employeeID){

			objCon = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionStringCCSSQLGen"]);

			try{

				objCon.Open();
                objCmd = new SqlCommand("usp_GetEmpInfoByEmplID", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@pEmplID", SqlDbType.VarChar, 9);
                objCmd.Parameters["@pEmplID"].Value = employeeID;
                objCmd.Parameters.Add("@pEmpName", SqlDbType.VarChar, 30);
                objCmd.Parameters["@pEmpName"].Direction = ParameterDirection.Output;
                objCmd.Parameters.Add("@pEmpType", SqlDbType.VarChar, 3); 
                objCmd.Parameters["@pEmpType"].Direction = ParameterDirection.Output;
                objCmd.Parameters.Add("@pDeptName", SqlDbType.VarChar, 30); 
                objCmd.Parameters["@pDeptName"].Direction = ParameterDirection.Output;

                try{
                    objCmd.ExecuteNonQuery();
                    return objCmd.Parameters["@pEmpName"].Value.ToString();
                }catch{
                    return "";
                }

			}finally{
				objCon.Close();
			}
		}


		public DataSet GetCourseUserPermissionByPK(Int32 permissionID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT EMPLID, Name, Permission " +
					"FROM CoursePermissions " +
					"WHERE PermissionID = " + permissionID + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/*********************************** DeleteCourseUserPermission *****************************************
		 * INPUT:
		 * permissionID - primary key
		 * 
		 * OUTPUT: bool
		 *		   true - delete was successful
		 *		   false - delete failed
		 * 
		 * USED IN: /iCatalog/course/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION: Deletes a user's permission.
		 * 
		 ********************************************************************************************************/
		public bool DeleteCourseUserPermission(Int32 permissionID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "DELETE CoursePermissions " +
					"WHERE PermissionID = " + permissionID + ";";

				objCmd = new SqlCommand(strSQL, objCon);

				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();

				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		/************************************ GetCoursePermissions *********************************************
		 * INPUT: none 
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: PermissionID - primary key
		 *             EMPLID - employee ID
		 *             Permission - 1 = Admin, 2 = Edit/Delete, 3 = Read Only
		 * 
		 * USED IN: /iCatalog/course/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION: Retrieves all user permissions for display in the edit/delete list.
		 * 
		 ********************************************************************************************************/
		public DataSet GetCoursePermissions(){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT * FROM CoursePermissions " + 
						 "ORDER BY Permission, Name;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/********************************* GetCourseUserPermissionByEmployeeID ****************************************
		 * INPUT:
		 * employeeID - employee ID
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: PermissionID - Primary Key
		 *             Permission - 1 = Admin, 2 = Edit/Delete, 3 = Read Only
		 * 
		 * USED IN: /iCatalog/course/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION: Used to check if user already has permissions before inserting.
		 * 
		 ********************************************************************************************************/
		public DataSet GetCourseUserPermissionByEmployeeID(String employeeID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();
			
				strSQL = "SELECT PermissionID, Permission " +
					"FROM CoursePermissions " +
					"WHERE EMPLID = '" + employeeID + "';";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/************************************ EditCourseUserPermission ******************************************
		 * INPUT:
		 * permissionID - primary key
		 * permission - 1 = Admin, 2 = Edit/Delete, 3 = Read Only 
		 * 
		 * OUTPUT: bool
		 *		   true - update was successful
		 *		   false - update failed
		 * 
		 * USED IN: /iCatalog/course/grantpermissisons.aspx.cs
		 * 
		 * DESCRIPTION: Updates a user's permission.
		 * 
		 ********************************************************************************************************/
		public bool EditCourseUserPermission(Int32 permissionID, Char permission){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "UPDATE CoursePermissions " +
					"SET Permission = '" + permission + "' " +
					"WHERE PermissionID = " + permissionID + ";";

				objCmd = new SqlCommand(strSQL, objCon);

				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();
				
				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		/*********************************** AddCourseUserPermission ******************************************
		 * INPUT:
		 * employeeID - employee ID
		 * permission - 1 = Admin, 2 = Edit/Delete, 3 = Read Only
		 * 
		 * OUTPUT: bool
		 *		   true - the insert was successful
		 *		   false - the insert failed
		 * 
		 * USED IN: /iCatalog/course/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION: Inserts a user's permission.
		 * 
		 ********************************************************************************************************/
		public bool AddCourseUserPermission(String employeeID, String strName, Char permission){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "INSERT INTO CoursePermissions(EMPLID, Name, Permission) " +
					"VALUES('" + employeeID + "','" + strName + "','" + permission + "');";

				objCmd = new SqlCommand(strSQL, objCon);

				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();

				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		public DataSet GetCredUserPermissionByPK(Int32 permissionID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT EMPLID, Name, Permission " +
					"FROM CredentialPermissions " +
					"WHERE PermissionID = " + permissionID + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/*********************************** DeleteCredUserPermission *****************************************
		 * INPUT:
		 * permissionID - primary key
		 * 
		 * OUTPUT: bool
		 *		   true - delete was successful
		 *		   false - delete failed
		 * 
		 * USED IN: /iCatalog/cred/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION: Deletes a user's permission.
		 * 
		 ********************************************************************************************************/
		public bool DeleteCredUserPermission(Int32 permissionID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "DELETE CredentialPermissions " +
					"WHERE PermissionID = " + permissionID + ";";

				objCmd = new SqlCommand(strSQL, objCon);

				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();

				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		/************************************ GetCredentialPermissions *********************************************
		 * INPUT: none 
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: PermissionID - primary key
		 *             EMPLID - employee ID
		 *             Permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * USED IN: /iCatalog/cred/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION: Retrieves all user permissions for display in the edit/delete list.
		 * 
		 ********************************************************************************************************/
		public DataSet GetCredentialPermissions(){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT * FROM CredentialPermissions " + 
						 "ORDER BY Permission, Name;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/********************************* GetCredUserPermissionByEmployeeID ****************************************
		 * INPUT:
		 * employeeID - employee ID
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: PermissionID - Primary Key
		 *             Permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * USED IN: 
		 * cred/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Used to check if user already has permissions before inserting.
		 ********************************************************************************************************/
		public DataSet GetCredUserPermissionByEmployeeID(String employeeID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();
			
				strSQL = "SELECT PermissionID, Permission " +
					"FROM CredentialPermissions " +
					"WHERE EMPLID = '" + employeeID + "';";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/************************************ EditCredUserPermission ******************************************
		 * INPUT:
		 * permissionID - primary key
		 * permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * OUTPUT: bool
		 * true - update was successful
		 * false - update failed
		 * 
		 * USED IN: 
		 * cred/grantpermissisons.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Updates a user's permission.
		 ********************************************************************************************************/
		public bool EditCredUserPermission(Int32 permissionID, Char permission){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "UPDATE CredentialPermissions " +
					"SET Permission = '" + permission + "' " +
					"WHERE PermissionID = " + permissionID + ";";

				objCmd = new SqlCommand(strSQL, objCon);

				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();
				
				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		/*********************************** AddCredUserPermission ******************************************
		 * INPUT:
		 * employeeID - employee ID
		 * permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * OUTPUT: bool
		 * true - the insert was successful
		 * false - the insert failed
		 * 
		 * USED IN: 
		 * cred/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Inserts a user's permission.
		 ********************************************************************************************************/
		public bool AddCredUserPermission(String employeeID, String strName, Char permission){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "INSERT INTO CredentialPermissions(EMPLID, Name, Permission) " +
					"VALUES('" + employeeID + "','" + strName + "','" + permission + "');";

				objCmd = new SqlCommand(strSQL, objCon);

				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();

				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		public DataSet GetProgUserPermissionByPK(Int32 permissionID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT EMPLID, Name, Permission " +
					"FROM ProgramPermissions " +
					"WHERE PermissionID = " + permissionID + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/*********************************** DeleteProgUserPermission *****************************************
		 * INPUT:
		 * permissionID - primary key
		 * 
		 * OUTPUT: bool
		 * true - delete was successful
		 * false - delete failed
		 * 
		 * USED IN: 
		 * program/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Deletes a user's permission.
		 ********************************************************************************************************/
		public bool DeleteProgUserPermission(Int32 permissionID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "DELETE ProgramPermissions " +
					"WHERE PermissionID = " + permissionID + ";";

				objCmd = new SqlCommand(strSQL, objCon);

				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();

				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		/************************************ GetProgramPermissions *********************************************
		 * INPUT: none 
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: PermissionID - primary key
		 *             EMPLID - employee ID
		 *             Permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * USED IN: 
		 * program/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Retrieves all user permissions for display in the edit/delete list.
		 ********************************************************************************************************/
		public DataSet GetProgramPermissions(){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT * FROM ProgramPermissions " +
						 "ORDER BY Permission, Name;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/********************************* GetProgUserPermissionByEmployeeID ****************************************
		 * INPUT:
		 * employeeID - employee ID
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: PermissionID - Primary Key
		 *             Permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * USED IN: 
		 * program/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Used to check if user already has permissions before inserting.
		 ********************************************************************************************************/
		public DataSet GetProgUserPermissionByEmployeeID(String employeeID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();
			
				strSQL = "SELECT PermissionID, Permission " +
					"FROM ProgramPermissions " +
					"WHERE EMPLID = '" + employeeID + "';";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/************************************ EditProgUserPermission ******************************************
		 * INPUT:
		 * permissionID - primary key
		 * permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * OUTPUT: bool
		 * true - update was successful
		 * false - update failed
		 * 
		 * USED IN: 
		 * program/grantpermissisons.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Updates a user's permission.
		 ********************************************************************************************************/
		public bool EditProgUserPermission(Int32 permissionID, Char permission){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "UPDATE ProgramPermissions " +
					"SET Permission = '" + permission + "' " +
					"WHERE PermissionID = " + permissionID + ";";

				objCmd = new SqlCommand(strSQL, objCon);

				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();
				
				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		/*********************************** AddProgUserPermission ******************************************
		 * INPUT:
		 * employeeID - employee id
		 * permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * OUTPUT: bool
		 * true - the insert was successful
		 * false - the insert failed
		 * 
		 * USED IN: 
		 * program/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Inserts a user's permission.
		 ********************************************************************************************************/
		public bool AddProgUserPermission(String employeeID, String strName, Char permission){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "INSERT INTO ProgramPermissions(EMPLID, Name, Permission) " +
					"VALUES('" + employeeID + "','" + strName + "','" + permission + "');";

				objCmd = new SqlCommand(strSQL, objCon);

				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();

				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


        public DataSet GetCollegeUserPermissionByPK(Int32 permissionID){

            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                strSQL = "SELECT EMPLID, Name, Permission " +
                    "FROM CollegePermissions " +
                    "WHERE PermissionID = " + permissionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            }finally{
                objCon.Close();
            }
        }


        /*********************************** DeleteProgUserPermission *****************************************
		 * INPUT:
		 * permissionID - primary key
		 * 
		 * OUTPUT: bool
		 * true - delete was successful
		 * false - delete failed
		 * 
		 * USED IN: 
		 * college/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Deletes a user's permission.
		 ********************************************************************************************************/
        public bool DeleteCollegeUserPermission(Int32 permissionID){

            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                strSQL = "DELETE CollegePermissions " +
                    "WHERE PermissionID = " + permissionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                Int32 intCtr = 0;
                intCtr = objCmd.ExecuteNonQuery();

                if (intCtr == 0){
                    return false;
                }else{
                    return true;
                }

            }finally{
                objCon.Close();
            }
        }


        /********************************* GetCollegeUserPermissionByEmployeeID ****************************************
		 * INPUT:
		 * employeeID - employee ID
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: PermissionID - Primary Key
		 *             Permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * USED IN: 
		 * college/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Used to check if user already has permissions before inserting.
		 ********************************************************************************************************/
        public DataSet GetCollegeUserPermissionByEmployeeID(String employeeID){

            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                strSQL = "SELECT PermissionID, Permission " +
                    "FROM CollegePermissions " +
                    "WHERE EMPLID = '" + employeeID + "';";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            }finally{
                objCon.Close();
            }
        }


        /************************************ EditCollegeUserPermission ******************************************
		 * INPUT:
		 * permissionID - primary key
		 * permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * OUTPUT: bool
		 * true - update was successful
		 * false - update failed
		 * 
		 * USED IN: 
		 * college/grantpermissisons.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Updates a user's permission.
		 ********************************************************************************************************/
        public bool EditCollegeUserPermission(Int32 permissionID, Char permission){

            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                strSQL = "UPDATE CollegePermissions " +
                    "SET Permission = '" + permission + "' " +
                    "WHERE PermissionID = " + permissionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                Int32 intCtr = 0;
                intCtr = objCmd.ExecuteNonQuery();

                if (intCtr == 0){
                    return false;
                }else{
                    return true;
                }

            }finally{
                objCon.Close();
            }
        }


        /*********************************** AddCollegeUserPermission ******************************************
		 * INPUT:
		 * employeeID - employee ID
		 * permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * OUTPUT: bool
		 * true - the insert was successful
		 * false - the insert failed
		 * 
		 * USED IN: 
		 * college/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Inserts a user's permission.
		 ********************************************************************************************************/
        public bool AddCollegeUserPermission(String employeeID, String strName, Char permission){

            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                strSQL = "INSERT INTO CollegePermissions(EMPLID, Name, Permission) " +
                    "VALUES('" + employeeID + "','" + strName + "','" + permission + "');";

                objCmd = new SqlCommand(strSQL, objCon);

                Int32 intCtr = 0;
                intCtr = objCmd.ExecuteNonQuery();

                if (intCtr == 0){
                    return false;
                }else{
                    return true;
                }

            }finally{
                objCon.Close();
            }
        }


        /************************************ GetCollegePermissions *********************************************
		 * INPUT: none 
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: PermissionID - primary key
		 *             EMPLID - employee ID
		 *             Permission - 1 = Admin, 2 = Add/Edit/Delete
		 * 
		 * USED IN: 
		 * college/grantpermissions.aspx.cs
		 * 
		 * DESCRIPTION:
		 * Retrieves all user permissions for display in the edit/delete list.
		 ********************************************************************************************************/
        public DataSet GetCollegePermissions(){

            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                strSQL = "SELECT * FROM CollegePermissions " +
                         "ORDER BY Permission, Name;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            }finally{
                objCon.Close();
            }
        }
	}
}
