<%@ Page language="c#" Inherits="ICatalog.cred.add" CodeFile="add.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Add Credentials</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <script type="text/javascript" src="../_phatt3_src_files/trim.js"></script>
        <script type="text/javascript" src="../_phatt3_src_files/expand.js"></script>
        <script type="text/javascript">
        <!--
            //check field values on submit
            function validate(){
                isValid = false;
                 var chkSCC = document.getElementById("chkCredentialWorkUnit_0");
                 var chkSFCC = document.getElementById("chkCredentialWorkUnit_1");
                 var chkIEL = document.getElementById("chkCredentialWorkUnit_2");
                 var chkDIST = document.getElementById("chkCredentialWorkUnit_3");

                if(trim(document.frmCred.txtCredentialEmployeeID.value) == ""){
                    alert("Please enter a ctcLink ID.");
                    document.frmCred.txtCredentialEmployeeID.select();
                }else if(isNaN(document.frmCred.txtCredentialEmployeeID.value)){
                    alert("Please enter numbers only for the ctcLink ID.");
                    document.frmCred.txtCredentialEmployeeID.select();
                }else if(trim(document.frmCred.txtCredentialName.value) == ""){
                    alert("Please enter a name.");
                    document.frmCred.txtCredentialName.select();
                }else if(trim(document.frmCred.txtCredentialAssignment.value) == ""){
                    alert("Please enter assigned to.");
                    document.frmCred.txtCredentialAssignment.select();
                }else if(chkSCC.checked != true && chkSFCC.checked != true && chkIEL.checked != true && chkDIST.checked != true){
					alert("Please select a work unit.");
				}else if(document.frmCred.cboCredentialTenureTrack.value == ""){
					alert("Please select a tenure status.");
                }else if(trim(document.frmCred.txtCredentialDescription.value) == ""){
                    alert("Please enter a description.");
                    document.frmCred.txtCredentialDescription.select();
                }else{
                    isValid = true;
                }
                return isValid; 
            }
            
        //-->
        </script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmCred" runat="server">
					<input type="hidden" id="hidLtr" runat="server" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Add Credentials</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <asp:panel runat="server" id="panAdd">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <!-- Error Message -->
                                                                            <asp:panel runat="server" id="panError">
                                                                                <tr>
                                                                                    <td colspan="2" class="portletMain" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                        <asp:label runat="server" id="lblErrorMsg" style="color:red;"></asp:label>
                                                                                    </td>
                                                                                </tr>
                                                                            </asp:panel>
                                                                            <!-- ctcLink ID -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>ctcLink ID:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtCredentialEmployeeID" cssclass="small" maxlength="9" style="width:60px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- NAME -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Name:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtCredentialName" cssclass="small" maxlength="50" style="width:250px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- ASSIGNED TO -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Assigned To:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtCredentialAssignment" cssclass="small" maxlength="120" style="width:250px;"></asp:textbox>
                                                                                </td>
                                                                            </tr> 
                                                                            <!-- WORK UNIT -->
                                                                            <tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Work Unit:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;">
																					<asp:checkboxlist runat="server" id="chkCredentialWorkUnit" cssclass="small" RepeatDirection="Horizontal">
																						<asp:listitem value="SCC">SCC</asp:listitem>
																						<asp:listitem value="SFCC">SFCC</asp:listitem>
																						<asp:listitem value="IEL">IEL</asp:listitem>
																						<asp:listitem value="DIST">DIST</asp:listitem>
																					</asp:checkboxlist>
																				</td>
                                                                            </tr>
                                                                            <!-- TENURE TRACK -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Tenure Status:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:dropdownlist runat="server" id="cboCredentialTenureTrack" cssclass="small">
																						<asp:listitem value="Tenure Track">Tenure Track</asp:listitem>
                                                                                        <asp:listitem value="Non-Tenure Track">Non-Tenure Track</asp:listitem>
                                                                                        <asp:listitem Value="Executive">Executive</asp:listitem>
                                                                                        <asp:listitem value="Administrator">Administrator</asp:listitem>
                                                                                        <asp:listitem value="Non-Applicable">Non-Applicable</asp:listitem>
																					</asp:dropdownlist>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Credentials -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;">
                                                                                    <b>Credentials:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;padding-right:6px;">
                                                                                    <textarea runat="server" class="small" id="txtCredentialDescription" rows="3" cols="76" style="width:450px" ondblclick="expandTextArea(this.id,37);" title="Double click to expand this text area when the text overflows. Double click again to return to the original size."></textarea>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                        <input type="button" id="cmdCancel" value="Cancel" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="location.href='edit.aspx?ltr=' + document.frmCred.hidLtr.value;" />
															                        &nbsp;<asp:button id="cmdSubmit" runat="server" text="Save Addition" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclientclick="return validate();" onclick="cmdSubmit_Click"></asp:button>
														                        </td>
													                        </tr>
                                                                        </table>
                                                                     </asp:panel>
                                                                     <asp:panel runat="server" id="panConfirm">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <tr>
                                                                                <td class="portletMain" colspan="2" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                    The following credential has been successfully added.
                                                                                </td>
                                                                            </tr>
                                                                            <!-- ctcLink ID -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>ctcLink ID:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblCredentialEmployeeID"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- NAME -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Name:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblCredentialName"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- ASSIGNED TO -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Assigned To:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblCredentialAssignment"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- WORK UNIT -->
                                                                            <tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Work Unit:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:label runat="server" id="lblCredentialWorkUnit"></asp:label>
																				</td>
                                                                            </tr>
                                                                            <!-- TENURE TRACK -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Tenure Status:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblCredentialTenureTrack"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- DESCRIPTION -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:6px;">
                                                                                    <b>Description:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;padding-right:6px;">
                                                                                    <asp:label runat="server" id="lblCredentialDescription"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                        <input type="button" id="cmdOK" runat="server" value="OK" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="location.href='edit.aspx?ltr=' + document.frmCred.hidLtr.value;" />
														                        </td>
													                        </tr>
                                                                        </table>
                                                                     </asp:panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <div class="clearer"></div>
        </asp:panel>
    </body>
</html>
