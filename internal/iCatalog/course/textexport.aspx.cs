using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.course
{
	/// <summary>
	/// Summary description for textexport.
	/// </summary>
	public partial class textexport : System.Web.UI.Page
	{

		courseData csCourse = new courseData();
		termData csTerm = new termData();
		

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }
			
			if(!IsPostBack){
				DataSet dsTerms = csCourse.GetTermsForCourses();

				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
					cboTerm.Items.Add(new ListItem(dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString(), dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString()));
				}

				try{
					cboTerm.SelectedValue = csTerm.GetCurrentTerm();
				}catch{
					//do nothing
				}
			}

            //create and populate the college checkboxes
            programData csProgramData = new programData();
            DataSet dsCollegeList = csProgramData.GetCollegeList();
            panCollege.Controls.Clear();
            for (Int32 intDSRow = 0; intDSRow < dsCollegeList.Tables[0].Rows.Count; intDSRow++)
            {
                String collegeShortTitle = dsCollegeList.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
                String collegeID = dsCollegeList.Tables[0].Rows[intDSRow]["CollegeID"].ToString();

                HtmlInputCheckBox chkOfferedAt = new HtmlInputCheckBox();
                chkOfferedAt.Value = collegeID;
                chkOfferedAt.ID = "chk" + collegeID;
                chkOfferedAt.Name = "chkOfferedAt";

                String strSelectedCollege = Request.Form[chkOfferedAt.ID];

                if (strSelectedCollege != null && strSelectedCollege != "")
                {
                    chkOfferedAt.Checked = true;
                }

                chkOfferedAt.Attributes["onclick"] = "addCollege('" + chkOfferedAt.ID + "','" + collegeID + "');";
                panCollege.Controls.Add(new LiteralControl("&nbsp;"));
                panCollege.Controls.Add(chkOfferedAt);
                panCollege.Controls.Add(new LiteralControl(collegeShortTitle));
            }
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
            Response.Redirect(csCourse.ExportCoursesToText(hidCollegeList.Value, cboTerm.SelectedValue));
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
