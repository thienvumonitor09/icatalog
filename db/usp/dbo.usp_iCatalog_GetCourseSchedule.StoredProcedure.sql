USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_iCatalog_GetCourseSchedule]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/22/17
-- Description:	Returns a course description and class schedule (class schedule temporarily removed until converted to ctcLink ODS data)
--		 INPUT: CourseOffering - Course subject area padded to five spaces and course catalog number
--				STRM - Term code
--				CollegeID - College primary key
--				BeginSTRM - Program begin term code
--				EndSTRM - Program end term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_iCatalog_GetCourseSchedule] 
	@CourseOffering varchar(9),
	@STRM varchar(4),
	@CollegeID varchar(10) = NULL,
	@BeginSTRM varchar(4),
	@EndSTRM varchar(4)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT COURSE_TITLE_LONG, DESCRLONG, CourseID, UNITS_MINIMUM, UNITS_MAXIMUM, VariableUnits, CourseOffering, Course.SUBJECT, CATALOG_NBR, SubjectArea.DESCR AS SubjectArea_DESCR, 
		(SELECT DESCRLONG 
		FROM Course 
		WHERE CourseID IN 
		(SELECT DescriptionCourseID 
		FROM Course 
		WHERE CourseOffering = @CourseOffering 
		AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999')) 
		OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM AND (CourseEndSTRM <= @EndSTRM OR CourseEndSTRM = 'Z999'))) 
		AND (CourseBeginSTRM <= @STRM AND (CourseEndSTRM >= @STRM OR CourseEndSTRM = 'Z999')))) AS AlternateDESCRLONG 
	FROM Course 
	INNER JOIN SubjectArea 
	ON Course.SUBJECT = SubjectArea.SUBJECT 
	WHERE CourseOffering = @CourseOffering 
	AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999'))
	OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM AND (CourseEndSTRM <= @EndSTRM OR CourseEndSTRM = 'Z999'))) 
	AND (CourseBeginSTRM <= @STRM AND (CourseEndSTRM >= @STRM OR CourseEndSTRM = 'Z999')) 
END

GO
