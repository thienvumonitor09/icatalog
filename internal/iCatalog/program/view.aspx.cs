using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;
using System.Text.RegularExpressions;


namespace ICatalog.program
{
	/// <summary>
	/// Summary description for view.
	/// </summary>
	public partial class view : System.Web.UI.Page
	{

		programData csProgram = new programData();
        permissionData csPermissions = new permissionData();
		termData csTerm = new termData();

		#region PAGE LEVEL CONTROLS
		#endregion

		#region DISPLAY PANEL CONTROLS
		#endregion

		#region VIEW PANEL CONTROLS

		#region OUTLINE PANEL CONTROLS
		#endregion

		#region TEXT DISPLAY PANEL CONTROLS
		#endregion

		#endregion

		#region BUTTON PANEL CONTROLS
		#endregion

		#region CPG PANEL CONTROLS
		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			#region CHECK USER LOGIN

			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			#endregion

			#region HIDE PANELS AND CONTROLS

			panOutline.Visible = false;
			panTextDisplay.Visible = false;
			panDegTitle.Visible = false;
			panView.Visible = false;
			panButtons.Visible = false;
			panDisplay.Visible = false;
			panCPG.Visible = false;
			cmdCPG.Visible = false;

			#endregion

			if(!IsPostBack){
				DataSet dsTerms = csTerm.GetTerms();
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
                    cboTerm.Items.Add(new ListItem(dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString(), dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString()));
				}
				cboTerm.SelectedValue = Request.QueryString["strm"];

				DataSet dsCollege = csProgram.GetCollegeList();
				for(Int32 intDSRow = 0; intDSRow < dsCollege.Tables[0].Rows.Count; intDSRow++){
                    cboOfferedAt.Items.Add(new ListItem(dsCollege.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString(), dsCollege.Tables[0].Rows[intDSRow]["CollegeID"].ToString()));
				}
				cboOfferedAt.SelectedValue = Request.QueryString["colid"];

				String strProgramVersionID = Request.QueryString["pvid"];
				if(strProgramVersionID != null && strProgramVersionID != ""){
					hidProgramVersionID.Value = strProgramVersionID;
					hidProgramDisplay.Value = Request.QueryString["pd"];
					hidTodo.Value = "view";
				}
			}

			if(hidTodo.Value == "view"){
				
				String strSearchText = Request.QueryString["searchtxt"];
				String strSearchDept = Request.QueryString["searchsub"];
				String strSearchId = Request.QueryString["searchid"];
				String searchSTRM = Request.QueryString["searchstrm"];
				Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value);

				DataSet dsProgramDescription = csProgram.GetProgramDescription(programVersionID);
				if(dsProgramDescription.Tables[0].Rows.Count > 0){
					lblLastModifiedName.Text = csPermissions.GetEmpName(dsProgramDescription.Tables[0].Rows[0]["LastModifiedEMPLID"].ToString());
					lblLastModifiedDate.Text = dsProgramDescription.Tables[0].Rows[0]["LastModifiedDate"].ToString();
					lblProgramTitle.Text = dsProgramDescription.Tables[0].Rows[0]["ProgramTitle"].ToString();
					lblBeginTerm.Text = dsProgramDescription.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString();
					lblEndTerm.Text = dsProgramDescription.Tables[0].Rows[0]["EndTerm_DESCR"].ToString();
					lblOfferedAt.Text = dsProgramDescription.Tables[0].Rows[0]["CollegeShortTitle"].ToString();
					lblProgramDescription.Text = dsProgramDescription.Tables[0].Rows[0]["ProgramDescription"].ToString();
                    hidProgramBeginSTRM.Value = dsProgramDescription.Tables[0].Rows[0]["ProgramBeginSTRM"].ToString();
                    hidProgramEndSTRM.Value = dsProgramDescription.Tables[0].Rows[0]["ProgramEndSTRM"].ToString();
					if(strSearchText != null && strSearchText != "" && lblProgramDescription.Text.ToLower().IndexOf(strSearchText.ToLower()) > -1){
						lblProgramDescription.Text = Regex.Replace(dsProgramDescription.Tables[0].Rows[0]["ProgramDescription"].ToString(), strSearchText, "<span style=\"background-color: yellow\">" + lblProgramDescription.Text.Substring(lblProgramDescription.Text.ToLower().IndexOf(strSearchText.ToLower()), strSearchText.Length) + "</span>", RegexOptions.IgnoreCase);
					}

					DataSet dsCategories = csProgram.GetProgramCategories(programVersionID);
					for(Int32 intDSRow = 0; intDSRow < dsCategories.Tables[0].Rows.Count; intDSRow++){
						if(Convert.ToByte(dsCategories.Tables[0].Rows[intDSRow]["PrimaryCategory"]) == 1){
							lblCategory.Text = dsCategories.Tables[0].Rows[intDSRow]["CategoryTitle"].ToString();
						}else{
							lblAddCategories.Text += "; " + dsCategories.Tables[0].Rows[intDSRow]["CategoryTitle"].ToString();
						}
					}
					if(lblAddCategories.Text.IndexOf(";") == 0){
						lblAddCategories.Text = lblAddCategories.Text.Substring(1, lblAddCategories.Text.Length-1);
					}

					if(hidProgramDisplay.Value == "2"){

						DataSet dsProgramDegree = csProgram.GetProgramDegree(programVersionID);
						String degreeShortTitles = "";

						for(Int32 intDSRow = 0; intDSRow < dsProgramDegree.Tables[0].Rows.Count; intDSRow++){
							Int32 programDegreeID = Convert.ToInt32(dsProgramDegree.Tables[0].Rows[intDSRow]["ProgramDegreeID"]);
							String degreeShortTitle = dsProgramDegree.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
							degreeShortTitles += ", " + degreeShortTitle;
							String fileName = csProgram.GetDegreeRequirementWorksheetByProgramDegreeID(programDegreeID);
							if(fileName != null && fileName != ""){
								degreeShortTitles += ": <a href=\"#\" onclick=\"popupCenter('degworksheet/" + fileName + "','Requirements', 950, 950);\">Requirements/Worksheet</a>";
							}
						}
						degreeShortTitles = degreeShortTitles.Substring(2);

						lblProgText.Text = csProgram.GetProgramTextOutline(programVersionID);
						lblDegTitle.Text = degreeShortTitles;
						panDegTitle.Visible = true;
						panTextDisplay.Visible = true;

					}else if(hidProgramDisplay.Value == "1"){
						
						DataSet dsProgramDegree = csProgram.GetProgramDegree(programVersionID);

						for(Int32 i = 0; i< dsProgramDegree.Tables[0].Rows.Count; i++){
							Int32 programDegreeID = Convert.ToInt32(dsProgramDegree.Tables[0].Rows[i]["ProgramDegreeID"]);
							String degreeShortTitle = dsProgramDegree.Tables[0].Rows[i]["DegreeShortTitle"].ToString();

							//Add a blank row for space
							TableRow trProgOutline = new TableRow();

							TableCell tdProgOutline = new TableCell();
							tdProgOutline.Attributes["style"] = "width:100%;";
							tdProgOutline.CssClass = "portletLight";
							tdProgOutline.Controls.Add(new LiteralControl("&nbsp;"));

							trProgOutline.Cells.Add(tdProgOutline);
							tblProgOutline.Rows.Add(trProgOutline);

							//Display the degree title
							trProgOutline = new TableRow();

							tdProgOutline = new TableCell();
							tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:4px;padding-top:4px;padding-left:6px;";
							tdProgOutline.CssClass = "portletMain";
							tdProgOutline.Controls.Add(new LiteralControl("<b>" + degreeShortTitle + "</b>"));

							trProgOutline.Cells.Add(tdProgOutline);
							tblProgOutline.Rows.Add(trProgOutline);

							//Show the Degree Requirement/Worksheet if one exists
							String fileName = csProgram.GetDegreeRequirementWorksheetByProgramDegreeID(programDegreeID);
							if(fileName != null && fileName != ""){
								trProgOutline = new TableRow();

								tdProgOutline = new TableCell();
								tdProgOutline.Attributes["style"] = "width:100%;padding-top:10px;padding-left:6px;";
								tdProgOutline.CssClass = "portletLight";
								tdProgOutline.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('degworksheet/" + fileName + "','Requirements', 950, 950);\">Requirements/Worksheet</a>"));

								trProgOutline.Cells.Add(tdProgOutline);
								tblProgOutline.Rows.Add(trProgOutline);
							}

							//Add a blank row for space
							trProgOutline = new TableRow();

							tdProgOutline = new TableCell();
							tdProgOutline.Attributes["style"] = "width:100%;";
							tdProgOutline.CssClass = "portletLight";
							tdProgOutline.Controls.Add(new LiteralControl("&nbsp;"));

							trProgOutline.Cells.Add(tdProgOutline);
							tblProgOutline.Rows.Add(trProgOutline);

							//Get the program options for the selected degree
							DataSet dsProgOptions = csProgram.GetProgOptions(programDegreeID);

							for(Int32 intDSRow = 0; intDSRow < dsProgOptions.Tables[0].Rows.Count; intDSRow++){
								Int32 optionID = Convert.ToInt32(dsProgOptions.Tables[0].Rows[intDSRow]["OptionID"]);
								String optionTitle = dsProgOptions.Tables[0].Rows[intDSRow]["OptionTitle"].ToString();
								String optionStateApproval = dsProgOptions.Tables[0].Rows[intDSRow]["OptionStateApproval"].ToString();
								String optionCIP = dsProgOptions.Tables[0].Rows[intDSRow]["CIP"].ToString();
								String optionEPC = dsProgOptions.Tables[0].Rows[intDSRow]["EPC"].ToString();
                                String academicPlan = dsProgOptions.Tables[0].Rows[intDSRow]["ACAD_PLAN"].ToString();
								String gainfulEmploymentID = dsProgOptions.Tables[0].Rows[intDSRow]["GainfulEmploymentID"].ToString();
                                String totalQuarters = dsProgOptions.Tables[0].Rows[intDSRow]["TotalQuarters"].ToString();
								String optionDescription = dsProgOptions.Tables[0].Rows[intDSRow]["OptionDescription"].ToString();
								if(strSearchText != null && strSearchText != "" && optionDescription.ToLower().IndexOf(strSearchText.ToLower()) > -1){
									optionDescription = Regex.Replace(optionDescription, strSearchText, "<span style=\"background-color: yellow\">" + optionDescription.Substring(optionDescription.ToLower().IndexOf(strSearchText.ToLower()), strSearchText.Length) + "</span>", RegexOptions.IgnoreCase);
								}

								if(dsProgOptions.Tables[0].Rows[intDSRow]["PrimaryOption"].ToString() == "True"){
									lblPrimaryStateApproval.Text = optionStateApproval;
									lblPrimaryCIP.Text = optionCIP;
                                    lblPrimaryAcademicPlan.Text = academicPlan;
									lblPrimaryEPC.Text = optionEPC;
								}

								if(optionTitle != ""){
									//display the program option title
									trProgOutline = new TableRow();

									tdProgOutline = new TableCell();
									tdProgOutline.Attributes["style"] = "width:100%;padding-left:6px;padding-top:5px;border-bottom:1px solid black;font-weight:bold;";
									tdProgOutline.CssClass = "portletLight";
									tdProgOutline.Controls.Add(new LiteralControl(optionTitle));

									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);

									//add a blank row for space
									trProgOutline = new TableRow();
									tdProgOutline = new TableCell();
									tdProgOutline.Attributes["style"] = "padding-top:5px;";
									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);
								}

								if((dsProgOptions.Tables[0].Rows.Count > 0) && (optionStateApproval != "" || optionCIP != "" || optionEPC != "" || academicPlan != "")){
									//display option state approval, cip and epc
									String strDisplay = "";
									if(optionStateApproval != ""){
										strDisplay += "<b>State Approval Date:</b> " + optionStateApproval + "&nbsp;&nbsp;";
									}
									if(optionCIP != ""){
										strDisplay += "<b>CIP:</b> " + optionCIP + "&nbsp;&nbsp;";
									}
									if(optionEPC != ""){
										strDisplay += "<b>EPC:</b> " + optionEPC + "&nbsp;&nbsp;";
									}
                                    if (academicPlan != "") {
                                        strDisplay += "<b>Academic Plan:</b> " + academicPlan + "&nbsp;&nbsp;";
                                    }
						
									trProgOutline = new TableRow();

									tdProgOutline = new TableCell();
									tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:6px;";
									tdProgOutline.CssClass = "portletLight";
									tdProgOutline.Controls.Add(new LiteralControl(strDisplay));

									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);
						
								}

								if(gainfulEmploymentID != null && gainfulEmploymentID.Trim() != ""){
									//display the Gainful employment courseid
									trProgOutline = new TableRow();

									tdProgOutline = new TableCell();
									tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:6px;";
									tdProgOutline.CssClass = "portletLight";
									tdProgOutline.Controls.Add(new LiteralControl("<b>Gainful Employment CourseID:</b> " + gainfulEmploymentID));

									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);
								}

                                if (totalQuarters != null && totalQuarters != "") {
                                    trProgOutline = new TableRow();

                                    tdProgOutline = new TableCell();
                                    tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:6px;";
                                    tdProgOutline.CssClass = "portletLight";
                                    tdProgOutline.Controls.Add(new LiteralControl("<b>Total Quarters:</b> " + totalQuarters));

                                    trProgOutline.Cells.Add(tdProgOutline);
                                    tblProgOutline.Rows.Add(trProgOutline);
                                }

								//DISPLAY Locations Here
								DataSet dsOptionLocations = csProgram.GetOptionLocations(optionID);

								trProgOutline = new TableRow();
					
								tdProgOutline = new TableCell();
								tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:6px;";
								tdProgOutline.CssClass = "portletLight";
								tdProgOutline.Controls.Add(new LiteralControl("<b>Offered at:</b>&nbsp;"));

								String strLocations = "";
								for(Int32 j = 0; j < dsOptionLocations.Tables[0].Rows.Count; j++){
									strLocations += ",&nbsp;" + dsOptionLocations.Tables[0].Rows[j]["LocationTitle"].ToString();
								}
					
								if(strLocations != ""){
									tdProgOutline.Controls.Add(new LiteralControl(strLocations.Substring(1)));
								}
								trProgOutline.Cells.Add(tdProgOutline);
								tblProgOutline.Rows.Add(trProgOutline);

								//DISPLAY M CODES HERE
								DataSet dsOptionMCodes = csProgram.GetOptionMCodes(optionID);
								Int32 intMCodeCount = dsOptionMCodes.Tables[0].Rows.Count;
								if(intMCodeCount > 0){
									trProgOutline = new TableRow();

									tdProgOutline = new TableCell();
									tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:3px;padding-left:6px;";
									tdProgOutline.CssClass = "portletLight";
									tdProgOutline.Controls.Add(new LiteralControl("<b>Classes that apply to this program option at " + lblOfferedAt.Text + " can be taken at the following campuses:</b>"));
						
									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);

									for(Int32 j = 0; j < intMCodeCount; j++){
										trProgOutline = new TableRow();

										tdProgOutline = new TableCell();
										tdProgOutline.Attributes["style"] = "width:100%;padding-left:30px;";
										if(j == (intMCodeCount - 1)){
											tdProgOutline.Attributes["style"] += "padding-bottom:10px;";
										}else{
											tdProgOutline.Attributes["style"] += "padding-bottom:3px;";
										}
										tdProgOutline.CssClass = "portletLight";
										tdProgOutline.Controls.Add(new LiteralControl("<b>Campus:</b>&nbsp;" + dsOptionMCodes.Tables[0].Rows[j]["CollegeShortTitle"].ToString() + "&nbsp;&nbsp;<b>M-Code:</b>&nbsp;" + dsOptionMCodes.Tables[0].Rows[j]["MCode"].ToString()));

										trProgOutline.Cells.Add(tdProgOutline);
										tblProgOutline.Rows.Add(trProgOutline);
									}
								}

								if(optionDescription != ""){
									
									//display the program option description
									trProgOutline = new TableRow();

									tdProgOutline = new TableCell();
									tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:6px;";
									tdProgOutline.CssClass = "portletLight";
									tdProgOutline.Controls.Add(new LiteralControl(optionDescription));
									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);
								}

								#region GET PREREQUISITES FROM THE DB FOR DISPLAY

								DataSet dsPrereqs = csProgram.GetOptPrereqs(optionID, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
								Int32 intPrereqCount = dsPrereqs.Tables[0].Rows.Count;
				
								Table tblOptPrereqs2 = new Table();
								tblOptPrereqs2.Attributes["style"] = "width:100%;padding-bottom:10px;";
								TableRow tr = new TableRow();
								TableCell td = new TableCell();

								if(intPrereqCount > 0){
									trProgOutline = new TableRow();

									tdProgOutline = new TableCell();
									tdProgOutline.Attributes["style"] = "width:100%;padding-left:6px;font-weight:bold;";
									tdProgOutline.CssClass = "portletLight";
									tdProgOutline.Controls.Add(new LiteralControl("Prerequisites:"));

									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);

									trProgOutline = new TableRow();

									tdProgOutline = new TableCell();
									tdProgOutline.Attributes["style"] = "width:100%;padding-left:6px;padding-bottom:10px;";
									tdProgOutline.CssClass = "portletLight";

									String oldCourseOffering = "";

									//get the option prerequisites for display
									for(Int32 j = 0; j < intPrereqCount; j++){
										String courseOffering = dsPrereqs.Tables[0].Rows[j]["CourseOffering"].ToString();
										String courseSubject = dsPrereqs.Tables[0].Rows[j]["SUBJECT"].ToString();
										String strOptionPrerequisiteID = dsPrereqs.Tables[0].Rows[j]["OptionPrerequisiteID"].ToString();
										String strOptionFootnoteID = dsPrereqs.Tables[0].Rows[j]["OptionFootnoteID"].ToString();
										String courseEndSTRM = dsPrereqs.Tables[0].Rows[j]["CourseEndSTRM"].ToString();
										String catalogNbr = dsPrereqs.Tables[0].Rows[j]["CATALOG_NBR"].ToString();

										if(oldCourseOffering != courseOffering){
											if(strSearchId == courseOffering){
												courseSubject = "<span style=\"background-color:yellow\">" + courseSubject + "</span>";
												catalogNbr = "<span style=\"background-color:yellow\">" + catalogNbr + "</span>";
											}

											tr = new TableRow();

											td = new TableCell();
											td.Attributes["style"] = "width:55px;vertical-align:top;";
											td.CssClass = "portletLight";
											td.Controls.Add(new LiteralControl("�&nbsp;&nbsp;" + courseSubject));

											tr.Cells.Add(td);

											td = new TableCell();
											td.Attributes["style"] = "width:25px;vertical-align:top;";
											td.CssClass = "portletLight";
											td.Controls.Add(new LiteralControl(catalogNbr));

											tr.Cells.Add(td);

											td = new TableCell();
											td.CssClass = "portletLight";
											td.Attributes["style"] = "width:425px;vertical-align:top;";
											td.Controls.Add(new LiteralControl(dsPrereqs.Tables[0].Rows[j]["COURSE_TITLE_LONG"].ToString()));
											td.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + dsPrereqs.Tables[0].Rows[j]["FootnoteNumber"].ToString() + "</i></span>"));

											tr.Cells.Add(td);

											tblOptPrereqs2.Rows.Add(tr);
										}
										oldCourseOffering = courseOffering;
									}

									tdProgOutline.Controls.Add(tblOptPrereqs2);
									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);
								}

								#endregion

								#region GET OPTION COURSES AND ELECTIVES FROM THE DB FOR DISPLAY

								//get option courses and electives
                                DataRow[] drCourses = csProgram.GetOptionCoursesAndElectives(optionID, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);

								Double dblTotalMinCred = 0, dblTotalMaxCred = 0, dblDegMinCred = 0, dblDegMaxCred = 0;
								Int16 intQuarter = 0;

								if(drCourses.Length > 0){
									trProgOutline = new TableRow();

									tdProgOutline = new TableCell();
									tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:4px;";
									tdProgOutline.CssClass = "portletLight";

									Table tblOptCourses2 = new Table();
									tblOptCourses2.Attributes["style"] = "width:100%;";

									String oldCourseOffering = "";

									//loop through option courses
									for(Int32 intCrsCtr = 0; intCrsCtr < drCourses.Length; intCrsCtr++){
										String courseOffering = drCourses[intCrsCtr]["CourseOffering"].ToString();

										if(oldCourseOffering != courseOffering || courseOffering == "ZZZ"){
											String strQuarter = drCourses[intCrsCtr]["Quarter"].ToString();
											
											//determine quarter begins
											if(strQuarter != ""){

												if(intQuarter.ToString() != strQuarter){
													intQuarter = Convert.ToInt16(strQuarter);
													tr = new TableRow();

													td = new TableCell();
													td.CssClass = "portletLight";
													td.ColumnSpan = 4;
													td.Attributes["style"] = "width:313px;";

													if(intQuarter == 1){
														td.Controls.Add(new LiteralControl("<b>First Quarter</b>"));
													}else if(intQuarter == 2){
														td.Controls.Add(new LiteralControl("<b>Second Quarter</b>"));
													}else if(intQuarter == 3){
														td.Controls.Add(new LiteralControl("<b>Third Quarter</b>"));
													}else if(intQuarter == 4){
														td.Controls.Add(new LiteralControl("<b>Fourth Quarter</b>"));
													}else if(intQuarter == 5){
														td.Controls.Add(new LiteralControl("<b>Fifth Quarter</b>"));
													}else if(intQuarter == 6){
														td.Controls.Add(new LiteralControl("<b>Sixth Quarter</b>"));
													}else if(intQuarter == 7){
														td.Controls.Add(new LiteralControl("<b>Seventh Quarter</b>"));
													}else if(intQuarter == 8){
														td.Controls.Add(new LiteralControl("<b>Eighth Quarter</b>"));
													}else if(intQuarter == 9){
														td.Controls.Add(new LiteralControl("<b>Ninth Quarter</b>"));
													}else if(intQuarter == 10){
														td.Controls.Add(new LiteralControl("<b>Tenth Quarter</a>"));
													}

													dblTotalMinCred = 0;
													dblTotalMaxCred = 0;

													tr.Cells.Add(td);
													tblOptCourses2.Rows.Add(tr);
												}

											}
											//determine quarter ends

											//determine credit display

											//added on 10/12/06 to allow variable credit overwrite
											Double unitsMinimum = 0, unitsMaximum = 0;
											bool blnOverwriteUnits = false;

											if(Convert.ToByte(drCourses[intCrsCtr]["OverwriteUnits"]) == 1){
												unitsMinimum = Convert.ToDouble(drCourses[intCrsCtr]["OverwriteUnitsMinimum"]);
												unitsMaximum = Convert.ToDouble(drCourses[intCrsCtr]["OverwriteUnitsMaximum"]);
												blnOverwriteUnits = true;
											}else{
												unitsMinimum = Convert.ToDouble(drCourses[intCrsCtr]["UNITS_MINIMUM"]);
												unitsMaximum = Convert.ToDouble(drCourses[intCrsCtr]["UNITS_MAXIMUM"]);
											}

											String strCredits = "";

											if(unitsMinimum == unitsMaximum){
												strCredits = unitsMinimum.ToString();
												dblTotalMinCred += unitsMinimum;
												dblTotalMaxCred += unitsMinimum;
											}else{
												strCredits = unitsMinimum + "-" + unitsMaximum;
												dblTotalMinCred += unitsMinimum;
												dblTotalMaxCred += unitsMaximum;
											}

											//display courses begins
											String courseLongTitle = drCourses[intCrsCtr]["COURSE_TITLE_LONG"].ToString();
											String strOptionFootnoteID = drCourses[intCrsCtr]["OptionFootnoteID"].ToString();
											String courseSubject = drCourses[intCrsCtr]["SUBJECT"].ToString();
											String courseEndSTRM = drCourses[intCrsCtr]["CourseEndSTRM"].ToString();
											String catalogNbr = drCourses[intCrsCtr]["CATALOG_NBR"].ToString();

											if(strSearchId == courseOffering){
												courseSubject = "<span style=\"background-color:yellow\">" + courseSubject + "</span>";
												catalogNbr = "<span style=\"background-color:yellow\">" + catalogNbr + "</span>";
											}

											tr = new TableRow();

											td = new TableCell();
											td.Attributes["style"] = "width:38px;vertical-align:top;";
											td.CssClass = "portletLight";

											td.Controls.Add(new LiteralControl(courseSubject));

											tr.Cells.Add(td);

											td = new TableCell();
											td.Attributes["style"] = "width:25px;vertical-align:top;";
											td.CssClass = "portletLight";
											td.Controls.Add(new LiteralControl(catalogNbr));

											tr.Cells.Add(td);

											td = new TableCell();
											td.CssClass = "portletLight";
											td.Attributes["style"] = "width:390px;vertical-align:top;";
											td.Controls.Add(new LiteralControl(courseLongTitle));
											td.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + drCourses[intCrsCtr]["FootnoteNumber"].ToString() + "</i></span>"));

											tr.Cells.Add(td);

											td = new TableCell();
											td.CssClass = "smallText";
											td.Attributes["style"] = "vertical-align:top;width:50px;text-align:right;";
											if(blnOverwriteUnits){
												td.Attributes["style"] += "color:red;";
											}
											td.Controls.Add(new LiteralControl(strCredits));

											tr.Cells.Add(td);
											tblOptCourses2.Rows.Add(tr);
											
										}

                                        //check if the quarter or group of courses/electives is on the last course/elective
                                        if ((intCrsCtr == (drCourses.Length - 1)) || (intCrsCtr < (drCourses.Length - 1) && drCourses[intCrsCtr + 1]["Quarter"].ToString() != drCourses[intCrsCtr]["Quarter"].ToString())) {
                                            //display the total min and max credits for the quarter
                                            String strTotalCred = "";

                                            if (dblTotalMinCred < dblTotalMaxCred) {
                                                dblDegMinCred += dblTotalMinCred;
                                                dblDegMaxCred += dblTotalMaxCred;
                                                strTotalCred = dblTotalMinCred + "-" + dblTotalMaxCred;
                                            } else {
                                                dblDegMinCred += dblTotalMinCred;
                                                dblDegMaxCred += dblTotalMinCred;
                                                strTotalCred = dblTotalMaxCred.ToString();
                                            }

                                            tr = new TableRow();
                                            td = new TableCell();
                                            td.ColumnSpan = 3;
                                            tr.Cells.Add(td);
                                            td = new TableCell();
                                            td.Attributes["style"] = "width:50px;text-align:right;padding-top:2px;border-top: 1px solid #000000;";
                                            td.Controls.Add(new LiteralControl(strTotalCred));
                                            tr.Cells.Add(td);
                                            td = new TableCell();
                                            tr.Cells.Add(td);
                                            tblOptCourses2.Rows.Add(tr);
                                        }

                                        if (intCrsCtr == (drCourses.Length - 1)) {
                                            //calculate and display total credits for the selected degree
                                            String strDegCred = "";

                                            if (dblDegMinCred < dblDegMaxCred) {
                                                strDegCred = dblDegMinCred + "-" + dblDegMaxCred;
                                            } else {
                                                strDegCred = dblDegMaxCred.ToString();
                                            }

                                            strDegCred += " credits are required for the " + degreeShortTitle;

                                            tr = new TableRow();
                                            td = new TableCell();
                                            td.ColumnSpan = 5;
                                            td.Controls.Add(new LiteralControl(strDegCred));
                                            td.Attributes["style"] = "width:100%;";
                                            tr.Cells.Add(td);
                                            tblOptCourses2.Rows.Add(tr);
                                        }	

										oldCourseOffering = courseOffering;
									}
									tdProgOutline.Controls.Add(tblOptCourses2);
									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);
								}

								#endregion

								#region GET PROGRAM ELECTIVE GROUPS FROM THE DB FOR DISPLAY

								//get program electives
								DataSet dsElectGroup = csProgram.GetOptionElectiveGroups(optionID);
								Int32 intElectGroupCount = dsElectGroup.Tables[0].Rows.Count;

								if(intElectGroupCount > 0){

									trProgOutline = new TableRow();

									//tdProgOutline = new TableCell();
									//tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:4px;padding-top:4px;padding-left:4px;";
									//tdProgOutline.CssClass = "portletMain";
									//tdProgOutline.Controls.Add(new LiteralControl("<b>Elective Groups</b>"));

									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);
				
									trProgOutline = new TableRow();

									tdProgOutline = new TableCell();
									tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:4px;padding-right:6px;";
									tdProgOutline.CssClass = "portletLight";

									Table tblOptionElectives2 = new Table();
									tblOptionElectives2.Attributes["style"] = "width:100%;";

									for(Int16 intElectGroupCtr = 0; intElectGroupCtr < dsElectGroup.Tables[0].Rows.Count; intElectGroupCtr++){
										Int32 optionElectiveGroupID = Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroupCtr]["OptionElectiveGroupID"]);
										Int32 intElectiveCount = Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroupCtr]["ElectiveCount"]);
										String strElectGroupTitle = dsElectGroup.Tables[0].Rows[intElectGroupCtr]["ElectiveGroupTitle"].ToString();

										if(intElectiveCount > 0){
											tr = new TableRow();

											td = new TableCell();
											td.ColumnSpan = 4; //check into this
											td.CssClass = "smallText";
											td.Attributes["style"] = "padding-top:5px;width:313px;";
											td.Controls.Add(new LiteralControl("<b><u>" + strElectGroupTitle + "</u></b>&nbsp;<i><font style=\"font-size:7pt;\">" + dsElectGroup.Tables[0].Rows[intElectGroupCtr]["FootnoteNumber"].ToString() + "</font></i>"));

											tr.Cells.Add(td);
											tblOptionElectives2.Rows.Add(tr);

                                            DataSet dsElectiveGroupCourses = csProgram.GetElectiveGroupCourses(optionElectiveGroupID, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);

											for(Int32 intElectCtr = 0; intElectCtr < dsElectiveGroupCourses.Tables[0].Rows.Count; intElectCtr++){
												//display electives begin
												Double dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["UNITS_MINIMUM"]);
												Double dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["UNITS_MAXIMUM"]);
												String courseOffering = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CourseOffering"].ToString();
												String courseSubject = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["SUBJECT"].ToString();
												String strOptionFootnoteID = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OptionFootnoteID"].ToString();
												String catalogNbr = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CATALOG_NBR"].ToString();
												//Int32 courseID = Convert.ToInt32(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CourseID"]);
												bool blnOverwriteUnits = false;
												String courseEndSTRM = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CourseEndSTRM"].ToString();

												if(strSearchId == courseOffering){
													courseSubject = "<span style=\"background-color:yellow\">" + courseSubject + "</span>";
													catalogNbr = "<span style=\"background-color:yellow\">" + catalogNbr + "</span>";
												}

												if(Convert.ToByte(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnits"]) == 1){
													dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnitsMinimum"]);
													dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnitsMaximum"]);
													blnOverwriteUnits = true;
												}

												String strElectCredits = "";

												if(dblElectCredMin == dblElectCredMax){
													strElectCredits = dblElectCredMin.ToString();
												}else{
													strElectCredits = dblElectCredMin + "-" + dblElectCredMax;
												}

												tr = new TableRow();

												td = new TableCell();
												td.Attributes["style"] = "width:38px;vertical-align:top;";
												td.CssClass = "portletLight";

												td.Controls.Add(new LiteralControl(courseSubject));

												tr.Cells.Add(td);

												td = new TableCell();
												td.Attributes["style"] = "width:25px;vertical-align:top;";
												td.CssClass = "portletLight";
												td.Controls.Add(new LiteralControl(catalogNbr));

												tr.Cells.Add(td);

												td = new TableCell();
												td.CssClass = "portletLight";
												td.Attributes["style"] = "width:390px;vertical-align:top;";
												td.Controls.Add(new LiteralControl(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["COURSE_TITLE_LONG"].ToString()));
												td.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["FootnoteNumber"].ToString() + "</i></span>"));

												tr.Cells.Add(td);

												td = new TableCell();
												td.CssClass = "portletLight";
												td.Attributes["style"] = "vertical-align:top;width:50px;text-align:right;";
												if(blnOverwriteUnits){
													td.Attributes["style"] += "color:red;";
												}
												td.Controls.Add(new LiteralControl(strElectCredits));

												tr.Cells.Add(td);

												tblOptionElectives2.Rows.Add(tr);
											}
										}
									}
									tdProgOutline.Controls.Add(tblOptionElectives2);
									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);
								}
								#endregion

								#region GET PROGRAM FOOTNOTES FROM THE DB FOR DISPLAY

								//get the program footnotes
								DataSet dsFootnotes = csProgram.GetOptFootnotes(optionID);
								Int32 intFootnoteCount = dsFootnotes.Tables[0].Rows.Count;

								if(intFootnoteCount > 0){

									trProgOutline = new TableRow();

									//tdProgOutline = new TableCell();
									//tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:4px;padding-top:4px;padding-left:6px;";
									//tdProgOutline.CssClass = "portletMain";
									//tdProgOutline.Controls.Add(new LiteralControl("<b>Footnotes</b>"));

									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);

									trProgOutline = new TableRow();

									tdProgOutline = new TableCell();
									tdProgOutline.Attributes["style"] = "width:100%;padding-bottom:10px;padding-left:4px;";
									tdProgOutline.CssClass = "portletLight";

									Table tblProgFootnotes2 = new Table();
									tblProgFootnotes2.Attributes["style"] = "width:100%;";

									for(Int32 intFootnoteCtr = 0; intFootnoteCtr < intFootnoteCount; intFootnoteCtr++){
										Int16 intFootnoteNum = Convert.ToInt16(dsFootnotes.Tables[0].Rows[intFootnoteCtr]["FootnoteNumber"]);
										Int32 optionFootnoteID = Convert.ToInt32(dsFootnotes.Tables[0].Rows[intFootnoteCtr]["OptionFootnoteID"]);
										String strFootnote = dsFootnotes.Tables[0].Rows[intFootnoteCtr]["Footnote"].ToString().Replace("&amp;","&");
										if(strSearchText != null && strSearchText != "" && strFootnote.ToLower().IndexOf(strSearchText.ToLower()) > -1){
											strFootnote = Regex.Replace(strFootnote, strSearchText, "<span style=\"background-color: yellow\">" + strFootnote.Substring(strFootnote.ToLower().IndexOf(strSearchText.ToLower()), strSearchText.Length) + "</span>", RegexOptions.IgnoreCase);
										}

										tr = new TableRow();

										td = new TableCell();
										td.CssClass = "portletLight";
										td.Attributes["style"] = "width:18px;font-size:7pt;vertical-align:top;";
										if(intFootnoteCtr == 0){
											td.Attributes["style"] += "padding-top:5px;";
										}
										td.Controls.Add(new LiteralControl("<i>" + intFootnoteNum + "</i>"));

										tr.Cells.Add(td);

										td = new TableCell();
										td.CssClass = "portletLight";
										if(intFootnoteCtr == 0){
											td.Attributes["style"] = "padding-top:5px;";
										}
										td.Controls.Add(new LiteralControl("<i>" + strFootnote + "</i>"));
										tr.Cells.Add(td);

										tblProgFootnotes2.Rows.Add(tr);
									}

									tdProgOutline.Controls.Add(tblProgFootnotes2);
									trProgOutline.Cells.Add(tdProgOutline);
									tblProgOutline.Rows.Add(trProgOutline);
								}

								#endregion

							}	
						}

						panOutline.Visible = true;
					}

					panView.Visible = true;
					if(csProgram.HasPublishedCPGCopy(programVersionID)){
						cmdCPG.Visible = true;
					}
					panButtons.Visible = true;

				}else{
					//show error message stating that the program does not exist - won't happen though
				}
				
			}else{

				if(hidTodo.Value != "search"){
					hidLtr.Value = Request.QueryString["ltr"];
				}

				GenerateProgramList();
				panDisplay.Visible = true;

			}
		}

		protected void cmdBack_Click(object sender, System.EventArgs e){
			String strSearchText = Request.QueryString["searchtxt"];
			String strSearchId = Request.QueryString["searchid"];
			String strSearchDept = Request.QueryString["searchsub"];
			String searchSTRM = Request.QueryString["searchstrm"];

			if(strSearchText != null && strSearchText != ""){
				Response.Redirect("coursesearch.aspx?searchtxt=" + strSearchText.Replace("&","%26"));
			}else if(strSearchId != null && strSearchId != ""){
				Response.Redirect("coursesearch.aspx?searchid=" + strSearchId.Replace("&","%26") + "&searchsub=" + strSearchDept.Replace("&","%26") + "&searchstrm=" + searchSTRM);
			}else{
				Response.Redirect("view.aspx?ltr=" + hidLtr.Value + "&strm=" + cboTerm.SelectedValue + "&colid=" + cboOfferedAt.SelectedValue);
			}
		}

		protected void cmdCPG_Click(object sender, System.EventArgs e){
            //Response.Redirect("http://icatalog.ccs.spokane.edu/program/cpg.aspx?pvid=" + Request.Form["hidProgramVersionID"] + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&SYRQ=" + cboTerm.SelectedValue + "&pt=" + lblProgramTitle.Text);
			Response.Redirect("cpg/view.aspx?pvid=" + Request.Form["hidProgramVersionID"] + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&SYRQ=" + cboTerm.SelectedValue + "&pt=" + lblProgramTitle.Text);
		}

		public void GenerateProgramList(){ //generate and show the program list alphabetically
			TableRow tr = new TableRow();
			
			TableCell td = new TableCell();
            /*
			td.Attributes["style"] = "padding:1px;width:1px;";
			tr.Cells.Add(td);
            */

			//create the alphabet selection list
			for(Int16 intAlphaCtr = 65; intAlphaCtr <= 90; intAlphaCtr++){
				td = new TableCell();
				td.Attributes["style"] = "width:23px;text-align:center;";
				td.Attributes["onclick"] = "document.frmProgram.hidTodo.value='search';document.frmProgram.hidLtr.value='" + Convert.ToChar(intAlphaCtr) + "';document.frmProgram.submit();return false;";
				td.CssClass = "portletMain";
				td.Controls.Add(new LiteralControl("<a class=\"ltr\" href=\"#\" onclick=\"document.frmProgram.hidTodo.value='search';document.frmProgram.hidLtr.value='" + Convert.ToChar(intAlphaCtr) + "';document.frmProgram.submit();return false;\">" + Convert.ToChar(intAlphaCtr) + "</a>"));
				tr.Cells.Add(td);
			}

			tblAlpha.Rows.Add(tr);

			//select letter 'A' as the default
			String strLtr = hidLtr.Value;
			if(strLtr == ""){
				strLtr = "A";
			}

			//get a dataset of programs starting with the selected letter
			DataSet dsPrograms = csProgram.GetProgramsForDisplay(Convert.ToChar(strLtr), cboTerm.SelectedValue, cboOfferedAt.SelectedValue);
			Int32 intRowCtr = dsPrograms.Tables[0].Rows.Count;
			
			//display the program header information
			tr = new TableRow();

            td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:155px;";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("Award Type"));

            tr.Cells.Add(td);

			td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:278px;";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("Program Title"));

			tr.Cells.Add(td);

			td = new TableCell();
			td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;white-space:nowrap;width:72px;";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("Offered by"));

			tr.Cells.Add(td);

			td = new TableCell();
			td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:85px;white-space:nowrap";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("Begin Term"));

			tr.Cells.Add(td);

			td = new TableCell();
			td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:85px;white-space:nowrap";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("End Term"));

			tr.Cells.Add(td);

			td = new TableCell();
			td.Attributes["style"] = "width:42px;white-space:nowrap";
			td.CssClass = "portletSecondary";

			tr.Cells.Add(td);
			tblDisplay.Rows.Add(tr);

            String oldAreaOfStudyTitle = "";

			if(intRowCtr > 0){ //if area of study titles starting with the selected letter exist

				//loop through and display the programs
				for(Int32 intDSRow = 0; intDSRow < intRowCtr; intDSRow++){
					Int32 programVersionID = Convert.ToInt32(dsPrograms.Tables[0].Rows[intDSRow]["ProgramVersionID"]);
                    String degreeShortTitle = dsPrograms.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
					String programTitle = dsPrograms.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString();
                    String areaOfStudyTitle = dsPrograms.Tables[0].Rows[intDSRow]["Title"].ToString();

                    if (areaOfStudyTitle != oldAreaOfStudyTitle) {
                        tr = new TableRow();
                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "font-weight:bold;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;width:100%;";
                        td.ColumnSpan = 6;
                        td.Controls.Add(new LiteralControl(areaOfStudyTitle));
                        tr.Cells.Add(td);
                        tblDisplay.Rows.Add(tr);
                    }

					tr = new TableRow();

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:155px;";
                    td.Controls.Add(new LiteralControl(degreeShortTitle));

                    tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:278px;";
					td.Controls.Add(new LiteralControl(programTitle));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:72px;";
					td.Controls.Add(new LiteralControl(dsPrograms.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString()));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:85px;";
					td.Controls.Add(new LiteralControl(dsPrograms.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString()));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:85px;";
					td.Controls.Add(new LiteralControl(dsPrograms.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString()));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:center;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:42px;";
					td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.frmProgram.hidTodo.value='view';document.frmProgram.hidProgramDisplay.value='" + dsPrograms.Tables[0].Rows[intDSRow]["ProgramDisplay"].ToString() + "';document.frmProgram.hidProgramVersionID.value='" + programVersionID + "';document.frmProgram.submit();return false;\">View</a>"));
					tr.Cells.Add(td);
					tblDisplay.Rows.Add(tr);

                    oldAreaOfStudyTitle = areaOfStudyTitle;
				}

			}else{ //else if program titles starting with the selected letter do not exist

				//display "selected letter returned 0 results"
				tr = new TableRow();
				td = new TableCell();
				td.CssClass = "portletLight";
				td.ColumnSpan = 6;
				td.Attributes["style"] = "padding-top:4px;padding-bottom:4px;";
				td.Controls.Add(new LiteralControl("&nbsp;" + strLtr + " returned 0 results."));
				tr.Cells.Add(td);
				tblDisplay.Rows.Add(tr);

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
