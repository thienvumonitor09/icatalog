using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.college
{
	/// <summary>
	/// Summary description for add.
	/// </summary>
	public partial class add : System.Web.UI.Page {

		protected void Page_Load(object sender, System.EventArgs e) {
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panError.Visible = false;
			panConfirm.Visible = false;
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			programData csProgram = new programData();
			String institution = Request.Form["txtInstitution"], collegeShortTitle = Request.Form["txtCollegeShortTitle"], collegeLongTitle = Request.Form["txtCollegeLongTitle"], collegeAddress = Request.Form["txtCollegeAddress"], collegeCity = Request.Form["txtCollegeCity"], collegeState = Request.Form["txtCollegeState"], collegeZipCode1 = Request.Form["txtCollegeZipCode1"], collegeZipCode2 = Request.Form["txtCollegeZipCode2"], collegeWebsiteURL = Request.Form["txtCollegeWebsiteURL"], collegeRegistrationEmail = Request.Form["txtCollegeRegistrationEmail"];
			Int16 intSuccess = csProgram.AddCollege(institution, collegeShortTitle, collegeLongTitle, collegeAddress, collegeCity, collegeState, collegeZipCode1, collegeZipCode2, collegeWebsiteURL, collegeRegistrationEmail, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]); 
		
			if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The college entered already exists.";
				panError.Visible = true;
				panConfirm.Visible = false;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The insert failed.";
				panError.Visible = true;
				panConfirm.Visible = false;
			}else if(intSuccess == 1){
				lblInstitution.Text = institution;
				lblCollegeShortTitle.Text = collegeShortTitle;
				lblCollegeLongTitle.Text = collegeLongTitle;
				lblCollegeAddress.Text = collegeAddress;
				lblCollegeCity.Text = collegeCity;
				lblCollegeState.Text = collegeState;
				lblCollegeZipCode.Text = collegeZipCode1;
				if(collegeZipCode2 != ""){
					lblCollegeZipCode.Text += "-" + collegeZipCode2;
				}
				if(collegeWebsiteURL == ""){
					lblCollegeWebsiteURL.Text = "&nbsp;";
				}else{
					lblCollegeWebsiteURL.Text = collegeWebsiteURL;
				}
				if(collegeRegistrationEmail == ""){
					lblCollegeRegistrationEmail.Text = "&nbsp;";
				}else{
					lblCollegeRegistrationEmail.Text = collegeRegistrationEmail;
				}
				panError.Visible = false;
				panConfirm.Visible = true;
				panAdd.Visible = false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {    
		}
		#endregion
	}
}
