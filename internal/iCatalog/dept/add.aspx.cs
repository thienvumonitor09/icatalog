using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.dept
{
	/// <summary>
	/// Summary description for add.
	/// </summary>
	public partial class add : System.Web.UI.Page
	{

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			if(!IsPostBack){
				panConfirm.Visible = false;
				panAdd.Visible = true;
				panError.Visible = false;
				hidLtr.Value = Request.QueryString["ltr"];
			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			courseData csCourse = new courseData();
			Int16 intSuccess = csCourse.AddSubject(txtSubject.Text, txtSubjectDescription.Text, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]);
		
			if(intSuccess == 1){
				lblSubject.Text = txtSubject.Text;
				lblSubjectDescription.Text = txtSubjectDescription.Text;
				panConfirm.Visible = true;
				panAdd.Visible = false;
			}else if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The subject area entered already exists.";
				panError.Visible = true;
				panConfirm.Visible = false;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The insert failed.";
				panError.Visible = true;
				panConfirm.Visible = false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
