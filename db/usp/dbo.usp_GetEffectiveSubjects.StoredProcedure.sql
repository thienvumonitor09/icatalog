USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEffectiveSubjects]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/13/2019
-- Description:	Return a list of subjects by term code
-- Parameters:	
--   @STRM - Term code
--   @INSTITUITION - Instituition name
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetEffectiveSubjects]
	@STRM varchar(4),
	@INSTITUTION varchar(5)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT SUBJECT, COUNT(CATALOG_NBR) AS CrsCount
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	WHERE CourseOffering.EFFDT = 
		(
			SELECT MAX(CourseOffering2.EFFDT) 
			FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
			WHERE CourseOffering2.SUBJECT = CourseOffering.SUBJECT
				AND CourseOffering2.CATALOG_NBR = CourseOffering.CATALOG_NBR
				AND INSTITUTION = @INSTITUTION
				AND
				(
					CourseOffering2.EFFDT <= (SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE STRM = @STRM ORDER BY TERM_END_DT DESC)
					OR
					@STRM = 'Z999'
				)
		) 
		AND ACAD_CAREER = 'UGRD'
		AND CATALOG_PRINT = 'Y'
		AND EFF_STATUS = 'A'
		AND INSTITUTION = @INSTITUTION
	GROUP BY SUBJECT
	ORDER BY SUBJECT ASC
END
GO
