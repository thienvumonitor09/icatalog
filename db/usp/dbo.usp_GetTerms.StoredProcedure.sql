USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTerms]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughhn
-- Create date: 3/2/2017
-- Description:	Gets all terms after a specified term if a STRM (Term Code) is provided otherwise all terms are returned.
--		 INPUT: STRM (optional) - Term Code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetTerms] 
	@STRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT STRM, DESCR
	FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
	WHERE (STRM IS NULL OR STRM >= @STRM)
	ORDER BY STRM DESC;
END

GO
