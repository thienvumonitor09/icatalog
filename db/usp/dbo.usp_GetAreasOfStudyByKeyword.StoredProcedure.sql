USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAreasOfStudyByKeyword]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 3/1/18
-- Description:	Searches iCatalog for a keyword and returns an alphabetic area of study list
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetAreasOfStudyByKeyword]
	@STRM varchar(4) = NULL,
	@Institution varchar(5) = NULL,
	@AlphaCharacter char = NULL,
	@Keyword varchar(150) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SQL nvarchar(max),
			@Parameters nvarchar(4000)
			
	IF @STRM IS NULL
		SET @STRM = (SELECT MIN(STRM)
					FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					WHERE DATEADD(day,10,TERM_BEGIN_DT) > GETDATE()
					AND TERM_END_DT >= GETDATE())
							
		SET @SQL = 'SELECT AreaOfStudy.AreaOfStudyID, AreaOfStudy.Title, AreaOfStudy.WebsiteURL, AreaOfStudy.Institution, CollegeShortTitle
					FROM AreaOfStudy
					LEFT OUTER JOIN AreaOfStudyProgram
					ON AreaOfStudyProgram.AreaOfStudyID = AreaOfStudy.AreaOfStudyID
					LEFT OUTER JOIN Program
					ON Program.ProgramID = AreaOfStudyProgram.ProgramID
					LEFT OUTER JOIN ProgramDegree
					ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID				
					LEFT OUTER JOIN CareerPlanningGuide 
					ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
					LEFT OUTER JOIN College 
					ON College.Institution = AreaOfStudy.Institution
					LEFT OUTER JOIN ProgramDegreeOption 
					ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
					LEFT OUTER JOIN Degree
					ON Degree.DegreeID = ProgramDegree.DegreeID 
					LEFT OUTER JOIN OptionPrerequisite 
					ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
					LEFT OUTER JOIN OptionElectiveGroup 
					ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
					LEFT OUTER JOIN ElectiveGroupCourse 
					ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
					LEFT OUTER JOIN OptionCourse 
					ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
					LEFT OUTER JOIN Course
					ON Course.CourseOffering = OptionPrerequisite.CourseOffering
					OR Course.CourseOffering = ElectiveGroupCourse.CourseOffering 
					OR Course.CourseOffering = OptionCourse.CourseOffering 
					LEFT OUTER JOIN OptionLocation 
					ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
					LEFT OUTER JOIN Location 
					ON Location.LocationID = OptionLocation.LocationID 
					WHERE (AreaOfStudyProgram.ProgramID IS NULL OR ((ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = ''Z999'' OR ProgramEndSTRM >= @STRM)) 
					AND PublishedProgram = 1)) '
				
	IF @Institution IS NOT NULL
		SET @SQL += 'AND AreaOfStudy.Institution = @Institution '
		
	IF @AlphaCharacter IS NOT NULL
		SET @SQL += 'AND Title LIKE @AlphaCharacter + ''%'' '
					 
	IF @Keyword IS NOT NULL
		SET @SQL += 'AND ((LocationTitle LIKE ''%'' + @Keyword + ''%'')
					 OR (AreaOfStudy.Title LIKE ''%'' + @Keyword + ''%'')
					 OR (ProgramTitle LIKE ''%'' + @Keyword + ''%'')
					 OR (ProgramDescription LIKE ''%'' + @Keyword + ''%'')
					 OR (OptionTitle LIKE ''%'' + @Keyword + ''%'')
					 OR (DegreeShortTitle LIKE ''%'' + @Keyword + ''%'')
					 OR (DegreeLongTitle LIKE ''%'' + @Keyword + ''%'')
					 OR (DegreeType LIKE ''%'' + @Keyword + ''%'')
					 OR ((Course.COURSE_TITLE_LONG LIKE ''%'' + @Keyword + ''%'') 
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR ((Course.DESCR LIKE ''%'' + @Keyword + ''%'') 
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR ((Course.SUBJECT LIKE ''%'' + @Keyword + ''%'') 
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR ((Course.CATALOG_NBR LIKE ''%'' + @Keyword + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR (((Course.SUBJECT + LTRIM(Course.CATALOG_NBR)) LIKE + ''%'' + @Keyword + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR (((Course.SUBJECT + Course.CATALOG_NBR) LIKE + ''%'' + @Keyword + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR (((Course.SUBJECT + '' '' + Course.CATALOG_NBR) LIKE + ''%'' + @Keyword + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))) '
	
	SET @SQL += 'GROUP BY AreaOfStudy.AreaOfStudyID, AreaOfStudy.Title, AreaOfStudy.WebsiteURL, AreaOfStudy.Institution, CollegeShortTitle  
				 ORDER BY AreaOfStudy.Title, CollegeShortTitle;'
				 
	SET @Parameters = '@STRM varchar(4),
					   @Institution varchar(5),
					   @AlphaCharacter char,
					   @Keyword varchar(150)'
						 
	EXEC sp_executesql @SQL, @Parameters, @STRM, @Institution, @AlphaCharacter, @Keyword
	
END

GO
