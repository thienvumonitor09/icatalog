<%@ Reference Page="~/program/edit.aspx" %>
<%@ Page language="c#" Inherits="ICatalog.program.addprereq" CodeFile="addprereq.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Add Option Prerequisite</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
		<script type="text/javascript">
		<!--
			function getProgOptValues(){
				document.getElementById("hidOptionDescription").value = window.opener.CKEDITOR.instances['txtOptionDescription'].getData();
				document.getElementById("hidGainfulEmploymentID").value = window.opener.document.getElementById('txtGainfulEmploymentID').value;
				document.getElementById("hidOptionStateApproval").value = window.opener.document.getElementById('txtOptionStateApproval').value;
				document.getElementById("hidCIP").value = window.opener.document.getElementById('txtCIP').value;
				document.getElementById("hidEPC").value = window.opener.document.getElementById('txtEPC').value;
				document.getElementById("hidAcademicPlan").value = window.opener.document.getElementById('txtAcademicPlan').value;
				document.getElementById("hidTotalQuarters").value = window.opener.document.getElementById('txtTotalQuarters').value;
				document.getElementById("hidPrimaryOption").value = window.opener.document.getElementById('chkPrimary').checked;
				
				var strOptionLocationFields = window.opener.document.getElementById('hidLocationFields').value;
				var hidLocations = document.getElementById("hidLocations");
				hidLocations.value = "";
				if(strOptionLocationFields != null && strOptionLocationFields != ""){
					var aryOptionLocationFields = strOptionLocationFields.substring(1).split("|");
					for(var i = 0; i < aryOptionLocationFields.length; i++){

						if(window.opener.document.getElementById(aryOptionLocationFields[i]).checked){
							hidLocations.value += "|" + aryOptionLocationFields[i].replace("chkLocation_" + document.getElementById("hidOptionID").value + "_","");
						}
					}
				}
				
				var strOptionMCodeFields = window.opener.document.getElementById('hidMCodeFields').value;
				var hidMCodes = document.getElementById("hidMCodes");
				hidMCodes.value = "";
				if(strOptionMCodeFields != null && strOptionMCodeFields != ""){
					var aryOptionMCodeFields = strOptionMCodeFields.substring(1).split("|");
					for(var i = 0; i < aryOptionMCodeFields.length; i++){
						var aryCollegeMCodeFields = aryOptionMCodeFields[i].split(";");
						if(window.opener.document.getElementById(aryCollegeMCodeFields[0]).checked){
							hidMCodes.value += "|" + aryCollegeMCodeFields[0].replace("chkMCode_" + document.getElementById("hidOptionID").value + "_","") + ";" + window.opener.document.getElementById(aryCollegeMCodeFields[1]).value;
						}						
					}
				}
				return true;
			}
		//-->
		</script>
	</head>
	<body style="background: #ffffff url('');">
		<form id="frmPrereq" runat="server">
			<input type="hidden" id="hidProgramDegreeID" runat="server" />
			<input type="hidden" id="hidProgramVersionID" runat="server" />
			<input type="hidden" id="hidCollegeID" runat="server" />
			<input type="hidden" id="hidLtr" runat="server" />
			<input type="hidden" id="hidProgramTitle" runat="server" />
			<input type="hidden" id="hidOfferedAt" runat="server" />
			<input type="hidden" id="hidOptionDescription" runat="server" />
			<input type="hidden" id="hidOptionStateApproval" runat="server" />
			<input type="hidden" id="hidCIP" runat="server" />
			<input type="hidden" id="hidEPC" runat="server" />
            <input type="hidden" id="hidAcademicPlan" runat="server" />
            <input type="hidden" id="hidTotalQuarters" runat="server" />
			<input type="hidden" id="hidPrimaryOption" runat="server" />
			<input type="hidden" id="hidOptionID" runat="server" />
			<input type="hidden" id="hidDegreeID" runat="server" />
			<input type="hidden" id="hidProgramDisplay" runat="server" />
			<input type="hidden" id="hidFocus" runat="server" />
			<input type="hidden" id="hidProgramBeginSTRM" runat="server" />
			<input type="hidden" id="hidProgramEndSTRM" runat="server" />
			<input type="hidden" id="hidPublishedProgram" runat="server" />
			<input type="hidden" id="hidPublishedProgramVersionID" runat="server" />
			<input type="hidden" id="hidSTRM" runat="server" />
			<input type="hidden" id="hidSelectedCollegeID" runat="server" />
			<input type="hidden" id="hidProgramID" runat="server" />
			<input type="hidden" id="hidMCodes" runat="server" />
			<input type="hidden" id="hidLocations" runat="server" />
			<input type="hidden" id="hidGainfulEmploymentID" runat="server" />
			<input type="hidden" id="hidSearchSubject" runat="server" />
			<input type="hidden" id="hidSearchId" runat="server" />
			<input type="hidden" id="hidSearchSTRM" runat="server" />
			<input type="hidden" id="hidSearchText" runat="server" />
			<table class="centeredTable" style="width:280px;" cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding-left:4px;padding-right:0px;">
						<table style="width:100%;" cellpadding="0" cellspacing="0">
							<tr>
								<td style="background-color:#000000;text-align:left;">
									<table style="width:100%;" cellpadding="1" cellspacing="1">
										<tr>
											<td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;"
												class="portletHeader">
												<table style="width:100%;" cellpadding="0" cellspacing="0">
													<tr>
														<td class="portletHeader" style="width:100%;">&nbsp;Add Option Prerequisite</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="portletMain" style="width:100%">
												<table style="width:100%;" cellspacing="0" cellpadding="0">
													<tr>
														<td class="portletDark" style="width:100%;">
															<table cellspacing="1" cellpadding="0" style="width:100%;">
																<tr>
																	<td class="portletLight" style="width:100%;vertical-align:text-top;">
																		<table cellpadding="0" cellspacing="0" style="width:100%;">
																			<tr>
																				<td style="width:100%;padding-bottom:15px;">
                                                                                    <table cellpadding="0" cellspacing="0" style="width:100%;">
																						<!-- ADD OPTION COURSE/ELECTIVE GROUP SELECTION -->
																						<tr>
																							<td class="portletLight" style="padding-top:15px;padding-left:10px;">
																								<asp:radiobuttonlist id="optTodo" runat="server" cssclass="small" style="font-weight:bold;" repeatdirection="horizontal" autopostback="true">
																									<asp:listitem value="AddCourse">Add Course</asp:listitem>
																									<asp:listitem value="AddOptionElectiveGroup">Add Electives Group</asp:listitem>
																								</asp:radiobuttonlist>
																							</td>
																						</tr>
																					</table>
                                                                                    <!-- ADD OPTION COURSE PANEL -->
                                                                                    <asp:panel id="panAddCourse" runat="server">
                                                                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
																						    <asp:panel runat="server" id="panError">
																							    <tr>
																								    <td class="portletLight" colspan="2" style="color:red;padding-left:10px;padding-top:15px;">
																									    <asp:label runat="server" id="lblErrorMsg"></asp:label>
																								    </td>
																							    </tr>
																						    </asp:panel>
																						    <!-- COURSE SUBJECT AREA SELECTION -->
																						    <tr>
																							    <td class="portletLight" style="padding-top:15px;padding-left:10px;">
																								    <b>Subject Area:</b>&nbsp;&nbsp;
                                                                                                    <asp:dropdownlist id="cboCrsDept" runat="server" cssclass="small" autopostback="true" OnFocus="document.frmPrereq.hidFocus.value=this.id;"></asp:dropdownlist>
																							    </td>
																						    </tr>
																						    <!-- COURSE CATALOG NBR SELECTION -->
																						    <tr>
																							    <td class="portletLight" style="padding-top:15px;padding-left:10px;">
																								    <b>Catalog Nbr:</b>&nbsp;&nbsp;
																							        <asp:dropdownlist id="cboCatalogNbr" runat="server" cssclass="small" autopostback="true" OnFocus="document.frmPrereq.hidFocus.value=this.id;"></asp:dropdownlist>
																							    </td>
																						    </tr>
																						    <!-- COURSE TERM -->
																						    <tr>
																							    <td class="portletLight" style="padding-top:15px;padding-left:10px;">
																								    <b>Effective Date:</b>&nbsp;&nbsp;
																								    <asp:label id="lblEFFDT" runat="server" cssclass="small"></asp:label>
																							    </td>
																						    </tr>
																						    <!-- LONG COURSE TITLE -->
																						    <tr>
																							    <td class="portletLight" style="padding-top:15px;width:100%;padding-left:10px;">
																								    <b>Long Course Title:</b>&nbsp;
																							    </td>
																						    </tr>
																						    <tr>
																							    <td class="portletLight" style="padding-top:15px;width:100%;padding-left:10px;padding-right:10px;">
																								    <asp:label id="lblCourseLongTitle" runat="server" cssclass="small"></asp:label>
																							    </td>
																						    </tr>
																						    <!-- PREREQUISITE FOOTNOTE SELECTION -->
																						    <tr>
																							    <td class="portletLight" style="padding-top:15px;padding-left:10px;">
																								    <b>Select Footnote (optional):</b>&nbsp;&nbsp;
																							        <asp:dropdownlist id="cboFootnote" runat="server" cssclass="small" autopostback="true" OnFocus="document.frmPrereq.hidFocus.value=this.id;"></asp:dropdownlist>
																							    </td>
																						    </tr>
																						    <!-- PREREQUISITE FOOTNOTE DISPLAY -->
																						    <tr>
																							    <td class="portletLight" style="padding-top:15px;padding-left:10px;width:100%;text-align:left;">
																								    <b>Footnote:</b>
																							    </td>
																						    </tr>
																						    <tr>
																							    <td class="portletLight" style="padding-top:15px;padding-left:10px;padding-right:10px;width:100%;text-align:left;">
																								    <asp:label id="lblFootnote" runat="server" cssclass="small">None Selected</asp:label>
																							    </td>
																						    </tr>
																						    <!-- BUTTONS -->
																						    <tr>
																							    <td class="portletLight" style="padding-top:20px;width:100%;text-align:center;">
																								    <input type="button" id="cmdClose" value="Close Window" class="small" style="background:#ccccaa;border:1px solid #000000;width:85px;" onclick="self.close();" />
																								    &nbsp;<asp:button runat="server" id="cmdAddPrereq" text="Add Option Prerequisite" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:135px;" onclientclick="return getProgOptValues();" onclick="cmdAddPrereq_Click"></asp:button>
																							    </td>
																						    </tr>
																					    </table>
                                                                                    </asp:panel>

																					<!-- ADD ELECTIVE GROUP PANEL -->
                                                                                    <asp:panel id="panAddElectGroup" runat="server">
																						<table cellpadding="0" cellspacing="0" style="width:100%;">
																							<asp:panel runat="server" id="panError2">
																								<tr>
																									<td class="portletLight" colspan="2" style="color:red;padding-left:10px;padding-top:15px;">
																										<asp:label runat="server" id="lblErrorMsg2"></asp:label>
																									</td>
																								</tr>
																							</asp:panel>
																							<!-- ELECTIVE GROUP SELECTION -->
																							<tr>
																								<td class="portletLight" colspan="2" style="padding-top:15px;width:100%;padding-left:10px;">
																									<b>Elective Group:</b>&nbsp;
																								</td>
																							</tr>
																							<tr>
																								<td class="portletLight" colspan="2" style="padding-top:5px;width:100%;padding-left:10px;padding-right:10px;">
																									<asp:dropdownlist id="cboElectGroupTitle" runat="server" cssclass="small"></asp:dropdownlist>
																								</td>
																							</tr>
																							<!-- ELECTIVE GROUP COURSE QUARTER -->
                                                                                            <!--
																							<tr>
																								<td class="portletLight" style="padding-top:15px;width:185px;text-align:right;">
																									<b>Course Quarter:</b>&nbsp;&nbsp;
																								</td>
																								<td class="portletLight" style="padding-top:15px;width:115px;">
																									<asp:dropdownlist id="cboElectGroupQuarter" runat="server" cssclass="small">
																										<asp:listitem value="0">&nbsp;</asp:listitem>
																										<asp:listitem value="1">1</asp:listitem>
																										<asp:listitem value="2">2</asp:listitem>
																										<asp:listitem value="3">3</asp:listitem>
																										<asp:listitem value="4">4</asp:listitem>
																										<asp:listitem value="5">5</asp:listitem>
																										<asp:listitem value="6">6</asp:listitem>
																										<asp:listitem value="7">7</asp:listitem>
																										<asp:listitem value="8">8</asp:listitem>
																										<asp:listitem value="9">9</asp:listitem>
																										<asp:listitem value="10">10</asp:listitem>
																									</asp:dropdownlist>
																								</td>
																							</tr>
                                                                                            -->
																							<!-- MINIMUM ELECTIVE CREDITS -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;width:185px;text-align:right;">
																									<b>Minimum Units:</b>&nbsp;&nbsp;
																								</td>
																								<td class="portletLight" style="padding-top:15px;width:115px;">
																									<asp:textbox id="txtUnitsMinimum" runat="server" cssclass="small" maxlength="4" style="width:25px;"></asp:textbox>
																								</td>
																							</tr>
																							<!-- MAXIMUM ELECTIVE CREDITS -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;text-align:right;">
																									<b>Maximum Units:</b>&nbsp;&nbsp;
																								</td>
																								<td class="portletLight" style="padding-top:15px;">
																									<asp:textbox id="txtUnitsMaximum" runat="server" cssclass="small" maxlength="4" style="width:25px;"></asp:textbox>
																								</td>
																							</tr>
																							<!-- COURSE ELECTIVE FOOTNOTE SELECTION -->
																							<tr>
																								<td class="portletLight" style="padding-top:15px;text-align:right;">
																									<b>Select Footnote (optional):</b>&nbsp;&nbsp;
																								</td>
																								<td class="portletLight" style="padding-top:15px;">
																									<asp:dropdownlist id="cboElectGroupFootnote" runat="server" cssclass="small" OnFocus="document.frmOptCrs.hidFocus.value=this.id;" autopostback="true"></asp:dropdownlist>
																								</td>
																							</tr>
																							<!-- COURSE ELECTIVE FOOTNOTE DISPLAY -->
																							<tr>
																								<td class="portletLight" colspan="2" style="padding-top:15px;padding-left:10px;width:100%;text-align:left;">
																									<b>Footnote:</b>
																								</td>
																							</tr>
																							<tr>
																								<td class="portletLight" colspan="2" style="padding-top:15px;padding-left:10px;padding-right:10px;width:100%;text-align:left;">
																									<asp:label id="lblElectGroupFootnote" runat="server" cssclass="small">None Selected</asp:label>
																								</td>
																							</tr>
																							<!-- BUTTONS -->
																							<tr>
																								<td class="portletLight" colspan="2" style="padding-top:20px;width:100%;text-align:center;">
																									<input type="button" id="cmdClose2" value="Close Window" class="small" style="background:#ccccaa;border:1px solid #000000;width:85px;" onclick="self.close();" />
																									&nbsp;<asp:button runat="server" id="cmdAddElectGroup" text="Add Elective Group" cssclass="small"
																										style="background:#ccccaa;border:1px solid #000000;width:135px;" onclientclick="return validate();" onclick="cmdAddElectGroup_Click"></asp:button>
																								</td>
																							</tr>
																						</table>
																					</asp:panel>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<script type="text/javascript">
			<!--
				var hidFocus = document.getElementById("hidFocus");
				
				if(hidFocus.value != ""){
					document.getElementById(hidFocus.value).focus();
				}else{
					document.frmPrereq.cboCrsDept.focus();
				}
			//-->
			</script>
		</form>
	</body>
</html>
