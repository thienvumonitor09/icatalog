USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCollege]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/9/17
-- Description:	Returns college details
--		 INPUT: CollegeShortTitle - (SCC or SFCC)
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCollege]
	@CollegeShortTitle varchar(4)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT CollegeLongTitle, CollegeAddress, CollegeCity, CollegeState, CollegeZipCode1, CollegeZipCode2, CollegeWebsiteURL
    FROM College
    WHERE CollegeShortTitle = @CollegeShortTitle;
    
END

GO
