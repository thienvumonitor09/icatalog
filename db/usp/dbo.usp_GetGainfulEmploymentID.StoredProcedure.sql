USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetGainfulEmploymentID]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/15/17
-- Description:	Returns the GainfulEmploymentID for all program options based on ProgramID.
--		 INPUT: ProgramID - Non-changing program id
--				STRM (optional) - Term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetGainfulEmploymentID]
	@ProgramID int,
	@STRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)
					 
    SELECT GainfulEmploymentID 
    FROM ProgramDegreeOption 
    INNER JOIN ProgramDegree 
    ON ProgramDegree.ProgramDegreeID = ProgramDegreeOption.ProgramDegreeID 
    INNER JOIN Program 
    ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
    WHERE ProgramID = @ProgramID
    AND GainfulEmploymentID IS NOT NULL 
    AND GainfulEmploymentID <> '' 
    AND ProgramBeginSTRM <= @STRM 
    AND (ProgramEndSTRM = 'Z999'  OR ProgramEndSTRM >= @STRM) 
    GROUP BY GainfulEmploymentID, ProgramID;
END

GO
