using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.cpg
{
	/// <summary>
	/// Summary description for add.
	/// </summary>
	public partial class edit : System.Web.UI.Page
	{
		programData csProgram = new programData();
		termData csTerm = new termData();

		#region PROTECTED CONTROLS

		protected System.Web.UI.WebControls.DropDownList cboPrograms;

		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panDisplay.Visible = false;
			panEdit.Visible = false;
			panConfirm.Visible = false;
			panPreview.Visible = false;
			panCPGBack2.Visible = false;
			panError.Visible = false;

			if(!IsPostBack){
				DataSet dsTerms = csProgram.GetTermsForPrograms();
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
                    cboTerm.Items.Add(new ListItem(dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString(), dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString()));
				}
				cboTerm.SelectedValue = Request.QueryString["strm"];

				DataSet dsCollege = csProgram.GetCollegeList();
				for(Int32 intDSRow = 0; intDSRow < dsCollege.Tables[0].Rows.Count; intDSRow++){
                    cboOfferedAt.Items.Add(new ListItem(dsCollege.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString(), dsCollege.Tables[0].Rows[intDSRow]["CollegeID"].ToString()));
				}
				cboOfferedAt.SelectedValue = Request.QueryString["colid"];
			}

			if(hidTodo.Value == "edit"){

				DataSet dsCareerPlanningGuide = csProgram.GetCareerPlanningGuide(Convert.ToInt32(hidProgramVersionID.Value));
				Int32 intRecordCount = dsCareerPlanningGuide.Tables[0].Rows.Count;

				if(intRecordCount > 0){
					String programDisplay = dsCareerPlanningGuide.Tables[0].Rows[0]["ProgramDisplay"].ToString();

					cboCareerPlanningGuideFormat.Items.Clear();
					if(programDisplay == "2"){
                        cboCareerPlanningGuideFormat.Items.Add(new ListItem("Large Text Field Display", "3"));
					}else{
                        cboCareerPlanningGuideFormat.Items.Add(new ListItem("Two Column Display (default)", "1"));
                        cboCareerPlanningGuideFormat.Items.Add(new ListItem("One Column Display (centered)", "2"));

						try{
							cboCareerPlanningGuideFormat.SelectedValue = dsCareerPlanningGuide.Tables[0].Rows[0]["CareerPlanningGuideFormat"].ToString();
						}catch{
							cboCareerPlanningGuideFormat.SelectedValue = "1";
						}
					}

					lblProgramTitle.Text = dsCareerPlanningGuide.Tables[0].Rows[0]["ProgramTitle"].ToString();
                    lblDegree.Text = dsCareerPlanningGuide.Tables[0].Rows[0]["DegreeLongTitle"].ToString();
					lblProgramBeginSTRM.Text = dsCareerPlanningGuide.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString();
					lblProgramEndSTRM.Text = dsCareerPlanningGuide.Tables[0].Rows[0]["EndTerm_DESCR"].ToString();
					txtProgramEnrollment.Text = dsCareerPlanningGuide.Tables[0].Rows[0]["ProgramEnrollment"].ToString();
					txtProgramCourseOfStudy.Value = dsCareerPlanningGuide.Tables[0].Rows[0]["ProgramCourseOfStudy"].ToString();
					txtProgramGoals.Value = dsCareerPlanningGuide.Tables[0].Rows[0]["ProgramGoals"].ToString();
					txtProgramCareerOpportunities.Value = dsCareerPlanningGuide.Tables[0].Rows[0]["ProgramCareerOpportunities"].ToString();
					txtProgramWebsiteURL.Text = dsCareerPlanningGuide.Tables[0].Rows[0]["ProgramWebsiteURL"].ToString();
					lblOfferedAt.Text = dsCareerPlanningGuide.Tables[0].Rows[0]["CollegeShortTitle"].ToString();
					optPublishedCareerPlanningGuide.SelectedValue = dsCareerPlanningGuide.Tables[0].Rows[0]["PublishedCareerPlanningGuide"].ToString();
					hidPublishedCareerPlanningGuideID.Value = dsCareerPlanningGuide.Tables[0].Rows[0]["PublishedCareerPlanningGuideID"].ToString();
					hidCareerPlanningGuideID.Value = dsCareerPlanningGuide.Tables[0].Rows[0]["CareerPlanningGuideID"].ToString();
					lblCategory.Text = dsCareerPlanningGuide.Tables[0].Rows[0]["CategoryTitle"].ToString();
				}else{
					hidCareerPlanningGuideID.Value = "0";
				}

				panEdit.Visible = true;
				container.Visible = true;
			}else if(hidTodo.Value == "delete"){
				//update the CPG data to NULL for the selected year...effective date when versioning is in place
				csProgram.DeleteProgCPGData(Convert.ToInt32(hidCareerPlanningGuideID.Value), hidPublishedCareerPlanningGuideID.Value, Request.Cookies["phatt2"]["userctclinkid"], DateTime.Now);
				GenerateProgramList();
				panDisplay.Visible = true;
				container.Visible = true;
			}else{
				GenerateProgramList();
				panDisplay.Visible = true;
				container.Visible = true;
			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			Int32 programVersionID = Convert.ToInt32(Request.Form["hidProgramVersionID"]);
			String programCourseOfStudy = Request.Form["txtProgramCourseOfStudy"].Replace("&lt;","<").Replace("&gt;",">").Replace("&amp;","&");
			String programGoals = Request.Form["txtProgramGoals"].Replace("&lt;","<").Replace("&gt;",">").Replace("&amp;","&");
			String programCareerOpportunities = Request.Form["txtProgramCareerOpportunities"].Replace("&lt;","<").Replace("&gt;",">").Replace("&amp;","&");
			String programEnrollment = Request.Form["txtProgramEnrollment"], publishedCareerPlanningGuide = Request.Form["optPublishedCareerPlanningGuide"];
			String careerPlanningGuideFormat = Request.Form["cboCareerPlanningGuideFormat"];
			String programWebsiteURL = Request.Form["txtProgramWebsiteURL"];

			if(csProgram.Strip(programCourseOfStudy).Replace("&nbsp;"," ").Trim() == ""){
				programCourseOfStudy = "";
			}

			if(csProgram.Strip(programGoals).Replace("&nbsp;"," ").Trim() == ""){
				programGoals = "";
			}

			if(csProgram.Strip(programCareerOpportunities).Replace("&nbsp;"," ").Trim() == ""){
				programCareerOpportunities = "";
			}

			if(csProgram.EditProgCPGData(Convert.ToInt32(hidCareerPlanningGuideID.Value), programVersionID, programEnrollment, programWebsiteURL, careerPlanningGuideFormat, programCourseOfStudy, programGoals, programCareerOpportunities, publishedCareerPlanningGuide, hidPublishedCareerPlanningGuideID.Value, Request.Cookies["phatt2"]["userctclinkid"], DateTime.Now)){

				if(programCourseOfStudy == ""){
					programCourseOfStudy = "None Entered";
				}

				if(programGoals == ""){
					programGoals = "None Entered";
				}

				if(programCareerOpportunities == ""){
					programCareerOpportunities = "None Entered";
				}
				
				if(careerPlanningGuideFormat == "1"){
					lblCareerPlanningGuideFormat.Text = "Two Column Display (default)";
				}else if(careerPlanningGuideFormat == "2"){
					lblCareerPlanningGuideFormat.Text = "One Column Display (centered)";
				}else if(careerPlanningGuideFormat == "3"){
					lblCareerPlanningGuideFormat.Text = "Large Text Field Display";
				}

				lblProgramEnrollment.Text = programEnrollment;
				lblProgramWebsiteURL.Text = programWebsiteURL;
				lblProgramTitle2.Text = lblProgramTitle.Text;
                lblDegree2.Text = lblDegree.Text;
				lblOfferedAt2.Text = lblOfferedAt.Text;
				lblProgramBeginSTRM2.Text = lblProgramBeginSTRM.Text;
				lblProgramEndSTRM2.Text = lblProgramEndSTRM.Text;
				lblCategory2.Text = lblCategory.Text;
				lblProgramCourseOfStudy.Text = programCourseOfStudy;
				lblProgramGoals.Text = programGoals;
				lblProgramCareerOpportunities.Text = programCareerOpportunities;
				if(publishedCareerPlanningGuide == "0"){
					lblPublishedCareerPlanningGuide.Text = "Working Copy";
				}else if(publishedCareerPlanningGuide == "1"){
					lblPublishedCareerPlanningGuide.Text = "Published Copy";
				}
				panConfirm.Visible = true;
				panEdit.Visible = false;

			}else{
				lblErrorMsg.Text = "Error: The update failed.";
				panError.Visible = true;
				panEdit.Visible = true;
			}
		}

		protected void cmdPreview_Click(object sender, System.EventArgs e){
			String programCareerOpportunities = Request.Form["txtProgramCareerOpportunities"];
			String programGoals = Request.Form["txtProgramGoals"];
			String programCourseOfStudy = Request.Form["txtProgramCourseOfStudy"];
			String programEnrollment = Request.Form["txtProgramEnrollment"];
			String programWebsiteURL = Request.Form["txtProgramWebsiteURL"];
			String careerPlanningGuideFormat = Request.Form["cboCareerPlanningGuideFormat"];
			Int32 programVersionID = Convert.ToInt32(Request.Form["hidProgramVersionID"]);
			String collegeShortTitle = "", collegeShortTitleDisplay = "", collegeLongTitle = "";
			DataSet dsProgramDegree = csProgram.GetProgramDegree(programVersionID);
			String degreeShortTitle = "", degreeShortTitles = "", currentTermDescription = csTerm.GetTermDescription(csTerm.GetCurrentTerm());

			for(Int32 i = 0; i < dsProgramDegree.Tables[0].Rows.Count; i++){
				Int32 programDegreeID = Convert.ToInt32(dsProgramDegree.Tables[0].Rows[i]["ProgramDegreeID"]);
				degreeShortTitle += ", " + dsProgramDegree.Tables[0].Rows[i]["DegreeShortTitle"].ToString();
				degreeShortTitles = degreeShortTitle;
				String fileName = csProgram.GetDegreeRequirementWorksheetByProgramDegreeID(programDegreeID);
				if(fileName != null && fileName != ""){
					degreeShortTitles += ": <span style=\"font-weight:normal;\"><a href=\"#\" onclick=\"popupCenter('../degworksheet/" + fileName + "','Requirements', 950, 950);\">Requirements/Worksheet</a></span>";
				}
			}
			degreeShortTitle = degreeShortTitle.Substring(1,degreeShortTitle.Length-1);
			degreeShortTitles = degreeShortTitles.Substring(1, degreeShortTitles.Length-1);
			
			DataSet dsProgramCollege = csProgram.GetProgramColleges(programVersionID);
			Int32 intProgramCollegeCount = dsProgramCollege.Tables[0].Rows.Count;

			if(intProgramCollegeCount > 0){
				collegeShortTitleDisplay = dsProgramCollege.Tables[0].Rows[0]["CollegeShortTitle"].ToString();
				collegeLongTitle = dsProgramCollege.Tables[0].Rows[0]["CollegeLongTitle"].ToString();
			}

			//------------------------------------ Career Planning Guide Front Begins -------------------------------------------
			
			TableRow tr = new TableRow();
			tr.Attributes["style"] = "height:85px;";

			TableCell td = new TableCell();
			td.Attributes["style"] = "background:#ffffff;vertical-align:top;text-align:left;";
			
			//HEADER INFO BEGINS

			Table tblTemp = new Table();
			tblTemp.CellPadding = 2;
			tblTemp.CellSpacing = 0;

			TableRow trTemp = new TableRow();

			TableCell tdTemp = new TableCell();
			tdTemp.Attributes["style"] = "padding-right:6px;";
			tdTemp.Controls.Add(new LiteralControl("<img src=\"../../images/logo.jpg\" />"));

			trTemp.Cells.Add(tdTemp);

			tdTemp = new TableCell();
			tdTemp.CssClass = "headerText";
			tdTemp.Controls.Add(new LiteralControl(currentTermDescription + " Career Planning Guide<br /><br />"));
			tdTemp.Controls.Add(new LiteralControl("<font style=\"font-size:12pt;\">" + lblProgramTitle.Text + "</font>"));
			tdTemp.Controls.Add(new LiteralControl("<br /><br />" + collegeLongTitle));

			trTemp.Cells.Add(tdTemp);
			tblTemp.Rows.Add(trTemp);

			//HEADER INFO ENDS

			td.Controls.Add(tblTemp);
			tr.Cells.Add(td);
			tblCPGFront.Rows.Add(tr);

			tr = new TableRow();
			tr.Attributes["style"] = "height:70px;";

			td = new TableCell();
			td.Attributes["style"] = "background:#ffffff;width:100%;vertical-align:top;text-align:left;";

			tblTemp = new Table();
			tblTemp.Attributes["style"] = "width:100%;";
			tblTemp.CellSpacing = 0;
			tblTemp.CellPadding = 2;

			trTemp = new TableRow();
			
			//COLLEGE/DEGREE INFO BEGINS
			
			tdTemp = new TableCell();
			tdTemp.Attributes["style"] = "width:320px;vertical-align:top;";
			tdTemp.CssClass = "smallText";
			
			//display college title,address,url
			DataSet dsCollege = csProgram.GetCollege(collegeShortTitleDisplay);

			if(dsCollege.Tables[0].Rows.Count > 0){
				tdTemp.Controls.Add(new LiteralControl("<b>" + dsCollege.Tables[0].Rows[0]["CollegeLongTitle"].ToString() + "</b><br />"));
				tdTemp.Controls.Add(new LiteralControl(dsCollege.Tables[0].Rows[0]["CollegeAddress"].ToString() + "<br />"));
				tdTemp.Controls.Add(new LiteralControl(dsCollege.Tables[0].Rows[0]["CollegeCity"].ToString() + ", " + dsCollege.Tables[0].Rows[0]["CollegeState"].ToString() + " " + dsCollege.Tables[0].Rows[0]["CollegeZipCode1"].ToString() + "-" + dsCollege.Tables[0].Rows[0]["CollegeZipCode2"].ToString() + "<br />"));
				tdTemp.Controls.Add(new LiteralControl("<a style=\"color:blue;\" href=\"" + dsCollege.Tables[0].Rows[0]["CollegeWebsiteURL"].ToString() + "\" target=\"self\">" + dsCollege.Tables[0].Rows[0]["CollegeWebsiteURL"].ToString() + "</a><br /><br />"));
			}
			
			//COLLEGE INFO ENDS
			
			trTemp.Cells.Add(tdTemp);

			//PROGRAM DEGREE/FEES BEGIN

			tdTemp = new TableCell();
			tdTemp.Attributes["style"] = "width:340px;vertical-align:top;";
			tdTemp.CssClass = "smallText";

			tdTemp.Controls.Add(new LiteralControl("<b>Completion Award:</b> " + degreeShortTitle + "<br /><br />"));

			if(programEnrollment.Length > 0){
				tdTemp.Controls.Add(new LiteralControl("<b>Start:</b> " + programEnrollment + "<br /><br />"));
			}

			tdTemp.Controls.Add(new LiteralControl("<b>Tuition and Fees:</b>&nbsp;<a href=\"http://catalog.spokane.edu/fees.pdf\" target=\"self\">http://catalog.spokane.edu/fees.pdf</a><br /><br />"));

			//get the program specific fees
			DataSet dsProgramFees = csProgram.GetProgramFees(programVersionID);

			if(dsProgramFees.Tables[0].Rows.Count > 0){
				String strBookMinimum = dsProgramFees.Tables[0].Rows[0]["BookMinimum"].ToString();
				String strBookMaximum = dsProgramFees.Tables[0].Rows[0]["BookMaximum"].ToString();
				String strSuppliesMinimum = dsProgramFees.Tables[0].Rows[0]["SuppliesMinimum"].ToString();
				String strSuppliesMaximum = dsProgramFees.Tables[0].Rows[0]["SuppliesMaximum"].ToString();
				String strMiscMinimum = dsProgramFees.Tables[0].Rows[0]["MiscMinimum"].ToString();
				String strMiscMaximum = dsProgramFees.Tables[0].Rows[0]["MiscMaximum"].ToString();
				String note = dsProgramFees.Tables[0].Rows[0]["Note"].ToString();
		
				Table tblTemp2 = new Table();
				tblTemp2.Attributes["style"] = "width:100%;";
				tblTemp2.CellSpacing = 0;
				tblTemp2.CellPadding = 0;

				TableRow trTemp2 = new TableRow();
				TableCell tdTemp2 = new TableCell();

				tdTemp2.CssClass = "smallText";
				tdTemp2.ColumnSpan = 3;
				tdTemp2.Controls.Add(new LiteralControl("<b>Additional Costs</b>"));

				trTemp2.Cells.Add(tdTemp2);
				tblTemp2.Rows.Add(trTemp2);

				if(strBookMinimum != "" && strBookMaximum != ""){
					String strBookFees = "";
					Double bookMinimum = Convert.ToDouble(strBookMinimum), bookMaximum = Convert.ToDouble(strBookMaximum);

					if(bookMaximum > bookMinimum){
						strBookFees = "$" + bookMinimum.ToString() + "-" + bookMaximum.ToString();
					}else{
						strBookFees = "$" + bookMinimum.ToString();
					}

					trTemp2 = new TableRow();
					tdTemp2 = new TableCell();
				
					tdTemp2.CssClass = "smallText";
					tdTemp2.Controls.Add(new LiteralControl("Books"));

					trTemp2.Cells.Add(tdTemp2);

					tdTemp2 = new TableCell();
					tdTemp2.CssClass = "smallText";
					tdTemp2.Attributes["style"] = "text-align:right;";
					tdTemp2.Controls.Add(new LiteralControl(strBookFees));

					trTemp2.Cells.Add(tdTemp2);
					tblTemp2.Rows.Add(trTemp2);
				}

				if(strSuppliesMinimum != "" && strSuppliesMaximum != ""){
					String strSupAndEquipFees = "";
					Double suppliesMinimum = Convert.ToDouble(strSuppliesMinimum), suppliesMaximum = Convert.ToDouble(strSuppliesMaximum);

					if(suppliesMaximum > suppliesMinimum){
						strSupAndEquipFees = "$" + suppliesMinimum.ToString() + "-" + suppliesMaximum.ToString();
					}else{
						strSupAndEquipFees = "$" + suppliesMinimum.ToString();
					}

					trTemp2 = new TableRow();

					tdTemp2 = new TableCell();
					tdTemp2.CssClass = "smallText";
					tdTemp2.Controls.Add(new LiteralControl("Supplies and Equipment"));

					trTemp2.Cells.Add(tdTemp2);

					tdTemp2 = new TableCell();
					tdTemp2.CssClass = "smallText";
					tdTemp2.Attributes["style"] = "text-align:right;";
					tdTemp2.Controls.Add(new LiteralControl(strSupAndEquipFees));

					trTemp2.Cells.Add(tdTemp2);
					tblTemp2.Rows.Add(trTemp2);
				}

				if(strMiscMinimum != "" && strMiscMaximum != ""){
					String strMiscFees = "";
					Double miscMinimum = Convert.ToDouble(strMiscMinimum), miscMaximum = Convert.ToDouble(strMiscMaximum);

					if(miscMaximum > miscMinimum){
						strMiscFees = "$" + miscMinimum.ToString() + "-" + miscMaximum.ToString();
					}else{
						strMiscFees = "$" + miscMinimum.ToString();
					}

					trTemp2 = new TableRow();

					tdTemp2 = new TableCell();
					tdTemp2.CssClass = "smallText";
					tdTemp2.Controls.Add(new LiteralControl("Misc. Fees"));

					if(note != ""){
						tdTemp2.Controls.Add(new LiteralControl(" <b>*</b>"));
					}

					trTemp2.Cells.Add(tdTemp2);

					tdTemp2 = new TableCell();
					tdTemp2.CssClass = "smallText";
					tdTemp2.Attributes["style"] = "text-align:right;";
					tdTemp2.Controls.Add(new LiteralControl(strMiscFees));

					trTemp2.Cells.Add(tdTemp2);
					tblTemp2.Rows.Add(trTemp2);
				}

				if(note != ""){
					trTemp2 = new TableRow();

					tdTemp2 = new TableCell();
					tdTemp.ColumnSpan = 3;
					tdTemp2.CssClass = "smallText";
					tdTemp2.Controls.Add(new LiteralControl("<b>*</b> " + note));

					trTemp2.Cells.Add(tdTemp2);
					tblTemp2.Rows.Add(trTemp2);
				}

				tdTemp.Controls.Add(tblTemp2);
			}
			//PROGRAM FEES INFO ENDS

			trTemp.Cells.Add(tdTemp);
			tblTemp.Rows.Add(trTemp);

			td.Controls.Add(tblTemp);
			tr.Cells.Add(td);
			tblCPGFront.Rows.Add(tr);

			tr = new TableRow();

			//Temporary height fix
			if(programCareerOpportunities.Trim() == ""){
				tr.Attributes["style"] = "height:695px;";
			}else{
				tr.Attributes["style"] = "height:555px;";
			}

			td = new TableCell();
			td.Attributes["style"] = "background:#ffffff;width:100%;vertical-align:top;text-align:left;";
			
			tblTemp = new Table();
			tblTemp.Attributes["style"] = "width:100%;";
			tblTemp.CellSpacing = 0;
			tblTemp.CellPadding = 0;

			//display program url
			
			if(programWebsiteURL != null && programWebsiteURL.Trim().Length > 0){
					
				trTemp = new TableRow();

				tdTemp = new TableCell();
				tdTemp.CssClass = "mainText";
				tdTemp.Attributes["style"] = "padding-bottom:10px;";

				Table tblURLs = new Table();
				tblURLs.CellSpacing = 0;
				tblURLs.CellPadding = 0;

				TableRow trURLs = new TableRow();
				TableCell tdURLs = new TableCell();
				tdURLs.Attributes["style"] = "width:135px;";
				tdURLs.Controls.Add(new LiteralControl("<b>PROGRAM WEBSITE:</b>&nbsp;"));
				trURLs.Cells.Add(tdURLs);

				tdURLs = new TableCell();
				tdURLs.Controls.Add(new LiteralControl("<a style=\"color:blue;\" href=\"" + programWebsiteURL + "\" target=\"self\">" + programWebsiteURL + "</a>"));

				trURLs.Cells.Add(tdURLs);
				tblURLs.Rows.Add(trURLs);

				tdTemp.Controls.Add(tblURLs);
				trTemp.Cells.Add(tdTemp);
				tblTemp.Rows.Add(trTemp);
			}

			trTemp = new TableRow();

			tdTemp = new TableCell();
			tdTemp.CssClass = "headerText";
			tdTemp.Attributes["style"] = "padding-bottom:5px;";
			tdTemp.Controls.Add(new LiteralControl("Program Description"));

			//GET PROGRAM DESCRIPTION
			DataSet dsProgramDescription = csProgram.GetProgramDescription(programVersionID);
			String programDescription = "None Provided";

			if(dsProgramDescription.Tables[0].Rows.Count > 0){
				programDescription = dsProgramDescription.Tables[0].Rows[0]["ProgramDescription"].ToString();
			}

			trTemp.Cells.Add(tdTemp);
			tblTemp.Rows.Add(trTemp);

			trTemp = new TableRow();

			tdTemp = new TableCell();
			tdTemp.CssClass = "mainText";
			tdTemp.Controls.Add(new LiteralControl(programDescription));

			trTemp.Cells.Add(tdTemp);
			tblTemp.Rows.Add(trTemp);

			td.Controls.Add(tblTemp);
			tr.Cells.Add(td);
			tblCPGFront.Rows.Add(tr);

			if(programCourseOfStudy != ""){

				trTemp = new TableRow();

				tdTemp = new TableCell();
				tdTemp.Attributes["style"] = "padding-bottom:5px;padding-top:15px;";
				tdTemp.CssClass = "headerText";
				tdTemp.Controls.Add(new LiteralControl("Course of Study"));

				trTemp.Cells.Add(tdTemp);
				tblTemp.Rows.Add(trTemp);

				trTemp = new TableRow();

				tdTemp = new TableCell();
				tdTemp.CssClass = "mainText";
				tdTemp.Controls.Add(new LiteralControl(programCourseOfStudy));

				trTemp.Cells.Add(tdTemp);
				tblTemp.Rows.Add(trTemp);
				
				td.Controls.Add(tblTemp);
				tr.Cells.Add(td);
				tblCPGFront.Rows.Add(tr);
			}

			if(programGoals != ""){

				trTemp = new TableRow();

				tdTemp = new TableCell();
				tdTemp.Attributes["style"] = "padding-bottom:5px;padding-top:15px;";
				tdTemp.CssClass = "headerText";
				tdTemp.Controls.Add(new LiteralControl("Program Learning Outcomes"));

				trTemp.Cells.Add(tdTemp);
				tblTemp.Rows.Add(trTemp);

				trTemp = new TableRow();

				tdTemp = new TableCell();
				tdTemp.CssClass = "mainText";
				tdTemp.Controls.Add(new LiteralControl(programGoals));

				trTemp.Cells.Add(tdTemp);
				tblTemp.Rows.Add(trTemp);
				
				td.Controls.Add(tblTemp);
				tr.Cells.Add(td);
				tblCPGFront.Rows.Add(tr);
			}

			if(programCareerOpportunities != ""){
			
				tr = new TableRow();

				td = new TableCell();
				td.Attributes["style"] = "background:#ffffff;width:100%;vertical-align:top;text-align:left;";

				tblTemp = new Table();
				tblTemp.CellSpacing = 0;
				tblTemp.CellPadding = 0;
				tblTemp.Attributes["style"] = "width:100%;";

				trTemp = new TableRow();

				tdTemp = new TableCell();
				tdTemp.CssClass = "headerText";
				tdTemp.Attributes["style"] = "padding-bottom:5px;";
				tdTemp.Controls.Add(new LiteralControl("Career Opportunities"));

				trTemp.Cells.Add(tdTemp);
				tblTemp.Rows.Add(trTemp);

				trTemp = new TableRow();

				tdTemp = new TableCell();
				tdTemp.CssClass = "mainText";
				tdTemp.Attributes["style"] = "padding-bottom:5px;";
				tdTemp.Controls.Add(new LiteralControl(programCareerOpportunities));

				trTemp.Cells.Add(tdTemp);
				tblTemp.Rows.Add(trTemp);

				td.Controls.Add(tblTemp);
				tr.Cells.Add(td);
				tblCPGFront.Rows.Add(tr);
			}

			//-------------------------------------- Career Planning Guide Front Ends ------------------------------------------------
			//-------------------------------------- Career Planning Guide Back Begins -----------------------------------------------

			tr = new TableRow();

			td = new TableCell();
			td.ColumnSpan = 3;
			td.Attributes["style"] = "background:#ffffff;text-align:left;";
				
			//HEADER INFO BEGINS

			tblTemp = new Table();
			tblTemp.Attributes["style"] = "width:100%;";
			tblTemp.CellPadding = 2;
			tblTemp.CellSpacing = 0;

			trTemp = new TableRow();

			tdTemp = new TableCell();
			tdTemp.CssClass = "headerText";
			tdTemp.Attributes["style"] = "width:60%;";
			tdTemp.Controls.Add(new LiteralControl("<font style=\"font-size:12pt;\">" + lblProgramTitle.Text + "</font><br />"));
			if(careerPlanningGuideFormat == "3")
			{
				tdTemp.Controls.Add(new LiteralControl(degreeShortTitles + ":&nbsp;"));
			}
			else
			{
				tdTemp.Controls.Add(new LiteralControl(degreeShortTitle + ":&nbsp;"));
			}
			tdTemp.Controls.Add(new LiteralControl(collegeShortTitleDisplay + "<br />"));

			trTemp.Cells.Add(tdTemp);

			tdTemp = new TableCell();
			tdTemp.CssClass = "headerText";
			tdTemp.Attributes["style"] = "width:40%;text-align:right;vertical-align:top;";
			tdTemp.Controls.Add(new LiteralControl("Suggested Course of Study " + currentTermDescription + "<br />"));
			tdTemp.Controls.Add(new LiteralControl("<font style=\"font-size:9pt;font-weight:normal;\">Consult Adviser/Counselor for Program, Planning and Selection of Electives</font>"));
				
			trTemp.Cells.Add(tdTemp);
			tblTemp.Rows.Add(trTemp);

			td.Controls.Add(tblTemp);
			tr.Cells.Add(td);
			tblCPGBack.Rows.Add(tr);

			//HEADER INFO ENDS

			td.Controls.Add(tblTemp);
			tr.Cells.Add(td);
			tblCPGBack.Rows.Add(tr);

			/*display degrees, options, course outlines, elective groups, etc here. manually code the app to break into multiple
				columns when the text is to long. Fix all the data so it uses the database when finished with this part. Then look
				at other formats if this format does not work. And other formats for the A.A. and A.S.T. degrees. Consider seperating 
				degrees and/or program options on different CPG's.*/

			tr = new TableRow();
			tr.Attributes["style"] = "height:762px;";

			td = new TableCell();
			td.CssClass = "mainText";
			td.Attributes["style"] = "background:#ffffff;text-align:left;";

			//height counter used to calculate table height
			Int32 intHeight = 0;
			Int16 intColCtr = 1;
			bool blnAddTable = false;
			String strAlign = "";
				
			if(careerPlanningGuideFormat == "3")
			{

				td.Attributes["style"] += "width:100%;vertical-align:top;";
				td.Controls.Add(new LiteralControl(csProgram.GetProgramTextOutline(programVersionID)));
				tr.Cells.Add(td);
				tblCPGBack.Rows.Add(tr);
				tblCPGBack2.Visible = false;

			}
			else
			{

				if(careerPlanningGuideFormat == "2")
				{
					strAlign = "margin-left:auto;margin-right:auto;";
					td.Attributes["style"] += "width:100%;text-align:center;vertical-align:center;";
				}
				else
				{
					td.Attributes["style"] += "width:313px;vertical-align:top;";
				}

				//loop through the degrees
				for(Int16 intDegCtr = 0; intDegCtr < dsProgramDegree.Tables[0].Rows.Count; intDegCtr++)
				{

					Int32 programDegreeID = Convert.ToInt32(dsProgramDegree.Tables[0].Rows[intDegCtr]["ProgramDegreeID"]);
					degreeShortTitle = dsProgramDegree.Tables[0].Rows[intDegCtr]["DegreeShortTitle"].ToString();

					Table tblDegTitle = new Table();
					tblDegTitle.Attributes["style"] = "width:313px;" + strAlign;
					tblDegTitle.CellSpacing = 0;
					tblDegTitle.CellPadding = 0;

					TableRow trDegTitle = new TableRow();
					intHeight += 22;

					TableCell tdDegTitle = new TableCell();
					tdDegTitle.CssClass = "mainText";
					tdDegTitle.Attributes["style"] = "width:313px;background:#ffffff;padding-top:1px;padding-bottom:1px;font-size:12pt;text-align:left;";

					tdDegTitle.Controls.Add(new LiteralControl("<b>" + degreeShortTitle + "</b>"));

					trDegTitle.Cells.Add(tdDegTitle);
					tblDegTitle.Rows.Add(trDegTitle);

					td.Controls.Add(tblDegTitle);

					String fileName = csProgram.GetDegreeRequirementWorksheetByProgramDegreeID(programDegreeID);
					if(fileName != null && fileName != "")
					{ //show link to the degree requirement/worksheet			
						Table tblReqWkst = new Table();
						tblReqWkst.CellSpacing = 0;
						tblReqWkst.CellPadding = 0;
						tblReqWkst.Attributes["style"] = "width:313px;text-align:left;" + strAlign;
						TableRow trReqWkst = new TableRow();
						TableCell tdReqWkst = new TableCell();
						tdReqWkst.CssClass = "mainText";
						tdReqWkst.Attributes["style"] = "text-align:left;width:313px;";
						tdReqWkst.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('../degworksheet/" + fileName + "','Requirements', 950, 950);\">Requirements/Worksheet</a><br /><br />"));
						trReqWkst.Cells.Add(tdReqWkst);
						tblReqWkst.Rows.Add(trReqWkst);
						td.Controls.Add(tblReqWkst);
						intHeight += 22;
					}

					//get the degree options
					DataSet dsProgOptions = csProgram.GetProgOptions(programDegreeID);

					//loop through the degree options
					for(Int16 intOptCtr = 0; intOptCtr < dsProgOptions.Tables[0].Rows.Count; intOptCtr++)
					{
						Int32 optionID = Convert.ToInt32(dsProgOptions.Tables[0].Rows[intOptCtr]["OptionID"]);
							
						Table tblOutline = new Table();
						tblOutline.CellSpacing = 0;
						tblOutline.CellPadding = 0;
						tblOutline.Attributes["style"] = "width:313px;" + strAlign;

						TableRow trOutline = new TableRow();
						intHeight += 20;

						TableCell tdOutline = new TableCell();
						tdOutline.CssClass = "smallText";
						tdOutline.Attributes["style"] = "width:313px;padding-top:4px;padding-bottom:4px;text-align:left;";

						tdOutline.Controls.Add(new LiteralControl("<u><b>" + dsProgOptions.Tables[0].Rows[intOptCtr]["OptionTitle"].ToString() + "</b></u>"));
							
						trOutline.Cells.Add(tdOutline);
						tblOutline.Rows.Add(trOutline);

						td.Controls.Add(tblOutline);

						//get the degree prerequisites
						DataSet dsPrereqs = csProgram.GetOptPrereqs(optionID, lblProgramBeginSTRM.Text, lblProgramEndSTRM.Text);
						Int32 intPrereqRowCtr = dsPrereqs.Tables[0].Rows.Count;
						String oldCourseOffering = "";

						if(intPrereqRowCtr > 0) {
							Table tblPrereq = new Table();
							tblPrereq.CellSpacing = 0;
							tblPrereq.CellPadding = 0;
							tblPrereq.Attributes["style"] = "width:313px;" + strAlign;

							TableRow trPrereq = new TableRow();
							intHeight += 14;

							TableCell tdPrereq = new TableCell();
							tdPrereq.Attributes["style"] = "vertical-align:top;padding-top:3px;text-align:left;font-weight:bold;";
							tdPrereq.ColumnSpan = 3;
							tdPrereq.CssClass = "smallText";
							tdPrereq.Controls.Add(new LiteralControl("Prerequisites:"));

							trPrereq.Cells.Add(tdPrereq);
							tblPrereq.Rows.Add(trPrereq);

							//loop through the prerequisites
							for(Int16 intPrereqCtr = 0; intPrereqCtr < intPrereqRowCtr; intPrereqCtr++) {
								String courseOffering = dsPrereqs.Tables[0].Rows[intPrereqCtr]["CourseOffering"].ToString();
								
								if(oldCourseOffering != courseOffering){
									//display prereqs begins
									trPrereq = new TableRow();
									intHeight += 14;

									tdPrereq = new TableCell();
									tdPrereq.Attributes["style"] = "width:60px;vertical-align:top;";
									if(intPrereqCtr + 1 == intPrereqRowCtr) {
										tdPrereq.Attributes["style"] += "padding-bottom:3px;";
									}
									tdPrereq.CssClass = "smallText";

									tdPrereq.Controls.Add(new LiteralControl("�&nbsp;&nbsp;" + dsPrereqs.Tables[0].Rows[intPrereqCtr]["SUBJECT"].ToString()));

									trPrereq.Cells.Add(tdPrereq);

									tdPrereq = new TableCell();
									tdPrereq.Attributes["style"] = "width:25px;vertical-align:top;text-align:left;";
									tdPrereq.CssClass = "smallText";
									tdPrereq.Controls.Add(new LiteralControl(dsPrereqs.Tables[0].Rows[intPrereqCtr]["CATALOG_NBR"].ToString()));

									trPrereq.Cells.Add(tdPrereq);

									tdPrereq = new TableCell();
									tdPrereq.CssClass = "smallText";
									tdPrereq.Attributes["style"] = "width:225px;vertical-align:top;text-align:left;";
									tdPrereq.Controls.Add(new LiteralControl(dsPrereqs.Tables[0].Rows[intPrereqCtr]["COURSE_TITLE_LONG"].ToString()));
									tdPrereq.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + dsPrereqs.Tables[0].Rows[intPrereqCtr]["FootnoteNumber"].ToString() + "</i></span>"));

									trPrereq.Cells.Add(tdPrereq);
									tblPrereq.Rows.Add(trPrereq);
								}
								oldCourseOffering = courseOffering;
							}
							//check if a new column or table needs to be created
							td.Controls.Add(tblPrereq);
						}

						tblOutline = new Table();
						tblOutline.Attributes["style"] = "width:313px;text-align:left;" + strAlign;
						tblOutline.CellSpacing = 0;
						tblOutline.CellPadding = 0;

						DataRow[] drCourses = csProgram.GetOptionCoursesAndElectives(optionID, lblProgramBeginSTRM.Text, lblProgramEndSTRM.Text);
						Double dblTotalMinCred = 0, dblTotalMaxCred = 0, dblDegMinCred = 0, dblDegMaxCred = 0;
						Int16 intQuarter = -1;
						oldCourseOffering = "";

						//loop through option courses
						for(Int32 intCrsCtr = 0; intCrsCtr < drCourses.Length; intCrsCtr++)
						{
							String courseOffering = drCourses[intCrsCtr]["CourseOffering"].ToString();

							if(oldCourseOffering != courseOffering || courseOffering == "ZZZ"){
								String strQuarter = drCourses[intCrsCtr]["Quarter"].ToString();
									
								//determine quarter begins
								if(strQuarter != "") {

									if(intQuarter.ToString() != strQuarter) {
										intQuarter = Convert.ToInt16(strQuarter);
										trOutline = new TableRow();
										intHeight += 15;

										tdOutline = new TableCell();
										tdOutline.CssClass = "smallText";
										tdOutline.ColumnSpan = 4;
										tdOutline.Attributes["style"] = "width:313px;text-align:left;";

										if(intQuarter == 1) {
											tdOutline.Controls.Add(new LiteralControl("<b>First Quarter</b>"));
										}
										else if(intQuarter == 2) {
											tdOutline.Controls.Add(new LiteralControl("<b>Second Quarter</b>"));
										}
										else if(intQuarter == 3) {
											tdOutline.Controls.Add(new LiteralControl("<b>Third Quarter</b>"));
										}
										else if(intQuarter == 4) {
											tdOutline.Controls.Add(new LiteralControl("<b>Fourth Quarter</b>"));
										}
										else if(intQuarter == 5) {
											tdOutline.Controls.Add(new LiteralControl("<b>Fifth Quarter</b>"));
										}
										else if(intQuarter == 6) {
											tdOutline.Controls.Add(new LiteralControl("<b>Sixth Quarter</b>"));
										}
										else if(intQuarter == 7) {
											tdOutline.Controls.Add(new LiteralControl("<b>Seventh Quarter</b>"));
										}
										else if(intQuarter == 8) {
											tdOutline.Controls.Add(new LiteralControl("<b>Eighth Quarter</b>"));
										}
										else if(intQuarter == 9) {
											tdOutline.Controls.Add(new LiteralControl("<b>Ninth Quarter</b>"));
										}
										else if(intQuarter == 10) {
											tdOutline.Controls.Add(new LiteralControl("<b>Tenth Quarter</a>"));
										}


										dblTotalMinCred = 0;
										dblTotalMaxCred = 0;

										trOutline.Cells.Add(tdOutline);
										tblOutline.Rows.Add(trOutline);
									}

								}
								//determine quarter ends

								//determine credit display
									
								//added on 10/18/06 to allow variable credit overwrite
								Double unitsMinimum = 0, unitsMaximum = 0;

								if(Convert.ToByte(drCourses[intCrsCtr]["OverwriteUnits"]) == 1) {
									unitsMinimum = Convert.ToDouble(drCourses[intCrsCtr]["OverwriteUnitsMinimum"]);
									unitsMaximum = Convert.ToDouble(drCourses[intCrsCtr]["OverwriteUnitsMaximum"]);
								}
								else {
									unitsMinimum = Convert.ToDouble(drCourses[intCrsCtr]["UNITS_MINIMUM"]);
									unitsMaximum = Convert.ToDouble(drCourses[intCrsCtr]["UNITS_MAXIMUM"]);
								}

								String strCredits = "", courseLongTitle = drCourses[intCrsCtr]["COURSE_TITLE_LONG"].ToString();

								if(unitsMinimum == unitsMaximum) {
									strCredits = unitsMinimum.ToString();
									dblTotalMinCred += unitsMinimum;
									dblTotalMaxCred += unitsMinimum;
								}
								else {
									strCredits = unitsMinimum + "-" + unitsMaximum;
									dblTotalMinCred += unitsMinimum;
									dblTotalMaxCred += unitsMaximum;
								}

								//display courses begins
								trOutline = new TableRow();

								if(courseLongTitle.Length > 40) {
									intHeight += (courseLongTitle.Length/40) * 14;
								}
								else {
									intHeight += 14;
								}

								tdOutline = new TableCell();
								tdOutline.Attributes["style"] = "width:38px;vertical-align:top;text-align:left;";
								tdOutline.CssClass = "smallText";

								tdOutline.Controls.Add(new LiteralControl(drCourses[intCrsCtr]["SUBJECT"].ToString()));

								trOutline.Cells.Add(tdOutline);

								tdOutline = new TableCell();
								tdOutline.Attributes["style"] = "width:25px;vertical-align:top;text-align:left;";
								tdOutline.CssClass = "smallText";
								tdOutline.Controls.Add(new LiteralControl(drCourses[intCrsCtr]["CATALOG_NBR"].ToString()));

								trOutline.Cells.Add(tdOutline);

								tdOutline = new TableCell();
								tdOutline.CssClass = "smallText";
								tdOutline.Attributes["style"] = "width:210px;vertical-align:top;text-align:left;";
								tdOutline.Controls.Add(new LiteralControl(courseLongTitle));
								tdOutline.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + drCourses[intCrsCtr]["FootnoteNumber"].ToString() + "</i></span>"));

								trOutline.Cells.Add(tdOutline);

								tdOutline = new TableCell();
								tdOutline.CssClass = "smallText";
								tdOutline.Attributes["style"] = "vertical-align:top;width:28px;text-align:right;";
								tdOutline.Controls.Add(new LiteralControl(strCredits));

								trOutline.Cells.Add(tdOutline);
								tblOutline.Rows.Add(trOutline);

							}

                            //check if the quarter or group of courses/electives is on the last course/elective
                            if ((intCrsCtr == (drCourses.Length - 1)) || (intCrsCtr < (drCourses.Length - 1) && drCourses[intCrsCtr + 1]["Quarter"].ToString() != drCourses[intCrsCtr]["Quarter"].ToString())) {
                                //display the total min and max credits for the quarter
                                String strTotalCred = "";

                                if (dblTotalMinCred < dblTotalMaxCred) {
                                    dblDegMinCred += dblTotalMinCred;
                                    dblDegMaxCred += dblTotalMaxCred;
                                    strTotalCred = dblTotalMinCred + "-" + dblTotalMaxCred;
                                } else {
                                    dblDegMinCred += dblTotalMinCred;
                                    dblDegMaxCred += dblTotalMinCred;
                                    strTotalCred = dblTotalMaxCred.ToString();
                                }
                                trOutline = new TableRow();
                                intHeight += 1;
                                tdOutline = new TableCell();
                                tdOutline.ColumnSpan = 3;
                                trOutline.Cells.Add(tdOutline);
                                tdOutline = new TableCell();
                                tdOutline.Attributes["style"] = "border-bottom: 1px solid #000000;";
                                trOutline.Cells.Add(tdOutline);
                                tblOutline.Rows.Add(trOutline);

                                trOutline = new TableRow();
                                intHeight += 14;
                                tdOutline = new TableCell();
                                tdOutline.ColumnSpan = 4;
                                tdOutline.CssClass = "smallText";
                                tdOutline.Attributes["style"] = "text-align:right";
                                tdOutline.Controls.Add(new LiteralControl(strTotalCred));
                                trOutline.Cells.Add(tdOutline);
                                tblOutline.Rows.Add(trOutline);
                            }

                            //checked in loop of option courses and electives
                            if ((intHeight > 760 && intColCtr == 1) || (intHeight > 2280 && intColCtr == 3)) {

                                td.Controls.Add(tblOutline);
                                tr.Cells.Add(td);

                                intColCtr++;
                                td = new TableCell();
                                td.Attributes["style"] = "width:313px;background:#ffffff;vertical-align:top;text-align:left;";

                                tblOutline = new Table();
                                tblOutline.Attributes["style"] = "width:100%;";
                                tblOutline.CellSpacing = 0;
                                tblOutline.CellPadding = 0;

                            } else if (intHeight > 1520 && intColCtr == 2) {

                                blnAddTable = true;
                                intColCtr++;

                                td.Controls.Add(tblOutline);
                                tr.Cells.Add(td);
                                tblCPGBack.Rows.Add(tr);

                                tr = new TableRow();

                                td = new TableCell();
                                td.ColumnSpan = 3;
                                td.Attributes["style"] = "background:#ffffff;text-align:left;";

                                //HEADER INFO BEGINS

                                tblTemp = new Table();
                                tblTemp.Attributes["style"] = "width:100%;";
                                tblTemp.CellPadding = 2;
                                tblTemp.CellSpacing = 0;

                                trTemp = new TableRow();

                                tdTemp = new TableCell();
                                tdTemp.CssClass = "headerText";
                                tdTemp.Attributes["style"] = "width:60%;";
                                tdTemp.Controls.Add(new LiteralControl("<font style=\"font-size:12pt;\">" + lblProgramTitle.Text + "</font><br />"));
                                tdTemp.Controls.Add(new LiteralControl(degreeShortTitle + ": " + collegeShortTitle + "<br />"));

                                trTemp.Cells.Add(tdTemp);

                                tdTemp = new TableCell();
                                tdTemp.CssClass = "headerText";
                                tdTemp.Attributes["style"] = "width:40%;text-align:right;vertical-align:top;";
                                tdTemp.Controls.Add(new LiteralControl("Suggested Course of Study " + currentTermDescription + "<br />"));
                                tdTemp.Controls.Add(new LiteralControl("<font style=\"font-size:9pt;font-weight:normal;\">Consult Advisor/Counselor for Program, Planning and Selection of Electives</font>"));

                                trTemp.Cells.Add(tdTemp);
                                tblTemp.Rows.Add(trTemp);

                                td.Controls.Add(tblTemp);
                                tr.Cells.Add(td);
                                tblCPGBack2.Rows.Add(tr);

                                //HEADER INFO ENDS

                                tr = new TableRow();
                                tr.Attributes["style"] = "height:762px;";

                                td = new TableCell();
                                td.CssClass = "mainText";
                                td.Attributes["style"] = "background:#ffffff;width:313px;vertical-align:top;text-align:left;";

                                tblOutline = new Table();
                                tblOutline.Attributes["style"] = "width:100%;";
                                tblOutline.CellSpacing = 0;
                                tblOutline.CellPadding = 0;
                            }

							oldCourseOffering = courseOffering;
						}

						//display total credits for degree option
						String strOptCr = "";

						if(dblDegMinCred < dblDegMaxCred)
						{
							strOptCr = dblDegMinCred + "-" + dblDegMaxCred;
						}
						else
						{
							strOptCr = dblDegMinCred.ToString();
						}

						if(dblDegMinCred != 0)
						{
							strOptCr += " credits are required for the " + degreeShortTitle;
						}
						else
						{
							strOptCr = "";
						}

						trOutline = new TableRow();
						intHeight += 16;
						tdOutline = new TableCell();
						tdOutline.ColumnSpan = 4;
						tdOutline.CssClass = "smallText";
						tdOutline.Attributes["style"] = "padding-bottom:5px;padding-top:5px;text-align:left;";
						tdOutline.Controls.Add(new LiteralControl(strOptCr));
						trOutline.Cells.Add(tdOutline);
						tblOutline.Rows.Add(trOutline);

						td.Controls.Add(tblOutline);
						//display courses ends

						//display elective groups starts
						DataSet dsElectGroup = csProgram.GetOptionElectiveGroups(optionID);

						/*electives are being added to td and tr after td and tr are added to tblCPGBack
							The display and table format seems ok, check into this more*/

						for(Int16 intElectGroupCtr = 0; intElectGroupCtr < dsElectGroup.Tables[0].Rows.Count; intElectGroupCtr++) {
							Int32 intElectiveCount = Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroupCtr]["ElectiveCount"]);

							if(intElectiveCount > 0) {
								Table tblOutline2 = new Table();
								tblOutline2.CellPadding = 0;
								tblOutline2.CellSpacing = 0;
								tblOutline2.Attributes["style"] = "width:313px;" + strAlign;

								TableRow trOutline2 = new TableRow();
								intHeight += 16;

								TableCell tdOutline2 = new TableCell();
								tdOutline2.ColumnSpan = 4;
								tdOutline2.CssClass = "smallText";
								tdOutline2.Attributes["style"] = "padding-top:5px;text-align:left;";
								tdOutline2.Controls.Add(new LiteralControl("<b><u>" + dsElectGroup.Tables[0].Rows[intElectGroupCtr]["ElectiveGroupTitle"].ToString() + "</u></b>&nbsp;<i><font style=\"font-size:7pt;\">" + dsElectGroup.Tables[0].Rows[intElectGroupCtr]["FootnoteNumber"].ToString() + "</font></i>"));

								trOutline2.Cells.Add(tdOutline2);
								tblOutline2.Rows.Add(trOutline2);

								DataSet dsElectiveGroupCourses = csProgram.GetElectiveGroupCourses(Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroupCtr]["OptionElectiveGroupID"]), lblProgramBeginSTRM.Text, lblProgramEndSTRM.Text);

								for(Int32 intElectCtr = 0; intElectCtr < dsElectiveGroupCourses.Tables[0].Rows.Count; intElectCtr++) {
									//display electives begin
									Double dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["UNITS_MINIMUM"]);
									Double dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["UNITS_MAXIMUM"]);
									String strElectCredits = "", courseLongTitle = dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["COURSE_TITLE_LONG"].ToString();

									if(Convert.ToByte(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnits"]) == 1) {
										dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnitsMinimum"]);
										dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["OverwriteUnitsMaximum"]);
									}

									if(dblElectCredMin == dblElectCredMax) {
										strElectCredits = dblElectCredMin.ToString();
									}
									else {
										strElectCredits = dblElectCredMin + "-" + dblElectCredMax;
									}

									trOutline2 = new TableRow();

									if(courseLongTitle.Length > 40) {
										intHeight += (courseLongTitle.Length/40) * 14;
									}
									else {
										intHeight += 14;
									}

									//check COURSE_TITLE_LONG character length to add to height compensation

									tdOutline2 = new TableCell();
									tdOutline2.Attributes["style"] = "width:38px;vertical-align:top;text-align:left;";
									tdOutline2.CssClass = "smallText";

									tdOutline2.Controls.Add(new LiteralControl(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["SUBJECT"].ToString()));

									trOutline2.Cells.Add(tdOutline2);

									tdOutline2 = new TableCell();
									tdOutline2.Attributes["style"] = "width:25px;vertical-align:top;text-align:left;";
									tdOutline2.CssClass = "smallText";
									tdOutline2.Controls.Add(new LiteralControl(dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["CATALOG_NBR"].ToString()));

									trOutline2.Cells.Add(tdOutline2);

									tdOutline2 = new TableCell();
									tdOutline2.CssClass = "smallText";
									tdOutline2.Attributes["style"] = "width:210px;vertical-align:top;text-align:left;";
									tdOutline2.Controls.Add(new LiteralControl(courseLongTitle));
									tdOutline2.Controls.Add(new LiteralControl("&nbsp;<span style=\"font-size:7pt;vertical-align:top;\"><i>" + dsElectiveGroupCourses.Tables[0].Rows[intElectCtr]["FootnoteNumber"].ToString() + "</i></span>"));

									trOutline2.Cells.Add(tdOutline2);

									tdOutline2 = new TableCell();
									tdOutline2.CssClass = "smallText";
									tdOutline2.Attributes["style"] = "vertical-align:top;width:28px;text-align:right;";
									tdOutline2.Controls.Add(new LiteralControl(strElectCredits));

									trOutline2.Cells.Add(tdOutline2);
									tblOutline2.Rows.Add(trOutline2);
									//display electives end

									if(((intHeight > 760 && intColCtr == 1) || (intHeight > 2280 && intColCtr == 3)) && careerPlanningGuideFormat == "1") {
									
										td.Controls.Add(tblOutline2);
										tr.Cells.Add(td);

										intColCtr++;
										td = new TableCell();
										td.Attributes["style"] = "width:313px;background:#ffffff;vertical-align:top;text-align:left;";
										
										tblOutline2 = new Table();
										tblOutline2.Attributes["style"] = "width:313px;";
										tblOutline2.CellSpacing = 0;
										tblOutline2.CellPadding = 0;
								
									}
									else if(intHeight > 1520 && (intColCtr == 2 || careerPlanningGuideFormat != "1")) {

										blnAddTable = true;
										intColCtr++;
										
										td.Controls.Add(tblOutline2);
										tr.Cells.Add(td);
										tblCPGBack.Rows.Add(tr);

										tr = new TableRow();

										td = new TableCell();
										td.ColumnSpan = 3;
										td.Attributes["style"] = "background:#ffffff;text-align:left;";
						
										//HEADER INFO BEGINS

										tblTemp = new Table();
										tblTemp.Attributes["style"] = "width:100%;";
										tblTemp.CellPadding = 2;
										tblTemp.CellSpacing = 0;

										trTemp = new TableRow();

										tdTemp = new TableCell();
										tdTemp.CssClass = "headerText";
										tdTemp.Attributes["style"] = "width:60%;";
										tdTemp.Controls.Add(new LiteralControl("<font style=\"font-size:12pt;\">" + lblProgramTitle.Text + "</font><br />"));
										tdTemp.Controls.Add(new LiteralControl(degreeShortTitle + ": " + collegeShortTitleDisplay + "<br />"));

										trTemp.Cells.Add(tdTemp);

										tdTemp = new TableCell();
										tdTemp.CssClass = "headerText";
										tdTemp.Attributes["style"] = "width:40%;text-align:right;vertical-align:top;";
										tdTemp.Controls.Add(new LiteralControl("Suggested Course of Study " + currentTermDescription + "<br />"));
										tdTemp.Controls.Add(new LiteralControl("<font style=\"font-size:9pt;font-weight:normal;\">Consult Advisor/Counselor for Program, Planning and Selection of Electives</font>"));
						
										trTemp.Cells.Add(tdTemp);
										tblTemp.Rows.Add(trTemp);

										td.Controls.Add(tblTemp);
										tr.Cells.Add(td);
										tblCPGBack2.Rows.Add(tr);

										//HEADER INFO ENDS

										tr = new TableRow();
										tr.Attributes["style"] = "height:762px;";

										td = new TableCell();
										td.CssClass = "mainText";
										td.Attributes["style"] = "background:#ffffff;width:313px;vertical-align:top;text-align:left;";

										tblOutline2 = new Table();
										tblOutline2.Attributes["style"] = "width:100%;";
										tblOutline2.CellSpacing = 0;
										tblOutline2.CellPadding = 0;
									}
								}
								td.Controls.Add(tblOutline2);
							}
						} 
						//display elective groups ends

						//display footnotes begins
						DataSet dsFootnotes = csProgram.GetOptFootnotes(optionID);

						Table tblOutline3 = new Table();
						tblOutline3.CellSpacing = 0;
						tblOutline3.CellPadding = 0;
						tblOutline3.Attributes["style"] = "width:313px;" + strAlign;

						for(Int32 intFootnoteCtr = 0; intFootnoteCtr < dsFootnotes.Tables[0].Rows.Count; intFootnoteCtr++) {
							String footnote = dsFootnotes.Tables[0].Rows[intFootnoteCtr]["Footnote"].ToString();

							TableRow trOutline3 = new TableRow();
							//trOutline3.Attributes["style"] = "height:13px;";

							if(footnote.Length > 55) {
								intHeight += (footnote.Length/55) * 13;
							}
							else {
								intHeight += 13;
							}

							TableCell tdOutline3 = new TableCell();
							tdOutline3.CssClass = "smallText";
							tdOutline3.Attributes["style"] = "width:18px;font-size:7pt;vertical-align:top;text-align:left;";
							if(intFootnoteCtr == 0) {
								tdOutline3.Attributes["style"] += "padding-top:5px;";
							}
							tdOutline3.Controls.Add(new LiteralControl("<i>" + dsFootnotes.Tables[0].Rows[intFootnoteCtr]["FootnoteNumber"].ToString() + "</i>"));

							trOutline3.Cells.Add(tdOutline3);

							tdOutline3 = new TableCell();
							tdOutline3.CssClass = "smallText";
							tdOutline3.Attributes["style"] = "text-align:left;";
							if(intFootnoteCtr == 0) {
								tdOutline3.Attributes["style"] += "padding-top:5px;";
							}
							tdOutline3.Controls.Add(new LiteralControl("<i>" + footnote + "</i>"));

							trOutline3.Cells.Add(tdOutline3);
							tblOutline3.Rows.Add(trOutline3);

							if(((intHeight > 760 && intColCtr == 1) || (intHeight > 2280 && intColCtr == 3)) && careerPlanningGuideFormat == "1") {		
								td.Controls.Add(tblOutline3);
								tr.Cells.Add(td);

								intColCtr++;
								td = new TableCell();
								td.Attributes["style"] = "width:313px;background:#ffffff;vertical-align:top;text-align:left;";
										
								tblOutline3 = new Table();
								tblOutline3.Attributes["style"] = "width:313px;";
								tblOutline3.CellSpacing = 0;
								tblOutline3.CellPadding = 0;
							}
							else if(intHeight > 1520 && (intColCtr == 2 || careerPlanningGuideFormat != "1")) {

								blnAddTable = true;
								intColCtr++;
										
								td.Controls.Add(tblOutline3);
								tr.Cells.Add(td);
								tblCPGBack.Rows.Add(tr);

								tr = new TableRow();

								td = new TableCell();
								td.ColumnSpan = 3;
								td.Attributes["style"] = "background:#ffffff;text-align:left;";
						
								//HEADER INFO BEGINS

								tblTemp = new Table();
								tblTemp.Attributes["style"] = "width:100%;";
								tblTemp.CellPadding = 2;
								tblTemp.CellSpacing = 0;

								trTemp = new TableRow();

								tdTemp = new TableCell();
								tdTemp.CssClass = "headerText";
								tdTemp.Attributes["style"] = "width:60%;";
								tdTemp.Controls.Add(new LiteralControl("<font style=\"font-size:12pt;\">" + lblProgramTitle.Text + "</font><br />"));
								tdTemp.Controls.Add(new LiteralControl(degreeShortTitle + ": " + collegeShortTitleDisplay + "<br />"));

								trTemp.Cells.Add(tdTemp);

								tdTemp = new TableCell();
								tdTemp.CssClass = "headerText";
								tdTemp.Attributes["style"] = "width:40%;text-align:right;vertical-align:top;";
								tdTemp.Controls.Add(new LiteralControl("Suggested Course of Study " + currentTermDescription + "<br />"));
								tdTemp.Controls.Add(new LiteralControl("<font style=\"font-size:9pt;font-weight:normal;\">Consult Advisor/Counselor for Program, Planning and Selection of Electives</font>"));
						
								trTemp.Cells.Add(tdTemp);
								tblTemp.Rows.Add(trTemp);

								td.Controls.Add(tblTemp);
								tr.Cells.Add(td);
								tblCPGBack2.Rows.Add(tr);

								//HEADER INFO ENDS

								tr = new TableRow();
								tr.Attributes["style"] = "height:762px;";

								td = new TableCell();
								td.CssClass = "mainText";
								td.Attributes["style"] = "background:#ffffff;width:313px;vertical-align:top;text-align:left;";

								tblOutline3 = new Table();
								tblOutline3.Attributes["style"] = "width:100%;";
								tblOutline3.CellSpacing = 0;
								tblOutline3.CellPadding = 0;
							}
						}

						td.Controls.Add(tblOutline3);
						tr.Cells.Add(td);

						//display footnotes end
					}
				}

				tr.Cells.Add(td);

				if(blnAddTable)
				{
					tblCPGBack2.Rows.Add(tr);
					panCPGBack2.Visible = true;
				}
				else
				{
					tblCPGBack.Rows.Add(tr);
					panCPGBack2.Visible = false;
				}

				//footnotes used to be here

				if(blnAddTable)
				{
					tblCPGBack2.Rows.Add(tr);

					tr = new TableRow();
					td = new TableCell();
					if(intColCtr > 1)
					{
						td.ColumnSpan = 3;
					}
					td.Attributes["style"] = "background:#ffffff;text-align:left;width:640px";
					td.CssClass = "smallerText";
					td.Controls.Add(new LiteralControl("<i><u><b>Disclaimer:</b></u> The college cannot guarantee courses will be offered in the quarters indicated." +
						" During the period this guide is in circulation, there may be curriculum revisions and program changes. <b>Students are responsible for</b> consulting" +
						" the appropriate academic unit or adviser for more current and specific information. The information in this guide is subject to change and does not" +
						" constitute an agreement between the college and the student.</i>"));
					
					tr.Cells.Add(td);
					tblCPGBack2.Rows.Add(tr);

					panCPGBack2.Visible = true;
				}
				else
				{
					tblCPGBack.Rows.Add(tr);
				}
			}
			tr = new TableRow();
			td = new TableCell();
			if(intColCtr > 1)
			{
				td.ColumnSpan = 2;
			}
			td.Attributes["style"] = "background:#ffffff;text-align:left;width:640px";
			td.CssClass = "smallerText";
			td.Controls.Add(new LiteralControl("<i><u><b>Disclaimer:</b></u> The college cannot guarantee courses will be offered in the quarters indicated." +
				" During the period this guide is in circulation, there may be curriculum revisions and program changes. <b>Students are responsible for</b> consulting" +
				" the appropriate academic unit or adviser for more current and specific information. The information in this guide is subject to change and does not" +
				" constitute an agreement between the college and the student.</i>"));
				
			tr.Cells.Add(td);
			tblCPGBack.Rows.Add(tr);

			//--------------------------------------- Career Planning Guide Back Ends -------------------------------------------------

			panPreview.Visible = true;
			container.Visible = false;
		}

		public void GenerateProgramList(){
			TableRow tr = new TableRow();
			TableCell td = new TableCell();

			//create the alphabet selection list
			for(Int16 intAlphaCtr = 65; intAlphaCtr <= 90; intAlphaCtr++){
				td = new TableCell();
				td.Attributes["style"] = "width:23px;text-align:center;";
				td.Attributes["onclick"] = "document.frmProgram.hidTodo.value='search';document.frmProgram.hidLtr.value='" + Convert.ToChar(intAlphaCtr) + "';document.frmProgram.submit();return false;";
				td.CssClass = "portletMain";
				td.Controls.Add(new LiteralControl("<a class=\"ltr\" href=\"#\" onclick=\"document.frmProgram.hidTodo.value='search';document.frmProgram.hidLtr.value='" + Convert.ToChar(intAlphaCtr) + "';document.frmProgram.submit();return false;\">" + Convert.ToChar(intAlphaCtr) + "</a>"));
				tr.Cells.Add(td);
			}

			tblAlpha.Rows.Add(tr);

			String strLtr = hidLtr.Value;
			if(strLtr == ""){
				strLtr = "A";
			}

			//get a dataset of programs starting with the selected letter
			DataSet dsPrograms = csProgram.GetCareerPlanningGuidePrograms(Convert.ToChar(strLtr), cboTerm.SelectedValue, cboOfferedAt.SelectedValue);
			Int32 intRowCtr = dsPrograms.Tables[0].Rows.Count;
			
			//display the program header information
			tr = new TableRow();

            td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:105px;";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("Award Type"));

            tr.Cells.Add(td);

			td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;width:218px;";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("Program Title"));

			tr.Cells.Add(td);

			td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;white-space:nowrap;width:69px;";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("Offered by"));

			tr.Cells.Add(td);

			td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:85px;white-space:nowrap";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("Begin Term"));

			tr.Cells.Add(td);

			td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:85px;white-space:nowrap";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("End Term"));

			tr.Cells.Add(td);

			td = new TableCell();
            td.Attributes["style"] = "text-align:center;font-weight:bold;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:82px;white-space:nowrap";
			td.CssClass = "portletSecondary";
			td.Controls.Add(new LiteralControl("Status"));

			tr.Cells.Add(td);

			td = new TableCell();
            td.Attributes["style"] = "text-align:center;width:73px;";
			td.CssClass = "portletSecondary";

			tr.Cells.Add(td);
			tblDisplay.Rows.Add(tr);

            String oldAreaOfStudyTitle = "";

			if(intRowCtr > 0){ //if area of study titles starting with the selected letter exist

				//loop through and display the programs
				for(Int32 intDSRow = 0; intDSRow < intRowCtr; intDSRow++){
					
					Int32 careerPlanningGuideID = Convert.ToInt32(dsPrograms.Tables[0].Rows[intDSRow]["CareerPlanningGuideID"]);
					String strProgramVersionID = dsPrograms.Tables[0].Rows[intDSRow]["ProgramVersionID"].ToString();
					String programTitle = dsPrograms.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString();
					String strCareerPlanningGuideID = careerPlanningGuideID.ToString();
					String publishedCareerPlanningGuideID = dsPrograms.Tables[0].Rows[intDSRow]["PublishedCareerPlanningGuideID"].ToString();
					String publishedCareerPlanningGuide = dsPrograms.Tables[0].Rows[intDSRow]["PublishedCareerPlanningGuide"].ToString();
					String collegeShortTitle = dsPrograms.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
					String programBeginSTRM = dsPrograms.Tables[0].Rows[intDSRow]["ProgramBeginSTRM"].ToString();
					String programEndSTRM = dsPrograms.Tables[0].Rows[intDSRow]["ProgramEndSTRM"].ToString();
                    String beginTerm = dsPrograms.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString();
                    String endTerm = dsPrograms.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString();
                    String degreeShortTitle = dsPrograms.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString();
                    String areaOfStudyTitle = dsPrograms.Tables[0].Rows[intDSRow]["Title"].ToString();

					if(publishedCareerPlanningGuide == "0"){
						publishedCareerPlanningGuide = "Working Copy";
					}else if(publishedCareerPlanningGuide == "1"){
						publishedCareerPlanningGuide = "Published Copy";
					}

                    if (areaOfStudyTitle != oldAreaOfStudyTitle) {
                        tr = new TableRow();
                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "font-weight:bold;padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;width:100%;";
                        td.ColumnSpan = 7;
                        td.Controls.Add(new LiteralControl(areaOfStudyTitle));
                        tr.Cells.Add(td);
                        tblDisplay.Rows.Add(tr);
                    }

					tr = new TableRow();

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:105px;";
                    td.Controls.Add(new LiteralControl(degreeShortTitle));

                    tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:218px;";
					td.Controls.Add(new LiteralControl(programTitle));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;width:69px;";
					td.Controls.Add(new LiteralControl(collegeShortTitle));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:85px;";
					td.Controls.Add(new LiteralControl(beginTerm));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;width:85px;";
					td.Controls.Add(new LiteralControl(endTerm));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;vertical-align:top;white-space:nowrap;text-align:right;width:82px;";
					td.Controls.Add(new LiteralControl(publishedCareerPlanningGuide));

					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["nowrap"] = "nowrap";
                    td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;padding-right:6px;width:73px;";
					if(!csProgram.HasWorkingCPGCopy(careerPlanningGuideID)){	
						td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.frmProgram.hidTodo.value='edit';document.frmProgram.hidProgramVersionID.value='" + strProgramVersionID + "';document.frmProgram.submit();\">Edit</a> | "));
						td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete the " + publishedCareerPlanningGuide + " CPG data \\nfor " + programTitle.Replace("'","\\'") + " offered at " + collegeShortTitle + " \\neffective " + beginTerm + " - " + endTerm + "?')){document.frmProgram.hidTodo.value='delete';document.frmProgram.hidCareerPlanningGuideID.value='" + strCareerPlanningGuideID + "';document.frmProgram.hidPublishedCareerPlanningGuideID.value='" + publishedCareerPlanningGuideID + "';document.frmProgram.submit();}\">Delete</a>"));
					}else{
						//td.Controls.Add(new LiteralControl("<a href=\"http://icatalog.ccs.spokane.edu/program/cpg.aspx?pvid=" + programVersionID + "&bstrm=" + programBeginSTRM + "&estrm=" + programEndSTRM + "&SYRQ=" + cboTerm.SelectedValue + "&pt=" + programTitle + "\">View</a>"));
						td.Controls.Add(new LiteralControl("<a href=\"view.aspx?pvid=" + strProgramVersionID + "&bstrm=" + programBeginSTRM + "&estrm=" + programEndSTRM + "&SYRQ=" + cboTerm.SelectedValue + "&pt=" + programTitle + "\">View</a>"));
					}
					tr.Cells.Add(td);
					tblDisplay.Rows.Add(tr);

                    oldAreaOfStudyTitle = areaOfStudyTitle;
				}
			}else{
				tr = new TableRow();
				td = new TableCell();
				td.CssClass = "portletLight";
				td.ColumnSpan = 7;
				td.Attributes["style"] = "padding-top:4px;padding-bottom:4px;";
				td.Controls.Add(new LiteralControl("&nbsp;" + strLtr + " returned 0 results."));
				tr.Cells.Add(td);
				tblDisplay.Rows.Add(tr);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
