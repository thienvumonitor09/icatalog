USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseArchives]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--- Author:		Vu Nguyen
-- Create date: 05/16/2019
-- Description:	Returns a list of archived courses by subject area and term
--		 INPUT: SUBJECT (optional) - Course Subject Area: ENGL
--				STRM (optional) - Term Code	
-- Logic: Return all courses except the most recent course (by STRM or CurrentSTRM)
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseArchives]
	@SUBJECT varchar(5) = NULL,
	@STRM varchar(4) = NULL,
	@INSTITUTION varchar(5),
	@CurrentSTRM varchar(5) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SET @CurrentSTRM = (SELECT STRM FROM udf_GetCurrentSTRM());
	--DECLARE @CurrentSTRM varchar(4)= SELECT * FROM udf_GetTERM_END_DT();
	--EXEC [dbo].[usp_GetCurrentSTRM] @CurrentSTRM OUTPUT;

    SELECT  DISTINCT CourseOffering.CRSE_ID, (SUBJECT + CATALOG_NBR) AS CourseOffering, SUBJECT, CATALOG_NBR, CourseOffering.EFFDT, EFF_STATUS, DESCR, CATALOG_PRINT
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	WHERE 
		CourseOffering.EFFDT < 
		(
			SELECT MAX(CourseOffering2.EFFDT) 
			FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
			WHERE CourseOffering2.CRSE_ID = CourseOffering.CRSE_ID 
				AND INSTITUTION = @INSTITUTION
				AND
				(
					-- If @STRM IS NOT NULL, use @STRM to get TERM_END_DT, otherwise, use @CurrentSTRM
					(
						@STRM IS NOT NULL 
						AND 
						CourseOffering2.EFFDT <= (SELECT TERM_END_DT FROM udf_GetTERM_END_DT(@STRM))
					)
					OR
					(
						@STRM IS NULL 
						AND 
						CourseOffering2.EFFDT <= (SELECT TERM_END_DT FROM udf_GetTERM_END_DT(@CurrentSTRM))
					)
				)
		) 
		AND (@SUBJECT IS NULL OR (SUBJECT = @SUBJECT OR SUBJECT = @SUBJECT + '&'))
		AND ACAD_CAREER = 'UGRD'
		--AND CATALOG_PRINT = 'Y'
		--AND CourseCatalog.EFF_STATUS = 'A'
		AND CourseOffering.INSTITUTION = @INSTITUTION
		ORDER BY SUBJECT, CATALOG_NBR,EFFDT desc
	
END
GO
