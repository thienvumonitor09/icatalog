USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseDescription]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/17/2019
-- Description:	Returns a course description by CRSE_ID and @EFFDT
-- Parameters:
--		@CRSE_ID
--		@EFFDT
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseDescription]
	@CRSE_ID varchar(6),
	@EFFDT date
AS
BEGIN
	SET NOCOUNT ON;

    SELECT CourseOffering.CRSE_ID, (SUBJECT + CATALOG_NBR) AS CourseOffering, SUBJECT, CATALOG_NBR, CourseOffering.EFFDT, UNITS_MINIMUM, UNITS_MAXIMUM, EFF_STATUS, DESCR, '' as SubjectArea_DESCR, DESCRLONG, CATALOG_PRINT
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	WHERE CourseOffering.CRSE_ID=@CRSE_ID AND CourseOffering.EFFDT = @EFFDT
END
GO
