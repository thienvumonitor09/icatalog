USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDegreeProgramsByDegree]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 06/13/2019
-- Description:	Returns a list of degree programs by degree
--		 INPUT: STRM - Term code
--				LocationID (optional) - Location primary key 
--				DegreeID (optional) - Degree primary key 
--				DegreeType (optional) - Degree Type (Transfer or Career Technical)
--				SUBJECT (optional) - Course subject area 
--				CATALOG_NBR (optional) - Course catalog number 
-- Changes:
--		Added local variables to avoid parameter sniffing
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDegreeProgramsByDegree]
	@STRM varchar(4) = NULL,
	@LocationID int = NULL,
	@DegreeID int = NULL,
	@DegreeType varchar(50) = NULL,
	@SUBJECT varchar(5) = NULL,
	@CATALOG_NBR varchar(3) = NULL,
	@GlobalSearch varchar(150) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@local_STRM varchar(4) = @STRM,
		@local_LocationID int = @LocationID,
		@local_DegreeID int = @DegreeID,
		@local_DegreeType varchar(50) = @DegreeType,
		@local_SUBJECT varchar(5) = @SUBJECT,
		@local_CATALOG_NBR varchar(3) = @CATALOG_NBR,
		@local_GlobalSearch varchar(150) = @GlobalSearch;

	IF @local_STRM IS NULL
		SET @local_STRM = (SELECT STRM FROM udf_GetCurrentSTRM())
	


;WITH tmp (CourseOffering, SUBJECT, CATALOG_NBR, EFFDT, COURSE_TITLE_LONG, DESCR)
AS 
(
	SELECT ( CAST(REPLACE(SUBJECT,' ','') AS CHAR(5)) + CAST(REPLACE(CATALOG_NBR,' ','') AS CHAR(3)) ) AS CourseOffering
				, REPLACE(SUBJECT,' ',''), REPLACE(CATALOG_NBR,' ',''), CourseOffering.EFFDT , COURSE_TITLE_LONG, DESCR
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	WHERE CATALOG_PRINT = 'Y' AND EFF_STATUS = 'A'
)
SELECT CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, DocumentTitle, DegreeType, DegreeDescription
FROM Degree
INNER JOIN ProgramDegree
ON ProgramDegree.DegreeID = Degree.DegreeID				
INNER JOIN Program
ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
LEFT OUTER JOIN CareerPlanningGuide 
ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
INNER JOIN College 
ON College.CollegeID = Program.CollegeID 
INNER JOIN ProgramDegreeOption 
ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
LEFT OUTER JOIN OptionPrerequisite 
ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
LEFT OUTER JOIN OptionElectiveGroup 
ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
LEFT OUTER JOIN ElectiveGroupCourse 
ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
LEFT OUTER JOIN OptionCourse 
ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
LEFT OUTER JOIN OptionLocation 
ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
LEFT OUTER JOIN Location 
ON Location.LocationID = OptionLocation.LocationID 
LEFT OUTER JOIN tmp
ON tmp.CourseOffering  = OptionPrerequisite.CourseOffering /* DIFFERENT HERE*/
WHERE (ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM)) 
AND PublishedProgram = 1
AND
	(
		@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
	)
AND
	(
		@local_DegreeID IS NULL OR OptionLocation.LocationID = @local_LocationID
	)
AND 
	(
		@local_DegreeType IS NULL OR Degree.DegreeType = @local_DegreeType
	)
AND 
	(
		@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
	)
AND
	(
		@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
	)
AND
	(
		@local_GlobalSearch IS NULL
		OR
		( 
			--((LocationTitle  + ProgramTitle + ProgramDescription + OptionTitle + DegreeShortTitle + DegreeLongTitle + DegreeType) LIKE '%' + @local_GlobalSearch +'%')				
			( LocationTitle LIKE '%' + @GlobalSearch +'%')
			OR (ProgramTitle LIKE '%' + @GlobalSearch + '%')
			OR (ProgramDescription LIKE '%' + @GlobalSearch + '%')
			OR (OptionTitle LIKE '%' + @GlobalSearch + '%')
			OR (DegreeShortTitle LIKE '%' + @GlobalSearch + '%')
			OR (DegreeLongTitle LIKE '%' + @GlobalSearch + '%')
			OR (DegreeType LIKE '%' + @GlobalSearch + '%') 				
			OR ( (tmp.COURSE_TITLE_LONG LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
			OR ((tmp.DESCR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( (tmp.SUBJECT LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( (tmp.CATALOG_NBR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( ((tmp.SUBJECT + LTRIM(tmp.CATALOG_NBR)) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( ((tmp.SUBJECT + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR (((tmp.SUBJECT + ' ' + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
		) 
	)
--GROUP BY Program.ProgramVersionID, CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, Degree.DegreeID, Degree.DocumentTitle, DegreeType 
--ORDER BY CategoryTitle, DegreeLongTitle, OptionTitle, ProgramTitle, CollegeShortTitle;


UNION

SELECT CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, DocumentTitle, DegreeType, DegreeDescription
FROM Degree
INNER JOIN ProgramDegree
ON ProgramDegree.DegreeID = Degree.DegreeID				
INNER JOIN Program
ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
LEFT OUTER JOIN CareerPlanningGuide 
ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
INNER JOIN College 
ON College.CollegeID = Program.CollegeID 
INNER JOIN ProgramDegreeOption 
ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
LEFT OUTER JOIN OptionPrerequisite 
ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
LEFT OUTER JOIN OptionElectiveGroup 
ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
LEFT OUTER JOIN ElectiveGroupCourse 
ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
LEFT OUTER JOIN OptionCourse 
ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
LEFT OUTER JOIN OptionLocation 
ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
LEFT OUTER JOIN Location 
ON Location.LocationID = OptionLocation.LocationID 
LEFT OUTER JOIN tmp
ON tmp.CourseOffering  = ElectiveGroupCourse.CourseOffering  /* DIFFERENT HERE*/
WHERE (ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM)) 
AND PublishedProgram = 1
AND
	(
		@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
	)
AND
	(
		@local_DegreeID IS NULL OR OptionLocation.LocationID = @local_LocationID
	)
AND 
	(
		@local_DegreeType IS NULL OR Degree.DegreeType = @local_DegreeType
	)
AND 
	(
		@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
	)
AND
	(
		@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
	)
AND
	(
		@local_GlobalSearch IS NULL
		OR
		( 
			--((LocationTitle  + ProgramTitle + ProgramDescription + OptionTitle + DegreeShortTitle + DegreeLongTitle + DegreeType) LIKE '%' + @local_GlobalSearch +'%')						
			( LocationTitle LIKE '%' + @GlobalSearch +'%')
			OR (ProgramTitle LIKE '%' + @GlobalSearch + '%')
			OR (ProgramDescription LIKE '%' + @GlobalSearch + '%')
			OR (OptionTitle LIKE '%' + @GlobalSearch + '%')
			OR (DegreeShortTitle LIKE '%' + @GlobalSearch + '%')
			OR (DegreeLongTitle LIKE '%' + @GlobalSearch + '%')
			OR (DegreeType LIKE '%' + @GlobalSearch + '%') 	
			OR ( (tmp.COURSE_TITLE_LONG LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
			OR ((tmp.DESCR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( (tmp.SUBJECT LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( (tmp.CATALOG_NBR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( ((tmp.SUBJECT + LTRIM(tmp.CATALOG_NBR)) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( ((tmp.SUBJECT + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR (((tmp.SUBJECT + ' ' + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
		) 
	)
UNION

SELECT CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, DocumentTitle, DegreeType, DegreeDescription
FROM Degree
INNER JOIN ProgramDegree
ON ProgramDegree.DegreeID = Degree.DegreeID				
INNER JOIN Program
ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
LEFT OUTER JOIN CareerPlanningGuide 
ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
INNER JOIN College 
ON College.CollegeID = Program.CollegeID 
INNER JOIN ProgramDegreeOption 
ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
LEFT OUTER JOIN OptionPrerequisite 
ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
LEFT OUTER JOIN OptionElectiveGroup 
ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
LEFT OUTER JOIN ElectiveGroupCourse 
ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
LEFT OUTER JOIN OptionCourse 
ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
LEFT OUTER JOIN OptionLocation 
ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
LEFT OUTER JOIN Location 
ON Location.LocationID = OptionLocation.LocationID 
LEFT OUTER JOIN tmp
ON tmp.CourseOffering  = OptionCourse.CourseOffering  /* DIFFERENT HERE*/
WHERE (ProgramBeginSTRM <= @local_STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @local_STRM)) 
AND PublishedProgram = 1
AND
	(
		@local_LocationID IS NULL OR OptionLocation.LocationID = @local_LocationID
	)
AND
	(
		@local_DegreeID IS NULL OR OptionLocation.LocationID = @local_LocationID
	)
AND 
	(
		@local_DegreeType IS NULL OR Degree.DegreeType = @local_DegreeType
	)
AND 
	(
		@local_SUBJECT IS NULL OR ( (SUBJECT = @local_SUBJECT OR SUBJECT = @local_SUBJECT + '&') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
	)
AND
	(
		@local_CATALOG_NBR IS NULL OR (CATALOG_NBR = @local_CATALOG_NBR AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
	)
AND
	(
		@local_GlobalSearch IS NULL
		OR
		( 
			--((LocationTitle  + ProgramTitle + ProgramDescription + OptionTitle + DegreeShortTitle + DegreeLongTitle + DegreeType) LIKE '%' + @local_GlobalSearch +'%')				
			( LocationTitle LIKE '%' + @GlobalSearch +'%')
			OR (ProgramTitle LIKE '%' + @GlobalSearch + '%')
			OR (ProgramDescription LIKE '%' + @GlobalSearch + '%')
			OR (OptionTitle LIKE '%' + @GlobalSearch + '%')
			OR (DegreeShortTitle LIKE '%' + @GlobalSearch + '%')
			OR (DegreeLongTitle LIKE '%' + @GlobalSearch + '%')
			OR (DegreeType LIKE '%' + @GlobalSearch + '%') 
			OR ( (tmp.COURSE_TITLE_LONG LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)) )
			OR ((tmp.DESCR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( (tmp.SUBJECT LIKE '%' + @local_GlobalSearch + '%')  AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( (tmp.CATALOG_NBR LIKE '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( ((tmp.SUBJECT + LTRIM(tmp.CATALOG_NBR)) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR ( ((tmp.SUBJECT + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
			OR (((tmp.SUBJECT + ' ' + tmp.CATALOG_NBR) LIKE + '%' + @local_GlobalSearch + '%') AND tmp.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@local_STRM)))
		) 
	)
ORDER BY DegreeLongTitle, OptionTitle, ProgramTitle, CollegeShortTitle
END
GO
