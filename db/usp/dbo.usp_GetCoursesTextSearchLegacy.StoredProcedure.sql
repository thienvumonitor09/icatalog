USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCoursesTextSearchLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 4/10/17
-- Description:	Returns courses containing a specified string in the long description
--		 INPUT: SearchText - The string being searched in the long course description
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCoursesTextSearchLegacy] 
	-- Add the parameters for the stored procedure here
	@SearchText varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT CourseID, CourseOffering, Course.DESCR, CourseBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, CourseEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, PublishedCourse, PublishedCourseID 
	FROM Course 
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = CourseBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = CourseEndSTRM
	WHERE DESCRLONG LIKE '%' + @SearchText + '%' 
	ORDER BY CourseOffering, CourseBeginSTRM DESC;
END

GO
