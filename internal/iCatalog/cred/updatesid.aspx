<%@ Page language="c#" Inherits="ICatalog.cred.updatesid" CodeFile="updatesid.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>SID Update</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <script type="text/javascript" src="../_phatt3_src_files/trim.js"></script>
        <script type="text/javascript">
        <!--
            //check field values on submit
            function validate(){
                isValid = false;
                if(trim(document.frmCred.txtOldSID.value) == ""){
                    alert("Please enter the old ctcLink ID");
                    document.frmCred.txtOldSID.select();
                }else if(isNaN(document.frmCred.txtOldSID.value)){
                    alert("Please enter numbers only for the old ctcLink ID.");
                    document.frmCred.txtOldSID.select();
                }else if(trim(document.frmCred.txtNewSID.value) == ""){
                    alert("Please enter the new ctcLink ID");
                    document.frmCred.txtNewSID.select();
                }else if(isNaN(document.frmCred.txtNewSID.value)){
                    alert("Please enter numbers only for the new ctcLink ID.");
                    document.frmCred.txtNewSID.select();
                }else if(trim(document.frmCred.txtConfirmNewSID.value) == ""){
                    alert("Please confirm the new ctcLink ID.");
                    document.frmCred.txtConfirmNewSID.select();
                }else if(document.frmCred.txtNewSID.value != document.frmCred.txtConfirmNewSID.value){
                    alert("The new ctcLink ID and the confirmation ctcLink ID do not match. \nPlease re-enter the confirmation ctcLink ID.");
                    document.frmCred.txtConfirmNewSID.select();
                }else{
                    isValid = true;
                }
                return isValid;
            }
        //-->
        </script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmCred" runat="server">
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;ctcLink ID Update</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <asp:panel runat="server" id="panUpdate">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <!-- Error Message -->
                                                                            <asp:panel runat="server" id="panError">
                                                                                <tr>
                                                                                    <td colspan="2" class="portletMain" style="color:red;padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                        Error: The ctcLink ID entered could not be found.
                                                                                    </td>
                                                                                </tr>
                                                                            </asp:panel>
                                                                            <!-- OLD ctcLink ID -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:42%;text-align:right;">
                                                                                    <b>Old ctcLink ID:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:58%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtOldSID" cssclass="small" maxlength="9" style="width:60px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- NEW ctcLink ID -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>New ctcLink ID:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtNewSID" cssclass="small" maxlength="9" style="width:60px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- CONFIRM NEW ctcLink ID -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Confirm New ctcLink ID:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtConfirmNewSID" cssclass="small" maxlength="9" style="width:60px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                        <asp:button id="cmdSubmit" runat="server" text="Save Addition" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclientclick="return validate();" onclick="cmdSubmit_Click"></asp:button>
														                        </td>
													                        </tr>
                                                                        </table>
                                                                     </asp:panel>
                                                                     <asp:panel runat="server" id="panConfirm">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <tr>
                                                                                <td class="portletMain" style="width:100%;padding-left:5px;padding-top:3px;padding-bottom:5px;" colspan="2">
                                                                                    The following ctcLink ID has been successfully updated.
                                                                                </td>
                                                                            </tr>
                                                                            <!-- OLD ctcLink ID -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:42%;text-align:right;">
                                                                                    <b>Old ctcLink ID:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:58%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblOldSID"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- NEW ctcLink ID -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>New ctcLink ID:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:label runat="server" id="lblNewSID"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                        <input type="button" id="cmdOK" runat="server" value="OK" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="location.href='updatesid.aspx';" />
														                        </td>
													                        </tr>
                                                                        </table>
                                                                     </asp:panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <div class="clearer"></div>
        </asp:panel>
    </body>
</html>
