USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetElectiveGroupCoursesByID]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 06/14/2019
-- Description:	Returns elective group courses
--		 INPUT: OptionElectiveGroupID - Elective Group primary key
--				SelectedSTRM - Selected term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetElectiveGroupCoursesByID]
	@OptionElectiveGroupID int,
	@SelectedSTRM varchar (4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @SelectedSTRM IS NULL
		SET @SelectedSTRM = (SELECT STRM FROM udf_GetCurrentSTRM());
    
	SELECT ElectiveGroupCourse.OptionElectiveGroupID, ElectiveGroupCourse.CourseOffering, SUBJECT, CATALOG_NBR,/* CourseSuffix, CourseEndSTRM,*/ COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber, ElectiveGroupCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
	FROM ElectiveGroupCourse 
	INNER JOIN OptionElectiveGroup
	ON OptionElectiveGroup.OptionElectiveGroupID = ElectiveGroupCourse.OptionElectiveGroupID
	INNER JOIN ProgramDegreeOption
	ON ProgramDegreeOption.OptionID = OptionElectiveGroup.OptionID
	INNER JOIN ProgramDegree
	ON ProgramDegree.ProgramDegreeID = ProgramDegreeOption.ProgramDegreeID
	INNER JOIN Program 
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
		ON REPLACE(CourseOffering.SUBJECT + CourseOffering.CATALOG_NBR,' ','') = REPLACE(ElectiveGroupCourse.CourseOffering,' ','')
		LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
		ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	LEFT OUTER JOIN OptionFootnote 
	ON OptionFootnote.OptionFootnoteID = ElectiveGroupCourse.OptionFootnoteID
	WHERE ElectiveGroupCourse.OptionElectiveGroupID = @OptionElectiveGroupID
		AND CourseOffering.EFFDT = 
				(
					SELECT MAX(CourseOffering2.EFFDT)
					FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
					WHERE CourseOffering2.SUBJECT = CourseOffering.SUBJECT AND CourseOffering2.CATALOG_NBR = CourseOffering.CATALOG_NBR
						--AND INSTITUTION = 'WA171'
					AND CourseOffering2.EFFDT <= (SELECT * FROM udf_GetCurrentSTRM())
				) 
	ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
END
GO
