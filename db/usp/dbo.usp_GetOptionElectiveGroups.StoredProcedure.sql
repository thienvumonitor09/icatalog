USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionElectiveGroups]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/11/17
-- Description:	Returns program degree option elective groups
--		 INPUT: OptionID - Program degree option id (ProgOptASN)
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionElectiveGroups]
	@OptionID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT ElectiveGroupTitle, FootnoteNumber, OptionElectiveGroup.OptionFootnoteID, OptionElectiveGroup.OptionElectiveGroupID, COUNT(ElectiveGroupCourse.OptionElectiveGroupID) AS ElectiveCount 
    FROM OptionElectiveGroup 
    LEFT OUTER JOIN OptionFootnote 
    ON OptionFootnote.OptionFootnoteID = OptionElectiveGroup.OptionFootnoteID 
    LEFT OUTER JOIN ElectiveGroupCourse 
    ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
    WHERE OptionElectiveGroup.OptionID = @OptionID
    GROUP BY ElectiveGroupTitle, FootnoteNumber, OptionElectiveGroup.OptionFootnoteID, OptionElectiveGroup.OptionElectiveGroupID 
    ORDER BY ElectiveGroupTitle;
END

GO
