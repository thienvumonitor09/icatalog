USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseOfferingTerms]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/17/2019
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseOfferingTerms]
	@CourseOffering varchar(10)
AS
BEGIN
	SET NOCOUNT ON;

    
	SELECT DISTINCT CourseOffering, CATALOG_NBR, EFFDT 
	FROM udf_GetCourseOfferings()
	WHERE REPLACE(CourseOffering,' ','') = REPLACE(@CourseOffering,' ','')
	ORDER BY EFFDT DESC
END
GO
