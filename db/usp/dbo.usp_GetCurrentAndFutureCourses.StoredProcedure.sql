USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCurrentAndFutureCourses]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCurrentAndFutureCourses]
	@STRM varchar(4) = NULL,
	@SUBJECT varchar(5) = NULL,
	@Published char = NULL,
	@INSTITUTION varchar(5),
	@CATALOG_PRINT varchar(1) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT CourseOffering.SUBJECT, CATALOG_NBR,(SUBJECT + CATALOG_NBR) AS CourseOffering, DESCR, CourseOffering.EFFDT, CATALOG_PRINT
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	WHERE CourseOffering.EFFDT = 
		(
			SELECT MAX(CourseOffering2.EFFDT) 
			FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
			WHERE  CourseOffering2.CRSE_ID = CourseOffering.CRSE_ID /*CourseOffering2.SUBJECT = CourseOffering.SUBJECT
					AND CourseOffering2.CATALOG_NBR = CourseOffering.CATALOG_NBR*/
				AND INSTITUTION = @INSTITUTION
				AND
				(
					@STRM IS NULL
					OR
					(
						CourseOffering2.EFFDT <= (SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE STRM = @STRM ORDER BY TERM_END_DT DESC)
						OR
						@STRM = 'Z999'
					)
					
				)
		) 
		AND (@SUBJECT IS NULL OR SUBJECT = @SUBJECT)
		AND ACAD_CAREER = 'UGRD'
		AND (@CATALOG_PRINT IS NULL OR CATALOG_PRINT = 'Y')
		AND CourseCatalog.EFF_STATUS = 'A'
		AND CourseOffering.INSTITUTION = @INSTITUTION
	ORDER BY SUBJECT, CATALOG_NBR ASC
END
GO
