USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCredentials]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/15/17
-- Description:	Returns administrative and academic employee credentials
--		 INPUT: AlphaCharacter - Alpha character program titles start with (A-Z)
--				WorkUnit - Employee's place of work (SCC, SFCC, DIST)
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCredentials] 
	@AlphaCharacter char(1) = NULL,
	@WorkUnit varchar(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    SELECT CredentialID, Name, WorkUnit, Assignment, Description 
    FROM Credential 
    WHERE (@WorkUnit IS NULL OR WorkUnit LIKE '%' + @WorkUnit + '%') 
    AND (@AlphaCharacter IS NULL OR Name LIKE @AlphaCharacter + '%') 
    ORDER BY Name;
END

GO
