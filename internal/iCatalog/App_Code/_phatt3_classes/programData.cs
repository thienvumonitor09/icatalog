using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;

namespace ICatalog._phatt3_classes {

    /// <summary>
    /// Summary description for program.
    /// </summary>
    public class programData {

        private SqlConnection objCon;
        private String strSQL, strConnection;
        private SqlCommand objCmd;
        private SqlDataAdapter objDA;
        private DataSet objDS;
        private termData csTerm = new termData();

        public programData() {
            //development connection string
            //strConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCS-SQL-Dev;Workstation ID=InternalICatalog;Integrated Security=true;";
            strConnection = "Initial Catalog=CCSICatalog;Data Source=CCS-SQL-Dev;Workstation ID=InternalICatalog;Integrated Security=true;";

            //production connection string
            //strConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=InternalICatalog;Integrated Security=true;";
            //strConnection = "Initial Catalog=CCSICatalog;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=InternalICatalog;Integrated Security=true;";
        }


        public Int16 AddCollege(String institution, String collegeShortTitle, String collegeLongTitle, String collegeAddress, String collegeCity, String collegeState, String collegeZipCode1, String collegeZipCode2, String collegeWebsiteURL, String collegeRegistrationEmail, DateTime lastModifiedDate, String lastModifiedEmployeeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM College " +
                    "WHERE CollegeShortTitle = '" + collegeShortTitle.Replace("'", "''") + "';";

                objCmd = new SqlCommand(strSQL, objCon);
                Int32 intRecordCtr = (Int32)objCmd.ExecuteScalar();

                if (intRecordCtr > 0) {
                    return 2;
                } else {

                    strSQL = "INSERT INTO College(Institution, CollegeShortTitle, CollegeLongTitle, CollegeAddress, CollegeCity, CollegeState, CollegeZipCode1, CollegeZipCode2, CollegeWebsiteURL, CollegeRegistrationEmail, LastModifiedDate, LastModifiedEMPLID) " +
                        "VALUES('" + institution + "','" + collegeShortTitle.Replace("'", "''") + "','" + collegeLongTitle.Replace("'", "''") + "','" + collegeAddress.Replace("'", "''") + "','" + collegeCity.Replace("'", "''") + "','" + collegeState + "','" + collegeZipCode1 + "','" + collegeZipCode2 + "','" + collegeWebsiteURL.Replace("'", "''") + "','" + collegeRegistrationEmail.Replace("'", "''") + "','" + lastModifiedDate + "','" + lastModifiedEmployeeID + "');";

                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr == 0) {
                        return 0;
                    } else {
                        return 1;
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetCollegeList() {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT College.CollegeID, CollegeShortTitle, CollegeLongTitle, COUNT(ProgramVersionID) AS ProgCount, COUNT(CollegeCourse.CourseID) AS CrsCount " +
                         "FROM College " +
                         "LEFT OUTER JOIN Program " +
                         "ON Program.CollegeID = College.CollegeID " +
                         "LEFT OUTER JOIN CollegeCourse " +
                         "ON CollegeCourse.CollegeID = College.CollegeID " +
                         "Group By College.CollegeID, CollegeShortTitle, CollegeLongTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetCollege(Int32 collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT Institution, CollegeShortTitle, CollegeLongTitle, CollegeAddress, CollegeCity, CollegeState, CollegeZipCode1, CollegeZipCode2, CollegeWebsiteURL, CollegeRegistrationEmail " +
                    "FROM College " +
                    "WHERE CollegeID = " + collegeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetCollege(String collegeShortTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT Institution, CollegeLongTitle, CollegeAddress, CollegeCity, CollegeState, CollegeZipCode1, CollegeZipCode2, CollegeWebsiteURL, CollegeRegistrationEmail " +
                    "FROM College " +
                    "WHERE CollegeShortTitle = '" + collegeShortTitle + "';";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }

        public String GetCollegeShortTitle(Int32 collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT CollegeShortTitle " +
                    "FROM College " +
                    "WHERE CollegeID = " + collegeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                try {
                    return objCmd.ExecuteScalar().ToString();
                } catch {
                    return "";
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 EditCollege(Int32 collegeID, String institution, String collegeShortTitle, String collegeLongTitle, String collegeAddress, String collegeCity, String collegeState, String collegeZipCode1, String collegeZipCode2, String collegeWebsiteURL, String collegeRegistrationEmail, DateTime lastModifiedDate, String lastModifiedEmployeeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM College " +
                    "WHERE CollegeShortTitle = '" + collegeShortTitle.Replace("'", "''") +
                    "' AND CollegeID <> " + collegeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0) {
                    strSQL = "UPDATE College " +
                        "SET Institution = '" + institution + "', CollegeShortTitle = '" + collegeShortTitle.Replace("'", "''") + "', CollegeLongTitle = '" + collegeLongTitle.Replace("'", "''") + "', CollegeAddress = '" + collegeAddress.Replace("'", "''") + "', CollegeCity = '" + collegeCity.Replace("'", "''") + "', CollegeState = '" + collegeState + "', CollegeZipCode1 = '" + collegeZipCode1 + "', CollegeZipCode2 = '" + collegeZipCode2 + "', CollegeWebsiteURL = '" + collegeWebsiteURL.Replace("'", "''") + "', CollegeRegistrationEmail = '" + collegeRegistrationEmail.Replace("'", "''") + "', LastModifiedDate = '" + lastModifiedDate + "', LastModifiedEMPLID = '" + lastModifiedEmployeeID +
                        "' WHERE CollegeID = " + collegeID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr == 0) {
                        return 0;
                    } else {
                        return 1;
                    }
                } else {
                    return 2;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteCollege(Int32 collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE College " +
                    "WHERE CollegeID = " + collegeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                Int32 intCtr = 0;
                intCtr = objCmd.ExecuteNonQuery();

                if (intCtr > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 AddLocation(String collegeID, String locationTitle, String locationAddress, String locationCity, String locationState, String locationZipCode1, String locationZipCode2, String locationWebsiteURL, String locationRegistrationEmail, DateTime lastModifiedDate, String lastModifiedEmployeeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Location " +
                    "WHERE LocationTitle = '" + locationTitle.Replace("'", "''") + "';";

                objCmd = new SqlCommand(strSQL, objCon);
                Int32 intRecordCtr = (Int32)objCmd.ExecuteScalar();

                if (intRecordCtr > 0) {
                    return 2;
                } else {

                    if (collegeID == "") {
                        strSQL = "INSERT INTO Location(LocationTitle, LocationAddress, LocationCity, LocationState, LocationZipCode1, LocationZipCode2, LocationWebsiteURL, LocationRegistrationEmail, LastModifiedDate, LastModifiedEMPLID) " +
                            "VALUES('" + locationTitle.Replace("'", "''") + "','" + locationAddress.Replace("'", "''") + "','" + locationCity.Replace("'", "''") + "','" + locationState + "','" + locationZipCode1 + "','" + locationZipCode2 + "','" + locationWebsiteURL.Replace("'", "''") + "','" + locationRegistrationEmail.Replace("'", "''") + "','" + lastModifiedDate + "','" + lastModifiedEmployeeID + "');";
                    } else {
                        strSQL = "INSERT INTO Location(CollegeID, LocationTitle, LocationAddress, LocationCity, LocationState, LocationZipCode1, LocationZipCode2, LocationWebsiteURL, LocationRegistrationEmail, LastModifiedDate, LastModifiedEMPLID) " +
                            "VALUES(" + collegeID + ",'" + locationTitle.Replace("'", "''") + "','" + locationAddress.Replace("'", "''") + "','" + locationCity.Replace("'", "''") + "','" + locationState + "','" + locationZipCode1 + "','" + locationZipCode2 + "','" + locationWebsiteURL.Replace("'", "''") + "','" + locationRegistrationEmail.Replace("'", "''") + "','" + lastModifiedDate + "','" + lastModifiedEmployeeID + "');";
                    }
                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr == 0) {
                        return 0;
                    } else {
                        return 1;
                    }
                }

            } finally {
                objCon.Close();
            }

        }


        public DataSet GetLocationList() {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT CollegeID, Location.LocationID, LocationTitle, COUNT(OptionID) AS OptionCount " +
                    "FROM Location LEFT OUTER JOIN OptionLocation " +
                    "ON OptionLocation.LocationID = Location.LocationID " +
                    "Group By CollegeID, Location.LocationID, LocationTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetLocation(Int32 locationID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT CollegeID, LocationTitle, LocationAddress, LocationCity, LocationState, LocationZipCode1, LocationZipCode2, LocationWebsiteURL, LocationRegistrationEmail " +
                    "FROM Location " +
                    "WHERE LocationID = " + locationID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public Int32 GetLocationID(Int32 collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT LocationID " +
                    "FROM Location " +
                    "WHERE CollegeID = " + collegeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                try {
                    return Convert.ToInt32(objCmd.ExecuteScalar());
                } catch {
                    return 0;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 EditLocation(Int32 locationID, String locationTitle, String locationAddress, String locationCity, String locationState, String locationZipCode1, String locationZipCode2, String locationWebsiteURL, String locationRegistrationEmail, DateTime lastModifiedDate, String lastModifiedEmployeeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Location " +
                    "WHERE LocationTitle = '" + locationTitle.Replace("'", "''") +
                    "' AND LocationID <> " + locationID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0) {
                    strSQL = "UPDATE Location " +
                        "SET LocationTitle = '" + locationTitle.Replace("'", "''") + "', LocationAddress = '" + locationAddress.Replace("'", "''") + "', LocationCity = '" + locationCity.Replace("'", "''") + "', LocationState = '" + locationState + "', LocationZipCode1 = '" + locationZipCode1 + "', LocationZipCode2 = '" + locationZipCode2 + "', LocationWebsiteURL = '" + locationWebsiteURL.Replace("'", "''") + "', LocationRegistrationEmail = '" + locationRegistrationEmail.Replace("'", "''") + "', LastModifiedDate = '" + lastModifiedDate + "', LastModifiedEMPLID = '" + lastModifiedEmployeeID +
                        "' WHERE LocationID = " + locationID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr == 0) {
                        return 0;
                    } else {
                        return 1;
                    }
                } else {
                    return 2;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteLocation(Int32 locationID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE Location " +
                    "WHERE LocationID = " + locationID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                Int32 intCtr = 0;
                intCtr = objCmd.ExecuteNonQuery();

                if (intCtr > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetOptionLocations(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionLocation.LocationID, LocationTitle " +
                    "FROM OptionLocation " +
                    "INNER JOIN Location " +
                    "ON Location.LocationID = OptionLocation.LocationID " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public bool AddOptionLocation(Int32 optionID, Int32 locationID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "INSERT INTO OptionLocation(OptionID, LocationID) " +
                    "VALUES(" + optionID + "," + locationID + ");";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteOptionLocations(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE OptionLocation " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteOptionLocations(Int32 optionID, SqlConnection objCon) {

            try {

                strSQL = "DELETE OptionLocation " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch {
                return false;
            }
        }


        public Int16 AddDegree(String degreeShortTitle, String degreeLongTitle, String degreeType, bool blnIncludeDegWkst, String documentTitle, String degreeDescription, DateTime lastModifiedDate, String lastModifiedEmployeeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Degree " +
                    "WHERE DegreeShortTitle = '" + degreeShortTitle.Replace("'", "''") +
                    "' AND DegreeLongTitle = '" + degreeLongTitle.Replace("'", "''") + "';";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0) {

                    if (blnIncludeDegWkst) {
                        strSQL = "INSERT INTO Degree(DegreeShortTitle, DegreeLongTitle, DegreeType, DegreeDescription, DocumentTitle, LastModifiedDate, LastModifiedEMPLID) " +
                            "VALUES('" + degreeShortTitle.Replace("'", "''") + "','" + degreeLongTitle.Replace("'", "''") + "','" + degreeType + "','" + degreeDescription.Replace("'", "''") + "','" + documentTitle.Replace("'", "''") + "','" + lastModifiedDate + "','" + lastModifiedEmployeeID + "');";
                    } else {
                        strSQL = "INSERT INTO Degree(DegreeShortTitle, DegreeLongTitle, DegreeType, DegreeDescription, LastModifiedDate, LastModifiedEMPLID) " +
                            "VALUES('" + degreeShortTitle.Replace("'", "''") + "','" + degreeLongTitle.Replace("'", "''") + "','" + degreeType + "','" + degreeDescription.Replace("'", "''") + "','" + lastModifiedDate + "','" + lastModifiedEmployeeID + "');";
                    }
                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr == 0) {
                        return 0;
                    } else {
                        return 1;
                    }

                } else {
                    return 2;
                }

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetDegreeList() {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, DegreeType, DegreeDescription, DocumentTitle, Count(ProgramDegreeID) AS ProgCount " +
                    "FROM Degree LEFT OUTER JOIN ProgramDegree " +
                    "ON ProgramDegree.DegreeID = Degree.DegreeID " +
                    "GROUP BY Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, DegreeType, DegreeDescription, DocumentTitle " +
                    "ORDER BY DegreeShortTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public String GetDegreeRequirementWorksheetFileNameBySTRM(String documentTitle, String beginSTRM, String endSTRM) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                if (beginSTRM == "" || endSTRM == "") {
                    //get the current strm data
                    DataSet dsTerm = csTerm.GetTermDetails(csTerm.GetCurrentTerm());
                    if (dsTerm.Tables[0].Rows.Count > 0) {
                        if (dsTerm.Tables[0].Rows[0]["DESCR"].ToString().ToLower().IndexOf("fall") > -1) {
                            strSQL = "SELECT FileName " +
                                "FROM DegreeRequirementWorksheet " +
                                "WHERE DocumentTitle = '" + documentTitle +
                                "' AND EffectiveYear = '" + Convert.ToDateTime(dsTerm.Tables[0].Rows[0]["TERM_BEGIN_DT"]).Year + "';";
                            objCmd = new SqlCommand(strSQL, objCon);
                            return objCmd.ExecuteScalar().ToString();
                        } else {
                            strSQL = "SELECT FileName " +
                                "FROM DegreeRequirementWorksheet " +
                                "WHERE DocumentTitle = '" + documentTitle +
                                "' AND EffectiveYear = '" + Convert.ToDateTime(dsTerm.Tables[0].Rows[0]["TERM_BEGIN_DT"]).AddYears(-1).Year + "';";
                            objCmd = new SqlCommand(strSQL, objCon);
                            return objCmd.ExecuteScalar().ToString();
                        }
                    } else {
                        return "";
                    }
                } else {
                    DataSet dsEndTerm = new DataSet();
                    DataSet dsBeginTerm = csTerm.GetTermDetails(beginSTRM);
                    if (endSTRM != "Z999") {
                        dsEndTerm = csTerm.GetTermDetails(endSTRM);
                    }
                    //get future strm data
                    if (endSTRM != "Z999" && Convert.ToDateTime(dsEndTerm.Tables[0].Rows[0]["TERM_END_DT"]).Date < DateTime.Today) {
                        if (dsEndTerm.Tables[0].Rows[0]["DESCR"].ToString().ToLower().IndexOf("fall") > -1) {
                            strSQL = "SELECT FileName " +
                                "FROM DegreeRequirementWorksheet " +
                                "WHERE DocumentTitle = '" + documentTitle +
                                "' AND EffectiveYear = '" + Convert.ToDateTime(dsEndTerm.Tables[0].Rows[0]["TERM_END_DT"]).Year + "';";
                            objCmd = new SqlCommand(strSQL, objCon);
                            return objCmd.ExecuteScalar().ToString();
                        } else {
                            strSQL = "SELECT FileName " +
                                "FROM DegreeRequirementWorksheet " +
                                "WHERE DocumentTitle = '" + documentTitle +
                                "' AND EffectiveYear = '" + Convert.ToDateTime(dsEndTerm.Tables[0].Rows[0]["TERM_END_DT"]).AddYears(-1).Year + "';";
                            objCmd = new SqlCommand(strSQL, objCon);
                            return objCmd.ExecuteScalar().ToString();
                        }
                        //get past strm data
                    } else if (Convert.ToDateTime(dsBeginTerm.Tables[0].Rows[0]["TERM_BEGIN_DT"]).Date > DateTime.Today) {
                        if (dsBeginTerm.Tables[0].Rows[0]["DESCR"].ToString().ToLower().IndexOf("fall") > -1) {
                            strSQL = "SELECT FileName " +
                                "FROM DegreeRequirementWorksheet " +
                                "WHERE DocumentTitle = '" + documentTitle +
                                "' AND EffectiveYear = '" + Convert.ToDateTime(dsBeginTerm.Tables[0].Rows[0]["TERM_BEGIN_DT"]).Year + "';";
                            objCmd = new SqlCommand(strSQL, objCon);
                            return objCmd.ExecuteScalar().ToString();
                        } else {
                            strSQL = "SELECT FileName " +
                                "FROM DegreeRequirementWorksheet " +
                                "WHERE DocumentTitle = '" + documentTitle +
                                "' AND EffectiveYear = '" + Convert.ToDateTime(dsBeginTerm.Tables[0].Rows[0]["TERM_BEGIN_DT"]).AddYears(-1).Year + "';";
                            objCmd = new SqlCommand(strSQL, objCon);
                            return objCmd.ExecuteScalar().ToString();
                        }
                        //get current strm data
                    } else {
                        DataSet dsTerm = csTerm.GetTermDetails(csTerm.GetCurrentTerm());
                        if (dsTerm.Tables[0].Rows[0]["DESCR"].ToString().ToLower().IndexOf("fall") > -1) {
                            strSQL = "SELECT FileName " +
                                "FROM DegreeRequirementWorksheet " +
                                "WHERE DocumentTitle = '" + documentTitle +
                                "' AND EffectiveYear = '" + Convert.ToDateTime(dsTerm.Tables[0].Rows[0]["TERM_BEGIN_DT"]).Year + "';";
                            objCmd = new SqlCommand(strSQL, objCon);
                            return objCmd.ExecuteScalar().ToString();
                        } else {
                            strSQL = "SELECT FileName " +
                                "FROM DegreeRequirementWorksheet " +
                                "WHERE DocumentTitle = '" + documentTitle +
                                "' AND EffectiveYear = '" + Convert.ToDateTime(dsTerm.Tables[0].Rows[0]["TERM_BEGIN_DT"]).AddYears(-1).Year + "';";
                            objCmd = new SqlCommand(strSQL, objCon);
                            return objCmd.ExecuteScalar().ToString();
                        }
                    }
                }

            } catch {
                return "";
            } finally {
                objCon.Close();
            }
        }


        public DataSet GetDegree(Int32 degreeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT DegreeShortTitle, DegreeLongTitle, DegreeType, DocumentTitle, DegreeDescription, LastModifiedEMPLID, LastModifiedDate " +
                    "FROM Degree " +
                    "WHERE DegreeID = " + degreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public String GetDegreeRequirementWorksheetByProgramDegreeID(Int32 programDegreeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT DocumentTitle, ProgramBeginSTRM, ProgramEndSTRM " +
                    "FROM ProgramDegree " +
                    "INNER JOIN Program " +
                    "ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID " +
                    "INNER JOIN Degree " +
                    "ON Degree.DegreeID = ProgramDegree.DegreeID " +
                    "WHERE ProgramDegreeID = " + programDegreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);
                objCon.Close();

                if (objDS.Tables[0].Rows.Count > 0) {
                    return GetDegreeRequirementWorksheetFileNameBySTRM(objDS.Tables[0].Rows[0]["DocumentTitle"].ToString(), objDS.Tables[0].Rows[0]["ProgramBeginSTRM"].ToString(), objDS.Tables[0].Rows[0]["ProgramEndSTRM"].ToString());
                } else {
                    return "";
                }

            } catch {
                return "";
            } finally {
                objCon.Close();
            }
        }


        public bool DeleteDegree(Int32 degreeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE Degree " +
                    "WHERE DegreeID = " + degreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                Int32 intCtr = 0;
                intCtr = objCmd.ExecuteNonQuery();

                if (intCtr == 0) {
                    return false;
                } else {
                    return true;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 EditDegree(Int32 degreeID, String degreeShortTitle, String degreeLongTitle, String degreeType, bool blnIncludeDegWkst, String documentTitle, String degreeDescription, DateTime lastModifiedDate, String lastModifiedEmployeeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Degree " +
                    "WHERE DegreeShortTitle = '" + degreeShortTitle.Replace("'", "''") +
                    "' AND DegreeID <> " + degreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0) {
                    if (blnIncludeDegWkst) {
                        strSQL = "UPDATE Degree " +
                            "SET DegreeShortTitle = '" + degreeShortTitle.Replace("'", "''") + "', DegreeLongTitle = '" + degreeLongTitle.Replace("'", "''") + "', DegreeType = '" + degreeType + "', DocumentTitle = '" + documentTitle.Replace("'", "''") + "', DegreeDescription = '" + degreeDescription.Replace("'", "''") + "', LastModifiedDate = '" + lastModifiedDate + "', LastModifiedEMPLID = '" + lastModifiedEmployeeID +
                            "' WHERE DegreeID = " + degreeID + ";";
                    } else {
                        strSQL = "UPDATE Degree " +
                            "SET DegreeShortTitle = '" + degreeShortTitle.Replace("'", "''") + "', DegreeLongTitle = '" + degreeLongTitle.Replace("'", "''") + "', DegreeType = '" + degreeType + "', DocumentTitle = NULL, DegreeDescription = '" + degreeDescription.Replace("'", "''") + "', LastModifiedDate = '" + lastModifiedDate + "', LastModifiedEMPLID = '" + lastModifiedEmployeeID +
                            "' WHERE DegreeID = " + degreeID + ";";
                    }

                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr == 0) {
                        return 0;
                    } else {
                        return 1;
                    }
                } else {
                    return 2;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 EditDegree(Int32 degreeID, String degreeShortTitle, String degreeLongTitle, String degreeDescription, DateTime lastModifiedDate, String lastModifiedEmployeeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Degree " +
                    "WHERE DegreeShortTitle = '" + degreeShortTitle.Replace("'", "''") +
                    "' AND DegreeID <> " + degreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0) {
                    strSQL = "UPDATE Degree " +
                        "SET DegreeShortTitle = '" + degreeShortTitle.Replace("'", "''") + "', DegreeLongTitle = '" + degreeLongTitle.Replace("'", "''") + "', DegreeDescription = '" + degreeDescription.Replace("'", "''") + "', LastModifiedDate = '" + lastModifiedDate + "', LastModifiedEMPLID = '" + lastModifiedEmployeeID +
                        "' WHERE DegreeID = " + degreeID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr == 0) {
                        return 0;
                    } else {
                        return 1;
                    }
                } else {
                    return 2;
                }

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetProgramTitles() {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT ProgramTitle, ProgramVersionID " +
                    "FROM Program " +
                    "ORDER BY ProgramTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetProgramTitles(String publishedProgram, String selectedSTRM, String collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                String strQuery = "";
                if (collegeID != "") {
                    strQuery += " AND Program.CollegeID = " + collegeID + " ";
                }

                strSQL = "SELECT ProgramTitle, ProgramBeginSTRM, ProgramEndSTRM, ProgramVersionID, CollegeShortTitle, Program.CollegeID, ProgramDisplay, PublishedProgram " +
                    "FROM Program INNER JOIN College " +
                    "ON College.CollegeID = Program.CollegeID " +
                    "WHERE PublishedProgram = " + publishedProgram +
                    " AND ProgramBeginSTRM <= '" + selectedSTRM +
                    "' AND (ProgramEndSTRM = 'Z999' " +
                    "OR ProgramEndSTRM >= '" + selectedSTRM + "') " +
                    strQuery +
                    "ORDER BY ProgramTitle, CollegeShortTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetAreaOfStudyProgramTitles(String publishedProgram, String selectedSTRM, String collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                String strQuery = "";
                if (collegeID != "") {
                    strQuery += " AND Program.CollegeID = " + collegeID + " ";
                }

                strSQL = "SELECT DISTINCT AreaofStudy.Title, ProgramTitle, ProgramBeginSTRM, ProgramEndSTRM, ProgramVersionID, CollegeShortTitle, Program.CollegeID, ProgramDisplay, PublishedProgram " +
                    "FROM AreaOfStudy " +
                    "INNER JOIN AreaOfStudyProgram " +
                    "ON AreaOfStudyProgram.AreaOfStudyID = AreaOfStudy.AreaOfStudyID " +
                    "INNER JOIN Program " +
                    "ON Program.ProgramID = AreaOfStudyProgram.ProgramID " +
                    "AND AreaOfStudyProgram.PrimaryAreaOfStudy = 1 " +
                    "INNER JOIN College " +
                    "ON College.CollegeID = Program.CollegeID " +
                    "WHERE PublishedProgram = " + publishedProgram +
                    " AND ProgramBeginSTRM <= '" + selectedSTRM +
                    "' AND (ProgramEndSTRM = 'Z999' " +
                    "OR ProgramEndSTRM >= '" + selectedSTRM + "') " +
                    strQuery +
                    "ORDER BY Title, ProgramTitle, CollegeShortTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetProgramColleges(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT CollegeLongTitle, CollegeShortTitle " +
                    "FROM College INNER JOIN Program " +
                    "ON Program.CollegeID = College.CollegeID " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetCollegeFees(Int32 collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT FeeTitle, CollegeFee " +
                    "FROM CollegeFee INNER JOIN FeeTitle " +
                    "ON FeeTitle.FeeASN = CollegeFee.FeeASN " +
                    "WHERE CollegeID = " + collegeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetCollegeCreditFees(Int32 collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT FeeTitle, CollegeFee, CollegeFeeCr, CollegeFeeAddCrMin, CollegeFeeAddCrMax, CollegeFeeAddPerCr " +
                    "FROM CollegeFeeCredit INNER JOIN FeeTitle " +
                    "ON FeeTitle.FeeASN = CollegeFeeCredit.FeeASN " +
                    "WHERE CollegeID = " + collegeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetProgramTitlesInAlphaOrder(Char chrAlpha) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT ProgramTitle, ProgramVersionID, COUNT(ProgramVersionID) AS ProgCount, COUNT(ProgramFeeID) AS ProgramFeeCount " +
                    "FROM Program " +
                    "LEFT OUTER JOIN ProgramFee " +
                    "ON ProgramFee.ProgramVersionID = Program.ProgramVersionID " +
                    "WHERE ProgramTitle LIKE '" + chrAlpha + "%' " +
                    "GROUP BY ProgramTitle, Program.ProgramVersionID " +
                    "ORDER BY ProgramTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        //CHECK IF THIS IS BEING USED
        public DataSet GetProgramsInAlphaOrder(Char chrAlpha) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT ProgramTitle, ProgramVersionID, CollegeShortTitle, ProgramBeginSTRM, ProgramEndSTRM, PublishedProgram, PublishedProgramVersionID, College.CollegeID " +
                    "FROM Program LEFT OUTER JOIN College " +
                    "ON College.CollegeID = Program.CollegeID " +
                    "WHERE ProgramTitle LIKE '" + chrAlpha + "%' " +
                    "ORDER BY ProgramTitle, CollegeShortTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        /// <summary>
        /// Returns a list of current and future programs that have a title starting with the alpha character passed in
        /// </summary>
        /// <param name="chrAlpha">Char: Alpha character program titles start with (A-Z)</param>
        /// <param name="STRM">String: Term code (Example: 2163)</param>
        /// <param name="collegeID">String: College primary key</param>
        /// <returns>
        /// DataSet
        /// Field List: DegreeShortTitle - DegreeShortTitle
        ///             ProgramTitle - Program title
        ///             ProgramVersionID - Program primary key
        ///             CollegeShortTitle - College abbreviation (SCC, SFCC)
        ///             ProgramBeginSTRM - Program begin term code (STRM)
        ///             BeginTerm_DESCR - Program begin term description (Example: Fall 2017)
        ///             ProgramEndSTRM - Program end term code (STRM)
        ///             EndTerm_DESCR - Program end term description (Example: Fall 2017)
        ///             ProgramDisplay - Identifies whether the program will have a program outline or large text description: 1 = program outline, 2 =  large text description
        ///             PublishedProgram - 0 = Working Copy / not displayed to the public, 1 = Published Copy / displayed to the public
        ///             PublishedProgramVersionID - Stores the primary key of the Published Program Copy when a Working Copy of that program is created for editing purposes
        ///             CollegeID - College primary key
        /// </returns>
        public DataSet GetCurrentAndFuturePrograms(String alpha, String STRM, String collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCurrentAndFuturePrograms", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@AlphaCharacter", SqlDbType.Char);
                objCmd.Parameters["@AlphaCharacter"].Value = alpha;

                if (STRM != null && STRM != "") {
                    objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                    objCmd.Parameters["@STRM"].Value = STRM;
                }

                if (collegeID != null && collegeID != "") {
                    objCmd.Parameters.Add("@CollegeID", SqlDbType.VarChar);
                    objCmd.Parameters["@CollegeID"].Value = collegeID;
                }

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }


        /// <summary>
        /// Returns a list of published programs that have a title starting with the alpha character passed in
        /// </summary>
        /// <param name="chrAlpha">Char: Alpha character program titles start with (A-Z)</param>
        /// <param name="STRM">String: Term code (Example: 2163)</param>
        /// <param name="collegeID">String: College primary key</param>
        /// <returns>
        /// DataSet
        /// Field List: DegreeShortTitle - Degree Short Title
        ///             ProgramTitle - Program title
        ///             ProgramVersionID - Program primary key
        ///             CollegeShortTitle - College abbreviation (SCC, SFCC)
        ///             ProgramBeginSTRM - Program begin term code (STRM)
        ///             BeginTerm_DESCR - Program begin term description (Example: Fall 2017)
        ///             ProgramEndSTRM - Program end term code (STRM)
        ///             EndTerm_DESCR - Program end term description (Example: Fall 2017)
        ///             ProgramDisplay - Identifies whether the program will have a program outline or large text description: 1 = program outline, 2 =  large text description
        ///             CollegeID - College primary key
        /// </returns>
        public DataSet GetProgramsForDisplay(Char chrAlpha, String STRM, String collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetActivePrograms", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@AlphaCharacter", SqlDbType.Char);
                objCmd.Parameters["@AlphaCharacter"].Value = chrAlpha;

                if (STRM != null && STRM != "") {
                    objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                    objCmd.Parameters["@STRM"].Value = STRM;
                }

                if (collegeID != null && collegeID != "") {
                    objCmd.Parameters.Add("@CollegeID", SqlDbType.VarChar);
                    objCmd.Parameters["@CollegeID"].Value = collegeID;
                }

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }


        /// <summary>
        /// Returns a list of archived programs that have a title starting with the alpha character passed in
        /// </summary>
        /// <param name="chrAlpha">Char: Alpha character program titles start with (A-Z)</param>
        /// <param name="STRM">String: Term code (Example: 2163)</param>
        /// <param name="collegeID">String: College primary key</param>
        /// <returns>
        /// DataSet
        /// Field List: DegreeShortTitle - Degree Short Title
        ///             ProgramTitle - Program title
        ///             ProgramVersionID - Program primary key
        ///             CollegeShortTitle - College abbreviation (SCC, SFCC)
        ///             ProgramBeginSTRM - Program begin term code (STRM)
        ///             BeginTerm_DESCR - Program begin term description (Example: Fall 2017)
        ///             ProgramEndSTRM - Program end term code (STRM)
        ///             EndTerm_DESCR - Program end term description (Example: Fall 2017)
        ///             ProgramDisplay - Identifies whether the program will have a program outline or large text description: 1 = program outline, 2 =  large text description
        ///             CollegeID - College primary key
        /// </returns>
        public DataSet GetProgramArchives(Char chrAlpha, String STRM, String collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetProgramArchives", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@AlphaCharacter", SqlDbType.Char);
                objCmd.Parameters["@AlphaCharacter"].Value = chrAlpha;

                if (STRM != null && STRM != "") {
                    objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                    objCmd.Parameters["@STRM"].Value = STRM;
                }

                if (collegeID != null && collegeID != "") {
                    objCmd.Parameters.Add("@CollegeID", SqlDbType.VarChar);
                    objCmd.Parameters["@CollegeID"].Value = collegeID;
                }

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }


        /// <summary>
        /// Returns a list of published programs for career planning guides
        /// </summary>
        /// <param name="chrAlpha">Alpha character program titles start with (A-Z)</param>
        /// <param name="STRM">Term code</param>
        /// <param name="collegeID">College primary key</param>
        /// <returns>
        /// DataSet
        /// Field List: DegreeShortTitle - Degree short title
        ///             ProgramTitle - Program title
        ///             ProgramVersionID - Program primary key
        ///             CareerPlanningGuideID - Career planning guide primary key
        ///             CollegeShortTitle - College abbreviation (SCC, SFCC)
        ///             ProgramBeginSTRM - Program begin term code (STRM)
        ///             BeginTerm_DESCR - Program begin term description (Example: Fall 2017)
        ///             ProgramEndSTRM - Program end term code (STRM)
        ///             EndTerm_DESCR - Program end term description (Example: Fall 2017)
        ///             PublishedCareerPlanningGuide - Career planning guide status: 1 = Published Copy, 0 = Working Copy
        ///             PublishedCareerPlanningGuideID - Used to store the primary key of the Published CPG Copy when a Working Copy is created for editing purposes
        ///             CollegeID - College primary key            
        /// </returns>
        public DataSet GetCareerPlanningGuidePrograms(Char chrAlpha, String STRM, String collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCareerPlanningGuidePrograms", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@AlphaCharacter", SqlDbType.Char);
                objCmd.Parameters["@AlphaCharacter"].Value = chrAlpha;

                if (STRM != null && STRM != "") {
                    objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                    objCmd.Parameters["@STRM"].Value = STRM;
                }

                if (collegeID != null && collegeID != "") {
                    objCmd.Parameters.Add("@CollegeID", SqlDbType.VarChar);
                    objCmd.Parameters["@CollegeID"].Value = collegeID;
                }

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }


        //CHECK IF THIS IS BEING USED
        /* bv commented out - 10/9/17
		public DataSet GetProgramsInAlphaOrder(Char chrAlpha){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT ProgramTitle, ProgramVersionID, CollegeShortTitle " +
					"FROM Program INNER JOIN College " +
					"ON College.CollegeID = Program.CollegeID " +
					"WHERE ProgramTitle LIKE '" + chrAlpha + "%' " +
					"ORDER BY ProgramTitle, CollegeShortTitle;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}
         * */


        /// <summary>
        /// Returns program description details for a specific program
        /// </summary>
        /// <param name="programVersionID">Progam Primary Key</param>
        /// <returns>
        /// DataSet
        /// Field List: ProgramID - Non changing program identifier (The ID is the same for all versions of a program)
        ///             ProgramDescription - Program description
        ///             ProgramTitle - Program title
        ///             OptionStateApproval - Text field with state approval dates and descriptions
        ///             AreaOfStudyID - Area of Study ID
        ///             Title - Area of Study Title
        ///             CategoryID - Program category primary key
        ///             CategoryTitle - Program category title
        ///             CollegeShortTitle - College abbreviation (SCC, SFCC)
        ///             CollegeID - College primary key
        ///             PublishedProgramVersionID - Stores the primary key of the Published Program Copy when a Working Copy of that program is created for editing purposes
        ///             ProgramBeginSTRM - Program begin term code (STRM)
        ///             BeginTerm_DESCR - Program begin term description (Example: Fall 2017)
        ///             ProgramEndSTRM - Program end term code (STRM)
        ///             EndTerm_DESCR - Program end term description (Example: Fall 2017)
        ///             PublishedProgram - 0 = Working Copy / not displayed to the public, 1 = Published Copy / displayed to the public
        ///             LastModifiedEMPLID - EmplID of employee that last modified the program record
        ///             LastModifiedDate - Date/Time stamp when the record was last modified
        /// </returns>
        public DataSet GetProgramDescription(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetProgramDescription", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@ProgramVersionID", SqlDbType.Int);
                objCmd.Parameters["@ProgramVersionID"].Value = programVersionID;

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public String GetProgramDescriptionAsString(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT ProgramDescription " +
                    "FROM Program " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                try {
                    return objCmd.ExecuteScalar().ToString();
                } catch {
                    return "";
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 AddProgramDescription(String programTitle, String programDescription) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "INSERT INTO Program(ProgramTitle, ProgramDescription) " +
                    "VALUES('" + programTitle.Replace("'", "''") + "','" + programDescription.Replace("'", "''") + "');";

                objCmd = new SqlCommand(strSQL, objCon);

                Int32 intCtr = 0;
                intCtr = objCmd.ExecuteNonQuery();

                if (intCtr == 0) {
                    return 0;
                } else {
                    return 1;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 SaveSelectedValues(Int32 programVersionID, Int32 programID, Int32 degreeID, String programTitle, String strAreaOfStudyID, String strCategoryID, String programBeginSTRM, String programEndSTRM, Int32 collegeID, String programDescription, String publishedProgramVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                //check if another program exists with the same title, degree, and a conflict in effective year quarters
                strSQL = "SELECT COUNT(*) " +
                    "FROM Program " +
                    "INNER JOIN ProgramDegree " +
                    "ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID " +
                    "WHERE CollegeID = " + collegeID +
                    " AND ProgramTitle = '" + programTitle +
                    "' AND ((ProgramBeginSTRM >= '" + programBeginSTRM +
                    "' AND ProgramBeginSTRM <= '" + programEndSTRM +
                    "') OR (ProgramEndSTRM <= '" + programEndSTRM +
                    "' AND ProgramEndSTRM >= '" + programBeginSTRM +
                    "') OR (ProgramBeginSTRM >= '" + programBeginSTRM +
                    "' AND ProgramEndSTRM <= '" + programEndSTRM +
                    "')) AND Program.ProgramVersionID <> " + programVersionID +
                    " AND DegreeID = " + degreeID;

                if (publishedProgramVersionID != "") {
                    strSQL += " AND Program.ProgramVersionID <> " + publishedProgramVersionID + ";";
                }

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return -1;
                } else {

                    EditPrimaryAreaOfStudy(programID, strAreaOfStudyID, objCon);

                    EditPrimaryCategory(programVersionID, strCategoryID, objCon);

                    strSQL = "UPDATE Program " +
                            "SET ProgramTitle = '" + programTitle.Replace("'", "''") + "', ProgramBeginSTRM = '" + programBeginSTRM + "', ProgramEndSTRM = '" + programEndSTRM + "', CollegeID = " + collegeID + ", ProgramDescription = '" + programDescription.Replace("'", "''") +
                            "' WHERE ProgramVersionID = " + programVersionID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);
                    if (objCmd.ExecuteNonQuery() > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public Int32 SaveProgRevision(Int32 programVersionID, Int32 programID, Int32 degreeID, String strAreaOfStudyID, String strCategoryID, Int32 collegeID, String programTitle, String programBeginSTRM, String programEndSTRM, String publishedProgram, String programDescription, Char programDisplay, String lastModifiedEmployeeID, DateTime lastModifiedDate) {

            //reference code from EditProgDefinition for copying data from one program to another
            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Program " +
                    "INNER JOIN ProgramDegree " +
                    "ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID " +
                    "WHERE CollegeID = " + collegeID +
                    " AND ProgramTitle = '" + programTitle.Replace("'", "''") +
                    "' AND ((ProgramBeginSTRM >= '" + programBeginSTRM +
                    "' AND ProgramBeginSTRM <= '" + programEndSTRM +
                    "') OR (ProgramEndSTRM <= '" + programEndSTRM +
                    "' AND ProgramEndSTRM >= '" + programBeginSTRM +
                    "') OR (ProgramBeginSTRM >= '" + programBeginSTRM +
                    "' AND ProgramEndSTRM <= '" + programEndSTRM +
                    "')) AND Program.ProgramVersionID <> " + programVersionID +
                    " AND DegreeID = " + degreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return -1;
                } else {

                    //insert new version of program
                    Int32 newProgramVersionID = AddProgram(programVersionID, programID, programTitle, strAreaOfStudyID, strCategoryID, programDescription, collegeID, degreeID, programBeginSTRM, programEndSTRM, publishedProgram, programDisplay, lastModifiedEmployeeID, lastModifiedDate);

                    //get ProgramDegree data of previous program
                    DataSet dsProgramDegree = GetProgramDegree(programVersionID);
                    for (Int32 i = 0; i < dsProgramDegree.Tables[0].Rows.Count; i++) {

                        //store previous program ProgramDegreeID
                        Int32 programDegreeID = Convert.ToInt32(dsProgramDegree.Tables[0].Rows[i]["ProgramDegreeID"]);

                        //Insert ProgramDegree data for new program - store new program ProgramDegreeID
                        Int32 intNewProgramDegreeID = AddProgramDegree(newProgramVersionID, Convert.ToInt32(dsProgramDegree.Tables[0].Rows[i]["DegreeID"]));

                        //get ProgOptions data of previous program
                        DataSet dsProgOptions = GetProgOptions(programDegreeID);
                        for (Int32 j = 0; j < dsProgOptions.Tables[0].Rows.Count; j++) {
                            Int32 optionID = Convert.ToInt32(dsProgOptions.Tables[0].Rows[j]["OptionID"]);

                            //insert ProgOption data for new program
                            Int32 intNewOptionID = AddProgOption(intNewProgramDegreeID, dsProgOptions.Tables[0].Rows[j]["OptionTitle"].ToString());

                            //update the program option description
                            String optionDescription = dsProgOptions.Tables[0].Rows[j]["OptionDescription"].ToString();
                            String gainfulEmploymentID = dsProgOptions.Tables[0].Rows[j]["GainfulEmploymentID"].ToString();
                            String optionStateApproval = dsProgOptions.Tables[0].Rows[j]["OptionStateApproval"].ToString();
                            String optionCIP = dsProgOptions.Tables[0].Rows[j]["CIP"].ToString();
                            String optionEPC = dsProgOptions.Tables[0].Rows[j]["EPC"].ToString();
                            String academicPlan = dsProgOptions.Tables[0].Rows[j]["ACAD_PLAN"].ToString();
                            String totalQuarters = dsProgOptions.Tables[0].Rows[j]["TotalQuarters"].ToString();
                            String strPrimaryOption = dsProgOptions.Tables[0].Rows[j]["PrimaryOption"].ToString();
                            Byte bitPrimaryOption = 0;
                            if (strPrimaryOption == "True") {
                                bitPrimaryOption = 1;
                            }

                            EditOptionValues(intNewOptionID, newProgramVersionID, optionDescription, optionStateApproval, optionCIP, optionEPC, academicPlan, bitPrimaryOption, gainfulEmploymentID, totalQuarters);

                            //get program option locations from previous version
                            DataSet dsLocations = GetOptionLocations(optionID);
                            for (Int32 k = 0; k < dsLocations.Tables[0].Rows.Count; k++) {
                                //add option locations to new version
                                AddOptionLocation(intNewOptionID, Convert.ToInt32(dsLocations.Tables[0].Rows[k]["LocationID"]));
                            }

                            //get program option mcodes from previous version
                            DataSet dsMCodes = GetOptionMCodes(optionID);
                            for (Int32 k = 0; k < dsMCodes.Tables[0].Rows.Count; k++) {
                                //add option mcodes to new version
                                AddOptionCollegeMCode(intNewOptionID, Convert.ToInt16(dsMCodes.Tables[0].Rows[k]["CollegeID"]), dsMCodes.Tables[0].Rows[k]["MCode"].ToString());
                            }

                            //get program footnotes from previous version
                            DataSet dsFootnotes = GetOptFootnotes(optionID);
                            for (Int32 k = 0; k < dsFootnotes.Tables[0].Rows.Count; k++) {

                                //insert footnotes for new program version
                                AddOptFootnote(intNewOptionID, Convert.ToInt16(dsFootnotes.Tables[0].Rows[k]["FootnoteNumber"]), dsFootnotes.Tables[0].Rows[k]["Footnote"].ToString());
                            }

                            //get OptPrereq data of previous program
                            DataSet dsOptPrereqs = GetOptPrereqs(optionID, programBeginSTRM, programEndSTRM);
                            String oldCourseOffering = "";
                            for (Int32 k = 0; k < dsOptPrereqs.Tables[0].Rows.Count; k++) {
                                Int32 optionFootnoteID;
                                String courseOffering = dsOptPrereqs.Tables[0].Rows[k]["CourseOffering"].ToString();

                                if (oldCourseOffering != courseOffering) {
                                    try {
                                        //check if footnote exists
                                        optionFootnoteID = Convert.ToInt32(dsOptPrereqs.Tables[0].Rows[k]["OptionFootnoteID"]);
                                        //get new OptionFootnoteID for the new program
                                        optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dsOptPrereqs.Tables[0].Rows[k]["FootnoteNumber"]));
                                    } catch {
                                        optionFootnoteID = 0;
                                    }

                                    //insert DegPrereq data for new program
                                    AddOptPrereqs(intNewOptionID, dsOptPrereqs.Tables[0].Rows[k]["CourseOffering"].ToString(), optionFootnoteID);
                                }
                                oldCourseOffering = courseOffering;
                            }

                            //get program elective groups from previous version
                            DataSet dsOptionElectiveGroups = GetOptionElectiveGroups(optionID);
                            for (Int32 k = 0; k < dsOptionElectiveGroups.Tables[0].Rows.Count; k++) {
                                //store previous program OptionElectiveGroupID
                                Int32 optionElectiveGroupID = Convert.ToInt32(dsOptionElectiveGroups.Tables[0].Rows[k]["OptionElectiveGroupID"]);
                                Int32 optionFootnoteID;

                                try {
                                    //check if footnote exists
                                    optionFootnoteID = Convert.ToInt32(dsOptionElectiveGroups.Tables[0].Rows[k]["OptionFootnoteID"]);
                                    //get new OptionFootnoteID for new program version
                                    optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dsOptionElectiveGroups.Tables[0].Rows[k]["FootnoteNumber"]));
                                } catch {
                                    optionFootnoteID = 0;
                                }

                                //insert elective groups for new program version and store new program OptionElectiveGroupID
                                Int32 newOptionElectiveGroupID = AddOptionElectiveGroup(intNewOptionID, dsOptionElectiveGroups.Tables[0].Rows[k]["ElectiveGroupTitle"].ToString(), optionFootnoteID);

                                //get previous program elective group courses
                                DataSet dsElectiveGroupCourses = GetElectiveGroupCourses(optionElectiveGroupID, programBeginSTRM, programEndSTRM);
                                for (Int32 l = 0; l < dsElectiveGroupCourses.Tables[0].Rows.Count; l++) {
                                    Double overwriteUnitsMinimum, overwriteUnitsMaximum;

                                    try {
                                        //check if footnote exists
                                        optionFootnoteID = Convert.ToInt32(dsElectiveGroupCourses.Tables[0].Rows[l]["OptionFootnoteID"]);
                                        //get new OptionFootnoteID for the new program version
                                        optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dsElectiveGroupCourses.Tables[0].Rows[l]["FootnoteNumber"]));
                                    } catch {
                                        optionFootnoteID = 0;
                                    }

                                    try {
                                        overwriteUnitsMinimum = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[l]["OverwriteUnitsMinimum"]);
                                        overwriteUnitsMaximum = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[l]["OverwriteUnitsMaximum"]);
                                    } catch {
                                        overwriteUnitsMinimum = 0;
                                        overwriteUnitsMaximum = 0;
                                    }

                                    //insert elective group courses for new program version
                                    AddElectiveGroupCourse(newOptionElectiveGroupID, dsElectiveGroupCourses.Tables[0].Rows[l]["CourseOffering"].ToString(), optionFootnoteID, Convert.ToByte(dsElectiveGroupCourses.Tables[0].Rows[l]["OverwriteUnits"]), overwriteUnitsMinimum, overwriteUnitsMaximum);
                                }
                            }

                            //get program option courses and electives of previous program
                            DataRow[] dr = GetOptionCoursesAndElectives(optionID, programBeginSTRM, programEndSTRM);
                            oldCourseOffering = "";
                            for (Int32 k = 0; k < dr.Length; k++) {
                                String courseOffering = dr[k]["CourseOffering"].ToString();

                                if (oldCourseOffering != courseOffering || courseOffering == "ZZZ") {
                                    Int16 intQuarter;
                                    Int32 optionFootnoteID;
                                    Double overwriteUnitsMinimum, overwriteUnitsMaximum;

                                    try {
                                        intQuarter = Convert.ToInt16(dr[k]["Quarter"]);
                                    } catch {
                                        intQuarter = 0;
                                    }

                                    try {
                                        //check if footnote exists
                                        optionFootnoteID = Convert.ToInt32(dr[k]["OptionFootnoteID"]);
                                        //get new OptionFootnoteID for the new program
                                        optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dr[k]["FootnoteNumber"]));
                                    } catch {
                                        optionFootnoteID = 0;
                                    }

                                    try {
                                        overwriteUnitsMinimum = Convert.ToDouble(dr[k]["OverwriteUnitsMinimum"]);
                                        overwriteUnitsMaximum = Convert.ToDouble(dr[k]["OverwriteUnitsMaximum"]);
                                    } catch {
                                        overwriteUnitsMinimum = 0;
                                        overwriteUnitsMaximum = 0;
                                    }

                                    if (courseOffering == "ZZZ") {
                                        //get new OptionElectiveGroupID of new program for insert
                                        Int32 newOptionElectiveGroupID = GetOptionElectiveGroupID(intNewOptionID, dr[k]["COURSE_TITLE_LONG"].ToString());
                                        //insert program option elective for new program
                                        AddElectGroupRef(intNewOptionID, newOptionElectiveGroupID, intQuarter, Convert.ToDouble(dr[k]["UNITS_MINIMUM"]), Convert.ToDouble(dr[k]["UNITS_MAXIMUM"]), optionFootnoteID);
                                    } else {
                                        //insert program option course for new program
                                        AddOptCourse(intNewOptionID, courseOffering, optionFootnoteID, intQuarter, Convert.ToByte(dr[k]["OverwriteUnits"]), overwriteUnitsMinimum, overwriteUnitsMaximum);
                                    }
                                }
                                oldCourseOffering = courseOffering;
                            }
                        }
                    }

                    //get large text display from previous program
                    String strDisplay = GetProgramTextOutline(programVersionID);

                    //update new program to include the large text display
                    UpdateProgramTextOutline(newProgramVersionID, strDisplay);

                    return newProgramVersionID; //return ProgramVersionID of new program
                }

            } finally {
                objCon.Close();
            }
        }


        public Int32 EditProgDefinition(Int32 programVersionID, Int32 programID, Int32 degreeID, String strAreaOfStudyID, String strCategoryID, Int32 collegeID, String programTitle, String programBeginSTRM, String programEndSTRM, String publishedProgram, String publishedProgramVersionID, String programDescription, Char programDisplay, String lastModifiedEmployeeID, DateTime lastModifiedDate) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                Int32 newProgramVersionID = 0;

                //get progStatus of program before edit
                strSQL = "SELECT PublishedProgram " +
                    "FROM Program " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                String strOldStatus = objCmd.ExecuteScalar().ToString();

                //check if another program exists with a conflict in effective year quarters
                strSQL = "SELECT COUNT(*) " +
                         "FROM Program " +
                         "INNER JOIN ProgramDegree " +
                         "ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID " +
                         "WHERE CollegeID = " + collegeID +
                         " AND ProgramTitle = '" + programTitle.Replace("'", "''") +
                         "' AND ((ProgramBeginSTRM >= '" + programBeginSTRM +
                         "' AND ProgramBeginSTRM <= '" + programEndSTRM +
                         "') OR (ProgramEndSTRM <= '" + programEndSTRM +
                         "' AND ProgramEndSTRM >= '" + programBeginSTRM +
                         "') OR (ProgramBeginSTRM >= '" + programBeginSTRM +
                         "' AND ProgramEndSTRM <= '" + programEndSTRM +
                         "')) AND Program.ProgramVersionID <> " + programVersionID +
                         " AND DegreeID = " + degreeID;

                if (publishedProgramVersionID != "") {
                    strSQL += " AND Program.ProgramVersionID <> " + publishedProgramVersionID + ";";
                }

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) { //a conflict occured
                    //another program with the same title, degree, and college exists for the year quarter specified
                    return -1;
                } else {

                    if (strOldStatus == "1" && publishedProgram == "0") { //insert new program as Working Copy for minor changes

                        #region Add Published Copy Program Data to Working Copy of Program

                        newProgramVersionID = AddProgram(programID, programTitle, strAreaOfStudyID, strCategoryID, programDescription, collegeID, degreeID, programBeginSTRM, programEndSTRM, publishedProgram, programDisplay, programVersionID.ToString(), lastModifiedEmployeeID, lastModifiedDate);
                        //add all other fields of Published Copy of program to Working Copy of program

                        //get ProgramDegree data of published program copy
                        DataSet dsProgramDegree = GetProgramDegree(programVersionID);
                        for (Int32 i = 0; i < dsProgramDegree.Tables[0].Rows.Count; i++) {

                            //store Published Program Copy ProgramDegreeID
                            Int32 programDegreeID = Convert.ToInt32(dsProgramDegree.Tables[0].Rows[i]["ProgramDegreeID"]);

                            //Insert ProgramDegree data for Working Copy of program - store ProgramDegreeID
                            Int32 intNewProgramDegreeID = AddProgramDegree(newProgramVersionID, Convert.ToInt32(dsProgramDegree.Tables[0].Rows[i]["DegreeID"]));

                            //get ProgOptions data of Published Program Copy
                            DataSet dsProgOptions = GetProgOptions(programDegreeID);
                            for (Int32 j = 0; j < dsProgOptions.Tables[0].Rows.Count; j++) {
                                Int32 optionID = Convert.ToInt32(dsProgOptions.Tables[0].Rows[j]["OptionID"]);

                                //insert ProgOption data for the Working Program Copy
                                Int32 intNewOptionID = AddProgOption(intNewProgramDegreeID, dsProgOptions.Tables[0].Rows[j]["OptionTitle"].ToString());

                                //update the program option description
                                String optionDescription = dsProgOptions.Tables[0].Rows[j]["OptionDescription"].ToString();
                                String gainfulEmploymentID = dsProgOptions.Tables[0].Rows[j]["GainfulEmploymentID"].ToString();
                                String optionStateApproval = dsProgOptions.Tables[0].Rows[j]["OptionStateApproval"].ToString();
                                String optionCIP = dsProgOptions.Tables[0].Rows[j]["CIP"].ToString();
                                String optionEPC = dsProgOptions.Tables[0].Rows[j]["EPC"].ToString();
                                String academicPlan = dsProgOptions.Tables[0].Rows[j]["ACAD_PLAN"].ToString();
                                String totalQuarters = dsProgOptions.Tables[0].Rows[j]["TotalQuarters"].ToString();
                                String strPrimaryOption = dsProgOptions.Tables[0].Rows[j]["PrimaryOption"].ToString();
                                Byte bitPrimaryOption = 0;
                                if (strPrimaryOption == "True") {
                                    bitPrimaryOption = 1;
                                }

                                EditOptionValues(intNewOptionID, newProgramVersionID, optionDescription, optionStateApproval, optionCIP, optionEPC, academicPlan, bitPrimaryOption, gainfulEmploymentID, totalQuarters);

                                //get option locations of Published Program Copy
                                DataSet dsLocations = GetOptionLocations(optionID);
                                for (Int32 k = 0; k < dsLocations.Tables[0].Rows.Count; k++) {
                                    //insert option location for Working Program Copy
                                    AddOptionLocation(intNewOptionID, Convert.ToInt32(dsLocations.Tables[0].Rows[k]["LocationID"]));
                                }

                                //get option mcodes of Published Program Copy
                                DataSet dsMCodes = GetOptionMCodes(optionID);
                                for (Int32 k = 0; k < dsMCodes.Tables[0].Rows.Count; k++) {
                                    //insert option mcodes to for Working Program Copy
                                    AddOptionCollegeMCode(intNewOptionID, Convert.ToInt16(dsMCodes.Tables[0].Rows[k]["CollegeID"]), dsMCodes.Tables[0].Rows[k]["MCode"].ToString());
                                }

                                //get Published Program Copy footnotes
                                DataSet dsFootnotes = GetOptFootnotes(optionID);
                                for (Int32 k = 0; k < dsFootnotes.Tables[0].Rows.Count; k++) {

                                    //insert footnotes for Working Copy of program
                                    AddOptFootnote(intNewOptionID, Convert.ToInt16(dsFootnotes.Tables[0].Rows[k]["FootnoteNumber"]), dsFootnotes.Tables[0].Rows[k]["Footnote"].ToString());
                                }

                                //get OptPrereq data of Published Program Copy
                                DataSet dsOptPrereqs = GetOptPrereqs(optionID, programBeginSTRM, programEndSTRM);
                                String oldCourseOffering = "";
                                for (Int32 k = 0; k < dsOptPrereqs.Tables[0].Rows.Count; k++) {
                                    String courseOffering = dsOptPrereqs.Tables[0].Rows[k]["CourseOffering"].ToString();
                                    if (courseOffering != oldCourseOffering) {
                                        Int32 optionFootnoteID;

                                        try {
                                            //check if footnote exists
                                            optionFootnoteID = Convert.ToInt32(dsOptPrereqs.Tables[0].Rows[k]["OptionFootnoteID"]);
                                            //get new OptionFootnoteID for the hidden program copy
                                            optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dsOptPrereqs.Tables[0].Rows[k]["FootnoteNumber"]));
                                        } catch {
                                            optionFootnoteID = 0;
                                        }

                                        //insert OptPrereq data for the hidden program copy
                                        AddOptPrereqs(intNewOptionID, courseOffering, optionFootnoteID);
                                    }
                                    oldCourseOffering = courseOffering;
                                }

                                //get Published Program Copy elective groups
                                DataSet dsOptionElectiveGroups = GetOptionElectiveGroups(optionID);
                                for (Int32 k = 0; k < dsOptionElectiveGroups.Tables[0].Rows.Count; k++) {
                                    //store Published Program Copy OptionElectiveGroupID
                                    Int32 optionElectiveGroupID = Convert.ToInt32(dsOptionElectiveGroups.Tables[0].Rows[k]["OptionElectiveGroupID"]);
                                    Int32 optionFootnoteID;

                                    try {
                                        //check if footnote exists
                                        optionFootnoteID = Convert.ToInt32(dsOptionElectiveGroups.Tables[0].Rows[k]["OptionFootnoteID"]);
                                        //get new OptionFootnoteID for the Working Copy of the program
                                        optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dsOptionElectiveGroups.Tables[0].Rows[k]["FootnoteNumber"]));
                                    } catch {
                                        optionFootnoteID = 0;
                                    }

                                    //insert elective groups for Working Copy of the program and store OptionElectiveGroupID
                                    Int32 newOptionElectiveGroupID = AddOptionElectiveGroup(intNewOptionID, dsOptionElectiveGroups.Tables[0].Rows[k]["ElectiveGroupTitle"].ToString(), optionFootnoteID);

                                    //get Published Program Copy elective group courses
                                    DataSet dsElectiveGroupCourses = GetElectiveGroupCourses(optionElectiveGroupID, programBeginSTRM, programEndSTRM);
                                    for (Int32 l = 0; l < dsElectiveGroupCourses.Tables[0].Rows.Count; l++) {
                                        Double overwriteUnitsMinimum, overwriteUnitsMaximum;

                                        try {
                                            //check if footnote exists
                                            optionFootnoteID = Convert.ToInt32(dsElectiveGroupCourses.Tables[0].Rows[l]["OptionFootnoteID"]);
                                            //get new OptionFootnoteID for the hidden program copy
                                            optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dsElectiveGroupCourses.Tables[0].Rows[l]["FootnoteNumber"]));
                                        } catch {
                                            optionFootnoteID = 0;
                                        }

                                        try {
                                            overwriteUnitsMinimum = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[l]["OverwriteUnitsMinimum"]);
                                            overwriteUnitsMaximum = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[l]["OverwriteUnitsMaximum"]);
                                        } catch {
                                            overwriteUnitsMinimum = 0;
                                            overwriteUnitsMaximum = 0;
                                        }

                                        //insert elective group courses for the hidden program copy
                                        AddElectiveGroupCourse(newOptionElectiveGroupID, dsElectiveGroupCourses.Tables[0].Rows[l]["CourseOffering"].ToString(), optionFootnoteID, Convert.ToByte(dsElectiveGroupCourses.Tables[0].Rows[l]["OverwriteUnits"]), overwriteUnitsMinimum, overwriteUnitsMaximum);
                                    }
                                }

                                //get program option courses and electives of Published Program Copy
                                DataRow[] dr = GetOptionCoursesAndElectives(optionID, programBeginSTRM, programEndSTRM);
                                oldCourseOffering = "";
                                for (Int32 k = 0; k < dr.Length; k++) {
                                    String courseOffering = dr[k]["CourseOffering"].ToString();

                                    if (oldCourseOffering != courseOffering || courseOffering == "ZZZ") {
                                        Int16 intQuarter;
                                        Int32 optionFootnoteID;
                                        Double overwriteUnitsMinimum, overwriteUnitsMaximum;

                                        try {
                                            intQuarter = Convert.ToInt16(dr[k]["Quarter"]);
                                        } catch {
                                            intQuarter = 0;
                                        }

                                        try {
                                            //check if footnote exists
                                            optionFootnoteID = Convert.ToInt32(dr[k]["OptionFootnoteID"]);
                                            //get new OptionFootnoteID for the hidden program copy
                                            optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dr[k]["FootnoteNumber"]));
                                        } catch {
                                            optionFootnoteID = 0;
                                        }

                                        try {
                                            overwriteUnitsMinimum = Convert.ToDouble(dr[k]["OverwriteUnitsMinimum"]);
                                            overwriteUnitsMaximum = Convert.ToDouble(dr[k]["OverwriteUnitsMaximum"]);
                                        } catch {
                                            overwriteUnitsMinimum = 0;
                                            overwriteUnitsMaximum = 0;
                                        }

                                        if (courseOffering == "ZZZ") {
                                            //get new OptionElectiveGroupID of the hidden program copy for insert
                                            Int32 newOptionElectiveGroupID = GetOptionElectiveGroupID(intNewOptionID, dr[k]["COURSE_TITLE_LONG"].ToString());
                                            //insert program option elective for hidden program copy
                                            AddElectGroupRef(intNewOptionID, newOptionElectiveGroupID, intQuarter, Convert.ToDouble(dr[k]["UNITS_MINIMUM"]), Convert.ToDouble(dr[k]["UNITS_MAXIMUM"]), optionFootnoteID);
                                        } else {
                                            //insert program option course for hidden program copy
                                            AddOptCourse(intNewOptionID, courseOffering, optionFootnoteID, intQuarter, Convert.ToByte(dr[k]["OverwriteUnits"]), overwriteUnitsMinimum, overwriteUnitsMaximum);
                                        }
                                    }
                                    oldCourseOffering = courseOffering;
                                }
                            }
                        }

                        //get large text display from Published Program Copy
                        String strDisplay = GetProgramTextOutline(programVersionID);

                        //update the Working Program Copy to include the large text display
                        UpdateProgramTextOutline(newProgramVersionID, strDisplay);

                        return newProgramVersionID; //return new ProgramVersionID of the Working Program Copy
                        #endregion

                    } else { //update program

                        if (strOldStatus == "0" && publishedProgram == "1") { //update Published Program Copy and delete the Working Program Copy

                            strSQL = "SELECT PublishedProgramVersionID " +
                                "FROM Program " +
                                "WHERE ProgramVersionID = " + programVersionID + ";";

                            objCmd = new SqlCommand(strSQL, objCon);

                            try {
                                //if Published Program Copy exists
                                newProgramVersionID = Convert.ToInt32(objCmd.ExecuteScalar());

                                EditPrimaryAreaOfStudy(programID, strAreaOfStudyID, objCon);

                                EditPrimaryCategory(newProgramVersionID, strCategoryID, objCon);

                                //update the Program data
                                strSQL = "UPDATE Program " +
                                    "SET ProgramTitle = '" + programTitle.Replace("'", "''") + "', ProgramDescription = '" + programDescription.Replace("'", "''") + "', CollegeID = " + collegeID + ", ProgramBeginSTRM = '" + programBeginSTRM + "', ProgramEndSTRM = '" + programEndSTRM + "', ProgramDisplay = '" + programDisplay + "', LastModifiedEMPLID = '" + lastModifiedEmployeeID + "', LastModifiedDate = '" + lastModifiedDate + "' WHERE ProgramVersionID = " + newProgramVersionID + ";";

                                objCmd = new SqlCommand(strSQL, objCon);
                                objCmd.ExecuteNonQuery();

                                //delete old Published Program Copy data keeping the Program data & CPG data in place
                                strSQL = "SELECT DegreeID " +
                                    "FROM ProgramDegree " +
                                    "WHERE ProgramVersionID = " + newProgramVersionID +
                                    " GROUP BY DegreeID;";

                                objCmd = new SqlCommand(strSQL, objCon);
                                objDA = new SqlDataAdapter(objCmd);
                                DataSet dsDegrees = new DataSet();
                                objDA.Fill(dsDegrees);

                                for (Int32 intDSRow = 0; intDSRow < dsDegrees.Tables[0].Rows.Count; intDSRow++) {
                                    degreeID = Convert.ToInt32(dsDegrees.Tables[0].Rows[intDSRow]["DegreeID"]);
                                    //Delete Program Degrees, Options, Prerequisites, Courses, and Footnotes
                                    DeleteProgDeg(degreeID, newProgramVersionID, objCon);
                                }

                            } catch {

                                //Published Program Copy doesn't exist and needs to be added
                                newProgramVersionID = AddProgram(programVersionID, programID, programTitle, strAreaOfStudyID, strCategoryID, programDescription, collegeID, degreeID, programBeginSTRM, programEndSTRM, publishedProgram, programDisplay, lastModifiedEmployeeID, lastModifiedDate);

                                //get Working Copy of program CPG data
                                DataSet dsPreviousCPG = GetCareerPlanningGuide(programVersionID);

                                //add program CPG data for new program - get all data from the Working Program Copy
                                Int32 careerPlanningGuideID = AddProgCPGData(newProgramVersionID, lastModifiedEmployeeID, lastModifiedDate);

                                if (dsPreviousCPG.Tables[0].Rows.Count > 0) {
                                    //update new version CPG data with Working Copy of program CPG data
                                    EditProgCPGData(careerPlanningGuideID, newProgramVersionID, dsPreviousCPG.Tables[0].Rows[0]["ProgramEnrollment"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["ProgramWebsiteURL"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["CareerPlanningGuideFormat"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["ProgramCourseOfStudy"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["ProgramGoals"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["ProgramCareerOpportunities"].ToString(), dsPreviousCPG.Tables[0].Rows[0]["PublishedCareerPlanningGuide"].ToString(), "", lastModifiedEmployeeID, lastModifiedDate);
                                }

                                //get Working Program Copy fees
                                DataSet dsProgramFees = GetProgramFees(programVersionID);

                                //add program fees to Published Program Copy
                                if (dsProgramFees.Tables[0].Rows.Count > 0) {
                                    EditProgramFees(newProgramVersionID, dsProgramFees.Tables[0].Rows[0]["BookMinimum"].ToString(), dsProgramFees.Tables[0].Rows[0]["BookMaximum"].ToString(), dsProgramFees.Tables[0].Rows[0]["SuppliesMinimum"].ToString(), dsProgramFees.Tables[0].Rows[0]["SuppliesMaximum"].ToString(), dsProgramFees.Tables[0].Rows[0]["MiscMinimum"].ToString(), dsProgramFees.Tables[0].Rows[0]["MiscMaximum"].ToString(), dsProgramFees.Tables[0].Rows[0]["Note"].ToString());
                                }

                            } finally {

                                #region Add Working Program Copy Data to Published Program Copy
                                //add all other fields of Working Program Copy to the Published Program Copy

                                //get ProgramDegree data of Working Program Copy
                                DataSet dsProgramDegree = GetProgramDegree(programVersionID);
                                for (Int32 i = 0; i < dsProgramDegree.Tables[0].Rows.Count; i++) {

                                    //store Working Copy of program ProgramDegreeID
                                    Int32 programDegreeID = Convert.ToInt32(dsProgramDegree.Tables[0].Rows[i]["ProgramDegreeID"]);

                                    //Insert ProgramDegree data for Published Program Copy - store ProgramDegreeID
                                    Int32 intNewProgramDegreeID = AddProgramDegree(newProgramVersionID, Convert.ToInt32(dsProgramDegree.Tables[0].Rows[i]["DegreeID"]));

                                    //get ProgOptions data of Working Program Copy
                                    DataSet dsProgOptions = GetProgOptions(programDegreeID);
                                    for (Int32 j = 0; j < dsProgOptions.Tables[0].Rows.Count; j++) {
                                        Int32 optionID = Convert.ToInt32(dsProgOptions.Tables[0].Rows[j]["OptionID"]);

                                        //insert ProgOption data for the Published Program Copy
                                        Int32 intNewOptionID = AddProgOption(intNewProgramDegreeID, dsProgOptions.Tables[0].Rows[j]["OptionTitle"].ToString());

                                        //update the program option description
                                        String optionDescription = dsProgOptions.Tables[0].Rows[j]["OptionDescription"].ToString();
                                        String gainfulEmploymentID = dsProgOptions.Tables[0].Rows[j]["GainfulEmploymentID"].ToString();
                                        String optionStateApproval = dsProgOptions.Tables[0].Rows[j]["OptionStateApproval"].ToString();
                                        String optionCIP = dsProgOptions.Tables[0].Rows[j]["CIP"].ToString();
                                        String optionEPC = dsProgOptions.Tables[0].Rows[j]["EPC"].ToString();
                                        String academicPlan = dsProgOptions.Tables[0].Rows[j]["ACAD_PLAN"].ToString();
                                        String totalQuarters = dsProgOptions.Tables[0].Rows[j]["TotalQuarters"].ToString();
                                        String strPrimaryOption = dsProgOptions.Tables[0].Rows[j]["PrimaryOption"].ToString();
                                        Byte bitPrimaryOption = 0;
                                        if (strPrimaryOption == "True") {
                                            bitPrimaryOption = 1;
                                        }

                                        EditOptionValues(intNewOptionID, newProgramVersionID, optionDescription, optionStateApproval, optionCIP, optionEPC, academicPlan, bitPrimaryOption, gainfulEmploymentID, totalQuarters);

                                        //get option locations of Working Program Copy
                                        DataSet dsLocations = GetOptionLocations(optionID);
                                        for (Int32 k = 0; k < dsLocations.Tables[0].Rows.Count; k++) {
                                            //insert option location for Published Program Copy
                                            AddOptionLocation(intNewOptionID, Convert.ToInt32(dsLocations.Tables[0].Rows[k]["LocationID"]));
                                        }

                                        //get option mcodes of Working Program Copy
                                        DataSet dsMCodes = GetOptionMCodes(optionID);
                                        for (Int32 k = 0; k < dsMCodes.Tables[0].Rows.Count; k++) {
                                            //insert option mcodes to for Published Program Copy
                                            AddOptionCollegeMCode(intNewOptionID, Convert.ToInt16(dsMCodes.Tables[0].Rows[k]["CollegeID"]), dsMCodes.Tables[0].Rows[k]["MCode"].ToString());
                                        }

                                        //get Working Program Copy footnotes
                                        DataSet dsFootnotes = GetOptFootnotes(optionID);
                                        for (Int32 k = 0; k < dsFootnotes.Tables[0].Rows.Count; k++) {
                                            //insert footnotes for Published Program Copy
                                            AddOptFootnote(intNewOptionID, Convert.ToInt16(dsFootnotes.Tables[0].Rows[k]["FootnoteNumber"]), dsFootnotes.Tables[0].Rows[k]["Footnote"].ToString());
                                        }

                                        //get OptPrereq data of Working Program Copy
                                        DataSet dsOptPrereqs = GetOptPrereqs(optionID, programBeginSTRM, programEndSTRM);
                                        String oldCourseOffering = "";

                                        for (Int32 k = 0; k < dsOptPrereqs.Tables[0].Rows.Count; k++) {
                                            Int32 optionFootnoteID;
                                            String courseOffering = dsOptPrereqs.Tables[0].Rows[k]["CourseOffering"].ToString();

                                            if (oldCourseOffering != courseOffering) {
                                                try {
                                                    //check if a footnote exists
                                                    optionFootnoteID = Convert.ToInt32(dsOptPrereqs.Tables[0].Rows[k]["OptionFootnoteID"]);
                                                    //get the new OptionFootnoteID for the Published Program Copy
                                                    optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dsOptPrereqs.Tables[0].Rows[k]["FootnoteNumber"]));
                                                } catch {
                                                    optionFootnoteID = 0;
                                                }

                                                //insert OptPrereq data for the Published Program Copy
                                                AddOptPrereqs(intNewOptionID, dsOptPrereqs.Tables[0].Rows[k]["CourseOffering"].ToString(), optionFootnoteID);
                                            }
                                            oldCourseOffering = courseOffering;
                                        }

                                        //get Working Program Copy elective groups
                                        DataSet dsOptionElectiveGroups = GetOptionElectiveGroups(optionID);
                                        for (Int32 k = 0; k < dsOptionElectiveGroups.Tables[0].Rows.Count; k++) {
                                            //store Working Program Copy OptionElectiveGroupID
                                            Int32 optionElectiveGroupID = Convert.ToInt32(dsOptionElectiveGroups.Tables[0].Rows[k]["OptionElectiveGroupID"]);
                                            Int32 optionFootnoteID;

                                            try {
                                                //check if a footnote exists
                                                optionFootnoteID = Convert.ToInt32(dsOptionElectiveGroups.Tables[0].Rows[k]["OptionFootnoteID"]);
                                                //get the new OptionFootnoteID for the Published Program Copy
                                                optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dsOptionElectiveGroups.Tables[0].Rows[k]["FootnoteNumber"]));
                                            } catch {
                                                optionFootnoteID = 0;
                                            }

                                            //insert elective groups for Published Program Copy and store OptionElectiveGroupID
                                            Int32 newOptionElectiveGroupID = AddOptionElectiveGroup(intNewOptionID, dsOptionElectiveGroups.Tables[0].Rows[k]["ElectiveGroupTitle"].ToString(), optionFootnoteID);

                                            if (newOptionElectiveGroupID > 0) {
                                                //get Working Program Copy elective group courses
                                                DataSet dsElectiveGroupCourses = GetElectiveGroupCourses(optionElectiveGroupID, programBeginSTRM, programEndSTRM);
                                                for (Int32 l = 0; l < dsElectiveGroupCourses.Tables[0].Rows.Count; l++) {
                                                    Double overwriteUnitsMinimum, overwriteUnitsMaximum;

                                                    try {
                                                        //check if a footnote exists
                                                        optionFootnoteID = Convert.ToInt32(dsElectiveGroupCourses.Tables[0].Rows[l]["OptionFootnoteID"]);
                                                        //get the new OptionFootnoteID for the Published Program Copy
                                                        optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dsElectiveGroupCourses.Tables[0].Rows[l]["FootnoteNumber"]));
                                                    } catch {
                                                        optionFootnoteID = 0;
                                                    }

                                                    try {
                                                        overwriteUnitsMinimum = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[l]["OverwriteUnitsMinimum"]);
                                                        overwriteUnitsMaximum = Convert.ToDouble(dsElectiveGroupCourses.Tables[0].Rows[l]["OverwriteUnitsMaximum"]);
                                                    } catch {
                                                        overwriteUnitsMinimum = 0;
                                                        overwriteUnitsMaximum = 0;
                                                    }

                                                    //insert elective group courses for Published Program Copy
                                                    AddElectiveGroupCourse(newOptionElectiveGroupID, dsElectiveGroupCourses.Tables[0].Rows[l]["CourseOffering"].ToString(), optionFootnoteID, Convert.ToByte(dsElectiveGroupCourses.Tables[0].Rows[l]["OverwriteUnits"]), overwriteUnitsMinimum, overwriteUnitsMaximum);
                                                }
                                            }
                                        }

                                        //get program option courses and electives of hidden program copy
                                        DataRow[] dr = GetOptionCoursesAndElectives(optionID, programBeginSTRM, programEndSTRM);
                                        oldCourseOffering = "";
                                        for (Int32 k = 0; k < dr.Length; k++) {
                                            String courseOffering = dr[k]["CourseOffering"].ToString();

                                            if (oldCourseOffering != courseOffering || courseOffering == "ZZZ") {
                                                Int16 intQuarter;
                                                Int32 optionFootnoteID;
                                                Double overwriteUnitsMinimum, overwriteUnitsMaximum;

                                                try {
                                                    intQuarter = Convert.ToInt16(dr[k]["Quarter"]);
                                                } catch {
                                                    intQuarter = 0;
                                                }

                                                try {
                                                    //check if a footnote exists
                                                    optionFootnoteID = Convert.ToInt32(dr[k]["OptionFootnoteID"]);
                                                    //get the new OptionFootnoteID for the Published Program Copy
                                                    optionFootnoteID = GetOptionFootnoteID(intNewOptionID, Convert.ToInt16(dr[k]["FootnoteNumber"]));
                                                } catch {
                                                    optionFootnoteID = 0;
                                                }

                                                try {
                                                    overwriteUnitsMinimum = Convert.ToDouble(dr[k]["OverwriteUnitsMinimum"]);
                                                    overwriteUnitsMaximum = Convert.ToDouble(dr[k]["OverwriteUnitsMaximum"]);
                                                } catch {
                                                    overwriteUnitsMinimum = 0;
                                                    overwriteUnitsMaximum = 0;
                                                }

                                                if (courseOffering == "ZZZ") {
                                                    //get new OptionElectiveGroupID of the Published Program Copy to insert
                                                    Int32 newOptionElectiveGroupID = GetOptionElectiveGroupID(intNewOptionID, dr[k]["COURSE_TITLE_LONG"].ToString());
                                                    //insert program option elective for the Published Program Copy
                                                    AddElectGroupRef(intNewOptionID, newOptionElectiveGroupID, intQuarter, Convert.ToDouble(dr[k]["UNITS_MINIMUM"]), Convert.ToDouble(dr[k]["UNITS_MAXIMUM"]), optionFootnoteID);
                                                } else {
                                                    //insert program option course for the Published Program Copy
                                                    AddOptCourse(intNewOptionID, courseOffering, optionFootnoteID, intQuarter, Convert.ToByte(dr[k]["OverwriteUnits"]), overwriteUnitsMinimum, overwriteUnitsMaximum);
                                                }
                                            }
                                            oldCourseOffering = courseOffering;
                                        }
                                    }
                                }

                                //get large text display from Working Program Copy
                                String strDisplay = GetProgramTextOutline(programVersionID);

                                //update the Published Program Copy to include the large text display
                                UpdateProgramTextOutline(newProgramVersionID, strDisplay);

                                #endregion

                            }

                            //delete Working Program Copy
                            DeleteProgram(programVersionID);

                            return newProgramVersionID; //return stored ProgramVersionID of the Published Program Copy

                        } else {

                            EditPrimaryAreaOfStudy(programID, strAreaOfStudyID, objCon);

                            EditPrimaryCategory(programVersionID, strCategoryID, objCon);

                            //update the program
                            strSQL = "UPDATE Program " +
                                "SET ProgramTitle = '" + programTitle.Replace("'", "''") + "', ProgramDescription = '" + programDescription.Replace("'", "''") + "', CollegeID = " + collegeID + ", ProgramBeginSTRM = '" + programBeginSTRM + "', ProgramEndSTRM = '" + programEndSTRM + "', LastModifiedEMPLID = '" + lastModifiedEmployeeID + "', LastModifiedDate = '" + lastModifiedDate + "' WHERE ProgramVersionID = " + programVersionID + ";";

                            objCmd = new SqlCommand(strSQL, objCon);

                            if (objCmd.ExecuteNonQuery() == 0) {
                                return 0;
                            } else {
                                return programVersionID;
                            }
                        }
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteProgramDescription(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE Program " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int32 AddProgCPGData(Int32 programVersionID, String lastModifiedEmployeeID, DateTime lastModifiedDate) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " + //check if the record has already been inserted
                    "FROM CareerPlanningGuide " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return -1;
                } else {
                    strSQL = "INSERT INTO CareerPlanningGuide(ProgramVersionID, CareerPlanningGuideFormat, PublishedCareerPlanningGuide, LastModifiedEMPLID, LastModifiedDate) " +
                        "VALUES (" + programVersionID + ", '1', 0, '" + lastModifiedEmployeeID + "', '" + lastModifiedDate + "') " +
                        "SELECT CareerPlanningGuideID FROM CareerPlanningGuide WHERE (CareerPlanningGuideID = SCOPE_IDENTITY());";

                    objCmd = new SqlCommand(strSQL, objCon);
                    return Convert.ToInt32(objCmd.ExecuteScalar());
                }

            } finally {
                objCon.Close();
            }
        }


        public bool HasPublishedCPGCopy(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM CareerPlanningGuide " +
                    "WHERE ProgramVersionID = " + programVersionID +
                    " AND PublishedCareerPlanningGuide = 1;";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        /// <summary>
        /// Returns career planning guide details for a program
        /// </summary>
        /// <param name="programVersionID">Program primary key</param>
        /// <returns>
        /// DataSet
        /// Field List: CareerPlanningGuideID - Career planning guide primary key
        ///             ProgramTitle - Program title
        ///             DegreeLongTitle - Degree Long Title
        ///             DegreeShortTitle - Degree Short Title
        ///             CategoryTitle - Program category title
        ///             ProgramBeginSTRM - Program begin term code (STRM)
        ///             BeginTerm_DESCR - Program begin term description (Example: Fall 2017)
        ///             ProgramEndSTRM - Program end term code (STRM)
        ///             EndTerm_DESCR - Program end term description (Example: Fall 2017)
        ///             ProgramDisplay - Identifies whether the program will have a program outline or large text description: 1 = program outline, 2 =  large text description
        ///             CollegeShortTitle - College abbreviation (SCC, SFCC)
        ///             ProgramEnrollment - When students can enroll in the program
        ///             ProgramWebsiteURL - URL to program website
        ///             CareerPlanningGuideFormat - How to format the back of a career planning guide: 1 = two column display, 2 = one column centered
        ///             ProgramCourseOfStudy - Program course of study text
        ///             ProgramGoals - Program goals
        ///             ProgramCareerOpportunities - Program career opportunities
        ///             PublishedCareerPlanningGuide - Career planning guide status: 1 = Published Copy, 0 = Working Copy
        ///             PublishedCareerPlanningGuideID - Used to store the primary key of the Published CPG Copy when an Working Copy is created for editing purposes
        /// </returns>
        public DataSet GetCareerPlanningGuide(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCareerPlanningGuide", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@ProgramVersionID", SqlDbType.Int);
                objCmd.Parameters["@ProgramVersionID"].Value = programVersionID;

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetPublishedProgramCPGData(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT CareerPlanningGuideID, ProgramTitle, ProgramBeginSTRM, ProgramEndSTRM, ProgramDisplay, CollegeShortTitle, ProgramEnrollment, ProgramWebsiteURL, CareerPlanningGuideFormat, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, PublishedCareerPlanningGuide, PublishedCareerPlanningGuideID " +
                    "FROM CareerPlanningGuide INNER JOIN Program " +
                    "ON Program.ProgramVersionID = CareerPlanningGuide.ProgramVersionID " +
                    "INNER JOIN College " +
                    "ON College.CollegeID = Program.CollegeID " +
                    "WHERE Program.ProgramVersionID = " + programVersionID +
                    " AND PublishedCareerPlanningGuide = 1;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public bool EditProgCPGData(Int32 careerPlanningGuideID, Int32 programVersionID, String programEnrollment, String programWebsiteURL, String careerPlanningGuideFormat, String programCourseOfStudy, String programGoals, String programCareerOpportunities, String publishedProgram, String publishedCareerPlanningGuideID, String lastModifiedEmployeeID, DateTime lastModifiedDate) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                //get progStatus of program before edit
                strSQL = "SELECT PublishedCareerPlanningGuide " +
                        "FROM CareerPlanningGuide " +
                        "WHERE CareerPlanningGuideID = " + careerPlanningGuideID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                String strOldStatus = objCmd.ExecuteScalar().ToString();

                if (strOldStatus == "1" && publishedProgram == "0") {

                    //insert Working Copy of CPG for minor changes
                    strSQL = "INSERT INTO CareerPlanningGuide(ProgramVersionID, ProgramWebsiteURL, ProgramEnrollment, CareerPlanningGuideFormat, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, PublishedCareerPlanningGuide, PublishedCareerPlanningGuideID, LastModifiedEMPLID, LastModifiedDate) " +
                             "VALUES(" + programVersionID + ",'" + programWebsiteURL + "','" + programEnrollment + "','" + careerPlanningGuideFormat + "','" + programCourseOfStudy.Replace("'", "''") + "','" + programGoals.Replace("'", "''") + "','" + programCareerOpportunities.Replace("'", "''") + "','" + publishedProgram + "'," + careerPlanningGuideID + ",'" + lastModifiedEmployeeID + "','" + lastModifiedDate + "');";

                    objCmd = new SqlCommand(strSQL, objCon);
                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }

                } else if (strOldStatus == "0" && publishedProgram == "1") {

                    if (publishedCareerPlanningGuideID != "") { //update Published Copy with Working Copy cpg data
                        strSQL = "UPDATE CareerPlanningGuide " +
                            "SET ProgramWebsiteURL = '" + programWebsiteURL + "', ProgramEnrollment = '" + programEnrollment + "', CareerPlanningGuideFormat = '" + careerPlanningGuideFormat + "', ProgramCourseOfStudy = '" + programCourseOfStudy.Replace("'", "''") + "', ProgramGoals = '" + programGoals.Replace("'", "''") + "', ProgramCareerOpportunities = '" + programCareerOpportunities.Replace("'", "''") + "', LastModifiedEMPLID = '" + lastModifiedEmployeeID + "', LastModifiedDate = '" + lastModifiedDate +
                            "' WHERE CareerPlanningGuideID = " + publishedCareerPlanningGuideID + ";";

                        objCmd = new SqlCommand(strSQL, objCon);
                        if (objCmd.ExecuteNonQuery() == 0) {
                            return false;
                        }
                    } else { //insert Published Copy for public display
                        strSQL = "INSERT INTO CareerPlanningGuide(ProgramVersionID, ProgramWebsiteURL, ProgramEnrollment, CareerPlanningGuideFormat, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, PublishedCareerPlanningGuide, LastModifiedEMPLID, LastModifiedDate) " +
                            "VALUES(" + programVersionID + ",'" + programWebsiteURL + "','" + programEnrollment + "','" + careerPlanningGuideFormat + "','" + programCourseOfStudy.Replace("'", "''") + "','" + programGoals.Replace("'", "''") + "','" + programCareerOpportunities.Replace("'", "''") + "','" + publishedProgram + "','" + lastModifiedEmployeeID + "','" + lastModifiedDate + "');";

                        objCmd = new SqlCommand(strSQL, objCon);
                        if (objCmd.ExecuteNonQuery() == 0) {
                            return false;
                        }
                    }

                    //delete hidden copy
                    strSQL = "DELETE CareerPlanningGuide " +
                             "WHERE CareerPlanningGuideID = " + careerPlanningGuideID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);
                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }

                } else {

                    //update current cpg data
                    strSQL = "UPDATE CareerPlanningGuide " +
                            "SET ProgramEnrollment = '" + programEnrollment + "', ProgramWebsiteURL = '" + programWebsiteURL + "', CareerPlanningGuideFormat = '" + careerPlanningGuideFormat + "', ProgramCourseOfStudy = '" + programCourseOfStudy.Replace("'", "''") + "', ProgramGoals = '" + programGoals.Replace("'", "''") + "', ProgramCareerOpportunities = '" + programCareerOpportunities.Replace("'", "''") + "', LastModifiedEMPLID = '" + lastModifiedEmployeeID + "', LastModifiedDate = '" + lastModifiedDate +
                            "' WHERE CareerPlanningGuideID = " + careerPlanningGuideID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = Convert.ToInt32(objCmd.ExecuteNonQuery());

                    if (intCtr == 0) {
                        return false;
                    } else {
                        return true;
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteProgCPGData(Int32 careerPlanningGuideID, String publishedCareerPlanningGuideID, String lastModifiedEmployeeID, DateTime lastModifiedDate) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                if (publishedCareerPlanningGuideID != "") {
                    strSQL = "DELETE CareerPlanningGuide " +
                        "WHERE CareerPlanningGuideID = " + careerPlanningGuideID + ";";
                } else {
                    strSQL = "UPDATE CareerPlanningGuide " +
                        "SET ProgramWebsiteURL = NULL, ProgramEnrollment = NULL, ProgramCourseOfStudy = NULL, ProgramGoals = NULL, ProgramCareerOpportunities = NULL, CareerPlanningGuideFormat = 1, PublishedCareerPlanningGuide = 0, LastModifiedEMPLID = '" + lastModifiedEmployeeID + "', LastModifiedDate = '" + lastModifiedDate +
                        "' WHERE CareerPlanningGuideID = " + careerPlanningGuideID + ";";
                }

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        /// <summary>
        /// Returns the conflicting program version's begin and end term description as a string
        /// </summary>
        /// <param name="programTitle">Program Title</param>
        /// <param name="collegeID">College Primary Key</param>
        /// <param name="beginSTRM">Begin Term Code</param>
        /// <param name="endSTRM">End Term Code</param>
        /// <returns>String: Begin Term Description - End Term Description (Example: Spring 2017 - Fall 2018)</returns>
        public String GetConflictingTermsForNewProgram(String programTitle, Int32 collegeID, Int32 degreeID, String beginSTRM, String endSTRM) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetConflictingTermsForNewProgram", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@ProgramTitle", SqlDbType.VarChar);
                objCmd.Parameters["@ProgramTitle"].Value = programTitle;

                objCmd.Parameters.Add("@CollegeID", SqlDbType.Int);
                objCmd.Parameters["@CollegeID"].Value = collegeID;

                objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                objCmd.Parameters["@DegreeID"].Value = degreeID;

                objCmd.Parameters.Add("@BeginSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@BeginSTRM"].Value = beginSTRM;

                objCmd.Parameters.Add("@EndSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@EndSTRM"].Value = endSTRM;


                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);

                if (objDS.Tables[0].Rows.Count > 0) {
                    return (objDS.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString() + " - " + objDS.Tables[0].Rows[0]["EndTerm_DESCR"].ToString());
                } else {
                    return "";
                }

            } finally {
                objCon.Close();
            }
        }


        /// <summary>
        /// Returns the conflicting program version's begin and end term range as a string
        /// </summary>
        /// <param name="programVersionID">Program Primary Key</param>
        /// <param name="programTitle">Program Title</param>
        /// <param name="collegeID">College Primary Key</param>
        /// <param name="degreeID">Degree Priamry Key</param>
        /// <param name="beginSTRM">Begin Term Code</param>
        /// <param name="endSTRM">End Term Code</param>
        /// <returns>String: Begin Term Description - End Term Description (Example: Spring 2017 - Fall 2018)</returns>
        public String GetConflictingTermsForExistingProgram(Int32 programVersionID, String programTitle, Int32 collegeID, Int32 degreeID, String beginSTRM, String endSTRM) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetConflictingTermsForExistingProgram", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@ProgramTitle", SqlDbType.VarChar);
                objCmd.Parameters["@ProgramTitle"].Value = programTitle;

                objCmd.Parameters.Add("@CollegeID", SqlDbType.Int);
                objCmd.Parameters["@CollegeID"].Value = collegeID;

                objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                objCmd.Parameters["@DegreeID"].Value = degreeID;

                objCmd.Parameters.Add("@BeginSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@BeginSTRM"].Value = beginSTRM;

                objCmd.Parameters.Add("@EndSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@EndSTRM"].Value = endSTRM;

                objCmd.Parameters.Add("@ProgramVersionID", SqlDbType.Int);
                objCmd.Parameters["@ProgramVersionID"].Value = programVersionID;

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);

                if (objDS.Tables[0].Rows.Count > 0) {
                    return (objDS.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString() + " - " + objDS.Tables[0].Rows[0]["EndTerm_DESCR"].ToString());
                } else {
                    return "";
                }

            } finally {
                objCon.Close();
            }
        }


        //adds a new program from scratch
        public Int32 AddProgram(String programTitle, String areaOfStudyID, String strCategoryID, String programDescription, Int32 collegeID, Int32 degreeID, String programBeginSTRM, String programEndSTRM, String publishedProgram, Char programDisplay, String lastModifiedEmployeeID, DateTime lastModifiedDate) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                         "FROM Program " +
                         "INNER JOIN ProgramDegree " +
                         "ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID " +
                         "WHERE ProgramTitle = '" + programTitle.Replace("'", "''") +
                         "' AND CollegeID = " + collegeID +
                         " AND ((ProgramBeginSTRM >= '" + programBeginSTRM +
                         "' AND ProgramBeginSTRM <= '" + programEndSTRM +
                         "') OR (ProgramEndSTRM <= '" + programEndSTRM +
                         "' AND ProgramEndSTRM >= '" + programBeginSTRM +
                         "') OR (ProgramBeginSTRM >= '" + programBeginSTRM +
                         "' AND ProgramEndSTRM <= '" + programEndSTRM + "'))" +
                         " AND DegreeID = " + degreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                Int32 intProgCount = Convert.ToInt32(objCmd.ExecuteScalar());

                if (intProgCount == 0) {

                    strSQL = "INSERT INTO Program(ProgramTitle, ProgramDescription, CollegeID, ProgramBeginSTRM, ProgramEndSTRM, PublishedProgram, ProgramDisplay, LastModifiedEMPLID, LastModifiedDate) " +
                        "VALUES ('" + programTitle.Replace("'", "''") + "','" + programDescription.Replace("'", "''") + "'," + collegeID + ",'" + programBeginSTRM + "','" + programEndSTRM + "','" + publishedProgram + "'," + programDisplay + ",'" + lastModifiedEmployeeID + "','" + lastModifiedDate + "'); " +
                        "SELECT ProgramVersionID FROM Program WHERE (ProgramVersionID = SCOPE_IDENTITY());";

                    objCmd = new SqlCommand(strSQL, objCon);
                    Int32 programID = Convert.ToInt32(objCmd.ExecuteScalar());

                    //insert non-changing program id
                    strSQL = "UPDATE Program " +
                        "SET ProgramID = " + programID +
                        " WHERE ProgramVersionID = " + programID + ";";
                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        if (strCategoryID != "") {
                            AddProgramCategory(programID, Convert.ToInt32(strCategoryID), 1, objCon);
                        }
                        if (areaOfStudyID != "") {
                            AddProgramAreaOfStudy(programID, Convert.ToInt32(areaOfStudyID), 1, objCon);
                        }
                        return programID;
                    } else {
                        return 0;
                    }
                } else {
                    return -1;
                }

            } finally {
                objCon.Close();
            }
        }


        //used to create a new version of a program
        public Int32 AddProgram(Int32 programVersionID, Int32 programID, String programTitle, String strAreaOfStudyID, String strCategoryID, String programDescription, Int32 collegeID, Int32 degreeID, String programBeginSTRM, String programEndSTRM, String publishedProgram, Char programDisplay, String lastModifiedEmployeeID, DateTime lastModifiedDate) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Program " +
                    "INNER JOIN ProgramDegree " +
                    "ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID " +
                    "WHERE ProgramTitle = '" + programTitle.Replace("'", "''") +
                    "' AND CollegeID = " + collegeID +
                    " AND ((ProgramBeginSTRM >= '" + programBeginSTRM +
                    "' AND ProgramBeginSTRM <= '" + programEndSTRM +
                    "') OR (ProgramEndSTRM <= '" + programEndSTRM +
                    "' AND ProgramEndSTRM >= '" + programBeginSTRM +
                    "') OR (ProgramBeginSTRM >= '" + programBeginSTRM +
                    "' AND ProgramEndSTRM <= '" + programEndSTRM +
                    "')) AND Program.ProgramVersionID <> " + programVersionID +
                    " AND DegreeID = " + degreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                Int32 intProgCount = Convert.ToInt32(objCmd.ExecuteScalar());

                if (intProgCount == 0) {
                    strSQL = "INSERT INTO Program(ProgramID, ProgramTitle, ProgramDescription, CollegeID, ProgramBeginSTRM, ProgramEndSTRM, PublishedProgram, ProgramDisplay, LastModifiedEMPLID, LastModifiedDate) " +
                        "VALUES (" + programID + ",'" + programTitle.Replace("'", "''") + "','" + programDescription.Replace("'", "''") + "'," + collegeID + ",'" + programBeginSTRM + "','" + programEndSTRM + "','" + publishedProgram + "'," + programDisplay + ",'" + lastModifiedEmployeeID + "','" + lastModifiedDate + "'); " +
                        "SELECT ProgramVersionID FROM Program WHERE (ProgramVersionID = SCOPE_IDENTITY());";

                    objCmd = new SqlCommand(strSQL, objCon);
                    programVersionID = Convert.ToInt32(objCmd.ExecuteScalar());

                    if (strAreaOfStudyID != "") {
                        AddProgramAreaOfStudy(programID, Convert.ToInt32(strAreaOfStudyID), 1, objCon);
                    }

                    if (strCategoryID != "") {
                        AddProgramCategory(programVersionID, Convert.ToInt32(strCategoryID), 1, objCon);
                    }

                    return programVersionID;
                } else {
                    return -1;
                }

            } finally {
                objCon.Close();
            }
        }


        //used when a Published Program Copy is switched to unpublished
        public Int32 AddProgram(Int32 programID, String programTitle, String strAreaOfStudyID, String strCategoryID, String programDescription, Int32 collegeID, Int32 degreeID, String programBeginSTRM, String programEndSTRM, String publishedProgram, Char programDisplay, String publishedProgramVersionID, String lastModifiedEmployeeID, DateTime lastModifiedDate) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Program " +
                    "INNER JOIN ProgramDegree " +
                    "ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID " +
                    "WHERE ProgramTitle = '" + programTitle.Replace("'", "''") +
                    "' AND CollegeID = " + collegeID +
                    " AND ((ProgramBeginSTRM >= '" + programBeginSTRM +
                    "' AND ProgramBeginSTRM <= '" + programEndSTRM +
                    "') OR (ProgramEndSTRM <= '" + programEndSTRM +
                    "' AND ProgramEndSTRM >= '" + programBeginSTRM +
                    "') OR (ProgramBeginSTRM >= '" + programBeginSTRM +
                    "' AND ProgramEndSTRM <= '" + programEndSTRM +
                    "')) AND Program.ProgramVersionID <> " + publishedProgramVersionID +
                    " AND DegreeID = " + degreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                Int32 intProgCount = Convert.ToInt32(objCmd.ExecuteScalar());

                if (intProgCount == 0) {

                    strSQL = "INSERT INTO Program(ProgramID, ProgramTitle, ProgramDescription, CollegeID, ProgramBeginSTRM, ProgramEndSTRM, PublishedProgram, ProgramDisplay, PublishedProgramVersionID, LastModifiedEMPLID, LastModifiedDate) " +
                        "VALUES (" + programID + ",'" + programTitle.Replace("'", "''") + "','" + programDescription.Replace("'", "''") + "'," + collegeID + ",'" + programBeginSTRM + "','" + programEndSTRM + "','" + publishedProgram + "'," + programDisplay + "," + publishedProgramVersionID + ",'" + lastModifiedEmployeeID + "','" + lastModifiedDate + "'); " +
                        "SELECT ProgramVersionID FROM Program WHERE (ProgramVersionID = SCOPE_IDENTITY());";

                    objCmd = new SqlCommand(strSQL, objCon);
                    Int32 programVersionID = Convert.ToInt32(objCmd.ExecuteScalar());

                    if (strAreaOfStudyID != "") {
                        AddProgramAreaOfStudy(programID, Convert.ToInt32(strAreaOfStudyID), 1, objCon);
                    }

                    if (strCategoryID != "") {
                        AddProgramCategory(programVersionID, Convert.ToInt32(strCategoryID), 1, objCon);
                    }

                    return programVersionID;
                } else {
                    return -1;
                }

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetProgram(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                //added group by clause because some records were repeated, added begin and end strm
                strSQL = "SELECT ProgramTitle, ProgramBeginSTRM, ProgramEndSTRM, CollegeShortTitle, Program.CollegeID, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, Program.ProgramVersionID, ProgramDisplay, PublishedProgram " +
                        "FROM Program " +
                        "LEFT OUTER JOIN CareerPlanningGuide " +
                        "ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID " +
                        "INNER JOIN College " +
                        "ON College.CollegeID = Program.CollegeID " +
                        "WHERE Program.ProgramVersionID = " + programVersionID +
                        " GROUP BY ProgramTitle, CollegeShortTitle, Program.CollegeID, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, Program.ProgramVersionID, ProgramDisplay, ProgramBeginSTRM, ProgramEndSTRM, PublishedProgram;";

                /* query used in live version
                 "SELECT CollegeShortTitle, Program.CollegeID, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, Program.ProgramVersionID, ProgramDisplay " +
                        "FROM Program LEFT OUTER JOIN CareerPlanningGuide " +
                        "ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID " +
                        "INNER JOIN College " +
                        "ON College.CollegeID = Program.CollegeID " +
                        "WHERE Program.ProgramVersionID = " + programVersionID + ";";
                */

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public String GetProgramTextOutline(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {
                objCon.Open();

                strSQL = "SELECT ProgramTextOutline " +
                    "FROM Program " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS.Tables[0].Rows[0]["ProgramTextOutline"].ToString();

            } finally {
                objCon.Close();
            }
        }


        public bool UpdateProgramTextOutline(Int32 programVersionID, String programTextOutline) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                if (programTextOutline == "") {
                    strSQL = "UPDATE Program " +
                            "SET ProgramTextOutline = NULL " +
                            "WHERE ProgramVersionID = " + programVersionID + ";";
                } else {
                    strSQL = "UPDATE Program " +
                            "SET ProgramTextOutline = '" + programTextOutline.Replace("'", "''") +
                            "' WHERE ProgramVersionID = " + programVersionID + ";";
                }

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool UpdateProgramDisplay(Int32 programVersionID, Char programDisplay) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "UPDATE Program " +
                        "SET ProgramDisplay = '" + programDisplay +
                        "' WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int32 AddProgramDegree(Int32 programVersionID, Int32 degreeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM ProgramDegree " +
                    "WHERE ProgramVersionID = " + programVersionID +
                    " AND DegreeID = " + degreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0) {

                    strSQL = "INSERT INTO ProgramDegree(ProgramVersionID, DegreeID) " +
                        "VALUES(" + programVersionID + "," + degreeID + ") " +
                        "SELECT ProgramDegreeID FROM ProgramDegree WHERE (ProgramDegreeID = SCOPE_IDENTITY());";

                    objCmd = new SqlCommand(strSQL, objCon);
                    return Convert.ToInt32(objCmd.ExecuteScalar());

                } else {
                    return -1;
                }

            } finally {
                objCon.Close();
            }
        }

        public bool UpdateProgramDegree(Int32 programVersionID, Int32 degreeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "UPDATE ProgramDegree " +
                        "SET DegreeID = " + degreeID +
                        " WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int32 GetProgramDegreeID(Int32 programVersionID, Int32 degreeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT ProgramDegreeID " +
                    "FROM ProgramDegree " +
                    "WHERE ProgramVersionID = " + programVersionID +
                    " AND DegreeID = " + degreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                try {
                    return Convert.ToInt32(objCmd.ExecuteScalar());
                } catch {
                    return 0;
                }

            } finally {
                objCon.Close();
            }
        }


        public String GetProgOptionTitle(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionTitle " +
                    "FROM ProgramDegreeOption " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                try {
                    return objCmd.ExecuteScalar().ToString();
                } catch {
                    return "";
                }

            } finally {
                objCon.Close();
            }
        }


        public Int32 AddProgOption(Int32 programDegreeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " + //check if record was already inserted
                    "FROM ProgramDegreeOption " +
                    "WHERE ProgramDegreeID = " + programDegreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return -1;
                } else {

                    strSQL = "INSERT INTO ProgramDegreeOption(ProgramDegreeID) " +
                        "VALUES(" + programDegreeID + ")" +
                        " SELECT OptionID FROM ProgramDegreeOption WHERE (OptionID = SCOPE_IDENTITY());";

                    objCmd = new SqlCommand(strSQL, objCon);
                    return Convert.ToInt32(objCmd.ExecuteScalar()); //may need to add error handling and return 0 if an error occurs
                }

            } finally {
                objCon.Close();
            }
        }


        public Int32 AddProgOption(Int32 programDegreeID, String optionTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                        "FROM ProgramDegreeOption " +
                        "WHERE OptionTitle = '" + optionTitle.Replace("'", "''") +
                        "' AND ProgramDegreeID = " + programDegreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return -1;
                } else {
                    strSQL = "INSERT INTO ProgramDegreeOption(ProgramDegreeID, OptionTitle) " +
                            "VALUES(" + programDegreeID + ",'" + optionTitle.Replace("'", "''") + "')" +
                            " SELECT OptionID FROM ProgramDegreeOption WHERE (OptionID = SCOPE_IDENTITY());";

                    objCmd = new SqlCommand(strSQL, objCon);
                    return Convert.ToInt32(objCmd.ExecuteScalar()); //may need to add error handling and return 0 if an error occurs
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 AddOptionTitle(Int32 degreeID, Int32 programVersionID, String optionTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                        "FROM ProgramDegreeOption " +
                        "WHERE OptionTitle = '" + optionTitle.Replace("'", "''") +
                        "' AND ProgramDegreeID = " +
                        "(SELECT ProgramDegreeID " +
                        "FROM ProgramDegree " +
                        "WHERE ProgramVersionID = " + programVersionID +
                        " AND DegreeID = " + degreeID + ");";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return 2;
                } else {
                    Int32 programDegreeID = GetProgramDegreeID(programVersionID, degreeID);

                    objCon.Open(); //added because the call to GetProgramDegreeID above closed the connection

                    strSQL = "UPDATE ProgramDegreeOption " +
                            "SET OptionTitle = '" + optionTitle.Replace("'", "''") +
                            "' WHERE ProgramDegreeID = " + programDegreeID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 EditOptionTitle(Int32 degreeID, Int32 programVersionID, Int32 optionID, String optionTitle, String strOldOptTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                        "FROM ProgramDegreeOption " +
                        "INNER JOIN ProgramDegree " +
                        "ON ProgramDegree.ProgramDegreeID = ProgramDegreeOption.ProgramDegreeID " +
                        "WHERE OptionTitle = '" + optionTitle.Replace("'", "''") +
                        "' AND OptionID <> " + optionID +
                        " AND ProgramVersionID = " + programVersionID +
                        " AND DegreeID = " + degreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return 2;
                } else {
                    strSQL = "UPDATE ProgramDegreeOption " +
                            "SET OptionTitle = '" + optionTitle.Replace("'", "''") +
                            "' WHERE OptionID = " + optionID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteProgOption(Int32 degreeID, Int32 optionID, String optionTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                //delete the option prerequisites
                DeleteOptPrereqs(optionID, objCon);

                //delete OptElectives
                DeleteProgOptElectives(optionID, objCon);

                //delete ProgOptCourses
                DeleteProgOptCourses(optionID, objCon);

                //delete Elective Groups
                DeleteOptionElectives(optionID, objCon);

                //delete ProgOptFootnotes
                DeleteProgOptFootnotes(optionID, objCon);

                //delete OptMCodes
                DeleteOptionMCodes(optionID, objCon);

                //deleete Option Locations
                DeleteOptionLocations(optionID, objCon);

                //delete option
                strSQL = "DELETE " +
                        "ProgramDegreeOption " +
                        "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool UpdateOptionTitle(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "UPDATE ProgramDegreeOption " +
                    "SET OptionTitle = NULL " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetProgramDegree(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT DegreeLongTitle, DegreeShortTitle, ProgramDegreeID, ProgramDegree.DegreeID " +
                    "FROM ProgramDegree INNER JOIN Degree " +
                    "ON Degree.DegreeID = ProgramDegree.DegreeID " +
                    "INNER JOIN Program " +
                    "ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID " +
                    "WHERE ProgramDegree.ProgramVersionID = " + programVersionID +
                    " ORDER BY DegreeShortTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetOptPrereqs(Int32 optionID, String programBeginSTRM, String programEndSTRM) {
            
            objCon = new SqlConnection(strConnection);

            try {
                objCon.Open();
                objCmd = new SqlCommand("usp_GetOptionPrerequisites", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                //objCmd.Parameters.AddWithValue("@BeginSTRM", programBeginSTRM);
                //objCmd.Parameters.AddWithValue("@EndSTRM", programEndSTRM);
                objCmd.Parameters.AddWithValue("@OptionID", optionID);
                objCmd.Parameters.AddWithValue("@CurrentSTRM", csTerm.GetCurrentTerm());
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

                /*
                objCon.Open();

                String currentSTRM = csTerm.GetCurrentTerm();
                if (String.Compare(programEndSTRM, currentSTRM, true) < 0) { //archived program
                    //display the last published version of the course in the program strm span
                    strSQL = "SELECT SUBJECT, CATALOG_NBR, CourseSuffix, Course.CourseOffering, COURSE_TITLE_LONG, CourseBeginSTRM, CourseEndSTRM, FootnoteNumber, OptionPrerequisiteID, OptionPrerequisite.OptionFootnoteID " +
                             "FROM OptionPrerequisite INNER JOIN Course " +
                             "ON Course.CourseOffering = OptionPrerequisite.CourseOffering " +
                             "AND ((CourseBeginSTRM <= '" + programBeginSTRM + "' AND (CourseEndSTRM >= '" + programBeginSTRM + "' OR CourseEndSTRM = 'Z999')) " +
                             "OR (CourseBeginSTRM >= '" + programBeginSTRM + "' AND CourseBeginSTRM <= '" + programEndSTRM + "' AND (CourseEndSTRM <= '" + programEndSTRM + "' OR CourseEndSTRM = 'Z999'))) " +
                             "LEFT OUTER JOIN OptionFootnote " +
                             "ON OptionFootnote.OptionFootnoteID = OptionPrerequisite.OptionFootnoteID " +
                             "WHERE OptionPrerequisite.OptionID = " + optionID +
                             " ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR, CourseEndSTRM DESC;";
                } else if (String.Compare(programBeginSTRM, currentSTRM, true) > 0) { //future program
                    //display the first published version of the course in the program strm span
                    strSQL = "SELECT SUBJECT, CATALOG_NBR, CourseSuffix, Course.CourseOffering, COURSE_TITLE_LONG, CourseBeginSTRM, CourseEndSTRM, FootnoteNumber, OptionPrerequisiteID, OptionPrerequisite.OptionFootnoteID " +
                             "FROM OptionPrerequisite INNER JOIN Course " +
                             "ON Course.CourseOffering = OptionPrerequisite.CourseOffering " +
                             "AND ((CourseBeginSTRM <= '" + programBeginSTRM + "' AND (CourseEndSTRM >= '" + programBeginSTRM + "' OR CourseEndSTRM = 'Z999')) " +
                             "OR (CourseBeginSTRM >= '" + programBeginSTRM + "' AND CourseBeginSTRM <= '" + programEndSTRM + "' AND (CourseEndSTRM <= '" + programEndSTRM + "' OR CourseEndSTRM = 'Z999'))) " +
                             "LEFT OUTER JOIN OptionFootnote " +
                             "ON OptionFootnote.OptionFootnoteID = OptionPrerequisite.OptionFootnoteID " +
                             "WHERE OptionPrerequisite.OptionID = " + optionID +
                             " ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR, CourseEndSTRM ASC;";
                } else { //current program
                    //display the most current published version of the course in the program strm span
                    strSQL = "SELECT SUBJECT, CATALOG_NBR, CourseSuffix, Course.CourseOffering, COURSE_TITLE_LONG, CourseBeginSTRM, CourseEndSTRM, FootnoteNumber, OptionPrerequisiteID, OptionPrerequisite.OptionFootnoteID " +
                             "FROM OptionPrerequisite INNER JOIN Course " +
                             "ON Course.CourseOffering = OptionPrerequisite.CourseOffering " +
                             "AND ((CourseBeginSTRM <= '" + programBeginSTRM + "' AND (CourseEndSTRM >= '" + programBeginSTRM + "' OR CourseEndSTRM = 'Z999')) " +
                             "OR (CourseBeginSTRM >= '" + programBeginSTRM + "' AND CourseBeginSTRM <= '" + programEndSTRM + "' AND (CourseEndSTRM <= '" + programEndSTRM + "' OR CourseEndSTRM = 'Z999'))) " +
                             "AND (CourseBeginSTRM <= '" + currentSTRM + "' AND (CourseEndSTRM >= '" + currentSTRM + "' OR CourseEndSTRM = 'Z999')) " +
                             "LEFT OUTER JOIN OptionFootnote " +
                             "ON OptionFootnote.OptionFootnoteID = OptionPrerequisite.OptionFootnoteID " +
                             "WHERE OptionPrerequisite.OptionID = " + optionID +
                             " ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR, CourseEndSTRM ASC;";
                }

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;
                */
            }
            finally {
                objCon.Close();
            }
        }

        public DataRow[] GetOptPrereqsAndElectives(Int32 optionID, String programBeginSTRM, String programEndSTRM)
        {

            objCon = new SqlConnection(strConnection);

            try
            {
                objCon.Open();
                objCmd = new SqlCommand("usp_GetOptionPrerequisites", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.AddWithValue("@OptionID", optionID);
                objCmd.Parameters.AddWithValue("@CurrentSTRM", csTerm.GetCurrentTerm());
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);

                //retrieve Prereq Electives
                objCmd = new SqlCommand("usp_GetPrerequisiteElectives", objCon);
                DataSet objDSElectives = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.AddWithValue("@OptionID", optionID);
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDSElectives);
                DataTable dtElectives = objDSElectives.Tables[0];
                DataRow dr;

                //combine program option courses and electives in a dataset
                foreach (DataRow drElectives in dtElectives.Rows)
                {
                    dr = objDS.Tables[0].NewRow();
                    dr["CourseOffering"] = "ZZZ";
                    dr["CourseID"] = drElectives["OptionElectiveGroupID"];
                    dr["SUBJECT"] = "";
                    dr["CATALOG_NBR"] = "";
                    dr["COURSE_TITLE_LONG"] = drElectives["ElectiveGroupTitle"];
                    dr["FootnoteNumber"] = drElectives["FootnoteNumber"];
                    dr["OptionFootnoteID"] = drElectives["OptionFootnoteID"];

                    objDS.Tables[0].Rows.Add(dr);
                }

                DataRow[] drArray = objDS.Tables[0].Select("", "Quarter");
                return drArray;
            }
            finally
            {
                objCon.Close();
            }
        }
        public Int32 GetDegreeID(Int32 programDegreeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT DegreeID " +
                    "FROM ProgramDegree " +
                    "WHERE ProgramDegreeID = " + programDegreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                return Convert.ToInt32(objCmd.ExecuteScalar());

            } finally {
                objCon.Close();
            }
        }


        public Int16 AddOptPrereqs(Int32 optionID, String courseOffering, Int32 optionFootnoteID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                        "FROM OptionPrerequisite " +
                        "WHERE OptionID = " + optionID +
                        " AND CourseOffering = '" + courseOffering + "';";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0) {
                    if (optionFootnoteID != 0) {
                        strSQL = "INSERT INTO OptionPrerequisite(OptionID, CourseOffering, OptionFootnoteID) " +
                                "VALUES(" + optionID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + optionFootnoteID + ");";
                    } else {
                        strSQL = "INSERT INTO OptionPrerequisite(OptionID, CourseOffering) " +
                                "VALUES(" + optionID + ", '" + courseOffering.Replace("&nbsp;", " ") + "');";
                    }

                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr > 0) {
                        return 1;
                    } else {
                        return 0;
                    }

                } else {
                    return 2;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 EditOptPrereq(Int32 optionPrerequisiteID, Int32 optionID, String oldCourseOffering, String newCourseOffering, Int32 optionFootnoteID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                        "FROM OptionPrerequisite " +
                        "WHERE OptionPrerequisiteID <> " + optionPrerequisiteID +
                        " AND OptionID = " + optionID +
                        " AND CourseOffering = '" + newCourseOffering + "';";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0) {
                    if (optionFootnoteID != 0) {
                        strSQL = "UPDATE OptionPrerequisite " +
                                "SET CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = " + optionFootnoteID +
                                " WHERE OptionPrerequisiteID = " + optionPrerequisiteID + ";";
                    } else {
                        strSQL = "UPDATE OptionPrerequisite " +
                                "SET CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = NULL " +
                                "WHERE OptionPrerequisiteID = " + optionPrerequisiteID + ";";
                    }

                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr > 0) {
                        return 1;
                    } else {
                        return 0;
                    }

                } else {
                    return 2;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteOptPrereq(Int32 optionPrerequisiteID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE " +
                        "OptionPrerequisite " +
                        "WHERE OptionPrerequisiteID = " + optionPrerequisiteID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                Int32 intCtr = 0;
                intCtr = objCmd.ExecuteNonQuery();

                if (intCtr > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 AddOptCourse(Int32 optionID, String courseOffering, Int32 optionFootnoteID, Int16 intQuarter, byte overwriteUnits, Double overwriteUnitsMinimum, Double overwriteUnitsMaximum) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                if (intQuarter == 0) {
                    strSQL = "SELECT COUNT(*) " +
                            "FROM OptionCourse " +
                            "WHERE OptionID = " + optionID +
                            " AND Quarter IS NULL " +
                            " AND CourseOffering = '" + courseOffering + "';";
                } else {
                    strSQL = "SELECT COUNT(*) " +
                            "FROM OptionCourse " +
                            "WHERE OptionID = " + optionID +
                            " AND Quarter = " + intQuarter +
                            " AND CourseOffering = '" + courseOffering + "';";
                }

                objCmd = new SqlCommand(strSQL, objCon);

                strSQL = "";
                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0) {

                    if (optionFootnoteID != 0 && intQuarter != 0 && overwriteUnits == 1) { //modified 10/12/06 to allow variable credit overwrite
                        strSQL = "INSERT INTO OptionCourse(OptionID, CourseOffering, OptionFootnoteID, Quarter, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum) " +
                                "VALUES(" + optionID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + optionFootnoteID + ", " + intQuarter + ", " + overwriteUnits + ", " + overwriteUnitsMinimum + ", " + overwriteUnitsMaximum + ");";
                    } else if (optionFootnoteID == 0 && intQuarter != 0 && overwriteUnits == 1) {
                        strSQL = "INSERT INTO OptionCourse(OptionID, CourseOffering, Quarter, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum) " +
                                "VALUES(" + optionID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + intQuarter + ", " + overwriteUnits + ", " + overwriteUnitsMinimum + ", " + overwriteUnitsMaximum + ");";
                    } else if (optionFootnoteID != 0 && intQuarter == 0 && overwriteUnits == 1) {
                        strSQL = "INSERT INTO OptionCourse(OptionID, CourseOffering, OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum) " +
                                "VALUES(" + optionID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + optionFootnoteID + ", " + overwriteUnits + ", " + overwriteUnitsMinimum + ", " + overwriteUnitsMaximum + ");";
                    } else if (optionFootnoteID == 0 && intQuarter == 0 && overwriteUnits == 1) {
                        strSQL = "INSERT INTO OptionCourse(OptionID, CourseOffering, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum) " +
                                "VALUES(" + optionID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + overwriteUnits + ", " + overwriteUnitsMinimum + ", " + overwriteUnitsMaximum + ");";
                    } else if (optionFootnoteID != 0 && intQuarter != 0 && overwriteUnits == 0) {
                        strSQL = "INSERT INTO OptionCourse(OptionID, CourseOffering, OptionFootnoteID, Quarter, OverwriteUnits) " +
                                "VALUES(" + optionID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + optionFootnoteID + ", " + intQuarter + ", " + overwriteUnits + ");";
                    } else if (optionFootnoteID == 0 && intQuarter != 0 && overwriteUnits == 0) {
                        strSQL = "INSERT INTO OptionCourse(OptionID, CourseOffering, Quarter, OverwriteUnits) " +
                                "VALUES(" + optionID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + intQuarter + ", " + overwriteUnits + ");";
                    } else if (optionFootnoteID != 0 && intQuarter == 0 && overwriteUnits == 0) {
                        strSQL = "INSERT INTO OptionCourse(OptionID, CourseOffering, OptionFootnoteID, OverwriteUnits) " +
                                "VALUES(" + optionID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + optionFootnoteID + ", " + overwriteUnits + ");";
                    } else if (optionFootnoteID == 0 && intQuarter == 0 && overwriteUnits == 0) {
                        strSQL = "INSERT INTO OptionCourse(OptionID, CourseOffering, OverwriteUnits) " +
                                "VALUES(" + optionID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + overwriteUnits + ");";
                    }

                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr > 0) {
                        return 1;
                    } else {
                        return 0;
                    }

                } else {
                    return 2;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 AddElectGroupRef(Int32 optionID, Int32 optionElectiveGroupID, Int16 intQuarter, Double unitsMinimum, Double unitsMaximum, Int32 optionFootnoteID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                        "FROM OptionElective " +
                        "WHERE OptionID = " + optionID +
                        " AND OptionElectiveGroupID = " + optionElectiveGroupID;

                if (intQuarter == 0) {
                    strSQL += " AND Quarter IS NULL;";
                } else {
                    strSQL += " AND Quarter = " + intQuarter + ";";
                }

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0) {
                    if (optionFootnoteID != 0 && intQuarter != 0) {
                        strSQL = "INSERT INTO OptionElective(OptionElectiveGroupID, OptionID, Quarter, UnitsMinimum, UnitsMaximum, OptionFootnoteID) " +
                                "VALUES(" + optionElectiveGroupID + ", " + optionID + ", " + intQuarter + ", " + unitsMinimum + ", " + unitsMaximum + ", " + optionFootnoteID + ");";
                    } else if (optionFootnoteID == 0 && intQuarter != 0) {
                        strSQL = "INSERT INTO OptionElective(OptionElectiveGroupID, OptionID, Quarter, UnitsMinimum, UnitsMaximum) " +
                                "VALUES(" + optionElectiveGroupID + ", " + optionID + ", " + intQuarter + ", " + unitsMinimum + ", " + unitsMaximum + ");";
                    } else if (optionFootnoteID != 0 && intQuarter == 0) {
                        strSQL = "INSERT INTO OptionElective(OptionElectiveGroupID, OptionID, UnitsMinimum, UnitsMaximum, OptionFootnoteID) " +
                                "VALUES(" + optionElectiveGroupID + ", " + optionID + ", " + unitsMinimum + ", " + unitsMaximum + ", " + optionFootnoteID + ");";
                    } else if (optionFootnoteID == 0 && intQuarter == 0) {
                        strSQL = "INSERT INTO OptionElective(OptionElectiveGroupID, OptionID, UnitsMinimum, UnitsMaximum) " +
                                "VALUES(" + optionElectiveGroupID + ", " + optionID + ", " + unitsMinimum + ", " + unitsMaximum + ");";
                    }

                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr > 0) {
                        return 1;
                    } else {
                        return 0;
                    }

                } else {
                    return 2;
                }

            } finally {
                objCon.Close();
            }
        }

        
        public Int16 AddElectGroupRefForPrereq(Int32 optionID, Int32 optionElectiveGroupID,  Int32 optionFootnoteID)
        {

            objCon = new SqlConnection(strConnection);

            try
            {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                        "FROM PrerequisiteElective " +
                        "WHERE OptionID = " + optionID +
                        " AND OptionElectiveGroupID = " + optionElectiveGroupID;

                

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0)
                {
                    if (optionFootnoteID != 0)
                    {
                        strSQL = "INSERT INTO PrerequisiteElective(OptionElectiveGroupID, OptionID, OptionFootnoteID) " +
                                "VALUES(" + optionElectiveGroupID + ", " + optionID + ", " + optionFootnoteID + ");";
                    }
                    

                    objCmd = new SqlCommand(strSQL, objCon);

                    Int32 intCtr = 0;
                    intCtr = objCmd.ExecuteNonQuery();

                    if (intCtr > 0)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }

                }
                else
                {
                    return 2;
                }

            }
            finally
            {
                objCon.Close();
            }
        }
        public Int16 EditOptCourse(String strEditType, String newCourseOffering, String oldCourseOffering, String strOldOptionElectiveGroupID, Int32 optionID, Int32 degreeID, Int32 optionFootnoteID, Int16 intQuarter, Int16 intOldQuarter, Byte overwriteUnits, Double overwriteUnitsMinimum, Double overwriteUnitsMaximum) {

            objCon = new SqlConnection(strConnection);

            try {

                if (strEditType == "editCrsElect") { //if an elective reference is being changed to an option course

                    //insert the new course	
                    Int16 intReturn = AddOptCourse(optionID, newCourseOffering, optionFootnoteID, intQuarter, overwriteUnits, overwriteUnitsMinimum, overwriteUnitsMaximum);

                    //if the insert was successful
                    if (intReturn == 1) {

                        //if deleting the old elective reference is successful return 1 else return 0
                        if (DeleteElectGroupRef(optionID, degreeID, Convert.ToInt32(strOldOptionElectiveGroupID), intOldQuarter) == true) {
                            return 1;
                        } else {
                            return 0;
                        }

                    } else {
                        return intReturn; //will return 0 if insert was unsucessful or 2 if the course is a duplicate
                    }

                } else if (strEditType == "editProgCrs") { //if a course is being updated

                    objCon.Open();

                    bool blnRepeatingCourse = false;
                    if (newCourseOffering != oldCourseOffering || intOldQuarter != intQuarter) {
                        if (intQuarter == 0) {
                            strSQL = "SELECT COUNT(*) " +
                                "FROM OptionCourse " +
                                "WHERE OptionID = " + optionID +
                                " AND Quarter IS NULL " +
                                " AND CourseOffering = '" + newCourseOffering + "';";
                        } else {
                            strSQL = "SELECT COUNT(*) " +
                                "FROM OptionCourse " +
                                "WHERE OptionID = " + optionID +
                                " AND Quarter = " + intQuarter +
                                " AND CourseOffering = '" + newCourseOffering + "';";
                        }

                        objCmd = new SqlCommand(strSQL, objCon);

                        if (Convert.ToInt32(objCmd.ExecuteScalar()) != 0) {
                            blnRepeatingCourse = true;
                        }
                    }

                    if (blnRepeatingCourse == false) {

                        if (optionFootnoteID != 0 && intQuarter != 0 && overwriteUnits == 1) {
                            strSQL = "UPDATE OptionCourse " +
                                    " SET CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = " + optionFootnoteID + ", Quarter = " + intQuarter + ", OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = " + overwriteUnitsMinimum + ", OverwriteUnitsMaximum = " + overwriteUnitsMaximum +
                                    " WHERE OptionID = " + optionID +
                                    " AND CourseOffering = '" + oldCourseOffering + "'";
                        } else if (optionFootnoteID == 0 && intQuarter != 0 && overwriteUnits == 1) {
                            strSQL = "UPDATE OptionCourse " +
                                    " SET CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = NULL, Quarter = " + intQuarter + ", OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = " + overwriteUnitsMinimum + ", OverwriteUnitsMaximum = " + overwriteUnitsMaximum +
                                    " WHERE OptionID = " + optionID +
                                    " AND CourseOffering = '" + oldCourseOffering + "'";
                        } else if (optionFootnoteID != 0 && intQuarter == 0 && overwriteUnits == 1) {
                            strSQL = "UPDATE OptionCourse " +
                                    " SET CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = " + optionFootnoteID + ", Quarter = NULL, OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = " + overwriteUnitsMinimum + ", OverwriteUnitsMaximum = " + overwriteUnitsMaximum +
                                    " WHERE OptionID = " + optionID +
                                    " AND CourseOffering = '" + oldCourseOffering + "'";
                        } else if (optionFootnoteID == 0 && intQuarter == 0 && overwriteUnits == 1) {
                            strSQL = "UPDATE OptionCourse " +
                                    " SET CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = NULL, Quarter = NULL, OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = " + overwriteUnitsMinimum + ", OverwriteUnitsMaximum = " + overwriteUnitsMaximum +
                                    " WHERE OptionID = " + optionID +
                                    " AND CourseOffering = '" + oldCourseOffering + "'";
                        } else if (optionFootnoteID != 0 && intQuarter != 0 && overwriteUnits == 0) {
                            strSQL = "UPDATE OptionCourse " +
                                    "SET CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = " + optionFootnoteID + ", Quarter = " + intQuarter + ", OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = NULL, OverwriteUnitsMaximum = NULL " +
                                    "WHERE OptionID = " + optionID +
                                    " AND CourseOffering = '" + oldCourseOffering + "'";
                        } else if (optionFootnoteID == 0 && intQuarter != 0 && overwriteUnits == 0) {
                            strSQL = "UPDATE OptionCourse " +
                                    "SET CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = NULL, Quarter = " + intQuarter + ", OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = NULL, OverwriteUnitsMaximum = NULL " +
                                    "WHERE OptionID = " + optionID +
                                    " AND CourseOffering = '" + oldCourseOffering + "'";
                        } else if (optionFootnoteID != 0 && intQuarter == 0 && overwriteUnits == 0) {
                            strSQL = "UPDATE OptionCourse " +
                                    "SET CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = " + optionFootnoteID + ", Quarter = NULL, OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = NULL, OverwriteUnitsMaximum = NULL " +
                                    "WHERE OptionID = " + optionID +
                                    " AND CourseOffering = '" + oldCourseOffering + "'";
                        } else if (optionFootnoteID == 0 && intQuarter == 0 && overwriteUnits == 0) {
                            strSQL = "UPDATE OptionCourse " +
                                    "SET CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = NULL, Quarter = NULL, OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = NULL, OverwriteUnitsMaximum = NULL " +
                                    "WHERE OptionID = " + optionID +
                                    " AND CourseOffering = '" + oldCourseOffering + "'";
                        }

                        if (intOldQuarter == 0) {
                            strSQL += " AND Quarter IS NULL;";
                        } else {
                            strSQL += " AND Quarter = " + intOldQuarter + ";";
                        }

                        objCmd = new SqlCommand(strSQL, objCon);
                        if (objCmd.ExecuteNonQuery() > 0) {
                            return 1;
                        } else {
                            return 0;
                        }

                    } else {
                        return 2;
                    }

                } else {
                    return 0;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 EditElectGroupRef(String strEditType, Int32 degreeID, Int32 optionID, String strOldOptionElectiveGroupID, Int32 newOptionElectiveGroupID, String oldCourseOffering, Int16 intOldQuarter, Int16 intNewQuarter, Double unitsMinimum, Double unitsMaximum, Int32 optionFootnoteID) {

            objCon = new SqlConnection(strConnection);

            try {

                //check if changing a course to an elective reference
                if (strEditType == "editProgCrs") {

                    //insert the new elective reference
                    Int16 intReturn = AddElectGroupRef(optionID, newOptionElectiveGroupID, intNewQuarter, unitsMinimum, unitsMaximum, optionFootnoteID);

                    //if the insert was successful
                    if (intReturn == 1) {

                        //if deleting the old elective reference is successful return 1 else return 0
                        if (DeleteOptCourse(optionID, degreeID, oldCourseOffering, intOldQuarter) == true) {
                            return 1;
                        } else {
                            return 0;
                        }

                    } else {
                        return intReturn;
                    }

                } else if (strEditType == "editCrsElect") {

                    objCon.Open();

                    //if the elective reference is being changed, make sure the new elective reference doesn't already exist
                    bool blnRepeatingElective = false;
                    Int32 oldOptionElectiveGroupID = Convert.ToInt32(strOldOptionElectiveGroupID);
                    if ((newOptionElectiveGroupID != oldOptionElectiveGroupID) || (newOptionElectiveGroupID == oldOptionElectiveGroupID && intOldQuarter != intNewQuarter)) {
                        strSQL = "SELECT COUNT(*) " +
                            "FROM OptionElective " +
                            "WHERE OptionID = " + optionID +
                            " AND OptionElectiveGroupID = " + newOptionElectiveGroupID;

                        if (intNewQuarter == 0) {
                            strSQL += " AND Quarter IS NULL;";
                        } else {
                            strSQL += " AND Quarter = " + intNewQuarter + ";";
                        }

                        objCmd = new SqlCommand(strSQL, objCon);

                        if (Convert.ToInt32(objCmd.ExecuteScalar()) != 0) {
                            blnRepeatingElective = true;
                        }
                    }

                    if (blnRepeatingElective == false) {
                        if (optionFootnoteID != 0 && intNewQuarter != 0) {
                            strSQL = "UPDATE OptionElective " +
                                "SET OptionElectiveGroupID = " + newOptionElectiveGroupID + ", Quarter = " + intNewQuarter + ", UnitsMinimum = " + unitsMinimum + ", UnitsMaximum = " + unitsMaximum + ", OptionFootnoteID = " + optionFootnoteID;
                        } else if (optionFootnoteID == 0 && intNewQuarter != 0) {
                            strSQL = "UPDATE OptionElective " +
                                "SET OptionElectiveGroupID = " + newOptionElectiveGroupID + ", Quarter = " + intNewQuarter + ", UnitsMinimum = " + unitsMinimum + ", UnitsMaximum = " + unitsMaximum + ", OptionFootnoteID = NULL ";
                        } else if (optionFootnoteID != 0 && intNewQuarter == 0) {
                            strSQL = "UPDATE OptionElective " +
                                "SET OptionElectiveGroupID = " + newOptionElectiveGroupID + ", Quarter = NULL, UnitsMinimum = " + unitsMinimum + ", UnitsMaximum = " + unitsMaximum + ", OptionFootnoteID = " + optionFootnoteID;
                        } else if (optionFootnoteID == 0 && intNewQuarter == 0) {
                            strSQL = "UPDATE OptionElective " +
                                "SET OptionElectiveGroupID = " + newOptionElectiveGroupID + ", Quarter = NULL, UnitsMinimum = " + unitsMinimum + ", UnitsMaximum = " + unitsMaximum + ", OptionFootnoteID = NULL ";
                        }

                        strSQL += " WHERE OptionID = " + optionID +
                            " AND OptionElectiveGroupID = " + oldOptionElectiveGroupID;

                        if (intOldQuarter == 0) {
                            strSQL += " AND Quarter IS NULL;";
                        } else {
                            strSQL += " AND Quarter = " + intOldQuarter + ";";
                        }

                        objCmd = new SqlCommand(strSQL, objCon);

                        Int32 intCtr = 0;
                        intCtr = objCmd.ExecuteNonQuery();

                        if (intCtr > 0) {
                            return 1;
                        } else {
                            return 0;
                        }

                    } else {
                        return 2;
                    }

                } else {
                    return 0;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteOptCourse(Int32 optionID, Int32 degreeID, String courseOffering, Int16 intQuarter) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE OptionCourse " +
                        "WHERE OptionID = " + optionID +
                        " AND CourseOffering = '" + courseOffering + "'";

                if (intQuarter == 0) {
                    strSQL += " AND Quarter IS NULL;";
                } else {
                    strSQL += " AND Quarter = " + intQuarter + ";";
                }

                objCmd = new SqlCommand(strSQL, objCon);

                Int32 intCtr = 0;
                intCtr = objCmd.ExecuteNonQuery();

                if (intCtr == 0) {
                    return false;
                } else {
                    return true;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteElectGroupRef(Int32 optionID, Int32 degreeID, Int32 optionElectiveGroupID, Int16 intQuarter) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE OptionElective " +
                        "WHERE OptionID = " + optionID +
                        " AND OptionElectiveGroupID = " + optionElectiveGroupID;

                if (intQuarter == 0) {
                    strSQL += " AND Quarter IS NULL;";
                } else {
                    strSQL += " AND Quarter = " + intQuarter + ";";
                }

                objCmd = new SqlCommand(strSQL, objCon);

                Int32 intCtr = 0;
                intCtr = objCmd.ExecuteNonQuery();

                if (intCtr == 0) {
                    return false;
                } else {
                    return true;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool EditOptionValues(Int32 optionID, Int32 programVersionID, String optionDescription, String optionStateApproval, String optionCIP, String optionEPC, String academicPlan, Byte bitPrimaryOption, String gainfulEmploymentID, String totalQuarters) {

            objCon = new SqlConnection(strConnection);

            try {
                objCon.Open();

                if (totalQuarters == null || totalQuarters == "") {
                    totalQuarters = "0";
                }

                if (bitPrimaryOption == 1) {
                    //make sure no other program options are set as primary
                    strSQL = "UPDATE ProgramDegreeOption " +
                        "SET PrimaryOption = 0 " +
                        "WHERE ProgramDegreeID IN " +
                        "(SELECT ProgramDegreeID " +
                        "FROM ProgramDegree " +
                        "WHERE ProgramVersionID = " + programVersionID + ")";

                    objCmd = new SqlCommand(strSQL, objCon);
                    objCmd.ExecuteNonQuery();
                }

                strSQL = "UPDATE ProgramDegreeOption " +
                    "SET OptionDescription = '" + optionDescription.Replace("'", "''") +
                    "', OptionStateApproval = '" + optionStateApproval.Replace("'", "''") +
                    "', CIP = '" + optionCIP.Replace("'", "''") +
                    "', EPC = '" + optionEPC.Replace("'", "''") +
                    "', ACAD_PLAN = '" + academicPlan.Replace("'", "''") +
                    "', PrimaryOption = " + bitPrimaryOption +
                    ", GainfulEmploymentID = '" + gainfulEmploymentID.Trim() +
                    "', TotalQuarters = " + totalQuarters +
                    " WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public void EditOptionDesc(Int32 optionID, String optionDescription) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "UPDATE ProgramDegreeOption " +
                        "SET OptionDescription = '" + optionDescription.Replace("'", "''") +
                        "' WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objCmd.ExecuteNonQuery();

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetProgOptions(Int32 programDegreeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionTitle, OptionDescription, OptionID, OptionStateApproval, PrimaryOption, CIP, EPC, ACAD_PLAN, GainfulEmploymentID, TotalQuarters " +
                    "FROM ProgramDegreeOption " +
                    "WHERE ProgramDegreeID = " + programDegreeID +
                    " ORDER BY PrimaryOption DESC, OptionTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetPrimaryValues(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionStateApproval, CIP, EPC, ACAD_PLAN " +
                    "FROM ProgramDegreeOption " +
                    "WHERE ProgramDegreeID IN " +
                    "(SELECT ProgramDegreeID " +
                    "FROM ProgramDegree " +
                    "WHERE ProgramVersionID = " + programVersionID + ") " +
                    "AND PrimaryOption = 1;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        //Created 2/19/09 to replace GetOptionDescription
        public DataSet GetProgOptValues(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionDescription, OptionStateApproval, PrimaryOption, CIP, EPC, ACAD_PLAN, GainfulEmploymentID, TotalQuarters " +
                    "FROM ProgramDegreeOption " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public String GetOptionDescription(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionDescription " +
                    "FROM ProgramDegreeOption " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                try {
                    return objCmd.ExecuteScalar().ToString();
                } catch {
                    return "";
                }

            } finally {
                objCon.Close();
            }
        }


        public DataRow[] GetOptionCoursesAndElectives(Int32 optionID, String programBeginSTRM, String programEndSTRM) {
            objCon = new SqlConnection(strConnection);

            try
            {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetOptionCoursesAndElectives", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.AddWithValue("@BeginSTRM", programBeginSTRM);
                objCmd.Parameters.AddWithValue("@EndSTRM", programEndSTRM);
                objCmd.Parameters.AddWithValue("@OptionID", optionID);
                objCmd.Parameters.AddWithValue("@CurrentSTRM", csTerm.GetCurrentTerm());
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);

                //retrieve program option electives
                objCmd = new SqlCommand("usp_GetProgramOptionElectives", objCon);
                DataSet objDSElectives = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.AddWithValue("@OptionID", optionID);
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDSElectives);
                DataTable dtElectives = objDSElectives.Tables[0];
                DataRow dr;

                //combine program option courses and electives in a dataset
                foreach (DataRow drElectives in dtElectives.Rows)
                {
                    dr = objDS.Tables[0].NewRow();
                    dr["CourseOffering"] = "ZZZ";
                    dr["CourseID"] = drElectives["OptionElectiveGroupID"]; 
                    dr["SUBJECT"] = "";
                    dr["CATALOG_NBR"] = "";
                    //dr["CourseSuffix"] = "";
                    //dr["CourseBeginSTRM"] = "";
                    //dr["CourseEndSTRM"] = "";
                    dr["COURSE_TITLE_LONG"] = drElectives["ElectiveGroupTitle"];
                    dr["UNITS_MINIMUM"] = drElectives["UnitsMinimum"];
                    dr["UNITS_MAXIMUM"] = drElectives["UnitsMaximum"];
                    dr["Quarter"] = drElectives["Quarter"];
                    dr["FootnoteNumber"] = drElectives["FootnoteNumber"];
                    dr["OptionFootnoteID"] = drElectives["OptionFootnoteID"];
                    dr["OverwriteUnits"] = 0; //added on 10/12/06 for variable credit overwrite
                    dr["OverwriteUnitsMinimum"] = 0;
                    dr["OverwriteUnitsMaximum"] = 0;

                    objDS.Tables[0].Rows.Add(dr);
                }

                DataRow[] drArray = objDS.Tables[0].Select("", "Quarter");
                return drArray;

            }
            finally
            {
                objCon.Close();
            }
            /*
            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                String currentSTRM = csTerm.GetCurrentTerm();
                if (String.Compare(programEndSTRM, currentSTRM, true) < 0) { //archived program
                    //display the last published version of the course in the program strm span
                    strSQL = "SELECT Course.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                             "FROM OptionCourse INNER JOIN Course " +
                             "ON Course.CourseOffering = OptionCourse.CourseOffering " +
                             "AND ((CourseBeginSTRM <= '" + programBeginSTRM + "' AND (CourseEndSTRM >= '" + programBeginSTRM + "' OR CourseEndSTRM = 'Z999')) " +
                             "OR (CourseBeginSTRM >= '" + programBeginSTRM + "' AND CourseBeginSTRM <= '" + programEndSTRM + "')) " + // AND (CourseEndSTRM <= '" + programEndSTRM + "' OR CourseEndSTRM = 'Z999'))) " +
                             "LEFT OUTER JOIN OptionFootnote " +
                             "ON OptionFootnote.OptionFootnoteID = OptionCourse.OptionFootnoteID " +
                             "WHERE OptionCourse.OptionID = " + optionID +
                             " GROUP BY Course.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                             "ORDER BY Quarter, SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR, CourseEndSTRM DESC;";
                } else if (String.Compare(programBeginSTRM, currentSTRM, true) > 0) { //future program
                    //future program
                    //display the first published version of the course in the program strm span
                    strSQL = "SELECT Course.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum, COUNT(*) " +
                             "FROM OptionCourse " +
                             "INNER JOIN Course " +
                             "ON Course.CourseOffering = OptionCourse.CourseOffering " +
                             "AND ('" + programBeginSTRM + "' BETWEEN CourseBeginSTRM AND CourseEndSTRM) " +
                             "LEFT OUTER JOIN OptionFootnote " +
                             "ON OptionFootnote.OptionFootnoteID = OptionCourse.OptionFootnoteID " +
                             "WHERE OptionCourse.OptionID = " + optionID +
                             " GROUP BY Course.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                             "ORDER BY Quarter, SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR, CourseEndSTRM ASC;";

                } else { //current program
                    //display the most current published version of the course in the program strm span
                    strSQL = "SELECT Course.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                             "FROM OptionCourse INNER JOIN Course " +
                             "ON Course.CourseOffering = OptionCourse.CourseOffering " +
                             "AND ((CourseBeginSTRM <= '" + programBeginSTRM + "' AND (CourseEndSTRM >= '" + programBeginSTRM + "' OR CourseEndSTRM = 'Z999')) " +
                             "OR (CourseBeginSTRM >= '" + programBeginSTRM + "' AND CourseBeginSTRM <= '" + programEndSTRM + "' AND (CourseEndSTRM <= '" + programEndSTRM + "' OR CourseEndSTRM = 'Z999'))) " +
                             "AND (CourseBeginSTRM <= '" + currentSTRM + "' AND (CourseEndSTRM >= '" + currentSTRM + "' OR CourseEndSTRM = 'Z999')) " +
                             "LEFT OUTER JOIN OptionFootnote " +
                             "ON OptionFootnote.OptionFootnoteID = OptionCourse.OptionFootnoteID " +
                             "WHERE OptionCourse.OptionID = " + optionID +
                             " GROUP BY  Course.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                             "ORDER BY Quarter, SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR, CourseEndSTRM ASC;";
                }

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                //retrieve program option electives
                strSQL = "SELECT ElectiveGroupTitle, OptionElective.OptionElectiveGroupID, Quarter, UnitsMinimum, UnitsMaximum, FootnoteNumber, OptionElective.OptionFootnoteID " +
                    "FROM OptionElective INNER JOIN OptionElectiveGroup " +
                    "ON OptionElectiveGroup.OptionElectiveGroupID = OptionElective.OptionElectiveGroupID " +
                    "LEFT OUTER JOIN OptionFootnote " +
                    "ON OptionFootnote.OptionFootnoteID = OptionElective.OptionFootnoteID " +
                    "WHERE OptionElective.OptionID = " + optionID +
                    " ORDER BY Quarter, ElectiveGroupTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                DataSet objDSElectives = new DataSet();
                objDA.Fill(objDSElectives);

                DataRow dr;

                //combine program option courses and electives in a dataset
                for (Int32 intRowCtr = 0; intRowCtr < objDSElectives.Tables[0].Rows.Count; intRowCtr++) {
                    dr = objDS.Tables[0].NewRow();
                    dr["CourseOffering"] = "ZZZ";
                    dr["CourseID"] = objDSElectives.Tables[0].Rows[intRowCtr]["OptionElectiveGroupID"];
                    dr["SUBJECT"] = "";
                    dr["CATALOG_NBR"] = "";
                    dr["CourseSuffix"] = "";
                    dr["CourseBeginSTRM"] = "";
                    dr["CourseEndSTRM"] = "";
                    dr["COURSE_TITLE_LONG"] = objDSElectives.Tables[0].Rows[intRowCtr]["ElectiveGroupTitle"];
                    dr["UNITS_MINIMUM"] = objDSElectives.Tables[0].Rows[intRowCtr]["UnitsMinimum"];
                    dr["UNITS_MAXIMUM"] = objDSElectives.Tables[0].Rows[intRowCtr]["UnitsMaximum"];
                    dr["Quarter"] = objDSElectives.Tables[0].Rows[intRowCtr]["Quarter"];
                    dr["FootnoteNumber"] = objDSElectives.Tables[0].Rows[intRowCtr]["FootnoteNumber"];
                    dr["OptionFootnoteID"] = objDSElectives.Tables[0].Rows[intRowCtr]["OptionFootnoteID"];
                    dr["OverwriteUnits"] = 0; //added on 10/12/06 for variable credit overwrite
                    dr["OverwriteUnitsMinimum"] = 0;
                    dr["OverwriteUnitsMaximum"] = 0;

                    objDS.Tables[0].Rows.Add(dr);
                }

                DataRow[] drArray = objDS.Tables[0].Select("", "Quarter");
                return drArray;

            } finally {
                objCon.Close();
            }
            */
        }


        public DataSet GetOptionElectiveGroups(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT ElectiveGroupTitle, FootnoteNumber, OptionElectiveGroup.OptionFootnoteID, OptionElectiveGroup.OptionElectiveGroupID, COUNT(ElectiveGroupCourse.OptionElectiveGroupID) AS ElectiveCount " +
                    "FROM OptionElectiveGroup LEFT OUTER JOIN OptionFootnote " +
                    "ON OptionFootnote.OptionFootnoteID = OptionElectiveGroup.OptionFootnoteID " +
                    "LEFT OUTER JOIN ElectiveGroupCourse " +
                    "ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID " +
                    "WHERE OptionElectiveGroup.OptionID = " + optionID +
                    " GROUP BY ElectiveGroupTitle, FootnoteNumber, OptionElectiveGroup.OptionFootnoteID, OptionElectiveGroup.OptionElectiveGroupID " +
                    "ORDER BY ElectiveGroupTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetOptionElectiveGroup(Int32 optionElectiveGroupID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionElectiveGroup.OptionID, ElectiveGroupTitle, OptionElectiveGroup.OptionFootnoteID, FootnoteNumber, Footnote " +
                    "FROM OptionElectiveGroup LEFT OUTER JOIN OptionFootnote " +
                    "ON OptionFootnote.OptionFootnoteID = OptionElectiveGroup.OptionFootnoteID " +
                    "WHERE OptionElectiveGroupID = " + optionElectiveGroupID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public Int32 AddOptionElectiveGroup(Int32 optionID, String electiveGroupTitle, Int32 optionFootnoteID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                        "FROM OptionElectiveGroup " +
                        "WHERE ElectiveGroupTitle = '" + electiveGroupTitle.Replace("'", "''") +
                        "' AND OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return -1;
                } else {

                    if (optionFootnoteID == 0) {
                        strSQL = "INSERT INTO OptionElectiveGroup(OptionID, ElectiveGroupTitle) " +
                                "VALUES(" + optionID + ", '" + electiveGroupTitle.Replace("'", "''") + "') " +
                                "SELECT OptionElectiveGroupID FROM OptionElectiveGroup WHERE (OptionElectiveGroupID = SCOPE_IDENTITY());";
                    } else {
                        strSQL = "INSERT INTO OptionElectiveGroup(OptionID, ElectiveGroupTitle, OptionFootnoteID) " +
                                "VALUES(" + optionID + ", '" + electiveGroupTitle.Replace("'", "''") + "', " + optionFootnoteID + ") " +
                                "SELECT OptionElectiveGroupID FROM OptionElectiveGroup WHERE (OptionElectiveGroupID = SCOPE_IDENTITY());";
                    }

                    objCmd = new SqlCommand(strSQL, objCon);
                    return Convert.ToInt32(objCmd.ExecuteScalar()); //may need to add error handling and return 0 if an error occurs
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 EditOptionElectiveGroup(Int32 optionID, String electiveGroupTitle, Int32 optionFootnoteID, Int32 optionElectiveGroupID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                        "FROM OptionElectiveGroup " +
                        "WHERE ElectiveGroupTitle = '" + electiveGroupTitle.Replace("'", "''") +
                        "' AND OptionElectiveGroupID <> " + optionElectiveGroupID +
                        " AND OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return 2;
                } else {

                    if (optionFootnoteID == 0) {
                        strSQL = "UPDATE OptionElectiveGroup " +
                                "SET ElectiveGroupTitle = '" + electiveGroupTitle.Replace("'", "''") +
                                "', OptionFootnoteID = NULL " +
                                "WHERE OptionElectiveGroupID = " + optionElectiveGroupID + ";";
                    } else {
                        strSQL = "UPDATE OptionElectiveGroup " +
                                "SET ElectiveGroupTitle = '" + electiveGroupTitle.Replace("'", "''") +
                                "', OptionFootnoteID = " + optionFootnoteID +
                                "WHERE OptionElectiveGroupID = " + optionElectiveGroupID + ";";
                    }

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteElectGroupRefs(Int32 optionElectiveGroupID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE OptionElective " +
                        "WHERE OptionElectiveGroupID = " + optionElectiveGroupID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteElectGroupCourses(Int32 optionElectiveGroupID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE ElectiveGroupCourse " +
                        "WHERE OptionElectiveGroupID = " + optionElectiveGroupID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteOptionElectiveGroup(Int32 optionElectiveGroupID) {

            objCon = new SqlConnection(strConnection);

            try {

                DeleteElectGroupRefs(optionElectiveGroupID);
                DeleteElectGroupCourses(optionElectiveGroupID);

                strSQL = "DELETE OptionElectiveGroup " +
                        "WHERE OptionElectiveGroupID = " + optionElectiveGroupID + ";";

                objCon.Open();

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetElectiveGroupCourses(Int32 optionElectiveGroupID, String programBeginSTRM, String programEndSTRM)
        {
            objCon = new SqlConnection(strConnection);

            try
            {
                objCon.Open();
                //objCmd = new SqlCommand("usp_GetElectiveGroupCourses_2", objCon); ?Why need 2?
                objCmd = new SqlCommand("usp_GetElectiveGroupCourses", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.AddWithValue("@BeginSTRM", programBeginSTRM);
                objCmd.Parameters.AddWithValue("@EndSTRM", programEndSTRM);
                objCmd.Parameters.AddWithValue("@OptionElectiveGroupID", optionElectiveGroupID);
                objCmd.Parameters.AddWithValue("@CurrentSTRM", csTerm.GetCurrentTerm());
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            }
            finally
            {
                objCon.Close();
            }

            /*
            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                String currentSTRM = csTerm.GetCurrentTerm();
                if (String.Compare(programEndSTRM, currentSTRM, true) < 0) { //archived program
                    //display the last published version of elective group courses in the program strm span
                    strSQL = "SELECT OptionElectiveGroupID, ElectiveGroupCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber, ElectiveGroupCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                        "FROM ElectiveGroupCourse INNER JOIN Course " +
                        "ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering " +
                        "AND ((CourseBeginSTRM <= '" + programBeginSTRM + "' AND (CourseEndSTRM >= '" + programBeginSTRM + "' OR CourseEndSTRM = 'Z999')) " +
                        "OR (CourseBeginSTRM >= '" + programBeginSTRM + "' AND CourseBeginSTRM <= '" + programEndSTRM + "' AND (CourseEndSTRM <= '" + programEndSTRM + "' OR CourseEndSTRM = 'Z999'))) " +
                        "LEFT OUTER JOIN OptionFootnote " +
                        "ON OptionFootnote.OptionFootnoteID = ElectiveGroupCourse.OptionFootnoteID " +
                        "WHERE OptionElectiveGroupID = " + optionElectiveGroupID +
                        " ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;";
                } else if (String.Compare(programBeginSTRM, currentSTRM, true) > 0) { //future program
                    //display the first published version of the elective group courses in the program strm span
                    strSQL = "SELECT OptionElectiveGroupID, ElectiveGroupCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber, ElectiveGroupCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                        "FROM ElectiveGroupCourse INNER JOIN Course " +
                        "ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering " +
                        "AND ((CourseBeginSTRM <= '" + programBeginSTRM + "' AND (CourseEndSTRM >= '" + programBeginSTRM + "' OR CourseEndSTRM = 'Z999')) " +
                        "OR (CourseBeginSTRM >= '" + programBeginSTRM + "' AND CourseBeginSTRM <= '" + programEndSTRM + "' AND (CourseEndSTRM <= '" + programEndSTRM + "' OR CourseEndSTRM = 'Z999'))) " +
                        "LEFT OUTER JOIN OptionFootnote " +
                        "ON OptionFootnote.OptionFootnoteID = ElectiveGroupCourse.OptionFootnoteID " +
                        "WHERE OptionElectiveGroupID = " + optionElectiveGroupID +
                        " ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;";
                } else { //current program
                    //display the most current published version of elective group courses in the program strm span
                    strSQL = "SELECT OptionElectiveGroupID, ElectiveGroupCourse.CourseOffering, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber, ElectiveGroupCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                        "FROM ElectiveGroupCourse INNER JOIN Course " +
                        "ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering " +
                        "AND ((CourseBeginSTRM <= '" + programBeginSTRM + "' AND (CourseEndSTRM >= '" + programBeginSTRM + "' OR CourseEndSTRM = 'Z999')) " +
                        "OR (CourseBeginSTRM >= '" + programBeginSTRM + "' AND CourseBeginSTRM <= '" + programEndSTRM + "' AND (CourseEndSTRM <= '" + programEndSTRM + "' OR CourseEndSTRM = 'Z999'))) " +
                        "AND (CourseBeginSTRM <= '" + currentSTRM + "' AND (CourseEndSTRM >= '" + currentSTRM + "' OR CourseEndSTRM = 'Z999')) " +
                        "LEFT OUTER JOIN OptionFootnote " +
                        "ON OptionFootnote.OptionFootnoteID = ElectiveGroupCourse.OptionFootnoteID " +
                        "WHERE OptionElectiveGroupID = " + optionElectiveGroupID +
                        " ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;";
                }

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
            */
        }


        public Int16 AddElectiveGroupCourse(Int32 optionElectiveGroupID, String courseOffering, Int32 optionFootnoteID, byte overwriteUnits, Double overwriteUnitsMinimum, Double overwriteUnitsMaximum) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM ElectiveGroupCourse " +
                    "WHERE OptionElectiveGroupID = " + optionElectiveGroupID +
                    " AND CourseOffering = '" + courseOffering + "';";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {

                    return 2;

                } else {

                    if (optionFootnoteID == 0 && overwriteUnits == 1) {
                        strSQL = "INSERT INTO ElectiveGroupCourse(OptionElectiveGroupID, CourseOffering, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum) " +
                                "VALUES(" + optionElectiveGroupID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + overwriteUnits + ", " + overwriteUnitsMinimum + ", " + overwriteUnitsMaximum + ");";
                    } else if (optionFootnoteID == 0 && overwriteUnits == 0) {
                        strSQL = "INSERT INTO ElectiveGroupCourse(OptionElectiveGroupID, CourseOffering, OverwriteUnits) " +
                                "VALUES(" + optionElectiveGroupID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + overwriteUnits + ");";
                    } else if (optionFootnoteID != 0 && overwriteUnits == 1) {
                        strSQL = "INSERT INTO ElectiveGroupCourse(OptionElectiveGroupID, CourseOffering, OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum) " +
                                "VALUES(" + optionElectiveGroupID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + optionFootnoteID + ", " + overwriteUnits + ", " + overwriteUnitsMinimum + ", " + overwriteUnitsMaximum + ");";
                    } else if (optionFootnoteID != 0 && overwriteUnits == 0) {
                        strSQL = "INSERT INTO ElectiveGroupCourse(OptionElectiveGroupID, CourseOffering, OptionFootnoteID, OverwriteUnits) " +
                                "VALUES(" + optionElectiveGroupID + ", '" + courseOffering.Replace("&nbsp;", " ") + "', " + optionFootnoteID + ", " + overwriteUnits + ");";
                    }

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public Int16 EditElectiveGroupCourse(Int32 oldOptionElectiveGroupID, Int32 newOptionElectiveGroupID, String oldCourseOffering, String newCourseOffering, Int32 optionFootnoteID, byte overwriteUnits, Double overwriteUnitsMinimum, Double overwriteUnitsMaximum) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                bool blnRepeatingElective = false;

                if (oldOptionElectiveGroupID != newOptionElectiveGroupID || oldCourseOffering != newCourseOffering) {
                    strSQL = "SELECT COUNT(*) " +
                        "FROM ElectiveGroupCourse " +
                        "WHERE CourseOffering = '" + newCourseOffering +
                        "' AND OptionElectiveGroupID = " + newOptionElectiveGroupID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                        blnRepeatingElective = true;
                    }
                }

                if (blnRepeatingElective) {
                    return 2;
                } else {

                    if (optionFootnoteID == 0 && overwriteUnits == 1) {
                        strSQL = "UPDATE ElectiveGroupCourse " +
                                "SET OptionElectiveGroupID = " + newOptionElectiveGroupID + ", CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = NULL, OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = " + overwriteUnitsMinimum + ", OverwriteUnitsMaximum = " + overwriteUnitsMaximum +
                                " WHERE OptionElectiveGroupID = " + oldOptionElectiveGroupID +
                                " AND CourseOffering = '" + oldCourseOffering.Replace("&nbsp;", " ") + "';";
                    } else if (optionFootnoteID != 0 && overwriteUnits == 1) {
                        strSQL = "UPDATE ElectiveGroupCourse " +
                                "SET OptionElectiveGroupID = " + newOptionElectiveGroupID + ", CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = " + optionFootnoteID + ", OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = " + overwriteUnitsMinimum + ", OverwriteUnitsMaximum = " + overwriteUnitsMaximum +
                                " WHERE OptionElectiveGroupID = " + oldOptionElectiveGroupID +
                                " AND CourseOffering = '" + oldCourseOffering.Replace("&nbsp;", " ") + "';";
                    } else if (optionFootnoteID == 0 && overwriteUnits == 0) {
                        strSQL = "UPDATE ElectiveGroupCourse " +
                                "SET OptionElectiveGroupID = " + newOptionElectiveGroupID + ", CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = NULL, OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = NULL, OverwriteUnitsMaximum = NULL " +
                                "WHERE OptionElectiveGroupID = " + oldOptionElectiveGroupID +
                                " AND CourseOffering = '" + oldCourseOffering.Replace("&nbsp;", " ") + "';";
                    } else if (optionFootnoteID != 0 && overwriteUnits == 0) {
                        strSQL = "UPDATE ElectiveGroupCourse " +
                                "SET OptionElectiveGroupID = " + newOptionElectiveGroupID + ", CourseOffering = '" + newCourseOffering.Replace("&nbsp;", " ") + "', OptionFootnoteID = " + optionFootnoteID + ", OverwriteUnits = " + overwriteUnits + ", OverwriteUnitsMinimum = NULL, OverwriteUnitsMaximum = NULL " +
                                "WHERE OptionElectiveGroupID = " + oldOptionElectiveGroupID +
                                " AND CourseOffering = '" + oldCourseOffering.Replace("&nbsp;", " ") + "';";
                    }

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteElectiveGroupCourse(Int32 optionElectiveGroupID, String courseOffering) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE " +
                        "ElectiveGroupCourse " +
                        "WHERE OptionElectiveGroupID = " + optionElectiveGroupID +
                        " AND CourseOffering = '" + courseOffering.Replace("&nbsp;", " ") + "';";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetOptFootnotes(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionFootnoteID, FootnoteNumber, Footnote " +
                    "FROM OptionFootnote " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public String GetFootnote(Int32 optionFootnoteID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT Footnote " +
                    "FROM OptionFootnote " +
                    "WHERE OptionFootnoteID = " + optionFootnoteID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                try {
                    return objCmd.ExecuteScalar().ToString();
                } catch {
                    return "";
                }

            } finally {
                objCon.Close();
            }
        }


        public Int32 GetOptionFootnoteID(Int32 optionID, Int16 footnoteNumber) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionFootnoteID " +
                    "FROM OptionFootnote " +
                    "WHERE OptionID = " + optionID +
                    " AND FootnoteNumber = " + footnoteNumber + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                try {
                    return Convert.ToInt32(objCmd.ExecuteScalar());
                } catch {
                    return 0;
                }

            } finally {
                objCon.Close();
            }
        }


        public Int32 GetOptionElectiveGroupID(Int32 optionID, String electiveGroupTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionElectiveGroupID " +
                    "FROM OptionElectiveGroup " +
                    "WHERE OptionID = " + optionID +
                    " AND ElectiveGroupTitle = '" + electiveGroupTitle.Replace("'", "''") + "';";

                objCmd = new SqlCommand(strSQL, objCon);

                try {
                    return Convert.ToInt32(objCmd.ExecuteScalar());
                } catch {
                    return 0;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteProgram(Int32 programVersionID) {

            SqlConnection objCon2 = new SqlConnection(strConnection);

            try { //ADDED 10/5/06

                objCon2.Open();

                strSQL = "SELECT DegreeID " +
                    "FROM ProgramDegree " +
                    "WHERE ProgramVersionID = " + programVersionID +
                    " GROUP BY DegreeID;";

                objCmd = new SqlCommand(strSQL, objCon2);
                objDA = new SqlDataAdapter(objCmd);
                DataSet dsDegrees = new DataSet();
                objDA.Fill(dsDegrees);

                for (Int32 intDSRow = 0; intDSRow < dsDegrees.Tables[0].Rows.Count; intDSRow++) {
                    Int32 degreeID = Convert.ToInt32(dsDegrees.Tables[0].Rows[intDSRow]["DegreeID"]);
                    //Delete Program Options, Prerequisites, Courses and Degrees 
                    DeleteProgDeg(degreeID, programVersionID);
                }

                //delete Program CPG Data
                DeleteCareerPlanningGuide(programVersionID, objCon2);

                //delete Program Fees
                DeleteProgramFees(programVersionID, objCon2);

                //delete Program Categories
                DeleteProgramCategories(programVersionID);

                //delete Program Colleges Program
                if (DeleteProgram(programVersionID, objCon2) == true) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon2.Close();
            }
        }


        public bool DeleteProgDeg(Int32 degreeID, Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                //get the ProgramDegreeID to use in delete statements below from the other tables
                strSQL = "SELECT ProgramDegreeID FROM ProgramDegree " +
                    "WHERE DegreeID = " + degreeID +
                    " AND ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                String strSQL2 = "";
                for (Int32 i = 0; i < objDS.Tables[0].Rows.Count; i++) {
                    Int32 programDegreeID = Convert.ToInt32(objDS.Tables[0].Rows[i]["ProgramDegreeID"]);

                    //delete the program options for the degree
                    DeleteProgDegOptions(programDegreeID, objCon);

                    //write query to delete data from ProgramDegree
                    strSQL2 += " DELETE ProgramDegree " +
                        "WHERE ProgramDegreeID = " + programDegreeID + ";";
                }

                if (strSQL2 != "") {
                    objCmd = new SqlCommand(strSQL2, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteProgDeg(Int32 degreeID, Int32 programVersionID, SqlConnection objCon) {

            try {

                //get the ProgramDegreeID to use in delete statements below from the other tables
                strSQL = "SELECT ProgramDegreeID FROM ProgramDegree " +
                    "WHERE DegreeID = " + degreeID +
                    " AND ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                String strSQL2 = "";
                for (Int32 i = 0; i < objDS.Tables[0].Rows.Count; i++) {
                    Int32 programDegreeID = Convert.ToInt32(objDS.Tables[0].Rows[i]["ProgramDegreeID"]);

                    //delete the program options for the degree
                    DeleteProgDegOptions(programDegreeID, objCon);

                    //write query to delete data from ProgramDegree
                    strSQL2 += " DELETE ProgramDegree " +
                        "WHERE ProgramDegreeID = " + programDegreeID + ";";
                }

                if (strSQL2 != "") {
                    objCmd = new SqlCommand(strSQL2, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }

            } catch {
                return false;
            }
        }


        public bool DeleteOptPrereqs(Int32 optionID, SqlConnection objCon) {

            try {

                strSQL = "DELETE OptionPrerequisite " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch {
                return false;
            }
        }


        public void DeleteProgOptions(Int32 degreeID, Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {
                objCon.Open();

                //get the ProgramDegreeID to use in delete statements below from the other tables
                strSQL = "SELECT ProgramDegreeID FROM ProgramDegree " +
                    "WHERE DegreeID = " + degreeID +
                    " AND ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                for (Int32 i = 0; i < objDS.Tables[0].Rows.Count; i++) {
                    Int32 programDegreeID = Convert.ToInt32(objDS.Tables[0].Rows[i]["ProgramDegreeID"]);

                    //delete the program options for the degree
                    DeleteProgDegOptions(programDegreeID, objCon);
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteProgDegOptions(Int32 programDegreeID, SqlConnection objCon) {

            try {

                strSQL = "SELECT OptionID " +
                    "FROM ProgramDegreeOption " +
                    "WHERE ProgramDegreeID = " + programDegreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                DataSet objTempDS = new DataSet();
                objDA.Fill(objTempDS);

                for (Int32 j = 0; j < objTempDS.Tables[0].Rows.Count; j++) {
                    Int32 optionID = Convert.ToInt32(objTempDS.Tables[0].Rows[j]["OptionID"]);

                    //delete the option prerequisites
                    DeleteOptPrereqs(optionID, objCon);

                    //delete OptElectives
                    DeleteProgOptElectives(optionID, objCon);

                    //delete ProgOptCourses
                    DeleteProgOptCourses(optionID, objCon);

                    //delete Elective Groups
                    DeleteOptionElectives(optionID, objCon);

                    //delete ProgOptFootnotes
                    DeleteProgOptFootnotes(optionID, objCon);

                    //delete OptMCodes
                    DeleteOptionMCodes(optionID, objCon);

                    //delete Option Locations
                    DeleteOptionLocations(optionID, objCon);
                }

                //delete ProgDegOptions
                strSQL = "DELETE ProgramDegreeOption " +
                    "WHERE ProgramDegreeID = " + programDegreeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch {
                return false;
            }
        }


        public bool DeleteProgOptElectives(Int32 optionID, SqlConnection objCon) {

            try {

                strSQL = "DELETE " +
                    "OptionElective " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch {
                return false;
            }
        }


        public bool DeleteProgOptCourses(Int32 optionID, SqlConnection objCon) {

            try {

                strSQL = "DELETE " +
                    "OptionCourse " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch {
                return false;
            }

        }


        public bool DeleteOptionElectives(Int32 optionID, SqlConnection objCon) {

            try {

                strSQL = "SELECT OptionElectiveGroupID " +
                        "FROM OptionElectiveGroup " +
                        "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                DataSet dsOptionElectiveGroups = new DataSet();
                objDA.Fill(dsOptionElectiveGroups);

                String strSQL2 = "";
                for (Int32 intDSRow = 0; intDSRow < dsOptionElectiveGroups.Tables[0].Rows.Count; intDSRow++) {
                    Int32 optionElectiveGroupID = Convert.ToInt32(dsOptionElectiveGroups.Tables[0].Rows[intDSRow]["OptionElectiveGroupID"]);

                    strSQL2 += " DELETE ElectiveGroupCourse " +
                            "WHERE OptionElectiveGroupID = " + optionElectiveGroupID + ";";

                    strSQL2 += " DELETE OptionElectiveGroup " +
                            "WHERE OptionElectiveGroupID = " + optionElectiveGroupID + ";";
                }

                if (strSQL2 != "") {
                    objCmd = new SqlCommand(strSQL2, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }

            } catch {
                return false;
            }
        }


        public bool DeleteOptionElectives(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionElectiveGroupID " +
                        "FROM OptionElectiveGroup " +
                        "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                DataSet dsOptionElectiveGroups = new DataSet();
                objDA.Fill(dsOptionElectiveGroups);

                String strSQL2 = "";
                for (Int32 intDSRow = 0; intDSRow < dsOptionElectiveGroups.Tables[0].Rows.Count; intDSRow++) {
                    Int32 optionElectiveGroupID = Convert.ToInt32(dsOptionElectiveGroups.Tables[0].Rows[intDSRow]["OptionElectiveGroupID"]);

                    strSQL2 += " DELETE ElectiveGroupCourse " +
                            "WHERE OptionElectiveGroupID = " + optionElectiveGroupID + ";";

                    strSQL2 += " DELETE OptionElectiveGroup " +
                            "WHERE OptionElectiveGroupID = " + optionElectiveGroupID + ";";
                }

                if (strSQL2 != "") {
                    objCmd = new SqlCommand(strSQL2, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }

            } catch {
                return false;
            } finally {
                objCon.Close();
            }
        }


        public bool DeleteProgOptFootnotes(Int32 optionID, SqlConnection objCon) {

            try {

                strSQL = "DELETE OptionFootnote " +
                        "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch {
                return false;
            }
        }


        public bool DeleteProgOptFootnotes(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE OptionFootnote " +
                        "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch {
                return false;
            } finally {
                objCon.Close();
            }
        }


        public bool DeleteCareerPlanningGuide(Int32 programVersionID, SqlConnection objCon) {

            try {

                strSQL = "DELETE CareerPlanningGuide " +
                        "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch {
                return false;
            }
        }


        public bool DeleteProgramFees(Int32 programVersionID, SqlConnection objCon) {

            try {

                strSQL = "DELETE ProgramFee " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch {
                return false;
            }
        }


        public bool DeleteProgram(Int32 programVersionID, SqlConnection objCon) {

            try {

                strSQL = "DELETE Program " +
                        "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch {
                return false;
            }
        }


        public Int16 GetMaxFootnoteNumber(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT MAX(FootnoteNumber) " +
                    "FROM OptionFootnote " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                try {
                    return Convert.ToInt16(objCmd.ExecuteScalar());
                } catch {
                    return 0;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool AddOptFootnote(Int32 optionID, Int16 footnoteNumber, String footnote) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "INSERT INTO OptionFootnote(OptionID, FootnoteNumber, Footnote) " +
                        "VALUES(" + optionID + ", " + footnoteNumber + ", '" + footnote.Replace("'", "''") + "');";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool EditFootnote(Int32 optionFootnoteID, String footnote) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "UPDATE OptionFootnote " +
                        "SET Footnote = '" + footnote.Replace("'", "''") +
                        "' WHERE OptionFootnoteID = " + optionFootnoteID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteFootnote(Int32 optionID, Int32 optionFootnoteID, Int16 footnoteNumber) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "UPDATE OptionPrerequisite " +
                        "SET OptionFootnoteID = NULL " +
                        "WHERE OptionFootnoteID = " + optionFootnoteID + ";";

                strSQL += " UPDATE OptionCourse " +
                        "SET OptionFootnoteID = NULL " +
                        "WHERE OptionFootnoteID = " + optionFootnoteID + ";";

                strSQL += " UPDATE OptionElective " +
                        "SET OptionFootnoteID = NULL " +
                        "WHERE OptionFootnoteID = " + optionFootnoteID + ";";

                strSQL += " UPDATE ElectiveGroupCourse " +
                        "SET OptionFootnoteID = NULL " +
                        "WHERE OptionFootnoteID = " + optionFootnoteID + ";";

                strSQL += " UPDATE OptionElectiveGroup " +
                        "SET OptionFootnoteID = NULL " +
                        "WHERE OptionFootnoteID = " + optionFootnoteID + ";";

                strSQL += " DELETE OptionFootnote " +
                        "WHERE OptionFootnoteID = " + optionFootnoteID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    strSQL = "SELECT OptionFootnoteID, FootnoteNumber " +
                            "FROM OptionFootnote " +
                            "WHERE FootnoteNumber > " + footnoteNumber +
                            " AND OptionID = " + optionID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);
                    objDA = new SqlDataAdapter(objCmd);
                    objDS = new DataSet();
                    objDA.Fill(objDS);

                    String strSQL2 = "";
                    for (Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++) {
                        strSQL2 += " UPDATE OptionFootnote " +
                                "SET FootnoteNumber = " + (Convert.ToInt16(objDS.Tables[0].Rows[intDSRow]["FootnoteNumber"]) - 1) +
                                "WHERE OptionFootnoteID = " + objDS.Tables[0].Rows[intDSRow]["OptionFootnoteID"].ToString() + ";";
                    }

                    if (strSQL2 != "") {
                        objCmd = new SqlCommand(strSQL2, objCon);
                        if (objCmd.ExecuteNonQuery() > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetCrsCredits(Int32 courseID) { //added 10/12/06 for variable credit overwrite

            objCon = new SqlConnection(strConnection);

            try {
                objCon.Open();

                strSQL = "SELECT VariableUnits, UNITS_MINIMUM, UNITS_MAXIMUM " +
                    "FROM Course " +
                    "WHERE CourseID = " + courseID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetOverwriteUnits(Int32 optionID, String courseOffering, String programBeginSTRM, String programEndSTRM) { //added 10/17/06 for variable credit overwrite

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                String currentSTRM = csTerm.GetCurrentTerm();
                if (String.Compare(programEndSTRM, currentSTRM, true) < 0) { //archived program
                    strSQL = "SELECT OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                        "FROM OptionCourse " +
                        "INNER JOIN Course " +
                        "ON Course.CourseOffering = OptionCourse.CourseOffering " +
                        "AND ((CourseBeginSTRM <= '" + programBeginSTRM +
                        "' AND (CourseEndSTRM >= '" + programBeginSTRM +
                        "' OR CourseEndSTRM = 'Z999')) " +
                        "OR (CourseBeginSTRM >= '" + programBeginSTRM +
                        "' AND CourseBeginSTRM <= '" + programEndSTRM +
                        "' AND (CourseEndSTRM <= '" + programEndSTRM +
                        "' OR CourseEndSTRM = 'Z999'))) " +
                        "WHERE OptionID = " + optionID +
                        " AND OptionCourse.CourseOffering = '" + courseOffering + "';";
                } else if (String.Compare(programBeginSTRM, currentSTRM, true) > 0) { //future program
                    strSQL = "SELECT OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                        "FROM OptionCourse " +
                        "INNER JOIN Course " +
                        "ON Course.CourseOffering = OptionCourse.CourseOffering " +
                        "AND ((CourseBeginSTRM <= '" + programBeginSTRM +
                        "' AND (CourseEndSTRM >= '" + programBeginSTRM +
                        "' OR CourseEndSTRM = 'Z999')) " +
                        "OR (CourseBeginSTRM >= '" + programBeginSTRM +
                        "' AND CourseBeginSTRM <= '" + programEndSTRM +
                        "' AND (CourseEndSTRM <= '" + programEndSTRM +
                        "' OR CourseEndSTRM = 'Z999'))) " +
                        "WHERE OptionID = " + optionID +
                        " AND OptionCourse.CourseOffering = '" + courseOffering + "';";
                } else { //current program
                    strSQL = "SELECT OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                        "FROM OptionCourse " +
                        "INNER JOIN Course " +
                        "ON Course.CourseOffering = OptionCourse.CourseOffering " +
                        "AND ((CourseBeginSTRM <= '" + programBeginSTRM +
                        "' AND (CourseEndSTRM >= '" + programBeginSTRM +
                        "' OR CourseEndSTRM = 'Z999')) " +
                        "OR (CourseBeginSTRM >= '" + programBeginSTRM +
                        "' AND CourseBeginSTRM <= '" + programEndSTRM +
                        "' AND (CourseEndSTRM <= '" + programEndSTRM +
                        "' OR CourseEndSTRM = 'Z999'))) " +
                        "AND (CourseBeginSTRM <= '" + currentSTRM +
                        "' AND (CourseEndSTRM >= '" + currentSTRM +
                        "' OR CourseEndSTRM = 'Z999')) " +
                        "WHERE OptionID = " + optionID +
                        " AND OptionCourse.CourseOffering = '" + courseOffering + "';";
                }

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetElectiveOverwriteUnits(Int32 optionElectiveGroupID, String courseOffering, String programBeginSTRM, String programEndSTRM) { //added 10/24/06 for variable credit overwrite

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                String currentSTRM = csTerm.GetCurrentTerm();
                if (String.Compare(programEndSTRM, currentSTRM, true) < 0) { //archived program
                    strSQL = "SELECT OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                        "FROM ElectiveGroupCourse " +
                        "INNER JOIN Course " +
                        "ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering " +
                        "AND ((CourseBeginSTRM <= '" + programBeginSTRM +
                        "' AND (CourseEndSTRM >= '" + programBeginSTRM +
                        "' OR CourseEndSTRM = 'Z999')) " +
                        "OR (CourseBeginSTRM >= '" + programBeginSTRM +
                        "' AND CourseBeginSTRM <= '" + programEndSTRM +
                        "' AND (CourseEndSTRM <= '" + programEndSTRM +
                        "' OR CourseEndSTRM = 'Z999'))) " +
                        "WHERE OptionElectiveGroupID = " + optionElectiveGroupID +
                        " AND ElectiveGroupCourse.CourseOffering = '" + courseOffering + "';";
                } else if (String.Compare(programBeginSTRM, currentSTRM, true) > 0) { //future program
                    strSQL = "SELECT OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                        "FROM ElectiveGroupCourse " +
                        "INNER JOIN Course " +
                        "ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering " +
                        "AND ((CourseBeginSTRM <= '" + programBeginSTRM +
                        "' AND (CourseEndSTRM >= '" + programBeginSTRM +
                        "' OR CourseEndSTRM = 'Z999')) " +
                        "OR (CourseBeginSTRM >= '" + programBeginSTRM +
                        "' AND CourseBeginSTRM <= '" + programEndSTRM +
                        "' AND (CourseEndSTRM <= '" + programEndSTRM +
                        "' OR CourseEndSTRM = 'Z999'))) " +
                        "WHERE OptionElectiveGroupID = " + optionElectiveGroupID +
                        " AND ElectiveGroupCourse.CourseOffering = '" + courseOffering + "';";
                } else { //current program
                    strSQL = "SELECT OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum " +
                        "FROM ElectiveGroupCourse " +
                        "INNER JOIN Course " +
                        "ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering " +
                        "AND ((CourseBeginSTRM <= '" + programBeginSTRM +
                        "' AND (CourseEndSTRM >= '" + programBeginSTRM +
                        "' OR CourseEndSTRM = 'Z999')) " +
                        "OR (CourseBeginSTRM >= '" + programBeginSTRM +
                        "' AND CourseBeginSTRM <= '" + programEndSTRM +
                        "' AND (CourseEndSTRM <= '" + programEndSTRM +
                        "' OR CourseEndSTRM = 'Z999'))) " +
                        "AND (CourseBeginSTRM <= '" + currentSTRM +
                        "' AND (CourseEndSTRM >= '" + currentSTRM +
                        "' OR CourseEndSTRM = 'Z999')) " +
                        "WHERE OptionElectiveGroupID = " + optionElectiveGroupID +
                        " AND ElectiveGroupCourse.CourseOffering = '" + courseOffering + "';";
                }
                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetProgramFees(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT * FROM ProgramFee " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public bool AddProgramFees(Int32 programVersionID, String strBookMinimum, String strBookMaximum, String strSuppliesMinimum, String strSuppliesMaximum, String strMiscMinimum, String strMiscMaximum, String note, SqlConnection objCon) {

            try {

                if (note != "") {
                    strSQL = "INSERT INTO ProgramFee(ProgramVersionID, BookMinimum, BookMaximum, SuppliesMinimum, SuppliesMaximum, MiscMinimum, MiscMaximum, Note) " +
                        "VALUES(" + programVersionID + ", " + strBookMinimum + ", " + strBookMaximum + ", " + strSuppliesMinimum + ", " + strSuppliesMaximum + ", " + strMiscMinimum + ", " + strMiscMaximum + ",'" + note.Replace("'", "''") + "');";
                } else {
                    strSQL = "INSERT INTO ProgramFee(ProgramVersionID, BookMinimum, BookMaximum, SuppliesMinimum, SuppliesMaximum, MiscMinimum, MiscMaximum) " +
                        "VALUES(" + programVersionID + ", " + strBookMinimum + ", " + strBookMaximum + ", " + strSuppliesMinimum + ", " + strSuppliesMaximum + ", " + strMiscMinimum + ", " + strMiscMaximum + ");";
                }
                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool EditProgramFees(Int32 programVersionID, String strBookMinimum, String strBookMaximum, String strSuppliesMinimum, String strSuppliesMaximum, String strMiscMinimum, String strMiscMaximum, String note) {

            if (strBookMinimum == "" || strBookMaximum == "") {
                strBookMinimum = "NULL";
                strBookMaximum = "NULL";
            }

            if (strSuppliesMinimum == "" || strSuppliesMaximum == "") {
                strSuppliesMinimum = "NULL";
                strSuppliesMaximum = "NULL";
            }

            if (strMiscMinimum == "" || strMiscMaximum == "") {
                strMiscMinimum = "NULL";
                strMiscMaximum = "NULL";
            }

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM ProgramFee " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0) {
                    return AddProgramFees(programVersionID, strBookMinimum, strBookMaximum, strSuppliesMinimum, strSuppliesMaximum, strMiscMinimum, strMiscMaximum, note, objCon);
                } else {

                    if (note != "") {
                        note = "'" + note.Replace("'", "''") + "'";
                    } else {
                        note = "NULL";
                    }

                    strSQL = "UPDATE ProgramFee " +
                        "SET BookMinimum = " + strBookMinimum + ",BookMaximum = " + strBookMaximum + ",SuppliesMinimum = " + strSuppliesMinimum + ",SuppliesMaximum = " + strSuppliesMaximum + ",MiscMinimum = " + strMiscMinimum + ",MiscMaximum = " + strMiscMaximum + ",Note=" + note +
                        " WHERE ProgramVersionID = " + programVersionID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);
                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteProgramFees(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE ProgramFee " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        /// <summary>
        /// Gets all Terms/Quarters after Winter 2007 from the ctcLink ODS
        /// </summary>
        /// <returns>
        /// DataSet 
        /// Field List: STRM (Example: 2173)
        ///             DESCR (Example: SPRING 2017)
        /// </returns>
        public DataSet GetTermsForPrograms() {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetTerms", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = "2071";
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }


        /**************************************** GetSubjectsForPrograms ******************************************
         * INPUT: 
         * String programBeginSTRM - Program Beginning Year/Quarter
         * String programEndSTRM - Program Ending Year/Quarter
         * 
         * OUTPUT: DateSet
         * FIELD LIST: SUBJECT - Course Department
         *             CrsCount - Course Count
         * 
         * USED IN: /iCatalog/program/popups/addelective.aspx.cs
         *			/iCatalog/program/popups/addoptcrs.aspx.cs
         *			/iCatalog/program/popups/addprereq.aspx.cs
         *			/iCatalog/program/popups/editelective.aspx.cs
         *			/iCatalog/program/popups/editoptcrs.aspx.cs
         *			/iCatalog/program/popups/editprereq.aspx.cs
         * 
         * DESCRIPTION: Gets Course Departments for current and future courses.
         * 
         *******************************************************************************************************/
        public DataSet GetSubjectsForPrograms(String programBeginSTRM, String programEndSTRM)
        {
            objCon = new SqlConnection(strConnection);

            try
            {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCourseSubjects", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.AddWithValue("@EndSTRM", programEndSTRM);
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;
            }
            finally
            {
                objCon.Close();
            }
            /*
            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT SubjectArea.SUBJECT, COUNT(CourseID) AS CrsCount " +
                         "FROM SubjectArea " +
                         "INNER JOIN Course " +
                         "ON Course.SUBJECT = SubjectArea.SUBJECT " +
                         "WHERE ((CourseBeginSTRM <= '" + programBeginSTRM +
                         "' AND (CourseEndSTRM >= '" + programBeginSTRM +
                         "' OR CourseEndSTRM = 'Z999')) " +
                         "OR (CourseBeginSTRM >= '" + programBeginSTRM +
                         "' AND CourseBeginSTRM <= '" + programEndSTRM +
                         "' AND (CourseEndSTRM <= '" + programEndSTRM +
                         "' OR CourseEndSTRM = 'Z999'))) " +
                         "AND PublishedCourse = '1' " +
                         "GROUP BY SubjectArea.SUBJECT " +
                         "ORDER BY SubjectArea.SUBJECT;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
            */
        }


        /******************************************** GetCourseOfferings ***********************************************
         * INPUT:
         * courseSubject - Course Department
         * programBeginSTRM - Program Beggining Year/Quarter
         * programEndSTRM - Program Ending Year/Quarter
         * 
         * OUTPUT: DataSet
         * FIELD LIST: CourseOffering - Course ID
         *			   CATALOG_NBR - Course Number
         * 
         * USED IN: /iCatalog/program/popups/addelective.aspx.cs
         *			/iCatalog/program/popups/addoptcrs.aspx.cs
         *			/iCatalog/program/popups/addprereq.aspx.cs
         *			/iCatalog/program/popups/editelective.aspx.cs
         *			/iCatalog/program/popups/editoptcrs.aspx.cs
         *			/iCatalog/program/popups/editprereq.aspx.cs
         * 
         * DESCRIPTION: Gets Course IDs and Course Numbers for the Course Department entered.
         * 
         *******************************************************************************************************/
        public DataSet GetCourseOfferings(String courseSubject, String programBeginSTRM, String programEndSTRM) //courses used by programs
        {
            objCon = new SqlConnection(strConnection);

            try
            {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCoursesBySubject", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.AddWithValue("@EndSTRM", programEndSTRM);
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");
                objCmd.Parameters.AddWithValue("@SUBJECT", courseSubject);
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;
            }
            finally
            {
                objCon.Close();
            }
            /*
            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT CourseOffering, CATALOG_NBR, CourseSuffix " +
                    "FROM Course " +
                    "WHERE SUBJECT = '" + courseSubject +
                    "' AND ((CourseBeginSTRM <= '" + programBeginSTRM +
                    "' AND (CourseEndSTRM >= '" + programBeginSTRM +
                    "' OR CourseEndSTRM = 'Z999')) " +
                    "OR (CourseBeginSTRM >= '" + programBeginSTRM +
                    "' AND CourseBeginSTRM <= '" + programEndSTRM +
                    "' AND (CourseEndSTRM <= '" + programEndSTRM +
                    "' OR CourseEndSTRM = 'Z999'))) " +
                    "AND PublishedCourse = '1' " +
                    "GROUP BY CourseOffering, CATALOG_NBR, CourseSuffix " +
                    "ORDER BY CATALOG_NBR;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
            */
        }


        public bool ExpiredSTRM(String endSTRM, String compareSTRM) {
            Int32 intResult = string.Compare(endSTRM, compareSTRM, true);

            if (intResult < 0) {
                return true;
            } else {
                return false;
            }
        }


        public bool ExpiredSTRM(String endSTRM) {
            String currentSTRM = csTerm.GetCurrentTerm();

            Int32 intResult = string.Compare(endSTRM, currentSTRM);

            if (intResult < 0) {
                return true;
            } else {
                return false;
            }
        }


        public bool FutureSTRM(String beginSTRM) {
            String currentSTRM = csTerm.GetCurrentTerm();
            Int32 intResult = string.Compare(currentSTRM, beginSTRM, true);

            if (intResult < 0) {
                return true;
            } else {
                return false;
            }
        }


        public bool HasWorkingProgramCopy(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Program " +
                    "WHERE PublishedProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }

        public bool HasWorkingCPGCopy(Int32 careerPlanningGuideID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM CareerPlanningGuide " +
                    "WHERE PublishedCareerPlanningGuideID = " + careerPlanningGuideID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }

        public DataSet GetPathways(String institution) {

            objCon = new SqlConnection(strConnection);

            try {
                String query = "";
                if(institution != null && institution != "") {
                    query += "WHERE Institution = '" + institution + "' ";
                }

                strSQL = "SELECT PathwayID, Institution, PathwayTitle " +
                         "FROM Pathway " +
                         query + 
                         " ORDER BY PathwayTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }

        public DataSet GetAreasOfStudy(String collegeShortTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                String query = "";
                if (collegeShortTitle != null && collegeShortTitle != "") {
                    query += "WHERE CollegeShortTitle = '" + collegeShortTitle + "' ";
                }

                strSQL = "SELECT AreaOfStudy.AreaOfStudyID, Title, COUNT(AreaOfStudyProgram.AreaOfStudyID) AS ProgramCount, CollegeShortTitle " +
                         "FROM AreaOfStudy " +
                         "LEFT OUTER JOIN AreaOfStudyProgram " +
                         "ON AreaOfStudyProgram.AreaOfStudyID = AreaOfStudy.AreaOfStudyID " +
                         "LEFT OUTER JOIN College " +
                         "ON College.Institution = AreaOfStudy.Institution " +
                         query +
                         "GROUP BY AreaOfStudy.AreaOfStudyID, Title, CollegeShortTitle " +
                         "ORDER BY Title, CollegeShortTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }

        public DataSet GetAreaOfStudy(Int32 areaOfStudyID) {

            //returning dataset instead of string to support additional fields being added to categories later
            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT Institution, Title, WebsiteURL " +
                    "FROM AreaOfStudy " +
                    "WHERE AreaOfStudyID = " + areaOfStudyID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }

        public Int32 AddAreaOfStudy(String institution, String pathwayID, String areaOfStudyTitle, String webPageURL) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM AreaOfStudy " +
                    "WHERE Institution = '" + institution + "' " +
                    "AND Title = '" + areaOfStudyTitle.Replace("'", "''") + "';";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return -1;
                } else {
                    strSQL = "INSERT INTO AreaOfStudy(Institution, Title, WebsiteURL) " +
                        "VALUES('" + institution + "','" + areaOfStudyTitle.Replace("'", "''") + "','" + webPageURL + "'); " +
                        "SELECT AreaOfStudyID FROM AreaOfStudy WHERE (AreaOfStudyID = SCOPE_IDENTITY());";

                    objCmd = new SqlCommand(strSQL, objCon);
                    Int32 areaOfStudyID = Convert.ToInt32(objCmd.ExecuteScalar());

                    if (pathwayID != "") {
                        strSQL = "INSERT INTO PathwayAreaOfStudy(PathwayID, AreaOfStudyID, PrimaryPathway) " +
                                 "VALUES(" + pathwayID + "," + areaOfStudyID + ",1);";

                        objCmd = new SqlCommand(strSQL, objCon);
                        objCmd.ExecuteNonQuery();
                    }

                    return areaOfStudyID;
                }

            } finally {
                objCon.Close();
            }

        }

        public Int16 EditAreaOfStudy(Int32 areaOfStudyID, String institution, String pathwayID, String areaOfStudyTitle, String webPageURL) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM AreaOfStudy " +
                    "WHERE Institution = '" + institution + 
                    "' AND Title = '" + areaOfStudyTitle.Replace("'", "''") + 
                    "' AND AreaOfStudyID <> " + areaOfStudyID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return 2;
                } else {
                    strSQL = "UPDATE AreaOfStudy " +
                        "SET Institution = '" + institution +
                        "', Title = '" + areaOfStudyTitle.Replace("'", "''") +
                        "', WebsiteURL = '" + webPageURL + 
                        "' WHERE AreaOfStudyID = " + areaOfStudyID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        if (pathwayID != "") {

                            strSQL = "DELETE PathwayAreaOfStudy " +
                                     "WHERE AreaOfStudyID = " + areaOfStudyID +
                                     " AND PrimaryPathway = 1;";
                            objCmd = new SqlCommand(strSQL, objCon);
                            objCmd.ExecuteNonQuery();

                            strSQL = "INSERT INTO PathwayAreaOfStudy(PathwayID, AreaOfStudyID, PrimaryPathway) " +
                                     "VALUES(" + pathwayID + "," + areaOfStudyID + ",1);";

                            objCmd = new SqlCommand(strSQL, objCon);
                            objCmd.ExecuteNonQuery();
                        }
                        return 1;
                    } else {
                        return 0;
                    }
                }

            } finally {
                objCon.Close();
            }
        }

        public bool DeleteAdditionalAreaOfStudyPathways(Int32 areaOfStudyID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE PathwayAreaOfStudy " +
                    "WHERE AreaOfStudyID = " + areaOfStudyID +
                    " AND PrimaryPathway = 0;";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }

        public bool DeleteAreaOfStudy(Int32 areaOfStudyID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE PathwayAreaOfStudy " +
                         "WHERE AreaOfStudyID = " + areaOfStudyID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                strSQL = "DELETE AreaOfStudy " +
                         "WHERE AreaOfStudyID = " + areaOfStudyID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }

        public bool AddAreaOfStudyPathway(Int32 areaOfStudyID, Int32 pathwayID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM PathwayAreaOfStudy " +
                    "WHERE AreaOfStudyID = " + areaOfStudyID +
                    " AND PathwayID = " + pathwayID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return false;
                } else {
                    strSQL = "INSERT INTO PathwayAreaOfStudy(PathwayID, AreaOfStudyID, PrimaryPathway) " +
                        "VALUES(" + pathwayID + "," + areaOfStudyID + ",0);";


                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }

            } finally {
                objCon.Close();
            }
        }

        public DataSet GetAreaOfStudyPathways(Int32 areaOfStudyID)
        {

            objCon = new SqlConnection(strConnection);

            try
            {

                objCon.Open();

                strSQL = "SELECT PathwayAreaOfStudy.PathwayID, PrimaryPathway, PathwayTitle " +
                         "FROM PathwayAreaOfStudy " +
                         "INNER JOIN Pathway " +
                         "ON Pathway.PathwayID = PathwayAreaOfStudy.PathwayID " +
                         "WHERE AreaOfStudyID = " + areaOfStudyID +
                         "ORDER BY PrimaryPathway DESC;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            }
            finally
            {
                objCon.Close();
            }
        }

        public Int16 AddPathway(String institution, String pathwayTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Pathway " +
                    "WHERE Institution = '" + institution +
                    "' AND PathwayTitle = '" + pathwayTitle.Replace("'", "''") + "';";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return 2;
                } else {
                    strSQL = "INSERT INTO Pathway(Institution, PathwayTitle) " +
                        "VALUES('" + institution + "','" + pathwayTitle.Replace("'", "''") + "');";

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }

            } finally {
                objCon.Close();
            }
        }

        public DataSet GetPathways() {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT Pathway.PathwayID, Institution, PathwayTitle, COUNT(PathwayAreaOfStudy.PathwayID) AS AreaOfStudyCount " +
                    "FROM Pathway " +
                    "LEFT OUTER JOIN PathwayAreaOfStudy " +
                    "ON PathwayAreaOfStudy.PathwayID = Pathway.PathwayID " +
                    "GROUP BY Pathway.PathwayID, Institution, PathwayTitle " +
                    "ORDER BY PathwayTitle, Institution;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }

        public DataSet GetPathway(Int32 pathwayID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT Institution, PathwayTitle, COUNT(PathwayAreaOfStudy.PathwayID) AS AreaOfStudyCount " +
                    "FROM Pathway " +
                    "LEFT OUTER JOIN PathwayAreaOfStudy " +
                    "ON PathwayAreaOfStudy.PathwayID = Pathway.PathwayID " +
                    "WHERE Pathway.PathwayID = " + pathwayID +
                    " GROUP BY Pathway.PathwayID, Institution, PathwayTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }

        public Int16 EditPathway(Int32 pathwayID, String institution, String pathwayTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Pathway " +
                    "WHERE PathwayTitle = '" + pathwayTitle +
                    "' AND Institution = '" + institution +
                    "' AND PathwayID <> " + pathwayID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return 2;
                } else {
                    strSQL = "UPDATE Pathway " +
                        "SET PathwayTitle = '" + pathwayTitle.Replace("'", "''") +
                        "', Institution = '" + institution + 
                        "' WHERE PathwayID = " + pathwayID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }

            } finally {
                objCon.Close();
            }
        }

        public bool DeletePathway(Int32 pathwayID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE Pathway " +
                    "WHERE PathwayID = " + pathwayID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }

        public DataSet GetCategories() {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT Category.CategoryID, CategoryTitle, COUNT(ProgramCategory.CategoryID) AS ProgCount " +
                    "FROM Category " +
                    "LEFT OUTER JOIN ProgramCategory " +
                    "ON ProgramCategory.CategoryID = Category.CategoryID " +
                    "GROUP BY Category.CategoryID, CategoryTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public Int16 AddCategory(String categoryTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Category " +
                    "WHERE CategoryTitle = '" + categoryTitle.Replace("'", "''") + "';";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return 2;
                } else {
                    strSQL = "INSERT INTO Category(CategoryTitle) " +
                        "VALUES('" + categoryTitle.Replace("'", "''") + "');";

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }

            } finally {
                objCon.Close();
            }
        }
  

        public DataSet GetCategory(Int32 intCatASN) {

            //returning dataset instead of string to support additional fields being added to categories later
            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT CategoryTitle " +
                    "FROM Category " +
                    "WHERE CategoryID = " + intCatASN + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public Int16 EditCategory(Int32 intCatASN, String categoryTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM Category " +
                    "WHERE CategoryTitle = '" + categoryTitle +
                    "' AND CategoryID <> " + intCatASN + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return 2;
                } else {
                    strSQL = "UPDATE Category " +
                        "SET CategoryTitle = '" + categoryTitle.Replace("'", "''") +
                        "' WHERE CategoryID = " + intCatASN + ";";

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteCategory(Int32 intCatASN) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE Category " +
                    "WHERE CategoryID = " + intCatASN + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        /// <summary>
        /// Returns current and future terms for programs
        /// </summary>
        /// <returns>
        /// DataSet 
        /// Field List: STRM (Example: 2173)
        ///             DESCR (Example: SPRING 2017)
        /// </returns>
        public DataSet GetProgramTerms() {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetProgramTerms", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("@StartDate", SqlDbType.Date);
                objCmd.Parameters["@StartDate"].Value = DateTime.Now.AddYears(-2).ToShortDateString();
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public String ExportProgramsToGraphics(String selectedSTRM) {

            try {

                FileStream objFS = File.Open(HttpContext.Current.Server.MapPath("../ProgOutlines.doc"), FileMode.Truncate);
                StreamWriter objSW = new StreamWriter(objFS);

                iCatalogXML.iCatalogXML wsICatalog = new iCatalogXML.iCatalogXML();

                DataSet dsPrograms = wsICatalog.GetProgramsByTitle("", selectedSTRM);
                for (Int32 intDSRow = 0; intDSRow < dsPrograms.Tables[0].Rows.Count; intDSRow++) {
                    Int32 programVersionID = Convert.ToInt32(dsPrograms.Tables[0].Rows[intDSRow]["ProgramVersionID"]);

                    objSW.WriteLine("<pstyle:ProgTitle>" + dsPrograms.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString().Replace("-", "<0x2014>").Replace("&mdash;", "<0x2014>"));

                    DataSet dsDegTitles = wsICatalog.GetProgramDegrees(programVersionID);
                    Int32 intDegCount = dsDegTitles.Tables[0].Rows.Count;

                    if (intDegCount > 0) {
                        String degreeShortTitles = "<pstyle:DegreeTitle>";
                        for (Int16 i = 0; i < intDegCount; i++) {
                            degreeShortTitles += dsDegTitles.Tables[0].Rows[i]["DegreeShortTitle"].ToString();
                            if (i + 1 != intDegCount) {
                                degreeShortTitles += ", ";
                            }
                        }
                        objSW.WriteLine(degreeShortTitles.Replace("-", "<0x2014>").Replace("&mdash;", "<0x2014>").Replace("&ndash;", "<0x2014>") + ": " + dsPrograms.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString());
                    }

                    DataSet dsProgram = wsICatalog.GetProgramByVersionID(programVersionID);
                    if (dsProgram.Tables[0].Rows.Count > 0) {
                        String programBeginSTRM = dsProgram.Tables[0].Rows[0]["ProgramBeginSTRM"].ToString();
                        String programEndSTRM = dsProgram.Tables[0].Rows[0]["ProgramEndSTRM"].ToString();

                        //DISPLAY PROGRAM DESCRIPTION
                        String programDescription = Regex.Replace(dsProgram.Tables[0].Rows[0]["ProgramDescription"].ToString(), "</P>", "", RegexOptions.IgnoreCase);
                        programDescription = programDescription.Replace("-", "<0x2014>").Replace("&mdash;", "<0x2014>").Replace("ndash;", "<0x2014>");
                        programDescription = Regex.Replace(programDescription, "\n", "");
                        programDescription = Regex.Replace(programDescription, "\r", "");
                        programDescription = Regex.Replace(programDescription, "<P>", "\n<pstyle:ProgDesc>", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<BR></STRONG>", "</STRONG><BR>", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<BR /></STRONG>", "</STRONG><BR />", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<STRONG>", "<cstyle:TEXT-bold>", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "</STRONG>", "<cstyle:>", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<UL>", "", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<LI>", "\n<pstyle:Bullet><cstyle:Bullet>n\t<cstyle:>", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "</LI>", "", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "</UL>", "", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "&nbsp;", " ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "  ", " ");
                        programDescription = Regex.Replace(programDescription, "<P></P>", "", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<BR>", "<0x000A>", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<br />", "<0x000A>", RegexOptions.IgnoreCase);
                        programDescription = programDescription.Replace("\n<pstyle:ProgDesc>\n<pstyle:Bullet><cstyle:Bullet>n\t<cstyle:>", "\n<pstyle:Bullet><cstyle:Bullet>n\t<cstyle:>");
                        programDescription = programDescription.Replace("\n<pstyle:ProgDesc>\n<pstyle:ProgDesc>", "\n<pstyle:ProgDesc>");
                        if (!programDescription.StartsWith("\n<pstyle:ProgDesc>")) {
                            programDescription = "\n<pstyle:ProgDesc>" + programDescription;
                        }
                        programDescription = programDescription.Substring(1, programDescription.Length - 1);
                        objSW.WriteLine(programDescription);

                        DataSet dsProgDeg = wsICatalog.GetProgramDegrees(programVersionID);

                        for (Int32 intDeg = 0; intDeg < dsProgDeg.Tables[0].Rows.Count; intDeg++) {
                            Int32 programDegreeID = Convert.ToInt32(dsProgDeg.Tables[0].Rows[intDeg]["ProgramDegreeID"]);
                            String degreeShortTitle = dsProgDeg.Tables[0].Rows[intDeg]["DegreeShortTitle"].ToString().Replace("-", "<0x2014>").Replace("&mdash;", "<0x2014>");
                            objSW.WriteLine("<pstyle:ProgOptTitle>" + degreeShortTitle);

                            DataSet dsDegDetails = wsICatalog.GetProgramDegree(programDegreeID);
                            String documentTitle = dsDegDetails.Tables[0].Rows[0]["DocumentTitle"].ToString();
                            if (documentTitle != null && documentTitle != "") {
                                objSW.WriteLine("<pstyle:ProgDesc+Bold>See transfer degree requirements in the Transfer Program Outlines section of this catalog.");
                            }

                            //DISPLAY DEGREE OPTIONS
                            DataSet dsOption = wsICatalog.GetOptions(programDegreeID);
                            Int32 intOptCount = dsOption.Tables[0].Rows.Count;

                            if (intOptCount > 0) {
                                if (Convert.ToInt16(dsDegDetails.Tables[0].Rows[0]["ProgramDisplay"]) == 2) {
                                    //DISPLAY PROGRAM TEXT
                                    String programTextOutline = dsDegDetails.Tables[0].Rows[0]["ProgramTextOutline"].ToString();

                                    if (programTextOutline != null && programTextOutline != "") {
                                        programTextOutline = programTextOutline.Replace("-", "<0x2014>").Replace("&mdash;", "<0x2014>").Replace("ndash;", "<0x2014>");
                                        programTextOutline = Regex.Replace(programTextOutline, "</P>", "", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "\n", "");
                                        programTextOutline = Regex.Replace(programTextOutline, "\r", "");
                                        programTextOutline = Regex.Replace(programTextOutline, "<P>", "\n<pstyle:ProgDesc>", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "<BR></STRONG>", "</STRONG><BR>", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "<BR /></STRONG>", "</STRONG><BR />", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "<STRONG>", "<cstyle:TEXT-bold>", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "</STRONG>", "<cstyle:>", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "<UL>", "", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "<LI>", "\n<pstyle:Bullet><cstyle:Bullet>n\t<cstyle:>", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "</LI>", "", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "</UL>", "", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "&nbsp;", " ", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "  ", " ");
                                        programTextOutline = Regex.Replace(programTextOutline, "<P></P>", "", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "<BR>", "<0x000A>", RegexOptions.IgnoreCase);
                                        programTextOutline = Regex.Replace(programTextOutline, "<br />", "<0x000A>", RegexOptions.IgnoreCase);
                                        programTextOutline = programTextOutline.Replace("\n<pstyle:ProgDesc>\n<pstyle:Bullet><cstyle:Bullet>n\t<cstyle:>", "\n<pstyle:Bullet><cstyle:Bullet>n\t<cstyle:>");
                                        programTextOutline = programTextOutline.Replace("\n<pstyle:ProgDesc>\n<pstyle:ProgDesc>", "\n<pstyle:ProgDesc>");
                                        if (!programTextOutline.StartsWith("\n<pstyle:ProgDesc>")) {
                                            programTextOutline = "\n<pstyle:ProgDesc>" + programTextOutline;
                                        }
                                        programTextOutline = programTextOutline.Substring(1, programTextOutline.Length - 1);
                                        objSW.WriteLine(programTextOutline);
                                    }
                                } else {
                                    //DISPLAY PROGRAM OUTLINE
                                    for (Int32 intOpt = 0; intOpt < intOptCount; intOpt++) {
                                        Int32 optionID = Convert.ToInt32(dsOption.Tables[0].Rows[intOpt]["OptionID"]);
                                        String optionDescription = dsOption.Tables[0].Rows[intOpt]["OptionDescription"].ToString().Replace("-", "<0x2014>").Replace("&mdash;", "<0x2014>").Replace("ndash;", "<0x2014>").Replace("&nbsp;", " ").Replace("  ", " ");
                                        if (intOptCount > 1) {
                                            objSW.WriteLine("<pstyle:ProgOptTitle>" + dsOption.Tables[0].Rows[intOpt]["OptionTitle"].ToString());
                                        }

                                        if (optionDescription != null && optionDescription != "") {
                                            optionDescription = optionDescription.Replace("\n", "").Replace("\r", "<pstyle:ProgDesc>").Replace("<pstyle:ProgDesc><pstyle:ProgDesc>", "<pstyle:ProgDesc>");
                                            if (optionDescription.EndsWith("<pstyle:ProgDesc>")) {
                                                optionDescription = optionDescription.Substring(0, optionDescription.LastIndexOf("<pstyle:ProgDesc>"));
                                            }
                                            objSW.WriteLine("<pstyle:ProgDesc>" + optionDescription);
                                        }

                                        //DISPLAY PREREQUISITES
                                        DataSet dsPrereq = wsICatalog.GetOptionPrerequisites(optionID, programBeginSTRM, programEndSTRM, selectedSTRM);
                                        Int32 intPrereqCount = dsPrereq.Tables[0].Rows.Count;
                                        if (intPrereqCount > 0) {
                                            objSW.WriteLine("<pstyle:ProgDesc+Bold>Prerequisites");
                                            for (Int32 intPrereq = 0; intPrereq < intPrereqCount; intPrereq++) {
                                                String courseSubject = dsPrereq.Tables[0].Rows[intPrereq]["SUBJECT"].ToString();
                                                String catalogNbr = dsPrereq.Tables[0].Rows[intPrereq]["CATALOG_NBR"].ToString();
                                                String courseLongTitle = dsPrereq.Tables[0].Rows[intPrereq]["COURSE_TITLE_LONG"].ToString();
                                                String footnoteNumber = dsPrereq.Tables[0].Rows[intPrereq]["FootnoteNumber"].ToString();
                                                if (footnoteNumber != null && footnoteNumber != "") {
                                                    footnoteNumber = "<cp:Superscript>" + footnoteNumber + "<cp:>";
                                                } else {
                                                    footnoteNumber = "";
                                                }
                                                if (intPrereq == 0) {
                                                    objSW.WriteLine("<pstyle:prereq>" + courseSubject + "\t" + catalogNbr + "\t" + courseLongTitle + footnoteNumber);
                                                } else {
                                                    objSW.WriteLine(courseSubject + "\t" + catalogNbr + "\t" + courseLongTitle + footnoteNumber);
                                                }
                                            }
                                        }

                                        DataSet dsOptCrs = wsICatalog.GetOptionCoursesAndElectives(optionID, programBeginSTRM, programEndSTRM, selectedSTRM);
                                        DataRow[] drOptCrs = dsOptCrs.Tables[0].Select("", "Quarter");
                                        Int32 intCrsCount = drOptCrs.Length;
                                        if (intCrsCount > 0) {
                                            Double dblTotalMinCred = 0, dblTotalMaxCred = 0, dblDegMinCred = 0, dblDegMaxCred = 0;
                                            Int16 intQuarter = 0;

                                            for (Int32 intCrs = 0; intCrs < intCrsCount; intCrs++) {
                                                String strQuarter = drOptCrs[intCrs]["Quarter"].ToString();
                                                String strCrs = "";
                                                if (strQuarter != "") {
                                                    if (intQuarter.ToString() != strQuarter) {
                                                        intQuarter = Convert.ToInt16(strQuarter);

                                                        if (intQuarter == 1) {
                                                            objSW.WriteLine("<pstyle:Quarter>First Quarter");
                                                        } else if (intQuarter == 2) {
                                                            objSW.WriteLine("<pstyle:Quarter>Second Quarter");
                                                        } else if (intQuarter == 3) {
                                                            objSW.WriteLine("<pstyle:Quarter>Third Quarter");
                                                        } else if (intQuarter == 4) {
                                                            objSW.WriteLine("<pstyle:Quarter>Fourth Quarter");
                                                        } else if (intQuarter == 5) {
                                                            objSW.WriteLine("<pstyle:Quarter>Fifth Quarter");
                                                        } else if (intQuarter == 6) {
                                                            objSW.WriteLine("<pstyle:Quarter>Sixth Quarter");
                                                        } else if (intQuarter == 7) {
                                                            objSW.WriteLine("<pstyle:Quarter>Seventh Quarter");
                                                        } else if (intQuarter == 8) {
                                                            objSW.WriteLine("<pstyle:Quarter>Eighth Quarter");
                                                        } else if (intQuarter == 9) {
                                                            objSW.WriteLine("<pstyle:Quarter>Ninth Quarter");
                                                        } else if (intQuarter == 10) {
                                                            objSW.WriteLine("<pstyle:Quarter>Tenth Quarter");
                                                        }

                                                        dblTotalMinCred = 0;
                                                        dblTotalMaxCred = 0;

                                                        strCrs = "<pstyle:Course>";
                                                    }
                                                } else if (strQuarter == "" && intCrs == 0) {
                                                    strCrs = "<pstyle:Course>";
                                                }

                                                Double unitsMinimum = 0, unitsMaximum = 0;

                                                if (Convert.ToByte(drOptCrs[intCrs]["OverwriteUnits"]) == 1) {
                                                    unitsMinimum = Convert.ToDouble(drOptCrs[intCrs]["OverwriteUnitsMinimum"]);
                                                    unitsMaximum = Convert.ToDouble(drOptCrs[intCrs]["OverwriteUnitsMaximum"]);
                                                } else {
                                                    unitsMinimum = Convert.ToDouble(drOptCrs[intCrs]["UNITS_MINIMUM"]);
                                                    unitsMaximum = Convert.ToDouble(drOptCrs[intCrs]["UNITS_MAXIMUM"]);
                                                }

                                                String strCredits = "";

                                                if (unitsMinimum == unitsMaximum) {
                                                    strCredits = unitsMinimum.ToString();
                                                    dblTotalMinCred += unitsMinimum;
                                                    dblTotalMaxCred += unitsMinimum;
                                                } else {
                                                    strCredits = unitsMinimum + "-" + unitsMaximum;
                                                    dblTotalMinCred += unitsMinimum;
                                                    dblTotalMaxCred += unitsMaximum;
                                                }

                                                String courseLongTitle = drOptCrs[intCrs]["COURSE_TITLE_LONG"].ToString();
                                                String footnoteNumber = drOptCrs[intCrs]["FootnoteNumber"].ToString();

                                                if (drOptCrs[intCrs]["CourseOffering"].ToString() != "ZZZ") {
                                                    strCrs += drOptCrs[intCrs]["SUBJECT"].ToString() + "\t" + drOptCrs[intCrs]["CATALOG_NBR"].ToString();
                                                } else {
                                                    strCrs += "\t";
                                                }

                                                strCrs += "\t" + courseLongTitle;

                                                if (footnoteNumber != null && footnoteNumber != "") {
                                                    strCrs += "<cp:Superscript>" + footnoteNumber + "<cp:>";
                                                }

                                                objSW.WriteLine(strCrs + "\t" + strCredits);

                                                //check if the quarter or group of courses/electives is on the last course/elective
                                                if ((intCrs == (intCrsCount - 1)) || (intCrs < (intCrsCount - 1) && drOptCrs[intCrs + 1]["Quarter"].ToString() != drOptCrs[intCrs]["Quarter"].ToString())) {
                                                    //display the total min and max credits for the quarter
                                                    String strTotalCred = "";

                                                    if (dblTotalMinCred < dblTotalMaxCred) {
                                                        dblDegMinCred += dblTotalMinCred;
                                                        dblDegMaxCred += dblTotalMaxCred;
                                                        strTotalCred = dblTotalMinCred + "-" + dblTotalMaxCred;
                                                    } else {
                                                        dblDegMinCred += dblTotalMinCred;
                                                        dblDegMaxCred += dblTotalMinCred;
                                                        strTotalCred = dblTotalMaxCred.ToString();
                                                    }

                                                    objSW.WriteLine("<pstyle:Total>\tTotal\t" + strTotalCred);
                                                }

                                                if (intCrs == (intCrsCount - 1)) {
                                                    //calculate and display total credits for the selected degree
                                                    String strDegCred = "";

                                                    if (dblDegMinCred < dblDegMaxCred) {
                                                        strDegCred = dblDegMinCred + "-" + dblDegMaxCred;
                                                    } else {
                                                        strDegCred = dblDegMaxCred.ToString();
                                                    }

                                                    objSW.WriteLine("<pstyle:TotalCredits>" + strDegCred + " credits are required for the " + degreeShortTitle + ".");
                                                }
                                            }
                                        }

                                        //Get Program Electives
                                        DataSet dsElectGroup = wsICatalog.GetOptionElectiveGroups(optionID);
                                        Int32 intElectGroupCount = dsElectGroup.Tables[0].Rows.Count;
                                        bool blnAddSpace = false;

                                        for (Int32 intElectGroup = 0; intElectGroup < intElectGroupCount; intElectGroup++) {
                                            Int32 optionElectiveGroupID = Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroup]["OptionElectiveGroupID"]);
                                            Int32 intElectCount = Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroup]["ElectiveCount"]);

                                            if (intElectCount > 0) {
                                                blnAddSpace = true;

                                                objSW.WriteLine("<pstyle:ElectiveTitle>" + dsElectGroup.Tables[0].Rows[intElectGroup]["ElectiveGroupTitle"].ToString());

                                                DataSet dsElectiveGroupCourse = wsICatalog.GetElectiveGroupCourses(optionElectiveGroupID, programBeginSTRM, programEndSTRM, selectedSTRM);
                                                for (Int32 intElect = 0; intElect < dsElectiveGroupCourse.Tables[0].Rows.Count; intElect++) {
                                                    Double dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["UNITS_MINIMUM"]);
                                                    Double dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["UNITS_MAXIMUM"]);

                                                    if (Convert.ToByte(dsElectiveGroupCourse.Tables[0].Rows[intElect]["OverwriteUnits"]) == 1) {
                                                        dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["OverwriteUnitsMinimum"]);
                                                        dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["OverwriteUnitsMaximum"]);
                                                    }

                                                    String strElectCredits = "", strElect = "";
                                                    if (dblElectCredMin == dblElectCredMax) {
                                                        strElectCredits = dblElectCredMin.ToString();
                                                    } else {
                                                        strElectCredits = dblElectCredMin + "-" + dblElectCredMax;
                                                    }

                                                    if (intElect == 0) {
                                                        strElect += "<pstyle:Course>";
                                                    }

                                                    objSW.WriteLine(strElect + dsElectiveGroupCourse.Tables[0].Rows[intElect]["SUBJECT"].ToString() + "\t" + dsElectiveGroupCourse.Tables[0].Rows[intElect]["CATALOG_NBR"].ToString() + "\t" + dsElectiveGroupCourse.Tables[0].Rows[intElect]["COURSE_TITLE_LONG"].ToString() + "\t" + strElectCredits);
                                                }
                                            }
                                        }

                                        //Get Program Footnotes
                                        DataSet dsFootnote = wsICatalog.GetOptionFootnotes(optionID);
                                        Int32 intFootnoteCount = dsFootnote.Tables[0].Rows.Count;
                                        if (blnAddSpace && intFootnoteCount > 0) {
                                            objSW.WriteLine("<pstyle:AddSpace>");
                                        }
                                        for (Int32 intFootnote = 0; intFootnote < intFootnoteCount; intFootnote++) {
                                            String footnote = dsFootnote.Tables[0].Rows[intFootnote]["Footnote"].ToString().Replace("-", "<0x2014>").Replace("&mdash;", "<0x2014>").Replace("ndash;", "<0x2014>");
                                            footnote = footnote.Replace("\n", "").Replace("\r", "").Replace("\t", "").Replace("&nbsp;", " ").Replace("  ", " ").Replace("&amp;", "&");
                                            objSW.WriteLine("<pstyle:Footnote><cp:Superscript>" + dsFootnote.Tables[0].Rows[intFootnote]["FootnoteNumber"].ToString() + "<cp:><0x2002>" + footnote);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                objSW.Close();
                objFS.Close();
                return "../ProgOutlines.doc";

            } catch {
                return "";
            }
        }


        public String ExportProgramsToRTF(String publishedProgram, String selectedSTRM, String collegeID, String strProgramVersionID) {

            try {

                FileStream objFS = File.Open(HttpContext.Current.Server.MapPath("../Program.doc"), FileMode.Truncate);
                StreamWriter objSW = new StreamWriter(objFS);
                objSW.Write("{\\rtf1 ");

                DataSet dsPrograms = new DataSet();
                if (strProgramVersionID == "") {
                    dsPrograms = GetProgramTitles(publishedProgram, selectedSTRM, collegeID);
                } else {
                    dsPrograms = GetProgram(Convert.ToInt32(strProgramVersionID));
                }

                //LOOP THROUGH THE PROGRAM(S)
                for (Int32 intDSRow = 0; intDSRow < dsPrograms.Tables[0].Rows.Count; intDSRow++) {
                    if (intDSRow > 0) {
                        //add a page break
                        objSW.WriteLine("{\\page}");
                    }

                    Int32 programVersionID = Convert.ToInt32(dsPrograms.Tables[0].Rows[intDSRow]["ProgramVersionID"]);
                    objSW.WriteLine("{\\fs30\\b " + dsPrograms.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString() + "\\line}");

                    //GET DEGREE TITLES AND COLLEGE ABBREVIATION
                    DataSet dsProgramDegree = GetProgramDegree(programVersionID);
                    Int32 intDegCount = dsProgramDegree.Tables[0].Rows.Count;
                    if (intDegCount > 0) {
                        String degreeShortTitles = "";
                        for (Int16 i = 0; i < intDegCount; i++) {
                            degreeShortTitles += dsProgramDegree.Tables[0].Rows[i]["DegreeShortTitle"].ToString();
                            if (i + 1 != intDegCount) {
                                degreeShortTitles += ", ";
                            }
                        }
                        objSW.WriteLine("{\\fs26\\b " + degreeShortTitles.Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ") + ": " + dsPrograms.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString() + "\\par}");
                    }

                    //GET THE BEGIN AND END TERM
                    String beginTermDescription = "", endTermDescription = "";
                    String programBeginSTRM = dsPrograms.Tables[0].Rows[intDSRow]["ProgramBeginSTRM"].ToString();
                    String programEndSTRM = dsPrograms.Tables[0].Rows[intDSRow]["ProgramEndSTRM"].ToString();

                    termData csTerm = new termData();
                    if (programEndSTRM == "Z999") {
                        endTermDescription = "Undefined";
                    } else {
                        endTermDescription = csTerm.GetTermDescription(programEndSTRM);
                    }
                    beginTermDescription = csTerm.GetTermDescription(programBeginSTRM);

                    objSW.WriteLine("{\\par\\fs22 " + programBeginSTRM + " (" + beginTermDescription + ") - " + programEndSTRM + " (" + endTermDescription + ")\\par}");

                    publishedProgram = dsPrograms.Tables[0].Rows[intDSRow]["PublishedProgram"].ToString();
                    if (publishedProgram == "0") {
                        publishedProgram = "Working Copy";
                    } else if (publishedProgram == "1") {
                        publishedProgram = "Published Copy";
                    }
                    objSW.WriteLine("{\\fs22 Program Status: " + publishedProgram + "\\par}");

                    //GET STATE APPROVAL DATE
                    DataSet dsPrimaryValues = GetPrimaryValues(programVersionID);
                    if (dsPrimaryValues.Tables[0].Rows.Count > 0) {
                        objSW.WriteLine("{\\fs22 State Approval Date: " + dsPrimaryValues.Tables[0].Rows[0]["OptionStateApproval"].ToString());
                        objSW.WriteLine("  CIP: " + dsPrimaryValues.Tables[0].Rows[0]["CIP"].ToString());
                        objSW.WriteLine("  EPC: " + dsPrimaryValues.Tables[0].Rows[0]["EPC"].ToString());
                        objSW.WriteLine("  Academic Plan: " + dsPrimaryValues.Tables[0].Rows[0]["ACAD_PLAN"].ToString() + "\\par}");
                    }

                    //GET PROGRAM DESCRIPTION
                    DataSet dsProgramDescription = GetProgramDescription(programVersionID);
                    if (dsProgramDescription.Tables[0].Rows.Count > 0) {
                        String programDescription = Regex.Replace(dsProgramDescription.Tables[0].Rows[0]["ProgramDescription"].ToString(), "</P>", "\\par ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "\n", "");
                        programDescription = Regex.Replace(programDescription, "\r", "");
                        programDescription = programDescription.Replace("-", "\\endash ").Replace("�", "\\endash ").Replace("�", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ");
                        programDescription = Regex.Replace(programDescription, "<P>", "\\par ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<BR></STRONG>", "</STRONG><BR>", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<BR /></STRONG>", "</STRONG><BR />", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<STRONG>", "\\b ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "</STRONG>", "\\b0 ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<UL><LI>", "<UL>", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<UL>", "\\pard\\par}{\\*\\pn\\pnlvlblt\\pnf10\\pnindent300{\\pntxtb\\bullet}\\fi-300\\li600 ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<LI>", "\\par ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "</LI>", "", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "</UL>", "\\par\\pard}{", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "&nbsp;", " ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "  ", " ");
                        programDescription = Regex.Replace(programDescription, "<P></P>", "\\par ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<BR>", "\\line ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<br />", "\\line ", RegexOptions.IgnoreCase);
                        if (!programDescription.StartsWith("\\par ")) {
                            programDescription = "\\par " + programDescription;
                        }
                        if (!programDescription.EndsWith("\\par ")) {
                            programDescription = programDescription + "\\par ";
                        }
                        objSW.WriteLine("{\\fs22 " + programDescription + "}");
                    }

                    //CHECK IF PROGRAM CONTAINS COURSE OUTLINE OR TEXT OUTLINE
                    if (dsPrograms.Tables[0].Rows[intDSRow]["ProgramDisplay"].ToString() == "2") {
                        //GET TEXT OUTLINE
                        String programTextOutline = GetProgramTextOutline(programVersionID);
                        if (programTextOutline != null && programTextOutline != "") {
                            programTextOutline = Regex.Replace(programTextOutline, "</P>", "\\par ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "\n", "");
                            programTextOutline = Regex.Replace(programTextOutline, "\r", "");
                            programTextOutline = programTextOutline.Replace("-", "\\endash ").Replace("�", "\\endash ").Replace("�", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ");
                            programTextOutline = Regex.Replace(programTextOutline, "<P>", "\\par ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<BR></STRONG>", "</STRONG><BR>", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<BR /></STRONG>", "</STRONG><BR />", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<STRONG>", "\\b ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "</STRONG>", "\\b0 ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<UL><LI>", "<UL>", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<UL>", "\\pard\\par}{\\*\\pn\\pnlvlblt\\pnf10\\pnindent300{\\pntxtb\\bullet}\\fi-300\\li600 ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<LI>", "\\par ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "</LI>", "", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "</UL>", "\\par\\pard}{", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "&nbsp;", " ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "  ", " ");
                            programTextOutline = Regex.Replace(programTextOutline, "<P></P>", "\\par ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<BR>", "\\line ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<br />", "\\line ", RegexOptions.IgnoreCase);
                            if (!programTextOutline.StartsWith("\\par ")) {
                                programTextOutline = "\\par " + programTextOutline;
                            }
                            if (!programTextOutline.EndsWith("\\par ")) {
                                programTextOutline = programTextOutline + "\\par ";
                            }
                            objSW.WriteLine("{\\fs22 " + programTextOutline + "}");
                        }
                    } else {
                        //GET PROGRAM OUTLINE

                        //loop through degrees
                        for (Int32 intDeg = 0; intDeg < dsProgramDegree.Tables[0].Rows.Count; intDeg++) {
                            Int32 programDegreeID = Convert.ToInt32(dsProgramDegree.Tables[0].Rows[intDeg]["ProgramDegreeID"]);
                            String degreeShortTitle = dsProgramDegree.Tables[0].Rows[intDeg]["DegreeShortTitle"].ToString().Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ");

                            objSW.WriteLine("{\\fs26\\par\\b " + degreeShortTitle + "\\par}");

                            //get the degree options
                            DataSet dsOption = GetProgOptions(programDegreeID);

                            //loop through the options
                            for (Int32 intOpt = 0; intOpt < dsOption.Tables[0].Rows.Count; intOpt++) {
                                Int32 optionID = Convert.ToInt32(dsOption.Tables[0].Rows[intOpt]["OptionID"]);
                                String strOptTitle = dsOption.Tables[0].Rows[intOpt]["OptionTitle"].ToString();
                                String strOptDesc = dsOption.Tables[0].Rows[intOpt]["OptionDescription"].ToString();
                                String optionStateApproval = dsOption.Tables[0].Rows[intOpt]["OptionStateApproval"].ToString();
                                String optionCIP = dsOption.Tables[0].Rows[intOpt]["CIP"].ToString();
                                String optionEPC = dsOption.Tables[0].Rows[intOpt]["EPC"].ToString();
                                String academicPlan = dsOption.Tables[0].Rows[intOpt]["ACAD_PLAN"].ToString();

                                if (strOptTitle != null && strOptTitle != "") {
                                    objSW.WriteLine("{\\par\\fs22\\b " + strOptTitle.Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ") + "\\b0\\par}");
                                }

                                if (optionStateApproval != "" || optionCIP != "" || optionEPC != "" || academicPlan != "") {
                                    objSW.WriteLine("{\\fs22 State Approval Date: " + optionStateApproval);
                                    objSW.WriteLine("  CIP: " + optionCIP);
                                    objSW.WriteLine("  EPC: " + optionEPC);
                                    objSW.WriteLine("  Academic Plan: " + academicPlan + "\\par}");
                                }

                                //DISPLAY Locations Here
                                DataSet dsOptionLocations = GetOptionLocations(optionID);
                                objSW.WriteLine("{\\par\\fs22\\b Offered at:\\b0");
                                String strLocations = "";
                                for (Int32 j = 0; j < dsOptionLocations.Tables[0].Rows.Count; j++) {
                                    strLocations += ",&nbsp;" + dsOptionLocations.Tables[0].Rows[j]["LocationTitle"].ToString();
                                }
                                if (strLocations != "") {
                                    objSW.WriteLine(strLocations.Substring(1).Replace("&nbsp;", " "));
                                }
                                objSW.WriteLine("\\par}");

                                if (strOptDesc != null && strOptDesc != "") {
                                    objSW.WriteLine("{\\par\\fs22 " + strOptDesc.Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ") + "\\par}");
                                }

                                //get the prerequisites
                                DataSet dsPrereq = GetOptPrereqs(optionID, programBeginSTRM, programEndSTRM);
                                Int32 intPrereqCount = dsPrereq.Tables[0].Rows.Count;
                                String oldCourseOffering = "";

                                if (intPrereqCount > 0) {
                                    objSW.WriteLine("{\\fs22\\par\\b Prerequisites:\\par}");

                                    //loop through prerequisites
                                    for (Int32 intPrereq = 0; intPrereq < intPrereqCount; intPrereq++) {
                                        String courseOffering = dsPrereq.Tables[0].Rows[intPrereq]["CourseOffering"].ToString();

                                        if (oldCourseOffering != courseOffering) {
                                            objSW.WriteLine("{\\*\\pn\\fs22\\endash " + " " + dsPrereq.Tables[0].Rows[intPrereq]["SUBJECT"].ToString());
                                            objSW.WriteLine("\\tx1000\\tab " + dsPrereq.Tables[0].Rows[intPrereq]["CATALOG_NBR"].ToString());
                                            objSW.WriteLine("\\tx1800\\tab " + dsPrereq.Tables[0].Rows[intPrereq]["COURSE_TITLE_LONG"].ToString());
                                            objSW.WriteLine(" \\fs18\\i " + dsPrereq.Tables[0].Rows[intPrereq]["FootnoteNumber"].ToString() + "\\i0\\par}");
                                        }
                                        oldCourseOffering = courseOffering;
                                    }
                                }

                                //get option courses
                                DataRow[] drCourses = GetOptionCoursesAndElectives(optionID, programBeginSTRM, programEndSTRM);
                                Double dblTotalMinCred = 0, dblTotalMaxCred = 0, dblDegMinCred = 0, dblDegMaxCred = 0;
                                Int16 intQuarter = -1;
                                oldCourseOffering = "";

                                //loop through courses
                                for (Int32 intCrs = 0; intCrs < drCourses.Length; intCrs++) {
                                    String courseOffering = drCourses[intCrs]["CourseOffering"].ToString();

                                    if (oldCourseOffering != courseOffering || courseOffering == "ZZZ") {
                                        String strQuarter = drCourses[intCrs]["Quarter"].ToString();

                                        //determine quarter
                                        if (strQuarter != "") {
                                            if (intQuarter.ToString() != strQuarter) {
                                                intQuarter = Convert.ToInt16(strQuarter);
                                                if (intQuarter == 1) {
                                                    objSW.WriteLine("{\\par\\fs22\\b First Quarter\\b0\\par}");
                                                } else if (intQuarter == 2) {
                                                    objSW.WriteLine("{\\par\\fs22\\b Second Quarter\\b0\\par}");
                                                } else if (intQuarter == 3) {
                                                    objSW.WriteLine("{\\par\\fs22\\b Third Quarter\\b0\\par}");
                                                } else if (intQuarter == 4) {
                                                    objSW.WriteLine("{\\par\\fs22\\b Fourth Quarter\\b0\\par}");
                                                } else if (intQuarter == 5) {
                                                    objSW.WriteLine("{\\par\\fs22\\b Fifth Quarter\\b0\\par}");
                                                } else if (intQuarter == 6) {
                                                    objSW.WriteLine("{\\par\\fs22\\b Sixth Quarter\\b0\\par}");
                                                } else if (intQuarter == 7) {
                                                    objSW.WriteLine("{\\par\\fs22\\b Seventh Quarter\\b0\\par}");
                                                } else if (intQuarter == 8) {
                                                    objSW.WriteLine("{\\par\\fs22\\b Eighth Quarter\\b0\\par}");
                                                } else if (intQuarter == 9) {
                                                    objSW.WriteLine("{\\par\\fs22\\b Ninth Quarter\\b0\\par}");
                                                } else if (intQuarter == 10) {
                                                    objSW.WriteLine("{\\par\\fs22\\b Tenth Quarter\\b0\\par}");
                                                }

                                                dblTotalMinCred = 0;
                                                dblTotalMaxCred = 0;
                                            }
                                        } else if (intCrs == 0) {
                                            objSW.WriteLine("{\\par}");
                                        }

                                        Double unitsMinimum = 0, unitsMaximum = 0;
                                        if (Convert.ToByte(drCourses[intCrs]["OverwriteUnits"]) == 1) {
                                            unitsMinimum = Convert.ToDouble(drCourses[intCrs]["OverwriteUnitsMinimum"]);
                                            unitsMaximum = Convert.ToDouble(drCourses[intCrs]["OverwriteUnitsMaximum"]);
                                        } else {
                                            unitsMinimum = Convert.ToDouble(drCourses[intCrs]["UNITS_MINIMUM"]);
                                            unitsMaximum = Convert.ToDouble(drCourses[intCrs]["UNITS_MAXIMUM"]);
                                        }

                                        String strCredits = "";

                                        if (unitsMinimum == unitsMaximum) {
                                            strCredits = unitsMinimum.ToString();
                                            dblTotalMinCred += unitsMinimum;
                                            dblTotalMaxCred += unitsMinimum;
                                        } else {
                                            strCredits = unitsMinimum + "-" + unitsMaximum;
                                            dblTotalMinCred += unitsMinimum;
                                            dblTotalMaxCred += unitsMaximum;
                                        }

                                        objSW.WriteLine("{\\*\\pn\\fs22 " + drCourses[intCrs]["SUBJECT"].ToString());
                                        objSW.WriteLine("\\tx1000\\tab " + drCourses[intCrs]["CATALOG_NBR"].ToString());
                                        objSW.WriteLine("\\tx1800\\tab " + drCourses[intCrs]["COURSE_TITLE_LONG"].ToString());
                                        objSW.WriteLine(" \\fs24\\super " + drCourses[intCrs]["FootnoteNumber"].ToString() + "\\nosupersub");
                                        objSW.WriteLine("\\fs22\\tx8500\\tab " + strCredits + "\\par}");

                                    }

                                    if ((intCrs == (drCourses.Length - 1)) || (intCrs < (drCourses.Length - 1) && drCourses[intCrs + 1]["Quarter"].ToString() != drCourses[intCrs]["Quarter"].ToString())) {
                                        String strTotalCred = "";

                                        if (dblTotalMinCred < dblTotalMaxCred) {
                                            dblDegMinCred += dblTotalMinCred;
                                            dblDegMaxCred += dblTotalMaxCred;
                                            strTotalCred = dblTotalMinCred + "\\endash " + dblTotalMaxCred;
                                        } else {
                                            dblDegMinCred += dblTotalMinCred;
                                            dblDegMaxCred += dblTotalMinCred;
                                            strTotalCred = dblTotalMaxCred.ToString();
                                        }
                                        objSW.WriteLine("{\\*\\pn\\b\\fs21\\tab\\tab Total\\tab " + strTotalCred + "\\b0\\par}");
                                    }

                                    oldCourseOffering = courseOffering;
                                }

                                //display total credits for degree option
                                String strOptCr = "";

                                if (dblDegMinCred < dblDegMaxCred) {
                                    strOptCr = dblDegMinCred + "-" + dblDegMaxCred;
                                } else {
                                    strOptCr = dblDegMinCred.ToString();
                                }

                                if (dblDegMinCred != 0) {
                                    strOptCr += " credits are required for the " + degreeShortTitle;
                                } else {
                                    strOptCr = "";
                                }

                                if (strOptCr != "") {
                                    objSW.WriteLine("{\\par\\fs22\\b " + strOptCr + "\\b0\\par}");
                                }

                                //GET ELECTIVE GROUPS
                                DataSet dsElectGroup = GetOptionElectiveGroups(optionID);
                                for (Int32 intElectGroup = 0; intElectGroup < dsElectGroup.Tables[0].Rows.Count; intElectGroup++) {
                                    Int32 intElectCount = Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroup]["ElectiveCount"]);

                                    if (intElectCount > 0) {
                                        objSW.WriteLine("{\\par\\fs22\\b " + dsElectGroup.Tables[0].Rows[intElectGroup]["ElectiveGroupTitle"].ToString().Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ") + " \\fs24\\super " + dsElectGroup.Tables[0].Rows[intElectGroup]["FootnoteNumber"].ToString() + "\\nosupersub\\b0\\par}");

                                        //GET ELECTIVES
                                        DataSet dsElectiveGroupCourse = GetElectiveGroupCourses(Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroup]["OptionElectiveGroupID"]), programBeginSTRM, programEndSTRM);
                                        for (Int32 intElect = 0; intElect < dsElectiveGroupCourse.Tables[0].Rows.Count; intElect++) {
                                            Double dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["UNITS_MINIMUM"]);
                                            Double dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["UNITS_MAXIMUM"]);
                                            String strElectCredits = "";

                                            if (Convert.ToByte(dsElectiveGroupCourse.Tables[0].Rows[intElect]["OverwriteUnits"]) == 1) {
                                                dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["OverwriteUnitsMinimum"]);
                                                dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["OverwriteUnitsMaximum"]);
                                            }

                                            if (dblElectCredMin == dblElectCredMax) {
                                                strElectCredits = dblElectCredMin.ToString();
                                            } else {
                                                strElectCredits = dblElectCredMin + "-" + dblElectCredMax;
                                            }

                                            objSW.WriteLine("{\\*\\pn\\fs22 " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["SUBJECT"].ToString());
                                            objSW.WriteLine("\\tx1000\\tab " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["CATALOG_NBR"].ToString());
                                            objSW.WriteLine("\\tx1800\\tab " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["COURSE_TITLE_LONG"].ToString());
                                            objSW.WriteLine(" \\fs24\\super " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["FootnoteNumber"].ToString() + "\\nosupersub");
                                            objSW.WriteLine("\\fs22\\tx8500\\tab " + strElectCredits + "\\par}");
                                        }
                                    }
                                }

                                //GET THE FOOTNOTES
                                DataSet dsFootnote = GetOptFootnotes(optionID);
                                for (Int32 intFootnote = 0; intFootnote < dsFootnote.Tables[0].Rows.Count; intFootnote++) {
                                    if (intFootnote == 0) {
                                        objSW.WriteLine("{\\par}");
                                    }
                                    objSW.WriteLine("{\\fs24\\super " + dsFootnote.Tables[0].Rows[intFootnote]["FootnoteNumber"].ToString() + "\\nosupersub");
                                    objSW.WriteLine(" \\fs22 " + dsFootnote.Tables[0].Rows[intFootnote]["Footnote"].ToString().Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&amp;", "&").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ") + "\\par}");
                                }
                            }
                        }
                    }
                }

                objSW.Write("}");
                objSW.Close();
                objFS.Close();

                return "../Program.doc";

            } catch {
                return "";
            }
        }

        public String ExportToPrintableCatalog(String selectedSTRM, String collegeID) {

            try {

                FileStream objFS = File.Open(HttpContext.Current.Server.MapPath("../PrintableProgramOutlines.doc"), FileMode.Truncate);
                StreamWriter objSW = new StreamWriter(objFS);

                String rtf = "";

                DataSet dsPrograms = GetAreaOfStudyProgramTitles("1", selectedSTRM, collegeID); //get published programs for the selected quarter and college
                //set preliminaries
                objSW.Write("{\\rtf1\\ansi\\deff0 {\\fonttbl {\\f0 Arial;}}");
                rtf += "{\\rtf1\\ansi\\deff0 {\\fonttbl {\\f0 Arial;}}<br />";
                // set preliminary values
                objSW.Write("\\deflang1033 \\plain \\fs26 \\widowctrl \\margl1008 \\margr864 \\margt475 \\margb475 \\hyphauto \\ftnbj");
                rtf += "\\deflang1033 \\plain \\fs26 \\widowctrl \\margl1008 \\margr864 \\margt475 \\margb475 \\hyphauto \\ftnbj<br />";
                // Set the document title
                objSW.WriteLine("\\sectd \\par \\sa180 \\qc \\fs36 \\b Program Outlines \\par");
                rtf += "\\sectd \\par \\sa180 \\qc \\fs36 \\b Program Outlines \\par<br />";
                objSW.WriteLine("\\pard \\plain \\qc \\fs17 For the most current information on career and technical program offerings at SCC and SFCC, view our programs online at: ");
                rtf += "\\pard \\plain \\qc \\fs17 For the most current information on career and technical program offerings at SCC and SFCC, view our programs online at: <br />";
                objSW.WriteLine("\\sa180 \\qc \\fs17 \\field{\\*\\fldinst{HYPERLINK http://icatalog.ccs.spokane.edu/program/default.aspx }}{\\fldrslt{\\ul\\f0}} \\par \\line \\sect");
                rtf += "\\sa180 \\qc \\fs17 \\field{\\*\\fldinst{HYPERLINK http://icatalog.ccs.spokane.edu/program/default.aspx }}{\\fldrslt{\\ul\\f0}} \\par \\line \\sect<br />";
                objSW.Write("\\sectd \\sbknone \\cols2 \\colsx600 \\plain \\ql");
                rtf += "\\sectd \\sbknone \\cols2 \\colsx600 \\plain \\ql<br />";

                String oldAreaOfStudyTitle = "";

                //LOOP THROUGH THE PROGRAM(S)
                for (Int32 intDSRow = 0; intDSRow < dsPrograms.Tables[0].Rows.Count; intDSRow++)
                {
                    String areaOfStudyTitle = dsPrograms.Tables[0].Rows[intDSRow]["Title"].ToString();
                    String newAreaOfStudyTitle = areaOfStudyTitle.ToUpper() + ": " + dsPrograms.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
                    Int32 programVersionID = Convert.ToInt32(dsPrograms.Tables[0].Rows[intDSRow]["ProgramVersionID"]);

                    //objSW.WriteLine("{\\pard \\brdrt\\brdrs\\brdrw10\\brsp60\\fs17\\ql {\\b " + dsPrograms.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString() + "\\line");
                    if (newAreaOfStudyTitle != oldAreaOfStudyTitle)
                    {
                        objSW.WriteLine("{\\pard \\brdrt\\brdrs\\brdrw10\\brsp40\\fs17\\ql {\\b " + newAreaOfStudyTitle + "} \\brdrb \\brdrs \\brdrw10 \\brsp40 \\par}\\line");
                        rtf += "{\\pard \\brdrt\\brdrs\\brdrw10\\brsp40\\fs17\\ql {\\b " + newAreaOfStudyTitle + "} \\brdrb \\brdrs \\brdrw10 \\brsp40 \\par}\\line<br />";
                    }
                    objSW.WriteLine("\\fs17\\ql {\\b " + dsPrograms.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString() + " }\\line");
                    rtf += "\\fs17\\ql {\\b " + dsPrograms.Tables[0].Rows[intDSRow]["ProgramTitle"].ToString() + " pvid = " + programVersionID + " }\\line<br />";

                    //GET DEGREE LONG TITLE
                    DataSet dsProgramDegree = GetProgramDegree(programVersionID);
                    Int32 intDegCount = dsProgramDegree.Tables[0].Rows.Count;
                    String degreeLongTitle = dsProgramDegree.Tables[0].Rows[0]["DegreeLongTitle"].ToString().Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ");
                    if (intDegCount > 0)
                    {
                        objSW.WriteLine("{\\fs17 \\ql \\b " + degreeLongTitle + "}\\line");
                        rtf += "{\\fs17 \\ql \\b " + degreeLongTitle + "}\\line<br />";
                    }

                    //GET THE BEGIN AND END TERM
                    String beginTermDescription = "", endTermDescription = "";
                    String programBeginSTRM = dsPrograms.Tables[0].Rows[intDSRow]["ProgramBeginSTRM"].ToString();
                    String programEndSTRM = dsPrograms.Tables[0].Rows[intDSRow]["ProgramEndSTRM"].ToString();

                    termData csTerm = new termData();
                    if (programEndSTRM == "Z999")
                    {
                        endTermDescription = "Undefined";
                    }
                    else
                    {
                        endTermDescription = csTerm.GetTermDescription(programEndSTRM);
                    }
                    beginTermDescription = csTerm.GetTermDescription(programBeginSTRM);

                    //objSW.WriteLine("{\\par\\fs22 " + programBeginSTRM + " (" + beginTermDescription + ") - " + programEndSTRM + " (" + endTermDescription + ")\\par}");

                    //GET PROGRAM DESCRIPTION
                    DataSet dsProgramDescription = GetProgramDescription(programVersionID);
                    if (dsProgramDescription.Tables[0].Rows.Count > 0)
                    {
                        String programDescription = Regex.Replace(dsProgramDescription.Tables[0].Rows[0]["ProgramDescription"].ToString(), "</P>", "\\line ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "\n", "");
                        programDescription = Regex.Replace(programDescription, "\\n", "");
                        programDescription = Regex.Replace(programDescription, "\r", "");
                        programDescription = Regex.Replace(programDescription, "\\r", "");
                        programDescription = programDescription.Replace("-", "\\endash ").Replace("�", "\\endash ").Replace("�", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&#39;", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ").Replace("&nbsp;", " ");
                        programDescription = Regex.Replace(programDescription, "<P>", "", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "</P>", "\\line", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<BR></STRONG>", "</STRONG><BR>", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<BR /></STRONG>", "</STRONG><BR />", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<STRONG>", "\\b ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "</STRONG>", "\\b0 ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "&nbsp;", " ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "  ", " ");
                        programDescription = Regex.Replace(programDescription, "   ", " ");
                        programDescription = Regex.Replace(programDescription, "	", " ");
                        programDescription = Regex.Replace(programDescription, "<UL>", "\\line\\pard\\par}{\\*\\pn\\pnlvlblt\\fs17\\pnindent200{\\pntxtb\\bullet\\fs17}\\fi-200\\li400", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<LI>", "", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "</LI>", "\\par", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "</UL>", "\\pard\\par}{", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<P></P>", "", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<BR>", "\\line ", RegexOptions.IgnoreCase);
                        programDescription = Regex.Replace(programDescription, "<br />", "\\line ", RegexOptions.IgnoreCase);
                        programDescription = programDescription.Trim();

                        if (!programDescription.StartsWith("\\line"))
                        {
                            programDescription = "\\line " + programDescription;
                        }
                        if (!programDescription.EndsWith("\\line"))
                        {
                            programDescription = programDescription + "\\line ";
                        }
                        objSW.WriteLine("{\\fs17\\ql " + programDescription + "}");
                        rtf += "{\\fs17\\ql " + programDescription + "}<br />";
                    }

                    //CHECK IF PROGRAM CONTAINS COURSE OUTLINE OR TEXT OUTLINE
                    if (dsPrograms.Tables[0].Rows[intDSRow]["ProgramDisplay"].ToString() == "2")
                    {
                        //GET TEXT OUTLINE
                        String programTextOutline = GetProgramTextOutline(programVersionID);
                        if (programTextOutline != null && programTextOutline != "")
                        {
                            programTextOutline = Regex.Replace(programTextOutline, "\n", "");
                            programTextOutline = Regex.Replace(programTextOutline, "\\n", "");
                            programTextOutline = Regex.Replace(programTextOutline, "\r", "");
                            programTextOutline = Regex.Replace(programTextOutline, "\\r", "");
                            programTextOutline = programTextOutline.Replace("-", "\\endash ").Replace("�", "\\endash ").Replace("�", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&#39;", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ").Replace("&nbsp;", " ");
                            programTextOutline = Regex.Replace(programTextOutline, "<P>", "", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "</P>", "\\line ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<BR></STRONG>", "</STRONG><BR>", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<BR /></STRONG>", "</STRONG><BR />", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<STRONG>", "\\b ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "</STRONG>", "\\b0 ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "&nbsp;", " ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "  ", " ");
                            programTextOutline = Regex.Replace(programTextOutline, "   ", " ");
                            programTextOutline = Regex.Replace(programTextOutline, "	", " ");
                            programTextOutline = Regex.Replace(programTextOutline, "<UL>", "\\line\\pard\\par}{\\*\\pn\\pnlvlblt\\fs17\\pnindent200{\\pntxtb\\bullet\\fs17}\\fi-200\\li400 ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<LI>", "", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "</LI>", "\\par", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "</UL>", "\\pard\\par}{", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<P></P>", "", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<BR>", "\\line ", RegexOptions.IgnoreCase);
                            programTextOutline = Regex.Replace(programTextOutline, "<br />", "\\line ", RegexOptions.IgnoreCase);
                            programTextOutline = programTextOutline.Trim();
                            if (!programTextOutline.StartsWith("\\line"))
                            {
                                programTextOutline = "\\line " + programTextOutline;
                            }
                            if (!programTextOutline.EndsWith("\\line"))
                            {
                                programTextOutline = programTextOutline + "\\line ";
                            }
                            objSW.WriteLine("{\\fs17\\ql " + programTextOutline + "}");
                            rtf += "{\\fs17\\ql " + programTextOutline + "}<br />";

                            objSW.WriteLine("{\\pard\\par}");
                            rtf += "{\\pard\\par}<br />";
                        }
                    }
                    else
                    {
                        //GET PROGRAM OUTLINE

                        //loop through degrees
                        for (Int32 intDeg = 0; intDeg < dsProgramDegree.Tables[0].Rows.Count; intDeg++)
                        {
                            Int32 programDegreeID = Convert.ToInt32(dsProgramDegree.Tables[0].Rows[intDeg]["ProgramDegreeID"]);
                            String degreeShortTitle = dsProgramDegree.Tables[0].Rows[intDeg]["DegreeShortTitle"].ToString().Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&#39;", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ").Replace("&nbsp;", " ");

                            //objSW.WriteLine("{\\fs17 \\ql \\b " + degreeShortTitle + "}");

                            //get the degree options
                            DataSet dsOption = GetProgOptions(programDegreeID);

                            //loop through the options
                            for (Int32 intOpt = 0; intOpt < dsOption.Tables[0].Rows.Count; intOpt++)
                            {
                                Int32 optionID = Convert.ToInt32(dsOption.Tables[0].Rows[intOpt]["OptionID"]);
                                String strOptTitle = dsOption.Tables[0].Rows[intOpt]["OptionTitle"].ToString();
                                String strOptDesc = dsOption.Tables[0].Rows[intOpt]["OptionDescription"].ToString();
                                String optionStateApproval = dsOption.Tables[0].Rows[intOpt]["OptionStateApproval"].ToString();
                                String optionCIP = dsOption.Tables[0].Rows[intOpt]["CIP"].ToString();
                                String optionEPC = dsOption.Tables[0].Rows[intOpt]["EPC"].ToString();
                                String academicPlan = dsOption.Tables[0].Rows[intOpt]["ACAD_PLAN"].ToString();

                                if (strOptTitle != null && strOptTitle != "")
                                {
                                    objSW.WriteLine("{\\line\\fs17\\b\\ql " + strOptTitle.Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&#39;", "'").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ").Replace("&nbsp;", " ") + "\\b0\\line}");
                                    rtf += "{\\line\\fs17\\b\\ql " + strOptTitle.Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&#39;", "'").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ").Replace("&nbsp;", " ") + "\\b0\\line}<br />";
                                }

                                //DISPLAY Locations Here
                                /*
							    DataSet dsOptionLocations = GetOptionLocations(optionID);
							    objSW.WriteLine("{\\par\\fs22\\b Offered at:\\b0");
							    String strLocations = "";
							    for(Int32 j = 0; j < dsOptionLocations.Tables[0].Rows.Count; j++){
								    strLocations += ",&nbsp;" + dsOptionLocations.Tables[0].Rows[j]["LocationTitle"].ToString();
							    }
							    if(strLocations != ""){
								    objSW.WriteLine(strLocations.Substring(1).Replace("&nbsp;"," "));
							    }
							    objSW.WriteLine("\\par}");
                                */

                                if (strOptDesc != null && strOptDesc != "")
                                {
                                    strOptDesc = strOptDesc.Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&#39;", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ").Replace("&nbsp;", " ").Replace("<br />", "\\line ");
                                    strOptDesc = strOptDesc.Trim();
                                    if (!strOptDesc.StartsWith("\\line"))
                                    {
                                        strOptDesc = " \\line " + strOptDesc;
                                    }
                                    if (!strOptDesc.EndsWith("\\line"))
                                    {
                                        strOptDesc += " \\line ";
                                    }
                                    objSW.WriteLine("{\\fs17\\ql " + strOptDesc + "}");

                                    rtf += "{\\fs17\\ql " + strOptDesc + "}<br />";
                                }

                                //get the prerequisites
                                DataSet dsPrereq = GetOptPrereqs(optionID, programBeginSTRM, programEndSTRM);
                                Int32 intPrereqCount = dsPrereq.Tables[0].Rows.Count;
                                String oldCourseOffering = "";

                                if (intPrereqCount > 0)
                                {
                                    //objSW.WriteLine("{\\fs17\\b\\ql Prerequisites\\line}");
                                    objSW.WriteLine("{\\line\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Prerequisites\\b0\\cell\\row}");
                                    rtf += "{\\line\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Prerequisites\\b0\\cell\\row}<br />";
                                    //loop through prerequisites
                                    for (Int32 intPrereq = 0; intPrereq < intPrereqCount; intPrereq++)
                                    {
                                        String courseOffering = dsPrereq.Tables[0].Rows[intPrereq]["CourseOffering"].ToString();

                                        if (oldCourseOffering != courseOffering)
                                        {
                                            /*
                                            objSW.WriteLine("{\\*\\pn\\fs17\\endash " + " " + dsPrereq.Tables[0].Rows[intPrereq]["SUBJECT"].ToString());
                                            objSW.WriteLine("\\tx800\\tab " + dsPrereq.Tables[0].Rows[intPrereq]["CATALOG_NBR"].ToString());
                                            objSW.WriteLine("\\tx1200\\tab " + dsPrereq.Tables[0].Rows[intPrereq]["COURSE_TITLE_LONG"].ToString());
                                            objSW.WriteLine(" \\fs18\\i " + dsPrereq.Tables[0].Rows[intPrereq]["FootnoteNumber"].ToString() + "\\i0\\line}");
                                            */
                                            objSW.WriteLine("{\\trowd\\trgaph");
                                            rtf += "{\\trowd\\trgaph<br />";
                                            objSW.WriteLine("\\cellx750\\cellx1250\\cellx4400");
                                            rtf += "\\cellx750\\cellx1250\\cellx4400<br />";
                                            objSW.WriteLine("\\pard\\fs17\\ql\\intbl " + dsPrereq.Tables[0].Rows[intPrereq]["SUBJECT"].ToString() + "\\cell");
                                            rtf += "\\pard\\fs17\\ql\\intbl " + dsPrereq.Tables[0].Rows[intPrereq]["SUBJECT"].ToString() + "\\cell<br />";
                                            objSW.WriteLine("\\pard\\fs17\\ql\\intbl " + dsPrereq.Tables[0].Rows[intPrereq]["CATALOG_NBR"].ToString() + "\\cell");
                                            rtf += "\\pard\\fs17\\ql\\intbl " + dsPrereq.Tables[0].Rows[intPrereq]["CATALOG_NBR"].ToString() + "\\cell<br />";
                                            objSW.WriteLine("\\pard\\fs17\\ql\\intbl " + dsPrereq.Tables[0].Rows[intPrereq]["COURSE_TITLE_LONG"].ToString() + "{ \\fs21\\super " + dsPrereq.Tables[0].Rows[intPrereq]["FootnoteNumber"].ToString() + "\\nosupersub}\\cell\\row}");
                                            rtf += "\\pard\\fs17\\ql\\intbl " + dsPrereq.Tables[0].Rows[intPrereq]["COURSE_TITLE_LONG"].ToString() + "{ \\fs21\\super " + dsPrereq.Tables[0].Rows[intPrereq]["FootnoteNumber"].ToString() + "\\nosupersub}\\cell\\row}<br />";
                                        }
                                        oldCourseOffering = courseOffering;
                                    }
                                }

                                //get option courses
                                DataRow[] drCourses = GetOptionCoursesAndElectives(optionID, programBeginSTRM, programEndSTRM);
                                Double dblTotalMinCred = 0, dblTotalMaxCred = 0, dblDegMinCred = 0, dblDegMaxCred = 0;
                                Int16 intQuarter = -1;
                                oldCourseOffering = "";

                                //loop through courses
                                for (Int32 intCrs = 0; intCrs < drCourses.Length; intCrs++)
                                {
                                    String courseOffering = drCourses[intCrs]["CourseOffering"].ToString();

                                    if (oldCourseOffering != courseOffering || courseOffering == "ZZZ")
                                    {
                                        String strQuarter = drCourses[intCrs]["Quarter"].ToString();
                                        if (strQuarter == null || strQuarter == "")
                                        { //added to display a Courses heading
                                            strQuarter = "0";
                                        }

                                        //determine quarter
                                        if (strQuarter != "")
                                        {
                                            if (intQuarter.ToString() != strQuarter)
                                            {
                                                intQuarter = Convert.ToInt16(strQuarter);
                                                if (intQuarter == 1)
                                                {
                                                    objSW.WriteLine("{\\line\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl First Quarter\\b0\\cell\\row}");
                                                    rtf += "{\\line\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl First Quarter\\b0\\cell\\row}<br />";
                                                }
                                                else if (intQuarter == 2)
                                                {
                                                    objSW.WriteLine("{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Second Quarter\\b0\\cell\\row}");
                                                    rtf += "{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Second Quarter\\b0\\cell\\row}<br />";
                                                }
                                                else if (intQuarter == 3)
                                                {
                                                    objSW.WriteLine("{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Third Quarter\\b0\\cell\\row}");
                                                    rtf += "{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Third Quarter\\b0\\cell\\row}<br />";
                                                }
                                                else if (intQuarter == 4)
                                                {
                                                    objSW.WriteLine("{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Fourth Quarter\\b0\\cell\\row}");
                                                    rtf += "{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Fourth Quarter\\b0\\cell\\row}<br />";
                                                }
                                                else if (intQuarter == 5)
                                                {
                                                    objSW.WriteLine("{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Fifth Quarter\\b0\\cell\\row}");
                                                    rtf += "{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Fifth Quarter\\b0\\cell\\row}<br />";
                                                }
                                                else if (intQuarter == 6)
                                                {
                                                    objSW.WriteLine("{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Sixth Quarter\\b0\\cell\\row}");
                                                    rtf += "{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Sixth Quarter\\b0\\cell\\row}<br />";
                                                }
                                                else if (intQuarter == 7)
                                                {
                                                    objSW.WriteLine("{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Seventh Quarter\\b0\\cell\\row}");
                                                    rtf += "{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Seventh Quarter\\b0\\cell\\row}<br />";
                                                }
                                                else if (intQuarter == 8)
                                                {
                                                    objSW.WriteLine("{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Eighth Quarter\\b0\\cell\\row}");
                                                    rtf += "{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Eighth Quarter\\b0\\cell\\row}<br />";
                                                }
                                                else if (intQuarter == 9)
                                                {
                                                    objSW.WriteLine("{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Ninth Quarter\\b0\\cell\\row}");
                                                    rtf += "{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Ninth Quarter\\b0\\cell\\row}<br />";
                                                }
                                                else if (intQuarter == 10)
                                                {
                                                    objSW.WriteLine("{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Tenth Quarter\\b0\\cell\\row}");
                                                    rtf += "{\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Tenth Quarter\\b0\\cell\\row}<br />";
                                                }
                                                else if (intQuarter == 0)
                                                {
                                                    objSW.WriteLine("{\\line\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Courses\\b0\\cell\\row}");
                                                    rtf += "{\\line\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl Courses\\b0\\cell\\row}<br />";
                                                }

                                                dblTotalMinCred = 0;
                                                dblTotalMaxCred = 0;
                                            }
                                        }
                                        else if (intCrs == 0)
                                        { //may not be used now
                                            objSW.WriteLine("{\\par}");
                                            rtf += "{\\par}<br />";
                                        }

                                        Double unitsMinimum = 0, unitsMaximum = 0;
                                        if (Convert.ToByte(drCourses[intCrs]["OverwriteUnits"]) == 1)
                                        {
                                            //unitsMinimum = Convert.ToDouble(drCourses[intCrs]["OverwriteUnitsMinimum"]);
                                            //unitsMaximum = Convert.ToDouble(drCourses[intCrs]["OverwriteUnitsMaximum"]);
                                            unitsMinimum = 1;
                                            unitsMaximum = 1;
                                        }
                                        else
                                        {
                                            unitsMinimum = Convert.ToDouble(drCourses[intCrs]["UNITS_MINIMUM"]);
                                            unitsMaximum = Convert.ToDouble(drCourses[intCrs]["UNITS_MAXIMUM"]);
                                        }

                                        String strCredits = "";

                                        if (unitsMinimum == unitsMaximum)
                                        {
                                            strCredits = unitsMinimum.ToString();
                                            dblTotalMinCred += unitsMinimum;
                                            dblTotalMaxCred += unitsMinimum;
                                        }
                                        else
                                        {
                                            strCredits = unitsMinimum + "-" + unitsMaximum;
                                            dblTotalMinCred += unitsMinimum;
                                            dblTotalMaxCred += unitsMaximum;
                                        }

                                        objSW.WriteLine("{\\trowd\\trgaph");
                                        rtf += "{\\trowd\\trgaph<br />";
                                        objSW.WriteLine("\\cellx750\\cellx1250\\cellx4400\\cellx4900");
                                        rtf += "\\cellx750\\cellx1250\\cellx4400\\cellx4900<br />";
                                        objSW.WriteLine("\\pard\\fs17\\ql\\intbl " + drCourses[intCrs]["SUBJECT"].ToString() + "\\cell");
                                        rtf += "\\pard\\fs17\\ql\\intbl " + drCourses[intCrs]["SUBJECT"].ToString() + "\\cell<br />";
                                        objSW.WriteLine("\\pard\\fs17\\ql\\intbl " + drCourses[intCrs]["CATALOG_NBR"].ToString() + "\\cell");
                                        rtf += "\\pard\\fs17\\ql\\intbl " + drCourses[intCrs]["CATALOG_NBR"].ToString() + "\\cell<br />";
                                        objSW.WriteLine("\\pard\\fs17\\ql\\intbl " + drCourses[intCrs]["COURSE_TITLE_LONG"].ToString() + "{ \\fs21\\super " + drCourses[intCrs]["FootnoteNumber"].ToString() + "\\nosupersub}\\cell");
                                        rtf += "\\pard\\fs17\\ql\\intbl " + drCourses[intCrs]["COURSE_TITLE_LONG"].ToString() + "{ \\fs21\\super " + drCourses[intCrs]["FootnoteNumber"].ToString() + "\\nosupersub}\\cell<br />";
                                        objSW.WriteLine("\\pard\\fs17\\qr\\intbl " + strCredits + "\\cell\\row}");
                                        rtf += "\\pard\\fs17\\qr\\intbl " + strCredits + "\\cell\\row}<br />";

                                    }

                                    if ((intCrs == (drCourses.Length - 1)) || (intCrs < (drCourses.Length - 1) && drCourses[intCrs + 1]["Quarter"].ToString() != drCourses[intCrs]["Quarter"].ToString()))
                                    {
                                        String strTotalCred = "";

                                        if (dblTotalMinCred < dblTotalMaxCred)
                                        {
                                            dblDegMinCred += dblTotalMinCred;
                                            dblDegMaxCred += dblTotalMaxCred;
                                            strTotalCred = dblTotalMinCred + "\\endash " + dblTotalMaxCred;
                                        }
                                        else
                                        {
                                            dblDegMinCred += dblTotalMinCred;
                                            dblDegMaxCred += dblTotalMinCred;
                                            strTotalCred = dblTotalMaxCred.ToString();
                                        }

                                        objSW.WriteLine("{\\trowd\\trgaph\\cellx750\\cellx1250\\cellx4400\\cellx4900\\pard\\intbl\\cell\\pard\\intbl\\cell\\pard\\b\\fs17\\intbl Total\\cell\\pard\\qr\\intbl " + strTotalCred + "\\cell\\row}\\line");
                                        rtf += "{\\trowd\\trgaph\\cellx750\\cellx1250\\cellx4400\\cellx4900\\pard\\intbl\\cell\\pard\\intbl\\cell\\pard\\b\\fs17\\intbl Total\\cell\\pard\\qr\\intbl " + strTotalCred + "\\cell\\row}\\line<br />";
                                    }

                                    oldCourseOffering = courseOffering;
                                }

                                //display total credits for degree option
                                String strOptCr = "";

                                if (dblDegMinCred < dblDegMaxCred)
                                {
                                    strOptCr = dblDegMinCred + "-" + dblDegMaxCred;
                                }
                                else
                                {
                                    strOptCr = dblDegMinCred.ToString();
                                }

                                if (dblDegMinCred != 0)
                                {
                                    strOptCr += " credits are required for the " + degreeLongTitle;
                                }
                                else
                                {
                                    strOptCr = "";
                                }

                                if (strOptCr != "")
                                {
                                    objSW.WriteLine("{\\pard\\fs17\\b\\ql " + strOptCr + "\\b0\\par}");
                                    rtf += "{\\pard\\fs17\\b\\ql " + strOptCr + "\\b0\\par}<br />";
                                }

                                //GET ELECTIVE GROUPS
                                DataSet dsElectGroup = GetOptionElectiveGroups(optionID);
                                for (Int32 intElectGroup = 0; intElectGroup < dsElectGroup.Tables[0].Rows.Count; intElectGroup++)
                                {
                                    Int32 intElectCount = Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroup]["ElectiveCount"]);

                                    if (intElectCount > 0)
                                    {
                                        objSW.WriteLine("{\\line\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl " + dsElectGroup.Tables[0].Rows[intElectGroup]["ElectiveGroupTitle"].ToString().Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ") + " \\b0{\\fs21\\super " + dsElectGroup.Tables[0].Rows[intElectGroup]["FootnoteNumber"].ToString() + " \\nosupersub}\\cell\\row}");
                                        rtf += "{\\line\\trowd\\trgaph\\cellx4400\\fs17\\b\\ql\\pard\\intbl" + dsElectGroup.Tables[0].Rows[intElectGroup]["ElectiveGroupTitle"].ToString().Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ") + " \\b0{\\fs21\\super " + dsElectGroup.Tables[0].Rows[intElectGroup]["FootnoteNumber"].ToString() + " \\nosupersub}\\cell\\row}<br />";

                                        //GET ELECTIVES
                                        DataSet dsElectiveGroupCourse = GetElectiveGroupCourses(Convert.ToInt32(dsElectGroup.Tables[0].Rows[intElectGroup]["OptionElectiveGroupID"]), programBeginSTRM, programEndSTRM);
                                        for (Int32 intElect = 0; intElect < dsElectiveGroupCourse.Tables[0].Rows.Count; intElect++)
                                        {
                                            Double dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["UNITS_MINIMUM"]);
                                            Double dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["UNITS_MAXIMUM"]);
                                            String strElectCredits = "";

                                            if (Convert.ToByte(dsElectiveGroupCourse.Tables[0].Rows[intElect]["OverwriteUnits"]) == 1)
                                            {
                                                dblElectCredMin = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["OverwriteUnitsMinimum"]);
                                                dblElectCredMax = Convert.ToDouble(dsElectiveGroupCourse.Tables[0].Rows[intElect]["OverwriteUnitsMaximum"]);
                                            }

                                            if (dblElectCredMin == dblElectCredMax)
                                            {
                                                strElectCredits = dblElectCredMin.ToString();
                                            }
                                            else
                                            {
                                                strElectCredits = dblElectCredMin + "-" + dblElectCredMax;
                                            }

                                            objSW.WriteLine("{\\trowd\\trgaph");
                                            rtf += "{\\trowd\\trgaph<br />";
                                            objSW.WriteLine("\\cellx750\\cellx1250\\cellx4400\\cellx4900");
                                            rtf += "\\cellx750\\cellx1250\\cellx4400\\cellx4900<br />";
                                            objSW.WriteLine("\\pard\\fs17\\ql\\intbl " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["SUBJECT"].ToString() + "\\cell");
                                            rtf += "\\pard\\fs17\\ql\\intbl " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["SUBJECT"].ToString() + "\\cell<br />";
                                            objSW.WriteLine("\\pard\\fs17\\ql\\intbl " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["CATALOG_NBR"].ToString() + "\\cell");
                                            rtf += "\\pard\\fs17\\ql\\intbl " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["CATALOG_NBR"].ToString() + "\\cell<br />";
                                            objSW.WriteLine("\\pard\\fs17\\ql\\intbl " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["COURSE_TITLE_LONG"].ToString() + "{ \\fs24\\super " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["FootnoteNumber"].ToString() + "\\nosupersub}\\cell");
                                            rtf += "\\pard\\fs17\\ql\\intbl " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["COURSE_TITLE_LONG"].ToString() + "{ \\fs24\\super " + dsElectiveGroupCourse.Tables[0].Rows[intElect]["FootnoteNumber"].ToString() + "\\nosupersub}\\cell<br />";
                                            objSW.WriteLine("\\pard\\fs17\\qr\\intbl " + strElectCredits + "\\cell\\row}");
                                            rtf += "\\pard\\fs17\\qr\\intbl " + strElectCredits + "\\cell\\row}<br />";
                                        }
                                    }
                                }

                                //GET THE FOOTNOTES
                                DataSet dsFootnote = GetOptFootnotes(optionID);
                                for (Int32 intFootnote = 0; intFootnote < dsFootnote.Tables[0].Rows.Count; intFootnote++)
                                {
                                    if (intFootnote == 0)
                                    {
                                        objSW.WriteLine("{\\trowd\\trgaph\\cellx4400\\fs17\\ql\\pard\\intbl\\cell\\row}");
                                        rtf += "{\\trowd\\trgaph\\cellx4400\\fs17\\ql\\pard\\intbl\\cell\\row}<br />";
                                    }

                                    objSW.WriteLine("{\\trowd\\trgaph");
                                    rtf += "{\\trowd\\trgaph<br />";
                                    objSW.WriteLine("\\cellx200\\cellx4900");
                                    rtf += "\\cellx200\\cellx4900<br />";
                                    objSW.WriteLine("\\pard\\intbl {\\fs21\\ql\\super " + dsFootnote.Tables[0].Rows[intFootnote]["FootnoteNumber"].ToString() + " \\nosupersub}\\cell");
                                    rtf += "\\pard\\intbl {\\fs21\\ql\\super " + dsFootnote.Tables[0].Rows[intFootnote]["FootnoteNumber"].ToString() + " \\nosupersub}\\cell<br />";
                                    objSW.WriteLine("\\pard\\fs17\\ql\\intbl " + dsFootnote.Tables[0].Rows[intFootnote]["Footnote"].ToString().Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&amp;", "&").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ") + "\\cell\\row}");
                                    rtf += "\\pard\\fs17\\ql\\intbl " + dsFootnote.Tables[0].Rows[intFootnote]["Footnote"].ToString().Replace("�", "\\endash ").Replace("-", "\\endash ").Replace("&mdash;", "\\endash ").Replace("&ndash;", "\\endash ").Replace("�", "\"").Replace("�", "\"").Replace("�", "'").Replace("&amp;", "&").Replace("&quot;", "\"").Replace("&rsquo;", "\\rquote ").Replace("&lsquo;", "\\lquote ").Replace("&ldquo;", "\\ldblquote ").Replace("&rdquo;", "\\rdblquote ") + "\\cell\\row}<br />";
                                }
                                objSW.WriteLine("{\\pard\\par}");
                                rtf += "{\\pard\\par}<br />";
                            }
                        }
                    }

                    oldAreaOfStudyTitle = newAreaOfStudyTitle;
                }
                //end of columns section
                objSW.Write("\\sect");
                rtf += "\\sect<br />";
                //end of document
                objSW.Write("}");
                rtf += "}<br />";

                objSW.Close();
                objFS.Close();

                return "../PrintableProgramOutlines.doc";
                //return rtf;
            } catch {
                return "";
            }
        }


        /// <summary>
        /// Returns a list of programs containing the specified course
        /// </summary>
        /// <param name="courseOffering">Course Subject Area padded to five characters + course suffix + Catalog Nbr (Example: ENG  101)</param>
        /// <param name="termSpan">Begin and End Term Code separated by a comma (Example: 2171,2173)</param>
        /// <returns>
        /// DataSet
        /// Field List: ProgramVersionID - Program primary key
        ///             CollegeShortTitle - College abbreviation (SCC, SFCC)
        ///             ProgramTitle - Program title
        ///             ProgramBeginSTRM - Program begin term code (STRM)
        ///             BeginTerm_DESCR - Program begin term description (Example: Fall 2017)
        ///             ProgramEndSTRM - Program end term code (STRM)
        ///             EndTerm_DESCR - Program end term description (Example: Fall 2017)
        ///             PublishedProgram - 0 = Working Copy / not displayed to the public, 1 = Published Copy / displayed to the public
        ///             PublishedProgramVersionID - Stores the primary key of the Published Program Copy when an Working Copy of that program is created for editing purposes
        ///             ProgramDisplay - Identifies whether the program will have a program outline or large text description: 1 = program outline, 2 =  large text description
        /// </returns>
        public DataSet GetProgramsContainingCourse(String courseOffering, String termSpan) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetProgramsContainingCourse", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                //objCmd.Parameters.Add("@CourseOffering", SqlDbType.VarChar);
                objCmd.Parameters.AddWithValue("@CourseOffering", courseOffering);

                if (termSpan.IndexOf(',') > -1) {
                    String beginSTRM = termSpan.Substring(0, termSpan.IndexOf(","));
                    String endSTRM = termSpan.Substring(termSpan.IndexOf(",") + 1);

                    objCmd.Parameters.Add("@BeginSTRM", SqlDbType.VarChar);
                    objCmd.Parameters["@BeginSTRM"].Value = beginSTRM;

                    objCmd.Parameters.Add("@EndSTRM", SqlDbType.VarChar);
                    objCmd.Parameters["@EndSTRM"].Value = endSTRM;
                }

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }


        /// <summary>
        /// Returns a list of programs containing the text searched
        /// </summary>
        /// <param name="searchText">Text string you are searching for</param>
        /// <returns>
        /// DataSet
        /// Field List: ProgramVersionID - Program primary key
        ///             CollegeShortTitle - College abbreviation (SCC, SFCC)
        ///             ProgramTitle - Program title
        ///             ProgramBeginSTRM - Program begin term code (STRM)
        ///             BeginTerm_DESCR - Program begin term description (Example: Fall 2017)
        ///             ProgramEndSTRM - Program end term code (STRM)
        ///             EndTerm_DESCR - Program end term description (Example: Fall 2017)
        ///             PublishedProgram - 0 = Working Copy / not displayed to the public, 1 = Published Copy / displayed to the public
        ///             PublishedProgramVersionID - Stores the primary key of the Published Program Copy when a Working Copy of that program is created for editing purposes
        ///             ProgramDisplay - Identifies whether the program will have a program outline or large text description: 1 = program outline, 2 =  large text description
        /// </returns>
        public DataSet GetProgramsContainingText(String searchText) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetProgramsContainingText", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.AddWithValue("@searchText", searchText);
                //objCmd.Parameters["@searchText"].Value = searchText;

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetDegreeRequirementWorksheetList() {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT DocumentID, FileName, DegreeRequirementWorksheet.DocumentTitle, EffectiveYear, Count(Degree.DocumentTitle) AS DegreeCount " +
                         "FROM DegreeRequirementWorksheet LEFT OUTER JOIN Degree " +
                         "ON Degree.DocumentTitle = DegreeRequirementWorksheet.DocumentTitle " +
                         "GROUP BY DocumentID, FileName, DegreeRequirementWorksheet.DocumentTitle, EffectiveYear " +
                         "ORDER BY EffectiveYear, DegreeRequirementWorksheet.DocumentTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetDocumentTitles() {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT DocumentTitle " +
                    "FROM DegreeRequirementWorksheet " +
                    "GROUP BY DocumentTitle " +
                    "ORDER BY DocumentTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetEffectiveYears(String documentTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT EffectiveYear " +
                    "FROM DegreeRequirementWorksheet " +
                    "WHERE DocumentTitle = '" + documentTitle +
                    "' ORDER BY EffectiveYear;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }

        public Int16 AddDegreeRequirementWorksheet(String effectiveYear, String fileName, String documentTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();
                strSQL = "SELECT COUNT(*) " +
                         "FROM DegreeRequirementWorksheet " +
                         "WHERE EffectiveYear = '" + effectiveYear +
                         "' AND DocumentTitle = '" + documentTitle +
                         "' AND FileName <> '" + fileName + "';";

                objCmd = new SqlCommand(strSQL, objCon);
                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return 2;
                } else {
                    strSQL = "SELECT COUNT(*) " +
                        "FROM DegreeRequirementWorksheet " +
                        "WHERE FileName = '" + fileName + "';";

                    objCmd = new SqlCommand(strSQL, objCon);
                    if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                        strSQL = "UPDATE DegreeRequirementWorksheet " +
                            "SET DocumentTitle = '" + documentTitle +
                            "', EffectiveYear = '" + effectiveYear +
                            "' WHERE FileName = '" + fileName + "';";
                        objCmd = new SqlCommand(strSQL, objCon);
                        if (objCmd.ExecuteNonQuery() > 0) {
                            return 1;
                        } else {
                            return 0;
                        }
                    } else {
                        strSQL = "INSERT INTO DegreeRequirementWorksheet(EffectiveYear, FileName, DocumentTitle) " +
                            "VALUES ('" + effectiveYear + "', '" + fileName + "', '" + documentTitle + "');";

                        objCmd = new SqlCommand(strSQL, objCon);
                        if (objCmd.ExecuteNonQuery() > 0) {
                            return 1;
                        } else {
                            return 0;
                        }
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        public String GetDegreeRequirementWorksheetFileName(String effectiveYear, String documentTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT FileName " +
                    "FROM DegreeRequirementWorksheet " +
                    "WHERE EffectiveYear = '" + effectiveYear +
                    "' AND DocumentTitle = '" + documentTitle + "';";

                objCmd = new SqlCommand(strSQL, objCon);
                try {
                    return objCmd.ExecuteScalar().ToString();
                } catch {
                    return "";
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteDegreeRequirementWorksheet(String fileName) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE DegreeRequirementWorksheet " +
                    "WHERE FileName = '" + fileName + "';";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetDegreesUsingWkst(String documentTitle) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT DegreeID, DegreeShortTitle, DegreeLongTitle " +
                    "FROM Degree " +
                    "WHERE DocumentTitle = '" + documentTitle +
                    "' ORDER BY DegreeShortTitle;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }

        public bool AddProgramAreaOfStudy(Int32 programID, Int32 areaOfStudyID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM AreaOfStudyProgram " +
                    "WHERE ProgramID = " + programID +
                    " AND AreaOfStudyID = " + areaOfStudyID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return false;
                } else {
                    strSQL = "INSERT INTO AreaOfStudyProgram(AreaOfStudyID, ProgramID, PrimaryAreaOfStudy) " +
                        "VALUES(" + areaOfStudyID + "," + programID + ",0);";


                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }

            } finally {
                objCon.Close();
            }
        }

        //OVERWRITES
        public bool AddProgramAreaOfStudy(Int32 programID, Int32 areaOfStudyID, Byte primaryAreaOfStudy, SqlConnection objCon) {

            try {

                strSQL = "SELECT COUNT(*) " +
                    "FROM AreaOfStudyProgram " +
                    "WHERE ProgramID = " + programID +
                    " AND AreaOfStudyID = " + areaOfStudyID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return false;
                } else {
                    strSQL = "INSERT INTO AreaOfStudyProgram(AreaOfStudyID, ProgramID, PrimaryAreaOfStudy) " +
                        "VALUES(" + areaOfStudyID + "," + programID + "," + primaryAreaOfStudy + ");";

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }

            } catch {
                return false;
            }
        }

        public DataSet GetProgramAreasOfStudy(Int32 programID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT AreaOfStudyProgram.AreaOfStudyID, PrimaryAreaOfStudy, Title " +
                         "FROM AreaOfStudyProgram " +
                         "INNER JOIN AreaOfStudy " +
                         "ON AreaOfStudy.AreaOfStudyID = AreaOfStudyProgram.AreaOfStudyID " +
                         "WHERE ProgramID = " + programID +
                         "ORDER BY PrimaryAreaOfStudy DESC;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }

        public bool EditPrimaryAreaOfStudy(Int32 programID, String areaOfStudyID, SqlConnection objCon) {

            try {

                strSQL = "DELETE AreaOfStudyProgram " +
                         "WHERE ProgramID = " + programID +
                         " AND PrimaryAreaOfStudy = 1;";

                objCmd = new SqlCommand(strSQL, objCon);
                objCmd.ExecuteNonQuery();

                if (areaOfStudyID != null && areaOfStudyID != "") {
                    strSQL = "SELECT COUNT(*) " +
                        "FROM AreaOfStudyProgram " +
                        "WHERE ProgramID = " + programID +
                        "AND AreaOfStudyID = " + areaOfStudyID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);
                    if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                        strSQL = "UPDATE AreaOfStudyProgram " +
                            "SET PrimaryAreaOfStudy = 1 " +
                            "WHERE ProgramID = " + programID +
                            "AND AreaOfStudyID = " + areaOfStudyID + ";";

                        objCmd = new SqlCommand(strSQL, objCon);
                        if (objCmd.ExecuteNonQuery() > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return AddProgramAreaOfStudy(programID, Convert.ToInt32(areaOfStudyID), 1, objCon);
                    }
                } else {
                    return true;
                }

            } catch {
                return false;
            }
        }

        public bool DeleteAdditionalProgramAreasOfStudy(Int32 programID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE AreaOfStudyProgram " +
                    "WHERE ProgramID = " + programID +
                    " AND PrimaryAreaOfStudy = 0;";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }

        public bool AddProgramCategory(Int32 programVersionID, Int32 categoryID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT COUNT(*) " +
                    "FROM ProgramCategory " +
                    "WHERE ProgramVersionID = " + programVersionID +
                    " AND CategoryID = " + categoryID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return false;
                } else {
                    strSQL = "INSERT INTO ProgramCategory(ProgramVersionID, CategoryID, PrimaryCategory) " +
                        "VALUES(" + programVersionID + "," + categoryID + ",0);";


                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }

            } finally {
                objCon.Close();
            }
        }


        //OVERWRITES
        public bool AddProgramCategory(Int32 programVersionID, Int32 categoryID, Byte primaryCategory, SqlConnection objCon) {

            try {

                strSQL = "SELECT COUNT(*) " +
                    "FROM ProgramCategory " +
                    "WHERE ProgramVersionID = " + programVersionID +
                    " AND CategoryID = " + categoryID + ";";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                    return false;
                } else {
                    strSQL = "INSERT INTO ProgramCategory(ProgramVersionID, CategoryID, PrimaryCategory) " +
                        "VALUES(" + programVersionID + "," + categoryID + "," + primaryCategory + ");";

                    objCmd = new SqlCommand(strSQL, objCon);

                    if (objCmd.ExecuteNonQuery() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }

            } catch {
                return false;
            }
        }


        public bool EditPrimaryCategory(Int32 programVersionID, String categoryID, SqlConnection objCon) {

            try {

                strSQL = "DELETE ProgramCategory " +
                         "WHERE ProgramVersionID = " + programVersionID +
                         " AND PrimaryCategory = 1;";

                objCmd = new SqlCommand(strSQL, objCon);
                objCmd.ExecuteNonQuery();

                if (categoryID != null && categoryID != "") {
                    strSQL = "SELECT COUNT(*) " +
                        "FROM ProgramCategory " +
                        "WHERE ProgramVersionID = " + programVersionID +
                        "AND CategoryID = " + categoryID + ";";

                    objCmd = new SqlCommand(strSQL, objCon);
                    if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0) {
                        strSQL = "UPDATE ProgramCategory " +
                            "SET PrimaryCategory = 1 " +
                            "WHERE ProgramVersionID = " + programVersionID +
                            "AND CategoryID = " + categoryID + ";";

                        objCmd = new SqlCommand(strSQL, objCon);
                        if (objCmd.ExecuteNonQuery() > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return AddProgramCategory(programVersionID, Convert.ToInt32(categoryID), 1, objCon);
                    }
                } else {
                    return true;
                }

            } catch {
                return false;
            }
        }


        public bool DeleteProgramCategories(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE ProgramCategory " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteAdditionalProgramCategories(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE ProgramCategory " +
                    "WHERE ProgramVersionID = " + programVersionID +
                    " AND PrimaryCategory = 0;";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetProgramCategories(Int32 programVersionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT ProgramCategory.CategoryID, PrimaryCategory, CategoryTitle " +
                         "FROM ProgramCategory " +
                         "INNER JOIN Category " +
                         "ON Category.CategoryID = ProgramCategory.CategoryID " +
                         "WHERE ProgramVersionID = " + programVersionID +
                         "ORDER BY PrimaryCategory DESC;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public DataSet GetOptionMCodes(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT OptionMCode.CollegeID, CollegeShortTitle, MCode " +
                    "FROM OptionMCode " +
                    "INNER JOIN College " +
                    "ON College.CollegeID = OptionMCode.CollegeID " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            } finally {
                objCon.Close();
            }
        }


        public String GetOptionCollegeMCode(Int32 optionID, Int16 collegeID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "SELECT MCode " +
                    "FROM OptionMCode " +
                    "WHERE OptionID = " + optionID +
                    " AND CollegeID = " + collegeID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                try {
                    return objCmd.ExecuteScalar().ToString();
                } catch {
                    return "";
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteOptionMCodes(Int32 optionID) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "DELETE OptionMCode " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool DeleteOptionMCodes(Int32 optionID, SqlConnection objCon) {

            try {

                strSQL = "DELETE OptionMCode " +
                    "WHERE OptionID = " + optionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch {
                return false;
            }
        }


        public bool AddOptionCollegeMCode(Int32 optionID, Int16 collegeID, String strMCode) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "INSERT INTO OptionMCode(OptionID, CollegeID, MCode) " +
                        "VALUES(" + optionID + "," + collegeID + ",'" + strMCode.Replace("'", "''") + "');";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public bool EditProgramTerms(Int32 programVersionID, String programBeginSTRM, String programEndSTRM) {

            objCon = new SqlConnection(strConnection);

            try {

                objCon.Open();

                strSQL = "UPDATE Program " +
                    "SET ProgramBeginSTRM = '" + programBeginSTRM + "', ProgramEndSTRM = '" + programEndSTRM + "' " +
                    "WHERE ProgramVersionID = " + programVersionID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0) {
                    return true;
                } else {
                    return false;
                }

            } finally {
                objCon.Close();
            }
        }


        public string Strip(string text) {
            return Regex.Replace(text, @"<(.|\n)*?>", string.Empty);
        }
    }
}
