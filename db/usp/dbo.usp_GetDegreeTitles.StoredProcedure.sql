USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDegreeTitles]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/15/17
-- Description:	Returns degree titles and primary keys
--		 INPUT: STRM (optional) - Term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDegreeTitles]
	@STRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)

    SELECT Degree.DegreeID, DegreeShortTitle, DegreeLongTitle
    FROM Degree 
    LEFT OUTER JOIN DegreeRequirementWorksheet
    ON DegreeRequirementWorksheet.DocumentTitle = Degree.DocumentTitle
    LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable
    ON STRM = @STRM
    LEFT OUTER JOIN ProgramDegree 
    ON ProgramDegree.DegreeID = Degree.DegreeID 
    LEFT OUTER JOIN Program 
    ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
    WHERE DegreeShortTitle <> 'Credit by Nontraditional Means' 
    AND (((ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM)) AND PublishedProgram = 1 AND Degree.DocumentTitle IS NULL)
    OR (((vw_TermTable.DESCR LIKE '%Fall%' AND EffectiveYear = YEAR(TERM_BEGIN_DT)) OR (vw_TermTable.DESCR NOT LIKE '%Fall%' AND EffectiveYear = YEAR(DATEADD(Year, -1, TERM_BEGIN_DT)))) AND (Degree.DocumentTitle IS NOT NULL AND Degree.DocumentTitle <> '')))
    GROUP BY Degree.DegreeID, DegreeShortTitle, DegreeLongTitle
    ORDER BY DegreeLongTitle;
END

GO
