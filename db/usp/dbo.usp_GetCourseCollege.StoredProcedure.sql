USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseCollege]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/12/17
-- Description:	Returns the college offering a course
--		 INPUT: CourseID - Course primary key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseCollege] 
	@CourseID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT CollegeCourse.CollegeID, CollegeShortTitle 
    FROM CollegeCourse 
    INNER JOIN College 
    ON College.CollegeID = CollegeCourse.CollegeID
    WHERE CourseID = @CourseID;
END

GO
