using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.popups
{
	/// <summary>
	/// Summary description for editprereq.
	/// </summary>
	public partial class editprereq : System.Web.UI.Page
	{
		programData csProgram = new programData();
		courseData csCourse = new courseData();


		protected void Page_Load(object sender, System.EventArgs e) {
			panError.Visible = false;

			if(!IsPostBack){
				hidProgramVersionID.Value = Request.QueryString["pvid"];
				hidProgramDegreeID.Value = Request.QueryString["pdid"];
				hidLtr.Value = Request.QueryString["ltr"];
				hidProgramTitle.Value = Request.QueryString["pt"];
				hidOfferedAt.Value = Request.QueryString["oa"];
				hidOptionPrerequisiteID.Value = Request.QueryString["pid"];
				hidOptionFootnoteID.Value = Request.QueryString["fid"];
				hidCourseOffering.Value = Request.QueryString["co"];
				hidCourseSubject.Value = Request.QueryString["cs"];
				hidOptionID.Value = Request.QueryString["oid"];
				hidDegreeID.Value = Request.QueryString["did"];
				hidProgramDisplay.Value = Request.QueryString["pd"];
				hidProgramBeginSTRM.Value = Request.QueryString["bstrm"];
				hidProgramEndSTRM.Value = Request.QueryString["estrm"];
				hidPublishedProgram.Value = Request.QueryString["pp"];
				hidPublishedProgramVersionID.Value = Request.QueryString["ppvid"];
				hidSTRM.Value = Request.QueryString["strm"];
				hidSelectedCollegeID.Value = Request.QueryString["scolid"];
				hidProgramID.Value = Request.QueryString["id"];
				hidCollegeID.Value = Request.QueryString["colid"];
				hidSearchSubject.Value = Request.QueryString["searchsub"];
				hidSearchId.Value = Request.QueryString["searchid"];
				hidSearchSTRM.Value = Request.QueryString["searchstrm"];
				hidSearchText.Value = Request.QueryString["searchtxt"];

                Int32 programVersionID = Convert.ToInt32(hidProgramVersionID.Value);
				
				DataSet dsCrsDept = csProgram.GetSubjectsForPrograms(hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
				Int32 intCrsDeptCount = dsCrsDept.Tables[0].Rows.Count;

				if(intCrsDeptCount > 0){
					for(Int32 intDSRow = 0; intDSRow < intCrsDeptCount; intDSRow++){
						if(Convert.ToInt32(dsCrsDept.Tables[0].Rows[intDSRow]["CrsCount"]) > 0){
							String subject = dsCrsDept.Tables[0].Rows[intDSRow]["SUBJECT"].ToString();
							cboCrsDept.Items.Add(subject);

							if(subject == hidCourseSubject.Value){
								cboCrsDept.Items[intDSRow].Selected = true;
							}
						}
					}
				}else{
					//do something -- may want to check if the cboCrsDept.length > 0
				}

				DataSet dsFootnotes = csProgram.GetOptFootnotes(Convert.ToInt32(hidOptionID.Value));
				Int32 intFootnoteCount = dsFootnotes.Tables[0].Rows.Count;
				
				if(intFootnoteCount > 0){
					cboFootnote.Items.Add(new ListItem("", "0"));
					for(Int32 intDSRow = 0; intDSRow < intFootnoteCount; intDSRow++){
						String strOptionFootnoteID = dsFootnotes.Tables[0].Rows[intDSRow]["OptionFootnoteID"].ToString();
						
						cboFootnote.Items.Add(new ListItem(dsFootnotes.Tables[0].Rows[intDSRow]["FootnoteNumber"].ToString(), dsFootnotes.Tables[0].Rows[intDSRow]["OptionFootnoteID"].ToString()));
						
						if(strOptionFootnoteID == hidOptionFootnoteID.Value){
							cboFootnote.Items[intDSRow + 1].Selected = true;
						}
					}
				}
			}

			if(cboFootnote.Items.Count == 0){
				cboFootnote.Items.Add(new ListItem("None Entered", "0"));
				lblFootnote.Text = "None Selected";
			}else{
				if(cboFootnote.SelectedValue == "0"){
					lblFootnote.Text = "None Selected";
				}else{
					lblFootnote.Text = csProgram.GetFootnote(Convert.ToInt32(cboFootnote.SelectedValue));
				}
			}

			DataSet dsCatalogNbrs = csProgram.GetCourseOfferings(cboCrsDept.SelectedValue, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
			Int32 intCrsCount = dsCatalogNbrs.Tables[0].Rows.Count;
			
			cboCatalogNbr.Items.Clear();

			if(intCrsCount > 0){
				for(Int32 intDSRow = 0; intDSRow < intCrsCount; intDSRow++){
					cboCatalogNbr.Items.Add(new ListItem(dsCatalogNbrs.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString(), dsCatalogNbrs.Tables[0].Rows[intDSRow]["CourseOffering"].ToString()));
				}

				if(!IsPostBack){
					try{
						cboCatalogNbr.SelectedValue = hidCourseOffering.Value;
					}catch{
						//do nothing
					}
				}else{
					try{
						cboCatalogNbr.SelectedValue = Request.Form["cboCatalogNbr"];
					}catch{
						//do nothing
					}
				}

				//get most current published version of course to display in program strm span
				DataSet dsCrsToDisplay = csCourse.GetMostCurrentCourseForProgram(cboCatalogNbr.SelectedValue, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
				Int32 intRowCount = dsCrsToDisplay.Tables[0].Rows.Count;

				if(intRowCount > 0){
                    //lblTerm.Text = dsCrsToDisplay.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString() + " - " + dsCrsToDisplay.Tables[0].Rows[0]["EndTerm_DESCR"].ToString();
                    lblEFFDT.Text = Convert.ToDateTime(dsCrsToDisplay.Tables[0].Rows[0]["EFFDT"]).ToShortDateString();
                    lblCourseLongTitle.Text = dsCrsToDisplay.Tables[0].Rows[0]["COURSE_TITLE_LONG"].ToString(); //should this store DESCR instead of COURSE_TITLE_LONG?
				}else{
                    lblCourseLongTitle.Text = "<div style=\"color:red\">The selected course is not offered during the program begin and end terms.</div>";
					//lblCourseLongTitle.Text = "<div style=\"color:red\">The most current version of the course selected could not be found.</div>";
				}

			}else{
				//do something -- may want to check if the cboCatalogNbrs.length > 0
			}
		}

		protected void cmdEditPrereq_Click(object sender, System.EventArgs e){
			Int32 degreeID = Convert.ToInt32(hidDegreeID.Value);
			Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
			Int32 optionID = Convert.ToInt32(hidOptionID.Value);

			Byte bitPrimaryOption = 0;
			if(Request.Form["hidPrimaryOption"] == "true"){
				bitPrimaryOption = 1;
			}

			csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), Request.Form["hidOptionDescription"], Request.Form["hidOptionStateApproval"], Request.Form["hidCIP"], Request.Form["hidEPC"], Request.Form["hidAcademicPlan"], bitPrimaryOption, Request.Form["hidGainfulEmploymentID"], Request.Form["hidTotalQuarters"]);
			
			csProgram.DeleteOptionLocations(optionID);
			if(hidLocations.Value != ""){
				String[] strOptionLocations = hidLocations.Value.Substring(1).Split('|');
				for(Int32 i = 0; i < strOptionLocations.Length; i++){
					csProgram.AddOptionLocation(optionID, Convert.ToInt32(strOptionLocations[i]));
				}
			}

			csProgram.DeleteOptionMCodes(optionID);
			if(hidMCodes.Value != ""){
				String[] strMCodes = hidMCodes.Value.Substring(1).Split('|');
				for(Int32 i = 0; i < strMCodes.Length; i++){
					String[] strCollegeMCode = strMCodes[i].Split(';');
					csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0]), strCollegeMCode[1]);
				}
			}

			Int16 intSuccess = csProgram.EditOptPrereq(Convert.ToInt32(hidOptionPrerequisiteID.Value), optionID, hidCourseOffering.Value, Request.Form["cboCatalogNbr"], Convert.ToInt32(Request.Form["cboFootnote"]));
			
			if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The prerequisite entered already exists.";
				panError.Visible = true;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The insert failed.";
				panError.Visible = true;
			}else{
				Response.Write("<script type=\"text/javascript\">opener.location.href='../edit.aspx?pvid=" + hidProgramVersionID.Value + "&oid=" + hidOptionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&todo=pop&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'","\\'") + "&pt=" + hidProgramTitle.Value.Replace("'","\\'") + "&pd=" + hidProgramDisplay.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&","%26") + "&searchid=" + hidSearchId.Value.Replace("&","%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&","%26").Replace("'","\\'") + "';self.close();</script>");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {    
		}
		#endregion
	}
}
