<!--

    //trim leading spaces to the left of the string.
    function ltrim(s){
	    return s.replace( /^\s*/, "" );
    }
    
    //trim leading spaces to the right of the string.
    function rtrim(s){
	    return s.replace( /\s*$/, "" );
    }
    
    //trim function
    function trim(s){
	    return rtrim(ltrim(s));
    }

//-->
