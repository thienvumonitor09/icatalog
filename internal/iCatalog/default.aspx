<%@ Reference Control="~/_phatt3_includes/sidemenu.ascx" %>
<%@ Reference Control="~/_phatt3_includes/header.ascx" %>
<%@ Page language="c#" Inherits="ICatalog._default" CodeFile="default.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>iCatalog</title>
		<meta name="author" content="CCS Information Systems" />
		<link rel="stylesheet" type="text/css" href="/ICatalog/_phatt3_css/default.css" />
	</head>
	<body>
		<asp:panel id="container" runat="server">
			<input id="hidMenu" type="hidden" runat="server" />
			<asp:panel id="header" runat="server"></asp:panel>
			<asp:panel id="sidemenu" runat="server"></asp:panel>
			<asp:panel id="content" runat="server"></asp:panel>
			<div class="clearer"></div>
		</asp:panel>
	</body>
</html>
