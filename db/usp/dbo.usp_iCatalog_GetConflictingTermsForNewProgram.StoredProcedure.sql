USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_iCatalog_GetConflictingTermsForNewProgram]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 4/20/17
-- Description:	Returns the conflicting program version's begin and end term description
--		 INPUT: ProgramTitle - Program Title
--				CollegeID - College Primary Key
--				DegreeID - Degree Primary Key
--				BeginSTRM - Begin Term Code
--				EndSTRM - End Term Code
--	  MODIFIED: 3/22/18 to include DegreeID as a parameter/filter by Brandy Vaughn
-- =============================================
CREATE PROCEDURE [dbo].[usp_iCatalog_GetConflictingTermsForNewProgram] 
	@ProgramTitle varchar(80),
	@CollegeID int,
	@DegreeID int,
	@BeginSTRM varchar(4),
	@EndSTRM varchar(4)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT BeginTerm.DESCR AS BeginTerm_DESCR, EndTerm.DESCR AS EndTerm_DESCR 
	FROM Program 
	INNER JOIN ProgramDegree
	ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = ProgramBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = ProgramEndSTRM
	WHERE ProgramTitle = @ProgramTitle
	AND CollegeID = @CollegeID
	AND DegreeID = @DegreeID
	AND ((ProgramBeginSTRM >= @BeginSTRM AND ProgramBeginSTRM <= @EndSTRM) 
	OR (ProgramEndSTRM <= @EndSTRM AND ProgramEndSTRM >= @BeginSTRM) 
	OR (ProgramBeginSTRM >= @BeginSTRM AND ProgramEndSTRM <= @EndSTRM));
END

GO
