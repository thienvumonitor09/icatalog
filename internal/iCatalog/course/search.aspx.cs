using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.course
{
	/// <summary>
	/// Summary description for search.
	/// </summary>
	public partial class search : System.Web.UI.Page
	{
		courseData csCourse = new courseData();

		protected System.Web.UI.WebControls.Button cmdSearch;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack){
				String strSearchText = Request.QueryString["searchtxt"];
				if(strSearchText != null && strSearchText != ""){
					txtTextSearch.Text = strSearchText;
					hidTodo.Value = "search";
				}
			}

			if(hidTodo.Value == "search"){
				
				TableRow tr = new TableRow();
				TableCell td = new TableCell();

				DataSet dsCourses = csCourse.GetCoursesTextSearch(txtTextSearch.Text);
                DataTable dtCourses = dsCourses.Tables[0];

                Int32 intRowCount = dsCourses.Tables[0].Rows.Count;

				if(intRowCount > 0){
						
					tr = new TableRow();
					td = new TableCell();
					td.Attributes["style"] = "padding-left:5px;padding-top:4px;padding-bottom:4px;";
					td.CssClass = "portletMain";
					td.ColumnSpan = 6;
					td.Controls.Add(new LiteralControl("The following courses contain " + txtTextSearch.Text + " in the course description."));
					tr.Cells.Add(td);
					tblDisplay.Rows.Add(tr);

					tr = new TableRow();
					td = new TableCell();
					td.CssClass = "portletSecondary";
					td.Attributes["style"] = "text-align:center;width:100px;";
					td.Controls.Add(new LiteralControl("<b>Course Offering</b>"));
					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletSecondary";
					td.Attributes["style"] = "text-align:center;";
					td.Controls.Add(new LiteralControl("<b>Description</b>"));
					tr.Cells.Add(td);
				
					td = new TableCell();
					td.CssClass = "portletSecondary";
					td.Attributes["style"] = "text-align:center;width:84px;";
					td.Controls.Add(new LiteralControl("<b>Effective Date</b>"));
					tr.Cells.Add(td);

                    /*
					td = new TableCell();
					td.CssClass = "portletSecondary";
					td.Attributes["style"] = "text-align:center;width:84px;";
					td.Controls.Add(new LiteralControl("<b>End Term</b>"));
					tr.Cells.Add(td);
                    */
					td = new TableCell();
					td.CssClass = "portletSecondary";
					td.Attributes["style"] = "text-align:center;width:84px;";
					td.Controls.Add(new LiteralControl("<b>Status</b>"));
					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletSecondary";
					td.Attributes["style"] = "text-align:center;width:74px;";
					tr.Cells.Add(td);
					
					tblDisplay.Rows.Add(tr);
                    foreach (DataRow drCourse in dtCourses.Rows)
                    {
                        String courseOffering = drCourse["CourseOffering"].ToString().Replace(" ", "&nbsp;");

                        tr = new TableRow();

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
                        td.Controls.Add(new LiteralControl("<a name=\"" + courseOffering + "\">" + courseOffering + "</a>"));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
                        td.Controls.Add(new LiteralControl(drCourse["DESCR"].ToString()));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;";
                        td.Controls.Add(new LiteralControl(Convert.ToDateTime(drCourse["EFFDT"]).ToShortDateString()));
                        tr.Cells.Add(td);

                        /*
                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;";
                        td.Controls.Add(new LiteralControl(""));
                        tr.Cells.Add(td);
                        */

                        td = new TableCell();
                        td.CssClass = "portletLight";
                        td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;";
                        td.Controls.Add(new LiteralControl(drCourse["CATALOG_PRINT"].ToString().Equals("Y") ? "Published Copy" : "Working Copy"));
                        tr.Cells.Add(td);

                        //Display Table
                        tblDisplay.Rows.Add(tr);

                    }
                    /*
					for(Int32 intDSRow = 0; intDSRow < intRowCount; intDSRow++)
                    { 
						Int32 courseID = Convert.ToInt32(dsCourses.Tables[0].Rows[intDSRow]["CourseID"]);
						String courseOffering = dsCourses.Tables[0].Rows[intDSRow]["CourseOffering"].ToString().Replace(" ","&nbsp;");
						String beginTerm = dsCourses.Tables[0].Rows[intDSRow]["BeginTerm_DESCR"].ToString();
						String endTerm = dsCourses.Tables[0].Rows[intDSRow]["EndTerm_DESCR"].ToString();
						String strCourses = csCourse.GetCoursesUsingSource(courseID), publishedCourse = "";
						String publishedCourseID = dsCourses.Tables[0].Rows[intDSRow]["PublishedCourseID"].ToString();
						Int16 intPublishedCourse = Convert.ToInt16(dsCourses.Tables[0].Rows[intDSRow]["PublishedCourse"]);

						if(!csCourse.HasWorkingCourseCopy(courseID)){
								
							if(intPublishedCourse == 0){
								publishedCourse = "Working Copy";
							}else if(intPublishedCourse == 1){
								publishedCourse = "Published Copy";
							}

							tr = new TableRow();

							td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
							td.Controls.Add(new LiteralControl("<a name=\"" + courseID + "\">" + courseOffering + "</a>"));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
							td.Controls.Add(new LiteralControl(dsCourses.Tables[0].Rows[intDSRow]["DESCR"].ToString()));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;";
							td.Controls.Add(new LiteralControl(beginTerm));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
                            td.Attributes["style"] = "text-align:right;padding-left:5px;padding-right:10px;padding-top:4px;padding-bottom:4px;";
							td.Controls.Add(new LiteralControl(endTerm));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;text-align:right";
							td.Controls.Add(new LiteralControl(publishedCourse));

							tr.Cells.Add(td);

							td = new TableCell();
							td.CssClass = "portletLight";
							td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:10px;";
							td.Controls.Add(new LiteralControl("<a href=\"edit.aspx?todo=edit&course=" + courseID.ToString() + "&sameas=" + publishedCourseID + "&searchtxt=" + txtTextSearch.Text.Replace("'","\\'").Replace("&","%26") + "\">Edit</a>"));

                            if (!csCourse.UsedInProgram(courseOffering.Replace("&nbsp;"," "))) {
								if(strCourses == ""){
									td.Controls.Add(new LiteralControl("&nbsp;|&nbsp;<a href=\"edit.aspx?todo=delete&course=" + courseID.ToString() + "&searchtxt=" + txtTextSearch.Text.Replace("'","\\'").Replace("&","%26") + "\" onclick=\"return confirm('Are you sure you want to delete " + courseOffering + " " + beginTerm + "?');\">Delete</a>"));
								}else{
									td.Controls.Add(new LiteralControl("&nbsp;|&nbsp;<a href=\"#\" onclick=\"alert('" + courseOffering + " " + beginTerm + " cannot be deleted because the following courses are using " + courseOffering + " " + beginTerm + " as the source for their descriptions.\\n" + strCourses + "');\">Delete</a>"));
								}
							}

							tr.Cells.Add(td);

							tblDisplay.Rows.Add(tr);
						}
                       
                }
                    */
					
				}else{
					tr = new TableRow();
					td = new TableCell();
					td.Attributes["style"] = "padding-left:5px;padding-top:4px;padding-bottom:4px;";
					td.CssClass = "portletMain";
					td.ColumnSpan = 6;
					td.Controls.Add(new LiteralControl("No courses were found containing " + txtTextSearch.Text + " in the long course description."));
					tr.Cells.Add(td);
					tblDisplay.Rows.Add(tr);
				}

				panResults.Visible = true;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
