using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog
{
	/// <summary>
	/// Summary description for _default.
	/// </summary>
	public partial class _default : System.Web.UI.Page
	{
		
		protected void Page_Load(object sender, System.EventArgs e) {

            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

            //if the user is not logged in with a valid AD account
            if (Request.Cookies["phatt2"]["userctclinkid"].ToString() == "000000000")
            {
                //redirect them to an error page
                Response.Redirect("error.htm");
            }
            else
            {
                //get user permissions
                String employeeID = Request.Cookies["phatt2"]["userctclinkid"];

                permissionData csPermission = new permissionData();
                String strCollegePermission = "0", strCredPermission = "0", strCoursePermission = "0", strProgPermission = "0";

                DataSet dsCollegePermission = csPermission.GetCollegeUserPermissionByEmployeeID(employeeID);
                if (dsCollegePermission.Tables[0].Rows.Count != 0)
                {
                    strCollegePermission = dsCollegePermission.Tables[0].Rows[0]["Permission"].ToString();
                }

                DataSet dsCredPermission = csPermission.GetCredUserPermissionByEmployeeID(employeeID);
                if (dsCredPermission.Tables[0].Rows.Count != 0)
                {
                    strCredPermission = dsCredPermission.Tables[0].Rows[0]["Permission"].ToString();
                }

                DataSet dsCoursePermission = csPermission.GetCourseUserPermissionByEmployeeID(employeeID);
                if (dsCoursePermission.Tables[0].Rows.Count != 0)
                {
                    strCoursePermission = dsCoursePermission.Tables[0].Rows[0]["Permission"].ToString();
                }

                DataSet dsProgPermission = csPermission.GetProgUserPermissionByEmployeeID(employeeID);
                if (dsProgPermission.Tables[0].Rows.Count != 0)
                {
                    strProgPermission = dsProgPermission.Tables[0].Rows[0]["Permission"].ToString();
                }

                //write user permission cookie
                HttpCookie objCookie = new HttpCookie("userPermission");
                Response.Cookies.Add(objCookie);
                objCookie.Values.Add("college", strCollegePermission);
                objCookie.Values.Add("cred", strCredPermission);
                objCookie.Values.Add("course", strCoursePermission);
                objCookie.Values.Add("prog", strProgPermission);
                Response.Cookies["userPermission"].Expires = DateTime.Now.AddDays(1);
                Response.Cookies["userPermission"].Path = "/";

                _phatt3_includes.header objHeader = (_phatt3_includes.header)LoadControl("_phatt3_includes/header.ascx");
                header.Controls.Add(objHeader);

                _phatt3_includes.sidemenu objMenu = (_phatt3_includes.sidemenu)LoadControl("_phatt3_includes/sidemenu.ascx");
                sidemenu.Controls.Add(objMenu);

            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
