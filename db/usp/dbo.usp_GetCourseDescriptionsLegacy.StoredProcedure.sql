USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseDescriptionsLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/12/17
-- Description:	Returns course descriptions by college, term, subject area, and catalog nbr. Courses using the same description are grouped together.
--		 INPUT: SUBJECT - Course Subject Area / Department
--				CollegeID (optional) - College primary key (CollegeASN)
--				STRM (optional) - Term code
--				CATALOG_NBR (optional) - Course number
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseDescriptionsLegacy]
	@SUBJECT varchar(5),
	@CollegeID int = NULL,
	@STRM varchar(4) = NULL,
	@CATALOG_NBR varchar(3) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)
					 
	DECLARE @SubjectLength int = LEN(@SUBJECT)

    SELECT A.CourseBeginSTRM, A.CourseID, A.COURSE_TITLE_LONG, A.DESCRLONG, A.UNITS_MINIMUM, A.UNITS_MAXIMUM, A.VariableUnits,  
		   A.CourseOffering, A.SUBJECT, A.CATALOG_NBR as CATALOG_NBR_A, B.CATALOG_NBR as CATALOG_NBR_B, B.DescriptionCourseID, SubjectArea.DESCR AS SubjectArea_DESCR
    FROM Course A 
    LEFT OUTER JOIN Course B 
    ON A.CourseID = B.DescriptionCourseID 
    INNER JOIN SubjectArea 
    ON A.SUBJECT = SubjectArea.SUBJECT 
    INNER JOIN CollegeCourse 
    ON CollegeCourse.CourseID = A.CourseID 
    WHERE A.CourseOffering + ' ' + A.CourseBeginSTRM IN 
    (SELECT CourseOffering + ' ' + CourseBeginSTRM 
    FROM Course 
    WHERE CourseBeginSTRM <= @STRM AND (CourseEndSTRM = 'Z999' OR CourseEndSTRM >= @STRM)) 
    AND A.PublishedCourse = '1' 
    AND A.DescriptionCourseID IS NULL 
    AND (@CollegeID IS NULL OR CollegeID = @CollegeID)
    AND (@CATALOG_NBR IS NULL OR A.CATALOG_NBR = @CATALOG_NBR)
    AND (A.SUBJECT = @SUBJECT OR A.SUBJECT = @SUBJECT + '&') 
    GROUP BY A.CourseBeginSTRM, A.CourseID, A.COURSE_TITLE_LONG, A.DESCRLONG, A.UNITS_MINIMUM, A.UNITS_MAXIMUM, A.VariableUnits, A.CourseOffering, A.SUBJECT, A.CATALOG_NBR, B.CATALOG_NBR, B.DescriptionCourseID, SubjectArea.DESCR, SubjectArea.SUBJECT
    ORDER BY SUBSTRING(SubjectArea.SUBJECT,1,@SubjectLength), SUBSTRING(A.SUBJECT,1,@SubjectLength), CAST(A.CATALOG_NBR AS int), CAST(B.CATALOG_NBR AS int);
END

GO
