USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramDegree]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/11/17
-- Description:	Returns program degree details
--		 INPUT: ProgramDegreeID - Program Degree Primary Key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramDegree] 
	@ProgramDegreeID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT ProgramTitle, DocumentTitle, ProgramDegree.ProgramVersionID, ProgramDisplay, ProgramTextOutline, CollegeShortTitle, Program.CollegeID, DegreeShortTitle, CareerPlanningGuideID, ProgramBeginSTRM, ProgramEndSTRM 
    FROM ProgramDegree 
    INNER JOIN Program 
    ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
    INNER JOIN College 
    ON College.CollegeID = Program.CollegeID 
    INNER JOIN Degree 
    ON Degree.DegreeID = ProgramDegree.DegreeID 
    LEFT OUTER JOIN CareerPlanningGuide 
    ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID
    AND PublishedCareerPlanningGuide = '1' 
    WHERE ProgramDegreeID = @ProgramDegreeID;
END

GO
