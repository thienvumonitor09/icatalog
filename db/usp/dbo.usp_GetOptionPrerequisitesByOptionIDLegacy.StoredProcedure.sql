USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionPrerequisitesByOptionIDLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 10/2/17
-- Description:	Returns option prerequisites
--		 INPUT: OptionID - Program degree option primary key
--				SelectedSTRM - Selected term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionPrerequisitesByOptionIDLegacy]
	@OptionID int,
	@SelectedSTRM varchar (4) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @SelectedSTRM IS NULL
		SET @SelectedSTRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)

	SELECT SUBJECT, CATALOG_NBR, CourseSuffix, OptionPrerequisite.CourseOffering, COURSE_TITLE_LONG, CourseBeginSTRM, CourseEndSTRM, FootnoteNumber, OptionPrerequisiteID, Course.CourseID, OptionPrerequisite.OptionFootnoteID, COUNT(*) AS PrerequisiteCount 
    FROM OptionPrerequisite 
    INNER JOIN ProgramDegreeOption
    ON ProgramDegreeOption.OptionID = OptionPrerequisite.OptionID
    INNER JOIN ProgramDegree
    ON ProgramDegree.ProgramDegreeID = ProgramDegreeOption.ProgramDegreeID
    INNER JOIN Program 
    ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
    INNER JOIN Course
    ON Course.CourseOffering = OptionPrerequisite.CourseOffering
    AND ((CourseBeginSTRM <= ProgramBeginSTRM AND (CourseEndSTRM >= ProgramBeginSTRM OR CourseEndSTRM = 'Z999')) 
    OR (CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM AND (CourseEndSTRM <= ProgramEndSTRM OR CourseEndSTRM = 'Z999')))
    AND (CourseBeginSTRM <= @SelectedSTRM AND (CourseEndSTRM >= @SelectedSTRM OR CourseEndSTRM = 'Z999')) 
    LEFT OUTER JOIN OptionFootnote 
    ON OptionFootnote.OptionFootnoteID = OptionPrerequisite.OptionFootnoteID
    WHERE OptionPrerequisite.OptionID = @OptionID
    GROUP BY SUBJECT, CATALOG_NBR, CourseSuffix, OptionPrerequisite.CourseOffering, COURSE_TITLE_LONG, CourseBeginSTRM, CourseEndSTRM, FootnoteNumber, OptionPrerequisiteID, Course.CourseID, OptionPrerequisite.OptionFootnoteID
    ORDER BY SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
END
GO
