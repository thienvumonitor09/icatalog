﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ICatalog._phatt3_classes;

public partial class course_printexport : System.Web.UI.Page
{
    courseData csCourse = new courseData();
    termData csTerm = new termData();

    protected void Page_Load(object sender, System.EventArgs e) {
        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        if (!IsPostBack) {
            DataSet dsTerms = csCourse.GetTermsForCourses();

            for (Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++) {
                cboTerm.Items.Add(new ListItem(dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString(), dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString()));
            }

            try {
                cboTerm.SelectedValue = csTerm.GetCurrentTerm();
            } catch {
                //do nothing
            }
        }
    }

    protected void cmdSubmit_Click(object sender, System.EventArgs e) {
        Response.Redirect(csCourse.ExportToPrintableCatalog(cboTerm.SelectedValue));
    }
}