using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.popups
{
	/// <summary>
	/// Summary description for editoptcrs.
	/// </summary>
	public partial class editoptcrs : System.Web.UI.Page //CHANGE THIS CODE TO EDIT COURSES AND ELECTIVE GROUP REFERENCES
	{
		programData csProgram = new programData();
		courseData csCourse = new courseData();

		#region PROTECTED CONTROLS

		protected System.Web.UI.WebControls.Button cmdAddPrereq;
		/*protected System.Web.UI.HtmlControls.HtmlInputHidden hidSearchId;*/

		#endregion

		protected void Page_Load(object sender, System.EventArgs e) {
			panOverwrite.Visible = false;
			panOverwriteUnits.Visible = false;
			panAddCourse.Visible = false;
			panAddElectGroup.Visible = false;
			panError.Visible = false;
			panError2.Visible = false;

			if(!IsPostBack){
				//check if all these fields are needed
				hidTodo.Value = Request.QueryString["todo"];
				hidProgramVersionID.Value = Request.QueryString["pvid"];
				hidProgramDegreeID.Value = Request.QueryString["pdid"];
				hidCollegeID.Value = Request.QueryString["colid"];
				hidLtr.Value = Request.QueryString["ltr"];
				hidProgramTitle.Value = Request.QueryString["pt"];
				hidOfferedAt.Value = Request.QueryString["oa"];
				hidDegreeID.Value = csProgram.GetDegreeID(Convert.ToInt32(hidProgramDegreeID.Value)).ToString();
				hidOptionID.Value = Request.QueryString["oid"];
				hidCourseOffering.Value = Request.QueryString["co"];
				hidOptionFootnoteID.Value = Request.QueryString["fid"];
				hidCourseSubject.Value = Request.QueryString["cs"];
				hidOptionElectiveGroupID.Value = Request.QueryString["egid"];
				hidQuarter.Value = Request.QueryString["Quarter"];
				hidUnitsMinimum.Value = Request.QueryString["CrMin"];
				hidUnitsMaximum.Value = Request.QueryString["CrMax"];
				hidProgramDisplay.Value = Request.QueryString["pd"];
				hidProgramBeginSTRM.Value = Request.QueryString["bstrm"];
				hidProgramEndSTRM.Value = Request.QueryString["estrm"];
				hidPublishedProgram.Value = Request.QueryString["pp"];
				hidPublishedProgramVersionID.Value = Request.QueryString["ppvid"];
				hidSTRM.Value = Request.QueryString["strm"];
				hidSelectedCollegeID.Value = Request.QueryString["scolid"];
				hidProgramID.Value = Request.QueryString["id"];
				hidSearchSubject.Value = Request.QueryString["searchsub"];
				hidSearchId.Value = Request.QueryString["searchid"];
				hidSearchSTRM.Value = Request.QueryString["searchstrm"];
				hidSearchText.Value = Request.QueryString["searchtxt"];

				if(hidTodo.Value == "editCrsElect"){
					hidCourseID.Value = hidOptionElectiveGroupID.Value;
					optTodo.SelectedIndex = 1;
					panAddElectGroup.Visible = true;
				}else if(hidTodo.Value == "editProgCrs"){
					hidOptionElectiveGroupID.Value = hidCourseID.Value;
					optTodo.SelectedIndex = 0;
					panAddCourse.Visible = true;
				}

				DataSet dsCrsDept = csProgram.GetSubjectsForPrograms(hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
				Int32 intCrsDeptCount = dsCrsDept.Tables[0].Rows.Count;

				if(intCrsDeptCount > 0){
					for(Int32 intDSRow = 0; intDSRow < intCrsDeptCount; intDSRow++){
						if(Convert.ToInt32(dsCrsDept.Tables[0].Rows[intDSRow]["CrsCount"]) > 0){
							cboCrsDept.Items.Add(dsCrsDept.Tables[0].Rows[intDSRow]["SUBJECT"].ToString());
						}
					}
				}else{
					//do something -- may want to check if the cboCrsDept.length > 0
				}

				DataSet dsFootnotes = csProgram.GetOptFootnotes(Convert.ToInt32(hidOptionID.Value));
				Int32 intFootnoteCount = dsFootnotes.Tables[0].Rows.Count;
				
				if(intFootnoteCount > 0){
					cboFootnote.Items.Add(new ListItem("", "0"));
                    cboElectGroupFootnote.Items.Add(new ListItem("", "0"));
					for(Int32 intDSRow = 0; intDSRow < intFootnoteCount; intDSRow++){
						cboFootnote.Items.Add(new ListItem(dsFootnotes.Tables[0].Rows[intDSRow]["FootnoteNumber"].ToString(), dsFootnotes.Tables[0].Rows[intDSRow]["OptionFootnoteID"].ToString()));
						cboElectGroupFootnote.Items.Add(new ListItem(dsFootnotes.Tables[0].Rows[intDSRow]["FootnoteNumber"].ToString(), dsFootnotes.Tables[0].Rows[intDSRow]["OptionFootnoteID"].ToString()));
					}
				}
			}

			if(optTodo.SelectedValue == "AddCourse"){
				String catalogNbr = Request.Form["cboCatalogNbr"];
				String courseID = Request.Form["cboCourseBeginSTRM"];
				String strOptionFootnoteID = Request.Form["cboFootnote"];
				String strQuarter = Request.Form["cboCrsQuarter"];

				panAddCourse.Visible = true;
				
				if(!IsPostBack){ //test this
					try{
						//reselect course values
						cboCrsQuarter.SelectedValue = hidQuarter.Value;
						cboCrsDept.SelectedValue = hidCourseSubject.Value;
					}catch{
						//do nothing
					}
				}

				DataSet dsCatalogNbrs = csProgram.GetCourseOfferings(cboCrsDept.SelectedValue, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
				Int32 intCrsCount = dsCatalogNbrs.Tables[0].Rows.Count;
			
				cboCatalogNbr.Items.Clear();

				if(intCrsCount > 0){
					for(Int32 intDSRow = 0; intDSRow < intCrsCount; intDSRow++){
                        //cboCatalogNbr.Items.Add(new ListItem(dsCatalogNbrs.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString() + dsCatalogNbrs.Tables[0].Rows[intDSRow]["CourseSuffix"].ToString(), dsCatalogNbrs.Tables[0].Rows[intDSRow]["CourseOffering"].ToString()));
                        cboCatalogNbr.Items.Add(new ListItem(dsCatalogNbrs.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString(), dsCatalogNbrs.Tables[0].Rows[intDSRow]["CourseOffering"].ToString()));
                    }

                    try
                    {
						if(catalogNbr == null){
							cboCatalogNbr.SelectedValue = hidCourseOffering.Value;
						}else{
							cboCatalogNbr.SelectedValue = catalogNbr;
						}
					}catch{
						//do nothing
					}

					//get most current published version of course to display in program strm span
					DataSet dsCrsToDisplay = csCourse.GetMostCurrentCourseForProgram(cboCatalogNbr.SelectedValue, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
					Int32 intRowCount = dsCrsToDisplay.Tables[0].Rows.Count;

					if(intRowCount > 0){
                        //lblTerm.Text = dsCrsToDisplay.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString() + " - " + dsCrsToDisplay.Tables[0].Rows[0]["EndTerm_DESCR"].ToString();
                        lblEFFDT.Text = Convert.ToDateTime(dsCrsToDisplay.Tables[0].Rows[0]["EFFDT"]).ToShortDateString();
                        lblCourseLongTitle.Text = dsCrsToDisplay.Tables[0].Rows[0]["COURSE_TITLE_LONG"].ToString();

						//use for variable credit overwrite
						double dblCrsMin = Convert.ToDouble(dsCrsToDisplay.Tables[0].Rows[0]["UNITS_MINIMUM"]), dblCrsMax = Convert.ToDouble(dsCrsToDisplay.Tables[0].Rows[0]["UNITS_MAXIMUM"]);
						
						if(dblCrsMin != dblCrsMax)
                        {
							hidVarCr.Value = "Y";
							lblCrsCredits.Text = dblCrsMin + " - " + dblCrsMax;

							if(!IsPostBack){
								//get the overwrite values from the db
								DataSet dsOverwriteUnits = csProgram.GetOverwriteUnits(Convert.ToInt32(hidOptionID.Value), cboCatalogNbr.SelectedValue, hidProgramBeginSTRM.Value, hidProgramEndSTRM.Value);
								if(dsOverwriteUnits.Tables[0].Rows.Count > 0){
									bool blnOverwriteUnits = Convert.ToBoolean(dsOverwriteUnits.Tables[0].Rows[0]["OverwriteUnits"]);
									if(blnOverwriteUnits){
										txtCrsMin.Text = dsOverwriteUnits.Tables[0].Rows[0]["OverwriteUnitsMinimum"].ToString();
										txtCrsMax.Text = dsOverwriteUnits.Tables[0].Rows[0]["OverwriteUnitsMaximum"].ToString();
										chkOverwrite.Checked = true;
									}else{
										txtCrsMin.Text = "";
										txtCrsMax.Text = "";
										chkOverwrite.Checked = false;
									}
								}else{
									chkOverwrite.Checked = false;
									txtCrsMin.Text = "";
									txtCrsMax.Text = "";
								}
							}

							panOverwrite.Visible = true;
							if(chkOverwrite.Checked){
								panOverwriteUnits.Visible = true;
							}
						}else{
							hidVarCr.Value = "N";
							lblCrsCredits.Text = dblCrsMin.ToString();
							chkOverwrite.Checked = false;
						}

						hidUnitsMinimum.Value = dblCrsMin.ToString();
						hidUnitsMaximum.Value = dblCrsMax.ToString();
					}

				}else{
					//do something -- may want to check if the cboCatalogNbrs.length > 0
				}

				if(cboFootnote.Items.Count == 0){
					cboFootnote.Items.Add(new ListItem("None Entered", "0"));
					lblFootnote.Text = "None Selected";
				}else{
					try{
						if(strOptionFootnoteID == null){
							cboFootnote.SelectedValue = hidOptionFootnoteID.Value;
						}else{
							cboFootnote.SelectedValue = strOptionFootnoteID;
						}
					}catch{
						//do nothing
					}

					if(cboFootnote.SelectedValue == "0"){
						lblFootnote.Text = "None Selected";
					}else{
						lblFootnote.Text = csProgram.GetFootnote(Convert.ToInt32(cboFootnote.SelectedValue));
					}
				}

				if(strQuarter == null){
					cboCrsQuarter.SelectedValue = hidQuarter.Value;
				}else{
					cboCrsQuarter.SelectedValue = strQuarter;
				}

			}
            else if(optTodo.SelectedValue == "AddOptionElectiveGroup")
            {
				String strOptionElectiveGroupID = Request.Form["cboElectGroupTitle"];
				String strOptionFootnoteID = Request.Form["cboElectGroupFootnote"];
				String strQuarter = Request.Form["cboElectGroupQuarter"];

				panAddElectGroup.Visible = true;

				DataSet dsElectGroup = csProgram.GetOptionElectiveGroups(Convert.ToInt32(hidOptionID.Value));
				Int32 intElectGroupCount = dsElectGroup.Tables[0].Rows.Count;

				cboElectGroupTitle.Items.Clear();

				if(intElectGroupCount > 0){
					for(Int32 intDSRow = 0; intDSRow < intElectGroupCount; intDSRow++){
						cboElectGroupTitle.Items.Add(new ListItem(dsElectGroup.Tables[0].Rows[intDSRow]["ElectiveGroupTitle"].ToString(), dsElectGroup.Tables[0].Rows[intDSRow]["OptionElectiveGroupID"].ToString()));
					}
					
					try{
						if(strOptionElectiveGroupID == null){
							cboElectGroupTitle.SelectedValue = hidOptionElectiveGroupID.Value;
						}else{
							cboElectGroupTitle.SelectedValue = strOptionElectiveGroupID;
						}
					}catch{
						//do nothing
					}

				}else{
					cboElectGroupTitle.Items.Add(new ListItem("No program electives currently exist", "0"));
				}

				if(cboElectGroupFootnote.Items.Count == 0){
					cboElectGroupFootnote.Items.Add(new ListItem("None Entered", "0"));
					lblElectGroupFootnote.Text = "None Selected";
				}else{
					try{
						if(strOptionFootnoteID == null){
							cboElectGroupFootnote.SelectedValue = hidOptionFootnoteID.Value;
						}else{
							cboElectGroupFootnote.SelectedValue = strOptionFootnoteID;
						}
					}catch{
						//do nothing
					}

					if(cboElectGroupFootnote.SelectedValue == "0"){
						lblElectGroupFootnote.Text = "None Selected";
					}else{
						lblElectGroupFootnote.Text = csProgram.GetFootnote(Convert.ToInt32(cboElectGroupFootnote.SelectedValue));
					}
				}

				if(strQuarter == null){
					cboElectGroupQuarter.SelectedValue = hidQuarter.Value;
				}else{
					cboElectGroupQuarter.SelectedValue = strQuarter;
				}

				if(hidTodo.Value == "editCrsElect" && !IsPostBack){
					//only fill in the credit fields if an elective was selected for edit
					txtUnitsMinimum.Text = hidUnitsMinimum.Value;
					txtUnitsMaximum.Text = hidUnitsMaximum.Value;
				}
			}
		}

		protected void cmdSaveOptCrs_Click(object sender, System.EventArgs e){
			
			Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
			Int32 optionID = Convert.ToInt32(hidOptionID.Value);
			Int32 degreeID = Convert.ToInt32(hidDegreeID.Value);
			byte overwriteUnits = 0;

			if(Request.Form["hidVarCr"] == "Y" && Request.Form["chkOverwrite"] == "on"){
				overwriteUnits = 1;
			}

			Byte bitPrimaryOption = 0;
			if(Request.Form["hidPrimaryOption"] == "true"){
				bitPrimaryOption = 1;
			}

			csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), Request.Form["hidOptionDescription"], Request.Form["hidOptionStateApproval"], Request.Form["hidCIP"], Request.Form["hidEPC"], Request.Form["hidAcademicPlan"], bitPrimaryOption, Request.Form["hidGainfulEmploymentID"], Request.Form["hidTotalQuarters"]);
			
			csProgram.DeleteOptionLocations(optionID);
			if(hidLocations.Value != ""){
				String[] strOptionLocations = hidLocations.Value.Substring(1).Split('|');
				for(Int32 i = 0; i < strOptionLocations.Length; i++){
					csProgram.AddOptionLocation(optionID, Convert.ToInt32(strOptionLocations[i]));
				}
			}

			csProgram.DeleteOptionMCodes(optionID);
            if (hidMCodes.Value != "")
            {
                String[] strMCodes = hidMCodes.Value.Substring(1).Split('|');
                for (Int32 i = 0; i < strMCodes.Length; i++)
                {
                    String[] strCollegeMCode = strMCodes[i].Split(';');
                    csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0]), strCollegeMCode[1]);
                }
            }

			Int16 intSuccess = csProgram.EditOptCourse(hidTodo.Value, Request.Form["cboCatalogNbr"], Request.Form["hidCourseOffering"], Request.Form["hidOptionElectiveGroupID"], optionID, degreeID, Convert.ToInt32(Request.Form["cboFootnote"]), Convert.ToInt16(Request.Form["cboCrsQuarter"]), Convert.ToInt16(Request.Form["hidQuarter"]), overwriteUnits, Convert.ToDouble(Request.Form["txtCrsMin"]), Convert.ToDouble(Request.Form["txtCrsMax"]));
			
			if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The course entered already exists."; //use a better message see if the same course can exist in different quarters
				panError.Visible = true;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The update failed.";
				panError.Visible = true;
			}else{
				Response.Write("<script type=\"text/javascript\">opener.location.href='../edit.aspx?oid=" + hidOptionID.Value + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&todo=pop&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'","\\'") + "&pt=" + hidProgramTitle.Value.Replace("'","\\'") + "&pd=" + hidProgramDisplay.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&","%26") + "&searchid=" + hidSearchId.Value.Replace("&","%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&","%26").Replace("'","\\'") + "';self.close();</script>");
			}
		}

		protected void cmdSaveElectGroup_Click(object sender, System.EventArgs e){
			
			Int32 programDegreeID = Convert.ToInt32(hidProgramDegreeID.Value);
			Int32 optionID = Convert.ToInt32(hidOptionID.Value);
			Int32 degreeID = Convert.ToInt32(hidDegreeID.Value);
		
			Byte bitPrimaryOption = 0;
			if(Request.Form["hidPrimaryOption"] == "true"){
				bitPrimaryOption = 1;
			}
			csProgram.EditOptionValues(optionID, Convert.ToInt32(hidProgramVersionID.Value), Request.Form["hidOptionDescription"], Request.Form["hidOptionStateApproval"], Request.Form["hidCIP"], Request.Form["hidEPC"], Request.Form["hidAcademicPlan"], bitPrimaryOption, Request.Form["hidGainfulEmploymentID"], Request.Form["hidTotalQuarters"]);
			csProgram.DeleteOptionMCodes(optionID);
			if(hidMCodes.Value != ""){
				String[] strMCodes = hidMCodes.Value.Substring(1).Split('|');
				for(Int32 i = 0; i < strMCodes.Length; i++){
					String[] strCollegeMCode = strMCodes[i].Split(';');
					csProgram.AddOptionCollegeMCode(optionID, Convert.ToInt16(strCollegeMCode[0]), strCollegeMCode[1]);
				}
			}
			Int16 intSuccess = csProgram.EditElectGroupRef(hidTodo.Value, degreeID, optionID, Request.Form["hidOptionElectiveGroupID"], Convert.ToInt32(Request.Form["cboElectGroupTitle"]), Request.Form["hidCourseOffering"], Convert.ToInt16(Request.Form["hidQuarter"]), Convert.ToInt16(Request.Form["cboElectGroupQuarter"]), Convert.ToDouble(txtUnitsMinimum.Text), Convert.ToDouble(txtUnitsMaximum.Text), Convert.ToInt32(Request.Form["cboElectGroupFootnote"]));

			if(intSuccess == 2){
				lblErrorMsg2.Text = "Error: The elective group selected already exists."; //use a better message including the quarter
				panError2.Visible = true;
			}else if(intSuccess == 0){
				lblErrorMsg2.Text = "Error: The update failed.";
				panError2.Visible = true;
			}else{
				Response.Write("<script type=\"text/javascript\">opener.location.href='../edit.aspx?oid=" + hidOptionID.Value + "&pvid=" + hidProgramVersionID.Value + "&pdid=" + hidProgramDegreeID.Value + "&did=" + hidDegreeID.Value + "&todo=pop&ltr=" + hidLtr.Value + "&strm=" + hidSTRM.Value + "&scolid=" + hidSelectedCollegeID.Value + "&oa=" + hidOfferedAt.Value.Replace("'","\\'") + "&pt=" + hidProgramTitle.Value.Replace("'","\\'") + "&pd=" + hidProgramDisplay.Value + "&bstrm=" + hidProgramBeginSTRM.Value + "&estrm=" + hidProgramEndSTRM.Value + "&pp=" + hidPublishedProgram.Value + "&ppvid=" + hidPublishedProgramVersionID.Value + "&id=" + hidProgramID.Value + "&colid=" + hidCollegeID.Value + "&searchsub=" + hidSearchSubject.Value.Replace("&","%26") + "&searchid=" + hidSearchId.Value.Replace("&","%26") + "&searchstrm=" + hidSearchSTRM.Value + "&searchtxt=" + hidSearchText.Value.Replace("&","%26").Replace("'","\\'") + "';self.close();</script>");
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {    
		}
		#endregion
	}
}

