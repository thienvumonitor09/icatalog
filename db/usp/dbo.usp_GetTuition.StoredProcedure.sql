USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTuition]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu, Nguyen
-- Create date: 05/02/2019
-- Description:	Return Tuition fee
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetTuition]
	-- Add the parameters for the stored procedure here
	@ACAD_PLAN varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TuitionResident, TuitionNonResident, TuitionNonResidentInternational, BookCosts, PlanFees, CourseFees
	FROM GainfulEmployment 
	WHERE ACAD_PLAN = @ACAD_PLAN;
END
GO
