USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramFees]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/9/17
-- Description:	Returns program fees
--		 INPUT: ProgramVersionID - Program version primary key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramFees]
	@ProgramVersionID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT * FROM ProgramFee
    WHERE ProgramVersionID = @ProgramVersionID
END

GO
