USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseCatalogNumbersLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/16/17
-- Description:	Returns active course catalog numbers by term, college, and subject area.
--		 INPUT: STRM - Term code
--				SUBJECT (optional) - Course subject area / department
--				CollegeID (optional) - College primary key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseCatalogNumbersLegacy]
	@STRM varchar(4),
	@SUBJECT varchar(5) = NULL,
	@CollegeID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

    SELECT CATALOG_NBR 
    FROM Course 
    INNER JOIN CollegeCourse 
    ON CollegeCourse.CourseID = Course.CourseID
    WHERE CourseBeginSTRM <= @STRM 
    AND (CourseEndSTRM = 'Z999' OR CourseEndSTRM >= @STRM) 
    AND PublishedCourse = 1 
    AND (@Subject IS NULL OR (SUBJECT = @Subject OR SUBJECT = @Subject + '&'))
    AND (@CollegeID IS NULL OR CollegeID = @CollegeID)
    GROUP BY CATALOG_NBR 
    ORDER BY CATALOG_NBR;
END

GO
