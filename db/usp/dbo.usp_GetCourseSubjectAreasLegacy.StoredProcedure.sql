USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseSubjectAreasLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/12/17
-- Description:	Returns subject areas of courses offered during the term entered.
--		 INPUT: CollegeID (optional) - College primary key 
--				STRM (optional) - Term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseSubjectAreasLegacy] 
	@CollegeID int = NULL,
	@STRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)
					 
	SELECT Replace(Course.SUBJECT,'&','') AS SUBJECT, SubjectArea.DESCR AS SubjectArea_DESCR
    FROM Course 
    INNER JOIN SubjectArea 
    ON SubjectArea.SUBJECT = Course.SUBJECT 
    INNER JOIN CollegeCourse 
    ON CollegeCourse.CourseID = Course.CourseID
    WHERE CourseBeginSTRM <= @STRM AND (CourseEndSTRM = 'Z999' OR CourseEndSTRM >= @STRM) 
    AND PublishedCourse = 1 
    AND (@CollegeID IS NULL OR CollegeID = @CollegeID)
    GROUP BY Replace(Course.SUBJECT,'&',''), SubjectArea.DESCR
    ORDER BY SUBJECT;
END

GO
