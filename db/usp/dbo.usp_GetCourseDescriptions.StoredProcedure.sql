USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourseDescriptions]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 06/10/2019
-- Description:	Returns course descriptions by college, term, subject area, and catalog nbr. Courses using the same description are grouped together.
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourseDescriptions]
	@SUBJECT varchar(5),
	@INSTITUTION varchar(5) = NULL,
	@STRM varchar(4) = NULL,
	@CATALOG_NBR varchar(3) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)
					 
	DECLARE @SubjectLength int = LEN(@SUBJECT)

	SELECT CourseOffering.EFFDT, COURSE_TITLE_LONG, DESCRLONG, UNITS_MINIMUM, UNITS_MAXIMUM, SUBJECT + CATALOG_NBR AS CourseOffering, SUBJECT, CATALOG_NBR
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
		ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	WHERE CourseOffering.EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM)) 
	AND CATALOG_PRINT = 'Y'
	AND EFF_STATUS = 'A'
	AND ACAD_CAREER = 'UGRD' 
	AND (@INSTITUTION IS NULL OR INSTITUTION = @INSTITUTION)
	AND (@CATALOG_NBR IS NULL OR CATALOG_NBR = @CATALOG_NBR)
	AND (SUBJECT = @SUBJECT OR SUBJECT = @SUBJECT + '&') 
	GROUP BY CourseOffering.EFFDT, COURSE_TITLE_LONG, DESCRLONG, UNITS_MINIMUM, UNITS_MAXIMUM, SUBJECT, CATALOG_NBR
END
GO
