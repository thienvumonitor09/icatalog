USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramDegreesByProgramID]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 11/1/17
-- Description:	Returns degrees offered by a program
--		 INPUT: ProgramID - Non-changing program id
--				STRM - Term Code (optional) 
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramDegreesByProgramID]
	@ProgramID int,
	@STRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CurrentSTRM varchar(4);
	IF @STRM IS NULL
		SET @CurrentSTRM = (SELECT TOP 1(STRM)
							FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
							WHERE TERM_END_DT > GETDATE()
							ORDER BY TERM_END_DT)

    SELECT DegreeShortTitle, DegreeLongTitle, ProgramDegree.ProgramDegreeID, OptionTitle, OptionID
	FROM ProgramDegree 
	INNER JOIN Degree
	ON Degree.DegreeID = ProgramDegree.DegreeID
	INNER JOIN Program
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
	INNER JOIN ProgramDegreeOption
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID
	WHERE ProgramID = @ProgramID
	AND PublishedProgram = 1 
	AND ((@STRM IS NOT NULL AND (ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM)))
	OR (@STRM IS NULL AND (ProgramBeginSTRM <= @CurrentSTRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @CurrentSTRM))))
	ORDER BY DegreeLongTitle, OptionTitle;
	
	/*old query
	SELECT DegreeShortTitle, DegreeLongTitle, ProgramDegreeID
	FROM ProgramDegree 
	INNER JOIN Degree
	ON Degree.DegreeID = ProgramDegree.DegreeID
	INNER JOIN Program
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
	WHERE ProgramID = @ProgramID
	AND PublishedProgram = 1 
	AND ((@STRM IS NOT NULL AND (ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM)))
	OR (@STRM IS NULL AND (ProgramBeginSTRM <= @CurrentSTRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @CurrentSTRM))))
	ORDER BY DegreeShortTitle;
	*/
END
GO
