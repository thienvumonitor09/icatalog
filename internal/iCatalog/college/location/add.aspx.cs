using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.college.location
{
	/// <summary>
	/// Summary description for add.
	/// </summary>
	public partial class add : System.Web.UI.Page {

		programData csProgram = new programData();

		protected void Page_Load(object sender, System.EventArgs e) {
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			if(!IsPostBack){
				DataSet dsCollegeList = csProgram.GetCollegeList();
				cboCollege.Items.Add("");
				for(Int32 i = 0; i < dsCollegeList.Tables[0].Rows.Count; i++){
                    cboCollege.Items.Add(new ListItem(dsCollegeList.Tables[0].Rows[i]["CollegeLongTitle"].ToString(), dsCollegeList.Tables[0].Rows[i]["CollegeID"].ToString()));
				}
			}

			panError.Visible = false;
			panConfirm.Visible = false;
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			String locationTitle = txtLocationTitle.Text, locationAddress = txtLocationAddress.Text, locationCity = txtLocationCity.Text, locationState = txtLocationState.Text, locationZipCode1 = txtLocationZipCode1.Text, locationZipCode2 = txtLocationZipCode2.Text, locationWebsiteURL = txtLocationWebsiteURL.Text, locationRegistrationEmail = txtLocationRegistrationEmail.Text;
			Int16 intSuccess = csProgram.AddLocation(cboCollege.SelectedValue, locationTitle, locationAddress, locationCity, locationState, locationZipCode1, locationZipCode2, locationWebsiteURL, locationRegistrationEmail, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]); 

			if(intSuccess == 2){
				lblErrorMsg.Text = "Error: The location entered already exists.";
				panError.Visible = true;
				panConfirm.Visible = false;
			}else if(intSuccess == 0){
				lblErrorMsg.Text = "Error: The insert failed.";
				panError.Visible = true;
				panConfirm.Visible = false;
			}else if(intSuccess == 1){
				lblLocationTitle.Text = locationTitle;
				lblLocationAddress.Text = locationAddress;
				lblLocationCity.Text = locationCity;
				lblLocationState.Text = locationState;
				lblLocationZipCode.Text = locationZipCode1;
				if(locationZipCode2 != ""){
					lblLocationZipCode.Text += "-" + locationZipCode2;
				}
				if(locationWebsiteURL == ""){
					lblLocationWebsiteURL.Text = "&nbsp;";
				}else{
					lblLocationWebsiteURL.Text = locationWebsiteURL;
				}
				if(locationRegistrationEmail == ""){
					lblLocationRegistrationEmail.Text = "&nbsp;";
				}else{
					lblLocationRegistrationEmail.Text = locationRegistrationEmail;
				}
				panError.Visible = false;
				panConfirm.Visible = true;
				panAdd.Visible = false;
			}
		}

		public void PopulateFieds(object sender, System.EventArgs e){
			if(cboCollege.SelectedValue != ""){
				DataSet dsCollege = csProgram.GetCollege(Convert.ToInt32(cboCollege.SelectedValue));
				if(dsCollege.Tables[0].Rows.Count > 0){
					txtLocationTitle.Text = dsCollege.Tables[0].Rows[0]["CollegeLongTitle"].ToString();
					txtLocationAddress.Text = dsCollege.Tables[0].Rows[0]["CollegeAddress"].ToString();
					txtLocationZipCode1.Text = dsCollege.Tables[0].Rows[0]["CollegeZipCode1"].ToString();
					txtLocationZipCode2.Text = dsCollege.Tables[0].Rows[0]["CollegeZipCode2"].ToString();
					txtLocationCity.Text = dsCollege.Tables[0].Rows[0]["CollegeCity"].ToString();
					txtLocationState.Text = dsCollege.Tables[0].Rows[0]["CollegeState"].ToString();
					txtLocationWebsiteURL.Text = dsCollege.Tables[0].Rows[0]["CollegeWebsiteURL"].ToString();
					txtLocationRegistrationEmail.Text = dsCollege.Tables[0].Rows[0]["CollegeRegistrationEmail"].ToString();

					txtLocationTitle.Enabled = false;
					txtLocationAddress.Enabled = false;
                    txtLocationCity.Enabled = false;
                    txtLocationState.Enabled = false;
					txtLocationZipCode1.Enabled = false;
					txtLocationZipCode2.Enabled = false;
					txtLocationWebsiteURL.Enabled = false;
					txtLocationRegistrationEmail.Enabled = false;
				}
			}else{
				txtLocationTitle.Text = "";
				txtLocationAddress.Text = "";
				txtLocationZipCode1.Text = "";
				txtLocationZipCode2.Text = "";
				txtLocationCity.Text = "";
				txtLocationState.Text = "";
				txtLocationWebsiteURL.Text = "";
				txtLocationRegistrationEmail.Text = "";

				txtLocationTitle.Enabled = true;
				txtLocationAddress.Enabled = true;
                txtLocationCity.Enabled = true;
                txtLocationState.Enabled = true;
                txtLocationZipCode1.Enabled = true;
                txtLocationZipCode2.Enabled = true;
				txtLocationWebsiteURL.Enabled = true;
				txtLocationRegistrationEmail.Enabled = true;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {    
		}
		#endregion
	}
}
