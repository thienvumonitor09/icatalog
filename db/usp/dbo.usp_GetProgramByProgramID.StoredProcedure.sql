USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramByProgramID]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/9/17
-- Description:	Returns program details by program id and term (optional) - If STRM is NULL the current term is returned
--		 INPUT: ProgramID - Non-changing program id
--				STRM - Term Code (optional) 
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramByProgramID]
	@ProgramID int,
	@STRM varchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	/*Store the current term STRM value*/ 
	DECLARE @CurrentSTRM varchar(4);
	IF @STRM IS NULL
		SET @CurrentSTRM = (SELECT TOP 1(STRM)
							FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
							WHERE TERM_END_DT > GETDATE()
							ORDER BY TERM_END_DT)

    SELECT DISTINCT Program.ProgramVersionID, ProgramTitle, Program.CollegeID, CollegeShortTitle, CollegeLongTitle, CareerPlanningGuideID, ProgramEnrollment, ProgramWebsiteURL, WebsiteURL, ProgramDescription, 
    ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, ProgramBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, ProgramEndSTRM, EndTerm.DESCR AS EndTerm_DESCR
	FROM Program 
	LEFT OUTER JOIN AreaOfStudyProgram
	ON AreaOfStudyProgram.ProgramID = Program.ProgramID
	AND AreaOfStudyProgram.PrimaryAreaOfStudy = 1
	LEFT OUTER JOIN AreaOfStudy
	ON AreaOfStudy.AreaOfStudyID = AreaOfStudyProgram.AreaOfStudyID
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = ProgramBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = ProgramEndSTRM
	LEFT OUTER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
	AND PublishedCareerPlanningGuide = '1' 
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID 
	WHERE Program.ProgramID = @ProgramID
	AND PublishedProgram = 1 
	AND ((@STRM IS NOT NULL AND (ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM)))
	OR (@STRM IS NULL AND (ProgramBeginSTRM <= @CurrentSTRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @CurrentSTRM))))

END

GO
