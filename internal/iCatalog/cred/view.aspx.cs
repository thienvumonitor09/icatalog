using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.cred
{
	/// <summary>
	/// Summary description for view.
	/// </summary>
	public partial class view : System.Web.UI.Page
	{

		credentialData csCredential = new credentialData();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			TableRow tr = new TableRow();
			TableCell td = new TableCell();

			for(Int16 intAlphaCtr = 65; intAlphaCtr <= 90; intAlphaCtr++){
				char chrAlphaCtr = Convert.ToChar(intAlphaCtr);
				td = new TableCell();
				td.Attributes["style"] = "width:21px;text-align:center;";
				td.Attributes["onclick"] = "document.getElementById(\"hidLtr\").value='" + chrAlphaCtr + "';document.frmCred.submit();";
				td.CssClass = "portletMain";
				td.Controls.Add(new LiteralControl("<a class=\"ltr\" href=\"#\" onclick=\"document.getElementById(\"hidLtr\").value='" + chrAlphaCtr + "';document.frmCred.submit();\">" + chrAlphaCtr + "</a>"));
				tr.Cells.Add(td);
			}

			td = new TableCell();
			td.Attributes["style"] = "width:50px;text-align:center;";
			td.Attributes["onclick"] = "document.getElementById('hidTodo').value='search';document.getElementById('hidLtr').value=' ';document.frmCred.submit();";
			td.CssClass = "portletMain";
			td.Controls.Add(new LiteralControl("<a class=\"ltr\" href=\"#\" onclick=\"document.getElementById('hidLtr').value=' ';document.frmCred.submit();\">ALL</a>"));
			tr.Cells.Add(td);

			tblAlpha.Rows.Add(tr);

			String strLtr = hidLtr.Value;
			if(strLtr == ""){
				String strRequestLtr = Request.QueryString["ltr"];
				if(strRequestLtr == null || strRequestLtr == ""){
					strLtr = "A";
				}else{
					strLtr = strRequestLtr;
				}
			}

			DataSet dsCredentials = csCredential.GetCredentials(strLtr, cboCredentialWorkUnit.SelectedValue, txtCredSearch.Text);
			Int32 intRowCount = dsCredentials.Tables[0].Rows.Count;

			if(intRowCount > 0){
				for(Int32 intDSRow = 0; intDSRow < intRowCount; intDSRow++){
					panDisplay.Controls.Add(new LiteralControl("<p>"));
					panDisplay.Controls.Add(new LiteralControl("<span style=\"font-size:9pt;font-weight:bold;\">" + dsCredentials.Tables[0].Rows[intDSRow]["Name"].ToString() + "</span>"));
					panDisplay.Controls.Add(new LiteralControl("<br /><strong>" + dsCredentials.Tables[0].Rows[intDSRow]["WorkUnit"].ToString() + ", " + dsCredentials.Tables[0].Rows[intDSRow]["Assignment"].ToString() + "</strong>"));
					panDisplay.Controls.Add(new LiteralControl("<br />" + dsCredentials.Tables[0].Rows[intDSRow]["Description"].ToString()));
					panDisplay.Controls.Add(new LiteralControl("</p>"));
				}
			}else{
				panDisplay.Controls.Add(new LiteralControl("<p>Your search returned 0 results.</p>"));
			}
		}

		private void cmdExport_Click(object sender, System.EventArgs e){
			Response.Redirect(csCredential.ExportCredToExcel(hidLtr.Value, cboCredentialWorkUnit.SelectedValue, txtCredSearch.Text));
		}

		private void cmdGraphics_Click(object sender, System.EventArgs e){
			Response.Redirect(csCredential.ExportCredentialsToGraphics());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.cmdExport.Click += new EventHandler(cmdExport_Click);
			this.cmdGraphics.Click += new EventHandler(cmdGraphics_Click);
		}
		#endregion
	}
}
