<%@ Page language="c#" Inherits="ICatalog.course.popups.editterm" CodeFile="editterm.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
	<head>
		<title>Create New Course Version</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
		<link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
		<script type="text/javascript">
		<!--
			function cancelRevision(){
				opener.document.frmCourse.chkRevision.checked=false;
				opener.prepareRevision(false);
				opener.document.frmCourse.chkRevision.disabled=false;
				self.close();
			}
			
			function validate(){
				var cboCurrentBeginTerm = document.getElementById("cboCurrentBeginTerm");
				var cboCurrentEndTerm = document.getElementById("cboCurrentEndTerm");
				var cboRevisionBeginTerm = document.getElementById("cboRevisionBeginTerm");
				var cboRevisionEndTerm = document.getElementById("cboRevisionEndTerm");
				if(cboCurrentBeginTerm.value > cboCurrentEndTerm.value){
					alert("The current course end term cannot be before the current course begin term.");
					return false;
				}else if(cboRevisionBeginTerm.value == ""){
					alert("Please select the new course begin term.");
					return false;
				}else if(cboRevisionEndTerm.value == ""){
					alert("Please select the new course end term.");
					return false;
				}else if(cboRevisionBeginTerm.value > cboRevisionEndTerm.value){
					alert("The new course end term cannot be before the new course begin term.");
					return false;
				}else if(((cboCurrentBeginTerm.value >= cboRevisionBeginTerm.value && cboCurrentBeginTerm.value <= cboRevisionEndTerm.value) 
					|| (cboCurrentEndTerm.value <= cboRevisionEndTerm.value && cboCurrentEndTerm.value >= cboRevisionBeginTerm.value) 
					|| (cboCurrentBeginTerm.value >= cboRevisionBeginTerm.value && cboCurrentEndTerm.value <= cboRevisionEndTerm.value))){
						alert("The new course begin and end terms overlap with the current course begin and end terms.");
						return false;
				}else{
					return true;
				}
			}
		//-->
		</script>
	</head>
	<body style="background: #ffffff url('');" onload="closing=true" onunload="if (closing) opener.document.frmCourse.chkRevision.disabled=false;opener.document.frmCourse.chkRevision.checked=false;">
		<form id="frmFootnote" runat="server">
			<input type="hidden" id="hidCourseOffering" runat="server" />
			<input type="hidden" id="hidCourseBeginSTRM" runat="server" />
			<input type="hidden" id="hidCourseEndSTRM" runat="server" />
			<input type="hidden" id="hidPublishedCourseID" runat="server" />
			<input type="hidden" id="hidPublishedCourse" runat="server" />
			<input type="hidden" id="hidPage" runat="server" />
			<table class="centeredTable" style="width:410px;" cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding-left:4px;padding-right:0px;">
						<table style="width:100%;" cellpadding="0" cellspacing="0">
							<tr>
								<td style="background-color:#000000;text-align:left;">
									<table style="width:100%;" cellpadding="1" cellspacing="1">
										<tr>
											<td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;" class="portletHeader">
												<table style="width:100%;" cellpadding="0" cellspacing="0">
													<tr>
														<td class="portletHeader" style="width:100%;">&nbsp;Create New Course Version</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="portletMain" style="width:100%">
												<table style="width:100%;" cellspacing="0" cellpadding="0">
													<tr>
														<td class="portletDark" style="width:100%;">
															<table cellspacing="1" cellpadding="0" style="width:100%;">
																<tr>
																	<td class="portletLight" style="width:100%;vertical-align:text-top;">
																		<table cellpadding="0" cellspacing="0" style="width:100%;">
																			<tr>
																				<td style="width:100%;padding-bottom:15px;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<asp:panel runat="server" id="panMsg">
																							<tr>
																								<td class="portletLight" colspan="2" style="color:red;padding-left:10px;padding-top:15px;">
																									<asp:label runat="server" id="lblMsg"></asp:label>
																								</td>
																							</tr>
																						</asp:panel>
																						<!-- TERM -->
																						<tr>
																							<td style="padding-left:10px;padding-right:5px;padding-top:15px;width:100%;">
																								<table cellpadding="0" cellspacing="0">
																									<tr>
																										<td class="portletLight" style="padding-bottom:15px;">
																											<b>Current Course Term:</b>&nbsp;
																											<b>Begin</b>&nbsp;<asp:DropDownList ID="cboCurrentBeginTerm" CssClass="small" Runat="server"></asp:DropDownList>&nbsp;
																											<b>End</b>&nbsp;<asp:DropDownList ID="cboCurrentEndTerm" CssClass="small" Runat="server"></asp:DropDownList>
																										</td>
																									</tr>
																									<tr>
																										<td class="portletLight" style="padding-bottom:20px;">
																											<b>New Course Term:</b>&nbsp;
																											<b>Begin</b>&nbsp;<asp:dropdownlist id="cboRevisionBeginTerm" cssclass="small" runat="server"></asp:dropdownlist>&nbsp;
																											<b>End</b>&nbsp;<asp:dropdownlist id="cboRevisionEndTerm" cssclass="small" runat="server"></asp:dropdownlist>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<!-- BUTTONS -->
																						<tr>
																							<td class="portletLight" style="width:100%;text-align:center;">
																								<input type="button" id="cmdCancel" value="Cancel" class="small" style="background:#ccccaa;border:1px solid #000000;width:85px;" onclick="cancelRevision();" />
																								&nbsp;<asp:button runat="server" id="cmdSave" text="Create New Version" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:135px;" onclientclick="return validate();"></asp:button>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<span id="script" runat="server"></span>
		</form>
	</body>
</html>


