<%@ Reference Page="~/course/edit.aspx" %>
<%@ Page language="c#" Inherits="ICatalog.course.search" CodeFile="search.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Search Descriptions</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <script type="text/javascript" src="../_phatt3_src_files/trim.js"></script>
        <script type="text/javascript">
        <!-- 
			function validate(){
				var isValid = true;
				
				var txtTextSearch = document.getElementById('txtTextSearch');
				if(trim(txtTextSearch.value) == ""){
					alert("Please enter the text to search for.");
					txtTextSearch.select();
					isValid = false;
				}
				
				if(isValid){
					document.getElementById('hidTodo').value = "search";
					document.frmCourse.submit();
				}
			}
        //-->
        </script>
    </head>
  <body>
		<asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmCourse" runat="server">
					<input type="hidden" id="hidTodo" runat="server" />
					<input type="hidden" id="hidCourseID" runat="server" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Search Descriptions</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
																	<asp:panel runat="server" id="panDisplay">
																		<table style="width:100%;" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="portletSecondary" style="width:100%;padding:10px;">
																					<b>Search Long Course Descriptions for:</b>
																					&nbsp;<asp:TextBox ID="txtTextSearch" Runat="server" Columns="27" CssClass="small"></asp:TextBox>
																				</td>
																			</tr>
																			<tr>
																				<td class="portletSecondary" style="width:100%;text-align:center;padding:5px;padding-bottom:10px;">
																					<input type="button" id="cmdSearch" value="Search" class="small" onclick="validate();" />
																				</td>
																			</tr>
																			<asp:Panel ID="panResults" Runat="server">
																				<tr>
																					<td class="portletDark" style="width:100%;">
																						<asp:table runat="server" id="tblDisplay" style="width:100%;" cellspacing="1" cellpadding="2"></asp:table>
																					</td>
																				</tr>
																			</asp:Panel>
																		</table>
																	</asp:panel>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</form>
			</asp:panel>
			<div class="clearer"></div>
		</asp:panel>
	</body>
</html>
