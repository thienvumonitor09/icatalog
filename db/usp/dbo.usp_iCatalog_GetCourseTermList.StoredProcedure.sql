USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_iCatalog_GetCourseTermList]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/19/17
-- Description:	Returns term codes and titles for course descriptions (Plast four years, current, and future)
-- =============================================
CREATE PROCEDURE [dbo].[usp_iCatalog_GetCourseTermList]
	
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT STRM, vw_TermTable.DESCR, TERM_BEGIN_DT, TERM_END_DT 
	FROM Course 
	INNER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable 
	ON (STRM >= CourseBeginSTRM) AND (STRM <= CourseEndSTRM AND CourseEndSTRM <> 'Z999') 
	OR (STRM >= CourseBeginSTRM) AND (STRM <= CourseBeginSTRM) 
	WHERE TERM_END_DT > DATEADD(Year, -4, GETDATE()) 
	AND PublishedCourse <> 0 
	AND CCSGen_ctcLink_ODS.dbo.vw_TermTable.ACAD_CAREER = 'UGRD' /* filter out continuing ed to remove duplicate terms */
	AND CCSGen_ctcLink_ODS.dbo.vw_TermTable.INSTITUTION = 'WA171' /* pull terms for SCC only to avoid duplicates */
	ORDER BY TERM_END_DT;
END

GO
