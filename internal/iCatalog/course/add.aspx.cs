using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.course
{
	/// <summary>
	/// Summary description for add.
	/// </summary>
	public partial class add : System.Web.UI.Page
	{

		courseData csCourse = new courseData();

		#region PROTECTED CONTROLS
		protected System.Web.UI.WebControls.TextBox txtCourseEndSTRM;
		protected System.Web.UI.WebControls.Label lblSubject;
		#endregion 

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panError.Visible = false;
			panConfirm.Visible = false;

			if(!IsPostBack){
				DataSet dsSubjects = csCourse.GetSubjects();
				cboCourseSubject.Items.Add("");
				cboDescriptionSubject.Items.Add("");
				for(Int32 intDSRow = 0; intDSRow < dsSubjects.Tables[0].Rows.Count; intDSRow++){
					String subject = dsSubjects.Tables[0].Rows[intDSRow]["SUBJECT"].ToString();
					cboCourseSubject.Items.Add(subject);
					if(Convert.ToInt16(dsSubjects.Tables[0].Rows[intDSRow]["CrsCount"]) > 0){
						cboDescriptionSubject.Items.Add(subject);
					}
				}

				try{
					cboCourseSubject.Value = Request.QueryString["subject"];
					hidGenerateFromScript.Value = "true";
				}catch{
					hidGenerateFromScript.Value = "false";
				}

				DataSet dsTerms = csCourse.GetTermsForCourses();
				cboCourseEndSTRM.Items.Add("Z999");
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
					String STRM = dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString();
                    String DESCR = dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString();
					cboCourseBeginSTRM.Items.Add(new ListItem(DESCR, STRM));
                    cboCourseEndSTRM.Items.Add(new ListItem(DESCR, STRM));
				}

				termData csTerm = new termData();
				String currentSTRM = csTerm.GetCurrentTerm();
				try{
					cboCourseBeginSTRM.SelectedValue = currentSTRM;
					cboCourseEndSTRM.SelectedValue = "Z999"; //currentSTRM; //changed 11/19/09
				}catch{
					//do nothing
				}

			}else{
				hidGenerateFromScript.Value = "false";
			}

            //store colleges in arraylist for contains comparison
            ArrayList lstExistingColleges = new ArrayList();
            String[] aryCollegeList = hidCollegeList.Value.Split(',');
            for (Int32 i = 0; i < aryCollegeList.Length; i++) {
                lstExistingColleges.Add(aryCollegeList[i]);
            }

            //create and populate the college checkboxes
            programData csProgramData = new programData();
            DataSet dsCollegeList = csProgramData.GetCollegeList();
            panCollege.Controls.Clear();
            for (Int32 intDSRow = 0; intDSRow < dsCollegeList.Tables[0].Rows.Count; intDSRow++) {
                String collegeShortTitle = dsCollegeList.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();
                String collegeID = dsCollegeList.Tables[0].Rows[intDSRow]["CollegeID"].ToString();

                HtmlInputCheckBox chkOfferedAt = new HtmlInputCheckBox();
                chkOfferedAt.Value = collegeID;
                chkOfferedAt.ID = "chk" + collegeID;
                chkOfferedAt.Name = "chkOfferedAt";

                String strSelectedCollege = Request.Form[chkOfferedAt.ID];

                if (strSelectedCollege != null && strSelectedCollege != "") {
                    chkOfferedAt.Checked = true;
                }

                if (lstExistingColleges.Contains(collegeID)) {
                    chkOfferedAt.Checked = true;
                }

                chkOfferedAt.Attributes["onclick"] = "addCollege('" + chkOfferedAt.ID + "','" + collegeID + "');";
                panCollege.Controls.Add(new LiteralControl("&nbsp;"));
                panCollege.Controls.Add(chkOfferedAt);
                panCollege.Controls.Add(new LiteralControl(collegeShortTitle));
            }
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			String strOfferedAt = "";
			String variableUnits = "N";
			String courseOffering = Request.Form["cboCourseSubject"];
			String courseSubject = courseOffering;
			String descriptionCourseID = Request.Form["cboDescriptionCourse"];
			String courseBeginSTRM = Request.Form["cboCourseBeginSTRM"];
			String courseEndSTRM = Request.Form["cboCourseEndSTRM"];
			String descriptionSubject = Request.Form["cboDescriptionSubject"];
			String subjectDescription = csCourse.GetSubjectDescription(cboCourseSubject.Value);
			String catalogNbr = Request.Form["txtCatalogNbr"];
			String courseSuffix = Request.Form["cboCourseSuffix"];
			String courseShortTitle = Request.Form["txtCourseShortTitle"];
			String courseLongTitle = Request.Form["txtCourseLongTitle"];
			String courseLongDescription = Request.Form["txtCourseLongDescription"];
			String publishedCourse = Request.Form["optPublishedCourse"];
			Double unitsMinimum = Convert.ToDouble(Request.Form["txtUnitsMinimum"]), unitsMaximum = Convert.ToDouble(Request.Form["txtUnitsMaximum"]);

			//pad the dept div up to 5 spaces
			while(courseOffering.Length < 5){
				courseOffering += " ";
			}
			courseOffering = courseOffering.Replace(" ","&nbsp;") + catalogNbr + courseSuffix;

			//check if the credits are fixed or variable
			if(Request.Form["optVariableUnits" ] == "optVariableUnitsY"){
				variableUnits = "Y";
				txtUnitsMinimum.Disabled = false;
				optVariableUnitsY.Checked = true;
				optVariableUnitsN.Checked = false;
			}else{
				unitsMaximum = unitsMinimum;
				txtUnitsMinimum.Disabled = true;
				optVariableUnitsN.Checked = true;
				optVariableUnitsY.Checked = false;
			}

			//repopulate the source course combo boxes
            if (descriptionSubject != null && descriptionSubject != "") {
				DataSet dsDescriptionCourses = csCourse.GetDescriptionCourses(descriptionSubject);
				cboDescriptionCourse.Items.Clear();
				cboDescriptionCourse.Items.Add("");
				for(Int32 intDSRow = 0; intDSRow < dsDescriptionCourses.Tables[0].Rows.Count; intDSRow++){
					String dsDescriptionCourseID = dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseID"].ToString();
					cboDescriptionCourse.Items.Add(new ListItem(dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseOffering"].ToString() + " " + dsDescriptionCourses.Tables[0].Rows[intDSRow]["CourseBeginSTRM"].ToString(), dsDescriptionCourseID));
					if(descriptionCourseID == dsDescriptionCourseID){
						cboDescriptionCourse.Items[intDSRow + 1].Selected = true;
					}
				}
			}else{
				cboDescriptionCourse.Items.Clear();
				cboDescriptionCourse.Items.Add(new ListItem("Select a Long Description Subject", ""));
			}

			//repopulate form values
			txtUnitsMinimum.Value = unitsMinimum.ToString();
			divSubjectDescription.InnerHtml = subjectDescription;
			divCourseOffering.InnerHtml = courseOffering;

			//insert the course
            Int32 courseID = csCourse.AddCourse(courseOffering, courseBeginSTRM, courseEndSTRM, courseSubject, catalogNbr, courseSuffix, courseShortTitle, courseLongTitle, variableUnits, unitsMinimum, unitsMaximum, publishedCourse, csCourse.Strip(courseLongDescription).Replace("&nbsp;", " "), descriptionSubject, descriptionCourseID, DateTime.Now, Request.Cookies["phatt2"]["userctclinkid"]);

			if(courseID > 0){

                //delete college course join data
                csCourse.DeleteCourseColleges(courseID);
                //add college course join data
                if (hidCollegeList.Value != "") {
                    String[] strCollegeList = hidCollegeList.Value.Split(',');
                    programData csProgram = new programData();
                    for (Int16 i = 0; i < strCollegeList.Length; i++) {
                        Int32 collegeID = Convert.ToInt32(strCollegeList[i]);
                        csCourse.AddCollegeCourse(collegeID, courseID);
                        strOfferedAt += ", " + csProgram.GetCollegeShortTitle(collegeID); ;
                    }
                }
                if (strOfferedAt != "") {
                    strOfferedAt = strOfferedAt.Substring(2);
                }

				lblCourseOffering.Text = courseOffering;
				lblCourseBeginSTRM.Text = courseBeginSTRM;
				lblCourseEndSTRM.Text = courseEndSTRM;
				lblCourseSubject.Text = courseSubject;
				lblCatalogNbr.Text = catalogNbr;
				lblCourseSuffix.Text = courseSuffix;
				lblCourseShortTitle.Text = courseShortTitle;
				lblCourseLongTitle.Text = courseLongTitle;
				if(optVariableUnitsY.Checked){
					lblVariableUnits.Text = "Variable";
				}else{
					lblVariableUnits.Text = "Fixed";
				}
				lblPublishedCourse.Text = optPublishedCourse.SelectedItem.Text;
				lblSubjectDescription.Text = subjectDescription;
				lblUnitsMinimum.Text = unitsMinimum.ToString();
				lblUnitsMaximum.Text = unitsMaximum.ToString();
				lblOfferedAt.Text = strOfferedAt;
				lblCourseLongDescription.Text = courseLongDescription;
				if(descriptionCourseID != ""){
					lblDescriptionCourseOffering.Text = csCourse.GetCourseOffering(Convert.ToInt32(descriptionCourseID));
					panSameAs2.Visible = true;
				}else{
					panSameAs2.Visible = false;
				}

				panConfirm.Visible = true;
				panAdd.Visible = false;
			}else if(courseID == -1){
				//get beg and end terms of overlapping course
				String termSpan = csCourse.GetConflictingTermsForNewCourse(courseOffering.Replace("&nbsp;"," "), courseBeginSTRM, courseEndSTRM);

				//display error message
				lblErrorMsg.Text = "Error: The term entered overlaps an existing \"" + courseOffering + "\" course";
				if(termSpan != ""){
					lblErrorMsg.Text += " effective " + termSpan + ".";
				}else{
					lblErrorMsg.Text += ".";
				}

				panError.Visible = true;
				panConfirm.Visible = false;
			}else if(courseID == 0){
				lblErrorMsg.Text = "Error: The insert failed.";
				panError.Visible = true;
				panConfirm.Visible = false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
