USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_iCatalog_GetConflictingTermsForNewCourse]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 4/3/17
-- Description:	Returns begin and end terms of conflicting course (a course already exists with overlapping terms)
--		 INPUT:	CourseOffering - Course Subject and Catalog Nbr (ENGL101)
--				BeginSTRM - Course Begin Term Code
--				EndSTRM - Course End Term Code	
-- =============================================
CREATE PROCEDURE [dbo].[usp_iCatalog_GetConflictingTermsForNewCourse]
	@CourseOffering varchar(9),
	@BeginSTRM varchar(4),
	@EndSTRM varchar(4)
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT CourseBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, CourseEndSTRM, EndTerm.DESCR AS EndTerm_DESCR
	FROM Course
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = CourseBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = CourseEndSTRM
	WHERE CourseOffering = Replace(@CourseOffering,'&nbsp;',' ')
	AND ((CourseBeginSTRM >= @BeginSTRM
	AND CourseBeginSTRM <= @EndSTRM) 
	OR (CourseEndSTRM <= @EndSTRM
	AND CourseEndSTRM >= @BeginSTRM) 
	OR (CourseBeginSTRM >= @BeginSTRM
	AND CourseEndSTRM <= @EndSTRM));
END

GO
