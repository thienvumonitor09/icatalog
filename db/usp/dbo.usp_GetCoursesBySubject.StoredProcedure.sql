USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCoursesBySubject]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/07/2019
-- Description:	Return a list of courses by subject and end date
-- Parameters:	
--   @EndSTRM - Program end term code
--   @INSTITUITION - Instituition name
--   @CourseSubject - Subject name
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCoursesBySubject]
	@EndSTRM varchar(4),
	@INSTITUTION varchar(5), 
	@SUBJECT varchar(8) 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT (SUBJECT + CATALOG_NBR) AS CourseOffering, CATALOG_NBR
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	WHERE CourseOffering.EFFDT = 
		(SELECT MAX(CourseOffering2.EFFDT) 
			FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
			WHERE CourseOffering2.CRSE_ID = CourseOffering.CRSE_ID
				AND INSTITUTION = @INSTITUTION
				AND
				(
					CourseOffering2.EFFDT <= ((SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE STRM = @EndSTRM ORDER BY TERM_END_DT DESC))
					OR
					@EndSTRM = 'Z999'
				)
		) 
		AND REPLACE(SUBJECT,' ','') = @SUBJECT
		AND ACAD_CAREER = 'UGRD'
		AND CATALOG_PRINT = 'Y'
		AND EFF_STATUS = 'A'
		AND INSTITUTION = @INSTITUTION
	GROUP BY SUBJECT, CATALOG_NBR
	ORDER BY SUBJECT, CATALOG_NBR
END
GO
