﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

public partial class program_printexport : System.Web.UI.Page
{
        programData csProgram = new programData();
		termData csTerm = new termData();

		protected void Page_Load(object sender, System.EventArgs e){
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			if(!IsPostBack){
				DataSet dsTerms = csProgram.GetProgramTerms();
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
					cboTerm.Items.Add(new ListItem(dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString(), dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString()));
				}

				try{
					cboTerm.SelectedValue = csTerm.GetCurrentTerm();
				}catch{
					//do nothing
				}

                cboCollege.Items.Add(new ListItem("ALL", ""));
				DataSet dsCollege = csProgram.GetCollegeList();
				for(Int32 intDSRow = 0; intDSRow < dsCollege.Tables[0].Rows.Count; intDSRow++){
					cboCollege.Items.Add(new ListItem(dsCollege.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString(), dsCollege.Tables[0].Rows[intDSRow]["CollegeID"].ToString()));
				}
			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e) {
			Response.Redirect(csProgram.ExportToPrintableCatalog(Request.Form["cboTerm"], Request.Form["cboCollege"]));
            //uncomment to write out the rtf syntax generated
            //Response.Write(csProgram.ExportToPrintableCatalog(Request.Form["cboTerm"], Request.Form["cboCollege"]));
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}

