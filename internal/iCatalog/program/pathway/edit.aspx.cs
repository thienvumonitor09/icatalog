﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ICatalog._phatt3_classes;
using System.Data;

public partial class program_pathway_edit : System.Web.UI.Page {
    programData csProgram = new programData();

    protected void Page_Load(object sender, System.EventArgs e) {
        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        panError.Visible = false;
        panEdit.Visible = false;
        panDisplay.Visible = false;
        panConfirm.Visible = false;

        if (!IsPostBack) {
            DataSet dsPathways = csProgram.GetPathways(); 
            Int32 intPathwayCount = dsPathways.Tables[0].Rows.Count;

            TableRow tr = new TableRow();

            TableCell td = new TableCell();
            td.Attributes["style"] = "text-align:center;width:125px;font-weight:bold;padding-top:4px;padding-bottom:4px;";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("Offered By"));

            tr.Cells.Add(td);

            td = new TableCell();
            td.Attributes["style"] = "text-align:center;width:400px;font-weight:bold;padding-top:4px;padding-bottom:4px;";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("Pathway Title"));

            tr.Cells.Add(td);

            td = new TableCell();
            td.Attributes["style"] = "width:68px;text-align:center";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("<a href=\"add.aspx\">Add New</a>"));

            tr.Cells.Add(td);
            tblDisplay.Rows.Add(tr);

            if (intPathwayCount > 0) {

                for (Int32 intDSRow = 0; intDSRow < intPathwayCount; intDSRow++) {
                    Int32 pathwayID = Convert.ToInt32(dsPathways.Tables[0].Rows[intDSRow]["PathwayID"]);
                    String pathwayTitle = dsPathways.Tables[0].Rows[intDSRow]["PathwayTitle"].ToString();
                    String institution = dsPathways.Tables[0].Rows[intDSRow]["Institution"].ToString();

                    if(institution == "WA171") {
                        institution = "SCC";
                    }else if(institution == "WA172") {
                        institution = "SFCC";
                    }

                    tr = new TableRow();

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
                    td.Controls.Add(new LiteralControl(institution));

                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
                    td.Controls.Add(new LiteralControl(pathwayTitle));

                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;";
                    td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.getElementById('hidTodo').value='edit';document.getElementById('hidPathwayID').value='" + pathwayID + "';document.frmPathway.submit();\">Edit</a>"));
                    if (Convert.ToInt32(dsPathways.Tables[0].Rows[intDSRow]["AreaOfStudyCount"]) == 0) {
                        td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete " + pathwayTitle.Replace("'", "\\'") + " offered by " + institution + "?')){document.getElementById('hidTodo').value='delete';document.getElementById('hidPathwayID').value='" + pathwayID + "';document.frmPathway.submit();}\">Delete</a>"));
                    }
                    tr.Cells.Add(td);
                    tblDisplay.Rows.Add(tr);
                }
            } else {
                tr = new TableRow();

                td = new TableCell();
                td.ColumnSpan = 3;
                td.Attributes["style"] = "width:100%;padding:4px;";
                td.CssClass = "portletLight";
                td.Controls.Add(new LiteralControl("No guided pathways currently exist."));

                tr.Cells.Add(td);
                tblDisplay.Rows.Add(tr);
            }
            panDisplay.Visible = true;
        } else if (hidTodo.Value == "edit") {
            DataSet dsPathway = csProgram.GetPathway(Convert.ToInt32(hidPathwayID.Value));
            if (dsPathway.Tables[0].Rows.Count > 0) {
                Int16 areaOfStudyCount = Convert.ToInt16(dsPathway.Tables[0].Rows[0]["AreaOfStudyCount"]);
                String institution = dsPathway.Tables[0].Rows[0]["Institution"].ToString();
                optOfferedAt.SelectedValue = institution;
                if (areaOfStudyCount > 0) {
                    if(institution == "WA171") {
                        institution = "SCC";
                    }else if(institution == "WA172") {
                        institution = "SFCC";
                    }
                    lblOfferedAt.Text = institution;
                    lblOfferedAt.Visible = true;
                    optOfferedAt.Visible = false;
                } else { 
                    optOfferedAt.Visible = true;
                    lblOfferedAt.Visible = false;
                }
                txtPathwayTitle.Text = dsPathway.Tables[0].Rows[0]["PathwayTitle"].ToString();
                panEdit.Visible = true;
            }
            hidTodo.Value = "";
        } else if (hidTodo.Value == "delete") {
            csProgram.DeletePathway(Convert.ToInt32(hidPathwayID.Value));
            Response.Redirect("edit.aspx");
        }
    }

    protected void cmdSubmit_Click(object sender, System.EventArgs e) {
        Int16 intSuccess = csProgram.EditPathway(Convert.ToInt32(hidPathwayID.Value), optOfferedAt.SelectedValue, txtPathwayTitle.Text);

        if (intSuccess == 0) {
            lblErrorMsg.Text = "Error: The update failed.";
            panError.Visible = true;
            panEdit.Visible = true;
        } else if (intSuccess == 2) {
            lblErrorMsg.Text = "Error: The pathway entered already exists.";
            panError.Visible = true;
            panEdit.Visible = true;
        } else if (intSuccess == 1) {
            String institution = optOfferedAt.SelectedValue;
            if(institution == "WA171") {
                institution = "SCC";
            }else if(institution == "WA172") {
                institution = "SFCC";
            }
            lblOfferedBy.Text = institution;
            lblPathwayTitle.Text = txtPathwayTitle.Text;
            panConfirm.Visible = true;
        }
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e) {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
    }
    #endregion
}