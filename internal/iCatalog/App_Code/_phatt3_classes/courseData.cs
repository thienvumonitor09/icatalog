using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;

namespace ICatalog._phatt3_classes
{
	/// <summary>
	/// Summary description for course.
	/// </summary>
	public class courseData
	{
		private SqlConnection objCon;
		private String strSQL, strConnection;
		private SqlCommand objCmd;
		private SqlDataAdapter objDA;
		private DataSet objDS;
		private termData csTerm = new termData();

		public courseData(){
            //development connection string
            //strConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCS-SQL-Dev;Workstation ID=InternalICatalog;Integrated Security=true;";
            strConnection = "Initial Catalog=CCSICatalog;Data Source=CCS-SQL-Dev;Workstation ID=InternalICatalog;Integrated Security=true;";

            //production connection string
            //strConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=InternalICatalog;Integrated Security=true;";
            //strConnection = "Initial Catalog=CCSICatalog;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=InternalICatalog;Integrated Security=true;";
        }


        /******************************************** AddSubject ***********************************************
		 * INPUT:
		 * subject - Course Department
		 * subjectDescription - Course Department Title
		 * lastModifiedDate - Date/Time Inserted
		 * lastModifiedEmployeeID - SID of User Inserting Data
		 * 
		 * OUTPUT: Int16
		 *		   0 - Insert Failed
		 *		   1 - Insert Successful
		 *	       2 - The Record Already Exists
		 * 
		 * USED IN: /iCatalog/dept/add.aspx.cs
		 * 
		 * DESCRIPTION: Inserts the department courses belong to.
		 * 
		 *******************************************************************************************************/
        public Int16 AddSubject(String subject, String subjectDescription, DateTime lastModifiedDate, String lastModifiedEmployeeID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT * " +
					"FROM SubjectArea " +
					"WHERE SUBJECT='" + subject + "';";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				if(objDS.Tables[0].Rows.Count > 0){
					return 2;
				}else{
					strSQL = "INSERT INTO SubjectArea(SUBJECT, DESCR, LastModifiedDate, LastModifiedEMPLID) " +
						"VALUES ('" + subject + "','" + subjectDescription.Replace("'","''") + "','" + lastModifiedDate + "','" + lastModifiedEmployeeID + "');";

					objCmd = new SqlCommand(strSQL, objCon);
				
					Int32 intCtr = 0;
					intCtr = objCmd.ExecuteNonQuery();

					if(intCtr == 0){
						return 0;
					}else{
						return 1;
					}
				}

			}finally{
				objCon.Close();
			}
		}		


		/*************************************** GetSubjectsForDisplay *****************************************
		 * INPUT:
		 * chrLtr - Letter of Alphabet as a Char DataType
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: SUBJECT - Course Department
		 *             DESCR - Course Department Title
		 *             CrsCount - Number of Courses Belonging to the Department
		 * 
		 * USED IN: /iCatalog/dept/editdelete.aspx.cs
		 * 
		 * DESCRIPTION: Gets all the course departments starting with the alphabet letter entered along with 
		 *				a course count for each department.
		 * 
		 *******************************************************************************************************/
		public DataSet GetSubjectsForDisplay(Char chrLtr){

            objCon = new SqlConnection(strConnection);
            try
            {
                objCon.Open();
                objCmd = new SqlCommand("usp_GetCourseSubjects_2", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.AddWithValue("@ChrLtr", chrLtr);
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;
            }
            finally
            {
                objCon.Close();
            }
            
            /*
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

                strSQL = "SELECT SubjectArea.SUBJECT, SubjectArea.DESCR, Count(CourseID) AS CrsCount " +
					"FROM SubjectArea LEFT OUTER JOIN Course " +
					"ON Course.SUBJECT = SubjectArea.SUBJECT " +
					"WHERE SubjectArea.SUBJECT LIKE '" + chrLtr + "%' " +
                    "GROUP BY SubjectArea.SUBJECT, SubjectArea.DESCR " +
					"ORDER BY SubjectArea.SUBJECT;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
            */
        }


		/***************************************** GetSubjectForEdit *******************************************
		 * INPUT:
		 * subject - Course Department
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: SUBJECT - Course Department
		 *             DESCR - Course Department Title
		 *             LastModifiedDate - Date/Time Record Was Modified
		 *             LastModifiedEMPLID - SID of User Who Last Modified the Record
		 * 
		 * USED IN: /iCatalog/dept/editdelete.aspx.cs
		 * 
		 * DESCRIPTION: Gets the selected course department for display in the edit screen.
		 * 
		 *******************************************************************************************************/
		public DataSet GetSubjectForEdit(String subject){
			
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT * " + 
					"FROM SubjectArea " +
					"WHERE SUBJECT ='" + subject + "';";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		/******************************************** EditSubject **********************************************
		 * INPUT:
		 * subject - Course Department
		 * subjectDescription - Course Department Title
		 * 
		 * OUTPUT: bool
		 *		   true - Update Was Successful
		 *		   false - Update Failed
		 * 
		 * USED IN: /iCatalog/dept/editdelete.aspx.cs
		 * 
		 * DESCRIPTION: Updates the Course Department and Course Department Title.
		 * 
		 *******************************************************************************************************/
		public bool EditSubject(String subject, String subjectDescription){
			
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "UPDATE SubjectArea " +
					"SET DESCR = '" + subjectDescription.Replace("'","''") + 
					"' WHERE SUBJECT = '" + subject + "';";

				objCmd = new SqlCommand(strSQL, objCon);
				
				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();

				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


		/****************************************** DeleteSubject *********************************************
		 * INPUT:
		 * subject - Course Department
		 * 
		 * OUTPUT: bool
		 *		   true - Delete was successful
		 *		   false - Delete failed
		 * 
		 * USED IN: /iCatalog/dept/editdelete.aspx.cs
		 * 
		 * DESCRIPTION: Deletes the selected Course Department and all of its courses.
		 * 
		 *******************************************************************************************************/
		public bool DeleteSubject(String subject){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				//select the PK for all courses in subject being deleted
				strSQL = "SELECT CourseID " +
					"FROM Course " +
					"WHERE SUBJECT = '" + subject + "';";
		                   
				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				strSQL = "";
				for(Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++){
                    Int32 courseID = Convert.ToInt32(objDS.Tables[0].Rows[intDSRow]["CourseID"]);

                    //delete course colleges for courses in subject being deleted
                    DeleteCourseColleges(courseID, objCon);

					//update the DescriptionCourseID to null if the source course is being deleted
					strSQL += " UPDATE Course " +
						"SET DescriptionCourseID = NULL " +
						"WHERE DescriptionCourseID = " + courseID + ";";
				}

				//execute the update if courses exists
				if(strSQL.Length > 0){
					objCmd = new SqlCommand(strSQL, objCon);
					objCmd.ExecuteNonQuery();
				}
		          
				//delete all courses belonging to the subject being deleted 
				strSQL = "DELETE FROM Course " +
					"WHERE SUBJECT = '" + subject + "';";
				
				objCmd = new SqlCommand(strSQL, objCon);
				objCmd.ExecuteNonQuery();
		             
				//delete the subject       
				strSQL = "DELETE FROM SubjectArea " +
					"WHERE SUBJECT = '" + subject + "';";
		          
				objCmd = new SqlCommand(strSQL, objCon);
				
				Int32 intCtr = 0;
				intCtr = objCmd.ExecuteNonQuery();

				if(intCtr == 0){
					return false;
				}else{
					return true;
				}

			}finally{
				objCon.Close();
			}
		}


        /// <summary>
        /// Returns a list of courses in a specific subject/department (optional)
        /// </summary>
        /// <param name="SUBJECT">Course Subject (Example: ENGL) Enter an empty string to return courses for all subject areas</param>
        /// <returns>
        /// DataSet
        /// Field List: CourseID - Course Primary Key
        ///             CATALOG_NBR - Course Number
        ///             CourseOffering - Course Id (Subject Area padded to 5 spaces and Catalog Nbr)
        ///             CourseBeginSTRM - Course Begin STRM
        ///             BeginTerm_DESCR - Course Begin Term Description
        ///             TERM_BEGIN_DT - Term Begin Date
        ///		 	    CourseEndSTRM - Course End STRM
        ///		 	    EndTerm_DESCR - Course End Term Description
        ///		 	    TERM_END_DT - Term End Date
        ///             SUBJECT - Course Department
        ///             DESCR - Course Short Title
        ///             PublishedCourse - 0 = Working Copy / not displayed to the public, 1 = Published Copy / displayed to the public
        ///             PublishedCourseID - The primary key for the course with a shared long course description
        /// </returns>
        public DataSet GetSubjectAreaCourses(String SUBJECT){

            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCurrentAndFutureCourses", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                if (!String.IsNullOrEmpty(SUBJECT))
                {
                    objCmd.Parameters.AddWithValue("@SUBJECT", SUBJECT);
                }

                objCmd.Parameters.AddWithValue("@CATALOG_PRINT", 'Y');
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            }finally{
                objCon.Close();
            }
        }


		public Int16 CopySubjectCourse(Int32 courseID, String oldSubject, String newSubject, String oldEndSTRM, String newBeginSTRM, String newEndSTRM, DateTime lastModifiedDate, String lastModifiedEMPLID){
			
            //ONLY COPY CURRENT AND FUTURE COURSES 
			//IF MORE THAN ONE VERSION OF A COURSE EXISTS, ONLY COPY THE LATEST VERSION

            String currentSTRM = csTerm.GetCurrentTerm();
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				//check if the course being copied already exist in the new subject with a conflicting strm
				strSQL = "SELECT COUNT(*) FROM Course " +
						 "WHERE CourseID = " + courseID +
						 " AND CATALOG_NBR IN(" +
						 "SELECT CATALOG_NBR " +
						 "FROM Course " +
						 "WHERE SUBJECT = '" + newSubject +
						 "' AND ((CourseBeginSTRM >= '" + newBeginSTRM +
						 "' AND CourseBeginSTRM <= '" + newEndSTRM +
						 "') OR (CourseEndSTRM <= '" + newEndSTRM +
						 "' AND CourseEndSTRM >= '" + newBeginSTRM +
						 "') OR (CourseBeginSTRM >= '" + newBeginSTRM + 
						 "' AND CourseEndSTRM <= '" + newEndSTRM + "'))) " +
						 "AND (CourseEndSTRM = 'Z999' " +
                         "OR CourseEndSTRM >= '" + currentSTRM + 
						 "') AND PublishedCourse = 1;";

				objCmd = new SqlCommand(strSQL, objCon);

				if(Convert.ToInt32(objCmd.ExecuteScalar()) > 0){
					return 2;
				}else{

					//change end strm of the course if the end strm = Z999
					strSQL = "UPDATE Course " +
						"SET CourseEndSTRM = '" + oldEndSTRM + 
						"' WHERE CourseEndSTRM = 'Z999' " +
						"AND CourseID = " + courseID + ";";
					objCmd = new SqlCommand(strSQL, objCon);
					objCmd.ExecuteNonQuery();

					//get course data to be copied
                    strSQL = "SELECT CourseOffering, CourseBeginSTRM, CourseEndSTRM, SUBJECT, CATALOG_NBR, CourseSuffix, DESCR, COURSE_TITLE_LONG, VariableUnits, UNITS_MAXIMUM, UNITS_MINIMUM, PublishedCourse, DESCRLONG, LastModifiedDate, LastModifiedEMPLID " +
						"FROM Course " +
						"WHERE CourseID = " + courseID + ";";

					objCmd = new SqlCommand(strSQL, objCon);
					objDA = new SqlDataAdapter(objCmd);
					DataSet dsCourse = new DataSet();
                    objDA.Fill(dsCourse);

					strSQL = "";
                    for (Int32 intDSRow = 0; intDSRow < dsCourse.Tables[0].Rows.Count; intDSRow++){
						String courseOffering = newSubject;
						
						//pad the dept div up to 5 spaces
						while(courseOffering.Length < 5){
							courseOffering += " ";
						}
                        courseOffering = courseOffering + dsCourse.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString() + dsCourse.Tables[0].Rows[intDSRow]["CourseSuffix"].ToString();

						//insert course with new subject
                        strSQL = "INSERT INTO Course(CourseOffering, CourseBeginSTRM, CourseEndSTRM, SUBJECT, CATALOG_NBR, CourseSuffix, DESCR, COURSE_TITLE_LONG, VariableUnits, UNITS_MAXIMUM, UNITS_MINIMUM, PublishedCourse, DESCRLONG, LastModifiedDate, LastModifiedEMPLID) " +
                                "VALUES ('" + courseOffering + "','" + newBeginSTRM + "','" + newEndSTRM + "','" + newSubject + "','" + dsCourse.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString() + "','" + dsCourse.Tables[0].Rows[intDSRow]["CourseSuffix"].ToString() +
                                "','" + dsCourse.Tables[0].Rows[intDSRow]["DESCR"].ToString().Replace("'", "''") + "','" + dsCourse.Tables[0].Rows[intDSRow]["COURSE_TITLE_LONG"].ToString().Replace("'", "''") + "','" + dsCourse.Tables[0].Rows[intDSRow]["VariableUnits"].ToString() +
                                "'," + Convert.ToInt16(dsCourse.Tables[0].Rows[intDSRow]["UNITS_MAXIMUM"]) + "," + Convert.ToInt16(dsCourse.Tables[0].Rows[intDSRow]["UNITS_MINIMUM"]) +
                                ",'" + dsCourse.Tables[0].Rows[intDSRow]["PublishedCourse"].ToString() + "','" + dsCourse.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString().Replace("'", "''") + "','" + lastModifiedDate + "','" + lastModifiedEMPLID + "') " +
                                " SELECT CourseID FROM Course WHERE (CourseID = SCOPE_IDENTITY());";
						objCmd = new SqlCommand(strSQL, objCon);
                        try{
                            Int32 newCourseID = Convert.ToInt32(objCmd.ExecuteScalar());
                            DataSet dsCourseCollegeList = GetCourseCollege(courseID);

                            for (Int32 i = 0; i < dsCourseCollegeList.Tables[0].Rows.Count; i++){
                                AddCollegeCourse(Convert.ToInt32(dsCourseCollegeList.Tables[0].Rows[i]["CollegeID"]), newCourseID);
                            }
                        }catch{
                            return 0; //the insert failed
                        }
						
					}
					return 1;
				}

			}finally{
				objCon.Close();
			}
		}


        public Int16 CopySubject(String oldSubject, String newSubject, String oldEndSTRM, String newBeginSTRM, String newEndSTRM, DateTime lastModifiedDate, String lastModifiedEMPLID)
        {
            //ONLY COPY CURRENT AND FUTURE COURSES 
            //IF MORE THAN ONE VERSION OF A COURSE EXISTS, ONLY COPY THE LATEST VERSION

            String currentSTRM = csTerm.GetCurrentTerm();
            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                //check if one of the courses being copied already exist in the new subject with a conflicting strm
                strSQL = "SELECT COUNT(*) FROM Course " +
                         "WHERE SUBJECT = '" + oldSubject +
                         "' AND CATALOG_NBR IN(" +
                         "SELECT CATALOG_NBR " +
                         "FROM Course " +
                         "WHERE SUBJECT = '" + newSubject +
                         "' AND ((CourseBeginSTRM >= '" + newBeginSTRM +
                         "' AND CourseBeginSTRM <= '" + newEndSTRM +
                         "') OR (CourseEndSTRM <= '" + newEndSTRM +
                         "' AND CourseEndSTRM >= '" + newBeginSTRM +
                         "') OR (CourseBeginSTRM >= '" + newBeginSTRM +
                         "' AND CourseEndSTRM <= '" + newEndSTRM + "'))) " +
                         "AND (CourseEndSTRM = 'Z999' " +
                         "OR CourseEndSTRM >= '" + currentSTRM +
                         "') AND PublishedCourse = 1;";

                objCmd = new SqlCommand(strSQL, objCon);

                if (Convert.ToInt32(objCmd.ExecuteScalar()) > 0){
                    return 2;
                }else{

                    //new query to only change end strm of courses ending Z999
                    strSQL = "UPDATE Course " +
                        "SET CourseEndSTRM = '" + oldEndSTRM +
                        "' WHERE CourseEndSTRM = 'Z999' " +
                        "AND SUBJECT = '" + oldSubject + "';";
                    objCmd = new SqlCommand(strSQL, objCon);
                    objCmd.ExecuteNonQuery();

                    //get courses under old subject
                    strSQL = "SELECT * " +
                        "FROM Course " +
                        "WHERE SUBJECT = '" + oldSubject +
                        "' AND (CourseEndSTRM = 'Z999' " +
                        "OR CourseEndSTRM >= '" + currentSTRM +
                        "') AND PublishedCourse = 1 " +
                        "ORDER BY CourseOffering, CourseBeginSTRM DESC;";

                    objCmd = new SqlCommand(strSQL, objCon);
                    objDA = new SqlDataAdapter(objCmd);
                    DataSet dsCourse = new DataSet();
                    objDA.Fill(dsCourse);

                    strSQL = "";
                    for (Int32 intDSRow = 0; intDSRow < dsCourse.Tables[0].Rows.Count; intDSRow++){
                        Int32 courseID = Convert.ToInt32(dsCourse.Tables[0].Rows[intDSRow]["CourseID"]);
                        String courseOffering = newSubject;

                        //pad the dept div up to 5 spaces
                        while (courseOffering.Length < 5){
                            courseOffering += " ";
                        }
                        courseOffering = courseOffering + dsCourse.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString() + dsCourse.Tables[0].Rows[intDSRow]["CourseSuffix"].ToString();

                        //check if course already exists
                        strSQL = "SELECT COUNT(*) " +
                                 "FROM Course " +
                                 "WHERE CourseOffering = '" + courseOffering +
                                 "' AND ((CourseBeginSTRM >= '" + newBeginSTRM +
                                 "' AND CourseBeginSTRM <= '" + newEndSTRM +
                                 "') OR (CourseEndSTRM <= '" + newEndSTRM +
                                 "' AND CourseEndSTRM >= '" + newBeginSTRM +
                                 "') OR (CourseBeginSTRM >= '" + newBeginSTRM +
                                 "' AND CourseEndSTRM <= '" + newEndSTRM + "'));";

                        objCmd = new SqlCommand(strSQL, objCon);

                        if (Convert.ToInt32(objCmd.ExecuteScalar()) == 0){

                            //insert course with new subject
                            strSQL = "INSERT INTO Course(CourseOffering, CourseBeginSTRM, CourseEndSTRM, SUBJECT, CATALOG_NBR, CourseSuffix, DESCR, COURSE_TITLE_LONG, VariableUnits, UNITS_MAXIMUM, UNITS_MINIMUM, PublishedCourse, DESCRLONG, LastModifiedDate, LastModifiedEMPLID) " +
                                "VALUES ('" + courseOffering + "','" + newBeginSTRM + "','" + newEndSTRM + "','" + newSubject + "','" + dsCourse.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString() + "','" + dsCourse.Tables[0].Rows[intDSRow]["CourseSuffix"].ToString() +
                                "','" + dsCourse.Tables[0].Rows[intDSRow]["DESCR"].ToString().Replace("'", "''") + "','" + dsCourse.Tables[0].Rows[intDSRow]["COURSE_TITLE_LONG"].ToString().Replace("'", "''") + "','" + dsCourse.Tables[0].Rows[intDSRow]["VariableUnits"].ToString() + "'," + Convert.ToInt16(dsCourse.Tables[0].Rows[intDSRow]["UNITS_MAXIMUM"]) + "," + Convert.ToInt16(dsCourse.Tables[0].Rows[intDSRow]["UNITS_MINIMUM"]) +
                                ",'" + dsCourse.Tables[0].Rows[intDSRow]["PublishedCourse"].ToString() + "','" + dsCourse.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString().Replace("'", "''") + "','" + lastModifiedDate + "','" + lastModifiedEMPLID + "') " + 
                                " SELECT CourseID FROM Course WHERE (CourseID = SCOPE_IDENTITY());";
                            objCmd = new SqlCommand(strSQL, objCon);
                            try{
                                Int32 newCourseID = Convert.ToInt32(objCmd.ExecuteScalar());
                                DataSet dsCourseCollegeList = GetCourseCollege(courseID);

                                for (Int32 i = 0; i < dsCourseCollegeList.Tables[0].Rows.Count; i++){
                                    AddCollegeCourse(Convert.ToInt32(dsCourseCollegeList.Tables[0].Rows[i]["CollegeID"]), newCourseID);
                                }
                            }catch{
                                return 0; //the insert failed
                            }
                        }
                    }
                    return 1;
                }

            }finally{
                objCon.Close();
            }
        }


        public DataSet GetConflictingCourses(String oldSubject, String newSubject, String newBeginSTRM, String newEndSTRM){

            String currentSTRM = csTerm.GetCurrentTerm();
            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                strSQL = "SELECT CourseOffering, Course.DESCR, CourseBeginSTRM, CourseEndSTRM, PublishedCourse " +
                    "FROM Course " +
                    "WHERE SUBJECT = '" + oldSubject +
                    "' AND CATALOG_NBR IN( " +
                    "SELECT CATALOG_NBR " +
                    "FROM Course " +
                    "WHERE SUBJECT = '" + newSubject +
                    "' AND ((CourseBeginSTRM >= '" + newBeginSTRM +
                    "' AND CourseBeginSTRM <= '" + newEndSTRM +
                    "') OR (CourseEndSTRM <= '" + newEndSTRM +
                    "' AND CourseEndSTRM >= '" + newBeginSTRM +
                    "') OR (CourseBeginSTRM >= '" + newBeginSTRM +
                    "' AND CourseEndSTRM <= '" + newEndSTRM + "'))) " +
                    //new addition to query
                    "AND (CourseEndSTRM = 'Z999' " +
                    "OR CourseEndSTRM >= '" + currentSTRM +
                    "') AND PublishedCourse = 1 " +
                    "ORDER BY CourseOffering, CourseBeginSTRM DESC;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            }finally{
                objCon.Close();
            }
        }


		/********************************************* GetSubjects *********************************************
		 * INPUT: none
		 * 
		 * OUTPUT: DateSet
		 * FIELD LIST: SUBJECT - Course Department
		 *             CrsCount - Course Count
		 * 
		 * USED IN: /iCatalog/dept/copy.aspx.cs
		 *			/iCatalog/dept/proof.aspx.cs
		 *			/iCatalog/course/add.aspx.cs
		 *			/iCatalog/course/editdelete.aspx.cs	
		 *			/iCatalog/course/proof.aspx.cs
		 *			/iCatalog/course/view.aspx.cs
		 *			/iCatalog/program/coursesearch.aspx.cs
		 * 
		 * DESCRIPTION: Gets all Course Departments and a course count for each department.
		 * 
		 *******************************************************************************************************/
		public DataSet GetSubjects(){
            objCon = new SqlConnection(strConnection);
            try
            {
                objCon.Open();
                objCmd = new SqlCommand("usp_GetCourseSubjects_2", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;
            }
            finally
            {
                objCon.Close();
            }
            /*
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT SubjectArea.SUBJECT, COUNT(CourseID) AS CrsCount " +
					"FROM SubjectArea LEFT OUTER JOIN Course " +
					"ON Course.SUBJECT = SubjectArea.SUBJECT " +
					"GROUP BY SubjectArea.SUBJECT " +
					"ORDER BY SubjectArea.SUBJECT;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
            */
        }


        public DataSet GetEffectiveSubjects(String selectedSTRM){
            objCon = new SqlConnection(strConnection);

            try
            {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCourseSubjects_2", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                if (!String.IsNullOrEmpty(selectedSTRM))
                {
                    objCmd.Parameters.AddWithValue("@STRM", selectedSTRM);
                }
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            }
            finally
            {
                objCon.Close();
            }
            /*
            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                String strQuery = "";
                if(selectedSTRM != ""){
                    strQuery += "WHERE CourseBeginSTRM <= '" + selectedSTRM +
                         "' AND (CourseEndSTRM = 'Z999' " +
                         "OR CourseEndSTRM >= '" + selectedSTRM + "')";
                }

                strSQL = "SELECT SubjectArea.SUBJECT, COUNT(CourseID) AS CrsCount " +
                         "FROM SubjectArea " +
                         "LEFT OUTER JOIN Course " +
                         "ON Course.SUBJECT = SubjectArea.SUBJECT " +
                         strQuery +
                         " GROUP BY SubjectArea.SUBJECT " +
                         "ORDER BY SubjectArea.SUBJECT;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            }finally{
                objCon.Close();
            }
            */
        }
		

		/*************************************** GetSubjectsForProof *******************************************
		 * INPUT:
		 * subject - Course Department
		 * strOrderBy - Field to Order Returned Results By
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: SUBJECT - Course Department
		 *             DESCR - Course Title
		 * 
		 * USED IN: /iCatalog/dept/proof.aspx.cs
		 * 
		 * DESCRIPTION: Gets all Course Departments and Course Department Titles for display.
		 * 
		 *******************************************************************************************************/
		public DataSet GetSubjectsForProof(String subject, String strOrderBy){
			
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();
                objCmd = new SqlCommand("usp_GetCourseSubjects_2", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                if (!subject.Equals("ALL"))
                {
                    objCmd.Parameters.AddWithValue("@SUBJECT", subject);
                }
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");
                /*
				if(subject == "ALL"){
					strSQL = "SELECT SUBJECT, DESCR " +
						"FROM SubjectArea " +
						"ORDER BY " + strOrderBy + ";";
				}else{
					strSQL = "SELECT SUBJECT, DESCR " +
						"FROM SubjectArea " +
						"WHERE SUBJECT = '" + subject +
						"' ORDER BY " + strOrderBy + ";";
				}
                */
                objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
		}


		public DataSet GetSubjectsForProof(String selectedSTRM, String subject, String strOrderBy){
			
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();
                objCmd = new SqlCommand("usp_GetCourseSubjects_2", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                if (!subject.Equals("ALL"))
                {
                    objCmd.Parameters.AddWithValue("@SUBJECT", subject);
                }
                objCmd.Parameters.AddWithValue("@STRM", selectedSTRM);
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);

                return objDS;
                /*
				if(subject == "ALL"){
                    strSQL = "SELECT SubjectArea.SUBJECT, SubjectArea.DESCR " +
						"FROM SubjectArea INNER JOIN Course " +
                        "ON Course.SUBJECT = SubjectArea.SUBJECT " +
						"WHERE (CourseBeginSTRM <= '" + selectedSTRM +
						"' AND (CourseEndSTRM = 'Z999' " +
						"OR CourseEndSTRM >= '" + selectedSTRM +
						"')) AND PublishedCourse = 1 " +
                        "GROUP BY SubjectArea.SUBJECT, SubjectArea.DESCR " +
						"ORDER BY " + strOrderBy + ";";
				}else{
                    strSQL = "SELECT SubjectArea.SUBJECT, SubjectArea.DESCR " +
						"FROM SubjectArea INNER JOIN Course " +
                        "ON Course.SUBJECT = SubjectArea.SUBJECT " +
						"WHERE (CourseBeginSTRM <= '" + selectedSTRM +
						"' AND (CourseEndSTRM = 'Z999' " +
						"OR CourseEndSTRM >= '" + selectedSTRM +
						"')) AND PublishedCourse = 1 " +
                        "AND SubjectArea.SUBJECT = '" + subject +
                        "' GROUP BY SubjectArea.SUBJECT, SubjectArea.DESCR " +
						"ORDER BY " + strOrderBy + ";";
				}

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;
                */
            }
            finally{
				objCon.Close();
			}
		}


		/**************************************** GetSubjectDescription ********************************************
		 * INPUT:
		 * subject - Course Department
		 * 
		 * OUTPUT: (String) Course Department Title
		 *		   
		 * USED IN: /iCatalog/course/add.aspx.cs
		 *			/iCatalog/course/courseinfo.aspx.cs
		 *			/iCatalog/course/editdelete.aspx.cs
		 * 
		 * DESCRIPTION: Gets the Course Department Title for the Course Department entered.
		 * 
		 *******************************************************************************************************/
		public String GetSubjectDescription(String subject){
			
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT DESCR " +
					"FROM SubjectArea " +
					"WHERE SUBJECT = '" + subject + "';";

				objCmd = new SqlCommand(strSQL, objCon);

				try{
					return objCmd.ExecuteScalar().ToString();
				}catch(Exception){
					return "";
				}

			}finally{
				objCon.Close();
			}
		}


        /// <summary>
        /// Returns a list of published courses offered for a specific quarter/term (optional) in a specific subject/department (optional)	 
        /// </summary>
        /// <param name="SUBJECT">String: Course Subject (Example: ENGL)</param>
        /// <param name="STRM">String: Term code (Example: 2163)</param>
        /// <returns>
        /// DataSet
        /// Field List: CourseID - Course Primary Key
        ///             CATALOG_NBR - Course Number
        ///             CourseOffering - Course Id (Subject Area padded to 5 spaces and Catalog Nbr)
		///             CourseBeginSTRM - Course Begin STRM
        ///             BeginTerm_DESCR - Course Begin Term Description
		///		 	    CourseEndSTRM - Course End STRM
        ///		 	    EndTerm_DESCR - Course End Term Description
		///             SUBJECT - Course Department
		///             DESCR - Course Short Title
        /// </returns>
		public DataSet GetCourses(String SUBJECT, String STRM){

			objCon = new SqlConnection(strConnection);
            SUBJECT = SUBJECT.Replace("&","");

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_GetActiveCourses", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                if (!String.IsNullOrEmpty(STRM))
                {
                    objCmd.Parameters.AddWithValue("@STRM", STRM);
                }

                if (!String.IsNullOrEmpty(SUBJECT))
                {
                    objCmd.Parameters.AddWithValue("@SUBJECT", SUBJECT);
                }
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

			}finally{
				objCon.Close();
			}
		}
		

        /// <summary>
        /// Returns a list of courses offered for a specific quarter/term (optional) in a specific subject/department (optional)	
        /// </summary>
        /// <param name="SUBJECT">String: Course Subject (Example: ENG) Enter an empty string to return all course subjects.</param>
        /// <param name="STRM">String: Term code (Example: 2163) Enter an empty string to return current and future courses.</param>
        /// <returns>
        /// DataSet
        /// Field List: CourseID - Course Primary Key
        ///             CATALOG_NBR - Course Number
        ///             CourseOffering - Course Id (Subject Area padded to 5 spaces and Catalog Nbr)
        ///             CourseBeginSTRM - Course Begin STRM
        ///             BeginTerm_DESCR - Course Begin Term Description
        ///		 	    CourseEndSTRM - Course End STRM
        ///		 	    EndTerm_DESCR - Course End Term Description
        ///             SUBJECT - Course Department
        ///             DESCR - Course Short Title
        ///             PublishedCourse - 0 = Working Copy / not displayed to the public, 1 = Published Copy / displayed to the public
        ///             PublishedCourseID - The primary key for the course with a shared long course description
        /// </returns>
		public DataSet GetCurrentAndFutureCourses(String SUBJECT, String STRM){
            
			objCon = new SqlConnection(strConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCurrentAndFutureCourses", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                if (STRM != null && STRM != ""){
                    objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                    objCmd.Parameters["@STRM"].Value = STRM;
                }

                if (SUBJECT != null && SUBJECT != ""){
                    objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                    objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
                }
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            }finally{
				objCon.Close();
			}
		}


        /// <summary>
        /// Returns a list of archived courses by subject area and term
        /// </summary>
        /// <param name="SUBJECT">Course Subject Area: ENGL</param>
        /// <param name="STRM">Term Code</param>
        /// <returns>
        /// DataSet
        /// Field List: CourseID - Course Primary Key
        ///             CATALOG_NBR - Course Number
        ///             CourseOffering - Course Id (Subject Area padded to 5 spaces and Catalog Nbr)
        ///             CourseBeginSTRM - Course Begin STRM
        ///             BeginTerm_DESCR - Course Begin Term Description
        ///		 	    CourseEndSTRM - Course End STRM
        ///		 	    EndTerm_DESCR - Course End Term Description
        ///             SUBJECT - Course Department
        ///             DESCR - Course Short Title
        ///             PublishedCourse - 0 = Working Copy / not displayed to the public, 1 = Published Copy / displayed to the public
        ///             PublishedCourseID - The primary key for the course with a shared long course description
        /// </returns>
		public DataSet GetCourseArchives(String SUBJECT, String STRM){

			objCon = new SqlConnection(strConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCourseArchives", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                if (!String.IsNullOrEmpty(SUBJECT))
                {
                    objCmd.Parameters.AddWithValue("@SUBJECT", SUBJECT);
                }
                if (!String.IsNullOrEmpty(STRM))
                {
                    objCmd.Parameters.AddWithValue("@STRM", STRM);
                }
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

			}finally{
				objCon.Close();
			}
		}


		public bool HasWorkingCourseCopy(Int32 courseID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT COUNT(*) " +
					"FROM Course " +
					"WHERE PublishedCourseID = " + courseID + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				if(Convert.ToInt32(objCmd.ExecuteScalar()) > 0){
					return true;
				}else{
					return false;
				}

			}finally{
				objCon.Close();
			}
		}

		
		public bool UsedInProgram(Int32 courseOffering){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT (SELECT COUNT(*) " +
						 "FROM OptionPrerequisite " +
						 "WHERE CourseOffering = " + courseOffering +
						 ") AS PrereqCount, (SELECT COUNT(*) " +
						 "FROM OptionCourse " +
						 "WHERE CourseOffering = " + courseOffering +
						 ") AS CourseCount, (SELECT COUNT(*) " +
						 "FROM ElectiveGroupCourse " +
						 "WHERE CourseOffering = " + courseOffering +
						 ") AS ElectiveCount;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				if(Convert.ToInt32(objDS.Tables[0].Rows[0]["PrereqCount"]) > 0 
					|| Convert.ToInt32(objDS.Tables[0].Rows[0]["CourseCount"]) > 0
					|| Convert.ToInt32(objDS.Tables[0].Rows[0]["ElectiveCount"]) > 0){
					return true;
				}else{
					return false;
				}

			}finally{
				objCon.Close();
			}
		}


		/*OVERWRITES*/
		public bool UsedInProgram(String subject){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT (SELECT COUNT(*) " +
					"FROM OptionPrerequisite " +
                    "INNER JOIN Course " +
					"ON Course.CourseOffering = OptionPrerequisite.CourseOffering " +
					"WHERE SUBJECT = '" + subject +
					"') AS PrereqCount, (SELECT COUNT(*) " +
					"FROM OptionCourse " +
                    "INNER JOIN Course " +
					"ON Course.CourseOffering = OptionCourse.CourseOffering " +
					"WHERE SUBJECT = '" + subject +
					"') AS CourseCount, (SELECT COUNT(*) " +
					"FROM ElectiveGroupCourse " +
                    "INNER JOIN Course " +
					"ON Course.CourseOffering = ElectiveGroupCourse.CourseOffering " +
					"WHERE SUBJECT = '" + subject +
					"') AS ElectiveCount;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				if(Convert.ToInt32(objDS.Tables[0].Rows[0]["PrereqCount"]) > 0 
					|| Convert.ToInt32(objDS.Tables[0].Rows[0]["CourseCount"]) > 0
					|| Convert.ToInt32(objDS.Tables[0].Rows[0]["ElectiveCount"]) > 0){
					return true;
				}else{
					return false;
				}

			}finally{
				objCon.Close();
			}
		}


		/*************************************** GetCourseLongTitle **********************************************
		 * INPUT:
		 * courseID - Course Primary Key
		 * 
		 * OUTPUT: (String) Course Long Title
		 * 
		 * USED IN: /iCatalog/program/popups/addelective.aspx.cs
		 *			/iCatalog/program/popups/addoptcrs.aspx.cs
		 *			/iCatalog/program/popups/addprereq.aspx.cs
		 *			/iCatalog/program/popups/editelective.aspx.cs
		 *			/iCatalog/program/popups/editoptcrs.aspx.cs
		 *			/iCatalog/program/popups/editprereq.aspx.cs
		 * 
		 * DESCRIPTION: Gets the Course Long Title for the Course entered.
		 * 
		 *******************************************************************************************************/
		public String GetCourseLongTitle(Int32 courseID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT COURSE_TITLE_LONG " +
					"FROM Course " +
					"WHERE CourseID = " + courseID + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				
				try{
					return objCmd.ExecuteScalar().ToString();
				}catch{
					return "";
				}

			}finally{
				objCon.Close();
			}
		}


        /// <summary>
        /// Returns the most current version of a course in a program term range
        /// </summary>
        /// <param name="courseOffering">Course Id (Subject Area padded to 5 spaces and Catalog Nbr)</param>
        /// <param name="beginSTRM">Begin Term Code</param>
        /// <param name="endSTRM">End Term Code</param>
        /// <returns>
        /// DataSet
        /// Field List: CourseID - Course Primary Key
        ///             COURSE_TITLE_LONG - Long Course Title
        ///             CourseBeginSTRM - Course Begin STRM
        ///             BeginTerm_DESCR - Course Begin Term Description
        ///		 	    CourseEndSTRM - Course End STRM
        ///		 	    EndTerm_DESCR - Course End Term Description
        ///		 	    UNITS_MINIMUM - Minimum Course Credits/Units
        ///		 	    UNITS_MAXIMUM - Maximum Course Credits/Units
        ///		 	    VariableUnits - Course Variable Credit Bit Flag
        ///		 	
        /// </returns>
        /*
         *  * USED IN: /iCatalog/program/popups/addelective.aspx.cs
		 *			/iCatalog/program/popups/addoptcrs.aspx.cs
		 *			/iCatalog/program/popups/addprereq.aspx.cs
		 *			/iCatalog/program/popups/editelective.aspx.cs
		 *			/iCatalog/program/popups/editoptcrs.aspx.cs
		 *			/iCatalog/program/popups/editprereq.aspx.cs
         */
        public DataSet GetMostCurrentCourseForProgram(String courseOffering, String beginSTRM, String endSTRM){

			objCon = new SqlConnection(strConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_GetMostCurrentCourseForProgram", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.AddWithValue("@CourseOffering", courseOffering);
                objCmd.Parameters.AddWithValue("@EndSTRM", endSTRM);
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");
                /*
                objCmd.Parameters.Add("@CourseOffering", SqlDbType.VarChar);
                objCmd.Parameters["@CourseOffering"].Value = courseOffering;
                
                objCmd.Parameters.Add("@BeginSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@BeginSTRM"].Value = beginSTRM;

                objCmd.Parameters.Add("@EndSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@EndSTRM"].Value = endSTRM;
                */
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

			}finally{
				objCon.Close();
			}
		}


		/************************************** GetCoursesUsingSource *****************************************
		 * INPUT:
		 * courseID - Course Primary Key
		 * 
		 * OUTPUT: (String) Course ID and Effective Year Quarter of Courses using the Description of the Course
		 *					Entered
		 * 
		 * USED IN: /iCatalog/course/editdelete.aspx.cs
		 * 
		 * DESCRIPTION: Gets all courses using the Courses Description or the Course entered.
		 * 
		 *******************************************************************************************************/
		public String GetCoursesUsingSource(Int32 courseID){
			
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT CourseOffering, CourseBeginSTRM " +
					"FROM Course " +
					"WHERE DescriptionCourseID = " + courseID + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				String strCourses = "";

				for(Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++){
					strCourses += "\\n" + objDS.Tables[0].Rows[intDSRow]["CourseOffering"].ToString().Replace(" ","&nbsp;") + " " + objDS.Tables[0].Rows[intDSRow]["CourseBeginSTRM"].ToString();
				}

				return strCourses;

			}finally{
				objCon.Close();
			}
		}


        /// <summary>
        /// Returns course details
        /// </summary>
        /// <param name="courseID">(Int32) Course Primary Key</param>
        /// <returns>
        /// DataSet
		/// FIELD LIST: CourseID - Course Primary Key
        ///             CourseOffering - Course Id (Subject Area padded to 5 spaces and Catalog Nbr)
		///             CourseBeginSTRM - Course Begin STRM
        ///             BeginTerm_DESCR - Course Begin Term Description (Example: Spring 2017)
		///             CourseEndSTRM - Course End STRM
        ///             EndTerm_DESCR - Course End Term Description (Example: Spring 2018)
		///             SUBJECT - Course Department
		///             CATALOG_NBR - Course Number
		///             CourseSuffix - Course Number Suffix
		///             DESCR - Course Short Title
		///             COURSE_TITLE_LONG - Course Long Title
		///             DESCRLONG - Course Description
		///             VariableUnits - Variable Credit Indicator
		///             UNITS_MINIMUM - Course Minimum Credits
		///             UNITS_MAXIMUM - Course Maximum Credits
		///             LastModifiedDate - Date/Time Course Was Updated
		///             LastModifiedEMPLID - SID of User Who Last Updated the Course
		///             DescriptionCourseID - Primary Key of Course Being Used as the Source for the Description
		/// 			DESCR - Course Department Title
        /// </returns>
		public DataSet GetCourse(Int32 courseID){
			
			objCon = new SqlConnection(strConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_iCatalog_GetCourse", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("@CourseID", SqlDbType.Int);
                objCmd.Parameters["@CourseID"].Value = courseID;
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

			}finally{
				objCon.Close();
			}
		}

        public DataSet GetCourse(string CRSE_ID, string effdtStr)
        {

            objCon = new SqlConnection(strConnection);

            try
            {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCourse", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.AddWithValue("@CRSE_ID", CRSE_ID);
                objCmd.Parameters.AddWithValue("@EFFDT", Convert.ToDateTime(effdtStr));
                objCmd.Parameters.AddWithValue("@INSTITUTION", "WA171");
                //objCmd.Parameters.Add("@CourseID", SqlDbType.Int);
                //objCmd.Parameters["@CourseID"].Value = courseID;
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            }
            finally
            {
                objCon.Close();
            }
        }


        /**************************************** GetCourseOffering *******************************************
		 * INPUT:
		 * courseID - Course Primary Key
		 * 
		 * OUTPUT: (String) Course ID and Effective Year Quarter of Course Entered
		 * 
		 * USED IN: /iCatalog/course/add.aspx.cs
		 *			/iCatalog/course/view.aspx.cs
		 * 
		 * DESCRIPTION: Gets the Course ID and Effective Year Quarter of the Course entered.
		 * 
		 *******************************************************************************************************/
        public String GetCourseOffering(Int32 courseID){
			
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT CourseOffering, CourseBeginSTRM " +
					"FROM Course " +
					"WHERE CourseID = " + courseID + ";";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				if(objDS.Tables[0].Rows.Count > 0){
					return objDS.Tables[0].Rows[0]["CourseOffering"].ToString() + "&nbsp;" + objDS.Tables[0].Rows[0]["CourseBeginSTRM"].ToString();
				}else{
					return "";
				}

			}finally{
				objCon.Close();
			}
		}


		/// <summary>
        /// Returns courses that can be used as the source for another course's long descripiton
        /// </summary>
        /// <param name="SUBJECT">String: Course Subject (Example: ENGL)</param>
        /// <returns>
        /// DataSet
        /// Field List: CourseID - Course Primary Key
        ///             CourseOffering - Course Id (Subject Area padded to 5 spaces and Catalog Nbr)
        ///             CourseBeginSTRM - Course Begin STRM
        ///             BeginTerm_DESCR - Course Begin Term Description
        ///		 	    CourseEndSTRM - Course End STRM
        ///		 	    EndTerm_DESCR - Course End Term Description
        /// </returns>
		public DataSet GetDescriptionCourses(String SUBJECT){
			
			objCon = new SqlConnection(strConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_iCatalog_GetDescriptionCourses", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

			}finally{
				objCon.Close();
			}
		}


		/******************************************** GetDescriptionByCourseID ***********************************************
		 * INPUT:
		 * courseID - Course Primary Key
		 * 
		 * OUTPUT: (String) Course Description from the Source Course
		 * 
		 * USED IN: /iCatalog/course/courseinfo.aspx.cs
		 * 
		 * DESCRIPTION: Gets the Course Description for the Course Primary Key entered.
		 * 
		 *******************************************************************************************************/
		public String GetDescriptionByCourseID(Int32 courseID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT DESCRLONG " +
					"FROM Course " +
					"WHERE CourseID = " + courseID + ";";

				objCmd = new SqlCommand(strSQL, objCon);

				try{
					return objCmd.ExecuteScalar().ToString();
				}catch(Exception){
					return "";
				}

			}finally{
				objCon.Close();
			}
		}


        /// <summary>
        /// Gets all Terms/Quarters after Spring 1999 from the ctcLink ODS
        /// </summary>
        /// <returns>
        /// DataSet 
        /// Field List: STRM (Example: 2173)
        ///             DESCR (Example: SPRING 2017)
        /// </returns>
		public DataSet GetTermsForCourses(){
			
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();
                objCmd = new SqlCommand("usp_GetTerms", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = "2005";
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;
                
            }finally{
				objCon.Close();
			}
		}


        /// <summary>
        /// Gets terms greater than or equal to the greatest begin term of courses in a department
        /// </summary>
        /// <param name="SUBJECT">String: SUBJECT (Example: ENGL)</param>
        /// <returns>
        /// DataSet
        /// Field List: STRM (Example: 2173)
        ///             DESCR (Example: SPRING 2017)
        /// </returns>
		public DataSet GetCourseEndTerms(String SUBJECT){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();
                objCmd = new SqlCommand("usp_iCatalog_GetCourseEndTerms", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            }finally{
				objCon.Close();
			}						 
		}


        /********************************************* AddCourse ***********************************************
         * INPUT:
         * courseOffering - Course Id (Subject Area padded to 5 spaces and Catalog Nbr)
         * courseBeginSTRM - Course Begin Effective Year Quarter
         * courseEndSTRM - Course End Effective Year Quarter
         * courseSubject - Course Department
         * catalogNbr - Course Number
         * courseSuffix - Course Number Suffix
         * courseShortTitle - Course Short Title
         * courseLongTitle - Course Long Title
         * variableUnits - Variable Credit Indicator
         * unitsMinimum - Course Minimum Credits
         * unitsMaximum - Course Maximum Credits
         * courseLongDescription - Course Description
         * descriptionSubject - Course Department
         * descriptionCourseID - Primary Key of Course Being Used as the Source for the Description
         * lastModifiedDate - Date/Time Course is Inserted
         * lastModifiedEMPLID - SID of User Inserting Course
         * 
         * OUTPUT: Int32
         *		   0 - Insert Failed
         *		   -1 - Course Already Exists
         *		   > 1 - CourseID
         * 
         * USED IN: /iCatalog/course/add.aspx.cs
         * 
         * DESCRIPTION: Inserts a Course into the DB
         * 
         *******************************************************************************************************/
        public Int32 AddCourse(String courseOffering, String courseBeginSTRM, String courseEndSTRM, String courseSubject, String catalogNbr, String courseSuffix, String courseShortTitle, String courseLongTitle, String variableUnits, Double unitsMinimum, Double unitsMaximum, String publishedCourse, String courseLongDescription, String descriptionSubject, String descriptionCourseID, DateTime lastModifiedDate, String lastModifiedEMPLID){
			
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT COUNT(*) " +
					"FROM Course " +
					"WHERE CourseOffering = '" + courseOffering.Replace("&nbsp;"," ") +
					"' AND ((CourseBeginSTRM >= '" + courseBeginSTRM +
					"' AND CourseBeginSTRM <= '" + courseEndSTRM + 
					"') OR (CourseEndSTRM <= '" + courseEndSTRM +
					"' AND CourseEndSTRM >= '" + courseBeginSTRM +
					"') OR (CourseBeginSTRM >= '" + courseBeginSTRM +
					"' AND CourseEndSTRM <= '" + courseEndSTRM + "'));";

				objCmd = new SqlCommand(strSQL, objCon);

				if(Convert.ToInt32(objCmd.ExecuteScalar()) == 0){
					
					//if the source course is not in the same subject assign null to descriptionCourseID
					if(courseSubject != descriptionSubject){
						descriptionCourseID = "null";
					}

					//if a source course wasn't selected assign the description from the for to courseLongDescription
					if(descriptionCourseID == "" || descriptionCourseID == "null"){
						descriptionCourseID = "null";
						courseLongDescription = courseLongDescription.Replace("'","''");
						//assign the description from the source course to courseLongDescription
					}else{
						strSQL = "SELECT DESCRLONG " +
							"FROM Course " +
							"WHERE CourseID = " + descriptionCourseID + ";";

						objCmd = new SqlCommand(strSQL, objCon);
						courseLongDescription = objCmd.ExecuteScalar().ToString().Replace("'","''");
					}

					strSQL = "INSERT INTO Course(CourseOffering, CourseBeginSTRM, CourseEndSTRM, SUBJECT, CATALOG_NBR, CourseSuffix, DESCR, COURSE_TITLE_LONG, VariableUnits, UNITS_MAXIMUM, UNITS_MINIMUM, PublishedCourse, DESCRLONG, LastModifiedDate, LastModifiedEMPLID, DescriptionCourseID) " +
						"VALUES ('" + courseOffering.Replace("&nbsp;"," ") + "','" + courseBeginSTRM + "','" + courseEndSTRM + "','" + courseSubject + "','" + catalogNbr + "','" + courseSuffix + "','" + courseShortTitle.Replace("'","''") + "','" + courseLongTitle.Replace("'","''") + 
						"','" + variableUnits + "'," + unitsMaximum + "," + unitsMinimum + ",'" + publishedCourse + "','" + courseLongDescription + "','" + lastModifiedDate + "','" + lastModifiedEMPLID + "'," + descriptionCourseID + ") " +
						" SELECT CourseID FROM Course WHERE (CourseID = SCOPE_IDENTITY());";

					objCmd = new SqlCommand(strSQL, objCon);
				
					try{
						return Convert.ToInt32(objCmd.ExecuteScalar()); //return the CourseID
					}catch{
						return 0; //the insert failed
					}

				}else{
					return -1; //course already exists
				}

			}finally{
				objCon.Close();
			}
		}


        /// <summary>
        /// Returns begin and end terms of conflicting course (a course already exists with overlapping terms)
        /// </summary>
        /// <param name="courseOffering">Course Id (Subject Area padded to 5 spaces and Catalog Nbr)</param>
        /// <param name="BeginSTRM">Course Begin Term Code</param>
        /// <param name="EndSTRM">Course End Term Code</param>
        /// <returns>String - Conflicting Begin and End Terms</returns>
		public String GetConflictingTermsForNewCourse(String courseOffering, String BeginSTRM, String EndSTRM){

			objCon = new SqlConnection(strConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_iCatalog_GetConflictingTermsForNewCourse", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@CourseOffering", SqlDbType.VarChar);
                objCmd.Parameters["@CourseOffering"].Value = courseOffering;

                objCmd.Parameters.Add("@BeginSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@BeginSTRM"].Value = BeginSTRM;

                objCmd.Parameters.Add("@EndSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@EndSTRM"].Value = EndSTRM;

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                if (objDS.Tables[0].Rows.Count > 0){
                    return (objDS.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString() + " - " + objDS.Tables[0].Rows[0]["EndTerm_DESCR"].ToString());
                }else{
                    return "";
                }

			}finally{
				objCon.Close();
			}
		}


        /// <summary>
        /// Returns begin and end terms of conflicting course (a course already exists with overlapping terms)
        /// </summary>
        /// <param name="courseID">Course Primary Key</param>
        /// <param name="newCourseOffering">Course Id (Subject Area padded to 5 spaces and Catalog Nbr)</param>
        /// <param name="BeginSTRM">Course Begin Term Code</param>
        /// <param name="EndSTRM">Course End Term Code</param>
        /// <param name="publishedCourseID">Primary Key of Course Used as the Source for the Long Description</param>
        /// <returns>String - Conflicting Begin and End Terms</returns>
		public String GetConflictingTermsForExistingCourse(Int32 courseID, String newCourseOffering, String beginSTRM, String endSTRM, String publishedCourseID){

			objCon = new SqlConnection(strConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_iCatalog_GetConflictingTermsForExistingCourse", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.Add("@CourseID", SqlDbType.Int);
                objCmd.Parameters["@CourseID"].Value = courseID;

                objCmd.Parameters.Add("@CourseOffering", SqlDbType.VarChar);
                objCmd.Parameters["@CourseOffering"].Value = newCourseOffering;

                objCmd.Parameters.Add("@BeginSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@BeginSTRM"].Value = beginSTRM;

                objCmd.Parameters.Add("@EndSTRM", SqlDbType.VarChar);
                objCmd.Parameters["@EndSTRM"].Value = endSTRM;

                if (publishedCourseID != null && publishedCourseID != ""){
                    objCmd.Parameters.Add("@PublishedCourseID", SqlDbType.Int);
                    objCmd.Parameters["@PublishedCourseID"].Value = publishedCourseID;
                }

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                if (objDS.Tables[0].Rows.Count > 0){
                    return (objDS.Tables[0].Rows[0]["BeginTerm_DESCR"].ToString() + " - " + objDS.Tables[0].Rows[0]["EndTerm_DESCR"].ToString());
                }else{
                    return "";
                }

			}finally{
				objCon.Close();
			}
		}


		/********************************************* EditCourse **********************************************
		 * INPUT:
		 * courseID - Course Primary Key
		 * oldCourseOffering - Old Course ID
		 * newCourseOffering - New Course ID
		 * courseBeginSTRM - Course Begin Effective Year Quarter
		 * courseEndSTRM - Course End Effective Year Quarter
		 * courseSubject - Course Department
		 * catalogNbr - Course Number
		 * courseSuffix - Course Number Suffix
		 * courseShortTitle - Course Short Title
		 * courseLongTitle - Course Long Title
		 * variableUnits - Variable Credit Indicator
		 * unitsMinimum - Course Minimum Credits
		 * unitsMaximum - Course Maximum Credits
		 * courseLongDescription - Course Description
		 * descriptionSubject - Course Department of Course Being Used as the Source for the Description
		 * descriptionCourseID - Primary Key of Course Being Used as the Source for the Description
		 * lastModifiedDate - Date/Time Course Was Updated
		 * lastModifiedEMPLID - SID of User Updating the Course
		 * 
		 * OUTPUT: Int16
		 *		   0 - Update Failed
		 *		   1 - Update Was Successful
		 *		   2 - Record Already Exists
		 * 
		 * USED IN: /iCatalog/course/editdelete.aspx.cs
		 * 
		 * DESCRIPTION: Updates the selected Course
		 * 
		 *******************************************************************************************************/
		public Int32 EditCourse(Int32 courseID, String oldCourseOffering, String newCourseOffering, String courseBeginSTRM, String courseEndSTRM, String courseSubject, String catalogNbr, String courseSuffix, String courseShortTitle, String courseLongTitle, String variableUnits, Double unitsMinimum, Double unitsMaximum, String publishedCourse, String publishedCourseID, String courseLongDescription, String descriptionSubject, String descriptionCourseID, DateTime lastModifiedDate, String lastModifiedEMPLID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT PublishedCourse " +
						 "FROM Course " +
						 "WHERE CourseID = " + courseID + ";";

				objCmd = new SqlCommand(strSQL, objCon);

				String strOldStatus = objCmd.ExecuteScalar().ToString();
					
				strSQL = "SELECT COUNT(*) " + 
						"FROM Course " +
						"WHERE CourseOffering = '" + newCourseOffering + 
						"' AND ((CourseBeginSTRM >= '" + courseBeginSTRM +
						"' AND CourseBeginSTRM <= '" + courseEndSTRM + 
						"') OR (CourseEndSTRM <= '" + courseEndSTRM +
						"' AND CourseEndSTRM >= '" + courseBeginSTRM +
						"') OR (CourseBeginSTRM >= '" + courseBeginSTRM +
						"' AND CourseEndSTRM <= '" + courseEndSTRM +
						"')) AND CourseID <> " + courseID;
						
				if(publishedCourseID != ""){
					strSQL +=" AND CourseID <> " + publishedCourseID + ";";
				}

				objCmd = new SqlCommand(strSQL, objCon);
				if(Convert.ToInt32(objCmd.ExecuteScalar()) == 0){

					//if the source course is not in the same subject assign null to descriptionCourseID
					if(courseSubject != descriptionSubject){
						descriptionCourseID = "null";
					}

					//if a source course wasn't selected assign the description from the for to courseLongDescription
					if(descriptionCourseID == "" || descriptionCourseID == "null"){
						descriptionCourseID = "null";
						courseLongDescription = courseLongDescription.Replace("'","''");
						//assign the description from the source course to courseLongDescription
					}else{
						strSQL = "SELECT DESCRLONG " +
							"FROM Course " +
							"WHERE CourseID = " + descriptionCourseID + ";";

						objCmd = new SqlCommand(strSQL, objCon);
						courseLongDescription = objCmd.ExecuteScalar().ToString().Replace("'","''");
					}

					if(strOldStatus == "1" && publishedCourse == "0"){ //insert new course as hidden for minor changes
						//stores CourseID of Published Course Copy in PublishedCourseID
						strSQL = "INSERT INTO Course(CourseOffering, CourseBeginSTRM, CourseEndSTRM, SUBJECT, CATALOG_NBR, CourseSuffix, DESCR, COURSE_TITLE_LONG, VariableUnits, UNITS_MAXIMUM, UNITS_MINIMUM, PublishedCourse, PublishedCourseID, DESCRLONG, LastModifiedDate, LastModifiedEMPLID, DescriptionCourseID) " +
								 "VALUES('" + newCourseOffering + "','" + courseBeginSTRM + "','" + courseEndSTRM + "','" + courseSubject + "','" + catalogNbr + "','" + courseSuffix + "','" + courseShortTitle.Replace("'","''") + "','" + courseLongTitle.Replace("'","''") + 
								 "','" + variableUnits + "'," + unitsMaximum + "," + unitsMinimum + ",'" + publishedCourse + "'," + courseID + ",'" + courseLongDescription + "','" + lastModifiedDate + "','" + lastModifiedEMPLID + "'," + descriptionCourseID + ")" +
                                 " SELECT CourseID FROM Course WHERE (CourseID = SCOPE_IDENTITY());";

						objCmd = new SqlCommand(strSQL, objCon);

                        try{
                            return Convert.ToInt32(objCmd.ExecuteScalar()); //return the CourseID
                        }catch{
                            return 0; //the insert failed
                        }

					}else{ //update course

						if(strOldStatus == "0" && publishedCourse == "1"){ //update Published Course Copy and delete working course copy

                            Int32 intReturn = 0;

							strSQL = "SELECT PublishedCourseID " +
									 "FROM Course " +
									 "WHERE CourseID = " + courseID + ";";

							objCmd = new SqlCommand(strSQL, objCon);
							
							try{
								
								//if Published Course Copy exists
								publishedCourseID = objCmd.ExecuteScalar().ToString();

								//update the Published Course Copy
								strSQL = "UPDATE Course " +
									"SET CourseOffering = '" + newCourseOffering + "', CourseBeginSTRM = '" + courseBeginSTRM + "', CourseEndSTRM = '" + courseEndSTRM + "', SUBJECT = '" + courseSubject +
									"', CATALOG_NBR = '" + catalogNbr + "', CourseSuffix = '" + courseSuffix + "', DESCR = '" + courseShortTitle.Replace("'", "''") + 
									"', COURSE_TITLE_LONG = '" + courseLongTitle.Replace("'","''") + "', VariableUnits = '" + variableUnits + "', UNITS_MAXIMUM = " + unitsMaximum + ", UNITS_MINIMUM = " + unitsMinimum +
									", DESCRLONG = '" + courseLongDescription + "', LastModifiedDate = '" + lastModifiedDate + "', LastModifiedEMPLID = '" + lastModifiedEMPLID +
									"', DescriptionCourseID = " + descriptionCourseID +
									" WHERE CourseID = " + publishedCourseID + ";";

								objCmd = new SqlCommand(strSQL, objCon);
                                if (objCmd.ExecuteNonQuery() == 0){
                                    intReturn = 0;
                                }else{
                                    intReturn = Convert.ToInt32(publishedCourseID);
                                }
							
							}catch{
								
								//insert Published Course Copy
								strSQL = "INSERT INTO Course(CourseOffering, CourseBeginSTRM, CourseEndSTRM, SUBJECT, CATALOG_NBR, CourseSuffix, DESCR, COURSE_TITLE_LONG, VariableUnits, UNITS_MAXIMUM, UNITS_MINIMUM, PublishedCourse, DESCRLONG, LastModifiedDate, LastModifiedEMPLID, DescriptionCourseID) " +
									"VALUES ('" + newCourseOffering.Replace("&nbsp;"," ") + "','" + courseBeginSTRM + "','" + courseEndSTRM + "','" + courseSubject + "','" + catalogNbr + "','" + courseSuffix + "','" + courseShortTitle.Replace("'","''") + "','" + courseLongTitle.Replace("'","''") + 
									"','" + variableUnits + "'," + unitsMaximum + "," + unitsMinimum + ",'" + publishedCourse + "','" + courseLongDescription + "','" + lastModifiedDate + "','" + lastModifiedEMPLID + "'," + descriptionCourseID + ")" +
                                    " SELECT CourseID FROM Course WHERE (CourseID = SCOPE_IDENTITY());";

								objCmd = new SqlCommand(strSQL, objCon);

                                try{
                                    intReturn = Convert.ToInt32(objCmd.ExecuteScalar()); //store CourseID
                                }catch{
                                    intReturn = 0;
                                }
							
							}

                            if (intReturn != 0){
                                //delete hidden course copy
                                strSQL = "DELETE CollegeCourse " +
                                         "WHERE CourseID = " + courseID + ";";
                                objCmd = new SqlCommand(strSQL, objCon);
                                objCmd.ExecuteNonQuery();

                                strSQL = "DELETE Course " +
                                    "WHERE CourseID = " + courseID + ";";

                                objCmd = new SqlCommand(strSQL, objCon);
                                if (objCmd.ExecuteNonQuery() == 0){
                                    return 0; //return 0 delete failed
                                }else{
                                    return intReturn; //return CourseID
                                }
                            }else{
                                return intReturn; //return 0 updated/insert failed
                            }

						}else{

							strSQL = "UPDATE Course " +
								"SET CourseOffering = '" + newCourseOffering + "', CourseBeginSTRM = '" + courseBeginSTRM + "', CourseEndSTRM = '" + courseEndSTRM + "', SUBJECT = '" + courseSubject +
								"', CATALOG_NBR = '" + catalogNbr + "', CourseSuffix = '" + courseSuffix + "', DESCR = '" + courseShortTitle.Replace("'", "''") + 
								"', COURSE_TITLE_LONG = '" + courseLongTitle.Replace("'","''") + "', VariableUnits = '" + variableUnits + "', UNITS_MAXIMUM = " + unitsMaximum + ", UNITS_MINIMUM = " + unitsMinimum +
								", DESCRLONG = '" + courseLongDescription + "', LastModifiedDate = '" + lastModifiedDate + "', LastModifiedEMPLID = '" + lastModifiedEMPLID +
								"', DescriptionCourseID = " + descriptionCourseID +
								" WHERE CourseID = " + courseID + ";";

							objCmd = new SqlCommand(strSQL, objCon);

							Int32 intCtr = 0;
							intCtr = objCmd.ExecuteNonQuery();

							if(intCtr == 0){
								return 0;
							}else{
								return courseID;
							}
						}
					}
				}else{
					return -1;
				}

			}finally{
				objCon.Close();
			}
		}


		/******************************************* DeleteCourse *********************************************
		 * INPUT:
		 * courseID - Course Primary Key
		 * 
		 * OUTPUT: bool
		 *		   true - Delete Was Successful
		 *		   false - Delete Failed
		 * 
		 * USED IN: /iCatalog/course/editdelete.aspx.cs 
		 * 
		 * DESCRIPTION: Deletes the selected Course.
		 * 
		 *******************************************************************************************************/
		public bool DeleteCourse(Int32 courseID){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				//retrieve any courses using the selected course as their source for the description
				strSQL = "SELECT COUNT(*) " +
					"FROM Course " +
					"WHERE DescriptionCourseID = " + courseID + ";";
				
				//if no courses were retrieved delete the selected course
				objCmd = new SqlCommand(strSQL, objCon);
				if(Convert.ToInt32(objCmd.ExecuteScalar()) == 0){
                    //delete course colleges
                    DeleteCourseColleges(courseID, objCon);

					strSQL = "DELETE Course " +
						"WHERE CourseID = " + courseID + ";";

					objCmd = new SqlCommand(strSQL, objCon);
					
					Int32 intCtr = 0;
					intCtr = objCmd.ExecuteNonQuery();

					if(intCtr == 0){
						return false;
					}else{
						return true;
					}
				}else{
					return false;
				}

			}finally{
				objCon.Close();
			}
		}


		/**************************************** ExportCoursesToText ******************************************
		 * INPUT:
		 * courseBeginSTRM - Course Effective Year Quarter
         * strCollegeString - CollegeID comma delimited string
		 * 
		 * OUTPUT: String - path to text file generated
		 * 
		 * USED IN: /iCatalog/course/textexport.aspx.cs
		 * 
		 * DESCRIPTION: Generates a text file containing Course data pertaining to the parameters entered for 
		 *				upload to the HP3K.
		 *  
		 *******************************************************************************************************/
		public String ExportCoursesToText(String strCollegeString, String courseBeginSTRM){
			
			String strQuery = "";
            if (strCollegeString != "")
            {
                String[] strCollege = strCollegeString.Split(',');
                for (Int32 i = 0; i < strCollege.Length; i++)
                {
                    strQuery += " OR CollegeID = " + strCollege[i];
                }
                strQuery = " AND (" + strQuery.Substring(3) + ") ";
            }
			
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

                strSQL = "SELECT Replace(SubjectArea.SUBJECT,'&','') AS SUBJECT " +
                         "FROM SubjectArea " +
                         "INNER JOIN Course " +
                         "ON Course.SUBJECT = SubjectArea.SUBJECT " +
                         "INNER JOIN CollegeCourse " +
                         "ON CollegeCourse.CourseID = Course.CourseID " +
                         "WHERE CourseBeginSTRM <= '" + courseBeginSTRM + 
                         "' AND (CourseEndSTRM = 'Z999'" +
                         " OR CourseEndSTRM >= '" + courseBeginSTRM + 
                         "') AND PublishedCourse = '1' " +
                         strQuery +
                         " GROUP BY Replace(SubjectArea.SUBJECT,'&','') " +
                         "ORDER BY SUBJECT;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				DataSet objDept = new DataSet();
				objDA.Fill(objDept);

				for(Int32 intDSRow = 0; intDSRow < objDept.Tables[0].Rows.Count; intDSRow++){
					String courseSubject = objDept.Tables[0].Rows[intDSRow]["SUBJECT"].ToString();
					Int32 intLength = courseSubject.Length;

                    strSQL = "SELECT CourseBeginSTRM, CourseOffering, SUBJECT, CATALOG_NBR, CourseSuffix, DESCRLONG " +
                             "FROM Course " +
                             "INNER JOIN CollegeCourse " +
                             "ON CollegeCourse.CourseID = Course.CourseID " +
                             "WHERE CourseBeginSTRM <= '" + courseBeginSTRM + 
                             "' AND (CourseEndSTRM = 'Z999'" +
                             " OR CourseEndSTRM >= 'B341') " +
                             "AND PublishedCourse = '1' " +
                             strQuery +
                             " AND (SUBJECT = '" + courseSubject + "' OR SUBJECT = '" + courseSubject + "&') " +
                             "ORDER BY SUBSTRING(SUBJECT,1," + intLength + "), CATALOG_NBR, CourseOffering;";


					objCmd = new SqlCommand(strSQL, objCon);
					objDA = new SqlDataAdapter(objCmd);

					if(intDSRow == 0){
						objDS = new DataSet();
						objDA.Fill(objDS);
					}else{
						DataSet objTemp = new DataSet();
						objDA.Fill(objTemp);
						objDS.Merge(objTemp);
					}
				}
				
				StreamWriter objSW = new StreamWriter(HttpContext.Current.Server.MapPath("../TextCrsDesc.txt"),false,System.Text.Encoding.GetEncoding("utf-8"));

				for(Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++){
					String courseSubject = objDS.Tables[0].Rows[intDSRow]["SUBJECT"].ToString();
					String catalogNbr = objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString();
					String courseSuffix = objDS.Tables[0].Rows[intDSRow]["CourseSuffix"].ToString();

					//pad the subject to be 5 spaces
					while(courseSubject.Length < 5){
						courseSubject += " "; 
					}

					objSW.WriteLine("<X>" + courseSubject + " " + objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString() + objDS.Tables[0].Rows[intDSRow]["CourseSuffix"].ToString() + " " + "1");
					objSW.WriteLine(objDS.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString());
					objSW.WriteLine("<Z>");
				}

				objSW.Close();
				return "../TextCrsDesc.txt";

			}finally{
				objCon.Close();
			}
		}


		/************************************ ExportCoursesToGraphics ******************************************
		 * INPUT:
		 * courseBeginSTRM - Course Effective Year Quarter
		 * 
		 * OUTPUT: String - path to text file generated
		 * 
		 * USED IN: /iCatalog/course/graphicsexport.aspx.cs 
		 * 
		 * DESCRIPTION: Generates a text file containing Course data pertaining to the parameters entered for 
		 *				upload to Graphics "Page Maker".
		 * 
		 *******************************************************************************************************/
		public String ExportCoursesToGraphics(String courseBeginSTRM){
			
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

                strSQL = "SELECT Replace(SubjectArea.SUBJECT,'&','') AS SUBJECT " +
					"FROM SubjectArea INNER JOIN Course " +
                    "ON Course.SUBJECT = SubjectArea.SUBJECT " +
					"WHERE CourseOffering + ' ' + CourseBeginSTRM IN " +
					"(SELECT CourseOffering + ' ' + CourseBeginSTRM " +
					"FROM Course " +
					"WHERE CourseBeginSTRM <= '" + courseBeginSTRM +
					"' AND (CourseEndSTRM = 'Z999' " +
					"OR CourseEndSTRM >= '" + courseBeginSTRM + 
					"')) AND PublishedCourse = '1' " +
					"AND DescriptionCourseID IS NULL " +
                    "GROUP BY Replace(SubjectArea.SUBJECT,'&',''), SubjectArea.DESCR " +
                    "ORDER BY SubjectArea.DESCR;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				DataSet objDept = new DataSet();
				objDA.Fill(objDept);

				for(Int32 intDSRow = 0; intDSRow < objDept.Tables[0].Rows.Count; intDSRow++){
					String courseSubject = objDept.Tables[0].Rows[intDSRow]["SUBJECT"].ToString();
					Int32 intLength = courseSubject.Length;

					strSQL = "SELECT A.CourseBeginSTRM, A.CourseID, A.COURSE_TITLE_LONG, A.DESCRLONG, " +
						"A.UNITS_MINIMUM, A.UNITS_MAXIMUM, A.VariableUnits, A.CourseOffering, " +
						"A.SUBJECT, A.CATALOG_NBR as CATALOG_NBR_A, B.CATALOG_NBR as CATALOG_NBR_B, " +
                        "B.DescriptionCourseID, SubjectArea.DESCR " +
						"FROM Course A " +
						"LEFT OUTER JOIN Course B " +
						"ON A.CourseID = B.DescriptionCourseID " +
						"INNER JOIN SubjectArea " +
                        "ON A.SUBJECT = SubjectArea.SUBJECT " +
						"WHERE A.CourseOffering + ' ' + A.CourseBeginSTRM IN " +
						"(SELECT CourseOffering + ' ' + CourseBeginSTRM " +
						"FROM Course " +
						"WHERE CourseBeginSTRM <= '" + courseBeginSTRM + 
						"' AND (CourseEndSTRM = 'Z999' " +
						"OR CourseEndSTRM >= '" + courseBeginSTRM + "')) " +
						"AND A.PublishedCourse = '1' " +
						"AND A.DescriptionCourseID IS NULL " +
						" AND (A.SUBJECT = '" + courseSubject + "' OR A.SUBJECT = '" + courseSubject + "&') " +
                        "ORDER BY SUBSTRING(SubjectArea.SUBJECT,1," + intLength + "), SUBSTRING(A.SUBJECT,1," + intLength + "), A.CATALOG_NBR, B.CATALOG_NBR;";

					objCmd = new SqlCommand(strSQL, objCon);
					objDA = new SqlDataAdapter(objCmd);

					if(intDSRow == 0){
						objDS = new DataSet();
						objDA.Fill(objDS);
					}else{
						DataSet objTemp = new DataSet();
						objDA.Fill(objTemp);
						objDS.Merge(objTemp);
					}
				}

				bool blnClearString = true, blnMoveNext = true, blnWriteFile;
				String courseLongTitle = "", courseLongDescription = "", catalogNbrs = "", variableUnits = "", strOfferedAt = "", strCrsCredits = "", subjectDescription = "";
                Double unitsMinimum = 0, unitsMaximum = 0;
				Int32 courseID = 0;

				StreamWriter objSW = new StreamWriter(HttpContext.Current.Server.MapPath("../GraphicsCrsDesc.txt"),false,System.Text.Encoding.GetEncoding("utf-8"));
				//StreamWriter objSW = File.CreateText(HttpContext.Current.Server.MapPath("../GraphicsCrsDesc.txt"));

				for(Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++){
                    Int32 dsCourseID = Convert.ToInt32(objDS.Tables[0].Rows[intDSRow]["CourseID"]);

                    //get course colleges
                    DataSet dsCourseCollege = GetCourseCollege(dsCourseID, objCon);
                    strOfferedAt = "";
                    for (Int32 i = 0; i < dsCourseCollege.Tables[0].Rows.Count; i++){
                        strOfferedAt += ", " + dsCourseCollege.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
                    }
                    strOfferedAt = "(" + strOfferedAt.Substring(2) + ")";
				
					//If a title and description have not been stored or have been cleared store the current record values 
					if(courseLongTitle.Length == 0 && courseLongDescription.Length == 0){
						courseLongTitle = objDS.Tables[0].Rows[intDSRow]["COURSE_TITLE_LONG"].ToString();
						courseLongDescription = objDS.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString();
					}
					
					//----------------- determine where the course is offered used to go here -------------------
					
					//if the course is not a duplicate record, the DescriptionCourseID is not null, and the course numbers aren't the same 
					//check to see if a string of course numbers exists 

					if(courseID != dsCourseID && objDS.Tables[0].Rows[intDSRow]["DescriptionCourseID"].ToString() != "" && objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() != objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString()){

						//if a string of course numbers does not exist
						//start a new string and store all other values needed
						//WriteFile is set to false until the record stops repeating
						if(catalogNbrs == ""){
							catalogNbrs = objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() + ", " + objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString();
							courseLongDescription = objDS.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString();
							courseLongTitle = objDS.Tables[0].Rows[intDSRow]["COURSE_TITLE_LONG"].ToString();
							variableUnits = objDS.Tables[0].Rows[intDSRow]["VariableUnits"].ToString();
							unitsMinimum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MINIMUM"]);
							unitsMaximum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MAXIMUM"]);
							blnWriteFile = false;

							//if a string of course numbers does exist a new course has started
							//assign True to WriteFile to write the courses using the previous course as the source
							//assign False to ClearString which is later used to concatinate course numbers using the current course as the source
						}else{
							blnWriteFile = true;
							blnClearString = false;
							blnMoveNext = true;
						}

						//if the course is a duplicate record, the DescriptionCourseID is not null, and the course numbers aren't the same
						//concatinate the course number to the existing course number string
						//WriteFile is set to false until the record stops repeating
					}else if(courseID == dsCourseID && objDS.Tables[0].Rows[intDSRow]["DescriptionCourseID"].ToString() != "" && objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() != objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString()){
						if(catalogNbrs.Substring((catalogNbrs.Length - 3)) != objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString()){
							catalogNbrs += ", " + objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString();
						}
						blnWriteFile = false;
						blnMoveNext = true;
					
						//The course is not a duplicate record assign True to WriteFile
						//Assign true to ClearString so the new course values can be stored in COURSE_TITLE_LONG, courseLongDescription, and catalogNbrs
					}else{
						blnWriteFile = true;
						blnClearString = true;
						
						//If a string of course numbers exist assign false to MoveNext so the recordset stays on the current record
						//which will be used in the next loop to store the new course values
						if(catalogNbrs.Length > 3){
							blnMoveNext = false;
			                  
							//If a string of course numbers does not exist store the current course values used for the credits and the course number
						}else{
							catalogNbrs = objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString();
							variableUnits = objDS.Tables[0].Rows[intDSRow]["VariableUnits"].ToString();
							unitsMinimum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MINIMUM"]);
							unitsMaximum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MAXIMUM"]);
							blnMoveNext = true;
						}
					}

					//If a string of course numbers does exist create the string of credits to write to the file 
					//for multiple courses using the same source  
					if(catalogNbrs.Length > 3){
						if(variableUnits == "Y"){
							strCrsCredits = " (" + unitsMinimum + "-" + unitsMaximum + " cr ea)";
						}else{
							strCrsCredits = " (" + unitsMaximum + " cr ea)";
						}
			                  
						//If a string of course numbers does not exist create the string of credits to write to the file 
						//for a single course
					}else{
						if(variableUnits == "Y"){
							strCrsCredits = " (" + unitsMinimum + "-" + unitsMaximum + " cr)";
						}else{
							strCrsCredits = " (" + unitsMaximum + " cr)";
						}
					}

					//If a new course has been started in the loop assign the PK to the CrsASN variable used
					//in the conditions above to create the string of course numbers
					if(courseID != dsCourseID){
						courseID = dsCourseID;
					}

					//Write the file 
					if(blnWriteFile){ 
						//If a new subject is started in the loop write the subject description to the file 
						if(subjectDescription != objDS.Tables[0].Rows[intDSRow]["DESCR"].ToString()){
							subjectDescription = objDS.Tables[0].Rows[intDSRow]["DESCR"].ToString();
							objSW.WriteLine("<@HEAD-rev-crs:>" + subjectDescription);
						}
						objSW.WriteLine("<@CRS:>" + objDS.Tables[0].Rows[intDSRow]["SUBJECT"].ToString() + " " + catalogNbrs + "<k-0.2> <\\�><k-0.2> " + courseLongTitle + strCrsCredits);
						objSW.WriteLine("<@DESC:>" + courseLongDescription + " " + strOfferedAt);
			                  
						//Clear the current course values so new values can be stored in the next loop
						if(blnClearString){
							courseLongTitle = "";
							courseLongDescription = "";
							catalogNbrs = "";
							//Store the current course values to be written in the next loop
						}else{
							catalogNbrs = objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() + ", " + objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString();
							courseLongDescription = objDS.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString();
							courseLongTitle = objDS.Tables[0].Rows[intDSRow]["COURSE_TITLE_LONG"].ToString();
							variableUnits = objDS.Tables[0].Rows[intDSRow]["VariableUnits"].ToString();
							unitsMinimum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MINIMUM"]);
							unitsMaximum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MAXIMUM"]);
						}
					}
					if(!blnMoveNext){
						intDSRow = intDSRow - 1;
					}
				}

				objSW.Close();
				return "../GraphicsCrsDesc.txt";

			}finally{
				objCon.Close();
			}
		}

        /************************************ ExportToPrintableCatalog ******************************************
		 * INPUT:
		 * courseBeginSTRM - Course Effective Year Quarter
		 * 
		 * OUTPUT: String - path to RTF file generated
		 * 
		 * USED IN: /iCatalog/course/printexport.aspx.cs 
		 * 
		 * DESCRIPTION: Generates a RTF file containing Course data pertaining to the parameters entered for 
		 *				the printable catalog.
		 * 
		 *******************************************************************************************************/
        public String ExportToPrintableCatalog(String courseBeginSTRM) {
            /********* RTF COMMANDS AND SYNTAX ************
            Line 1: Begin document; RTF version 1; character set; default font 0 black Arial
            {\rtf1\ansi\deff0 {\fonttbl {\f0 Arial;}}
            Line 2: Default language = 1033 (English); reset all previous font settings; size 17 (half points; must double)
            \deflang1033 \plain \fs17
            Line 3: Columns (2)
            \cols2
            Text formats: 
            \line = new line
            {\pard   \par} = paragraph where space is only content - works more consistently as intended as new line
            {\pard ... \par} =  Group\paragraph ... end paragraph\group
            \f0\fs17\b = use default font 0 font size 17 half points bold
            \keeppn and \keep = don't break up paragraph and title
            \'2d = hyphen and \'26 = ampersand
            \brdrt \brdrs \brdrw10 \brsp20 = top border specification for paragraph
            \brdrb \brdrs \brdrw10 \brsp20 = bottom border specification for paragraph
            } = end of document
            *******************************/
            objCon = new SqlConnection(strConnection);
            String strText = "";    //temp text for string manipulation
            try {

                objCon.Open();

                strSQL = "SELECT Replace(SubjectArea.SUBJECT,'&','') AS SUBJECT " +
                    "FROM SubjectArea INNER JOIN Course " +
                    "ON Course.SUBJECT = SubjectArea.SUBJECT " +
                    "WHERE CourseOffering + ' ' + CourseBeginSTRM IN " +
                    "(SELECT CourseOffering + ' ' + CourseBeginSTRM " +
                    "FROM Course " +
                    "WHERE CourseBeginSTRM <= '" + courseBeginSTRM +
                    "' AND (CourseEndSTRM = 'Z999' " +
                    "OR CourseEndSTRM >= '" + courseBeginSTRM +
                    "')) AND PublishedCourse = '1' " +
                    "AND DescriptionCourseID IS NULL " +
                    "GROUP BY Replace(SubjectArea.SUBJECT,'&',''), SubjectArea.DESCR " +
                    "ORDER BY SubjectArea.DESCR;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                DataSet objDept = new DataSet();
                objDA.Fill(objDept);

                for (Int32 intDSRow = 0; intDSRow < objDept.Tables[0].Rows.Count; intDSRow++) {
                    String courseSubject = objDept.Tables[0].Rows[intDSRow]["SUBJECT"].ToString();
                    Int32 intLength = courseSubject.Length;

                    strSQL = "SELECT A.CourseBeginSTRM, A.CourseID, A.COURSE_TITLE_LONG, A.DESCRLONG, " +
                        "A.UNITS_MINIMUM, A.UNITS_MAXIMUM, A.VariableUnits, A.CourseOffering, " +
                        "A.SUBJECT, A.CATALOG_NBR as CATALOG_NBR_A, B.CATALOG_NBR as CATALOG_NBR_B, " +
                        "B.DescriptionCourseID, SubjectArea.DESCR " +
                        "FROM Course A " +
                        "LEFT OUTER JOIN Course B " +
                        "ON A.CourseID = B.DescriptionCourseID " +
                        "INNER JOIN SubjectArea " +
                        "ON A.SUBJECT = SubjectArea.SUBJECT " +
                        "WHERE A.CourseOffering + ' ' + A.CourseBeginSTRM IN " +
                        "(SELECT CourseOffering + ' ' + CourseBeginSTRM " +
                        "FROM Course " +
                        "WHERE CourseBeginSTRM <= '" + courseBeginSTRM +
                        "' AND (CourseEndSTRM = 'Z999' " +
                        "OR CourseEndSTRM >= '" + courseBeginSTRM + "')) " +
                        "AND A.PublishedCourse = '1' " +
                        "AND A.DescriptionCourseID IS NULL " +
                        " AND (A.SUBJECT = '" + courseSubject + "' OR A.SUBJECT = '" + courseSubject + "&') " +
                        "ORDER BY SUBSTRING(SubjectArea.SUBJECT,1," + intLength + "), SUBSTRING(A.SUBJECT,1," + intLength + "), A.CATALOG_NBR, B.CATALOG_NBR;";

                    objCmd = new SqlCommand(strSQL, objCon);
                    objDA = new SqlDataAdapter(objCmd);

                    if (intDSRow == 0) {
                        objDS = new DataSet();
                        objDA.Fill(objDS);
                    } else {
                        DataSet objTemp = new DataSet();
                        objDA.Fill(objTemp);
                        objDS.Merge(objTemp);
                    }
                }

                bool blnClearString = true, blnMoveNext = true, blnWriteFile;
                String courseLongTitle = "", courseLongDescription = "", catalogNbrs = "", variableUnits = "", strOfferedAt = "", strCrsCredits = "", subjectDescription = "";
                Double unitsMinimum = 0, unitsMaximum = 0;
                Int32 courseID = 0;

                //PageMaker
                //StreamWriter objSW = new StreamWriter(HttpContext.Current.Server.MapPath("../PrintableCourseDescriptions.doc"), false, System.Text.Encoding.GetEncoding("utf-8"));
                //RTF
                StreamWriter objSW = new StreamWriter(HttpContext.Current.Server.MapPath("../PrintableCourseDescriptions.rtf"));
                //Start the RTF document - declare the defaults and preliminaries
                objSW.WriteLine("{\\rtf1\\ansi\\deff0 {\\fonttbl {\\f0 Arial}}");
                objSW.WriteLine("\\deflang1033 \\plain \\widowctrl \\fs17");
                //Set the columns with default spacing of 1/2 inch
                objSW.WriteLine("\\cols2");

                for (Int32 intDSRow = 0; intDSRow < objDS.Tables[0].Rows.Count; intDSRow++) {
                    Int32 dsCourseID = Convert.ToInt32(objDS.Tables[0].Rows[intDSRow]["CourseID"]);

                    //get course colleges
                    DataSet dsCourseCollege = GetCourseCollege(dsCourseID, objCon);
                    strOfferedAt = "";
                    for (Int32 i = 0; i < dsCourseCollege.Tables[0].Rows.Count; i++) {
                        strOfferedAt += ", " + dsCourseCollege.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
                    }
                    //if (strOfferedAt != "") { //added by brandy - there must be courses with the college missing
                        strOfferedAt = "(" + strOfferedAt.Substring(2) + ")";
                    //} else {
                    //    strOfferedAt = "(NULL)";
                    //}

                    //If a title and description have not been stored or have been cleared store the current record values 
                    if (courseLongTitle.Length == 0 && courseLongDescription.Length == 0) {
                        courseLongTitle = objDS.Tables[0].Rows[intDSRow]["COURSE_TITLE_LONG"].ToString();
                        courseLongDescription = objDS.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString();
                    }

                    //----------------- determine where the course is offered used to go here -------------------

                    //if the course is not a duplicate record, the DescriptionCourseID is not null, and the course numbers aren't the same 
                    //check to see if a string of course numbers exists 

                    if (courseID != dsCourseID && objDS.Tables[0].Rows[intDSRow]["DescriptionCourseID"].ToString() != "" && objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() != objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString()) {

                        //if a string of course numbers does not exist
                        //start a new string and store all other values needed
                        //WriteFile is set to false until the record stops repeating
                        if (catalogNbrs == "") {
                            catalogNbrs = objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() + ", " + objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString();
                            courseLongDescription = objDS.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString();
                            courseLongTitle = objDS.Tables[0].Rows[intDSRow]["COURSE_TITLE_LONG"].ToString();
                            variableUnits = objDS.Tables[0].Rows[intDSRow]["VariableUnits"].ToString();
                            unitsMinimum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MINIMUM"]);
                            unitsMaximum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MAXIMUM"]);
                            blnWriteFile = false;

                            //if a string of course numbers does exist a new course has started
                            //assign True to WriteFile to write the courses using the previous course as the source
                            //assign False to ClearString which is later used to concatinate course numbers using the current course as the source
                        } else {
                            blnWriteFile = true;
                            blnClearString = false;
                            blnMoveNext = true;
                        }

                        //if the course is a duplicate record, the DescriptionCourseID is not null, and the course numbers aren't the same
                        //concatinate the course number to the existing course number string
                        //WriteFile is set to false until the record stops repeating
                    } else if (courseID == dsCourseID && objDS.Tables[0].Rows[intDSRow]["DescriptionCourseID"].ToString() != "" && objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() != objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString()) {
                        if (catalogNbrs.Substring((catalogNbrs.Length - 3)) != objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString()) {
                            catalogNbrs += ", " + objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString();
                        }
                        blnWriteFile = false;
                        blnMoveNext = true;

                        //The course is not a duplicate record assign True to WriteFile
                        //Assign true to ClearString so the new course values can be stored in COURSE_TITLE_LONG, courseLongDescription, and catalogNbrs
                    } else {
                        blnWriteFile = true;
                        blnClearString = true;

                        //If a string of course numbers exist assign false to MoveNext so the recordset stays on the current record
                        //which will be used in the next loop to store the new course values
                        if (catalogNbrs.Length > 3) {
                            blnMoveNext = false;

                            //If a string of course numbers does not exist store the current course values used for the credits and the course number
                        } else {
                            catalogNbrs = objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString();
                            variableUnits = objDS.Tables[0].Rows[intDSRow]["VariableUnits"].ToString();
                            unitsMinimum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MINIMUM"]);
                            unitsMaximum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MAXIMUM"]);
                            blnMoveNext = true;
                        }
                    }

                    //If a string of course numbers does exist create the string of credits to write to the file 
                    //for multiple courses using the same source  
                    if (catalogNbrs.Length > 3) {
                        if (variableUnits == "Y") {
                            strCrsCredits = " (" + unitsMinimum + "-" + unitsMaximum + " cr ea)";
                        } else {
                            strCrsCredits = " (" + unitsMaximum + " cr ea)";
                        }

                        //If a string of course numbers does not exist create the string of credits to write to the file 
                        //for a single course
                    } else {
                        if (variableUnits == "Y") {
                            strCrsCredits = " (" + unitsMinimum + "-" + unitsMaximum + " cr)";
                        } else {
                            strCrsCredits = " (" + unitsMaximum + " cr)";
                        }
                    }

                    //If a new course has been started in the loop assign the PK to the CrsASN variable used
                    //in the conditions above to create the string of course numbers
                    if (courseID != dsCourseID) {
                        courseID = dsCourseID;
                    }

                    //Write the file 
                    if (blnWriteFile) {
                        //If a new subject is started in the loop write the subject description to the file 
                        if (subjectDescription != objDS.Tables[0].Rows[intDSRow]["DESCR"].ToString()) {
                            subjectDescription = objDS.Tables[0].Rows[intDSRow]["DESCR"].ToString();
                            //PageMaker
                            //objSW.WriteLine("<@HEAD-rev-crs:>" + subjectDescription);
                            //RTF - top and bottom paragraph border
                            objSW.WriteLine("{\\pard ");
                            objSW.WriteLine("\\brdrt \\brdrs \\brdrw10 \\brsp20");
                            objSW.WriteLine("\\brdrb \\brdrs \\brdrw10 \\brsp20");
                            objSW.WriteLine("\\b " + subjectDescription.ToUpper() + " \\par}");
                            objSW.WriteLine("{\\pard   \\par}");
                        }
                        //PageMaker
                        //objSW.WriteLine("<@CRS:>" + objDS.Tables[0].Rows[intDSRow]["SUBJECT"].ToString() + " " + catalogNbrs + "<k-0.2> <\\�><k-0.2> " + courseLongTitle + strCrsCredits);
                        //RTF
                        strText = objDS.Tables[0].Rows[intDSRow]["SUBJECT"].ToString() + " " + catalogNbrs;
                        //Encode ampersands
                        strText = strText.Replace("&", "\\'26");
                        objSW.WriteLine("{\\pard \\f0\\fs17\\keepn \\b " + strText + " \\'2d " + courseLongTitle + strCrsCredits + " \\par}");
                        //PageMaker
                        //objSW.WriteLine("<@DESC:>" + courseLongDescription + " " + strOfferedAt);
                        //RTF
                        objSW.WriteLine("{\\pard \\f0\\fs17\\keep " + courseLongDescription + " " + strOfferedAt + "\\par}");
                        objSW.WriteLine("{\\pard   \\par}");

                        //Clear the current course values so new values can be stored in the next loop
                        if (blnClearString) {
                            courseLongTitle = "";
                            courseLongDescription = "";
                            catalogNbrs = "";
                            //Store the current course values to be written in the next loop
                        } else {
                            catalogNbrs = objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_A"].ToString() + ", " + objDS.Tables[0].Rows[intDSRow]["CATALOG_NBR_B"].ToString();
                            courseLongDescription = objDS.Tables[0].Rows[intDSRow]["DESCRLONG"].ToString();
                            courseLongTitle = objDS.Tables[0].Rows[intDSRow]["COURSE_TITLE_LONG"].ToString();
                            variableUnits = objDS.Tables[0].Rows[intDSRow]["VariableUnits"].ToString();
                            unitsMinimum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MINIMUM"]);
                            unitsMaximum = Convert.ToDouble(objDS.Tables[0].Rows[intDSRow]["UNITS_MAXIMUM"]);
                        }
                    }
                    if (!blnMoveNext) {
                        intDSRow = intDSRow - 1;
                    }
                }
                //RTF - close the document
                objSW.WriteLine("}");

                objSW.Close();
                //PageMaker
                //return "../PrintableCourseDescriptions.doc";
                //RTF
                return "../PrintableCourseDescriptions.rtf";

            } finally {
                objCon.Close();
            }
        }


		/****************************************** GetCourseDescriptions *******************************************
		 * INPUT:
         * selectedSTRM - Selected Term Code
		 * courseSubject - Course Department
		 * strCollegeString - comma delimited string of CollegeID values
		 * strOrderBy - Field to Order Results By
		 * publishedCourse - Course Status
		 * 
		 * OUTPUT: DataSet
		 * FIELD LIST: A.CourseBeginSTRM - Course Effective Year Quarter
		 *             A.CourseID - Course Primary Key
		 *             A.COURSE_TITLE_LONG - Course Long Title
		 *             A.DESCRLONG - Course Description
		 *             A.UNITS_MINIMUM - Course Minimum Credits
		 *             A.UNITS_MAXIMUM - Course Maximum Credits
		 *             A.VariableUnits - Variable Credit Indicator
		 *             A.CourseOffering - Course ID
		 *             A.SUBJECT - Course Department
		 *             CATALOG_NBR_A - Course Number
		 *             CATALOG_NBR_B - Course Number of Course Being Used as the Source for the Description
		 *             B.DescriptionCourseID - Primary Key of Course Being Used as the Source for the Description
		 *             DESCR - Course Department Title
		 * 
		 * USED IN: /iCatalog/course/proof.aspx.cs
		 * 
		 * 
		 * DESCRIPTION: Gets the most current year quarter Course information for display
		 * 
		 *******************************************************************************************************/
		public DataSet GetCourseDescriptions(String selectedSTRM, String courseSubject, String strCollegeString, String strOrderBy, String publishedCourse){
			
            objCon = new SqlConnection(strConnection);
			
			try{

				objCon.Open();
				String strQuery = "";

                if (strCollegeString != ""){
                    String[] strCollege = strCollegeString.Split(',');
                    for (Int32 i = 0; i < strCollege.Length; i++)
                    {
                        strQuery += " OR CollegeID = " + strCollege[i];
                    }
                    strQuery = " AND (" + strQuery.Substring(3) + ") ";
                }

				if(courseSubject != "ALL"){
					Int32 intLength = courseSubject.Length;

                    strSQL = "SELECT A.CourseBeginSTRM, A.CourseID, A.COURSE_TITLE_LONG, A.DESCRLONG, " +
                             "A.UNITS_MINIMUM, A.UNITS_MAXIMUM, A.VariableUnits, A.CourseOffering, A.SUBJECT, " +
                             "A.CATALOG_NBR as CATALOG_NBR_A, B.CATALOG_NBR as CATALOG_NBR_B, " +
                             "B.DescriptionCourseID, SubjectArea.DESCR " +
                             "FROM Course A " +
                             "LEFT OUTER JOIN Course B " +
                             "ON A.CourseID = B.DescriptionCourseID " +
                             "INNER JOIN SubjectArea " +
                             "ON A.SUBJECT = SubjectArea.SUBJECT " +
                             "INNER JOIN CollegeCourse " +
                             "ON CollegeCourse.CourseID = A.CourseID " +
                             "WHERE A.CourseOffering + ' ' + A.CourseBeginSTRM IN " +
                             "(SELECT CourseOffering + ' ' + CourseBeginSTRM " +
                             "FROM Course " +
                             "WHERE CourseBeginSTRM <= '" + selectedSTRM +
                             "' AND (CourseEndSTRM = 'Z999' " +
                             "OR CourseEndSTRM >= '" + selectedSTRM +
                             "')) AND A.PublishedCourse = '" + publishedCourse +
                             "' AND A.DescriptionCourseID IS NULL " +
                             strQuery +
                             "AND (A.SUBJECT = '" + courseSubject + "' OR A.SUBJECT = '" + courseSubject + "&') " +
                             "GROUP BY A.CourseBeginSTRM, A.CourseID, A.COURSE_TITLE_LONG, A.DESCRLONG, A.UNITS_MINIMUM, A.UNITS_MAXIMUM, A.VariableUnits, A.CourseOffering, A.SUBJECT, A.CATALOG_NBR, B.CATALOG_NBR , B.DescriptionCourseID, SubjectArea.DESCR, SubjectArea.SUBJECT " +
                             "ORDER BY SUBSTRING(SubjectArea.SUBJECT,1," + intLength + "), SUBSTRING(A.SUBJECT,1," + intLength + "), A.CATALOG_NBR, B.CATALOG_NBR;";

					objCmd = new SqlCommand(strSQL, objCon);
					objDA = new SqlDataAdapter(objCmd);
					objDS = new DataSet();
					objDA.Fill(objDS);

					return objDS;

				}else{

                    strSQL = "SELECT Replace(SubjectArea.SUBJECT,'&','') AS SUBJECT " +
                             "FROM SubjectArea INNER JOIN Course " +
                             "ON Course.SUBJECT = SubjectArea.SUBJECT " +
                             "INNER JOIN CollegeCourse " +
                             "ON CollegeCourse.CourseID = Course.CourseID " +
                             "WHERE CourseOffering + ' ' + CourseBeginSTRM IN " +
                             "(SELECT CourseOffering + ' ' + CourseBeginSTRM " +
                             "FROM Course " +
                             "WHERE CourseBeginSTRM <= '" + selectedSTRM + 
                             "' AND (CourseEndSTRM = 'Z999' " +
                             "OR CourseEndSTRM >= '" + selectedSTRM + 
                             "')) AND PublishedCourse = '" + publishedCourse + 
                             "' AND DescriptionCourseID IS NULL " +
                             strQuery +
                             "GROUP BY Replace(SubjectArea.SUBJECT,'&',''), SubjectArea.DESCR " +
                             "ORDER BY SUBJECT;";

					objCmd = new SqlCommand(strSQL, objCon);
					objDA = new SqlDataAdapter(objCmd);
					DataSet objDept = new DataSet();
					objDA.Fill(objDept);

					if(objDept.Tables[0].Rows.Count > 0){

						for(Int32 intDSRow = 0; intDSRow < objDept.Tables[0].Rows.Count; intDSRow++) {
							courseSubject = objDept.Tables[0].Rows[intDSRow]["SUBJECT"].ToString();
							Int32 intLength = courseSubject.Length;
							
                            strSQL = "SELECT A.CourseBeginSTRM, A.CourseID, A.COURSE_TITLE_LONG, A.DESCRLONG, " +
                                     "A.UNITS_MINIMUM, A.UNITS_MAXIMUM, A.VariableUnits, A.CourseOffering, A.SUBJECT, " +
                                     "A.CATALOG_NBR as CATALOG_NBR_A, B.CATALOG_NBR as CATALOG_NBR_B, " +
                                     "B.DescriptionCourseID, SubjectArea.DESCR " +
                                     "FROM Course A " +
                                     "LEFT OUTER JOIN Course B " +
                                     "ON A.CourseID = B.DescriptionCourseID " +
                                     "INNER JOIN SubjectArea " +
                                     "ON A.SUBJECT = SubjectArea.SUBJECT " +
                                     "INNER JOIN CollegeCourse " +
                                     "ON CollegeCourse.CourseID = A.CourseID " +
                                     "WHERE A.CourseOffering + ' ' + A.CourseBeginSTRM IN " +
                                     "(SELECT CourseOffering + ' ' + CourseBeginSTRM " +
                                     "FROM Course " +
                                     "WHERE CourseBeginSTRM <= '" + selectedSTRM +
                                     "' AND (CourseEndSTRM = 'Z999' " +
                                     "OR CourseEndSTRM >= '" + selectedSTRM +
                                     "')) AND A.PublishedCourse = '" + publishedCourse +
                                     "' AND A.DescriptionCourseID IS NULL " +
                                     strQuery + 
                                     "AND (A.SUBJECT = '" + courseSubject + "' OR A.SUBJECT = '" + courseSubject + "&') " +
                                     "GROUP BY A.CourseBeginSTRM, A.CourseID, A.COURSE_TITLE_LONG, A.DESCRLONG, A.UNITS_MINIMUM, A.UNITS_MAXIMUM, A.VariableUnits, A.CourseOffering, A.SUBJECT, A.CATALOG_NBR, B.CATALOG_NBR , B.DescriptionCourseID, SubjectArea.DESCR, SubjectArea.SUBJECT " +
                                     "ORDER BY SUBSTRING(SubjectArea.SUBJECT,1," + intLength + "), SUBSTRING(A.SUBJECT,1," + intLength + "), A.CATALOG_NBR, B.CATALOG_NBR;";

							objCmd = new SqlCommand(strSQL, objCon);
							objDA = new SqlDataAdapter(objCmd);

							if(intDSRow == 0){
								objDS = new DataSet();
								objDA.Fill(objDS);
							}else{
								DataSet objTemp = new DataSet();
								objDA.Fill(objTemp);
								objDS.Merge(objTemp);
							}
						}
						return objDS;
					}else{
						return objDept;
					}
				}

			}finally{
				objCon.Close();
			}
		}


		public DataSet GetCourseOfferings(String courseSubject)
        {
            objCon = new SqlConnection(strConnection);

            try
            {

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCourseOfferings", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.AddWithValue("@SUBJECT", courseSubject);

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

            }
            finally
            {
                objCon.Close();
            }
            /*
			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "SELECT CourseOffering, CATALOG_NBR, CourseSuffix " +
					"FROM Course " +
					"WHERE SUBJECT = '" + courseSubject +
					"' GROUP BY CourseOffering, CATALOG_NBR, CourseSuffix " +
					"ORDER BY CATALOG_NBR;";

				objCmd = new SqlCommand(strSQL, objCon);
				objDA = new SqlDataAdapter(objCmd);
				objDS = new DataSet();
				objDA.Fill(objDS);

				return objDS;

			}finally{
				objCon.Close();
			}
            */
        }


        /// <summary>
        /// Returns the begin and end term for a course
        /// </summary>
        /// <param name="courseOffering">AKA Course Offering - Subject Area padded to five spaces + course suffix + Catalog Nbr (Example: ENG  101)</param>
        /// <returns>
        /// DataSet
        /// Field List: CourseID - Course Primary Key
        ///             CourseBeginSTRM - Course Begin STRM
        ///             BeginTerm_DESCR - Course Begin Term Description
        ///		 	    CourseEndSTRM - Course End STRM
        ///		 	    EndTerm_DESCR - Course End Term Description
        /// </returns>
		public DataSet GetCourseOfferingTerms(String courseOffering){

			objCon = new SqlConnection(strConnection);

			try{

                objCon.Open();
                objCmd = new SqlCommand("usp_GetCourseOfferingTerms", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.AddWithValue("@CourseOffering", courseOffering);

                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

			}finally{
				objCon.Close();
			}
		}


		public bool EditCourseTerms(Int32 courseID, String courseBeginSTRM, String courseEndSTRM){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();

				strSQL = "UPDATE Course " +
					"SET CourseBeginSTRM = '" + courseBeginSTRM + "', CourseEndSTRM = '" + courseEndSTRM + "' " +
					"WHERE CourseID = " + courseID + ";";
				
				objCmd = new SqlCommand(strSQL, objCon);
				if(objCmd.ExecuteNonQuery() > 0){
					return true;
				}else{
					return false;
				}

			}finally{
				objCon.Close();
			}
		}


        /// <summary>
        /// Returns a list of courses containing the text searched in the long course description
        /// </summary>
        /// <param name="searchText">The Search Text</param>
        /// <returns>
        /// DataSet
        /// FIELD LIST: CourseID - Course Primary Key
        ///             CourseOffering - Course Id (Subject Area padded to 5 spaces and Catalog Nbr)
        ///             DESCR - Course Short Title
        ///             CourseBeginSTRM - Course Begin STRM
        ///             BeginTerm_DESCR - Course Begin Term Description (Example: Spring 2017)
        ///             CourseEndSTRM - Course End STRM
        ///             EndTerm_DESCR - Course End Term Description (Example: Spring 2018)
        ///             PublishedCourse - 0 = Working Copy / not displayed to the public, 1 = Published Copy / displayed to the public
        ///             PublishedCourseID - The primary key for the course with a shared long course description 
        /// </returns>
		public DataSet GetCoursesTextSearch(String searchText){

			objCon = new SqlConnection(strConnection);

			try{

				objCon.Open();
                objCmd = new SqlCommand("usp_GetCoursesTextSearch", objCon);
                objDS = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("@SearchText", SqlDbType.VarChar);
                objCmd.Parameters["@SearchText"].Value = searchText;
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                return objDS;

			}finally{
				objCon.Close();
			}
		}


        public DataSet GetSMSCourseDetail(String courseOffering, String selectedSTRM){

            objCon = new SqlConnection(strConnection);

            try{

                String strQuery1 = "", strQuery2 = "";

                objCon.Open();

                strSQL = "SELECT CollegeCourse.CollegeID, Institution " +
                         "FROM CollegeCourse " +
                         "INNER JOIN Course " +
                         "ON Course.CourseID = CollegeCourse.CourseID " +
                         "INNER JOIN College " +
                         "ON College.CollegeID = CollegeCourse.CollegeID " +
                         "WHERE CourseOffering = '" + courseOffering + 
                         "' AND (CourseBeginSTRM <= '" + selectedSTRM + 
                         "' AND (CourseEndSTRM = 'Z999' OR CourseEndSTRM >= '" + selectedSTRM + "'));";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                for (Int32 i = 0; i < objDS.Tables[0].Rows.Count; i++){
                    strQuery1 += " OR CollegeID = " + objDS.Tables[0].Rows[i]["CollegeID"].ToString();
                    strQuery2 += " OR ColCode = '" + objDS.Tables[0].Rows[i]["Institution"].ToString() + "'";
                }

                if (strQuery1 != ""){
                    strQuery1 = " AND (" + strQuery1.Substring(4) + ")";
                    strQuery2 = " AND (" + strQuery2.Substring(4) + ")";
                }                         

                strSQL = "SELECT ContactHoursLecture, ContactHoursLab, ContactHoursClinical, ContactHoursOther, CoursePayTypeID, InstitutionalIntentID, Misc3 " +
                         ", (SELECT DESCRLONG FROM Course " +
                         "INNER JOIN CollegeCourse " +
                         "ON CollegeCourse.CourseID = Course.CourseID " +
                         "WHERE CourseOffering = '" + courseOffering +
                         "' " + strQuery1 + 
                         " AND (CourseBeginSTRM <= '" + selectedSTRM + "' AND (CourseEndSTRM = 'Z999' OR CourseEndSTRM >= '" + selectedSTRM + "')) " +
                         "GROUP BY DESCRLONG) AS DESCRLONG " +
                         "FROM CCSGen.dbo.vwODSSM_Course " +
                         "WHERE CourseId = '" + courseOffering + 
                         "' " + strQuery2 + 
                         " AND ((EffectiveYearQuarterBegin IS NULL OR EffectiveYearQuarterBegin <= '" + selectedSTRM + "') " +
                         " AND (EffectiveYearQuarterEnd = 'Z999' OR EffectiveYearQuarterEnd >= '" + selectedSTRM + "')) " +
                         " GROUP BY ContactHoursLecture, ContactHoursLab, ContactHoursClinical, ContactHoursOther, CoursePayTypeID, InstitutionalIntentID, Misc3;";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            }finally{
                objCon.Close();
            }
        }


        public bool AddCollegeCourse(Int32 collegeID, Int32 courseID){

            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                strSQL = "INSERT INTO CollegeCourse(CollegeID, CourseID) " +
                         "VALUES(" + collegeID + "," + courseID + ");";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0){
                    return true;
                }else{
                    return false;
                }

            }finally{
                objCon.Close();
            }
        }


        public bool DeleteCourseColleges(Int32 courseID){

            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                strSQL = "DELETE CollegeCourse " +
                         "WHERE CourseID = " + courseID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                if (objCmd.ExecuteNonQuery() > 0){
                    return true;
                }else{
                    return false;
                }

            }finally{
                objCon.Close();
            }
        }


        public bool DeleteCourseColleges(Int32 courseID, SqlConnection objCon){

            strSQL = "DELETE CollegeCourse " +
                     "WHERE CourseID = " + courseID + ";";

            objCmd = new SqlCommand(strSQL, objCon);
            if (objCmd.ExecuteNonQuery() > 0){
                return true;
            }else{
                return false;
            }
        }


        public DataSet GetCourseCollege(Int32 courseID){

            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                strSQL = "SELECT CollegeCourse.CollegeID, CollegeShortTitle " +
                         "FROM CollegeCourse " +
                         "INNER JOIN College " +
                         "ON College.CollegeID = CollegeCourse.CollegeID " +
                         "WHERE CourseID = " + courseID + ";";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            }finally{
                objCon.Close();
            }
        }


        public DataSet GetCourseCollege(Int32 courseID, SqlConnection objCon){

            strSQL = "SELECT CollegeCourse.CollegeID, CollegeShortTitle " +
                     "FROM CollegeCourse " +
                     "INNER JOIN College " +
                     "ON College.CollegeID = CollegeCourse.CollegeID " +
                     "WHERE CourseID = " + courseID + ";";

            objCmd = new SqlCommand(strSQL, objCon);
            objDA = new SqlDataAdapter(objCmd);
            DataSet dsCourseCollege = new DataSet();
            objDA.Fill(dsCourseCollege);

            return dsCourseCollege;
        }


        public DataSet GetCourseCollege(String courseOffering, String courseBeginSTRM){

            objCon = new SqlConnection(strConnection);

            try{

                objCon.Open();

                strSQL = "SELECT CollegeShortTitle, CollegeCourse.CollegeID " +
                         "FROM CollegeCourse " +
                         "INNER JOIN College " +
                         "ON College.CollegeID = CollegeCourse.CollegeID " +
                         "INNER JOIN Course " +
                         "ON Course.CourseID = CollegeCourse.CourseID " +
                         "WHERE CourseOffering = '" + courseOffering +
                         "' AND (CourseBeginSTRM <= '" + courseBeginSTRM +
                         "' AND (CourseEndSTRM = 'Z999' OR CourseEndSTRM >= '" + courseBeginSTRM + "'));";

                objCmd = new SqlCommand(strSQL, objCon);
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);

                return objDS;

            }finally{
                objCon.Close();
            }
        }

        public string Strip(string text){
            return Regex.Replace(text, @"<(.|\n)*?>", string.Empty);
        }
	}
}
