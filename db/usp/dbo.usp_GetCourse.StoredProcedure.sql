USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCourse]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/13/2019
-- Description:	Returns Course Details
--		 INPUT:	CRSE_ID - 
--              EFFDT -  Effective date 		
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCourse]
	@CRSE_ID varchar(6),
	@EFFDT date,
	@INSTITUTION varchar(5)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT INSTITUTION, CourseOffering.CRSE_ID, (SUBJECT + CATALOG_NBR) AS CourseOffering, SUBJECT, CATALOG_NBR, COURSE_TITLE_LONG, CourseOffering.EFFDT , EFF_STATUS, CATALOG_PRINT, SCHEDULE_PRINT, DESCR, UNITS_MINIMUM, UNITS_MAXIMUM,DESCRLONG
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	WHERE CourseOffering.CRSE_ID = @CRSE_ID
		AND ACAD_CAREER = 'UGRD'
		--AND CATALOG_PRINT = 'Y'
		AND EFF_STATUS = 'A'
		AND INSTITUTION = @INSTITUTION
		AND DATEDIFF(day, CourseOffering.EFFDT, @EFFDT) = 0
END
GO
