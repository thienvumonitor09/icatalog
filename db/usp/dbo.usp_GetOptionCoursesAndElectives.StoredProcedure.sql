USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionCoursesAndElectives]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/22/2019
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionCoursesAndElectives]
	@BeginSTRM varchar(4) ,
	@EndSTRM varchar(4) ,
	@OptionID int ,
	@CurrentSTRM varchar(4) 
AS
BEGIN
	SET NOCOUNT ON;

    IF @EndSTRM < @CurrentSTRM /* ARCHIVED PROGRAM */
		/*display the last published version of the course in the program strm span */
		SELECT Course.CourseOffering
			, Course.CourseID, SUBJECT
			, CATALOG_NBR, CourseSuffix
			, CourseBeginSTRM
			, CourseEndSTRM
			, COURSE_TITLE_LONG
			, UNITS_MINIMUM
			, UNITS_MAXIMUM
			, Quarter
			, FootnoteNumber
			, OptionCourse.OptionFootnoteID
			, OverwriteUnits
			, OverwriteUnitsMinimum
			, OverwriteUnitsMaximum 
        FROM OptionCourse INNER JOIN Course 
        ON Course.CourseOffering = OptionCourse.CourseOffering
        AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999')) 
        OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM)) 
        LEFT OUTER JOIN OptionFootnote 
        ON OptionFootnote.OptionFootnoteID = OptionCourse.OptionFootnoteID 
        WHERE OptionCourse.OptionID = @OptionID
        GROUP BY Course.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum 
        ORDER BY Quarter, SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR, CourseEndSTRM DESC;
ELSE 
		BEGIN
			IF @BeginSTRM > @CurrentSTRM /* FUTURE PROGRAM */
				/*display the first published version of the course in the program strm span*/
				SELECT Course.CourseOffering
				, Course.CourseID, SUBJECT
				, CATALOG_NBR, CourseSuffix
				, CourseBeginSTRM, CourseEndSTRM
				, COURSE_TITLE_LONG
				, UNITS_MINIMUM
				, UNITS_MAXIMUM
				, Quarter
				, FootnoteNumber
				, OptionCourse.OptionFootnoteID
				, OverwriteUnits
				, OverwriteUnitsMinimum
				, OverwriteUnitsMaximum
				, COUNT(*) 
				FROM OptionCourse
                INNER JOIN Course
                ON Course.CourseOffering = OptionCourse.CourseOffering 
                             AND (@BeginSTRM BETWEEN CourseBeginSTRM AND CourseEndSTRM) 
                             LEFT OUTER JOIN OptionFootnote 
                             ON OptionFootnote.OptionFootnoteID = OptionCourse.OptionFootnoteID
				WHERE OptionCourse.OptionID = @OptionID
				GROUP BY Course.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
				ORDER BY Quarter, SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR, CourseEndSTRM ASC
			ELSE /* CURRENT PROGRAM */
				/*display the most current published version of the course in the program strm span*/
				SELECT Course.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum 
				FROM OptionCourse INNER JOIN Course
				ON Course.CourseOffering = OptionCourse.CourseOffering 
				AND ((CourseBeginSTRM <= @BeginSTRM AND (CourseEndSTRM >= @BeginSTRM OR CourseEndSTRM = 'Z999')) 
				OR (CourseBeginSTRM >= @BeginSTRM AND CourseBeginSTRM <= @EndSTRM AND (CourseEndSTRM <= @EndSTRM OR CourseEndSTRM = 'Z999'))) 
				AND (CourseBeginSTRM <= @CurrentSTRM AND (CourseEndSTRM >= @CurrentSTRM OR CourseEndSTRM = 'Z999')) 
				LEFT OUTER JOIN OptionFootnote 
				ON OptionFootnote.OptionFootnoteID = OptionCourse.OptionFootnoteID 
				WHERE OptionCourse.OptionID = @OptionID
				GROUP BY  Course.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseBeginSTRM, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum 
				ORDER BY Quarter, SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR, CourseEndSTRM ASC;
		END			
END
GO
