﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ICatalog._phatt3_classes;
using System.Data;

public partial class program_popups_coursedetail : System.Web.UI.Page{

    protected void Page_Load(object sender, EventArgs e){

        courseData csCourseData = new courseData();

        String strCourseID = Request.QueryString["id"];
        String courseShortTitle = Request.QueryString["TITLE"];
        String courseBeginSTRM = Request.QueryString["bstrm"];
        String courseEndSTRM = Request.QueryString["estrm"];

        DataSet dsCourseCollege = csCourseData.GetCourseCollege(strCourseID, courseBeginSTRM);
        for (Int32 i = 0; i < dsCourseCollege.Tables[0].Rows.Count; i++)
        {
            lblOfferedAt.Text += ", " + dsCourseCollege.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
        }
        if (lblOfferedAt.Text != "")
        {
            lblOfferedAt.Text = lblOfferedAt.Text.Substring(2);
        }

        DataSet dsCourseDetail = csCourseData.GetSMSCourseDetail(strCourseID, courseBeginSTRM);
        if (dsCourseDetail.Tables[0].Rows.Count > 0)
        {
            lblCourseOffering.Text = strCourseID;
            lblCourseShortTitle.Text = courseShortTitle;
            lblTerm.Text = courseBeginSTRM + " - " + courseEndSTRM;
            lblContactHrLec.Text = dsCourseDetail.Tables[0].Rows[0]["ContactHoursLecture"].ToString();
            lblContactHrLab.Text = dsCourseDetail.Tables[0].Rows[0]["ContactHoursLab"].ToString();
            lblContactHrClin.Text = dsCourseDetail.Tables[0].Rows[0]["ContactHoursClinical"].ToString();
            lblContactHrOthr.Text = dsCourseDetail.Tables[0].Rows[0]["ContactHoursOther"].ToString();
            lblCoursPayType.Text = dsCourseDetail.Tables[0].Rows[0]["CoursePayTypeID"].ToString();
            lblInstitInt.Text = dsCourseDetail.Tables[0].Rows[0]["InstitutionalIntentID"].ToString();
            lblMisc3.Text = dsCourseDetail.Tables[0].Rows[0]["Misc3"].ToString();
            lblCourseLongDescription.Text = dsCourseDetail.Tables[0].Rows[0]["DESCRLONG"].ToString();
        }
    }
}