﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for iCatalogXML
/// </summary>
[WebService(Namespace = "http://external.spokane.edu/WebServices/iCatalogLegacyXML/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class iCatalogLegacyXML : System.Web.Services.WebService {

    private SqlConnection objCon, objCon2;
    private String strSQL, iCatalogConnection, ctcLinkConnection;
    private SqlCommand objCmd, objCmd2;
    private SqlDataAdapter objDA;
    private DataSet objDS;

    public iCatalogLegacyXML() {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 

        ctcLinkConnection = "Initial Catalog=CCSGen_ctcLink_ODS;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=DIST17_CCSNET;Integrated Security=true;";

        //production iCatalog connection string
        iCatalogConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=DIST17_CCSNET;Integrated Security=true;";

        //development iCatalog connection string
        //iCatalogConnection = "Initial Catalog=CCSICatalog_Legacy;Data Source=CCS-SQL-DEV;Workstation ID=DIST17_CCSNET;Integrated Security=true;";
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns STRM value for legacy YRQ</p>" +
             "<p><strong>Input:</strong><br />" +
             "String YRQ: Legacy Year/Quarter Code<br />" +
             "<p><strong>Output:</strong> String: STRM (Term code)</p>")]
    public String GetSTRMByYRQ(String YRQ) {
        objCon = new SqlConnection(ctcLinkConnection);

        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_Quarter_MetaData_Base_GetSTRMByYRQ", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@YRQ", SqlDbType.VarChar);
            objCmd.Parameters["@YRQ"].Value = YRQ;
            return objCmd.ExecuteScalar().ToString();
        } catch {
            return "";
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns the current term STRM value (If inbetween quarters/terms, the next term is returned.)</p>" +
          "<p><strong>Input:</strong> None<br /></p>" +
          "<p><strong>Output:</strong> String: STRM (Term code)</p>")]
    public String GetCurrentTerm() {
        objCon2 = new SqlConnection(ctcLinkConnection);
        try {
            objCon2.Open();
            objCmd2 = new SqlCommand("usp_ClassSchedule_GetCurrentTerm", objCon2);
            objCmd2.CommandType = CommandType.StoredProcedure;
            return objCmd2.ExecuteScalar().ToString();
        } catch {
            return "";
        } finally {
            objCon2.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns the registration term STRM value (Ten days after the current term, the next term is returned.)</p>" +
         "<p><strong>Input:</strong> None<br /></p>" +
         "<p><strong>Output:</strong> String - STRM (Term code)</p>")]
    public String GetRegistrationTerm() {
        objCon2 = new SqlConnection(ctcLinkConnection);
        try {
            objCon2.Open();
            objCmd2 = new SqlCommand("usp_ClassSchedule_GetRegistrationTerm", objCon2);
            objCmd2.CommandType = CommandType.StoredProcedure;
            SqlParameter objParam = new SqlParameter();
            objParam.ParameterName = "@RETURNVAL";
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Size = 30;
            objParam.Direction = ParameterDirection.Output;
            objCmd2.Parameters.Add(objParam);
            objCmd2.ExecuteScalar();
            return objCmd2.Parameters["@RETURNVAL"].Value.ToString();
        } catch {
            return "";
        } finally {
            objCon2.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns a term description (Example: Spring 2016)</p>" +
         "<p><strong>Input:</strong><br />" +
         "String STRM - Term code (Example: 2163)</p>" +
         "<p><strong>Output:</strong> String DESCR - Term description (Example: Spring 2016)</p>")]
    public String GetTermDescription(String STRM) {
        objCon = new SqlConnection(ctcLinkConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_ClassSchedule_GetTermDesc", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM == null || STRM == "") {
                STRM = GetCurrentTerm();
            }
            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;
            SqlParameter objParam = new SqlParameter();
            objParam.ParameterName = "@RETURNVAL";
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Size = 30;
            objParam.Direction = ParameterDirection.Output;
            objCmd.Parameters.Add(objParam);
            objCmd.ExecuteScalar();
            return objCmd.Parameters["@RETURNVAL"].Value.ToString();
        } catch {
            return "";
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program details by non-changing program id.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 ProgramID - Non-changing Program ID (ProgID)<br />" +
         "String STRM - Term Code (Enter an empty string to return the current term.)</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: ProgramVersionID, ProgramTitle, CollegeID, CollegeShortTitle, CareerPlanningGuideID, ProgramEnrollment, ProgramWebsiteURL, ProgramDescription, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, ProgramBeginSTRM, BeginTerm_DESCR, ProgramEndSTRM, EndTerm_DESCR</p>")]
    public DataSet GetProgramByProgramID(Int32 ProgramID, String STRM) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetProgramByProgramID", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@ProgramID", SqlDbType.Int);
            objCmd.Parameters["@ProgramID"].Value = ProgramID;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program details by version id (ProgColASN).</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 ProgramVersionID - Program version primary key</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: ProgramVersionID, ProgramTitle, CollegeID, CollegeShortTitle, CareerPlanningGuideID, ProgramEnrollment, ProgramWebsiteURL, ProgramDescription, ProgramCourseOfStudy, ProgramGoals, ProgramCareerOpportunities, ProgramBeginSTRM, BeginTerm_DESCR, ProgramEndSTRM, EndTerm_DESCR</p>")]
    public DataSet GetProgramByVersionID(Int32 VersionID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetProgramByVersionID", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@ProgramVersionID", SqlDbType.Int);
            objCmd.Parameters["@ProgramVersionID"].Value = VersionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns college details.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String CollegeShortTitle - (SCC or SFCC)</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: CollegeLongTitle, CollegeAddress, CollegeCity, CollegeState, CollegeZipCode1, CollegeZipCode2, CollegeWebsiteURL</p>")]
    public DataSet GetCollege(String CollegeShortTitle) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetCollege", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@CollegeShortTitle", SqlDbType.VarChar);
            objCmd.Parameters["@CollegeShortTitle"].Value = CollegeShortTitle;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns all colleges.</p>" +
         "<p><strong>Input:</strong> None</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: CollegeID, CollegeShortTitle, CollegeLongTitle</p>")]
    public DataSet GetColleges() {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetColleges", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns all locations offering programs.</p>" +
         "<p><strong>Input:</strong> None</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: LocationID, LocationTitle</p>")]
    public DataSet GetLocations() {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetLocations", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns all degrees offered in a program</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 ProgramVersionID - Program version primary key</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: DegreeShortTitle, ProgramDegreeID</p>")]
    public DataSet GetProgramDegrees(Int32 ProgramVersionID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetProgramDegrees", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@ProgramVersionID", SqlDbType.Int);
            objCmd.Parameters["@ProgramVersionID"].Value = ProgramVersionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program fees.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 ProgramVersionID - Program version primary key</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: ProgramFeeID, ProgramVersionID, BookMinimum, BookMaximum, SuppliesMinimum, SuppliesMaximum, MiscMinimum, MiscMaximum, Note</p>")]
    public DataSet GetProgramFees(Int32 ProgramVersionID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetProgramFees", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@ProgramVersionID", SqlDbType.Int);
            objCmd.Parameters["@ProgramVersionID"].Value = ProgramVersionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program degree details</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 ProgramDegreeID - Program degree primary key</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: ProgramTitle, DocumentTitle, ProgramVersionID, ProgramDisplay, ProgramTextOutline, CollegeShortTitle, CollegeID, DegreeShortTitle, CareerPlanningGuideID, ProgramBeginSTRM, ProgramEndSTRM </p>")]
    public DataSet GetProgramDegree(Int32 ProgramDegreeID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetProgramDegree", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@ProgramDegreeID", SqlDbType.Int);
            objCmd.Parameters["@ProgramDegreeID"].Value = ProgramDegreeID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns degree requirement worksheet file name by title and term/quarter.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String DocumentTitle - Degree Requirement Worksheet Title<br />" +
         "String STRM - Term Code (Enter an empty string to return the current term.)</p>" +
         "<p><strong>Output:</strong> String<br />" +
         "FileName - Degree Requirements/Worksheet File Name</p>")]
    public String GetDegreeRequirementWorksheet(String DocumentTitle, String STRM) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetDegreeRequirementWorksheet", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@DocumentTitle", SqlDbType.VarChar);
            objCmd.Parameters["@DocumentTitle"].Value = DocumentTitle;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            return objCmd.ExecuteScalar().ToString();
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program degree option prerequisites.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 OptionID - Program degree option primary key (ProgOptASN)<br />" +
         "String BeginSTRM - Program begin term code<br />" +
         "String EndSTRM - Program end term code<br />" +
         "String SelectedSTRM - Term code selected</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: SUBJECT, CATALOG_NBR, CourseSuffix, CourseOffering, COURSE_TITLE_LONG, CourseBeginSTRM, CourseEndSTRM, FootnoteNumber, OptionPrerequisiteID, OptionFootnoteID</p>")]
    public DataSet GetOptionPrerequisites(Int32 OptionID, String BeginSTRM, String EndSTRM, String SelectedSTRM) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetOptionPrerequisites", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objCmd.Parameters.Add("@BeginSTRM", SqlDbType.VarChar);
            objCmd.Parameters["@BeginSTRM"].Value = BeginSTRM;
            objCmd.Parameters.Add("@EndSTRM", SqlDbType.VarChar);
            objCmd.Parameters["@EndSTRM"].Value = EndSTRM;
            objCmd.Parameters.Add("@SelectedSTRM", SqlDbType.VarChar);
            objCmd.Parameters["@SelectedSTRM"].Value = SelectedSTRM;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program degree options.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 ProgramDegreeID - Program degree primary key</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: OptionTitle, OptionDescription, OptionID, GainfulEmploymentID</p>")]
    public DataSet GetOptions(Int32 ProgramDegreeID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetOptions", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@ProgramDegreeID", SqlDbType.Int);
            objCmd.Parameters["@ProgramDegreeID"].Value = ProgramDegreeID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns a program degree option.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 OptionID - Program degree option primary key</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: OptionTitle, OptionDescription, OptionID, GainfulEmploymentID</p>")]
    public DataSet GetOption(Int32 OptionID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetOption", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program degree option courses and electives combined in one dataset.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 OptionID - Program degree option primary key (ProgOptASN)<br />" +
         "String BeginSTRM - Program begin term code<br />" +
         "String EndSTRM - Program end term code<br />" +
         "String SelectedSTRM - Term code selected</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: CourseOffering (ZZZ will be stored as CourseOffering if elective), CourseID (OptionElectiveGroupID will be stored as CourseID if elective), SUBJECT (\"\" if elective), CATALOG_NBR (\"\" if elective), CourseSuffix (\"\" if elective), CourseEndSTRM (\"\" if elective), COURSE_TITLE_LONG (ElectiveGroupTitle will be stored as COURSE_TITLE_LONG if elective), UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionFootnoteID, OverwriteUnits (0 if elective), OverwriteUnitsMinumum (0 if elective), OverwriteUnitsMaximum (0 if elective)</p>")]
    public DataSet GetOptionCoursesAndElectives(Int32 OptionID, String BeginSTRM, String EndSTRM, String SelectedSTRM) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetOptionCourses", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objCmd.Parameters.Add("@BeginSTRM", SqlDbType.VarChar);
            objCmd.Parameters["@BeginSTRM"].Value = BeginSTRM;
            objCmd.Parameters.Add("@EndSTRM", SqlDbType.VarChar);
            objCmd.Parameters["@EndSTRM"].Value = EndSTRM;
            objCmd.Parameters.Add("@SelectedSTRM", SqlDbType.VarChar);
            objCmd.Parameters["@SelectedSTRM"].Value = SelectedSTRM;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            objCmd = new SqlCommand("usp_iCatalog_GetOptionElectives", objCon);
            DataSet objDSElectives = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDSElectives);

            DataRow dr;
            //combine program option courses and electives in a dataset
            for (Int32 intRowCtr = 0; intRowCtr < objDSElectives.Tables[0].Rows.Count; intRowCtr++) {
                dr = objDS.Tables[0].NewRow();
                dr["CourseOffering"] = "ZZZ";
                dr["CourseID"] = objDSElectives.Tables[0].Rows[intRowCtr]["OptionElectiveGroupID"];
                dr["SUBJECT"] = "";
                dr["CATALOG_NBR"] = "";
                dr["CourseSuffix"] = "";
                dr["CourseEndSTRM"] = "";
                dr["COURSE_TITLE_LONG"] = objDSElectives.Tables[0].Rows[intRowCtr]["ElectiveGroupTitle"];
                dr["UNITS_MINIMUM"] = objDSElectives.Tables[0].Rows[intRowCtr]["UnitsMinimum"];
                dr["UNITS_MAXIMUM"] = objDSElectives.Tables[0].Rows[intRowCtr]["UnitsMaximum"];
                dr["Quarter"] = objDSElectives.Tables[0].Rows[intRowCtr]["Quarter"];
                dr["FootnoteNumber"] = objDSElectives.Tables[0].Rows[intRowCtr]["FootnoteNumber"];
                dr["OptionFootnoteID"] = objDSElectives.Tables[0].Rows[intRowCtr]["OptionFootnoteID"];
                dr["OverwriteUnits"] = 0;
                dr["OverwriteUnitsMinimum"] = 0;
                dr["OverwriteUnitsMaximum"] = 0;
                objDS.Tables[0].Rows.Add(dr);
            }

            return objDS;

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program degree option elective groups.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 OptionID - Program degree option primary key (ProgOptASN)</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: ElectiveGroupTitle, FootnoteNumber, OptionFootnoteID, OptionElectiveGroupID, ElectiveCount</p>")]
    public DataSet GetOptionElectiveGroups(Int32 OptionID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetOptionElectiveGroups", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns elective group courses.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 OptionElectiveGroupID - Elective Gorup primary key<br />" +
         "String BeginSTRM - Program begin term code<br />" +
         "String EndSTRM - Program end term code<br />" +
         "String SelectedSTRM - Term code selected</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: OptionElectiveGroupID, CourseOffering, SUBJECT, CATALOG_NBR, CourseSuffix, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, FootnoteNumber, OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum</p>")]
    public DataSet GetElectiveGroupCourses(Int32 OptionElectiveGroupID, String BeginSTRM, String EndSTRM, String SelectedSTRM) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetElectiveGroupCourses", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionElectiveGroupID", SqlDbType.Int);
            objCmd.Parameters["@OptionElectiveGroupID"].Value = OptionElectiveGroupID;
            objCmd.Parameters.Add("@BeginSTRM", SqlDbType.VarChar);
            objCmd.Parameters["@BeginSTRM"].Value = BeginSTRM;
            objCmd.Parameters.Add("@EndSTRM", SqlDbType.VarChar);
            objCmd.Parameters["@EndSTRM"].Value = EndSTRM;
            objCmd.Parameters.Add("@SelectedSTRM", SqlDbType.VarChar);
            objCmd.Parameters["@SelectedSTRM"].Value = SelectedSTRM;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program degree option footnotes.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 OptionID - Program degree option primary key </p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: OptionFootnoteID, FootnoteNumber, Footnote</p>")]
    public DataSet GetOptionFootnotes(Int32 OptionID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetOptionFootnotes", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns subject areas of courses offered during the term entered.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String CollegeID - College primary key (CollegeASN) Enter an empty string to return all colleges.<br />" +
         "String STRM - Term Code (Enter an empty string to return the current term/quarter.)</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: SUBJECT, SubjectArea_DESCR</p>")]
    public DataSet GetCourseSubjectAreas(String CollegeID, String STRM) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetCourseSubjectAreas", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (CollegeID != null && CollegeID != "") {
                objCmd.Parameters.Add("@CollegeID", SqlDbType.Int);
                objCmd.Parameters["@CollegeID"].Value = CollegeID;
            }
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns the college offering a course.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String CourseID - Course Primary Key</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: CollegeID, CollegeShortTitle</p>")]
    public DataSet GetCourseCollege(Int32 CourseID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetCourseCollege", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@CourseID", SqlDbType.Int);
            objCmd.Parameters["@CourseID"].Value = CourseID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns all course descriptions by college, quarter, department and course number. Used to show course descriptions in the same format as the printed catalog. Courses using the same descriptions are grouped together.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String SUBJECT - Course subject area / department<br />" +
         "String CollegeID - College primary key (CollegeASN) Enter an empty string to return all colleges.<br />" +
         "String STRM - Term Code (Enter an empty string to return the current term.)<br />" +
         "String CATALOG_NBR - Course number (Enter an empty string to return all courses in a subject area.)</p>" +
         "<p><strong>Output:</strong>DataSet<br />" +
         "FIELD LIST: CourseBeginSTRM, CourseID, COURSE_TITLE_LONG, DESCRLONG, UNITS_MINIMUM, UNITS_MAXIMUM, VariableUnits, CourseOffering, SUBJECT, CATALOG_NBR_A, CATALOG_NBR_B, DescriptionCourseID, SubjectArea_DESCR</p>")]
    public DataSet GetCourseDescriptions(String SUBJECT, String CollegeID, String STRM, String CATALOG_NBR) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetCourseDescriptions", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
            objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
            if (CollegeID != null && CollegeID != "") {
                objCmd.Parameters.Add("@CollegeID", SqlDbType.Int);
                objCmd.Parameters["@CollegeID"].Value = CollegeID;
            }
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (CATALOG_NBR != null && CATALOG_NBR != "") {
                objCmd.Parameters.Add("@CATALOG_NBR", SqlDbType.VarChar);
                objCmd.Parameters["@CATALOG_NBR"].Value = CATALOG_NBR;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns course description by course primary key.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 CourseID - Course primary key</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: COURSE_TITLE_LONG, DESCRLONG, UNITS_MINIMUM, UNITS_MAXIMUM, VariableUnits, CourseOffering, SUBJECT, CATALOG_NBR, SubjectArea_DESCR, AlternameDESCRLONG</p>")]
    public DataSet GetCourseDescription(Int32 CourseID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetCourseDescription", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@CourseID", SqlDbType.Int);
            objCmd.Parameters["@CourseID"].Value = CourseID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program categories</p>" +
         "<p><strong>Input:</strong> None</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: CategoryID, CategoryTitle, ProgramCount</p>")]
    public DataSet GetProgramCategories() {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetProgramCategories", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns term codes and descriptions for versions of programs available. (Past four years, current and future)</p>" +
         "<p><strong>Input:</strong> None</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: STRM, DESCR, TERM_BEGIN_DT, TERM_END_DT</p>")]
    public DataSet GetProgramTermList() {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetProgramTermList", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns term codes and descriptions for course descriptions. (Past four years, current and future)</p>" +
         "<p><strong>Input:</strong> None</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: STRM, DESCR, TERM_BEGIN_DT, TERM_END_DT</p>")]
    public DataSet GetCourseTermList() {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetCourseTermList", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Retrieves administrative and academic employee credentials by work unit (SCC, SFCC, IEL, DIST).</p>" +
         "<p><strong>Input:</strong><br />" +
         "String AlphaCharacter - Letter of the alphabet the employee's last name start with</p>" +
         "String WorkUnit - Employee's place of work (SCC, SFCC, IEL, DIST). Enter an empty string to return credentials for employees at all work units.</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: CredentialID, Name, WorkUnit, Assignment, Description</p>")]
    public DataSet GetCredentials(String AlphaCharacter, String WorkUnit) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetCredentials", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (AlphaCharacter != null && AlphaCharacter != "") {
                objCmd.Parameters.Add("@AlphaCharacter", SqlDbType.Char);
                objCmd.Parameters["@AlphaCharacter"].Value = AlphaCharacter;
            }
            if (WorkUnit != null && WorkUnit != "") {
                objCmd.Parameters.Add("@WorkUnit", SqlDbType.VarChar);
                objCmd.Parameters["@WorkUnit"].Value = WorkUnit;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Retrieves course description and class schedule data for a course.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String CourseOffering - Course subject area padded to five spaces and course catalog number<br />" +
         "String STRM - Term code<br />" +
         "String CollegeID - College primary key<br />" +
         "String BeginSTRM - Program begin term code<br />" +
         "String EndSTRM - Program end term code</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: COURSE_TITLE_LONG, DESCRLONG, CourseID, UNITS_MINIMUM, UNITS_MAXIMUM, VariableUnits, CourseOffering, SUBJECT, CATALOG_NBR, SubjectArea_DESCR, AlternateDESCRLONG</p>")]
    public DataSet GetCourseSchedule(String CourseOffering, String STRM, String CollegeID, String BeginSTRM, String EndSTRM) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetCourseSchedule", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@CourseOffering", SqlDbType.VarChar);
            objCmd.Parameters["@CourseOffering"].Value = CourseOffering;
            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;
            if (CollegeID != null && CollegeID != "") {
                objCmd.Parameters.Add("@CollegeID", SqlDbType.VarChar);
                objCmd.Parameters["@CollegeID"].Value = CourseOffering;
            }
            objCmd.Parameters.Add("@BeginSTRM", SqlDbType.VarChar);
            objCmd.Parameters["@BeginSTRM"].Value = BeginSTRM;
            objCmd.Parameters.Add("@EndSTRM", SqlDbType.VarChar);
            objCmd.Parameters["@EndSTRM"].Value = EndSTRM;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns degree titles and primary keys by term (optional).</p>" +
         "<p><strong>Input:</strong><br />" +
         "String STRM - Term Code (Enter an empty string to return the current term.)</p>" +
         "<p><strong>Output:</strong>DataSet<br />" +
         "FIELD LIST: DegreeID, DegreeShortTitle, DegreeLongTitle</p>")]
    public DataSet GetDegreeTitles(String STRM) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetDegreeTitles", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns an alphabetic program option list by term, location, degree, letter the program option title starts with, course subject, and course catalog number.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String STRM - Term Code<br />" +
         "String LocationID - Location primary key (Enter an empty string to return program options for all locations.)<br />" +
         "String DegreeID - Degree primary key (Enter an empty string to return all degrees.)<br />" +
         "String AlphaCharacter - Letter of the alphabet the program titles and option titles start with (Enter an empty string to return all program and option titles.)<br />" +
         "String SUBJECT - Course subject area (Enter an empty string to return program options containing all course subject areas.)<br />" +
         "String CATALOG_NBR - Coruse catalog number (Enter an empty string to return program options containing all course catalog numbers.)</p>" +
         "<p><strong>Output:</strong>DataSet<br />" +
         "FIELD LIST: CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, ProgramVersionID, ProgramDegreeID, OptionID</p>")]
    public DataSet GetOptionsByTitle(String STRM, String LocationID, String DegreeID, String AlphaCharacter, String SUBJECT, String CATALOG_NBR) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetOptionsByTitle", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;
            if (LocationID != null && LocationID != "") {
                objCmd.Parameters.Add("@LocationID", SqlDbType.Int);
                objCmd.Parameters["@LocationID"].Value = LocationID;
            }
            if (DegreeID != null && DegreeID != "") {
                objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                objCmd.Parameters["@DegreeID"].Value = DegreeID;
            }
            if (AlphaCharacter != null && AlphaCharacter != "") {
                objCmd.Parameters.Add("@AlphaCharacter", SqlDbType.Char);
                objCmd.Parameters["@AlphaCharacter"].Value = AlphaCharacter;
            }
            if (SUBJECT != null && SUBJECT != "") {
                objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
            }
            if (CATALOG_NBR != null && CATALOG_NBR != "") {
                objCmd.Parameters.Add("@CATALOG_NBR", SqlDbType.VarChar);
                objCmd.Parameters["@CATALOG_NBR"].Value = CATALOG_NBR;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns an alphabetic area of study degree list.</p>" +
         "<p><strong>Input:</strong><br />" +
             "String STRM - Term Code<br />" +
             "String LocationID - Location primary key (Enter an empty string to return program options for all locations.)<br />" +
             "String DegreeID - Degree primary key (Enter an empty string to return all degrees.)<br />" +
             "String AlphaCharacter - Letter of the alphabet the area of study starts with (Enter an empty string to return all areas of study.)<br />" +
             "String AreaOfStudyID - Area of Study Primary key (Enter an empty string to return degrees in all areas of study.)</br />" +
             "String SUBJECT - Course subject area (Enter an empty string to return program options containing all course subject areas.)<br />" +
             "String CATALOG_NBR - Coruse catalog number (Enter an empty string to return program options containing all course catalog numbers.)<br />" +
             "String GlobalSearch - Search Text</p>" +
             "<p><strong>Output:</strong> JSONP<br />" +
             "FIELD LIST: AreaOfStudy.Title, AreaOfStudy.WebsiteURL, CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, ProgramID, ProgramVersionID, ProgramDegreeID, OptionID, DocumentTitle, DegreeType</p>")]
    public DataSet GetDegreesByAreaOfStudy(String STRM, String LocationID, String DegreeID, String AlphaCharacter, String AreaOfStudyID, String SUBJECT, String CATALOG_NBR, String GlobalSearch) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();

            //get programs by area of study
            objCmd = new SqlCommand("usp_iCatalog_GetDegreeProgramsByAreaOfStudy", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (LocationID != null && LocationID != "") {
                objCmd.Parameters.Add("@LocationID", SqlDbType.Int);
                objCmd.Parameters["@LocationID"].Value = LocationID;
            }
            if (DegreeID != null && DegreeID != "") {
                objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                objCmd.Parameters["@DegreeID"].Value = DegreeID;
            }
            if (AlphaCharacter != null && AlphaCharacter != "") {
                objCmd.Parameters.Add("@AlphaCharacter", SqlDbType.Char);
                objCmd.Parameters["@AlphaCharacter"].Value = AlphaCharacter;
            }
            if (AreaOfStudyID != null && AreaOfStudyID != "") {
                objCmd.Parameters.Add("@AreaOfStudyID", SqlDbType.Int);
                objCmd.Parameters["@AreaOfStudyID"].Value = AreaOfStudyID;
            }
            if (SUBJECT != null && SUBJECT != "") {
                objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
            }
            if (CATALOG_NBR != null && CATALOG_NBR != "") {
                objCmd.Parameters.Add("@CATALOG_NBR", SqlDbType.VarChar);
                objCmd.Parameters["@CATALOG_NBR"].Value = CATALOG_NBR;
            }
            if (GlobalSearch != null && GlobalSearch != "") {
                objCmd.Parameters.Add("@GlobalSearch", SqlDbType.VarChar);
                objCmd.Parameters["@GlobalSearch"].Value = GlobalSearch;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            if ((SUBJECT == null || SUBJECT == "") && (CATALOG_NBR == null || CATALOG_NBR == "")) {
                //get general degrees by area of study
                objCmd = new SqlCommand("usp_iCatalog_GetDegreesByAreaOfStudy", objCon);
                DataSet objDSDegrees = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                if (STRM != null && STRM != "") {
                    objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                    objCmd.Parameters["@STRM"].Value = STRM;
                }
                if (DegreeID != null && DegreeID != "") {
                    objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                    objCmd.Parameters["@DegreeID"].Value = DegreeID;
                }
                if (AlphaCharacter != null && AlphaCharacter != "") {
                    objCmd.Parameters.Add("@AlphaCharacter", SqlDbType.Char);
                    objCmd.Parameters["@AlphaCharacter"].Value = AlphaCharacter;
                }
                if (GlobalSearch != null && GlobalSearch != "") {
                    objCmd.Parameters.Add("@GlobalSearch", SqlDbType.VarChar);
                    objCmd.Parameters["@GlobalSearch"].Value = GlobalSearch;
                }
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDSDegrees);

                //if objDS dataset is not empty
                if (objDS.Tables.Count > 0) {

                    DataRow dr;
                    //combine program degrees and general degrees
                    for (Int32 i = 0; i < objDSDegrees.Tables[0].Rows.Count; i++) {
                        dr = objDS.Tables[0].NewRow();
                        dr["Title"] = objDSDegrees.Tables[0].Rows[i]["Title"];
                        dr["WebsiteURL"] = objDSDegrees.Tables[0].Rows[i]["WebsiteURL"];
                        dr["CollegeShortTitle"] = "";
                        dr["CollegeLongTitle"] = "";
                        dr["ProgramTitle"] = "";
                        dr["ProgramWebsiteURL"] = "";
                        dr["OptionTitle"] = "";
                        dr["DegreeID"] = objDSDegrees.Tables[0].Rows[i]["DegreeID"];
                        dr["DegreeShortTitle"] = objDSDegrees.Tables[0].Rows[i]["DegreeShortTitle"];
                        dr["DegreeLongTitle"] = objDSDegrees.Tables[0].Rows[i]["DegreeLongTitle"];
                        dr["ProgramID"] = 0;
                        dr["ProgramVersionID"] = 0;
                        dr["ProgramDegreeID"] = 0;
                        dr["OptionID"] = 0;
                        dr["DocumentTitle"] = objDSDegrees.Tables[0].Rows[i]["DocumentTitle"];
                        dr["DegreeType"] = objDSDegrees.Tables[0].Rows[i]["DegreeType"];
                        objDS.Tables[0].Rows.Add(dr);
                    }

                    //sort data by quarter
                    objDS.Tables[0].DefaultView.Sort = "Title, DegreeLongTitle, ProgramTitle, OptionTitle, CollegeShortTitle";

                    //store sorted data in a new datatable
                    DataTable objDT = objDS.Tables[0].DefaultView.ToTable();

                    //add datatable to dataset
                    objDS = new DataSet();
                    if (objDT.Rows.Count > 0) {
                        objDS.Tables.Add(objDT);
                    }

                    return objDS;

                } else {
                    return objDSDegrees;
                }

            } else {
                return objDS;
            }

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns an program option list by term, location, degree, category, course subject, and course catalog number.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String STRM - Term Code<br />" +
         "String LocationID - Location primary key (Enter an empty string to return program options for all locations.)<br />" +
         "String DegreeID - Degree primary key (Enter an empty string to return all degrees.)<br />" +
         "String CategoryID - Category primary key (Enter an empty string to return all categories.)<br />" +
         "String SUBJECT - Course subject area (Enter an empty string to return program options containing all course subject areas.)<br />" +
         "String CATALOG_NBR - Course catalog number (Enter an empty string to return program options containing all course catalog numbers.)</p>" +
         "<p><strong>Output:</strong>DataSet<br />" +
         "FIELD LIST: CollegeShortTitle, CollegeLongTitle, CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, ProgramVersionID, ProgramDegreeID, OptionID</p>")]
    public DataSet GetOptionsByCategory(String STRM, String LocationID, String DegreeID, String CategoryID, String SUBJECT, String CATALOG_NBR) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetOptionsByCategory", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;
            if (LocationID != null && LocationID != "") {
                objCmd.Parameters.Add("@LocationID", SqlDbType.Int);
                objCmd.Parameters["@LocationID"].Value = LocationID;
            }
            if (DegreeID != null && DegreeID != "") {
                objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                objCmd.Parameters["@DegreeID"].Value = DegreeID;
            }
            if (CategoryID != null && CategoryID != "") {
                objCmd.Parameters.Add("@CategoryID", SqlDbType.Int);
                objCmd.Parameters["@CategoryID"].Value = CategoryID;
            }
            if (SUBJECT != null && SUBJECT != "") {
                objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
            }
            if (CATALOG_NBR != null && CATALOG_NBR != "") {
                objCmd.Parameters.Add("@CATALOG_NBR", SqlDbType.VarChar);
                objCmd.Parameters["@CATALOG_NBR"].Value = CATALOG_NBR;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns a program list ordered by title.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String CollegeID - College primary key (Enter an empty string to return programs for all colleges.)<br />" +
         "String STRM - Term code (Enter an empty string to return the current term.)</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: ProgramTitle, ProgramVersionID, CollegeShortTitle</p>")]
    public DataSet GetProgramsByTitle(String CollegeID, String STRM) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetProgramsByTitle", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (CollegeID != null && CollegeID != "") {
                objCmd.Parameters.Add("@CollegeID", SqlDbType.Int);
                objCmd.Parameters["@CollegeID"].Value = CollegeID;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns a program list ordered by category.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String CollegeID - College primary key (Enter an empty string to return programs for all colleges.)<br />" +
         "String STRM - Term code (Enter an empty string to return the current term.)</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: CategoryTitle, CategoryID, ProgramTitle, ProgramVersionID, CollegeShortTitle</p>")]
    public DataSet GetProgramsByCategory(String CollegeID, String STRM) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetProgramsByCategory", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (CollegeID != null && CollegeID != "") {
                objCmd.Parameters.Add("@CollegeID", SqlDbType.Int);
                objCmd.Parameters["@CollegeID"].Value = CollegeID;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns active course catalog numbers by term, college, and subject area.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String SUBJECT - Course Subject Area / Department (Enter an empty string to return all course numbers regardless of subject area.)<br />" +
         "String STRM - Term code Code<br />" +
         "String CollegeID - College primary key (Enter an empty string to return all colleges)</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: CATALOG_NBR</p>")]
    public DataSet GetCourseCatalogNumbers(String SUBJECT, String STRM, String CollegeID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetCourseCatalogNumbers", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;
            if (SUBJECT != null && SUBJECT != "") {
                objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
            }
            if (CollegeID != null && CollegeID != "") {
                objCmd.Parameters.Add("@CollegeID", SqlDbType.Int);
                objCmd.Parameters["@CollegeID"].Value = CollegeID;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns program option locations.</p>" +
         "<p><strong>Input:</strong><br />" +
         "Int32 OptionID - Program option primary key (ProgOptASN)</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: LocationID, LocationTitle</p>")]
    public DataSet GetOptionLocations(Int32 OptionID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetOptionLocations", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@OptionID", SqlDbType.Int);
            objCmd.Parameters["@OptionID"].Value = OptionID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns OptionID, ProgramDegreeID, and ProgramVersionID based on GainfulEmploymentID for the current quarter program option.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String GainfulEmploymentID - Gainful Employment Course ID for Program Options</p>" +
         "<p><strong>Output:</strong> DataSet<br />" +
         "FIELD LIST: OptionID, ProgramDegreeID, ProgramVersionID</p>")]
    public DataSet GetOptionIDByGainfulEmploymentID(String GainfulEmploymentID) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetOptionIDByGainfulEmploymentID", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@GainfulEmploymentID", SqlDbType.VarChar);
            objCmd.Parameters["@GainfulEmploymentID"].Value = GainfulEmploymentID;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns GainfulEmploymentID for all program options based on the static program id.</p>" +
        "<p><strong>Input:</strong> ProgramID - Non-changing program id<br />" +
        "String STRM - Term Code (Enter an empty string to return the current term.)<p>" +
        "<p><strong>Output:</strong> DataSet<br />" +
        "FIELD LIST: GainfulEmploymentID</p>")]
    public DataSet GetGainfulEmploymentID(Int16 ProgramID, String STRM) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetGainfulEmploymentID", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@ProgramID", SqlDbType.Int);
            objCmd.Parameters["@ProgramID"].Value = ProgramID;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns all degrees offered for a selected term.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String STRM - Term Code<br />" +
         "String LocationID - Location primary key (Enter an empty string to return program options for all locations.)<br />" +
         "String DegreeType - Degree Type (Transfer or Career Technical)<br />" +
         "String DegreeID - Degree primary key (Enter an empty string to return all degrees.)<br />" +
         "String SUBJECT - Course subject area (Enter an empty string to return program options containing all course subject areas.)<br />" +
         "String CATALOG_NBR - Course catalog number (Enter an empty string to return program options containing all course catalog numbers.)<br />" +
         "String GlobalSearch - Search Text</p>" +
         "<p><strong>Output:</strong>DataSet<br />" +
         "FIELD LIST: CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, ProgramID, ProgramVersionID, ProgramDegreeID, OptionID, DocumentTitle, DegreeType</p>")]
    public DataSet GetDegrees(String STRM, String LocationID, String DegreeID, String DegreeType, String SUBJECT, String CATALOG_NBR, String GlobalSearch) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();

            //get programs by degree
            objCmd = new SqlCommand("usp_iCatalog_GetDegreeProgramsByDegree", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (LocationID != null && LocationID != "") {
                objCmd.Parameters.Add("@LocationID", SqlDbType.Int);
                objCmd.Parameters["@LocationID"].Value = LocationID;
            }
            if (DegreeID != null && DegreeID != "") {
                objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                objCmd.Parameters["@DegreeID"].Value = DegreeID;
            }
            if (DegreeType != null && DegreeType != "") {
                objCmd.Parameters.Add("@DegreeType", SqlDbType.VarChar);
                objCmd.Parameters["@DegreeType"].Value = DegreeType;
            }
            if (SUBJECT != null && SUBJECT != "") {
                objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
            }
            if (CATALOG_NBR != null && CATALOG_NBR != "") {
                objCmd.Parameters.Add("@CATALOG_NBR", SqlDbType.VarChar);
                objCmd.Parameters["@CATALOG_NBR"].Value = CATALOG_NBR;
            }
            if (GlobalSearch != null && GlobalSearch != "") {
                objCmd.Parameters.Add("@GlobalSearch", SqlDbType.VarChar);
                objCmd.Parameters["@GlobalSearch"].Value = GlobalSearch;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            if ((SUBJECT == null || SUBJECT == "") && (CATALOG_NBR == null || CATALOG_NBR == "")) {
                //get general degrees
                objCmd = new SqlCommand("usp_iCatalog_GetDegrees", objCon);
                DataSet objDSDegrees = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                if (STRM != null && STRM != "") {
                    objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                    objCmd.Parameters["@STRM"].Value = STRM;
                }
                if (DegreeID != null && DegreeID != "") {
                    objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                    objCmd.Parameters["@DegreeID"].Value = DegreeID;
                }
                if (DegreeType != null && DegreeType != "") {
                    objCmd.Parameters.Add("@DegreeType", SqlDbType.VarChar);
                    objCmd.Parameters["@DegreeType"].Value = DegreeType;
                }
                if (GlobalSearch != null && GlobalSearch != "") {
                    objCmd.Parameters.Add("@GlobalSearch", SqlDbType.VarChar);
                    objCmd.Parameters["@GlobalSearch"].Value = GlobalSearch;
                }
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDSDegrees);

                //if objDS dataset is not empty
                if (objDS.Tables.Count > 0) {

                    DataRow dr;
                    //combine program degrees and general degrees
                    for (Int32 i = 0; i < objDSDegrees.Tables[0].Rows.Count; i++) {
                        dr = objDS.Tables[0].NewRow();
                        dr["CollegeShortTitle"] = "";
                        dr["CollegeLongTitle"] = "";
                        dr["ProgramTitle"] = "";
                        dr["ProgramWebsiteURL"] = "";
                        dr["OptionTitle"] = "";
                        dr["DegreeID"] = objDSDegrees.Tables[0].Rows[i]["DegreeID"];
                        dr["DegreeShortTitle"] = objDSDegrees.Tables[0].Rows[i]["DegreeShortTitle"];
                        dr["DegreeLongTitle"] = objDSDegrees.Tables[0].Rows[i]["DegreeLongTitle"];
                        dr["ProgramID"] = 0;
                        dr["ProgramVersionID"] = 0;
                        dr["ProgramDegreeID"] = 0;
                        dr["OptionID"] = 0;
                        dr["DocumentTitle"] = objDSDegrees.Tables[0].Rows[i]["DocumentTitle"];
                        dr["DegreeType"] = objDSDegrees.Tables[0].Rows[i]["DegreeType"];
                        dr["DegreeDescription"] = objDSDegrees.Tables[0].Rows[i]["DegreeDescription"];
                        objDS.Tables[0].Rows.Add(dr);
                    }

                    //sort data by quarter
                    objDS.Tables[0].DefaultView.Sort = "DegreeLongTitle, ProgramTitle, OptionTitle, CollegeShortTitle";

                    //store sorted data in a new datatable
                    DataTable objDT = objDS.Tables[0].DefaultView.ToTable();

                    //add datatable to dataset
                    objDS = new DataSet();
                    if (objDT.Rows.Count > 0) {
                        objDS.Tables.Add(objDT);
                    }

                    return objDS;

                } else {
                    return objDSDegrees;
                }

            } else {
                return objDS;
            }

        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns a degree list by term, location, degree, category, course subject, and course catalog number.</p>" +
         "<p><strong>Input:</strong><br />" +
         "String STRM - Term Code<br />" +
         "String LocationID - Location primary key (Enter an empty string to return program options for all locations.)<br />" +
         "String DegreeID - Degree primary key (Enter an empty string to return all degrees.)<br />" +
         "String CategoryID - Category primary key (Enter an empty string to return all categories.)<br />" +
         "String SUBJECT - Course subject area (Enter an empty string to return program options containing all course subject areas.)<br />" +
         "String CATALOG_NBR - Course catalog number (Enter an empty string to return program options containing all course catalog numbers.)<br />" +
         "String GlobalSearch - Search Text</p>" +
         "<p><strong>Output:</strong>DataSet<br />" +
         "FIELD LIST: CollegeShortTitle, CollegeLongTitle, CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, ProgramID, ProgramVersionID, ProgramDegreeID, OptionID</p>")]
    public DataSet GetDegreesByCategory(String STRM, String LocationID, String DegreeID, String CategoryID, String SUBJECT, String CATALOG_NBR, String GlobalSearch) {
        objCon = new SqlConnection(iCatalogConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_iCatalog_GetDegreeProgramsByCategory", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (STRM != null && STRM != "") {
                objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                objCmd.Parameters["@STRM"].Value = STRM;
            }
            if (LocationID != null && LocationID != "") {
                objCmd.Parameters.Add("@LocationID", SqlDbType.Int);
                objCmd.Parameters["@LocationID"].Value = LocationID;
            }
            if (DegreeID != null && DegreeID != "") {
                objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                objCmd.Parameters["@DegreeID"].Value = DegreeID;
            }
            if (CategoryID != null && CategoryID != "") {
                objCmd.Parameters.Add("@CategoryID", SqlDbType.Int);
                objCmd.Parameters["@CategoryID"].Value = CategoryID;
            }
            if (SUBJECT != null && SUBJECT != "") {
                objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
            }
            if (CATALOG_NBR != null && CATALOG_NBR != "") {
                objCmd.Parameters.Add("@CATALOG_NBR", SqlDbType.VarChar);
                objCmd.Parameters["@CATALOG_NBR"].Value = CATALOG_NBR;
            }
            if (GlobalSearch != null && GlobalSearch != "") {
                objCmd.Parameters.Add("@GlobalSearch", SqlDbType.VarChar);
                objCmd.Parameters["@GlobalSearch"].Value = GlobalSearch;
            }
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);

            if ((SUBJECT == null || SUBJECT == "") && (CATALOG_NBR == null || CATALOG_NBR == "") && (CategoryID == null || CategoryID == "" || CategoryID == "18")) {
                //get general degrees by area of study
                objCmd = new SqlCommand("usp_iCatalog_GetDegrees", objCon);
                DataSet objDSDegrees = new DataSet();
                objCmd.CommandType = CommandType.StoredProcedure;
                if (STRM != null && STRM != "") {
                    objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
                    objCmd.Parameters["@STRM"].Value = STRM;
                }
                if (DegreeID != null && DegreeID != "") {
                    objCmd.Parameters.Add("@DegreeID", SqlDbType.Int);
                    objCmd.Parameters["@DegreeID"].Value = DegreeID;
                }
                objCmd.Parameters.Add("@DegreeType", SqlDbType.VarChar);
                objCmd.Parameters["@DegreeType"].Value = "Transfer";
                if (GlobalSearch != null && GlobalSearch != "") {
                    objCmd.Parameters.Add("@GlobalSearch", SqlDbType.VarChar);
                    objCmd.Parameters["@GlobalSearch"].Value = GlobalSearch;
                }
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDSDegrees);

                //if objDS dataset is not empty
                if (objDS.Tables.Count > 0) {

                    DataRow dr;
                    //combine program degrees and general degrees
                    for (Int32 i = 0; i < objDSDegrees.Tables[0].Rows.Count; i++) {
                        dr = objDS.Tables[0].NewRow();
                        dr["CollegeShortTitle"] = "";
                        dr["CollegeLongTitle"] = "";
                        dr["CategoryID"] = 18;
                        dr["CategoryTitle"] = "Transfer Degrees";
                        dr["ProgramTitle"] = "";
                        dr["ProgramWebsiteURL"] = "";
                        dr["OptionTitle"] = "";
                        dr["DegreeShortTitle"] = objDSDegrees.Tables[0].Rows[i]["DegreeShortTitle"];
                        dr["DegreeLongTitle"] = objDSDegrees.Tables[0].Rows[i]["DegreeLongTitle"];
                        dr["ProgramID"] = 0;
                        dr["ProgramVersionID"] = 0;
                        dr["ProgramDegreeID"] = 0;
                        dr["OptionID"] = 0;
                        dr["DegreeID"] = objDSDegrees.Tables[0].Rows[i]["DegreeID"];
                        dr["DocumentTitle"] = objDSDegrees.Tables[0].Rows[i]["DocumentTitle"];
                        dr["DegreeType"] = objDSDegrees.Tables[0].Rows[i]["DegreeType"];
                        objDS.Tables[0].Rows.Add(dr);
                    }

                    //sort data by quarter
                    objDS.Tables[0].DefaultView.Sort = "CategoryTitle, DegreeLongTitle, ProgramTitle, OptionTitle, CollegeShortTitle";

                    //store sorted data in a new datatable
                    DataTable objDT = objDS.Tables[0].DefaultView.ToTable();

                    //add datatable to dataset
                    objDS = new DataSet();
                    if (objDT.Rows.Count > 0) {
                        objDS.Tables.Add(objDT);
                    }

                    return objDS;

                } else {
                    return objDSDegrees;
                }

            } else {
                return objDS;
            }
        } finally {
            objCon.Close();
        }
    }

}
