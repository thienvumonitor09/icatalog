USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetActiveCourses]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn, Edited: Vu Nguyen
-- Create date: 3/8/17
-- Description:	Returns a list of active courses offered for a specific quarter/term (optional) in a specific subject/department (optional)	
--		 INPUT: STRM (optional) - Term Code
--				SUBJECT (optional) - Course Subject: ENGL
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetActiveCourses]
	@STRM varchar(4) = NULL,
	@SUBJECT varchar(5) = NULL,
	@INSTITUTION varchar(5)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DISTINCT INSTITUTION, CourseOffering.CRSE_ID, CourseCatalog.CRSE_ID, (SUBJECT + CATALOG_NBR) AS CourseOffering, SUBJECT, CATALOG_NBR, COURSE_TITLE_LONG, CourseOffering.EFFDT ,EFF_STATUS, CATALOG_PRINT, SCHEDULE_PRINT, DESCR
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT
	WHERE 
		(
			@STRM IS NULL
			OR
			CourseOffering.EFFDT = 
			(
				SELECT MAX(CourseOffering2.EFFDT) 
				FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
				WHERE CourseOffering2.CRSE_ID = CourseOffering.CRSE_ID
					AND INSTITUTION = @INSTITUTION
					AND
					(
						CourseOffering2.EFFDT <= (SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE (STRM = @STRM) ORDER BY TERM_END_DT DESC)
					)
			) 
		)
		
		AND (@SUBJECT IS NULL OR (REPLACE(SUBJECT,' ','') = @SUBJECT OR REPLACE(SUBJECT,' ','') = @SUBJECT + '&'))
		AND ACAD_CAREER = 'UGRD'
		AND CATALOG_PRINT = 'Y'
		AND EFF_STATUS = 'A'
		AND INSTITUTION = 'WA171'
	ORDER BY SUBJECT,CATALOG_NBR ASC
	/*
    SELECT DISTINCT INSTITUTION, CourseOffering.CRSE_ID, CourseCatalog.CRSE_ID, (SUBJECT + CATALOG_NBR) AS CourseOffering, SUBJECT, CATALOG_NBR, COURSE_TITLE_LONG, CourseOffering.EFFDT AS CourseOffering_EFFDT, CourseCatalog.EFFDT AS CourseCatalog_EFFDT, EFF_STATUS, CATALOG_PRINT, SCHEDULE_PRINT, DESCR
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID
	AND CourseCatalog.EFFDT = 
		(SELECT MAX(CourseCatalog2.EFFDT)
		FROM CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog2
		WHERE CourseCatalog2.CRSE_ID = CourseCatalog.CRSE_ID
		AND CourseCatalog2.EFFDT <= (SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE STRM = @STRM ORDER BY TERM_END_DT DESC))
	WHERE (@SUBJECT IS NULL OR (RTRIM(SUBJECT) = @SUBJECT OR RTRIM(SUBJECT) = @SUBJECT + '&'))
	AND CourseOffering.EFFDT = 
		(SELECT MAX(CourseOffering2.EFFDT) 
		FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
		WHERE CourseOffering2.CRSE_ID = CourseOffering.CRSE_ID
		AND INSTITUTION = 'WA171'
		AND CourseOffering2.EFFDT <= (SELECT TOP 1 TERM_END_DT FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable WHERE STRM = @STRM ORDER BY TERM_END_DT DESC)) 
	AND COURSE_APPROVED = 'A'
	AND EFF_STATUS = 'A'
	AND INSTITUTION = 'WA171'
	AND CATALOG_PRINT = 'Y'
	AND ACAD_CAREER = 'UGRD'
	ORDER BY CATALOG_NBR, CourseCatalog_EFFDT DESC;
	*/
END
GO
