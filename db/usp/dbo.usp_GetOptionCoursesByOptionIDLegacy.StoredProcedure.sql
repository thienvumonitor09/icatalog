USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionCoursesByOptionIDLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/11/17
-- Description:	Returns program degree option courses
--		 INPUT: OptionID - Program degree option id 
--				SelectedSTRM - Selected term code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionCoursesByOptionIDLegacy]
	@OptionID int,
	@SelectedSTRM varchar (4) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @SelectedSTRM IS NULL
		SET @SelectedSTRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)

    SELECT OptionCourse.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
    FROM OptionCourse 
    INNER JOIN ProgramDegreeOption
    ON ProgramDegreeOption.OptionID = OptionCourse.OptionID
    INNER JOIN ProgramDegree
    ON ProgramDegree.ProgramDegreeID = ProgramDegreeOption.ProgramDegreeID
    INNER JOIN Program 
    ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
    INNER JOIN Course 
    ON Course.CourseOffering = OptionCourse.CourseOffering 
    AND ((CourseBeginSTRM <= ProgramBeginSTRM AND (CourseEndSTRM >= ProgramBeginSTRM OR CourseEndSTRM = 'Z999')) 
    OR (CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM))
    AND (CourseBeginSTRM <= @SelectedSTRM AND (CourseEndSTRM >= @SelectedSTRM OR CourseEndSTRM = 'Z999')) 
	AND PublishedCourse = '1'
    LEFT OUTER JOIN OptionFootnote 
    ON OptionFootnote.OptionFootnoteID = OptionCourse.OptionFootnoteID 
    WHERE OptionCourse.OptionID = @OptionID
    GROUP BY OptionCourse.CourseOffering, Course.CourseID, SUBJECT, CATALOG_NBR, CourseSuffix, CourseEndSTRM, COURSE_TITLE_LONG, UNITS_MINIMUM, UNITS_MAXIMUM, Quarter, FootnoteNumber, OptionCourse.OptionFootnoteID, OverwriteUnits, OverwriteUnitsMinimum, OverwriteUnitsMaximum
    ORDER BY Quarter, SUBSTRING(SUBJECT,1,LEN(REPLACE(SUBJECT,'&',''))), CATALOG_NBR;
END
GO
