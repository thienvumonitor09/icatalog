using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.dept
{
	/// <summary>
	/// Summary description for proof.
	/// </summary>
	public partial class proof : System.Web.UI.Page
	{

		courseData csCourse = new courseData();
		termData csTerm = new termData();


		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			if(!IsPostBack){
				cboSubject.Items.Add("ALL");
				DataSet dsSubjects = csCourse.GetSubjects();
				for(Int32 intDSRow = 0; intDSRow < dsSubjects.Tables[0].Rows.Count; intDSRow++){
					cboSubject.Items.Add(dsSubjects.Tables[0].Rows[intDSRow]["SUBJECT"].ToString());
				}
				DataSet dsTerms = csCourse.GetTermsForCourses();
				for(Int32 intDSRow = 0; intDSRow < dsTerms.Tables[0].Rows.Count; intDSRow++){
					cboTerm.Items.Add(new ListItem(dsTerms.Tables[0].Rows[intDSRow]["DESCR"].ToString(), dsTerms.Tables[0].Rows[intDSRow]["STRM"].ToString()));
				}
				try{
					cboTerm.SelectedValue = csTerm.GetCurrentTerm();
				}catch{
					//do nothing
				}
				panProof.Visible = false;
			}
		}

		protected void cmdSubmit_Click(object sender, System.EventArgs e){
			DataSet dsSubjects = csCourse.GetSubjectsForProof(cboTerm.SelectedValue, cboSubject.SelectedValue, cboOrderBy.SelectedValue);
			Int32 intRowCtr = dsSubjects.Tables[0].Rows.Count;
			TableRow tr;
			TableCell td;

			if(intRowCtr > 0){
				tr = new TableRow();

				td = new TableCell();
				td.Attributes["style"] = "font-size:10pt;font-family:arial;text-align:left;";
				td.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;<b><u>Subject Area - Description</u></b><br />"));

				tr.Cells.Add(td);
				tblProof.Rows.Add(tr);

				for(Int32 intDSRow = 0; intDSRow < intRowCtr; intDSRow++){
					tr = new TableRow();

					td = new TableCell();
					td.Attributes["style"] = "font-size:10pt;font-family:arial;text-align:left;";
					td.Controls.Add(new LiteralControl("<br />&nbsp;&nbsp;&nbsp;<b>" + dsSubjects.Tables[0].Rows[intDSRow]["SUBJECT"].ToString() + "</b> - " + dsSubjects.Tables[0].Rows[intDSRow]["DESCR"].ToString()));

					tr.Cells.Add(td);
					tblProof.Rows.Add(tr);
				}
			}else{
				tr = new TableRow();

				td = new TableCell();
				td.Attributes["style"] = "text-align:left;";
				td.Controls.Add(new LiteralControl("<br /><br /><b>No records were found that match your criteria.</b>"));
				
				tr.Cells.Add(td);
				tblProof.Rows.Add(tr);
			}
			tr = new TableRow();

			td = new TableCell();
			td.Attributes["style"] = "font-size:10pt;font-family:arial;text-align:left;";
			td.Controls.Add(new LiteralControl("<br />&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick=\"history.back(1);\">Go Back</a>"));
			
			tr.Cells.Add(td);

			tblProof.Rows.Add(tr);

			container.Visible = false;
			panProof.Visible = true;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
