USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAreaOfStudyDegreeLocations]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 4/18/18
-- Description:	Returns a list of degree programs and their location by area of study
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetAreaOfStudyDegreeLocations] 
	@STRM varchar(4) = NULL,
	@AreaOfStudyID int = NULL,
	@ProgramID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
					FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					WHERE TERM_END_DT > GETDATE()
					ORDER BY TERM_END_DT)
					
	SELECT Title, LocationTitle, LocationWebsiteURL, COUNT(LocationTitle) AS LocationDegreeCount, 
		(SELECT COUNT(DISTINCT ProgramVersionID) 
		FROM CCSiCatalog_Legacy.dbo.Program AS Program2
		INNER JOIN AreaOfStudyProgram As AreaOfStudyProgram2
		ON AreaOfStudyProgram2.ProgramID = Program2.ProgramID
		WHERE AreaOfStudyProgram2.AreaOfStudyID = AreaOfStudy.AreaOfStudyID
		AND (Program2.ProgramBeginSTRM <= @STRM AND (Program2.ProgramEndSTRM = 'Z999' OR Program2.ProgramEndSTRM >= @STRM)) 
		AND (@ProgramID IS NULL OR (Program2.ProgramID = @ProgramID))
		AND Program2.PublishedProgram = 1)AS AreaOfStudyDegreeCount
	FROM AreaOfStudy 
	INNER JOIN AreaOfStudyProgram 
	ON AreaOfStudyProgram.AreaOfStudyID = AreaOfStudy.AreaOfStudyID 
	INNER JOIN Program 
	ON Program.ProgramID = AreaOfStudyProgram.ProgramID 
	INNER JOIN ProgramDegree 
	ON ProgramDegree.ProgramVersionID = Program.ProgramVersionID 
	INNER JOIN Degree 
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	INNER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	INNER JOIN OptionLocation 
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	INNER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 
	WHERE (ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM)) 
	AND (@AreaOfStudyID IS NULL OR (AreaOfStudy.AreaOfStudyID = @AreaOfStudyID))
	AND (@ProgramID IS NULL OR (Program.ProgramID = @ProgramID))
	AND PublishedProgram = 1
	GROUP BY AreaOfStudy.AreaOfStudyID, Title, LocationTitle, LocationWebsiteURL
	
END
GO
