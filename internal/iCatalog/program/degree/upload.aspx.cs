using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

namespace ICatalog.program.degree
{
	/// <summary>
	/// Summary description for upload.
	/// </summary>
	public partial class upload : System.Web.UI.Page
	{
		programData csProgram = new programData();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//check if the user is logged in
            HttpCookie cookie = Request.Cookies.Get("phatt2");
            if (cookie == null) {
                users user = new users(HttpContext.Current.User.Identity.Name);
            }

			panMsg.Visible = false;
			panView.Visible = false;
			panUpload.Visible = false;
			panDegrees.Visible = false;

			if(!IsPostBack){
				Int32 intStartYear = 2008, intEndYear = Convert.ToInt32(DateTime.Now.Year)+1;
				for(Int32 i = intStartYear; i <= intEndYear; i++){
					cboEffectiveYear.Items.Add(i.ToString());
				}
				DataSet dsDegWkstTitles = csProgram.GetDocumentTitles();
				for(Int32 i = 0; i < dsDegWkstTitles.Tables[0].Rows.Count; i++){
					cboDocumentTitle.Items.Add(dsDegWkstTitles.Tables[0].Rows[i]["DocumentTitle"].ToString());
				}
				panAddNew.Visible = false;
			}

			if(hidTodo.Value == "upload"){
				panUpload.Visible = true;
			}else if(hidTodo.Value == "degrees"){
				DataSet dsDegrees = csProgram.GetDegreesUsingWkst(hidDocumentTitle.Value);
				
				TableRow tr = new TableRow();
				TableCell td = new TableCell();
				td.ColumnSpan = 2;
				td.CssClass = "portletMain";
				td.Attributes["style"] = "padding-left:5px;padding-top:3px;padding-bottom:5px;";
				td.Controls.Add(new LiteralControl("The following degrees include the requirements/worksheet title " + hidDocumentTitle.Value + "."));
				tr.Controls.Add(td);
				tblDegrees.Rows.Add(tr);

				tr = new TableRow();
				td = new TableCell();
				td.CssClass = "portletSecondary";
				td.Attributes["style"] = "text-align:center;width:30%;font-weight:bold;padding-top:4px;padding-bottom:4px;";
				td.Controls.Add(new LiteralControl("<b>Degree Short Title</b>"));
				tr.Cells.Add(td);

				td = new TableCell();
				td.CssClass = "portletSecondary";
				td.Attributes["style"] = "text-align:center;width:70%;font-weight:bold;padding-top:4px;padding-bottom:4px;";
				td.Controls.Add(new LiteralControl("<b>Degree Long Title</b>"));
				tr.Cells.Add(td);
				tblDegrees.Rows.Add(tr);

				for(Int32 intDSRow = 0; intDSRow < dsDegrees.Tables[0].Rows.Count; intDSRow++){
					tr = new TableRow();
					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl(dsDegrees.Tables[0].Rows[intDSRow]["DegreeShortTitle"].ToString()));
					tr.Cells.Add(td);

					td = new TableCell();
					td.CssClass = "portletLight";
					td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
					td.Controls.Add(new LiteralControl(dsDegrees.Tables[0].Rows[intDSRow]["DegreeLongTitle"].ToString()));
					tr.Cells.Add(td);
					tblDegrees.Rows.Add(tr);
				}

				HtmlInputButton cmdBack = new HtmlInputButton();
				cmdBack.Attributes["style"] = "background:#ccccaa;border:1px solid #000000;width:75px;";
				cmdBack.Attributes["class"] = "small";
				cmdBack.Attributes["onclick"] = "history.back();";
				cmdBack.Value = "Back";
				tr = new TableRow();
				td = new TableCell();
				td.ColumnSpan = 2;
				td.CssClass = "portletMain";
				td.Attributes["style"] = "text-align:center;width:100%;padding:3px;";
				td.Controls.Add(cmdBack);
				tr.Cells.Add(td);
				tblDegrees.Rows.Add(tr);

				panDegrees.Visible = true;
			}else{
				if(hidTodo.Value == "delete"){
					//delete the file from the application server
					System.IO.File.Delete(Server.MapPath("..\\degworksheet\\") + hidFileName.Value);
                    //delete the file from the remote ccs-internet server
                    System.IO.File.Delete(System.IO.Path.Combine("\\\\ccs-internet\\websites\\OnlineCatalog\\degworksheet\\", hidFileName.Value));
                    //delete the file reference from the database
					csProgram.DeleteDegreeRequirementWorksheet(hidFileName.Value);
				}
				DataSet dsDegreeRequirementWorksheet = csProgram.GetDegreeRequirementWorksheetList();
				Int32 intDegreeRequirementWorksheetCount = dsDegreeRequirementWorksheet.Tables[0].Rows.Count;
				if(intDegreeRequirementWorksheetCount > 0){
					for(Int32 intDSRow = 0; intDSRow < intDegreeRequirementWorksheetCount; intDSRow++){
						String fileName = dsDegreeRequirementWorksheet.Tables[0].Rows[intDSRow]["FileName"].ToString();
                        String documentTitle = dsDegreeRequirementWorksheet.Tables[0].Rows[intDSRow]["DocumentTitle"].ToString();

						TableRow tr = new TableRow();
						TableCell td = new TableCell();
						td.Attributes["style"] = "text-align:center;";
						td.CssClass = "portletLight";
						td.Controls.Add(new LiteralControl(dsDegreeRequirementWorksheet.Tables[0].Rows[intDSRow]["EffectiveYear"].ToString()));
						tr.Cells.Add(td);

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Controls.Add(new LiteralControl(documentTitle));
						tr.Cells.Add(td);

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Controls.Add(new LiteralControl(fileName));
						tr.Cells.Add(td);

						td = new TableCell();
						td.CssClass = "portletLight";
						td.Attributes["style"] = "padding-left:5px;";
                        //view file on application server (internal only)
                        //td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('http://internal.spokane.edu/iCatalog/program/degworksheet/" + fileName + "','Requirements', 950, 950);\">View</a>"));
                        //view file on remote ccs-internet online catalog server (external)
                        td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"popupCenter('http://catalog.spokane.edu/degworksheet/" + fileName + "','Requirements', 950, 950);\">View</a>"));
                        if (Convert.ToInt32(dsDegreeRequirementWorksheet.Tables[0].Rows[intDSRow]["DegreeCount"]) == 0){
							td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete the file " + fileName + "?')){hidTodo.value='delete';hidFileName.value='" + fileName + "';document.frmDegree.submit();}\">Delete</a>"));
						}else{
							td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"document.frmDegree.hidTodo.value='degrees';document.frmDegree.hidFileName.value='" + fileName + "';document.frmDegree.hidDocumentTitle.value='" + documentTitle + "';document.frmDegree.submit();\">Degrees</a>"));
						}
						tr.Cells.Add(td);

						tblDisplay.Rows.Add(tr);
					}
				}else{
					TableRow tr = new TableRow();
					TableCell td = new TableCell();
					td.CssClass = "portletLight";
					td.ColumnSpan = 4;
					td.Controls.Add(new LiteralControl("No Degree Requirements/Worksheets currently exist."));
					tr.Cells.Add(td);
					tblDisplay.Rows.Add(tr);
				}
				panView.Visible = true;
			}
		}

		protected void cmdUpload_ServerClick(object sender, System.EventArgs e){
			if((file1.PostedFile != null) && (file1.PostedFile.ContentLength > 0)){
				String documentTitle = "";
				if(panSelectExisting.Visible){
					documentTitle = cboDocumentTitle.SelectedValue;
				}else if(panAddNew.Visible){
					documentTitle = txtDocumentTitle.Text;
				}
				String strFileName = System.IO.Path.GetFileName(file1.PostedFile.FileName);
                String strSaveLocation = Server.MapPath("..\\degworksheet\\") + strFileName;
                try {
                    //store file on application server
                    file1.PostedFile.SaveAs(strSaveLocation);
                    //copy file to remote ccs-internet server
                    System.IO.File.Copy(strSaveLocation, System.IO.Path.Combine("\\\\ccs-internet\\websites\\OnlineCatalog\\degworksheet\\", strFileName));
					Int16 intSuccess = csProgram.AddDegreeRequirementWorksheet(cboEffectiveYear.SelectedValue, strFileName, documentTitle);
					if(intSuccess == 2){
						lblMsg.Text = "Another file with the name '" + csProgram.GetDegreeRequirementWorksheetFileName(cboEffectiveYear.SelectedValue, documentTitle) + "' already exists for '" + cboEffectiveYear.SelectedValue + "' with the title '" + documentTitle + "'.<br />Please use the same file name to overwrite the existing file or choose a different year to create a new version.";
					}else if(intSuccess == 0){
						lblMsg.Text = "An error occurred while trying to save the selected file.";
					}else if(intSuccess == 1){
						txtDocumentTitle.Text = "";
						lblMsg.Text = "The file " + strFileName + " has been uploaded.";
					}
				}catch (Exception ex){
					lblMsg.Text = "An error occurred while trying to save the selected file.<br /> " + ex.Message;
				}
                
			}else{
				lblMsg.Text = "Please select a file to upload.";
			}
			panMsg.Visible = true;
		}

		private void lnkSelectExisting_Click(object sender, System.EventArgs e){
			panSelectExisting.Visible = true;
			panAddNew.Visible = false;
		}

		private void lnkAddNew_Click(object sender, System.EventArgs e){
			panAddNew.Visible = true;
			panSelectExisting.Visible = false;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.lnkAddNew.Click += new EventHandler(lnkAddNew_Click);
			this.lnkSelectExisting.Click += new EventHandler(lnkSelectExisting_Click);
		}
		#endregion
	}
}
