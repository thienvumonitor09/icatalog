USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionLocations]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/16/17
-- Description:	Returns program option locations
--		 INPUT: OptionID - Program option primary key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionLocations] 
	@OptionID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT OptionLocation.LocationID, LocationTitle, LocationWebsiteURL  
    FROM OptionLocation 
    INNER JOIN Location 
    ON Location.LocationID = OptionLocation.LocationID 
    WHERE OptionID = @OptionID
    ORDER BY LocationTitle
    
END

GO
