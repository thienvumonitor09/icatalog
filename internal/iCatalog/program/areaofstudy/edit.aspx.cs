﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ICatalog._phatt3_classes;

public partial class program_areaofstudy_edit : System.Web.UI.Page {

    programData csProgram = new programData();

    protected void Page_Load(object sender, System.EventArgs e) {
        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        panError.Visible = false;
        panEdit.Visible = false;
        panDisplay.Visible = false;
        panConfirm.Visible = false;

        if (!IsPostBack) {

            DataSet dsAreasOfStudy = csProgram.GetAreasOfStudy("");
            Int32 intAreaOfStudyCount = dsAreasOfStudy.Tables[0].Rows.Count;

            TableRow tr = new TableRow();

            TableCell td = new TableCell();
            td.Attributes["style"] = "text-align:center;width:125px;font-weight:bold;padding-top:4px;padding-bottom:4px;width:100px;";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("Offered by"));

            tr.Cells.Add(td);

            td = new TableCell();
            td.Attributes["style"] = "text-align:center;width:400px;font-weight:bold;padding-top:4px;padding-bottom:4px;";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("Area of Study Title"));

            tr.Cells.Add(td);

            td = new TableCell();
            td.Attributes["style"] = "width:68px;text-align:center";
            td.CssClass = "portletSecondary";
            td.Controls.Add(new LiteralControl("<a href=\"add.aspx\">Add New</a>"));

            tr.Cells.Add(td);
            tblDisplay.Rows.Add(tr);

            if (intAreaOfStudyCount > 0) {

                for (Int32 intDSRow = 0; intDSRow < intAreaOfStudyCount; intDSRow++) {
                    Int32 areaOfStudyID = Convert.ToInt32(dsAreasOfStudy.Tables[0].Rows[intDSRow]["AreaOfStudyID"]);
                    String areaOfStudyTitle = dsAreasOfStudy.Tables[0].Rows[intDSRow]["Title"].ToString();
                    String collegeShortTitle = dsAreasOfStudy.Tables[0].Rows[intDSRow]["CollegeShortTitle"].ToString();

                    tr = new TableRow();

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
                    td.Controls.Add(new LiteralControl(collegeShortTitle));

                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "padding-left:5px;padding-right:5px;padding-top:4px;padding-bottom:4px;";
                    td.Controls.Add(new LiteralControl(areaOfStudyTitle));

                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "portletLight";
                    td.Attributes["style"] = "text-align:left;vertical-align:top;padding-top:4px;padding-bottom:4px;padding-left:6px;";
                    td.Controls.Add(new LiteralControl("<a href=\"#\" onclick=\"document.getElementById('hidTodo').value='edit';document.getElementById('hidAreaOfStudyID').value='" + areaOfStudyID + "';document.frmAreaOfStudy.submit();\">Edit</a>"));
                    if (Convert.ToInt32(dsAreasOfStudy.Tables[0].Rows[intDSRow]["ProgramCount"]) == 0) {
                        td.Controls.Add(new LiteralControl(" | <a href=\"#\" onclick=\"if(confirm('Are you sure you want to delete " + areaOfStudyTitle.Replace("'", "\\'") + " offered by " + collegeShortTitle + "?')){document.getElementById('hidTodo').value='delete';document.getElementById('hidAreaOfStudyID').value='" + areaOfStudyID + "';document.frmAreaOfStudy.submit();}\">Delete</a>"));
                    }
                    tr.Cells.Add(td);
                    tblDisplay.Rows.Add(tr);
                }
            } else {
                tr = new TableRow();

                td = new TableCell();
                td.ColumnSpan = 3;
                td.Attributes["style"] = "width:100%;padding:4px;";
                td.CssClass = "portletLight";
                td.Controls.Add(new LiteralControl("No areas of study currently exist."));

                tr.Cells.Add(td);
                tblDisplay.Rows.Add(tr);
            }

            panDisplay.Visible = true;

        } else if (hidTodo.Value == "edit") {

            DataSet dsAreaOfStudy = csProgram.GetAreaOfStudy(Convert.ToInt32(hidAreaOfStudyID.Value));
            if (dsAreaOfStudy.Tables[0].Rows.Count > 0) {
                txtAreaOfStudyTitle.Text = dsAreaOfStudy.Tables[0].Rows[0]["Title"].ToString();
                txtWebPageURL.Text = dsAreaOfStudy.Tables[0].Rows[0]["WebsiteURL"].ToString();

                if (hidCollegeChanged.Value == "true") {
                    optOfferedAt.SelectedValue = Request.Form["optOfferedAt"];
                    GetPathways();
                    hidPathwayList.Value = "";
                } else {
                    optOfferedAt.SelectedValue = dsAreaOfStudy.Tables[0].Rows[0]["Institution"].ToString();
                    GetPathways();

                    //get area of study pathways
                    ArrayList lstPathways = new ArrayList();
                    DataSet dsAreaOfStudyPathways = csProgram.GetAreaOfStudyPathways(Convert.ToInt32(hidAreaOfStudyID.Value));
                    Int32 areaOfStudyPathwayCount = dsAreaOfStudyPathways.Tables[0].Rows.Count;
                    if (areaOfStudyPathwayCount > 0) {
                        for (Int32 intDSRow = 0; intDSRow < areaOfStudyPathwayCount; intDSRow++) {
                            String pathwayID = dsAreaOfStudyPathways.Tables[0].Rows[intDSRow]["PathwayID"].ToString();
                            lstPathways.Add(dsAreaOfStudyPathways.Tables[0].Rows[intDSRow]["PathwayTitle"].ToString());
                            if (dsAreaOfStudyPathways.Tables[0].Rows[intDSRow]["PrimaryPathway"].ToString() == "1") { //make sure the guided pathway doesn't already exist
                                cboPathway.SelectedValue = pathwayID;
                            } else {
                                hidPathwayList.Value += "," + pathwayID;
                                HtmlInputCheckBox chkPathway = (HtmlInputCheckBox)Page.FindControl("chkPathway" + pathwayID);
                                chkPathway.Checked = true;
                            }
                        }
                        if (hidPathwayList.Value.IndexOf(",") == 0) {
                            hidPathwayList.Value = hidPathwayList.Value.Substring(1, hidPathwayList.Value.Length - 1);
                        }
                    }
                }
                panEdit.Visible = true;
            }

            hidTodo.Value = "";

        } else if (hidTodo.Value == "delete") {
            csProgram.DeleteAreaOfStudy(Convert.ToInt32(hidAreaOfStudyID.Value));
            Response.Redirect("edit.aspx");
        }
    }

    protected void cmdSubmit_Click(object sender, System.EventArgs e) {
        Int32 areaOfStudyID = Convert.ToInt32(Request.Form["hidAreaOfStudyID"]);
        String institution = Request.Form["optOfferedAt"].ToString();
        String areaOfStudyTitle = Request.Form["txtAreaOfStudyTitle"].ToString();
        String pathwayID = Request.Form["cboPathway"].ToString();
        String webPageURL = Request.Form["txtWebPageURL"].ToString();
        String strAddPathways = Request.Form["hidPathwayList"];

        Int16 intSuccess = csProgram.EditAreaOfStudy(areaOfStudyID, institution, pathwayID, areaOfStudyTitle, webPageURL);

        if (intSuccess == 0) {

            lblErrorMsg.Text = "Error: The update failed.";
            GetPathways();
            cboPathway.SelectedValue = pathwayID;
            String[] aryPathwayList = strAddPathways.Split(',');
            for (Int16 i = 0; i < aryPathwayList.Length; i++) {
                if (pathwayID != aryPathwayList[i]) {
                    ((HtmlInputCheckBox)Page.FindControl("chkPathway" + aryPathwayList[i])).Checked = true;
                }
            }
            panError.Visible = true;
            panEdit.Visible = true;

        } else if (intSuccess == 2) {

            lblErrorMsg.Text = "Error: The area of study entered already exists.";
            GetPathways();
            cboPathway.SelectedValue = pathwayID;
            String[] aryPathwayList = strAddPathways.Split(',');
            for (Int16 i = 0; i < aryPathwayList.Length; i++) {
                if (pathwayID != aryPathwayList[i]) {
                    ((HtmlInputCheckBox)Page.FindControl("chkPathway" + aryPathwayList[i])).Checked = true;
                }
            }
            panError.Visible = true;
            panEdit.Visible = true;

        } else if (intSuccess == 1) {

            //delete and add (update) optional area of study pathways
            csProgram.DeleteAdditionalAreaOfStudyPathways(areaOfStudyID);
            if (strAddPathways != "") {
                String[] aryPathwayList = strAddPathways.Split(',');
                for (Int16 i = 0; i < aryPathwayList.Length; i++) {
                    if (pathwayID != aryPathwayList[i]) {
                        csProgram.AddAreaOfStudyPathway(areaOfStudyID, Convert.ToInt32(aryPathwayList[i]));
                    }
                }
            }

            //display area of study selections
            if(institution == "WA171") {
                lblOfferedBy.Text = "SCC";
            } else if(institution == "WA172") {
                lblOfferedBy.Text = "SFCC";
            }
            lblAreaOfStudyTitle.Text = areaOfStudyTitle;
            lblPathway.Text = cboPathway.Items.FindByValue(pathwayID).Text;
            lblWebPageURL.Text = webPageURL;

            //display optional pathways selected
            DataSet dsPathways = csProgram.GetAreaOfStudyPathways(areaOfStudyID);
            lblAddPathways.Text = "";
            for (Int32 intDSRow = 0; intDSRow < dsPathways.Tables[0].Rows.Count; intDSRow++) {
                if (Convert.ToByte(dsPathways.Tables[0].Rows[intDSRow]["PrimaryPathway"]) == 0) {
                    lblAddPathways.Text += "; " + dsPathways.Tables[0].Rows[intDSRow]["PathwayTitle"].ToString();
                }
            }
            if (lblAddPathways.Text.IndexOf(";") == 0) {
                lblAddPathways.Text = lblAddPathways.Text.Substring(1, lblAddPathways.Text.Length - 1);
            }

            //display confirmation panel
            panConfirm.Visible = true;
        }
    }

    public void GetPathways() {
        //get pathways
        DataSet dsPathways = csProgram.GetPathways(optOfferedAt.SelectedValue);
        cboPathway.Items.Clear();
        panPathway.Controls.Clear();
        for (Int32 i = 0; i < dsPathways.Tables[0].Rows.Count; i++) {
            String pathwayTitle = dsPathways.Tables[0].Rows[i]["PathwayTitle"].ToString();
            String pathwayID = dsPathways.Tables[0].Rows[i]["PathwayID"].ToString();

            cboPathway.Items.Add(new ListItem(pathwayTitle, pathwayID));

            HtmlInputCheckBox chkPathway = new HtmlInputCheckBox();
            chkPathway.Value = pathwayID;
            chkPathway.ID = "chkPathway" + pathwayID;
            chkPathway.Name = "chkPathway";

            chkPathway.Attributes["onclick"] = "addPathway('" + chkPathway.ID + "','" + pathwayID + "');";
            panPathway.Controls.Add(new LiteralControl("<div>"));
            panPathway.Controls.Add(chkPathway);
            panPathway.Controls.Add(new LiteralControl("&nbsp;" + pathwayTitle + "</div>"));
        }
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e) {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
    }
    #endregion
}
