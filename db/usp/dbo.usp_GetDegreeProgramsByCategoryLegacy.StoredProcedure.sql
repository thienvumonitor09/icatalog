USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDegreeProgramsByCategoryLegacy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 12/11/17
-- Description:	Returns a degree list by term, location, degree, category, course subject, and course catalog number
--		 INPUT: STRM - Term code
--				LocationID (optional) - Location primary key 
--				DegreeID (optional) - Degree primary key 
--				CategoryID (optional) - Category primary key 
--				SUBJECT (optional) - Course subject area 
--				CATALOG_NBR (optional) - Course catalog number 
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDegreeProgramsByCategoryLegacy] 
	@STRM varchar(4) = NULL,
	@LocationID int = NULL,
	@DegreeID int = NULL,
	@CategoryID int = NULL,
	@SUBJECT varchar(5) = NULL,
	@CATALOG_NBR varchar(3) = NULL,
	@GlobalSearch varchar(150) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @SQL nvarchar(max),
			@Parameters nvarchar(4000)
			
	IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
							FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
							WHERE TERM_END_DT > GETDATE()
							ORDER BY TERM_END_DT)

    SET @SQL = 'SELECT CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, Degree.DegreeID, Degree.DocumentTitle, DegreeType
				FROM ProgramDegree 
				INNER JOIN Program 
				ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
				INNER JOIN CareerPlanningGuide 
				ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID
				INNER JOIN College 
				ON College.CollegeID = Program.CollegeID
				INNER JOIN ProgramCategory 
				ON ProgramCategory.ProgramVersionID = Program.ProgramVersionID 
				INNER JOIN Category 
				ON Category.CategoryID = ProgramCategory.CategoryID 
				INNER JOIN ProgramDegreeOption 
				ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
				INNER JOIN Degree 
				ON Degree.DegreeID = ProgramDegree.DegreeID 
				LEFT OUTER JOIN OptionPrerequisite 
				ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID 
				LEFT OUTER JOIN OptionElectiveGroup 
				ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID 
				LEFT OUTER JOIN ElectiveGroupCourse 
				ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
				LEFT OUTER JOIN OptionCourse 
				ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
				LEFT OUTER JOIN Course 
				ON Course.CourseOffering = OptionPrerequisite.CourseOffering 
				OR Course.CourseOffering = ElectiveGroupCourse.CourseOffering 
				OR Course.CourseOffering = OptionCourse.CourseOffering 
				LEFT OUTER JOIN OptionLocation 
				ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
				LEFT OUTER JOIN Location 
				ON Location.LocationID = OptionLocation.LocationID 
				WHERE ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = ''Z999'' OR ProgramEndSTRM >= @STRM) 
				AND PublishedProgram = 1 '
				
	IF @LocationID IS NOT NULL
		SET @SQL += 'AND OptionLocation.LocationID = @LocationID '
		
	IF @DegreeID IS NOT NULL
		SET @SQL += 'AND ProgramDegree.DegreeID = @DegreeID '
		
	IF @CategoryID IS NOT NULL
		SET @SQL += 'AND ProgramCategory.CategoryID = @CategoryID '
		
	IF @SUBJECT IS NOT NULL
		SET @SQL += 'AND ((Course.SUBJECT = @SUBJECT OR Course.SUBJECT = @SUBJECT + ''&'') 
					 AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
					 OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM))) '
					 
	IF @CATALOG_NBR IS NOT NULL
		SET @SQL += 'AND (Course.CATALOG_NBR = @CATALOG_NBR
					 AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
					 OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM))) '
	
	IF @GlobalSearch IS NOT NULL
		SET @SQL += 'AND ((LocationTitle LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (CategoryTitle LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (ProgramTitle LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (ProgramDescription LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (OptionTitle LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (DegreeShortTitle LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (DegreeLongTitle LIKE ''%'' + @GlobalSearch + ''%'')
					 OR (DegreeType LIKE ''%'' + @GlobalSearch + ''%'')
					 OR ((Course.COURSE_TITLE_LONG LIKE ''%'' + @GlobalSearch + ''%'') 
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR ((Course.DESCR LIKE ''%'' + @GlobalSearch + ''%'') 
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR ((Course.SUBJECT LIKE ''%'' + @GlobalSearch + ''%'') 
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR ((Course.CATALOG_NBR LIKE ''%'' + @GlobalSearch + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR (((Course.SUBJECT + LTRIM(Course.CATALOG_NBR)) LIKE + ''%'' + @GlobalSearch + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR (((Course.SUBJECT + Course.CATALOG_NBR) LIKE + ''%'' + @GlobalSearch + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))
					 OR (((Course.SUBJECT + '' '' + Course.CATALOG_NBR) LIKE + ''%'' + @GlobalSearch + ''%'')
						AND ((CourseBeginSTRM >= ProgramBeginSTRM AND CourseBeginSTRM <= ProgramEndSTRM) 
						OR (CourseBeginSTRM <= ProgramBeginSTRM AND CourseEndSTRM >= ProgramBeginSTRM)))) '
						
    SET @SQL += 'GROUP BY Program.ProgramVersionID, CollegeShortTitle, CollegeLongTitle, Category.CategoryID, CategoryTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID, Degree.DegreeID, Degree.DocumentTitle, DegreeType 
				 ORDER BY CategoryTitle, DegreeLongTitle, OptionTitle, ProgramTitle, CollegeShortTitle;'
    
    SET @Parameters = '@STRM varchar(4),
					   @LocationID int,
					   @DegreeID int,
					   @CategoryID int,
					   @SUBJECT varchar(5),
					   @CATALOG_NBR varchar(3),
					   @GlobalSearch varchar(150)'
						 
	EXEC sp_executesql @SQL, @Parameters, @STRM, @LocationID, @DegreeID, @CategoryID, @SUBJECT, @CATALOG_NBR, @GlobalSearch
	
END

GO
