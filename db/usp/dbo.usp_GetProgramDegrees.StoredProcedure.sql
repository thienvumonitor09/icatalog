USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramDegrees]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/9/17
-- Description:	Returns all degrees offered in a program
--		 INPUT: ProgramVersionID - Program version primary key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramDegrees]
	@ProgramVersionID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DegreeShortTitle, ProgramDegreeID
    FROM ProgramDegree 
    INNER JOIN Degree
    ON Degree.DegreeID = ProgramDegree.DegreeID
    INNER JOIN Program
    ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID
    WHERE ProgramDegree.ProgramVersionID = @ProgramVersionID
    ORDER BY DegreeShortTitle;
END

GO
