<%@ Page language="c#" Inherits="ICatalog.program.fee.programfees" CodeFile="programfees.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Program Fees</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
        <script type="text/javascript" src="../../_phatt3_src_files/trim.js"></script>
        <script type="text/javascript">
        <!--			
			function validate(){		
				var txtBookMinimum = trim(document.getElementById("txtBookMinimum").value);
				var txtBookMaximum = trim(document.getElementById("txtBookMaximum").value);
				var txtSuppliesMinimum = trim(document.getElementById("txtSuppliesMinimum").value);
				var txtSuppliesMaximum = trim(document.getElementById("txtSuppliesMaximum").value);
				var txtMiscMinimum = trim(document.getElementById("txtMiscMinimum").value);
				var txtMiscMaximum = trim(document.getElementById("txtMiscMaximum").value);

				if(document.getElementById("hidFocus").value != "cmdCancel"){
					if(txtBookMinimum != "" && isNaN(txtBookMinimum)){
						alert("Please enter the minimum book fee in the 0.00 format.");
						document.getElementById("txtBookMinimum").select();
						return false;
					}else if(txtBookMaximum != "" && isNaN(txtBookMaximum)){
						alert("Please enter the maximum book fee in the 0.00 format.");
						document.getElementById("txtBookMaximum").select();
						return false;
					}else if(txtBookMaximum != "" && txtBookMinimum == ""){
						alert("Please enter the minimum book fee.");
						document.getElementById("txtBookMinimum").select();
						return false;
					}else if(txtBookMinimum != "" && txtBookMaximum == ""){
						alert("Please enter the maximum book fee.");
						document.getElementById("txtBookMaximum").select();
						return false;
					}else if((txtBookMinimum != "" && txtBookMaximum != "") && (eval(txtBookMinimum) > eval(txtBookMaximum))){
						alert("The minimum book fee cannot be greater than the maximum book fee.");
						document.getElementById("txtBookMinimum").select();
						return false;
					}else if(txtSuppliesMinimum != "" && isNaN(txtSuppliesMinimum)){
						alert("Please enter the minimum supplies and equipment fee in the 0.00 format.");
						document.getElementById("txtSuppliesMinimum").select();
						return false;
					}else if(txtSuppliesMaximum != "" && isNaN(txtSuppliesMaximum)){
						alert("Please enter the maximum supplies and equipment fee in the 0.00 format.");
						document.getElementById("txtSuppliesMaximum").select();
						return false;
					}else if(txtSuppliesMaximum != "" && txtSuppliesMinimum == ""){
						alert("Please enter the minimum supplies and equipment fee.");
						document.getElementById("txtSuppliesMinimum").select();
						return false;
					}else if(txtSuppliesMinimum != "" && txtSuppliesMaximum == ""){
						alert("Please enter the maximum supplies and equipment fee.");
						document.getElementById("txtSuppliesMaximum").select();
						return false;
					}else if((txtSuppliesMinimum != "" && txtSuppliesMaximum != "") && (eval(txtSuppliesMinimum) > eval(txtSuppliesMaximum))){
						alert("The minimum supplies and equipment fee cannot be geater than the maximum supplies and equipment fee.");
						document.getElementById("txtSuppliesMinimum").select();
						return false;
					}else if(txtMiscMinimum != "" && isNaN(txtMiscMinimum)){
						alert("Please enter the minimum miscellaneous fee in the 0.00 format.");
						document.getElementById("txtMiscMinimum").select();
						return false;
					}else if(txtMiscMaximum != "" && isNaN(txtMiscMaximum)){
						alert("Please enter the maximum miscellaneous fee in the 0.00 format.");
						document.getElementById("txtMiscMaximum").select();
						return false;
					}else if(txtMiscMaximum != "" && txtMiscMinimum == ""){
						alert("Please enter the minimum miscellaneous fee.");
						document.getElementById("txtMiscMinimum").select();
						return false;
					}else if(txtMiscMinimum != "" && txtMiscMaximum == ""){
						alert("Please enter the maximum miscellaneous fee.");
						document.getElementById("txtMiscMaximum").select();
						return false;
					}else if((txtMiscMinimum != "" && txtMiscMaximum != "") && (eval(txtMiscMinimum) > eval(txtMiscMaximum))){
						alert("The minimum miscellaneous fee cannot be greater than the maximum miscellaneous fee.");
						document.getElementById("txtMiscMinimum").select();
						return false;
					}else if(txtBookMinimum == "" && txtSuppliesMinimum == "" && txtMiscMinimum == "" && trim(document.getElementById("txtNote").value) == ""){
						alert("Please enter a fee or note before submitting.");
						document.getElementById("txtBookMinimum").select();
						return false;
					}else{
						document.getElementById("hidTodo").value = "edit";
						return true;
					}
				}else{
					document.getElementById("hidTodo").value = "edit";
					return true;
				}
			}
			
			function copyValue(value, element){
				if(value != "" && trim(element.value) == ""){
					element.value = value;
				}
			}
		//-->
		</script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmProgramFee" runat="server">
					<input type="hidden" id="hidProgramVersionID" runat="server" />
					<input type="hidden" id="hidTodo" runat="server" />
					<input type="hidden" id="hidLtr" runat="server" />
					<input type="hidden" id="hidFocus" runat="server" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Program Fees</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
																	<asp:panel runat="server" id="panDisplay">
																		<table style="width:100%;" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="portletDark" style="width:100%;">
                                                                                    <asp:Table runat="server" ID="tblSearch" style="width:100%;" cellspacing="0" cellpadding="0">
                                                                                        <asp:tablerow>
																				            <asp:tablecell style="width:100%;font-weight:bold;padding:10px;" cssclass="portletMain">
																								Offered by:&nbsp;
																								<asp:dropdownlist id="cboOfferedAt" cssclass="small" runat="server" autopostback="true">
																									<asp:listitem value="">ALL</asp:listitem>
																								</asp:dropdownlist>&nbsp;&nbsp;
																								Term:&nbsp;
																								<asp:dropdownlist id="cboTerm" cssclass="small" runat="server" autopostback="true">
																									<asp:listitem value="">ALL</asp:listitem>
																								</asp:dropdownlist>
																							</asp:tablecell>
																			            </asp:tablerow>
                                                                                    </asp:Table>
																					<asp:table runat="server" id="tblAlpha" style="width:100%;" cellspacing="0" cellpadding="0"></asp:table>
																					<asp:table runat="server" id="tblDisplay" style="width:100%;" cellspacing="1" cellpadding="2"></asp:table>
																				</td>
																			</tr>
																		</table>
																	</asp:panel>
                                                                    <asp:panel runat="server" id="panAdd">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <!-- Error Message -->
                                                                            <asp:panel runat="server" id="panError">
                                                                                <tr>
                                                                                    <td colspan="2" class="portletMain" style="color:red;padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                        <asp:label runat="server" id="lblErrorMsg"></asp:label>
                                                                                    </td>
                                                                                </tr>
                                                                            </asp:panel>
                                                                            <!-- Program Title -->
                                                                            <tr style="height:20px;">
																				<td class="portletSecondary" style="width:32%;text-align:right;">
																					<b>Program Title:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="width:68%;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:label runat="server" id="lblProgram" cssclass="small"></asp:label>
																				</td>
                                                                            </tr>
                                                                            <!-- Offered At -->
                                                                            <tr style="height:20px;">
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Offered by:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:label id="lblOfferedAt" runat="server"></asp:label>
																				</td>
                                                                            </tr>
                                                                            <!-- TERM -->
																			<tr style="height:20px;">
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Term:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<b>Begin</b>&nbsp;&nbsp;<asp:label runat="server" id="lblBeginTerm"></asp:label>
																					&nbsp;&nbsp;&nbsp;<b>End</b>&nbsp;&nbsp;<asp:label runat="server" id="lblEndTerm"></asp:label>
																				</td>
																			</tr>
																			<!-- Books -->
																			<tr>
																				<td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;">
																					<b>Books:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					$&nbsp;<input type="text" runat="server" id="txtBookMinimum" onblur="copyValue(this.value, document.getElementById('txtBookMaximum'));" maxlength="8" size="6" class="small" />&nbsp;Min
																					&nbsp;$&nbsp;<input type="text" runat="server" id="txtBookMaximum" onfocus="this.select();" maxlength="8" size="6" class="small" />&nbsp;Max
																				</td>
																			</tr>
																			<!-- Supplies and Equipment -->
																			<tr>
																				<td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;">
																					<b>Supplies and Equipment:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					$&nbsp;<input type="text" runat="server" id="txtSuppliesMinimum" onblur="copyValue(this.value, document.getElementById('txtSuppliesMaximum'));" maxlength="8" size="6" class="small" />&nbsp;Min
																					&nbsp;$&nbsp;<input type="text" runat="server" id="txtSuppliesMaximum" onfocus="this.select();" maxlength="8" size="6" class="small" />&nbsp;Max
																				</td>
																			</tr>
																			<!-- Misc. Fee -->
																			<tr>
																				<td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;">
																					<b>Misc. Fee:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					$&nbsp;<input type="text" runat="server" id="txtMiscMinimum" onblur="copyValue(this.value, document.getElementById('txtMiscMaximum'));" maxlength="8" size="6" class="small" />&nbsp;Min
																					&nbsp;$&nbsp;<input type="text" runat="server" id="txtMiscMaximum" onfocus="this.select();" maxlength="8" size="6" class="small" />&nbsp;Max
																				</td>
																			</tr>
																			<!-- Misc. Fee Footnote -->
																			<tr>
																				<td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;">
																					<b>Misc. Fee Note:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:textbox runat="server" id="txtNote" style="width:375px;height:13px;" maxlength="125" cssclass="small"></asp:textbox>
																				</td>
																			</tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
																					<asp:button id="cmdCancel" runat="server" text="Back" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:75px;" OnFocus="document.frmProgramFee.hidFocus.value='cmdCancel';" onclick="cmdCancel_Click"></asp:button>
																					&nbsp;<input type="button" id="cmdDelete" runat="server" value="Delete" class="small" style="background:#ccccaa;border:1px solid #000000;width:75px;" onclick="if(confirm('Are you sure you want to delete the current program fees?')){document.frmProgramFee.hidTodo.value='delete';document.frmProgramFee.submit();}" />
															                        &nbsp;<asp:button id="cmdSave" runat="server" text="Save" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:75px;" OnFocus="document.frmProgramFee.hidFocus.value='cmdSubmit';" OnClientClick="return validate();" onclick="cmdSave_Click"></asp:button>
														                        </td>
													                        </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                    <asp:panel runat="server" id="panConfirm">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <tr>
                                                                                <td class="portletMain" colspan="2" style="padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                    The following program fees have been successfully added.
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Program Title -->
                                                                            <tr>
																				<td class="portletSecondary" style="width:32%;text-align:right;">
																					<b>Program Title:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="width:68%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:label runat="server" id="lblProgramTitle" cssclass="small"></asp:label>
																				</td>
                                                                            </tr>
                                                                            <!-- Offered At -->
                                                                            <tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Offered by:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:label id="lblOfferedAt2" runat="server"></asp:label>
																				</td>
                                                                            </tr>
                                                                            <!-- TERM -->
																			<tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Term:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<b>Begin</b>&nbsp;&nbsp;<asp:label runat="server" id="lblBeginTerm2"></asp:label>
																					&nbsp;&nbsp;&nbsp;<b>End</b>&nbsp;&nbsp;<asp:label runat="server" id="lblEndTerm2"></asp:label>
																				</td>
																			</tr>
																			<!-- Books -->
																			<tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Books:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:label runat="server" id="lblBookFee" cssclass="small"></asp:label>
																				</td>
																			</tr>
																			<!-- Supplies and Equipment -->
																			<tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Supplies and Equipment:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:label runat="server" id="lblSupAndEquipFee" cssclass="small"></asp:label>
																				</td>
																			</tr>
																			<!-- Misc. Fee -->
																			<tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Misc. Fee:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:label runat="server" id="lblMiscFee" cssclass="small"></asp:label>
																				</td>
																			</tr>
																			<!-- Misc. Fee Footnote -->
																			<tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Misc. Fee Note:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:label runat="server" id="lblNote" cssclass="small"></asp:label>
																				</td>
																			</tr>
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;">
															                        <asp:button runat="server" id="cmdOK" text="OK" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="cmdOK_Click"></asp:button>
														                        </td>
													                        </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <DIV class="clearer"></DIV>
        </asp:panel>
    </body>
</html>