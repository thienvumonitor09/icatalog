USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDegreesByAreaOfStudy]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 12/1/17
-- Description:	Returns General Degrees (Without Program Outlines) by Area Of Study
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDegreesByAreaOfStudy]
	@STRM varchar(4) = NULL,
	@DegreeID int = NULL,
	@AlphaCharacter char = NULL,
	@AreaOfStudyID int = NULL,
	@GlobalSearch varchar(150) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @STRM IS NULL
		SET @STRM = (SELECT TOP 1(STRM)
							FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
							WHERE TERM_END_DT > GETDATE()
							ORDER BY TERM_END_DT)

    SELECT DISTINCT AreaOfStudy.Title, AreaOfStudy.WebsiteURL, Degree.DegreeID, DegreeShortTitle, DegreeLongTitle, Degree.DocumentTitle, DegreeType
	FROM AreaOfStudy
	INNER JOIN AreaOfStudyProgram
	ON AreaOfStudyProgram.AreaOfStudyID = AreaOfStudy.AreaOfStudyID
	LEFT OUTER JOIN Degree
	ON Degree.DegreeID = AreaOfStudyProgram.DegreeID
	LEFT OUTER JOIN DegreeRequirementWorksheet
    ON DegreeRequirementWorksheet.DocumentTitle = Degree.DocumentTitle
    LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable
    ON STRM = @STRM
	WHERE (((vw_TermTable.DESCR LIKE '%Fall%' AND EffectiveYear = YEAR(TERM_BEGIN_DT)) OR (vw_TermTable.DESCR NOT LIKE '%Fall%' AND EffectiveYear = YEAR(DATEADD(Year, -1, TERM_BEGIN_DT)))) AND (Degree.DocumentTitle IS NOT NULL AND Degree.DocumentTitle <> ''))
	AND (@DegreeID IS NULL OR Degree.DegreeID = @DegreeID)
	AND	(@AlphaCharacter IS NULL OR Title LIKE @AlphaCharacter + '%')
	AND (@AreaOfStudyID IS NULL OR AreaOfSTudyProgram.AreaOfStudyID = @AreaOfStudyID)
	AND (@GlobalSearch IS NULL OR 
		((Title LIKE '%' + @GlobalSearch + '%') 
		OR (DegreeShortTitle LIKE '%' + @GlobalSearch + '%') 
		OR (DegreeLongTitle LIKE '%' + @GlobalSearch + '%')
		OR (DegreeType LIKE '%' + @GlobalSearch + '%')))
	ORDER BY AreaOfStudy.Title, DegreeLongTitle		
				
END
GO
