<%@ Reference Page="~/program/edit.aspx" %>
<%@ Page language="c#" Inherits="ICatalog.program.grantpermissions" CodeFile="grantpermissions.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Grant Permissions</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
    </head>
    <body>
        <form id="frmProgramPermissions" method="post" runat="server">
            <input type="hidden" id="hidError" runat="server" />
		    <asp:panel id="container" runat="server">
		        <asp:panel id="header" runat="server"><uc1:header id="Header1" runat="server"></uc1:header></asp:panel>
                <asp:panel id="sidemenu" runat="server"><uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu></asp:panel>
                <asp:panel id="content" runat="server">
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
				        <tr>
				            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
					                    <td style="background-color:#000000;">
						                    <table style="width:100%;" cellpadding="1" cellspacing="1">
							                    <tr>
								                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;" class="portletHeader">
									                    <table style="width:100%;" cellpadding="0" cellspacing="0">
										                    <tr>
											                    <td class="portletHeader" style="width:100%;">&nbsp;Grant Permissions</td>
										                    </tr>
									                    </table>
								                    </td>
							                    </tr>
							                    <tr>
								                    <td class="portletMain" style="width:100%">
									                    <table style="width:100%;" cellspacing="0" cellpadding="0">
										                    <tr>
											                    <td class="portletDark" style="width:100%;">
											                        <asp:panel id="panAdd" runat="server">
												                        <table cellspacing="1" cellpadding="2" style="width:100%;">
													                        <tr style="height:18px;">
														                        <td class="portletSecondary" style="width:34%;text-align:right;padding-top:5px;padding-bottom:5px;">
															                        <b>Name:</b>&nbsp;
														                        </td>
														                        <td class="portletLight" style="width:66%;padding-left:5px;">
															                        <asp:label id="lblName1" runat="server" cssclass="small"></asp:label>
														                        </td>
													                        </tr>
													                        <tr>
														                        <td class="portletSecondary" style="text-align:right;">
															                        <b>ctcLink ID:</b>&nbsp;
														                        </td>
														                        <td class="portletLight" style="padding-top:4px;padding-bottom:4px;padding-left:5px;">
														                            <asp:textbox id="txtEmployeeID" runat="server" cssclass="small" style="width:60px;" autopostback="true" maxlength="9"></asp:textbox>
														                        </td>
													                        </tr>
													                        <tr>
														                        <td class="portletSecondary" style="text-align:right;">
															                        <b>Permissions:</b>&nbsp;
														                        </td>
														                        <td class="portletLight" style="padding-top:5px;padding-bottom:5px;padding-left:5px;">
														                            <asp:dropdownlist id="cboPermissions1" runat="server" cssclass="small">
														                                <asp:listitem value="1">Admin</asp:listitem>
														                                <asp:listitem value="2">Add/Edit/Delete</asp:listitem>
																						<asp:listitem value="3">Read Only</asp:listitem>
														                            </asp:dropdownlist>
														                        </td>
													                        </tr>
													                        <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                        &nbsp;<input type="button" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" id="cmdCancel1" value="Cancel" onclick="location.href='grantpermissions.aspx';" />
															                        &nbsp;<asp:button id="cmdAdd" runat="server" text="Add Permission" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="cmdAdd_Click"></asp:button>
														                        </td>
													                        </tr>
												                        </table>
												                    </asp:panel>
												                    <asp:panel id="panEdit" runat="server">
												                        <table cellspacing="1" cellpadding="2" style="width:100%;">
													                        <tr style="height:18px;">
														                        <td class="portletSecondary" style="width:34%;text-align:right;padding-top:5px;padding-bottom:5px;">
															                        <b>Name:</b>&nbsp;
														                        </td>
														                        <td class="portletLight" style="width:66%;padding-left:5px;">
															                        <asp:label id="lblName2" runat="server" cssclass="small"></asp:label>
														                        </td>
													                        </tr>
													                        <tr style="height:18px;">
														                        <td class="portletSecondary" style="text-align:right;padding-top:5px;padding-bottom:5px;">
															                        <b>ctcLink ID:</b>&nbsp;
														                        </td>
														                        <td class="portletLight" style="padding-left:5px;">
														                            <asp:label id="lblEmployeeID" runat="server" cssclass="small" style="width:60px;"></asp:label>
														                        </td>
													                        </tr>
													                        <tr>
														                        <td class="portletSecondary" style="text-align:right;">
															                        <b>Permissions:</b>&nbsp;
														                        </td>
														                        <td class="portletLight" style="padding-top:5px;padding-bottom:5px;padding-left:5px;">
														                            <asp:dropdownlist id="cboPermissions2" runat="server" cssclass="small">
														                                <asp:listitem value="1">Admin</asp:listitem>
														                                <asp:listitem value="2">Add/Edit/Delete</asp:listitem>
														                                <asp:listitem value="3">Read Only</asp:listitem>
														                            </asp:dropdownlist>
														                        </td>
													                        </tr>
													                        <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;">
															                        &nbsp;<input type="button" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" id="cmdCancel2" value="Cancel" onclick="location.href='grantpermissions.aspx';" />
															                        &nbsp;<asp:button id="cmdSave" runat="server" text="Save Permission" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="cmdSave_Click"></asp:button>
														                        </td>
													                        </tr>
												                        </table>
												                    </asp:panel>
												                    <asp:panel id="panView" runat="server">
												                        <asp:table id="tblUsers" cellpadding="3" cellspacing="1" runat="server" style="width:100%">
														                    <asp:tablerow>
															                    <asp:tablecell cssclass="portletSecondary" style="text-align:center;width:75px;"><b>ctcLink ID</b></asp:tablecell>
															                    <asp:tablecell cssclass="portletSecondary" style="text-align:center;"><b>Name</b></asp:tablecell>
															                    <asp:tablecell cssclass="portletSecondary" style="text-align:center;width:125px;"><b>Permissions</b></asp:tablecell>
															                    <asp:tablecell cssclass="portletSecondary" style="text-align:center;width:100px;"><a href="grantpermissions.aspx?todo=add">Add A New User</a></asp:tablecell>
														                    </asp:tablerow>
													                    </asp:table>
												                    </asp:panel>
											                    </td>
										                    </tr>
									                    </table>
								                    </td>
							                    </tr>
						                    </table>
					                    </td>
					                </tr>
					            </table>
					        </td>
				        </tr>
			        </table>
                </asp:panel>
                <div class="clearer"></div>
            </asp:panel>
            <script type="text/javascript">
            <!--
                if(document.frmProgramPermissions.hidError.value == 1){
                    alert("Please enter a valid ctcLink ID for the user you want to add.");
                    document.frmProgramPermissions.hidError.value = 0;
                    document.frmProgramPermissions.txtEmployeeID.select();
                }
            //-->
            </script>
        </form>
    </body>
</html>

