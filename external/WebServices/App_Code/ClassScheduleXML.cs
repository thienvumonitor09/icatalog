﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for ClassScheduleXML
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class ClassScheduleXML : System.Web.Services.WebService {

    private SqlConnection objCon, objCon2;
    private String strSQL, strConnection;
    private SqlCommand objCmd, objCmd2;
    private SqlDataAdapter objDA;
    private DataSet objDS;

    public ClassScheduleXML() {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 

        //production connection string
        strConnection = "Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=ClassScheduleWebService;Initial Catalog=CCSGen_ctcLink_ODS;Integrated Security=true;";

        //development connection string
        //strConnection = "Data Source=CCS-SQL-DEV;Workstation ID=ClassScheduleWebService;Initial Catalog=CCSGen_ctcLink_ODS;Integrated Security=true;";
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns true if all replicated tables have data. Returns false if any replicated tables are empty.</p>" +
             "<p><strong>Input:</strong> None<br /></p>" +
             "<p><strong>Output:</strong> Boolean</p>")]

    public bool ReplicatedDataExists() {
        objCon = new SqlConnection(strConnection);

        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_ClassSchedule_GetEmptyTableCount", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            if(Convert.ToInt16(objCmd.ExecuteScalar()) > 0) {
                return false;
            } else {
                return true;
            }
        } catch {
            return false;
        } finally {
            objCon.Close();
        }
    }

    public String GetCurrentTerm() {
        objCon2 = new SqlConnection(strConnection);

        try {
            objCon2.Open();
            objCmd2 = new SqlCommand("usp_ClassSchedule_GetCurrentTerm", objCon2);
            objCmd2.CommandType = CommandType.StoredProcedure;
            return objCmd2.ExecuteScalar().ToString();
        } catch {
            return "";
        } finally {
            objCon2.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns the registration term (Ten days after the current term, the next term is returned.)</p>" +
         "<p><strong>Input:</strong> None<br /></p>" +
         "<p><strong>Output:</strong> String</p>")]
    public String GetRegistrationTerm() {
        objCon2 = new SqlConnection(strConnection);

        try {
            objCon2.Open();
            objCmd2 = new SqlCommand("usp_ClassSchedule_GetRegistrationTerm", objCon2);
            objCmd2.CommandType = CommandType.StoredProcedure;

            SqlParameter objParam = new SqlParameter();
            objParam.ParameterName = "@RETURNVAL";
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Size = 30;
            objParam.Direction = ParameterDirection.Output;
            objCmd2.Parameters.Add(objParam);
            objCmd2.ExecuteScalar();
            return objCmd2.Parameters["@RETURNVAL"].Value.ToString();
        } catch {
            return "";
        } finally {
            objCon2.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns current and future terms.</p>" +
             "<p><strong>Input:</strong> None</p>" +
             "<p><strong>Output:</strong> DataSet<br />" +
             "FIELD LIST: Quarter, STRM, TERM_BEGIN_DT, SSR_SSENRLDISP_BDT</p>")]
    public DataSet GetTerms() {
        objCon = new SqlConnection(strConnection);

        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_ClassSchedule_GetTerms", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns term description.</p>" +
            "String STRM (optional): Term Code - If an empty string is passed, the current registration term is used</p>" +
            "<p><strong>Output:</strong> String</p>")]
    public String GetTermDesc(String STRM) {
        objCon = new SqlConnection(strConnection);

        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_ClassSchedule_GetTermDesc", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;

            if (STRM == null || STRM == "") {
                STRM = GetCurrentTerm();
            }

            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;

            SqlParameter objParam = new SqlParameter();
            objParam.ParameterName = "@RETURNVAL";
            objParam.SqlDbType = SqlDbType.VarChar;
            objParam.Size = 30;
            objParam.Direction = ParameterDirection.Output;
            objCmd.Parameters.Add(objParam);
            objCmd.ExecuteScalar();
            return objCmd.Parameters["@RETURNVAL"].Value.ToString();
        } catch {
            return "";
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns locations for current and future classes.</p>" +
            "<p><strong>Input:</strong><br />" +
            "String STRM (optional): Term Code - If an empty string is passed, the current registration term is used<br />" +
            "String INSTITUTION (optional): Location Code (WA171 = SCC) Enter an empty string to return locations for all institutions.</p>" +
            "<p><strong>Output:</strong> DataSet<br />" +
            "FIELD LIST: DESCR, LOCATION</p>")]
    public DataSet GetLocations(String STRM, String INSTITUTION) {
        objCon = new SqlConnection(strConnection);

        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_ClassSchedule_GetLocations", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;

            if (STRM == null || STRM == "") {
                STRM = GetCurrentTerm();
            }

            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;

            if (INSTITUTION != null && INSTITUTION != "") {
                objCmd.Parameters.Add("@INSTITUTION", SqlDbType.VarChar);
                objCmd.Parameters["@INSTITUTION"].Value = INSTITUTION;
            }

            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns the location description for a location code</p>" +
             "<p><strong>Input:</strong> None<br />" +
             "String LOCATION: Location Code (SMAIN = SCC, FMAIN = SFCC, PC = Pullman Campus, etc...)</p>" +
             "<p><strong>Output:</strong> String: DESCR (Location Description)</p>")]
    public String GetLocation(String LOCATION) {
        objCon = new SqlConnection(strConnection);

        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_ClassSchedule_GetLocation", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@LOCATION", SqlDbType.VarChar);
            objCmd.Parameters["@LOCATION"].Value = LOCATION;
            return objCmd.ExecuteScalar().ToString();
        } catch {
            return "";
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns class subjects.</p>" +
            "<p><strong>Input:</strong><br />" +
            "String STRM (optional): Term Code - If an empty string is passed, the current registration term is used<br />" +
            "String INSTITUTION (optional): Location Code (WA171 = SCC) Enter an empty string to return locations for all institutions.<br />" +
            "String LOCATION (optional): Location Code (SMAIN = SCC, FMAIN = SFCC, etc...)</p>" +
            "<p><strong>Output:</strong> DataSet<br />" +
            "FIELD LIST: SUBJECT, DESCR</p>")]
    public DataSet GetSubjects(String STRM, String INSTITUTION, String LOCATION) {
        objCon = new SqlConnection(strConnection);

        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_ClassSchedule_GetSubjects", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;

            if (STRM == null || STRM == "") {
                STRM = GetCurrentTerm();
            }

            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;

            if (INSTITUTION != null && INSTITUTION != "") {
                objCmd.Parameters.Add("@INSTITUTION", SqlDbType.VarChar);
                objCmd.Parameters["@INSTITUTION"].Value = INSTITUTION;
            }

            if (LOCATION != null && LOCATION != "") {
                objCmd.Parameters.Add("@LOCATION", SqlDbType.VarChar);
                objCmd.Parameters["@LOCATION"].Value = LOCATION;
            }

            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns class instruction modes.</p>" +
            "<p><strong>Input:</strong> None<br />" +
            "String STRM (optional): Term Code - If an empty string is passed, the current registration term is used<br />" +
            "String INSTITUTION (optional): Location Code (WA171 = SCC) Enter an empty string to return locations for all institutions.<br />" +
            "String LOCATION (optional): Location Code (SMAIN = SCC, FMAIN = SFCC, etc...)</p>" +
            "<p><strong>Output:</strong> DataSet<br />" +
            "FIELD LIST: DESCR, INSTRUCTION_MODE</p>")]
    public DataSet GetInstructionModes(String STRM, String INSTITUTION, String LOCATION) {
        objCon = new SqlConnection(strConnection);

        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_ClassSchedule_GetInstructionModes", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;

            if (STRM == null || STRM == "") {
                STRM = GetCurrentTerm();
            }

            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;

            if (INSTITUTION != null && INSTITUTION != "") {
                objCmd.Parameters.Add("@INSTITUTION", SqlDbType.VarChar);
                objCmd.Parameters["@INSTITUTION"].Value = INSTITUTION;
            }

            if (LOCATION != null && LOCATION != "") {
                objCmd.Parameters.Add("@LOCATION", SqlDbType.VarChar);
                objCmd.Parameters["@LOCATION"].Value = LOCATION;
            }

            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns classes by term (required) and optional search filters. Enter an empty string or 0 based on data type for optional search filters to return all.</p>" +
            "<p><strong>Input:</strong><br />" +
             "String STRM (optional): Term Code - If an empty string is passed, the current registration term is used<br />" +
             "String INSTITUTION (optional): Location Code (WA171 = SCC)<br />" +
             "String CRSE_ID (optional): Course ID<br />" +
             "Int16 CLASS_NBR (optional): Class ID<br />" +
             "String LOCATION (optional): Location Code (SMAIN = SCC, FMAIN = SFCC, etc...)<br />" +
             "String SUBJECT (optional): Course Subject (ENG, ACCT, etc...)<br />" +
             "String CATALOG_NBR (optional): Course Number (101, 102, 110, etc...)<br />" +
             "String ENRL_STAT (optional): O = Open, C = Closed, W = Wait List<br />" +
             "String InstructorName (optional): Instructor Name or partial text string<br />" +
             "String InstructionMode (optional): CO = Correspondence, HY = Hybrid, P = In Person, OL = On-Line, etc...<br />" +
             "Byte Credit (optional): 1 for Credit Classes, 0 for all<br />" +
             "Byte Noncredit (optional): 1 for Noncredit Classes, 0 for all<br />" +
             "Byte Act2 (optional): 1 for Act 2 Classes, 0 for all<br />" +
             "Byte WritingIntensive (optional): 1 for Writing Intensive Classes, 0 for all<br />" +
             "Byte Diversity (optional): 1 for Diversity Classes, 0 for all<br />" +
             "Byte Honors (optional): 1 for Honors Classes, 0 for all<br />" +
             "Byte LCCM (optional): 1 for low cost course material classes, 0 for all<br />" +
             "Byte SOER (optional): 1 for open educational resource classes, 0 for all<br />" +
             "String GlobalSearch (optional): global text search<br />" +
             "String StartTimeBegin<br />" +
             "String StartTimeEnd<br />" +
             "<p><strong>Output:</strong> DataSet<br />" +
             "FIELD LIST: INSTITUTION, STRM, TermTable_DESCR, CRSE_ID, CLASS_NBR, SUBJECT, CATALOG_NBR, UNITS_MINIMUM, UNITS_MAXIMUM, STND_MTG_PAT, MON, TUES, WED, THURS, FRI, SAT, SUN, ClassTimeStart, ClassTimeEnd, FacilityTable_DESCR, CLASS_STAT, ENRL_STAT, ClassTable_DESCR, COURSE_TITLE_LONG, ENRL_CAP, ENRL_STAT, ENRL_TOT, WAIT_CAP, NAME, FIRST_NAME, LAST_NAME, MIDDLE_NAME, INSTRUCTION_MODE, START_DT, END_DT, CourseCatalog_DESCRLONG, ClassNotes_DESCRLONG, DESCR254A, CourseTopics_DESCR, CRSE_ATTR, CourseAttributesTable_DESCR, InstructionMode_DESCR, LASTREPLICATIONDATE</p>")]
    public DataSet GetClasses(String STRM, String INSTITUTION, String CRSE_ID, Int16 CLASS_NBR, String LOCATION, String SUBJECT, String CATALOG_NBR, String ENRL_STAT, String InstructorName, String InstructionMode, Byte Credit, Byte Noncredit, Byte Act2, Byte WritingIntensive, Byte Diversity, Byte Honors, Byte LCCM, Byte SOER, String GlobalSearch, String StartTimeBegin, String StartTimeEnd) {
        objCon = new SqlConnection(strConnection);

        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_ClassSchedule_GetClasses", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;

            if (STRM == null || STRM == "") {
                STRM = GetCurrentTerm();
            }

            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;

            if (INSTITUTION != null && INSTITUTION != "") {
                objCmd.Parameters.Add("@INSTITUTION", SqlDbType.VarChar);
                objCmd.Parameters["@INSTITUTION"].Value = INSTITUTION;
            }

            if (CRSE_ID != null && CRSE_ID != "") {
                objCmd.Parameters.Add("@CRSE_ID", SqlDbType.VarChar);
                objCmd.Parameters["@CRSE_ID"].Value = CRSE_ID;
            }

            if (CLASS_NBR != 0) {
                objCmd.Parameters.Add("@CLASS_NBR", SqlDbType.Int);
                objCmd.Parameters["@CLASS_NBR"].Value = CLASS_NBR;
            }

            if (LOCATION != null && LOCATION != "") {
                objCmd.Parameters.Add("@LOCATION", SqlDbType.VarChar);
                objCmd.Parameters["@LOCATION"].Value = LOCATION;
            }

            if (SUBJECT != null && SUBJECT != "") {
                objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
            }

            if (CATALOG_NBR != null && CATALOG_NBR != "") {
                objCmd.Parameters.Add("@CATALOG_NBR", SqlDbType.VarChar);
                objCmd.Parameters["@CATALOG_NBR"].Value = CATALOG_NBR;
            }

            if (ENRL_STAT != null && ENRL_STAT != "") {
                objCmd.Parameters.Add("@ENRL_STAT", SqlDbType.VarChar);
                objCmd.Parameters["@ENRL_STAT"].Value = ENRL_STAT;
            }

            if (InstructorName != null && InstructorName != "") {
                objCmd.Parameters.Add("@InstructorName", SqlDbType.VarChar);
                objCmd.Parameters["@InstructorName"].Value = InstructorName;
            }

            if (InstructionMode != null && InstructionMode != "") {
                objCmd.Parameters.Add("@InstructionMode", SqlDbType.VarChar);
                objCmd.Parameters["@InstructionMode"].Value = InstructionMode;
            }

            if (Credit != 0) {
                objCmd.Parameters.Add("@Credit", SqlDbType.Bit);
                objCmd.Parameters["@Credit"].Value = Credit;
            }

            if (Noncredit != 0) {
                objCmd.Parameters.Add("@Noncredit", SqlDbType.Bit);
                objCmd.Parameters["@Noncredit"].Value = Noncredit;
            }

            if (Act2 != 0) {
                objCmd.Parameters.Add("@Act2", SqlDbType.Bit);
                objCmd.Parameters["@Act2"].Value = Act2;
            }

            if (WritingIntensive != 0) {
                objCmd.Parameters.Add("@WritingIntensive", SqlDbType.Bit);
                objCmd.Parameters["@WritingIntensive"].Value = WritingIntensive;
            }

            if (Diversity != 0) {
                objCmd.Parameters.Add("@Diversity", SqlDbType.Bit);
                objCmd.Parameters["@Diversity"].Value = Diversity;
            }

            if (Honors != 0) {
                objCmd.Parameters.Add("@Honors", SqlDbType.Bit);
                objCmd.Parameters["@Honors"].Value = Honors;
            }

            if (LCCM != 0) {
                objCmd.Parameters.Add("@LCCM", SqlDbType.Bit);
                objCmd.Parameters["@LCCM"].Value = LCCM;
            }

            if (SOER != 0) {
                objCmd.Parameters.Add("@SOER", SqlDbType.Bit);
                objCmd.Parameters["@SOER"].Value = SOER;
            }

            if (GlobalSearch != null && GlobalSearch != "") {
                objCmd.Parameters.Add("@GlobalSearch", SqlDbType.VarChar);
                objCmd.Parameters["@GlobalSearch"].Value = GlobalSearch;
            }

            if (StartTimeBegin != null && StartTimeBegin != "") {
                objCmd.Parameters.Add("@StartTimeBegin", SqlDbType.VarChar);
                objCmd.Parameters["@StartTimeBegin"].Value = StartTimeBegin;
            }

            if (StartTimeEnd != null && StartTimeEnd != "") {
                objCmd.Parameters.Add("@StartTimeEnd", SqlDbType.VarChar);
                objCmd.Parameters["@StartTimeEnd"].Value = StartTimeEnd;
            }

            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns classes by term (required) and optional search filters. Enter an empty string or 0 based on data type for optional search filters to return all.</p>" +
            "<p><strong>Input:</strong><br />" +
             "String STRM (optional): Term Code - If an empty string is passed, the current registration term is used<br />" +
             "String INSTITUTION (optional): Location Code (WA171 = SCC)<br />" +
             "String CRSE_ID (optional): Course ID<br />" +
             "Int16 CLASS_NBR (optional): Class ID<br />" +
             "String LOCATION (optional): Location Code (SMAIN = SCC, FMAIN = SFCC, etc...)<br />" +
             "String SUBJECT (optional): Course Subject (ENG, ACCT, etc...)<br />" +
             "String CATALOG_NBR (optional): Course Number (101, 102, 110, etc...)<br />" +
             "String ENRL_STAT (optional): O = Open, C = Closed, W = Wait List<br />" +
             "String InstructorName (optional): Instructor Name or partial text string<br />" +
             "String InstructionMode (optional): CO = Correspondence, HY = Hybrid, P = In Person, OL = On-Line, etc...<br />" +
             "Byte Credit (optional): 1 for Credit Classes, 0 for all<br />" +
             "Byte Noncredit (optional): 1 for Noncredit Classes, 0 for all<br />" +
             "Byte Act2 (optional): 1 for Act 2 Classes, 0 for all<br />" +
             "Byte WritingIntensive (optional): 1 for Writing Intensive Classes, 0 for all<br />" +
             "Byte Diversity (optional): 1 for Diversity Classes, 0 for all<br />" +
             "Byte Honors (optional): 1 for Honors Classes, 0 for all<br />" +
             "Byte LCCM (optional): 1 for low cost course material classes, 0 for all<br />" +
             "Byte SOER (optional): 1 for open educational resource classes, 0 for all<br />" +
             "String GlobalSearch (optional): global text search<br />" +
             "String StartTimeBegin<br />" +
             "String StartTimeEnd<br />" +
             "<p><strong>Output:</strong> DataSet<br />" +
             "FIELD LIST: STRM, INSTITUTION, CRSE_ID, CLASS_NBR, SUBJECT, CATALOG_NBR, UNITS_MINIMUM, UNITS_MAXIMUM, STND_MTG_PAT, MON, TUES, WED, THURS, FRI, SAT, SUN, ClassTimeStart, ClassTimeEnd, FacilityTable_DESCR, CLASS_STAT, ENRL_STAT, COURSE_TITLE_LONG, ENRL_CAP, ENRL_STAT, ENRL_TOT, WAIT_CAP, LASTREPLICATIONDATE, COMBINED_SECTION, SectionCombinedTable_ENRL_CAP, SectionCombinedTable_ENRL_TOT, SectionCombinedTable_WAIT_TOT </p>")]
    public DataSet GetClassList(String STRM, String INSTITUTION, String CRSE_ID, Int16 CLASS_NBR, String LOCATION, String SUBJECT, String CATALOG_NBR, String ENRL_STAT, String InstructorName, String InstructionMode, Byte Credit, Byte Noncredit, Byte Act2, Byte WritingIntensive, Byte Diversity, Byte Honors, Byte LCCM, Byte SOER, String GlobalSearch, String StartTimeBegin, String StartTimeEnd) {
        objCon = new SqlConnection(strConnection);

        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_ClassSchedule_GetClassList", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;

            if (STRM == null || STRM == "") {
                STRM = GetCurrentTerm();
            }

            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;

            if (INSTITUTION != null && INSTITUTION != "") {
                objCmd.Parameters.Add("@INSTITUTION", SqlDbType.VarChar);
                objCmd.Parameters["@INSTITUTION"].Value = INSTITUTION;
            }

            if (CRSE_ID != null && CRSE_ID != "") {
                objCmd.Parameters.Add("@CRSE_ID", SqlDbType.VarChar);
                objCmd.Parameters["@CRSE_ID"].Value = CRSE_ID;
            }

            if (CLASS_NBR != 0) {
                objCmd.Parameters.Add("@CLASS_NBR", SqlDbType.Int);
                objCmd.Parameters["@CLASS_NBR"].Value = CLASS_NBR;
            }

            if (LOCATION != null && LOCATION != "") {
                objCmd.Parameters.Add("@LOCATION", SqlDbType.VarChar);
                objCmd.Parameters["@LOCATION"].Value = LOCATION;
            }

            if (SUBJECT != null && SUBJECT != "") {
                objCmd.Parameters.Add("@SUBJECT", SqlDbType.VarChar);
                objCmd.Parameters["@SUBJECT"].Value = SUBJECT;
            }

            if (CATALOG_NBR != null && CATALOG_NBR != "") {
                objCmd.Parameters.Add("@CATALOG_NBR", SqlDbType.VarChar);
                objCmd.Parameters["@CATALOG_NBR"].Value = CATALOG_NBR;
            }

            if (ENRL_STAT != null && ENRL_STAT != "") {
                objCmd.Parameters.Add("@ENRL_STAT", SqlDbType.VarChar);
                objCmd.Parameters["@ENRL_STAT"].Value = ENRL_STAT;
            }

            if (InstructorName != null && InstructorName != "") {
                objCmd.Parameters.Add("@InstructorName", SqlDbType.VarChar);
                objCmd.Parameters["@InstructorName"].Value = InstructorName;
            }

            if (InstructionMode != null && InstructionMode != "") {
                objCmd.Parameters.Add("@InstructionMode", SqlDbType.VarChar);
                objCmd.Parameters["@InstructionMode"].Value = InstructionMode;
            }

            if (Credit != 0) {
                objCmd.Parameters.Add("@Credit", SqlDbType.Bit);
                objCmd.Parameters["@Credit"].Value = Credit;
            }

            if (Noncredit != 0) {
                objCmd.Parameters.Add("@Noncredit", SqlDbType.Bit);
                objCmd.Parameters["@Noncredit"].Value = Noncredit;
            }

            if (Act2 != 0) {
                objCmd.Parameters.Add("@Act2", SqlDbType.Bit);
                objCmd.Parameters["@Act2"].Value = Act2;
            }

            if (WritingIntensive != 0) {
                objCmd.Parameters.Add("@WritingIntensive", SqlDbType.Bit);
                objCmd.Parameters["@WritingIntensive"].Value = WritingIntensive;
            }

            if (Diversity != 0) {
                objCmd.Parameters.Add("@Diversity", SqlDbType.Bit);
                objCmd.Parameters["@Diversity"].Value = Diversity;
            }

            if (Honors != 0) {
                objCmd.Parameters.Add("@Honors", SqlDbType.Bit);
                objCmd.Parameters["@Honors"].Value = Honors;
            }

            if (LCCM != 0) {
                objCmd.Parameters.Add("@LCCM", SqlDbType.Bit);
                objCmd.Parameters["@LCCM"].Value = LCCM;
            }

            if (SOER != 0) {
                objCmd.Parameters.Add("@SOER", SqlDbType.Bit);
                objCmd.Parameters["@SOER"].Value = SOER;
            }

            if (GlobalSearch != null && GlobalSearch != "") {
                objCmd.Parameters.Add("@GlobalSearch", SqlDbType.VarChar);
                objCmd.Parameters["@GlobalSearch"].Value = GlobalSearch;
            }

            if (StartTimeBegin != null && StartTimeBegin != "") {
                objCmd.Parameters.Add("@StartTimeBegin", SqlDbType.VarChar);
                objCmd.Parameters["@StartTimeBegin"].Value = StartTimeBegin;
            }

            if (StartTimeEnd != null && StartTimeEnd != "") {
                objCmd.Parameters.Add("@StartTimeEnd", SqlDbType.VarChar);
                objCmd.Parameters["@StartTimeEnd"].Value = StartTimeEnd;
            }

            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    [WebMethod(Description = "<p><strong>Description:</strong> Returns classes by term (required) and optional search filters. Enter an empty string or 0 based on data type for optional search filters to return all.</p>" +
            "<p><strong>Input:</strong><br />" +
             "String STRM: Term Code - If an empty string is passed, the current registration term is used<br />" +
             "String INSTITUTION: Location Code (WA171 = SCC)<br />" +
             "Int16 CLASS_NBR: Class ID<br />" +
             "<p><strong>Output:</strong> DataSet<br />" +
             "FIELD LIST: INSTITUTION, CLASS_NBR, UNITS_MINIMUM, UNITS_MAXIMUM, CLASS_STAT, ENRL_STAT, ENRL_CAP, ENRL_STAT, ENRL_TOT, WAIT_CAP, NAME, FIRST_NAME, LAST_NAME, MIDDLE_NAME, START_DT, END_DT, CourseCatalog_DESCRLONG, ClassNotes_DESCRLONG, DESCR254A, CourseTopics_DESCR, CRSE_ATTR, CourseAttributesTable_DESCR, InstructionMode_DESCR, LASTREPLICATIONDATE, COMBINED_SECTION, SectionCombinedTable_ENRL_CAP, SectionCombinedTable_ENRL_TOT, SectionCombinedTable_WAIT_TOT </p>")]
    public DataSet GetClassDetails(String STRM, String INSTITUTION, Int16 CLASS_NBR) {
        objCon = new SqlConnection(strConnection);

        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_ClassSchedule_GetClassDetails", objCon);
            objDS = new DataSet();
            objCmd.CommandType = CommandType.StoredProcedure;

            if (STRM == null || STRM == "") {
                STRM = GetCurrentTerm();
            }

            objCmd.Parameters.Add("@STRM", SqlDbType.VarChar);
            objCmd.Parameters["@STRM"].Value = STRM;

            objCmd.Parameters.Add("@INSTITUTION", SqlDbType.VarChar);
            objCmd.Parameters["@INSTITUTION"].Value = INSTITUTION;

            objCmd.Parameters.Add("@CLASS_NBR", SqlDbType.Int);
            objCmd.Parameters["@CLASS_NBR"].Value = CLASS_NBR;

            objDA = new SqlDataAdapter(objCmd);
            objDA.Fill(objDS);
            return objDS;

        } finally {
            objCon.Close();
        }
    }
}
