USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionsByTitle]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/20/2019
-- Description:		Returns an alphabetic program option list by term, letter the option title starts with, location, degree, course subject, and course catalog number. 
--		 INPUT: STRM - Term code
--				LocationID (optional) - Location primary key 
--				DegreeID (optional) - Degree primary key 
--				AlphaCharacter (optional) - Letter of the alphabet the program option titles start with
--				SUBJECT (optional) - Course subject area 
--				CATALOG_NBR (optional) - Course catalog number 
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionsByTitle]
	@STRM varchar(4) ,
	@LocationID int = NULL,
	@DegreeID int = NULL,
	@AlphaCharacter char = NULL,
	@SUBJECT varchar(5) = NULL,
	@CATALOG_NBR varchar(3) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQL nvarchar(max),
			@Parameters nvarchar(4000)

	SET @SQL = 'SELECT CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID
			FROM ProgramDegree
			INNER JOIN Program 
			ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
			INNER JOIN CareerPlanningGuide 
			ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
			INNER JOIN College 
			ON College.CollegeID = Program.CollegeID 
			INNER JOIN ProgramDegreeOption 
			ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
			INNER JOIN Degree 
			ON Degree.DegreeID = ProgramDegree.DegreeID 
			LEFT OUTER JOIN OptionPrerequisite 
			ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
			LEFT OUTER JOIN OptionElectiveGroup 
			ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
			LEFT OUTER JOIN ElectiveGroupCourse 
			ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
			LEFT OUTER JOIN OptionCourse 
			ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
			LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
			ON REPLACE(SUBJECT+CATALOG_NBR, '' '','''')  = REPLACE(OptionPrerequisite.CourseOffering,'' '','''') 
				OR  REPLACE(SUBJECT+CATALOG_NBR,'' '' ,'''')  = REPLACE(ElectiveGroupCourse.CourseOffering,'' '','''') 
				OR REPLACE(SUBJECT+CATALOG_NBR, '' '' ,'''')  = REPLACE(OptionCourse.CourseOffering,'' '','''')
			LEFT OUTER JOIN OptionLocation 
			ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
			LEFT OUTER JOIN Location 
			ON Location.LocationID = OptionLocation.LocationID 
			WHERE ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = ''Z999'' OR ProgramEndSTRM >= @STRM) 
			AND PublishedProgram = 1 '
				
	IF @LocationID IS NOT NULL
		SET @SQL += 'AND OptionLocation.LocationID = @LocationID '
		
	IF @DegreeID IS NOT NULL
		SET @SQL += 'AND ProgramDegree.DegreeID = @DegreeID '
		
	IF @AlphaCharacter IS NOT NULL
		SET @SQL += 'AND (ProgramTitle LIKE @AlphaCharacter + ''%'' OR OptionTitle LIKE @AlphaCharacter + ''%'') '
		
	IF @SUBJECT IS NOT NULL
		SET @SQL += 'AND ( (SUBJECT = @SUBJECT OR SUBJECT = @SUBJECT + ''&'') 
						AND EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM)) ) '
	
	IF @CATALOG_NBR IS NOT NULL	
		SET @SQL += 'AND (CATALOG_NBR = @CATALOG_NBR 
						AND EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM))) '
					 
	SET @SQL += 'GROUP BY Program.ProgramVersionID, CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID 
					ORDER BY ProgramTitle, OptionTitle, DegreeShortTitle, CollegeShortTitle;'
				 
	SET @Parameters = '@STRM varchar(4),
						@LocationID int,
						@DegreeID int,
						@AlphaCharacter char,
						@SUBJECT varchar(5),
						@CATALOG_NBR varchar(3)'
						 
	EXEC sp_executesql @SQL, @Parameters, @STRM, @LocationID, @DegreeID, @AlphaCharacter, @SUBJECT, @CATALOG_NBR
	/*
    ;WITH CourseOffering (CourseOffering, SUBJECT, CATALOG_NBR, EFFDT)
	-- First table to retrieve list of subjects.  
	AS
	(
		SELECT  ( CAST(REPLACE(SUBJECT,' ','') AS CHAR(5)) + CAST(REPLACE(CATALOG_NBR,' ','') AS CHAR(3)) ) AS CourseOffering
				, REPLACE(SUBJECT,' ',''), REPLACE(CATALOG_NBR,' ',''), EFFDT 
		FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering1
		WHERE EFFDT = 
		(
			SELECT MAX(EFFDT) FROM 
			CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering2
			WHERE CourseOffering2.CRSE_ID = CourseOffering1.CRSE_ID 
					AND INSTITUTION = 'WA171'
					AND
					(
						CourseOffering2.EFFDT <= (SELECT TERM_END_DT FROM udf_GetTERM_END_DT(@STRM))
					)
					AND ACAD_CAREER = 'UGRD'
				AND CATALOG_PRINT = 'Y'
		)
	)
	SELECT CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, Program.ProgramVersionID, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID
	FROM ProgramDegree
	INNER JOIN Program 
	ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
	INNER JOIN CareerPlanningGuide 
	ON CareerPlanningGuide.ProgramVersionID = Program.ProgramVersionID 
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID 
	INNER JOIN ProgramDegreeOption 
	ON ProgramDegreeOption.ProgramDegreeID = ProgramDegree.ProgramDegreeID 
	INNER JOIN Degree 
	ON Degree.DegreeID = ProgramDegree.DegreeID 
	LEFT OUTER JOIN OptionPrerequisite 
	ON OptionPrerequisite.OptionID = ProgramDegreeOption.OptionID
	LEFT OUTER JOIN OptionElectiveGroup 
	ON OptionElectiveGroup.OptionID = ProgramDegreeOption.OptionID
	LEFT OUTER JOIN ElectiveGroupCourse 
	ON ElectiveGroupCourse.OptionElectiveGroupID = OptionElectiveGroup.OptionElectiveGroupID 
	LEFT OUTER JOIN OptionCourse 
	ON OptionCourse.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN CourseOffering
	ON  (  CourseOffering.CourseOffering  = OptionPrerequisite.CourseOffering) 
		OR ( CourseOffering.CourseOffering  = ElectiveGroupCourse.CourseOffering) 
		OR ( CourseOffering.CourseOffering  = OptionCourse.CourseOffering)
	LEFT OUTER JOIN OptionLocation 
	ON OptionLocation.OptionID = ProgramDegreeOption.OptionID 
	LEFT OUTER JOIN Location 
	ON Location.LocationID = OptionLocation.LocationID 

	WHERE ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM) 
		AND PublishedProgram = 1 
		AND 
			(
				@LocationID IS NULL OR OptionLocation.LocationID = @LocationID
			)
		AND
			(
				@DegreeID IS NULL OR ProgramDegree.DegreeID = @DegreeID
			)
		AND
			(
				@AlphaCharacter IS NULL OR (ProgramTitle LIKE @AlphaCharacter + '%' OR OptionTitle LIKE @AlphaCharacter + '%')
			)
		AND
			(
				@SUBJECT IS NULL
				OR
				(
				SUBJECT = @SUBJECT OR SUBJECT = @SUBJECT + '&' 
				AND EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM))
				)
			)
		AND
			(
				@CATALOG_NBR IS NULL 
				OR 
				(
				CourseOffering.CATALOG_NBR = @CATALOG_NBR 
				AND EFFDT <= (SELECT * FROM udf_GetTERM_END_DT(@STRM))
				) 
			) 
	GROUP BY Program.ProgramVersionID, CollegeShortTitle, CollegeLongTitle, ProgramTitle, ProgramWebsiteURL, OptionTitle, DegreeShortTitle, DegreeLongTitle, ProgramDegree.ProgramDegreeID, ProgramDegreeOption.OptionID 
	ORDER BY ProgramTitle, OptionTitle, DegreeShortTitle, CollegeShortTitle;
	*/
END
GO
