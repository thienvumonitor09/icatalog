USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTermsInRange]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 3/7/16
-- Description:	Returns terms between the start and end term specified
--       INPUT: STRMStart (required) - Start Term Code to include in range (inclusive)
--				STRMEnd (optional) - End Term Code of the range (non-inclusive)
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetTermsInRange] 
	@STRMStart varchar(4),
	@STRMEnd varchar(4) = NULL
AS
BEGIN

	SET NOCOUNT ON;

	SELECT DISTINCT STRM, DESCR 
	FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
	WHERE STRM >= @STRMStart
	AND (@STRMEnd IS NULL OR STRM < @STRMEnd)
	ORDER BY STRM DESC;
END

GO
