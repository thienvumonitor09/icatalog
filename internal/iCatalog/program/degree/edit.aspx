<%@ Reference Page="~/program/degree/add.aspx" %>
<%@ Page language="c#" Inherits="ICatalog.program.degree.edit" CodeFile="edit.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
    <head>
        <title>Edit/Delete College</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../../_phatt3_css/styles.css" />
        <script type="text/javascript" src="../../_phatt3_src_files/trim.js"></script>
        <script type="text/javascript" src="../../_phatt3_src_files/expand.js"></script>
        <script type="text/javascript">
        <!--
			function validate(){
				var isValid = false;
				var txtDegreeShortTitle = document.getElementById("txtDegreeShortTitle");
				
				if(trim(txtDegreeShortTitle.value) == ""){
					alert("Please enter a Degree Title.");
					txtDegreeShortTitle.select();
				}else{
					isValid = true;
				}
				
				return isValid;				
			}
			
            /*
			function launchCenter(url, name, height, width) {
				var str = "height=" + height + ",innerHeight=" + height;
				str += ",width=" + width + ",innerWidth=" + width;
				if (window.screen) {
					var ah = screen.availHeight - 30;
					var aw = screen.availWidth - 10;

					var xc = (aw - width) / 2;
					var yc = (ah - height) / 2;

					str += ",left=" + xc + ",screenX=" + xc;
					str += ",top=" + yc + ",screenY=" + yc;
					str += ",resizable=yes,scrollbars=yes";
				}
				return window.open(url, name, str);
            }
            */

            function popupCenter(url, title, h, w) {
                // Fixes dual-screen position  
                var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                var top = ((height / 2) - (h / 2)) + dualScreenTop;
                var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

                // Puts focus on the newWindow  
                if (window.focus) {
                    newWindow.focus();
                }
            }
        //-->
        </script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmDegree" runat="server">
                    <input type="hidden" runat="server" id="hidTodo" />
                    <input type="hidden" runat="server" id="hidDegreeID" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Edit/Delete Degree</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <asp:panel runat="server" id="panDisplay">
                                                                        <asp:table runat="server" id="tblDisplay" style="width:100%;" cellspacing="1" cellpadding="2"></asp:table>
                                                                    </asp:panel>
                                                                    <asp:panel runat="server" id="panEdit">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <!-- Error Message -->
                                                                            <asp:panel runat="server" id="panError">
                                                                                <tr>
                                                                                    <td colspan="2" class="portletMain" style="color:red;padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                        <asp:label runat="server" id="lblErrorMsg"></asp:label>
                                                                                    </td>
                                                                                </tr>
                                                                            </asp:panel>
                                                                            <!-- Degree Short Title -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:34%;text-align:right;">
                                                                                    <b>Degree Short Title:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:66%;vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtDegreeShortTitle" cssclass="small" maxlength="80" style="width:250px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Degree Long Title -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;">
                                                                                    <b>Degree Long Title:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <asp:textbox runat="server" id="txtDegreeLongTitle" cssclass="small" maxlength="120" style="width:360px;"></asp:textbox>
                                                                                </td>
                                                                            </tr>
                                                                             <!-- Degree Type -->
                                                                            <tr>
																				<td class="portletSecondary" style="text-align:right;">
																					<b>Degree Type:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:DropDownList Runat="server" ID="cboDegreeType" CssClass="small">
																						<asp:ListItem Value=""></asp:ListItem>
																						<asp:ListItem Value="Career Technical">Career Technical</asp:ListItem>
																						<asp:ListItem Value="Transfer">Transfer</asp:ListItem>
																					</asp:DropDownList>
																				</td>	
																			</tr>
                                                                            <!-- Degree Requirements/Worksheet -->
                                                                            <tr>
																				<td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;">
																					<b>Requirements/Worksheet:&nbsp;</b>
																				</td>
																				<td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
																					<asp:checkbox id="chkTransDeg" style="font-weight:bold;" runat="server"></asp:checkbox>
																					<asp:dropdownlist id="cboTransDeg" runat="server" cssclass="small" AutoPostBack="True"></asp:dropdownlist>
																					<div style="padding-left:22px;padding-top:5px">
																						<asp:DropDownList ID="cboTransDegYear" Runat="server" CssClass="small" AutoPostBack="True"></asp:DropDownList>
																						<asp:label ID="lblViewLink" Runat="server"></asp:label>
																				    </div>
																				</td>
																			</tr>
                                                                            <!-- Degree Description -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:9px;">
                                                                                    <b>Degree Description:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-top:6px;padding-bottom:6px;padding-left:6px;">
                                                                                    <textarea runat="server" id="txtDegreeDescription" rows="10" style="width:450px;" class="small" ondblclick="expandTextArea(this.id,123);" title="Double click to expand this text area when the text overflows. Double click again to return to the original size." NAME="txtDegreeDescription"></textarea>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
														                        <td class="portletMain" colspan="2" style="text-align:center;padding:3px;width:100%;">
															                        <input type="button" id="cmdCancel" value="Cancel" onclick="location.href='edit.aspx';" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" /> 
                                                                                    &nbsp;<asp:button id="cmdSubmit" runat="server" text="Save Changes" cssclass="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclientclick="return validate();" onclick="cmdSubmit_Click"></asp:button>
														                        </td>
													                        </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <DIV class="clearer"></DIV>
        </asp:panel>
    </body>
</html>



