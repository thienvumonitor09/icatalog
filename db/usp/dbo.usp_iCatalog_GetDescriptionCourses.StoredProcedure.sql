USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_iCatalog_GetDescriptionCourses]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 3/16/17
-- Description:	Returns courses that can be used as the source for another course's long descripiton
--		 INPUT: SUBJECT - Course Subject: ENGL
-- =============================================
CREATE PROCEDURE [dbo].[usp_iCatalog_GetDescriptionCourses] 
	@SUBJECT varchar(5)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT CourseOffering, CourseID, CourseBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, CourseEndSTRM, EndTerm.DESCR AS EndTerm_DESCR
	FROM Course 
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = CourseBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = CourseEndSTRM
	WHERE SUBJECT = @SUBJECT
	AND DescriptionCourseID IS NULL;
END

GO
