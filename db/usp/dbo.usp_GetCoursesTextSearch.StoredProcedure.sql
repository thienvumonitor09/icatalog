USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCoursesTextSearch]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vu Nguyen
-- Create date: 05/15/2019
-- Description:	Returns courses containing a specified string in the long description
--		 INPUT: SearchText - The string being searched in the long course description
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCoursesTextSearch]
	@SearchText varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT SUBJECT,CATALOG_NBR, (SUBJECT + CATALOG_NBR) AS CourseOffering, DESCR, CourseOffering.EFFDT,  CATALOG_PRINT
	FROM CCSGen_ctcLink_ODS.dbo.vw_CourseOffering AS CourseOffering
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_CourseCatalog AS CourseCatalog
	ON CourseCatalog.CRSE_ID = CourseOffering.CRSE_ID  AND CourseCatalog.EFFDT = CourseOffering.EFFDT 
	WHERE DESCRLONG LIKE '%' + @SearchText + '%' 
		AND ACAD_CAREER = 'UGRD'
	--AND CATALOG_PRINT = 'Y'
		AND EFF_STATUS = 'A'
	--AND INSTITUTION = 'WA171'
	ORDER BY CourseOffering, CourseOffering.EFFDT
END
GO
