USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTermDetails]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 3/7/17
-- Description:	Returns term description, start date, and end date
--		 INPUT: STRM (required) - Term Code
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetTermDetails]
	@STRM varchar(4)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT DESCR, TERM_BEGIN_DT, TERM_END_DT
	FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
	WHERE STRM = @STRM
	AND ACAD_CAREER = 'UGRD'
END

GO
