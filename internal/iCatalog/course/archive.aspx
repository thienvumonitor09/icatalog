<%@ Reference Page="~/course/view.aspx" %>
<%@ Page language="c#" Inherits="ICatalog.course.archive" CodeFile="archive.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../_phatt3_includes/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sidemenu" Src="../_phatt3_includes/sidemenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Course Archives</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
        <meta name="CODE_LANGUAGE" content="C#" />
        <meta name="vs_defaultClientScript" content="JavaScript" />
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/default.css" />
        <link rel="stylesheet" type="text/css" href="../_phatt3_css/styles.css" />
        <script type="text/javascript">
        <!--
            /*
			function launchCenter(url, name, height, width) {
				var str = "height=" + height + ",innerHeight=" + height;
				str += ",width=" + width + ",innerWidth=" + width;
				if (window.screen) {
					var ah = screen.availHeight - 30;
					var aw = screen.availWidth - 10;

					var xc = (aw - width) / 2;
					var yc = (ah - height) / 2;

					str += ",left=" + xc + ",screenX=" + xc;
					str += ",top=" + yc + ",screenY=" + yc;
					str += ",resizable=yes,scrollbars=yes";
				}
				return window.open(url, name, str);
            }
            */

            function popupCenter(url, title, h, w) {
                // Fixes dual-screen position  
                var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                var top = ((height / 2) - (h / 2)) + dualScreenTop;
                var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

                // Puts focus on the newWindow  
                if (window.focus) {
                    newWindow.focus();
                }
            }
			
			function prepareRevision(checked){
				if(checked){
					//show effective year quarter pop up
					popupCenter('popups/editterm.aspx?cid=' + document.frmCourse.hidCourseID.value + '&pcid=' + document.frmCourse.hidPublishedCourseID.value + '&pc=' + document.frmCourse.hidPublishedCourse.value + '&co=' + document.frmCourse.hidCourseOffering.value.replace(new RegExp('&nbsp;','g'),'%20').replace('&','%26') + '&bstrm=' + document.frmCourse.hidCourseBeginSTRM.value + '&estrm=' + document.frmCourse.hidCourseEndSTRM.value + '&pg=archive', 'EditTerm', 260, 450);
				}
			}
        //-->
        </script>
    </head>
    <body>
        <asp:panel id="container" runat="server">
            <asp:panel id="header" runat="server">
                <uc1:header id="Header1" runat="server"></uc1:header>
            </asp:panel>
            <asp:panel id="sidemenu" runat="server">
                <uc1:sidemenu id="mainmenu" runat="server"></uc1:sidemenu>
            </asp:panel>
            <asp:panel id="content" runat="server">
                <form id="frmCourse" runat="server">
                    <input type="hidden" id="hidTodo" runat="server" />
                    <input type="hidden" id="hidCourseID" runat="server" />
                    <input type="hidden" id="hidPublishedCourseID" runat="server" />
                    <input type="hidden" id="hidPublishedCourse" runat="server" />
                    <input type="hidden" id="hidCourseOffering" runat="server" />
                    <input type="hidden" id="hidCourseBeginSTRM" runat="server" />
                    <input type="hidden" id="hidCourseEndSTRM" runat="server" />
                    <input type="hidden" id="hidCollegeList" runat="server" />
                    <input type="hidden" id="hidVariableUnits" runat="server" />
                    <input type="hidden" id="hidDescriptionCourseID" runat="server" />
                    <table class="centeredTable" style="width:727px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-left:4px;padding-right:0px;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="background-color:#000000;">
                                            <table style="width:100%;" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td style="background-color:#5f7568;background-image:URL('../Images/Themes/Blue/gradient.gif');width:100%;"
                                                        class="portletHeader">
                                                        <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="portletHeader" style="width:100%;">&nbsp;Course Archives</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="portletMain" style="width:100%">
                                                        <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="portletDark" style="width:100%;">
                                                                    <asp:panel runat="server" id="panDisplay">
                                                                        <asp:table runat="server" id="tblDisplay" style="width:100%;" cellspacing="1" cellpadding="2">
																			<asp:TableRow>
																				<asp:TableCell ColumnSpan="6" cssclass="portletSecondary" style="padding-right:4px;padding-bottom:4px;">
																					<table cellpadding="0" cellspacing="0" style="float:right;">
																						<tr>
                                                                                            <td style="padding-right:10px;">
																								<b>Effecitve Date:</b>&nbsp;&nbsp;<asp:dropdownlist runat="server" id="cboTerm" cssclass="small" autopostback="true"></asp:dropdownlist>
																							</td>
																							<td>
																								<b>Subject Area:</b>&nbsp;&nbsp;<asp:dropdownlist runat="server" id="cboSubject" cssclass="small" autopostback="true"></asp:dropdownlist>
																							</td>
																						</tr>
																					</table>
																				</asp:TableCell>
																			</asp:TableRow>
                                                                            <asp:tablerow>
                                                                                <asp:tablecell style="text-align:center;width:100px;" cssclass="portletSecondary">
                                                                                    <b>Course Offering</b>
                                                                                </asp:tablecell>
                                                                                <asp:tablecell style="text-align:center;" cssclass="portletSecondary">
                                                                                    <b>Description</b>    
                                                                                </asp:tablecell>
                                                                                <asp:tablecell style="text-align:center;width:84px;" cssclass="portletSecondary">
                                                                                    <b>Effective Date</b>
                                                                                </asp:tablecell>
                                                                                <asp:tablecell style="text-align:center;width:84px;" cssclass="portletSecondary">
                                                                                    <b>End Term</b>
                                                                                </asp:tablecell>
                                                                                <asp:tablecell style="text-align:center;width:78px;" cssclass="portletSecondary">
                                                                                    <b>Status</b>
                                                                                </asp:tablecell>
                                                                                <asp:tablecell style="text-align:center;width:74px;" cssclass="portletSecondary">
                                                                                </asp:tablecell>
                                                                            </asp:tablerow>
                                                                        </asp:table>
                                                                    </asp:panel>
                                                                    <asp:panel runat="server" id="panView">
                                                                        <table cellspacing="1" cellpadding="2" style="width:100%;">
                                                                            <!-- Error Message -->
                                                                            <asp:panel runat="server" id="panError">
                                                                                <tr>
                                                                                    <td colspan="4" class="portletMain" style="color:red;padding-left:5px;padding-top:3px;padding-bottom:5px;">
                                                                                        <asp:label runat="server" id="lblErrorMsg"></asp:label>
                                                                                    </td>
                                                                                </tr>
                                                                            </asp:panel>
                                                                            <tr>
                                                                                <td class="portletMain" colspan="4" style="padding:6px;padding-top:5px;">
																					<table cellpadding="0" cellspacing="0" style="width:100%;">
																						<tr>
																							<td style="width:70%;">
																								<b>&nbsp;Last Modified by:</b>&nbsp;<asp:label runat="server" id="lblLastModifiedEMPLID"></asp:label>&nbsp;<b>on:</b>&nbsp;<asp:label runat="server" id="lblLastModifiedDate"></asp:label>
																							</td>
																							<td style="width:30%;text-align:right;">
																								<!--<input type="checkbox" runat="server" id="chkRevision" onclick="prepareRevision(this.checked);" /> &nbsp;<b>Course Revision</b> -->
																							</td>
																						</tr>
																					</table>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Course Offering, Term -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="width:15%;vertical-align:top;text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Course Offering:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:18%;vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCourseOffering"></asp:label>
                                                                                </td>
                                                                                <td class="portletSecondary" style="width:23%;text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Effective Date:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="width:44%;vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblEFFDT"></asp:label>
                                                                                    
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Subject, Subject Description -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Subject Area:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCourseSubject"></asp:label>
                                                                                </td>
                                                                                <td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Subject Area Description:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblSubjectDescription"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Catalog Number, Course Description -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Catalog Nbr:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCatalogNbr"></asp:label>
                                                                                </td>
                                                                                <td class="portletSecondary" style="text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Description:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCourseShortTitle"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Suffix, Long Course Title -->
                                                                            <tr>
                                                                                <td class="portletSecondary" style="vertical-align:top;text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Suffix:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding-left:6px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCourseSuffix"></asp:label>
                                                                                </td>
                                                                                <td class="portletSecondary" style="vertical-align:top;text-align:right;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Long Course Title:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="padding-left:6px;padding-right:5px;padding-top:6px;padding-bottom:6px;">
                                                                                    <asp:label runat="server" id="lblCourseLongTitle"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Units, Offered At -->
                                                                            <tr>
                                                                                <td rowspan="2" class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Units:&nbsp;</b>
                                                                                </td>
                                                                                <td rowspan="2" class="portletLight" style="vertical-align:top;padding:6px;">
                                                                                    <asp:label runat="server" id="lblVariableUnits"></asp:label><br />
                                                                                    Max:&nbsp;<asp:label runat="server" id="lblUnitsMaximum"></asp:label>
                                                                                    &nbsp;Min:&nbsp;<asp:label runat="server" id="lblUnitsMinimum"></asp:label>    
                                                                                </td>
                                                                                <td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Offered at:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding:6px;">
                                                                                    <asp:label runat="server" id="lblOfferedAt"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
																				<td class="portletSecondary" style="text-align:right;vertical-align:top;padding-top:6px;padding-bottom:6px;">
                                                                                    <b>Status:&nbsp;</b>
                                                                                </td>
                                                                                <td class="portletLight" style="vertical-align:top;padding:6px;">
                                                                                    <asp:label runat="server" id="lblPublishedCourse"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" class="portletSecondary" style="vertical-align:bottom;padding:6px;padding-bottom:2px;">
                                                                                    <table cellpadding="0" cellspacing="0" style="width:100%;">
                                                                                        <tr>
                                                                                            <td style="width:230px;">&nbsp;<b>Long Description:</b></td>
                                                                                            <asp:panel runat="server" id="panSameAs">
                                                                                                <td style="text-align:right;"><b>Same Long Description As:&nbsp;</b>
                                                                                                    <asp:label runat="server" id="lblDescriptionCourseOffering"></asp:label>   
                                                                                                </td>
                                                                                            </asp:panel>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" class="portletLight" style="padding:8px;">
                                                                                    <asp:label runat="server" id="lblCourseLongDescription"></asp:label>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- BUTTON -->
                                                                            <tr>
                                                                                <td class="portletMain" colspan="4" style="text-align:center;padding:3px;">
                                                                                    <input type="button" id="cmdBack" value="Back" class="small" style="background:#ccccaa;border:1px solid #000000;width:105px;" onclick="history.back(1);" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </asp:panel>
            <div class="clearer"></div>
        </asp:panel>
    </body>
</html>



