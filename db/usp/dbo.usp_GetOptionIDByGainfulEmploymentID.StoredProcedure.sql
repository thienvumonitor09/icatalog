USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOptionIDByGainfulEmploymentID]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/17/17
-- Description:	Returns ProgramVersionID, ProgramDegreeID and OptionID based on GainfulEmploymentID for the current quarter program option
--		 INPUT: GainfulEmploymentID - Gainful Employment Course ID for Program Options
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOptionIDByGainfulEmploymentID] 
	@GainfulEmploymentID varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	/*Store the current term*/
	DECLARE @STRM varchar(4)
	SET @STRM = (SELECT TOP 1(STRM)
					 FROM CCSGen_ctcLink_ODS.dbo.vw_TermTable
					 WHERE TERM_END_DT > GETDATE()
					 ORDER BY TERM_END_DT)

    SELECT OptionID, ProgramDegree.ProgramDegreeID, Program.ProgramVersionID
    FROM ProgramDegreeOption 
    INNER JOIN ProgramDegree 
    ON ProgramDegree.ProgramDegreeID = ProgramDegreeOption.ProgramDegreeID 
    INNER JOIN Program 
    ON Program.ProgramVersionID = ProgramDegree.ProgramVersionID 
    WHERE ProgramBeginSTRM <= @STRM AND (ProgramEndSTRM = 'Z999' OR ProgramEndSTRM >= @STRM) 
    AND PublishedProgram = 1 
    AND GainfulEmploymentID = @GainfulEmploymentID;
END

GO
