USE [CCSiCatalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProgramDescription]    Script Date: 6/18/2019 8:31:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandy Vaughn
-- Create date: 5/1/17
-- Description:	Returns program description details for a specific program
--		 INPUT: ProgramVersionID - Program Version Primary Key
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProgramDescription] 
	@ProgramVersionID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT Program.ProgramID, ProgramDescription, ProgramTitle, AreaOfStudyProgram.AreaOfStudyID, AreaOfStudy.Title, ProgramCategory.CategoryID, CategoryTitle, CollegeShortTitle, College.CollegeID, PublishedProgramVersionID, ProgramBeginSTRM, BeginTerm.DESCR AS BeginTerm_DESCR, ProgramEndSTRM, EndTerm.DESCR AS EndTerm_DESCR, PublishedProgram, Program.LastModifiedEMPLID, Program.LastModifiedDate
	FROM Program 
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS BeginTerm
	ON BeginTerm.STRM = ProgramBeginSTRM
	LEFT OUTER JOIN CCSGen_ctcLink_ODS.dbo.vw_TermTable AS EndTerm
	ON EndTerm.STRM = ProgramEndSTRM
	INNER JOIN College 
	ON College.CollegeID = Program.CollegeID 
	LEFT OUTER JOIN AreaOfStudyProgram
	ON AreaOfStudyProgram.ProgramID = Program.ProgramID
	AND PrimaryAreaOfStudy = 1
	LEFT OUTER JOIN AreaOfStudy
	ON AreaOfStudy.AreaOfStudyID = AreaOfStudyProgram.AreaOfStudyID
	LEFT OUTER JOIN ProgramCategory 
	ON ProgramCategory.ProgramVersionID = Program.ProgramVersionID 
	AND PrimaryCategory = 1 
	LEFT OUTER JOIN Category 
	ON Category.CategoryID = ProgramCategory.CategoryID 
	WHERE Program.ProgramVersionID = @ProgramVersionID;
END

GO
